// mbbComparisonPlots.C

#include "../baseCodeForPlots/twoDistributionsGaussianFit.C"

void mbbComparisonPlots(TString sampleName, TString title, TString inputDirectory)
{

  std::cout << sampleName << " " << title << " " << inputDirectory << std::endl;
  TString treePath = "mutau_tree";

  TString outputDirectory = "/eos/user/s/skkwan/hToAA/mbbComparisonPlots/";
  gSystem->Exec("mkdir -p " + outputDirectory);
  outputDirectory = outputDirectory + sampleName + "/";
  gSystem->Exec("mkdir -p " + outputDirectory);

  //  TString cut1 = "(bpt_deepflavour_1 > 20) && (bpt_deepflavour_1 < 400) && (bnMuons_deepflavour_1 > 0)";
  TString cut1 = "(bpt_deepflavour_1 > 20) && (bpt_deepflavour_1 < 400) && (bpt_deepflavour_2 > 20) && (bpt_deepflavour_2 < 400) && (bnMuons_deepflavour_1 > 0) && (bnMuons_deepflavour_2 > 0)";
  //  TString cut1 = "(bpt_deepflavour_1 > 20) && (bpt_deepflavour_1 < 400) && (bpt_deepflavour_2 > 20) && (bpt_deepflavour_2 < 400)";

  int nBins = 25;

  bool useLogY = false;
  float myMaxY = 0.17;
  twoDistributionPlots("mbb_deepFlav_res_comparison",
		       "(1.1 * mbb - mbbGenWN)/ mbbGenWN",
		       "(mbbCorr - mbbGenWN) / mbbGenWN",
		       cut1, title, treePath, inputDirectory, outputDirectory,
		       "m_{bb} resolution",
		       nBins, -0.5, 0.5, useLogY, myMaxY);

}
