# runResolutionPlots.sh
# Usage: source runResolutionPlots.sh

#########################################################
# Resolution of only the leading and sub-leading jet pT
#########################################################

# Commented out because I don't want to re-make these right now
# root -l -b -q 'bjetEnergyResolutionPlots.C("TTToHadronic", "TT To Hadronic", "/eos/user/s/skkwan/hToAA/skims/Nov-15-2020-22h34m/TTToHadronicSkim.root")'
# root -l -b -q 'bjetEnergyResolutionPlots.C("TTTo2L2Nu",    "TT To 2L2Nu",    "/eos/user/s/skkwan/hToAA/skims/Nov-15-2020-22h34m/TTTo2L2NuSkim.root")'
# root -l -b -q 'bjetEnergyResolutionPlots.C("TTToSemiLeptonic", "TT to Semileptonic", "/eos/user/s/skkwan/hToAA/skims/Nov-15-2020-22h34m/TTToSemiLeptonicSkim.root")'

#########################################################
# Resolution of m_bb
#########################################################

# root -l -b -q 'mbbResolutionPlots.C("TTToHadronic", "TT To Hadronic", "/eos/user/s/skkwan/hToAA/skims/Dec-04-2020-22h58m/TTToHadronicSkim.root")'
# root -l -b -q 'mbbResolutionPlots.C("TTTo2L2Nu",    "TT To 2L2Nu", "/eos/user/s/skkwan/hToAA/skims/Dec-04-2020-22h58m/TTTo2L2NuSkim.root")'
# root -l -b -q 'mbbResolutionPlots.C("TTToSemiLeptonic", "TT to Semileptonic", "/eos/user/s/skkwan/hToAA/skims/Dec-04-2020-22h58m/TTToSemiLeptonicSkim.root")'

# root -l -b -q 'mbbResolutionPlots.C("SUSYVBFHToAA_AToBB_AToTauTau_M-35", "VBF H to aa (35 GeV)", "/eos/user/s/skkwan/hToAA/skims/Jan-13-2021-01h10m/SUSYVBFToHToAA_AToBB_AToTauTau_M-35Skim.root")'
# root -l -b -q 'mbbResolutionPlots.C("SUSYGluGluToHToAA_AToBB_AToTauTau_M-35", "gg H to aa (35 GeV)", "/eos/user/s/skkwan/hToAA/skims/Jan-13-2021-01h10m/SUSYGluGluToHToAA_AToBB_AToTauTau_M-35Skim.root")'

# root -l -b -q 'mbbResolutionPlots.C("SUSYVBFHToAA_AToBB_AToTauTau_M-60", "VBF H to aa (60 GeV)", "/eos/user/s/skkwan/hToAA/skims/Jan-13-2021-01h32m/SUSYVBFToHToAA_AToBB_AToTauTau_M-60Skim.root")'
# root -l -b -q 'mbbResolutionPlots.C("SUSYGluGluToHToAA_AToBB_AToTauTau_M-60", "GluGlu H to aa (60 GeV)", "/eos/user/s/skkwan/hToAA/skims/Jan-13-2021-01h32m/SUSYGluGluToHToAA_AToBB_AToTauTau_M-60Skim.root")'

#########################################################
# Comparison of scaled m_bb resolution (no regression) and m_bb resolution (with regression)
#########################################################

root -l -b -q 'mbbComparisonPlots.C("TTToHadronic", "TT To Hadronic", "/eos/user/s/skkwan/hToAA/skims/Dec-04-2020-22h58m/TTToHadronicSkim.root")'
root -l -b -q 'mbbComparisonPlots.C("TTTo2L2Nu",    "TT To 2L2Nu", "/eos/user/s/skkwan/hToAA/skims/Dec-04-2020-22h58m/TTTo2L2NuSkim.root")'
root -l -b -q 'mbbComparisonPlots.C("TTToSemiLeptonic", "TT to Semileptonic", "/eos/user/s/skkwan/hToAA/skims/Dec-04-2020-22h58m/TTToSemiLeptonicSkim.root")'

root -l -b -q 'mbbComparisonPlots.C("SUSYVBFHToAA_AToBB_AToTauTau_M-35", "VBF H to aa (35 GeV)", "/eos/user/s/skkwan/hToAA/skims/Jan-13-2021-01h10m/SUSYVBFToHToAA_AToBB_AToTauTau_M-35Skim.root")'
root -l -b -q 'mbbComparisonPlots.C("SUSYGluGluToHToAA_AToBB_AToTauTau_M-35", "gg H to aa (35 GeV)", "/eos/user/s/skkwan/hToAA/skims/Jan-13-2021-01h10m/SUSYGluGluToHToAA_AToBB_AToTauTau_M-35Skim.root")'

root -l -b -q 'mbbComparisonPlots.C("SUSYVBFHToAA_AToBB_AToTauTau_M-60", "VBF H to aa (60 GeV)", "/eos/user/s/skkwan/hToAA/skims/Jan-13-2021-01h32m/SUSYVBFToHToAA_AToBB_AToTauTau_M-60Skim.root")'
root -l -b -q 'mbbComparisonPlots.C("SUSYGluGluToHToAA_AToBB_AToTauTau_M-60", "GluGlu H to aa (60 GeV)", "/eos/user/s/skkwan/hToAA/skims/Jan-13-2021-01h32m/SUSYGluGluToHToAA_AToBB_AToTauTau_M-60Skim.root")'
