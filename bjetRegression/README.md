# Development area for b-jet regression studies

This copy of the 'ToolRoom' repository is for investigating
whether b-jet regression improves the b-jet energy resolution.

To make the resolution plots, in `bjetEnergyResolutionPlots/`
 1. Make sure the baseline cuts are correct in `mbbResolutionPlots.C`, e.g.
   `(bpt_deepflavour_1 > 20) && (bpt_deepflavour_1 < 400) && (abs(mbb - mbbGenWN) != 0.0)`, and/or
   `bjetEnergyResolutionPlots.C`.
 2. Make sure the output directories are correct in `mbbResolutionPlots.C`
    and `bjetEnergyResolutionPlots.C` (they go to my own EOS area):
        ```
	ll /eos/user/s/skkwan/hToAA/bjetEnergyResolutionPlots/
        ll /eos/user/s/skkwan/hToAA/mbbResolutionPlots/
        ```
 3. Make sure `runResolutionPlots.sh` points to the correct input n-tuples (e.g. in EOS storage)
 4. Run the script and view the output plots (see Step 2)
    	```
        cd bjetEnergyResolutionPlots/
        source runResolutionPlots.sh
	```
