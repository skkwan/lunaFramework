#!/bin/bash

# Usage: source condorSkim.sh [samples.csv] [-1 for all files, or finite number of files to run per dataset]

SAMPLES=$1
NFILESTORUN=$2

#--------------------------------------------------------
# Check that the input .list files exist first!
#--------------------------------------------------------
echo ">>> condorSkim.sh: Reading from ${SAMPLES}..."
while IFS=, read -r SAMPLE INPUT_DIR YEAR CONFIG_DIR XSEC LUMI SCALE NFILESPERJOB
do
    echo ">>> condorSkim.sh: Starting sample ${SAMPLE}: with config directory ${CONFIG_DIR} and input list of files: ${INPUT_DIR}"
    if [[ -d "../syncNanoAOD/"${INPUT_DIR} ]]; then
	echo "   >>> ${INPUT_DIR} exists"
    else
	echo "   >>> condorSkim.sh: [ERROR]: Input folder of files not found"
    fi

done < ${SAMPLES}

#--------------------------------------------------------
# Get the voms-proxy-info certificate
#--------------------------------------------------------
export MYPROXYPATH="$(voms-proxy-info -path)"
echo ">>> condorSkim.sh: Found proxy at: ${MYPROXYPATH}, copying to /afs/cern.ch/user/s/skkwan/private/x509up_file"

cp ${MYPROXYPATH} /afs/cern.ch/user/s/skkwan/private/x509up_file

#--------------------------------------------------------
# Get the current date and time, needed for output files
#--------------------------------------------------------

DATETIME="$(date +"%b-%d-%Y-%Hh%Mm")"

EOS_DIR="/eos/user/s/skkwan/hToAA/condor"
WORKING_DIR="/afs/cern.ch/work/s/skkwan/public/hToAA/syncNanoAOD"
JOB_DIR="/afs/cern.ch/work/s/skkwan/private/condor_hToAA"

mkdir -p ${EOS_DIR}/${DATETIME}/
cp README.md ${EOS_DIR}/${DATETIME}/
echo  ">>> condorSkim.sh: Wrote ${EOS_DIR}/${DATETIME}/README.md"


# Access the .csv list of samples to run over
while IFS=, read -r SAMPLE INPUT_DIR YEAR CONFIG_REL_DIR XSEC LUMI SCALE NFILESPERJOB
do
    {
	EOSDIR=${EOS_DIR}/${DATETIME}/${SAMPLE}
	JOBDIR=${JOB_DIR}/${DATETIME}/${SAMPLE}

	mkdir -p ${EOSDIR}
	mkdir -p ${JOBDIR}
	echo ">------>"
	echo ">>> condorSkim.sh: [Time: $(TZ=America/New_York date)] ${SAMPLE}, writing to ${EOSDIR}/"

	echo ">>> eosdir: ${EOSDIR}"
        echo ">>> workingdir: ${WORKING_DIR}"
        echo ">>> jobdir: ${JOBDIR}"

	# Calculate how many 'queue' commands to submit
	N_TOTAL_INSTANCES=`find ${WORKING_DIR}/${INPUT_DIR}/ -name "*BATCH*" | xargs ls -lrt | wc -l`
	if [[ ${NFILESTORUN} == "-1" ]]
	    then NINSTANCES=${N_TOTAL_INSTANCES}
	else
	    NINSTANCES=${NFILESTORUN}
	fi

	echo ">>> Submitting ${NINSTANCES} .list files, out of ${N_TOTAL_INSTANCES} total .lists..."


	subfile=${JOBDIR}/${SAMPLE}_job-${NINSTANCES}-out-of-${N_TOTAL_INSTANCES}.sub
	echo ${subfile}

	JOB_OUTPUT="${JOBDIR}/${SAMPLE}_\$(Process).root"

	INPUT_LIST="${WORKING_DIR}/${INPUT_DIR}/${SAMPLE}_BATCH_\$(Process).list"
	CONFIG_DIR="${WORKING_DIR}/${CONFIG_REL_DIR}"

	echo ${WORKING_DIR}
	echo ${INPUT_DIR}
	echo ${INPUT_LIST}
	echo ${JOB_OUTPUT}

	# Edit parameters in the Condor .sub file
	cp jobTemplate.sub ${subfile}
	# cp jobTemplateLarge.sub ${subfile}
	sed -i "s|(sample)|${SAMPLE}|g" ${subfile}
	sed -i "s|(inputdir)|${INPUT_LIST}|g" ${subfile}
	sed -i "s|(year)|${YEAR}|g" ${subfile}
	sed -i "s|(configdir)|${CONFIG_DIR}|g" ${subfile}
        sed -i "s|(xsec)|${XSEC}|g" ${subfile}
        sed -i "s|(lumi)|${LUMI}|g" ${subfile}
        sed -i "s|(scale)|${SCALE}|g" ${subfile}

	sed -i "s|(eosdir)|${EOSDIR}|g" ${subfile}
	sed -i "s|(workingdir)|${WORKING_DIR}|g" ${subfile}
	sed -i "s|(job_output)|${JOB_OUTPUT}|g" ${subfile}
	sed -i "s|(jobdir)|${JOBDIR}|g" ${subfile}

	sed -i "s|(nInstances)|${NINSTANCES}|g" ${subfile}
	cat ${subfile}

	condor_submit ${subfile}


    }

done < ${SAMPLES}
