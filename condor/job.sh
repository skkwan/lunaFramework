#!/bin/bash

#-----------------------------------------------
# This is the bash script called by job.sub
#-----------------------------------------------

export X509_USER_PROXY=$1
voms-proxy-info -all
voms-proxy-info -all -file $1

export sample=$2
export input_dir=$3
export year=$4
export config_dir=$5
export xsec=$6
export lumi=$7
export scale=$8
export eosdir=$9
export job_output="${10}"

# source /cvmfs/sft.cern.ch/lcg/views/LCG_101/x86_64-centos7-gcc8-opt/setup.sh   # Feb 22 2022 switch command to the same one used in runSkim.sh

source /cvmfs/sft.cern.ch/lcg/views/LCG_95/x86_64-centos7-gcc8-opt/setup.sh
# No longer using startline/nfilesperjob scheme
# export JOB_OUTPUT=${jobdir}/${sample}_start-${startline}_size-${nfilesperjob}_${CLUSTERID}-${PROCID}.root


echo "eosdir: ${eosdir}"

# Use user CERNBOX as EOS instance
export EOS_MGM_URL=root://eosuser.cern.ch
# export EOSOUT="/eos/user/s/skkwan/hToAA/condor/"${sample}"/"


echo "Attempting to execute: /afs/cern.ch/work/s/skkwan/public/hToAA/syncNanoAOD/skim ${sample} ${input_dir} ${job_output} ${config_dir} ${xsec} ${lumi} ${scale}"

# 1. Sample Name
# 2. Process input (.list)
# 3. Path to output file
# 4. Configuration directory
# 5. xsec
# 6. lumi
# 7. scale
# 8. (optional) start line
# 9. (optional) number of lines to process
/afs/cern.ch/work/s/skkwan/public/hToAA/syncNanoAOD/skim ${sample} ${input_dir} ${job_output} ${config_dir} ${xsec} ${lumi} ${scale}

eos cp ${job_output} ${eosdir}
rm ${job_output}
