#!/bin/bash

# Wrapper for entire condor procedure. Make sure:
# 1. you have run: voms-proxy-init
# 2. in the syncNanoAOD/ directory, make sure the skim executable is compiled and up-to-date
# 3. To try a few jobs, do not use -mini.csv because now we rely on ls *.list for the "queue N" argument; instead point to the
#    full .csv and use the last argument to specify queue N


echo ">>> runCondorSubmit.sh: make sure syncNanoAOD/ directory has the skim executable you want to run ... "


#source condorSkim.sh ../syncNanoAOD/skim_syncOnly.csv 3

#source condorSkim.sh ../syncNanoAOD/skim_TEMP_FULL_stack_2018.csv 1


#source condorSkim.sh ../syncNanoAOD/skim_FULL_stack_2018.csv -1

source condorSkim.sh ../syncNanoAOD/skim_preapprovalCheck_2018.csv -1

#source condorSkim.sh ../syncNanoAOD/skim_recoilCorrectionsOnly_2018.csv -1

#source condorSkim.sh ../syncNanoAOD/skim_signal2018.csv -1
