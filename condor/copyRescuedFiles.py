# copyRescuedFiles.py
# Usage: python copyRescuedFiles.py [args]

import argparse
import os
import re
import shutil

parser = argparse.ArgumentParser(
    description="Check a folder of Condor log files for failed jobs."
)

parser.add_argument(
    "--maineos",
    help="Target EOS directory to copy rescued *.root files TO",
    required=True,
)
parser.add_argument(
    "--maindir",
    help="Target directory in priv/ to copy rescued log files TO",
    required=True,
)
parser.add_argument(
    "--rescuedir",
    help="Directory in /priv with rescue logs to scan and copy FROM",
    required=True,
)
parser.add_argument(
    "--rescueeos", help="Directory in EOS with .root files to copy FROM", required=True
)
parser.set_defaults(execute=False)

args = parser.parse_args()

# The string in the .out file that indicates a succesful job
proofstring = "Valid tau in selected pair"

# Loop through log files in the /priv directory of the rescue job
# If the job was successful, copy the corresponding .root file in args.rescueeos
# to the args.maineos where we are collecting all the successful .root files.
for subdir, dirs, files in os.walk(args.rescuedir):
    sample = os.path.basename(subdir)

    # Loop through the Condor log files
    for filename in files:
        path = os.path.join(subdir, filename)

        # In .out files, check for the proofstring
        if ".out" in path:
            with open(path) as f:
                if proofstring in f.read():
                    num = re.findall("\d+(?=\.\w+$)", filename)

                    # Make sure we unequivocably know the batch number
                    if len(num) > 1:
                        sys.exit("Could not find batch number in " + filename)

                    batchnum = num[0]

                    # Get the target .root file
                    targetRoot = sample + "_" + batchnum + ".root"
                    targetLogs = "test_" + sample + "_" + batchnum + "*"
                    print(os.path.join(args.rescueeos, sample, targetRoot))
                    print(os.path.join(args.maineos, sample))

                    # Copy the rescued *.root file
                    os.system(
                        "cp %s %s"
                        % (
                            os.path.join(args.rescueeos, sample, targetRoot),
                            os.path.join(args.maineos, sample),
                        )
                    )
                    # Copy the rescued log files
                    os.system(
                        "cp %s %s"
                        % (
                            os.path.join(args.rescuedir, sample, targetLogs),
                            os.path.join(args.maindir, sample),
                        )
                    )
