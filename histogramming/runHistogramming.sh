#!/bin/bash

#--------------------------------------------------------
# Usage:
#   bash getPaths.sh
#
# Once this is done, or, if we are running over local (lxplus /eos/) files:
#   cmsenv
#   cd ${CMSSW_BASE}/src/lunaFramework/
#   source venv-LUNA/bin/activate
#   bash runHistogramming.sh
#
#--------------------------------------------------------

# COMPILE: if false, do not compile histogramming.cxx, if true, compile
COMPILE=true
# SAMPLES: path to the list of files to run on
SAMPLES="hists-2018-syncOnly.csv"
# RUN_HISTOGRAMMING: true to actually run the compiled C++ executable, otherwise do nothing after attempting to compile
RUN_HISTOGRAMMING=true
# DO_SHIFTS: 1 (true) to do all systematics, 0 (false) to only do the nominal values
DO_SHIFTS=1
# CHANNEL: 0 for mutau, 1 for etau, 2 for emu
CHANNEL=0
# 0: inclusive, 5: do all categories (1: low mass SR, 2: medium mass SR, 3: high mass SR, 4: high mass CR)
CATEGORY_NUMBER=0
# DO_ALL_VARIABLES is 0 (false) to only do m_vis, 1 (true) to do all variables specified in histogramming.cxx
DO_ALL_VARIABLES=0
# IS_LOCAL is 1 (true) if running locally on lxplus, 0 if running on Condor
IS_LOCAL=1
# Output directory (point to EOS area, or leave as "" if you want to write it locally)
OUTDIR="/eos/cms/store/group/phys_susy/AN-24-166/${USER}/test"

#--------------------------------------------------------
# Check if we used bash
#--------------------------------------------------------
if [[ "$0" != "$BASH_SOURCE" ]]; then
    echo ">>> ${BASH_SOURCE[0]}: Error: Script must be run with bash"
    return
fi
# #--------------------------------------------------------
# # Check if environments are set
# #--------------------------------------------------------
# if [[ -z ${CMSSW_BASE} ]]; then
#     echo ">>> ${BASH_SOURCE[0]}: CMSSW environment is not set! Make sure to do cmsenv"
#     exit 1
# fi
# if [[ -z ${VIRTUAL_ENV} ]]; then
#     echo ">>> ${BASH_SOURCE[0]}: Virtual environment is not set! Make sure to source venv-LUNA/bin/activate"
#     exit 1
# fi


#--------------------------------------------------------
# First check if the input files exists
#--------------------------------------------------------
while IFS=, read -r SAMPLE_INFO PROCESS YEAR JOBSIZE
do
    {
    # If stored in eos,
    if [[ ${SAMPLE_INFO} =~ ^/eos/ ]]; then
        if [[ ! $(ls -f ${SAMPLE_INFO}) ]]; then
            echo "${BASH_SOURCE[0]}: ERROR: File ${SAMPLE_INFO} is not there, aborting. (Make sure there are no extra newlines in ${SAMPLES}.)"
            exit 1
        fi
    else
    # Else use gfal-ls for remote files
        if [[ ! $(gfal-ls ${SAMPLE_INFO}) ]]; then
            echo "${BASH_SOURCE[0]}: ERROR: tried to gfal-ls file ${SAMPLE_INFO} but it is is not there, aborting. (Make sure there are no extra newlines in ${SAMPLES}.)"
            exit 1
         fi
    fi
    }
done < ${SAMPLES}

while IFS=, read -r SAMPLE_INFO PROCESS YEAR
do
{
    # Make a temporary .list of the input n-tuples
    mkdir -p paths/${YEAR}/
    INPUT_LIST="paths/${YEAR}/inputs_${PROCESS}.list"

    # If the files are in /eos/, a simple "find" will work
    if [[ ${SAMPLE_INFO} =~ ^/eos/ ]]; then
        find $(dirname ${SAMPLE_INFO})/${PROCESS}/*.root -type f > ${INPUT_LIST}
        cat ${INPUT_LIST}
        echo "..."
    fi
}
done < ${SAMPLES}


#--------------------------------------------------------
# Compile executable
#--------------------------------------------------------
if [[ "$COMPILE" = true ]]; then
    echo ">>> ${BASH_SOURCE[0]}: Compiling histogramming.cxx executable ..."
    TARGET_FILES="helpers/histoConfig_class.cc helpers/helperFunctions.cc"
    COMPILER=$(root-config --cxx)
    FLAGS=$(root-config --cflags --libs)
    time $COMPILER -g -O3 -Wall -Wextra -Wpedantic -lRooFitCore -lRooFit -o histogramming histogramming.cxx $FLAGS ${TARGET_FILES}
    if [[ $? -ne 0 ]]; then
        echo ">>> Compile failed, exit"
        exit 1
    fi
fi


#--------------------------------------------------------
# Then run the histogramming
#--------------------------------------------------------
start=$(date +%s)

DATETIME="$(date +"%Y-%m-%d-%Hh%Mm")"

if [[ "${RUN_HISTOGRAMMING}" = true ]]; then
    while IFS=, read -r SAMPLE_INFO PROCESS YEAR
    do
    {
        INPUT_LIST="paths/${YEAR}/inputs_${PROCESS}.list"

	    LOG="${OUTDIR}/logRun_${PROCESS}"
        mkdir -p ${OUTDIR}

        # histogramming.cxx will replace "channel" with the channel that was run on
	    echo ">>> ${BASH_SOURCE[0]}: Starting histogramming executable for ${PROCESS}, writing output histograms to ${OUTPUT_HIST}"
        if [[ ${CATEGORY_NUMBER} == 0 ]]; then
            OUTPUT_HIST="${OUTDIR}/hists_${PROCESS}_channel_ch_${CHANNEL}.root"
            COMMAND="./histogramming ${SAMPLE_INFO} ${INPUT_LIST} ${PROCESS} ${YEAR} ${OUTPUT_HIST} ${CHANNEL} ${CATEGORY_NUMBER} ${DO_SHIFTS} ${DO_ALL_VARIABLES} ${IS_LOCAL} | tee ${LOG}_channel_${CHANNEL}_${PROCESS} &"
            echo ">>> Executing command ${COMMAND}"
            eval ${COMMAND}
        fi

        if [[ ${CATEGORY_NUMBER} == 5 ]]; then
            OUTPUT_HIST="${OUTDIR}/hists_${PROCESS}_channel_ch_${CHANNEL}_cat_0.root"
            COMMAND="./histogramming ${SAMPLE_INFO} ${INPUT_LIST} ${PROCESS} ${YEAR} ${OUTPUT_HIST} ${CHANNEL} 0 ${DO_SHIFTS} ${DO_ALL_VARIABLES} ${IS_LOCAL} | tee ${LOG}_channel_${CHANNEL}_${PROCESS} &"
            echo ">>> Executing command ${COMMAND}"
            eval ${COMMAND}
            OUTPUT_HIST="${OUTDIR}/hists_${PROCESS}_channel_ch_${CHANNEL}_cat_1.root"
            COMMAND="./histogramming ${SAMPLE_INFO} ${INPUT_LIST} ${PROCESS} ${YEAR} ${OUTPUT_HIST} ${CHANNEL} 1 ${DO_SHIFTS} ${DO_ALL_VARIABLES} ${IS_LOCAL} | tee ${LOG}_channel_${CHANNEL}_${PROCESS} &"
            echo ">>> Executing command ${COMMAND}"
            eval ${COMMAND}
            OUTPUT_HIST="${OUTDIR}/hists_${PROCESS}_channel_ch_${CHANNEL}_cat_2.root"
            COMMAND="./histogramming ${SAMPLE_INFO} ${INPUT_LIST} ${PROCESS} ${YEAR} ${OUTPUT_HIST} ${CHANNEL} 2 ${DO_SHIFTS} ${DO_ALL_VARIABLES} ${IS_LOCAL} | tee ${LOG}_channel_${CHANNEL}_${PROCESS} &"
            echo ">>> Executing command ${COMMAND}"
            eval ${COMMAND}
            OUTPUT_HIST="${OUTDIR}/hists_${PROCESS}_channel_ch_${CHANNEL}_cat_3.root"
            COMMAND="./histogramming ${SAMPLE_INFO} ${INPUT_LIST} ${PROCESS} ${YEAR} ${OUTPUT_HIST} ${CHANNEL} 3 ${DO_SHIFTS} ${DO_ALL_VARIABLES} ${IS_LOCAL} | tee ${LOG}_channel_${CHANNEL}_${PROCESS} &"
            echo ">>> Executing command ${COMMAND}"
            eval ${COMMAND}
            OUTPUT_HIST="${OUTDIR}/hists_${PROCESS}_channel_ch_${CHANNEL}_cat_4.root"
            COMMAND="./histogramming ${SAMPLE_INFO} ${INPUT_LIST} ${PROCESS} ${YEAR} ${OUTPUT_HIST} ${CHANNEL} 4 ${DO_SHIFTS} ${DO_ALL_VARIABLES} ${IS_LOCAL} | tee ${LOG}_channel_${CHANNEL}_${PROCESS} &"
            echo ">>> Executing command ${COMMAND}"
            eval ${COMMAND}
        fi

        # Hadd the outputs
        wait

	} &
done < ${SAMPLES}
fi
wait

endtime=$(( $(date +%s) - $start ))

echo "Done in ${endtime} seconds"
