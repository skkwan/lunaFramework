/*
 * Implementation of the histogramming step of the analysis.
 * Histogramming takes skimmed, post-processed n-tuples, and generates histograms
 * for the plotting stage.
 */

#include "ROOT/RDataFrame.hxx"

#include "ROOT/RVec.hxx"
#include <ROOT/RDF/RResultMap.hxx>
#include <ROOT/RDFHelpers.hxx>   // need this for VariationsFor()

#include "Math/Vector4D.h"
#include "RooWorkspace.h"
#include "TChain.h"
#include "TGraphAsymmErrors.h"
#include "TStopwatch.h"

#include <stdlib.h>
#include <iostream>
#include <cmath>

#include "helpers/btagJetCounting-histo.h"
#include "helpers/btagEffScaleFactors-histo.h"
#include "helpers/cut.h"
#include "helpers/electronES-histo.h"
#include "helpers/embeddedTauTracking-histo.h"
#include "helpers/fileIO.h"
#include "helpers/helperFunctions.h"
#include "helpers/jetFakeBackground-histo.h"
#include "helpers/jetFakeBackground-crossTrigger-histo.h"
#include "helpers/jerShifts-histo.h"
#include "helpers/muonES-histo.h"
#include "helpers/qcdMultiJetBackground-histo.h"
#include "helpers/ranges.h"
#include "helpers/recoilAndUnclusteredCorrections-histo.h"
#include "helpers/histoConfig_class.h"
#include "helpers/optionalRecomputeTriggerDecision-histo.h"
#include "helpers/overlapEmbedded-histo.h"
#include "helpers/prefiring-histo.h"
#include "helpers/tauES-histo.h"
#include "helpers/tauIDeffVsEleScaleFactors-histo.h"
#include "helpers/tauIDeffVsMuScaleFactors-histo.h"
#include "helpers/tauIDScaleFactors-histo.h"
#include "helpers/tauIDwpScaleFactor-histo.h"
#include "helpers/topPtReweighing-histo.h"
#include "helpers/triggerDecisions-histo.h"
#include "helpers/triggerEfficienciesTauTau-histo.h"
#include "helpers/zPtReweighing-histo.h"


/*
 * Create histograms from skimmed n-tuples with associated DNN scores.
 */
int main(int argc, char **argv) {

  // for (int count{ 0 }; count < argc; ++count) {
  //      std::cout << count << " " << argv[count] << '\n';
  //}

    int nArgc = 11;
    if (argc != nArgc) {
        std::cout << "[Error:] Incorrect number of arguments -- Use executable with following arguments: ./histogramming sampleInfoDir sampleListDir process year outputHist channel doShifts doCategories doAllVariables isLocalJob" << std::endl;
        std::cout << "[Error:] Found " << argc << " arguments instead of " << nArgc << std::endl;
        return -1;
    }
    std::cout << gROOT->GetVersion() << std::endl;

    std::string sampleInfoDir = argv[1];
    std::cout << ">>> Path to input info file: " << sampleInfoDir << std::endl;

    std::string sampleListDir = argv[2];
    std::cout << ">>> Path to list of input n-tuples: " << sampleListDir << std::endl;

    std::string process = argv[3];
    std::cout << ">>> Physics process name: " << process << std::endl;

    int year = atoi(argv[4]);
    std::cout << ">>> Year: " << year << std::endl;

    std::string outputFile = argv[5];
    std::cout << ">>> Output histogram: " << outputFile << std::endl;

    int channel = atoi(argv[6]);
    std::cout << ">>> Which channel to do (0 is mutau, 1 is etau, 2 is emu): " << channel << std::endl;

    int nCategory = atoi(argv[7]);
    std::cout << ">>> Category to do (0: inclusive, 1: low mass SR, 2: medium mass SR, 3: high mass SR, 4: high mass CR): " << nCategory << std::endl;

    bool doShifts = (bool) atoi(argv[8]);
    std::cout << ">>> Do shifts (0 is False): " << doShifts << std::endl;

    bool doAllVariables = (bool) atoi(argv[9]);
    std::cout << ">>> Do all variables (0 is False, m_tt only, 1: all variables): " << doAllVariables << std::endl;

    bool isLocalJob = (bool) atoi(argv[10]);
    std::cout << ">>> Is a local job (0 is False): " << isLocalJob << std::endl;

    TStopwatch time;
    time.Start();
    ROOT::EnableImplicitMT(); // Tell ROOT we want to go parallel - only compatible if Range is NOT used

    TH1::SetDefaultSumw2(kTRUE); // turn on Sumw2 by default

    //*****************************************//
    // Prepare input skim and DNN n-tuples
    //*****************************************//
    // Get the input TTree
    // (need to use this syntax, as opposed to TFile fInput(sampleDir_.c_str());
    // for opening a file, because it's remote)

    std::string treeToGet;
    bool doMuTau = (channel == Helper::mt);
    bool doETau = (channel == Helper::et);
    bool doEMu = (channel == Helper::em);
    if (doMuTau) { treeToGet = "mutau/event_tree"; }
    if (doETau) { treeToGet = "etau/event_tree"; }
    if (doEMu) { treeToGet = "emu/event_tree";  }

    // Prepare the TChains. Use this C-style approach to adding all the lines in the input file.
    TChain *ch = new TChain(treeToGet.c_str());
    std::ifstream file(sampleListDir, std::ifstream::in);
    if (file.is_open()) {
        std::string line;
        while (std::getline(file, line)) {
            std::string pathToFile = line;
            if (!isLocalJob) {
              // If batch job, assume job has copied the file to run on to the worker node
              pathToFile = line.substr(line.find_last_of("/") + 1);
            }
            std::cout << pathToFile << "\n" << std::endl;

            TFile *fIn = TFile::Open(pathToFile.c_str());
            if (fIn == 0) {
                std::cout << "[ERROR:] histogramming.cxx: File not found, exiting: " << line << std::endl;
                return 1;
            }
            TTree *tIn = (TTree*) fIn->Get(treeToGet.c_str());
            if (tIn == 0) {
                std::cout << "[ERROR:] histogramming.cxx: TTree " << treeToGet << " not found, could be expected if the input file has no events in this channel. Continuing..." << std::endl;
            }
            else {
                int result = ch->Add(pathToFile.c_str());
                if (result == 0) {
                    std::cout << "[ERROR:] histogramming.cxx: Failed to find TTree " << treeToGet << " in " << pathToFile << std::endl;
                    return 1;
                }
                std::cout << "Added TTree " << treeToGet << " from " << pathToFile << " which was taken from " << line << std::endl;
            }
        }
    }

    ROOT::RDataFrame df(*ch);

    const auto numEvents = *df.Count();
    std::cout << ">>> Number of events in input file: " << numEvents << std::endl;
    // hRuns and hEvents histogram
    TFile *fInfo = TFile::Open(sampleInfoDir.c_str());
    TH1F *hEvents = (TH1F*)fInfo->Get("hEvents");
    TH1F *hRuns   = (TH1F*)fInfo->Get("hRuns");
    if ((hEvents == 0) || (hRuns == 0)) {
        std::cout << "[ERROR:] hEvents or hRuns histogram NOT FOUND in " << sampleInfoDir << ", EXITING" << std::endl;
        return 1;
    }

    //*****************************************//
    // Load necessary workspaces and data
    //*****************************************//
    bool hasDNN = false;
    LUNA::histoConfig_t hConfig(process, doShifts, hasDNN, channel);
    hConfig.setYear(year);

    std::cout << ">>> isMC: " << hConfig.isMC() << ", isEmbedded: " << hConfig.isEmbedded() << ", isData: " << hConfig.isData() << ", channel: (0: mutau, 1: etau, 2: emu) " << hConfig.channel() << std::endl;

    //*****************************************//
    // Analyze
    //*****************************************//
    // If we want to test a different single/cross-trigger definition, and re-define the trigger decisions, set this flag
    hConfig.setRecomputeTrigger(false);

    auto df_channel = df.Filter([channel](int this_file_channel) { return (this_file_channel == channel); }, {"channel"});
    auto df_init = InitializeDataframe(df_channel, hConfig);

    // Each function needs to be flexible to handle all channels. Book all variations that affect the energy scale before doing any filtering
    auto df_energyScales_0 = DoElectronEnergyScaleShifts(df_init, hConfig);
    auto df_energyScales_1 = GetMuonEnergyScaleShifts(df_energyScales_0, hConfig);
    auto df_energyScales_2 = GetTauEnergyScaleShifts(df_energyScales_1, hConfig);
    auto df_energyScales_3 = OptionalRecomputeTriggerDecision(df_energyScales_2, hConfig);
    auto df_energyScales_4 = GetJERShifts(df_energyScales_3, hConfig);
    auto df_filter_tt = FilterTauTauTriggerDecisions(df_energyScales_4, hConfig);
    auto df_pass = FilterBTagJetCounting(df_filter_tt, hConfig);
    // by now, have leading tautau pair's shifts and trigger decisions

    // Calculate weights and calculate number of b-tag jets
    auto df_2 = GetTauIDShifts(df_pass, hConfig);
    auto df_3 = GetEFakingTauhWeightShifts(df_2, hConfig);
    auto df_4 = GetMuFakingTauhWeightShifts(df_3, hConfig);
    auto df_5 = GetBTagEfficiencyWeightShifts(df_4, hConfig);
    auto df_6 = GetRecoilAndUnclusteredEnergyShifts(df_5, hConfig);
    auto df_7 = GetZPtReweighingShifts(df_6, hConfig);
    auto df_8 = GetTopPtReweighingShifts(df_7, hConfig);

    auto df_9 = GetEmbeddedTauTrackingEfficiencyShifts(df_8, hConfig);
    auto df_10 = PrepareBranchesFilterMCOverlapWithEmbedded(df_9, hConfig);

    auto df_11 = GetJetFakeBackgroundScaleFactorShifts(df_10, hConfig);
    auto df_12 = GetJetFakeBackgroundCrossTriggerShifts(df_11, hConfig);

    auto df_13 = GetTauTauTriggerEfficiencyShifts(df_12, hConfig);
    auto df_14 = GetTauIDwpShifts(df_13, hConfig);

    auto df_15 = GetQCDMultijetBackgroundScaleFactorShifts(df_14, hConfig);
    auto df_16 = GetECALPrefiring_2017_2016(df_15, hConfig);
    auto df_beforeWeight = df_16;

    std::string weightdef_mt = "weight_initial_nominal \
                                      * weight_muonIdIso_nominal \
                                      * weight_tauideff_VSe_nominal * weight_tauideff_VSmu_nominal * weight_tauID_nominal \
                                      * weight_mt_trgsf_nominal \
                                      * weight_emb_tautracking_nominal * weight_m_sel_id_ratio_1 * weight_m_sel_id_ratio_2 * weight_m_sel_trg_ratio \
                                      * weight_btagEff_nominal \
                                      * weight_l1prefiring_nominal \
                                      * weight_zPt_nominal * weight_topPt_nominal * weight_DYJets_nominal * weight_WJets_nominal * weight_pu_nominal \
                                      * weight_ff_nominal * weight_crosstrg_fakefactor_nominal";
    std::string weightdef_et = "weight_initial_nominal \
                                      * weight_eleIdIso_nominal \
                                      * weight_tauideff_VSe_nominal * weight_tauideff_VSmu_nominal * weight_tauID_nominal \
                                      * weight_et_trgsf_nominal \
                                      * weight_emb_tautracking_nominal * weight_m_sel_id_ratio_1 * weight_m_sel_id_ratio_2 * weight_m_sel_trg_ratio \
                                      * weight_tauidWP_nominal \
                                      * weight_btagEff_nominal \
                                      * weight_l1prefiring_nominal \
                                      * weight_zPt_nominal * weight_topPt_nominal * weight_DYJets_nominal * weight_WJets_nominal *  weight_pu_nominal \
                                      * weight_ff_nominal * weight_crosstrg_fakefactor_nominal";
    std::string weightdef_em = "weight_initial_nominal \
                                  * weight_eleIdIso_nominal \
                                  * weight_muonIdIso_nominal \
                                  * weight_m_sel_id_ratio_1 * weight_m_sel_id_ratio_2 * weight_m_sel_trg_ratio \
                                  * weight_em_trgsf_nominal \
                                  * weight_btagEff_nominal \
                                  * weight_l1prefiring_nominal \
                                  * weight_zPt_nominal * weight_topPt_nominal * weight_DYJets_nominal * weight_WJets_nominal *  weight_pu_nominal \
                                  * weight_osss_nominal * weight_correction_nominal * weight_closure_nominal";

    std::cout << "[histogramming.cxx]: weightdef_mt is " << weightdef_mt << ", "
              << "weightdef_et is " << weightdef_et << ", "
              << "weightdef_em is " << weightdef_em << std::endl;

    // Capture these into the dataframe
    std::string branch_name_weight_nominal_passIso;
    std::string branch_name_weight_nominal_antiIso;
    std::string suffix_antiIso;

    if (doMuTau) {
        branch_name_weight_nominal_passIso = "weight_mt_passIso_nominal";
        branch_name_weight_nominal_antiIso = "weight_mt_antiIso_nominal";
        suffix_antiIso = "_antiIso";
    }
    else if (doETau) {
        branch_name_weight_nominal_passIso = "weight_et_passIso_nominal";
        branch_name_weight_nominal_antiIso = "weight_et_antiIso_nominal";
        suffix_antiIso = "_antiIso";
    }
    else {
        branch_name_weight_nominal_passIso = "weight_em_os_nominal";
        branch_name_weight_nominal_antiIso = "weight_em_ss_nominal";
        suffix_antiIso = "_ss";
    }

    auto df_general = df_beforeWeight.Define("weight_mt_passIso_nominal", weightdef_mt)
                                     .Define("weight_mt_antiIso_nominal", weightdef_mt)
                                     .Define("weight_et_passIso_nominal", weightdef_et)
                                     .Define("weight_et_antiIso_nominal", weightdef_et)
                                     .Define("weight_em_os_nominal", weightdef_em)
                                     .Define("weight_em_ss_nominal", weightdef_em);

    // Output file and folder (if different)
    std::string shortHand = "mt";
    if (doETau) shortHand = "et";
    if (doEMu) shortHand = "em";
    // Replace "channel" in the output file name with mt, et, or em
    outputFile.replace(outputFile.find("channel"), std::string("channel").length(), shortHand);
    TFile *fOut = new TFile(outputFile.c_str(), "RECREATE");
    fOut->cd();
    if (doMuTau) fOut->mkdir("mutau");
    if (doETau)  fOut->mkdir("etau");
    if (doEMu)   fOut->mkdir("emu");

    /*
     * List of possible categories. Get the category name (used to initialize the CutExperimental objects).
     */
    std::vector<std::vector<std::string>> categories;
    if (doEMu) {
        categories = { {"inclusive", "true"},  // dummy cut
          {"lowMassSR",    "(m_btautau_vis_nominal < 65) && (mtMET_1_nominal < 40) &&  (mtMET_2_nominal < 40) && (D_zeta_nominal > -30)"},
          {"mediumMassSR", "(m_btautau_vis_nominal >= 65) && (m_btautau_vis_nominal <= 80) && (mtMET_1_nominal < 40) && (mtMET_2_nominal < 40) && (D_zeta_nominal > -30)"},
          {"highMassSR",   "(m_btautau_vis_nominal >= 80) && (m_btautau_vis_nominal <= 95) && (mtMET_1_nominal < 40) && (mtMET_2_nominal < 40) && (D_zeta_nominal > -30)"},
          {"highMassCR",    "(m_btautau_vis_nominal > 95) && (mtMET_1_nominal < 40) && (mtMET_2_nominal < 40) && (D_zeta_nominal > -30)"}};
    }
    else if (doETau) {
        categories = { {"inclusive", "true"},  // dummy cut
          {"lowMassSR",    "(m_btautau_vis_nominal < 80) && (mtMET_1_nominal < 40) &&  (mtMET_2_nominal < 60)"},
          {"mediumMassSR", "(m_btautau_vis_nominal >= 80) && (m_btautau_vis_nominal <= 100) && (mtMET_1_nominal < 50) && (mtMET_2_nominal < 60)"},
          {"highMassSR",   "(m_btautau_vis_nominal >= 100) && (m_btautau_vis_nominal <= 120) && (mtMET_1_nominal < 50) && (mtMET_2_nominal < 60)"},
          {"highMassCR",    "(m_btautau_vis_nominal > 120) && (mtMET_1_nominal < 40) && (mtMET_2_nominal < 60)"}};
    }
    else if (doMuTau) {
        categories = { {"inclusive", "true"},  // dummy cut
          {"lowMassSR",    "(m_btautau_vis_nominal < 75) && (mtMET_1_nominal < 40) &&  (mtMET_2_nominal < 60)"},
          {"mediumMassSR", "(m_btautau_vis_nominal >= 75) && (m_btautau_vis_nominal <= 95) && (mtMET_1_nominal < 50) && (mtMET_2_nominal < 60) && (D_zeta_nominal < 0)"},
          {"highMassSR",   "(m_btautau_vis_nominal >= 95) && (m_btautau_vis_nominal <= 115) && (mtMET_1_nominal < 50) && (mtMET_2_nominal < 60)"},
          {"highMassCR",    "(m_btautau_vis_nominal > 115) && (mtMET_1_nominal < 40) && (mtMET_2_nominal < 60)"}};
    }
    // Apply category cut
    assert(nCategory < (int) categories.size());
    std::string catName = categories[nCategory][0];
    std::string catCut = categories[nCategory][1];

    ROOT::RDF::RNode dfCategory = df_general.Filter([](float met) { return (met >= 0) || (met < 0); }, {"met_nominal"}, "Dummy cut to get met_nominal in cutflow so met systematics appear in Vary")
                                            .Filter(catCut, "histogramming.cxx: Select category: " + catName);

    // initialize (empty initializer not accepted)
    ROOT::RDF::RNode dfFinal_passIso = dfCategory;
    ROOT::RDF::RNode dfFinal_antiIso = dfCategory;
    ROOT::RDF::RNode dfEmbedContamination = dfCategory;

    // Sort by channels
    if (doMuTau) {
        dfFinal_passIso = dfCategory.Filter("(byVVVLooseDeepVSjet_2 > 0) && (byMediumDeepVSjet_2 > 0)", "main: mutau: Passes iso").Filter("pass_final_embed_overlap_removal", "main: Remove overlap with Embedded in signal region");
        dfFinal_antiIso = dfCategory.Filter("(byVVVLooseDeepVSjet_2 > 0) && (byMediumDeepVSjet_2 == 0)", "main: mutau: Anti iso").Filter("pass_final_embed_overlap_removal", "main: Remove overlap with Embedded in anti-isolated region");

        dfEmbedContamination = dfCategory.Filter("(byVVVLooseDeepVSjet_2 > 0) && (byMediumDeepVSjet_2 > 0)", "main: mutau: [Embed contamination:] Passes iso")
                                         .Filter("!(pass_final_embed_overlap_removal)", "main: mutau: [Embed contamination:] Select overlap contamination with Embedded in signal region");
    }
    else if (doETau) {
        dfFinal_passIso = dfCategory.Filter("(byVVVLooseDeepVSjet_2 > 0) && (byMediumDeepVSjet_2 > 0)", "main: etau: Passes iso").Filter("pass_final_embed_overlap_removal", "main: Remove overlap with Embedded in signal region");
        dfFinal_antiIso = dfCategory.Filter("(byVVVLooseDeepVSjet_2 > 0) && (byMediumDeepVSjet_2 == 0)", "main: etau: Anti iso").Filter("pass_final_embed_overlap_removal", "main: Remove overlap with Embedded in anti-isolated region");

        dfEmbedContamination = dfCategory.Filter("(byVVVLooseDeepVSjet_2 > 0) && (byMediumDeepVSjet_2 > 0)", "main: etau: [Embed contamination:] Passes iso")
                                         .Filter("!(pass_final_embed_overlap_removal)", "main: etau: [Embed contamination:] Select overlap contamination with Embedded in signal region");
    }
    else {
        dfFinal_passIso = dfCategory.Filter("(q_1 * q_2) < 0", "main: emu: Opposite-sign");
        dfFinal_antiIso = dfCategory.Filter("(q_1 * q_2) > 0", "main: emu: Same-sign");

        dfEmbedContamination = dfCategory.Filter("(q_1 * q_2) < 0", "main: emu: Opposite-sign")
                                         .Filter("!(pass_final_embed_overlap_removal)", "main: etau: [Embed contamination:] Select overlap contamination with Embedded in signal region");
    }


    /*
     * Declare the variables and the shifts that we want to write out for each one.
     * Note that this happens independently of the RDataFrame objects, CutExperimental is a custom-defined class.
     */

    bool hasSeparateEmbedShift = true;


    std::map<std::string, std::vector<CutExperimental>> mapVars, mapWeights;
    std::vector<std::string> vars = {"m_tt_nominal", "pt_tt_nominal", "eta_tt_nominal", "phi_tt_nominal",
                                    "m_vis_nominal", "pt_vis_nominal",
                                    "m_btautau_vis_nominal", "pt_btautau_vis_nominal",
                                    "met_nominal", "metphi_nominal",
                                    "pt_1_nominal", "eta_1", "phi_1", "m_1_nominal", "pt_2_nominal", "eta_2", "phi_2", "m_2_nominal",
                                    "D_zeta_nominal", "mtMET_1_nominal", "mtMET_2_nominal",
                                    "bpt_deepflavour_1", "beta_deepflavour_1", "bphi_deepflavour_1", "bm_deepflavour_1",
                                    "bpt_deepflavour_2", "beta_deepflavour_2", "bphi_deepflavour_2", "bm_deepflavour_2",
                                    "metcov00", "metcov01", "metcov10", "metcov11"};
    if (!doAllVariables) {
        vars = {"m_tt_nominal"};
    }

    // Loosely grouping variables by whether they are shifted by various shifts
    for (auto variable: vars) {
        // First strip the variable name of any _nominal, because the output histogram should be called m_tt not m_tt_nominal
        std::string variableOutputName = variable;
        std::string ext = "_nominal";
        size_t pos = variableOutputName.find(ext);
        if (pos != std::string::npos) {
            variableOutputName.erase(pos, ext.length());
        }
        mapVars[variable] = {CutExperimental(catName, variableOutputName, {"mutau"}, {"muES_eta0to1p2", "muES_eta1p2to2p1", "muES_eta2p1to2p4"}, hasSeparateEmbedShift),
                            CutExperimental(catName, variableOutputName, {"emu"}, {"muES_eta0to1p2", "muES_eta1p2to2p1", "muES_eta2p1to2p4"}, hasSeparateEmbedShift),
                            CutExperimental(catName, variableOutputName, {"etau", "emu"}, {"eleES_bar", "eleES_end"}, hasSeparateEmbedShift),
                            CutExperimental(catName, variableOutputName, {"mutau", "etau"}, {"tauES_dm0", "tauES_dm1", "tauES_dm10", "tauES_dm11"}, hasSeparateEmbedShift)
        };
        if (hConfig.isMC()) {
            // electron faking tau ES
            mapVars[variable].push_back(CutExperimental(catName, variableOutputName, {"mutau", "etau"}, {"eleTES_dm0", "eleTES_dm1"}));
            // muon faking tau ES
            mapVars[variable].push_back(CutExperimental(catName, variableOutputName, {"mutau", "etau"}, {"muTES_dm0", "muTES_dm1"}));
            mapVars[variable].push_back(CutExperimental(catName, variableOutputName, {"mutau", "etau", "emu"},  {"JER", "JetAbsolute", "JetAbsoluteyear", "JetBBEC1", "JetBBEC1year", "JetEC2", "JetEC2year", "JetFlavorQCD", "JetHF", "JetHFyear", "JetRelativeBal", "JetRelativeSample"}, hasSeparateEmbedShift));

            // Recoil (response + resolution) corrections, otherwise UES
            mapVars[variable].push_back(CutExperimental(catName, variableOutputName,  {"mutau", "etau", "emu"}, {"met_0j_resolution", "met_1j_resolution", "met_gt1j_resolution", "met_0j_response",  "met_1j_response",  "met_gt1j_response"}));


            // UES
            mapVars[variable].push_back(CutExperimental(catName, variableOutputName,  {"mutau", "etau", "emu"}, {"UES"}));
        }
    }

    // Weights
    // MC ONLY
    if (hConfig.isMC()) {
        // First argument to CutExperimental must be the branch weight without the "_nominal" suffix
        mapWeights["weight_btagEff_nominal"]             = {CutExperimental(catName, "weight_btagEff",       {"mutau", "etau", "emu"}, {"btagsf_hf", "btagsf_lf", "btagsf_hfstats1", "btagsf_hfstats2", "btagsf_lfstats1", "btagsf_lfstats2", "btagsf_cferr1", "btagsf_cferr2",
                                                                                                                                    "JER", "JetAbsolute", "JetAbsoluteyear", "JetBBEC1", "JetBBEC1year", "JetEC2", "JetEC2year", "JetFlavorQCD", "JetHF", "JetHFyear", "JetRelativeBal", "JetRelativeSample"})};

        mapWeights["weight_tauideff_VSe_nominal"]        = {CutExperimental(catName, "weight_tauideff_VSe",  {"mutau", "etau"}, {"tauideff_VSe_bar", "tauideff_VSe_end"})};
        mapWeights["weight_tauideff_VSmu_nominal"]       = {CutExperimental(catName, "weight_tauideff_VSmu", {"mutau", "etau"}, {"tauideff_VSmu_eta0to0p4", "tauideff_VSmu_eta0p4to0p8", "tauideff_VSmu_eta0p8to1p2", "tauideff_VSmu_eta1p2to1p7", "tauideff_VSmu_eta1p7to2p3"})};
        mapWeights["weight_ff_nominal"]                  = {CutExperimental(catName, "weight_ff",            {"mutau", "etau"}, {"jetFR_pt0to25", "jetFR_pt25to30", "jetFR_pt30to35", "jetFR_pt35to40", "jetFR_pt40to50", "jetFR_pt50to60", "jetFR_pt60to80", "jetFR_pt80to100", "jetFR_pt100to120", "jetFR_pt120to150", "jetFR_ptgt150",
                                                            "muTES_dm0", "muTES_dm1", "eleTES_dm0", "eleTES_dm1"})};
        mapWeights["weight_crosstrg_fakefactor_nominal"] = {CutExperimental(catName, "weight_crosstrg_fakefactor", {"mutau", "etau"}, {"crosstrg_fakefactor"})};
        // DY sample-specific shifts: Zs pT reweighing
        mapWeights["weight_zPt_nominal"] = {CutExperimental(catName, "weight_zPt", {"mutau", "etau", "emu"}, {"Zpt"})};
        // Top sample-specific shifts: pT reweighing
        mapWeights["weight_topPt_nominal"] = {CutExperimental(catName, "weight_topPt", {"mutau", "etau", "emu"}, {"toppt"})};
    }
    // MC OR EMBED
    if (hConfig.isMC() || hConfig.isEmbedded()) {
        // First argument to CutExperimental must be the branch weight without the "_nominal" suffix
        mapWeights["weight_tauID_nominal"]   = {CutExperimental(catName, "weight_tauID", {"mutau", "etau"}, {"tauideff_pt20to25", "tauideff_pt25to30", "tauideff_pt30to35", "tauideff_pt35to40", "tauideff_pt40to500", "tauideff_pt500to1000", "tauideff_ptgt1000"}, hasSeparateEmbedShift)};
        mapWeights["weight_muonIdIso_nominal"] = {CutExperimental(catName, "weight_muonIdIso", {"mutau", "emu"}, {"muES_eta0to1p2", "muES_eta1p2to2p1", "muES_eta2p1to2p4"})};
        mapWeights["weight_eleIdIso_nominal"] = {CutExperimental(catName, "weight_eleId", {"etau", "emu"}, {"eleES_bar", "eleES_end"})};
        mapWeights["weight_mt_trgsf_nominal"] = {CutExperimental(catName, "weight_mt_trgsf", {"mutau"}, {"trgeff_single_mt", "trgeff_cross_mt", "muES_eta0to1p2", "muES_eta1p2to2p1", "muES_eta2p1to2p4"}, hasSeparateEmbedShift)};
        mapWeights["weight_et_trgsf_nominal"]   = {CutExperimental(catName, "weight_et_trgsf", {"etau"}, {"trgeff_single_et", "trgeff_cross_et", "eleES_bar", "eleES_end"}, hasSeparateEmbedShift)};
        mapWeights["weight_em_trgsf_nominal"]   = {CutExperimental(catName, "weight_em_trgsf", {"emu"},  {"trgeff_Mu8E23_em", "trgeff_Mu23E12_em", "trgeff_both_em", "muES_eta0to1p2",  "muES_eta1p2to2p1", "muES_eta2p1to2p4", "eleES_bar", "eleES_end"}, hasSeparateEmbedShift)};

        mapWeights["weight_tauidWP_nominal"] = {CutExperimental(catName, "weight_tauidWP", {"etau"}, {"tauidWP_et"})};
        mapWeights["weight_l1prefiring_nominal"] = {CutExperimental(catName, "weight_l1prefiring", {"mutau", "etau", "emu"}, {"prefiring"})};
        if (!hConfig.isSignal()) {
            mapWeights["weight_correction_nominal"] = {CutExperimental(catName, "weight_correction", {"emu"}, {"SScorrection", "SSboth2D", "eleES_bar", "eleES_end"})};
            mapWeights["weight_closure_nominal"]    = {CutExperimental(catName, "weight_closure",    {"emu"}, {"SSclosure",    "SSboth2D", "eleES_bar", "eleES_end"})};
            mapWeights["weight_osss_nominal"]       = {CutExperimental(catName, "weight_osss",       {"emu"}, {"osss"})};
        }
    }
    // EMBED ONLY
    if (hConfig.isEmbedded()) {
        mapWeights["weight_emb_tautracking"] = {CutExperimental(catName, "weight_emb_tautracking", {"mutau", "etau"}, {"tautrack_dm0dm10", "tautrack_dm1", "tautrack_dm11"}, hasSeparateEmbedShift)};
    }

    /*
     * Apply category cuts
     */
    std::string folderName = "mutau/" + catName;
    if (doMuTau) folderName = "mutau/" + catName;
    if (doETau)  folderName = "etau/" + catName;
    if (doEMu)   folderName = "emu/" + catName;
    fOut->mkdir(folderName.c_str());
    fOut->cd(folderName.c_str());

    // For each variable we want to plot,
    // 1. Call Histo1D to make the smart pointer
    // 2. Call ROOT::RDF::Experimental::VariationsFor on the smart pointer, to make a RResultMap
    // 3. For each available systematic (first looping over the generic name, and then :down or :up),
    //    read into the RResultMap and write out the correct output histogram name
    //    which is [variable_name]_{_CMS_ or _CMS_EMB_}_[systematicGenericName][year]{Up or Down}

    std::string prefix = "_CMS_";
    std::string yearString = hConfig.yearStr();


    for (auto mapVarIter : mapVars) {
        std::string varName = mapVarIter.first;
        std::string outName = varName; // initialize
        std::string toRemove = "_nominal";  // remove this substring from the variable name when writing the output histogram, e.g. want pt_2_CMS_muES etc. not pt_2_nominal_CMS_muES etc.
        std::size_t ind = outName.find(toRemove);
        if (ind != std::string::npos) {
            outName.erase(ind, toRemove.length());
        }

        std::cout << ">>> main: Making histogram of " << varName << std::endl;

        // var.second is a std::vector<CutExperimental>, e.g. this loops over "muES_eta0to1p2", "muES_eta1p2to2p1", etc.
        CutExperimental firstCut = mapVarIter.second[0];

        // Note: for the Embedded contamination contribution, we only write the nominal histogram
        ROOT::RDF::RResultPtr<TH1D> histoPtr_embedContamination = dfEmbedContamination.Histo1D(ROOT::RDF::TH1DModel(outName.c_str(), outName.c_str(), firstCut.nBins(), firstCut.xMin(), firstCut.xMax()), varName,
            branch_name_weight_nominal_passIso);
        ROOT::RDF::RResultPtr<TH1D> histoPtr_passIso = dfFinal_passIso.Histo1D(ROOT::RDF::TH1DModel(outName.c_str(), outName.c_str(), firstCut.nBins(), firstCut.xMin(), firstCut.xMax()), varName,
            branch_name_weight_nominal_passIso);
        ROOT::RDF::RResultPtr<TH1D> histoPtr_antiIso = dfFinal_antiIso.Histo1D(ROOT::RDF::TH1DModel(outName.c_str(), outName.c_str(), firstCut.nBins(), firstCut.xMin(), firstCut.xMax()), varName,
            branch_name_weight_nominal_antiIso);
        auto map_passIso = ROOT::RDF::Experimental::VariationsFor(histoPtr_passIso);
        auto map_antiIso = ROOT::RDF::Experimental::VariationsFor(histoPtr_antiIso);


        std::cout << ">>> main: writing nominal histograms" << std::endl;

        map_passIso["nominal"].Write((hConfig.name() + "_" +  outName).c_str());
        map_antiIso["nominal"].Write((hConfig.name() + "_" +  outName + suffix_antiIso).c_str());
        histoPtr_embedContamination.GetValue().Write((hConfig.name() + "_" + outName + "_contamination_estimation").c_str());

        // If data, escape this loop before trying to shift weights
        if (hConfig.isData()) {
            std::cout << "main: Data, do not shift weights" << std::endl;
            continue;
        }
        if (!hConfig.doShifts()) {
            std::cout << "main: Do not do shifts, exit loop" << std::endl;
            continue;
        }

        for (auto cut : mapVarIter.second) {
            // var.second is a std::vector<CutExperimental>, e.g. this loops over "muES_eta0to1p2", "muES_eta1p2to2p1", etc.

            // If the sample is Embedded, and the shift is marked as being binned by EMB as well, use a special prefix
            if (hConfig.isEmbedded() && cut.hasSeparateEmbedShift()) { prefix = "_CMS_EMB_"; } else { prefix = "_CMS_"; }

            // Write shifted variables
            for (auto sysName : cut.shifts()) {
                // If the shift is marked as affecting this channel
                if ((doMuTau && cut.hasMuTau()) || (doETau && cut.hasETau()) || (doEMu && cut.hasEMu())) {
                    std::string upName = (hConfig.name() + "_" +  outName + prefix + sysName + "_" + yearString + "Up");
                    std::string downName = (hConfig.name() + "_" +  outName + prefix + sysName + "_" + yearString + "Down");
                    std::string upName_antiIso, downName_antiIso;

                    if (doMuTau || doETau) {
                        upName_antiIso = (hConfig.name() + "_" +  outName + prefix + sysName + "_" + yearString + "Up" + suffix_antiIso);
                        downName_antiIso = (hConfig.name() + "_" +  outName  + prefix + sysName + "_" + yearString + "Down" + suffix_antiIso);
                    }
                    else if (doEMu) {  // slightly different syntax for emu: placement of "_ss" is in the middle of the histogram
                        upName_antiIso = (hConfig.name() + "_" +  outName + suffix_antiIso + prefix + sysName + "_" + yearString + "Up");
                        downName_antiIso = (hConfig.name() + "_" +  outName + suffix_antiIso + prefix + sysName + "_" + yearString + "Down");
                    }
                    map_passIso[sysName + ":up"].Write(upName.c_str());
                    map_passIso[sysName + ":down"].Write(downName.c_str());
                    map_antiIso[sysName + ":up"].Write(upName_antiIso.c_str());
                    map_antiIso[sysName + ":down"].Write(downName_antiIso.c_str());
                }
            }
        }

        for (auto mapWeightIter : mapWeights) { // e.g. mapWeights["weight_btagEff_nominal"]
            std::string weightName = mapWeightIter.first;
            for (auto wCut : mapWeightIter.second ) { // for each CutExperimental() in the {},
                for (auto wSysName : wCut.shifts()) { // loop over {"btagsf_hf", "btagsf_lf", "btagsf_hfstats1", "btagsf_hfstats2", ...}
                    // Read into map with tauideff_pt20to25, but write the variable m_tt

                    // If the sample is Embedded, and the shift is marked as being binned by EMB as well, use a special prefix
                    if (hConfig.isEmbedded() && wCut.hasSeparateEmbedShift()) { prefix = "_CMS_EMB_"; } else { prefix = "_CMS_";  }

                    // e.g. weight_ff is not in weight_mt_passIso: there will be no shift
                    bool hasWeightShift = ((doMuTau && wCut.hasMuTau() && Helper::containsSubstring(weightdef_mt, weightName)) ||
                                            (doETau  && wCut.hasETau()  && Helper::containsSubstring(weightdef_et, weightName)) ||
                                            (doEMu   && wCut.hasEMu()   && Helper::containsSubstring(weightdef_em, weightName)));
                    std::string upName = (hConfig.name() + "_" +  outName + prefix + wSysName + "_" + yearString + "Up");
                    std::string downName = (hConfig.name() + "_" +  outName + prefix + wSysName + "_" + yearString + "Down");
                    std::string upName_antiIso, downName_antiIso;

                    if (doMuTau || doETau) {
                        upName_antiIso = (hConfig.name() + "_" +  outName + prefix + wSysName + "_" + yearString + "Up" + suffix_antiIso);
                        downName_antiIso = (hConfig.name() + "_" +  outName + prefix + wSysName + "_" + yearString + "Down" + suffix_antiIso);
                    }
                    else if (doEMu) {
                        upName_antiIso = (hConfig.name() + "_" +  outName + suffix_antiIso + prefix + wSysName + "_" + yearString + "Up");
                        downName_antiIso = (hConfig.name() + "_" +  outName + suffix_antiIso + prefix + wSysName + "_" + yearString + "Down");
                    }

                    if (hasWeightShift) {
                        map_passIso[wSysName + ":up"].Write(upName.c_str());
                        map_passIso[wSysName + ":down"].Write(downName.c_str());
                        map_antiIso[wSysName + ":up"].Write(upName_antiIso.c_str());
                        map_antiIso[wSysName + ":down"].Write(downName_antiIso.c_str());
                    }
                }
            }
        }
    }


    // Make and print reports
    dfFinal_passIso.Report()->Print();
    dfFinal_antiIso.Report()->Print();

    fOut->Close();

    time.Stop();
    time.Print();

    return 0;

}
