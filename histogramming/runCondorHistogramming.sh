#!/bin/bash

#--------------------------------------------------------
# Usage:
#   bash runCondorHistogramming.sh
#
# Submits histogramming jobs for one channel, either for the final fit variable in all categories, or all variables in only the inclusive category
#
# In principle possible to submit jobs for all variables in all categories, but the runtime might be high.
#--------------------------------------------------------
# mutau: 0, etau: 1, emu: 2
CHANNEL=0
# path to .csv with list of samples to run on, formatted as: top-level directory to n-tuple (typically in EOS), sample name, config path, cross-section, and # of events in DAS
SAMPLES="hists-2018-benchmark-SVFit.csv"
# optional description that only affects the job name in Condor
DESCRIPTION="2018-mt"
# do systematic shifts: 1 (true) or 0 (false)
DO_SHIFTS=1
# do categories: 1 (true: make histograms with inclusive (no category cut), signal regions, and control regions as defined in histogramming.cxx) or 0 (false, only inclusive histograms)
DO_CATEGORIES=0
# do all variables: 1 (true: do all control variables as specified in histogramming.cxx) or 0 (false: only the single variable specified in histogramming.cxx, typically m_vis)
DO_ALL_VARIABLES=1
# submit: true (create .sub submit file and output directories, and submit with condor_submit) or false (only create .sub file and output directories, but do not submit)
SUBMIT=true


# set automatically, no need to change: paths to this directory and the EOS output directory
WORKING_DIR=${CMSSW_BASE}"/src/lunaFramework/histogramming"
EOS_BASE_DIR="/eos/cms/store/group/phys_susy/AN-24-166/${USER}/condorHistogramming"

#--------------------------------------------------------
# Check if we used bash
#--------------------------------------------------------
if [[ "$0" != "$BASH_SOURCE" ]]; then
    echo ">>> ${BASH_SOURCE[0]}: Error: Script must be run with bash"
    return
fi

#--------------------------------------------------------
# Check if environments are set
#--------------------------------------------------------
if [[ -z ${CMSSW_BASE} ]]; then
    echo ">>> ${BASH_SOURCE[0]}: CMSSW environment is not set! Make sure to do cmsenv"
    exit 1
fi
if [[ -z ${VIRTUAL_ENV} ]]; then
    echo ">>> ${BASH_SOURCE[0]}: Virtual environment is not set! Make sure to source venv-LUNA/bin/activate"
    exit 1
fi

#--------------------------------------------------------
# Get the voms-proxy-info certificate
#--------------------------------------------------------
export MYPROXYPATH="$(voms-proxy-info -path)"

# Use regex to extract the first letter of the username. ^ is the beginning of the string. \w means any alphanumeric character from the Latin alphabet. ( ) captures the variable
echo ${USER}
re="^(\w)"
# Bash's =~ operator matches the left hand side to the regex on the right hand side
[[ ${USER} =~ ${re} ]]
# The results of the regex can be extracted from the bash variable BASH_REMATCH. Index [0] returns all of the matches.
USER_FIRST_LETTER=${BASH_REMATCH[0]}


if [[ -f ${MYPROXYPATH} ]]; then
    echo ">>> runCondorHistogramming.sh: Copying proxy from ${MYPROXYPATH} to /afs/cern.ch/user/${USER_FIRST_LETTER}/${USER}/private/x509up_file"
    cp ${MYPROXYPATH} /afs/cern.ch/user/${USER_FIRST_LETTER}/${USER}/private/x509up_file
else
    echo ">>> ${BASH_SOURCE[0]}: [ERROR]: x509 proxy not found on this machine, make sure voms-proxy-init was run, exiting"
    exit 1
fi

#--------------------------------------------------------
# First check if the input files exists
#--------------------------------------------------------
while IFS=, read -r SAMPLE_INFO PROCESS YEAR JOBSIZE
do
    {
    # If stored in eos,
    if [[ ${SAMPLE_INFO} =~ ^/eos/ ]]; then
        if [[ ! $(ls -f ${SAMPLE_INFO}) ]]; then
            echo "${BASH_SOURCE[0]}: ERROR: File ${SAMPLE_INFO} is not there, aborting. (Make sure there are no extra newlines in ${SAMPLES}.)"
            exit 1
        fi
    else
    # Else use gfal-ls for remote files
        if [[ ! $(gfal-ls ${SAMPLE_INFO}) ]]; then
            echo "${BASH_SOURCE[0]}: ERROR: tried to gfal-ls file ${SAMPLE_INFO} but it is is not there, aborting. (Make sure there are no extra newlines in ${SAMPLES}.)"
            exit 1
         fi
    fi
    }
done < ${SAMPLES}

while IFS=, read -r SAMPLE_INFO SAMPLE YEAR JOBSIZE
do
    {
    # Make a temporary .list of the input n-tuples
    INPUT_LIST="paths/${YEAR}/inputs_${SAMPLE}.list"
    echo ${INPUT_LIST}
    rm -r ${INPUT_LIST}
    THISPATH="paths/${YEAR}/inputs_${SAMPLE}_batch_*.list"
    echo ${THISPATH}
    rm ${THISPATH}
    mkdir -p "paths/${YEAR}/"
    # If the files are in /eos/, a simple "find" will work
    if [[ ${SAMPLE_INFO} =~ ^/eos/ ]]; then
        find $(dirname ${SAMPLE_INFO})/${SAMPLE}/*postprocessed*.root -type f > ${INPUT_LIST}
        cat ${INPUT_LIST}
    fi
}
done < ${SAMPLES}

#--------------------------------------------------------
# Then run the histogramming
#--------------------------------------------------------
start=$(date +%s)

DATETIME="$(date +"%Y-%m-%d-%Hh%Mm")"

# Create the Condor submit files
ABSOLUTE_PATH_TO_EXECUTABLE="${EOS_PREFIX}${WORKING_DIR}/histogramming"

while IFS=, read -r SAMPLE_INFO SAMPLE YEAR JOBSIZE
do
{
    EOS_PREFIX="root://eosuser.cern.ch/"

    INPUT_LIST="paths/${YEAR}/inputs_${SAMPLE}.list"

    JOBLOG_DIR="logs/${DATETIME}"

    EOS_DIR="${EOS_PREFIX}${EOS_BASE_DIR}/${DATETIME}/${SAMPLE}/"

    # make the local submit directory (useful for checking the .sub file)
    # The EOS output directories are only created at the end of this .sh script if the execute flag is False.
    mkdir -p ${JOBLOG_DIR}/${SAMPLE}

    # Copy the template
    subfile="${JOBLOG_DIR}/h_${SAMPLE}.sub"
    cp jobTemplate.sub ${subfile}

    echo ">>> Writing to ${subfile}"

    # Have longer jobs if doing all variables
    # if [[ ${DO_ALL_VARIABLES} == 1 ]]; then
    #     JOB_FLAVOUR='"longlunch"'
    #     # Also override the JOBSIZE defined in the .csv file. Always do fewer files
    #     JOBSIZE=2
    #     if [[ ${SAMPLE} == *"TTTo"* ]]; then
    #         JOBSIZE=1
    #     fi
    # else
    #     JOB_FLAVOUR='"espresso"'
    # fi
    # TODO: TEMP: TEST
    JOB_FLAVOUR='"espresso"'

    # If doing categories, queue over the number of categories as well
    if [[ ${DO_CATEGORIES} == 1 ]]; then
        MAX_CATEGORY=5
    elif [[ ${DO_CATEGORIES} == 0 ]]; then
        MAX_CATEGORY=1
    fi

    # In order to have all samples run in under 20 minutes, the larger datasets need to be split into smaller jobs. The .csv file defines the default behaviour for
    # m_vis datacards only. COUNTER_BATCH=1 means there is 1 batch
    COUNTER_BATCH=0
    COUNTER=0
    while IFS="\n", read -r FILE
    do
    {
        # Do these in batches of JOBSIZE (e.g. 50) each
        if (( COUNTER >= JOBSIZE )); then
            COUNTER_BATCH=$((COUNTER_BATCH+1))
            COUNTER=0
        fi
        SUBLIST="paths/${YEAR}/inputs_${SAMPLE}_batch_${COUNTER_BATCH}.list"
        echo ${FILE} >> ${SUBLIST}
        COUNTER=$((COUNTER+1))
    }
    done < ${INPUT_LIST}

    OUTPUT_HIST="hist_${SAMPLE}_channel_cat_\$(category)_batch_\$(batchNum).root"
    SUBLIST_NAME="paths/${YEAR}/inputs_${SAMPLE}_batch_\$(batchNum).list"

    # Edit parameters in the Condor .sub file
    sed -i "s|(job_flavour)|${JOB_FLAVOUR}|g" ${subfile}

    sed -i "s|(sample)|${SAMPLE}|g" ${subfile}
    sed -i "s|(year)|${YEAR}|g" ${subfile}
    SUBLIST_REL_PATH="$(basename -- ${SUBLIST_NAME})"
    sed -i "s|(relative_path_to_list_of_input_files)|${SUBLIST_REL_PATH}|g" ${subfile}
    SAMPLE_INFO_REL_PATH="$(basename -- ${SAMPLE_INFO})"
    sed -i "s|(relative_path_to_sample_info)|${SAMPLE_INFO_REL_PATH}|g" ${subfile}
    sed -i "s|(channel)|${CHANNEL}|g" ${subfile}
    sed -i "s|(timestamp)|${DATETIME}|" ${subfile}
    sed -i "s|(output_hist)|${OUTPUT_HIST}|g" ${subfile}
    sed -i "s|(do_shifts)|${DO_SHIFTS}|g" ${subfile}
    sed -i "s|(do_all_variables)|${DO_ALL_VARIABLES}|g" ${subfile}
    IS_LOCAL=0  # is_local will be 0 (false) for Condor jobs
    sed -i "s|(isLocal)|${IS_LOCAL}|g" ${subfile}

    # Paths to ship with the job
    sed -i "s|(absolute_path_to_executable)|${ABSOLUTE_PATH_TO_EXECUTABLE}|g" ${subfile}
    sed -i "s|(absolute_path_to_list_of_input_files)|${WORKING_DIR}/${SUBLIST_NAME}|g" ${subfile}
    sed -i "s|(absolute_path_to_sample_info)|${EOS_PREFIX}${SAMPLE_INFO}|g" ${subfile}
    sed -i "s|(absolute_path_to_sample_path_eos)|${EOS_PREFIX}${SAMPLE_PATH_EOS}|g" ${subfile}
    sed -i "s|(eos_dir)|${EOS_DIR}|g" ${subfile}

    # Instead of just queue 10, we need to vary two variables, the category number (0, or 0 through 4) and the batch number (0, or 0 through N)
    MATRIX=""
    # Case 1: only one batch (== 0), only do inclusive category (== 0)
    if [[ ${COUNTER_BATCH} == 0 ]] && [[ ${MAX_CATEGORY} == 1 ]] ; then
        MATRIX="  0, 0"
    # Case 2: only one batch (==0), but do all categories (0 to 4)
    elif [[ ${COUNTER_BATCH} == 0 ]] && [[ ${MAX_CATEGORY} -gt 1 ]] ; then
        for ((i = 0; i < MAX_CATEGORY; i++)); do
            NEXT="  $i, 0"
            MATRIX+=${NEXT}
            # Only add a newline if we are not at the end
            if (( $i < (${MAX_CATEGORY} - 1))); then
                MATRIX+=$'\n'
            fi
        done
    # Case 3: multiple batches (0 through N), all categories (0 to 4)
    else
        for ((i = 0; i < MAX_CATEGORY; i++)); do
            for ((j = 0; j <= COUNTER_BATCH; j++)); do
                NEXT="  $i, $j"
                MATRIX+=${NEXT}
                # Only add a newline if we are not at the end
                if !( ((i == (MAX_CATEGORY - 1))) && ((j == (COUNTER_BATCH))) ); then
                    MATRIX+=$'\n'
                fi
            done
        done
    fi

    # Write the queue command to the submit file
    echo "queue category, batchNum from (" >> ${subfile}
    echo "${MATRIX}" >> ${subfile}
    echo ")" >> ${subfile}
    echo ${subfile}

    mysubmitcommand="condor_submit -batch-name histo_${DESCRIPTION}_${DATETIME}_${SAMPLE} -file ${subfile} priority=100 "
    if [[ ${SUBMIT} == true ]]; then
        mkdir -p ${EOS_BASE_DIR}/${DATETIME}/${SAMPLE}
        mkdir -p ${EOS_BASE_DIR}/${DATETIME}/${SAMPLE}/logs/${DATETIME}/${SAMPLE}
        eval ${mysubmitcommand}
    else
        echo ">>> Submit was false, did not submit"
    fi
}
done < ${SAMPLES}
wait
