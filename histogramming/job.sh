#!/bin/bash

#-----------------------------------------------
# This is the bash script called by job.sub
#-----------------------------------------------

export X509_USER_PROXY=$1
voms-proxy-info -all

export SAMPLE_INFO=$2
export INPUT_LIST=$3
export SAMPLE=$4
export YEAR=$5
export OUTPUT_HIST=$6
export CHANNEL=$7
export CATEGORY_NUMBER=$8
export DO_SHIFTS="${9}"
export DO_ALL_VARIABLES="${10}"
export IS_LOCAL="${11}"

# Stage-in: eos cp the input files to the worker node
export EOS_MGM_URL=root://eosuser.cern.ch
while IFS= read -r line || [ -n "${line}" ]; do
    COMMAND="eos cp ${line} ."
    eval ${COMMAND}
done < ${INPUT_LIST}

COMMAND="./histogramming ${SAMPLE_INFO} ${INPUT_LIST} ${SAMPLE} ${YEAR} ${OUTPUT_HIST} ${CHANNEL} ${CATEGORY_NUMBER} ${DO_SHIFTS} ${DO_ALL_VARIABLES} ${IS_LOCAL}"
echo "Attempting to execute: ${COMMAND}"
eval ${COMMAND}
wait

# clean up the files we copied in
echo "Do file cleanup"
while IFS= read -r line || [ -n "${line}" ]; do
    this_file="$(basename ${line})"
    COMMAND="rm ${this_file}"
    eval ${COMMAND}
done < ${INPUT_LIST}
