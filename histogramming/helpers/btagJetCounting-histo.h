// Helper functions for b-tag jet counting with jet systematics.

#ifndef BTAG_JET_COUNTING_HISTO_H_INCL
#define BTAG_JET_COUNTING_HISTO_H_INCL


#include "histoConfig_class.h"


/******************************************************************************/


template <typename T>
auto FilterBTagJetCounting(T &df, LUNA::histoConfig_t &hConfig){

  (void) hConfig;

  return df.Filter("nbtag20_nominal >= 1", "btagJetCounting-histo.h: Select events with >= 1 b-tag jet");

}

/******************************************************************************/

#endif
