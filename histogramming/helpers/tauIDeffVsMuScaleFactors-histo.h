// emuFakingTauhScaleFactors-experimental.h

#include "helperFunctions.h"
#include "histoConfig_class.h"

#ifndef TAU_ID_EFF_VS_MU_SCALE_FACTORS_HISTO_H_INCL
#define TAU_ID_EFF_VS_MU_SCALE_FACTORS_HISTO_H_INCL

/*******************************************************************/

/*
 * Weights and shifts for DeepTau2017v2p1 muons faking tauh
 */

template <typename T>
auto GetMuFakingTauhWeightShifts(T &df, LUNA::histoConfig_t &hConfig) {

  if (hConfig.isMC()) {
    if (hConfig.doShifts()) {
      return df.Vary("weight_tauideff_VSmu_nominal", "ROOT::RVecF{ weight_tauideff_VSmu_eta0to0p4_Up,  weight_tauideff_VSmu_eta0to0p4_Down}",    {"up", "down"}, "tauideff_VSmu_eta0to0p4")
               .Vary("weight_tauideff_VSmu_nominal", "ROOT::RVecF{ weight_tauideff_VSmu_eta0p4to0p8_Up, weight_tauideff_VSmu_eta0p4to0p8_Down}", {"up", "down"}, "tauideff_VSmu_eta0p4to0p8")
               .Vary("weight_tauideff_VSmu_nominal", "ROOT::RVecF{ weight_tauideff_VSmu_eta0p8to1p2_Up, weight_tauideff_VSmu_eta0p8to1p2_Down}", {"up", "down"}, "tauideff_VSmu_eta0p8to1p2")
               .Vary("weight_tauideff_VSmu_nominal", "ROOT::RVecF{ weight_tauideff_VSmu_eta1p2to1p7_Up, weight_tauideff_VSmu_eta1p2to1p7_Down}", {"up", "down"}, "tauideff_VSmu_eta1p2to1p7")
               .Vary("weight_tauideff_VSmu_nominal", "ROOT::RVecF{ weight_tauideff_VSmu_eta1p7to2p3_Up, weight_tauideff_VSmu_eta1p7to2p3_Down}", {"up", "down"}, "tauideff_VSmu_eta1p7to2p3");
    }
  }
  return df;
}

/*******************************************************************/

#endif
