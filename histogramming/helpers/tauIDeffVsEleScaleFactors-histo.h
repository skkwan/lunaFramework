// emuFakingTauhScaleFactors-experimental.h

#include "helperFunctions.h"
#include "histoConfig_class.h"

#ifndef TAU_ID_EFF_VS_E_SCALE_FACTORS_HISTO_H_INCL
#define TAU_ID_EFF_VS_E_SCALE_FACTORS_HISTO_H_INCL

/*******************************************************************/

/*
 * barrel + endcap for vs. e
 */

template <typename T>
auto GetEFakingTauhWeightShifts(T &df, LUNA::histoConfig_t &hConfig) {

  // MC: any year
  if (hConfig.isMC()) {
    if (hConfig.doShifts()) {
      // shifts of electron faking tauh weight itself. No dependence on energy so no shifts due to lepton energy scales
      return df.Vary("weight_tauideff_VSe_nominal", "ROOT::RVecF{weight_tauideff_VSe_bar_Up, weight_tauideff_VSe_bar_Down}", {"up", "down"}, "tauideff_VSe_bar")
               .Vary("weight_tauideff_VSe_nominal", "ROOT::RVecF{weight_tauideff_VSe_end_Up, weight_tauideff_VSe_end_Down}", {"up", "down"}, "tauideff_VSe_end");
    }
  }
  return df;
}

/*******************************************************************/

#endif
