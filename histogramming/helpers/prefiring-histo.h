// Prefiring weights

#ifndef PREFIRING_HISTO_H_INCL
#define PREFIRING_HISTO_H_INCL

#include "computePhysicsVariables.h"
#include "fileIO.h"
#include "helperFunctions.h"
#include "histoConfig_class.h"


/******************************************************************************/

/*
 * During 2016-2017, a gradual shift in the timing of the ECAL L1 trigger inputs occurred in the forward encap region, |eta| > 2.4
 */

template <typename T>
auto GetECALPrefiring_2017_2016(T &df, LUNA::histoConfig_t &hConfig) {
    if (hConfig.year() == 2018) {
        std::cout << ">>> prefiring-histo.h: Redefine weight_l1prefiring_nominal to 1.0f for 2018." << std::endl;
        return df.Redefine("weight_l1prefiring_nominal", []() { return 1.0f; })
                 .Vary("weight_l1prefiring_nominal", "ROOT::RVecF{1.0f, 1.0f}", {"up", "down"}, "prefiring");
    }

    return df.Vary("weight_l1prefiring_nominal", "ROOT::RVecF{weight_l1prefiring_up, weight_l1prefiring_down}", {"up", "down"}, "prefiring");

}

/******************************************************************************/

#endif
