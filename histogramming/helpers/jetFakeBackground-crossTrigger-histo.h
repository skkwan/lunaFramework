#ifndef JET_FAKE_BACKGROUND_CROSS_TRIGGER_HISTO_H_INCL
#define JET_FAKE_BACKGROUND_CROSS_TRIGGER_HISTO_H_INCL

#include "histoConfig_class.h"

/*******************************************************************/

/*
 * This systematic is not binned
 * https://github.com/hftsoi/aabbtt_finalselection/blob/master/selection_et_allyears.cc#L2335-L2348
 *
 * Interestingly, does not need/use the central value in this helper function.
 */

ROOT::RVecF getCrossTriggerFakeFactor_variations(int passCrossTrigger,
                                                 float weight_crosstrg_fakefactor_crosstrg_fakefactor_Up,
                                                 float weight_crosstrg_fakefactor_crosstrg_fakefactor_Down) {
    // This function is called in .Vary(), which will evaluate the up and down shifts of weight_crosstrg_fakefactor.
    if (passCrossTrigger) {
        return ROOT::RVecF{weight_crosstrg_fakefactor_crosstrg_fakefactor_Up, weight_crosstrg_fakefactor_crosstrg_fakefactor_Down};
    }
    // If an event is accepted in the single trigger, do not vary weight_crosstrg_factor, which was already 1.0.
    return ROOT::RVecF{1.0f, 1.0f};
}

/*******************************************************************/

/*
 * For mutau and etau: this is applied to anti-isolated events in the jet->tauh fake background:
 * For events that were selected with the cross-trigger, tighter pT cuts were applied which results in lower yields,
 * so we need to scale these up. The nominal value is 1.5
 */
template <typename T>
auto GetJetFakeBackgroundCrossTriggerShifts(T &df, LUNA::histoConfig_t &hConfig) {

    if (hConfig.doShifts() && hConfig.doMuTau()) {
        return df.Vary("weight_crosstrg_fakefactor_nominal", getCrossTriggerFakeFactor_variations, {"passCrossTrigger_mt_nominal",
                                                            "weight_crosstrg_fakefactor_crosstrg_fakefactor_Up", "weight_crosstrg_fakefactor_crosstrg_fakefactor_Down"},
                                                            {"up", "down"}, "crosstrg_fakefactor");
    }
    else if (hConfig.doShifts() && hConfig.doETau()) {
        return df.Vary("weight_crosstrg_fakefactor_nominal", getCrossTriggerFakeFactor_variations, {"passCrossTrigger_et_nominal",
                                                            "weight_crosstrg_fakefactor_crosstrg_fakefactor_Up", "weight_crosstrg_fakefactor_crosstrg_fakefactor_Down"},
                                                            {"up", "down"}, "crosstrg_fakefactor");
    }
    return df;
}

/*******************************************************************/

#endif
