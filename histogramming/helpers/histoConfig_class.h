#ifndef HISTO_CONFIG_CLASS_H_INCL
#define HISTO_CONFIG_CLASS_H_INCL

#include <string>

#include "helperFunctions.h"

/*************************************************************************/

namespace LUNA {

    class histoConfig_t {

        private:
            int histoChannel;

            std::string sampleName;
            bool isSampleData;
            bool isSampleEmbedded;
            bool isSampleMC;
            int sampleYear;
            std::string runTag;

            // Do systematics at all?
            bool doSys;
            bool hasDNN;

            bool redefineTrigger;

            bool isSampleTTbar;
            bool isSampleDY;

            bool isSampleDataEmbedMuTau;
            bool isSampleDataEmbedETau;
            bool isSampleDataEmbedEMu;

            // Do systematics for this dataset?
            bool doSys_muonES;
            bool doSys_tauES;
            bool doSys_tauhID;
            bool doSys_eleFakeTauh;
            bool doSys_muFakeTauh;
            bool doSys_JER;
            bool doSys_btagEff;
            bool doSys_recoilMET;
            bool doSys_unclusteredMET;


        public:

            // Are the JER systematic jet pT and MET branches in the file?
            bool hasJERsysInputs; // has "MET_T1Smear_pt_jerUp"
            bool hasJERyearsysInputs; // has "MET_T1Smear_pt_jesAbsolute_2018Up"
            bool hasMetUESInputs; // has "MET_T1Smear_pt_unclustEnUp"


            // To initialize a histogram config
            histoConfig_t(std::string processName, bool doDefaultShifts, bool hasDNNattached, int thisChannel);

            // Setters
            void setYear(int year);
            void setDoSystematics(bool performSys);
            void setRecomputeTrigger(bool redoTrigger);

            // Getters
            int channel();
            std::string name();
            bool doShifts();
            bool isData();
            bool isEmbedded();
            bool isMC();
            int year();
            std::string yearStr();

            // Is the sample data or embed that is specific to a final state
            bool isMuTauOnly();
            bool isETauOnly();
            bool isEMuOnly();

            // For this instance of histogramming, are we doing this channel? (argument set at command-line level to the executable)
            bool doMuTau();
            bool doETau();
            bool doEMu();

            bool runSystematics();
            bool isSignal();
            bool isTTbar();
            bool isDY();
            bool isWJets();
            bool isHiggsRecoil();
            bool doRecoil();
            int recoilBosonPdgId();
            bool recomputeTrigger();

            // Recoil type: 0 for none, 1 for W recoil boson, 2 for Z/H recoil boson (electrically neutral)
            int recoilType();

            // Is MC and not a Higgs sample (for contamination to Embedded from non-DYjets samples)
            bool isMCnonHiggs();

            std::string getRunTag();
    };

}
/*************************************************************************/

#endif
