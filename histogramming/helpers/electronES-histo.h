// Electron energy scale and shifts

#ifndef ELECTRON_ES_HISTO_H_INCL
#define ELECTRON_ES_HISTO_H_INCL

#include "helperFunctions.h"
#include "histoConfig_class.h"

/******************************************************************************/

// output is going to be {{pt_1_esUp, m_1_esUp}, {pt_1_esDown, m_1_esDown}}
ROOT::RVecF getElectronEnergyScale_variations(std::string sysToDo, float eleEta,
                                                    float val_nominal, float val_up, float val_down) {

    ROOT::RVecF variations = {val_nominal, val_nominal};

    if      ((abs(eleEta) > 0.0)   && (abs(eleEta) < 1.479) && (sysToDo == "eleES_bar")) { variations = {val_up, val_down}; }
    else if ((abs(eleEta) > 1.479) && (abs(eleEta) < 2.4)   && (sysToDo == "eleES_end")) { variations = {val_up, val_down}; }

    return variations;
}

// some day maybe there will be a workaround for error: deduced class type 'RVec' in function return type
ROOT::RVecI getElectronEnergyScale_variations_int(std::string sysToDo, float eleEta,
                                                  int val_nominal, int val_up, int val_down) {

    ROOT::RVecI variations = {val_nominal, val_nominal};

    if      ((abs(eleEta) > 0.0)   && (abs(eleEta) < 1.479) && (sysToDo == "eleES_bar")) { variations = {val_up, val_down}; }
    else if ((abs(eleEta) > 1.479) && (abs(eleEta) < 2.4)   && (sysToDo == "eleES_end")) { variations = {val_up, val_down}; }

    return variations;
}


/******************************************************************************/
// Pre-UL: https://twiki.cern.ch/twiki/bin/view/CMS/EgammaRunIIRecommendations#HEEPV7_0

// UL: https://twiki.cern.ch/twiki/bin/view/CMS/EgammaUL2016To2018 : might just use UL 2018 values
template <typename T>
auto DoElectronEnergyScaleShifts(T &df, LUNA::histoConfig_t &hConfig) {

    // etau
    if (hConfig.isMC() || hConfig.isEmbedded()) {
        if (hConfig.doShifts()) {
            if (hConfig.doETau() || hConfig.doEMu()) {
            // List all variables affected by the electron energy scale shifts
            return df.Define("eleES_bar", [=]() { return (std::string) "eleES_bar"; })
                     .Define("eleES_end", [=]() { return (std::string) "eleES_bar"; })
                     // Barrel shifts
                     .Vary("pt_1_nominal", getElectronEnergyScale_variations, {"eleES_bar", "eta_1", "pt_1_nominal", "pt_1_es1Up", "pt_1_es1Down"}, {"up", "down"}, "eleES_bar")
                     .Vary("m_1_nominal",  getElectronEnergyScale_variations, {"eleES_bar", "eta_1", "m_1_nominal", "m_1_es1Up", "m_1_es1Down"}, {"up", "down"}, "eleES_bar")
                     .Vary("passSingleTrigger_et_nominal",  getElectronEnergyScale_variations_int, {"eleES_bar", "eta_1", "passSingleTrigger_et_nominal", "passSingleTrigger_et_es1Up", "passSingleTrigger_et_es1Down"}, {"up", "down"}, "eleES_bar")
                     .Vary("passCrossTrigger_et_nominal",   getElectronEnergyScale_variations_int, {"eleES_bar", "eta_1", "passCrossTrigger_et_nominal", "passCrossTrigger_et_es1Up", "passCrossTrigger_et_es1Down"}, {"up", "down"}, "eleES_bar")
                     .Vary("met_nominal",   getElectronEnergyScale_variations, {"eleES_bar", "eta_1", "met_nominal", "met_es1Up", "met_es1Down"}, {"up", "down"}, "eleES_bar")
                     .Vary("metphi_nominal",   getElectronEnergyScale_variations, {"eleES_bar", "eta_1", "metphi_nominal", "metphi_es1Up", "metphi_es1Down"}, {"up", "down"}, "eleES_bar")
                     // SVFit mass if available
                     .Vary("m_tt_nominal",   getElectronEnergyScale_variations, {"eleES_bar", "eta_1", "m_tt_nominal", "m_tt_es1Up", "m_tt_es1Down"}, {"up", "down"}, "eleES_bar")
                     .Vary("pt_tt_nominal",   getElectronEnergyScale_variations, {"eleES_bar", "eta_1", "pt_tt_nominal", "pt_tt_es1Up", "pt_tt_es1Down"}, {"up", "down"}, "eleES_bar")
                     // Visible mass
                     .Vary("m_vis_nominal",   getElectronEnergyScale_variations, {"eleES_bar", "eta_1", "m_vis_nominal", "m_vis_es1Up", "m_vis_es1Down"}, {"up", "down"}, "eleES_bar")
                     .Vary("pt_vis_nominal",   getElectronEnergyScale_variations, {"eleES_bar", "eta_1", "pt_vis_nominal", "pt_vis_es1Up", "pt_vis_es1Down"}, {"up", "down"}, "eleES_bar")
                     // Barrel shifts of cut-based categorization variables. Note only mtMET_1 is shifted because the electron is leg #1 in etau and emu channels.
                     .Vary("m_btautau_vis_nominal",  getElectronEnergyScale_variations, {"eleES_bar", "eta_1", "m_btautau_vis_nominal",  "m_btautau_vis_es1Up",  "m_btautau_vis_es1Down"}, {"up", "down"}, "eleES_bar")
                     .Vary("pt_btautau_vis_nominal", getElectronEnergyScale_variations, {"eleES_bar", "eta_1", "pt_btautau_vis_nominal", "pt_btautau_vis_es1Up", "pt_btautau_vis_es1Down"}, {"up", "down"}, "eleES_bar")
                     .Vary("D_zeta_nominal",  getElectronEnergyScale_variations, {"eleES_bar", "eta_1", "D_zeta_nominal",  "D_zeta_es1Up",  "D_zeta_es1Down"}, {"up", "down"}, "eleES_bar")
                     .Vary("mtMET_1_nominal",  getElectronEnergyScale_variations, {"eleES_bar", "eta_1", "mtMET_1_nominal",  "mtMET_1_es1Up",  "mtMET_1_es1Down"}, {"up", "down"}, "eleES_bar")
                     // Barrel shifts of weights
                     .Vary("weight_crosstrg_fakefactor_nominal",   getElectronEnergyScale_variations, {"eleES_bar", "eta_1", "weight_crosstrg_fakefactor_nominal", "weight_crosstrg_fakefactor_es1Up", "weight_crosstrg_fakefactor_es1Down"}, {"up", "down"}, "eleES_bar")
                     .Vary("weight_em_trgsf_nominal", getElectronEnergyScale_variations, {"eleES_bar", "eta_1", "weight_em_trgsf_nominal", "weight_em_trgsf_es1Up", "weight_em_trgsf_es1Down"}, {"up", "down"}, "eleES_bar")
                     .Vary("weight_et_trgsf_nominal", getElectronEnergyScale_variations, {"eleES_bar", "eta_1", "weight_et_trgsf_nominal", "weight_et_trgsf_es1Up", "weight_et_trgsf_es1Down"}, {"up", "down"}, "eleES_bar")
                     .Vary("weight_eleIdIso_nominal",    getElectronEnergyScale_variations, {"eleES_bar", "eta_1", "weight_eleIdIso_nominal", "weight_eleIdIso_es1Up", "weight_eleIdIso_es1Down"}, {"up", "down"}, "eleES_bar")
                     .Vary("weight_correction_nominal",   getElectronEnergyScale_variations, {"eleES_bar", "eta_1", "weight_correction_nominal", "weight_correction_es1Up", "weight_correction_es1Down"}, {"up", "down"}, "eleES_bar")
                     .Vary("weight_closure_nominal",   getElectronEnergyScale_variations, {"eleES_bar", "eta_1", "weight_closure_nominal", "weight_closure_es1Up", "weight_closure_es1Down"}, {"up", "down"}, "eleES_bar")

                    // Endcap shifts
                     .Vary("pt_1_nominal", getElectronEnergyScale_variations, {"eleES_end", "eta_1", "pt_1_nominal", "pt_1_es1Up", "pt_1_es1Down"}, {"up", "down"}, "eleES_end")
                     .Vary("m_1_nominal",  getElectronEnergyScale_variations, {"eleES_end", "eta_1", "m_1_nominal", "m_1_es1Up", "m_1_es1Down"}, {"up", "down"}, "eleES_end")
                     .Vary("passSingleTrigger_et_nominal",  getElectronEnergyScale_variations_int, {"eleES_end", "eta_1", "passSingleTrigger_et_nominal", "passSingleTrigger_et_es1Up", "passSingleTrigger_et_es1Down"}, {"up", "down"}, "eleES_end")
                     .Vary("passCrossTrigger_et_nominal",   getElectronEnergyScale_variations_int, {"eleES_end", "eta_1", "passCrossTrigger_et_nominal", "passCrossTrigger_et_es1Up", "passCrossTrigger_et_es1Down"}, {"up", "down"}, "eleES_end")
                     .Vary("met_nominal",   getElectronEnergyScale_variations, {"eleES_end", "eta_1", "met_nominal", "met_es1Up", "met_es1Down"}, {"up", "down"}, "eleES_end")
                     .Vary("metphi_nominal",   getElectronEnergyScale_variations, {"eleES_end", "eta_1", "metphi_nominal", "metphi_es1Up", "metphi_es1Down"}, {"up", "down"}, "eleES_end")
                     // SVFit mass if available
                     .Vary("m_tt_nominal",   getElectronEnergyScale_variations, {"eleES_end", "eta_1", "m_tt_nominal", "m_tt_es1Up", "m_tt_es1Down"}, {"up", "down"}, "eleES_end")
                     .Vary("pt_tt_nominal",   getElectronEnergyScale_variations, {"eleES_end", "eta_1", "pt_tt_nominal", "pt_tt_es1Up", "pt_tt_es1Down"}, {"up", "down"}, "eleES_end")
                     // Visible mass
                     .Vary("m_vis_nominal",   getElectronEnergyScale_variations, {"eleES_end", "eta_1", "m_vis_nominal", "m_vis_es1Up", "m_vis_es1Down"}, {"up", "down"}, "eleES_end")
                     .Vary("pt_vis_nominal",   getElectronEnergyScale_variations, {"eleES_end", "eta_1", "pt_vis_nominal", "pt_vis_es1Up", "pt_vis_es1Down"}, {"up", "down"}, "eleES_end")
                     // Endcap shifts of cut-based categorization variables.
                     .Vary("m_btautau_vis_nominal",  getElectronEnergyScale_variations, {"eleES_end", "eta_1", "m_btautau_vis_nominal",  "m_btautau_vis_es1Up",  "m_btautau_vis_es1Down"}, {"up", "down"}, "eleES_end")
                     .Vary("pt_btautau_vis_nominal", getElectronEnergyScale_variations, {"eleES_end", "eta_1", "pt_btautau_vis_nominal", "pt_btautau_vis_es1Up", "pt_btautau_vis_es1Down"}, {"up", "down"}, "eleES_end")
                     .Vary("D_zeta_nominal",  getElectronEnergyScale_variations, {"eleES_end", "eta_1", "D_zeta_nominal",  "D_zeta_es1Up",  "D_zeta_es1Down"}, {"up", "down"}, "eleES_end")
                     .Vary("mtMET_1_nominal",  getElectronEnergyScale_variations, {"eleES_end", "eta_1", "mtMET_1_nominal",  "mtMET_1_es1Up",  "mtMET_1_es1Down"}, {"up", "down"}, "eleES_end")

                     // Endcap shifts of weights
                     .Vary("weight_crosstrg_fakefactor_nominal",   getElectronEnergyScale_variations, {"eleES_end", "eta_1", "weight_crosstrg_fakefactor_nominal", "weight_crosstrg_fakefactor_es1Up", "weight_crosstrg_fakefactor_es1Down"}, {"up", "down"}, "eleES_end")
                     .Vary("weight_et_trgsf_nominal", getElectronEnergyScale_variations, {"eleES_end", "eta_1", "weight_et_trgsf_nominal", "weight_et_trgsf_es1Up", "weight_et_trgsf_es1Down"}, {"up", "down"}, "eleES_end")
                     .Vary("weight_em_trgsf_nominal", getElectronEnergyScale_variations, {"eleES_end", "eta_1", "weight_em_trgsf_nominal", "weight_em_trgsf_es1Up", "weight_em_trgsf_es1Down"}, {"up", "down"}, "eleES_end")
                     .Vary("weight_eleIdIso_nominal",    getElectronEnergyScale_variations, {"eleES_end", "eta_1", "weight_eleIdIso_nominal", "weight_eleIdIso_es1Up", "weight_eleIdIso_es1Down"}, {"up", "down"}, "eleES_end")
                     .Vary("weight_correction_nominal",   getElectronEnergyScale_variations, {"eleES_end", "eta_1", "weight_correction_nominal", "weight_correction_es1Up", "weight_correction_es1Down"}, {"up", "down"}, "eleES_end")
                     .Vary("weight_closure_nominal",   getElectronEnergyScale_variations, {"eleES_end", "eta_1", "weight_closure_nominal", "weight_closure_es1Up", "weight_closure_es1Down"}, {"up", "down"}, "eleES_end");
            }
        }
    }
    return df;
}

/******************************************************************************/

#endif
