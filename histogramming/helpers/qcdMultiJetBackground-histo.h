#ifndef QCD_MULTIJET_BACKGROUND_HISTO_H_INCL
#define QCD_MULTIJET_BACKGROUND_HISTO_H_INCL

#include "histoConfig_class.h"

/******************************************************************************/

/*
 * Three scale factors necessary to extrapolate the QCD background for the e+mu channel from same-sign events
 * The pointers to the TF1 and TH2Fs are captured into the dataframe in the main function (postprocess-experimental.cxx)
 */

template <typename T>
auto GetQCDMultijetBackgroundScaleFactorShifts(T &df, LUNA::histoConfig_t &hConfig) {

    if (hConfig.doEMu() && !hConfig.isSignal() && hConfig.doShifts()) {
        return df.Vary("weight_correction_nominal", "ROOT::RVecF{weight_correction_SScorrection_Up, weight_correction_SScorrection_Down}", {"up", "down"}, "SScorrection")
                 .Vary("weight_correction_nominal", "ROOT::RVecF{weight_correction_SSboth2D_Up, weight_correction_SSboth2D_Down}", {"up", "down"}, "SSboth2D")
                 .Vary("weight_closure_nominal", "ROOT::RVecF{weight_closure_SSclosure_Up, weight_closure_SSclosure_Down}", {"up", "down"}, "SSclosure")
                 .Vary("weight_closure_nominal", "ROOT::RVecF{weight_closure_SSboth2D_Up, weight_closure_SSboth2D_Down}", {"up", "down"}, "SSboth2D")
                 .Vary("weight_osss_nominal", "ROOT::RVecF{weight_osss_osss_Up, weight_osss_osss_Down}", {"up", "down"}, "osss");
        // n.b. Shifts due to the lepton energy scales of leg 1 and leg 2: already handled in lepton energy scale function
    }
    return df;
}

/******************************************************************************/

#endif
