#ifndef EMBEDDED_TAU_TRACKING_HISTO_H_INCL
#define EMBEDDED_TAU_TRACKING_HISTO_H_INCL

#include "helperFunctions.h"
#include "histoConfig_class.h"

/*******************************************************************/

ROOT::RVecF getEmbedTauTrackingEff_variations(std::string sysToDo, int channel, int tau_dm, unsigned int tau_genMatch,
                                float val_nominal, float val_up, float val_down) {

    ROOT::RVecF variations = {val_nominal, val_nominal};
    if ((channel == Helper::mt || channel == Helper::et) && tau_genMatch == 5) {
        if      ((tau_dm == 0) &&  (sysToDo == "tautrack_dm0dm10"))  { variations = {val_up, val_down}; } // same uncertainty for dm0dm10
        else if ((tau_dm == 1) &&  (sysToDo == "tautrack_dm1"))      { variations = {val_up, val_down}; }
        else if ((tau_dm == 10) && (sysToDo == "tautrack_dm0dm10")) { variations = {val_up, val_down}; }  // same uncertainty for dm0dm10
        else if ((tau_dm == 11) && (sysToDo == "tautrack_dm11"))    { variations = {val_up, val_down}; }
    }
    return variations;

}
/*******************************************************************/

/*
 * In the MiniAOD code it appears only the embedded samples have a tau tracking efficiency
 * https://github.com/hftsoi/aabbtt_finalselection/blob/cefb486856b4c817d20c6b4c49d17eaef115f78e/selection_mt_allyears.cc#L504
 */

template <typename T>
auto GetEmbeddedTauTrackingEfficiencyShifts(T &df, LUNA::histoConfig_t &hConfig){

    if (hConfig.isEmbedded()) {
        if (hConfig.doShifts()) {
            return df.Define("tautrack_dm0dm10", []() { return (std::string) "tautrack_dm0dm10"; })
                     .Define("tautrack_dm1",     []() { return (std::string) "tautrack_dm1";  })
                     .Define("tautrack_dm11",    []() { return (std::string) "tautrack_dm11"; })
                     .Vary("weight_emb_tautracking_nominal", getEmbedTauTrackingEff_variations, {"tautrack_dm0dm10", "channel", "decayMode_2", "gen_match_2", "weight_emb_tautracking_nominal",
                            "weight_emb_tautracking_tautrack_dm0dm10_Up", "weight_emb_tautracking_tautrack_dm0dm10_Down"}, {"up", "down"}, "tautrack_dm0dm10")
                     .Vary("weight_emb_tautracking_nominal", getEmbedTauTrackingEff_variations, {"tautrack_dm1", "channel", "decayMode_2", "gen_match_2", "weight_emb_tautracking_nominal",
                            "weight_emb_tautracking_tautrack_dm1_Up", "weight_emb_tautracking_tautrack_dm1_Down"}, {"up", "down"}, "tautrack_dm1")
                     .Vary("weight_emb_tautracking_nominal", getEmbedTauTrackingEff_variations, {"tautrack_dm11", "channel", "decayMode_2", "gen_match_2", "weight_emb_tautracking_nominal",
                            "weight_emb_tautracking_tautrack_dm11_Up", "weight_emb_tautracking_tautrack_dm11_Down"}, {"up", "down"}, "tautrack_dm11");
        }
    }
    return df;
}


/*******************************************************************/



#endif
