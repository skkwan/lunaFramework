#ifndef RECOIL_AND_UNCLUSTERED_CORRECTIONS_HISTO_H_INCL
#define RECOIL_AND_UNCLUSTERED_CORRECTIONS_HISTO_H_INCL

#include "helperFunctions.h"
#include "histoConfig_class.h"

/*******************************************************************/

/*
 * Same procedure for resolution and response (just pass different branches for the second, third, fourth, fifth arguments).
 */

ROOT::RVecF getRecoil_Variations(float nRecoilJets, float val_nominal, float val_up, float val_down, std::string sysToDo) {
  if (Helper::containsSubstring(sysToDo, "0j") && (nRecoilJets == 0)) {
    return ROOT::RVecF{val_up, val_down};
  }
  else if (Helper::containsSubstring(sysToDo, "1j") && (nRecoilJets == 1)) {
    return ROOT::RVecF{val_up, val_down};
  }
  else if (Helper::containsSubstring(sysToDo, "gt1j") && (nRecoilJets > 1)) {
    return ROOT::RVecF{val_up, val_down};
  }
  else {
    // e.g. We requested the 0j systematic, but the event has 1 recoil jet. Return the nominal met and metphi
    return ROOT::RVecF{val_nominal, val_nominal};
  }
}

/*******************************************************************/

template <typename T>
auto GetRecoilAndUnclusteredEnergyShifts(T &df, LUNA::histoConfig_t &hConfig){

  // Recoil corrections affect met/metphi and therefore SVFit
  // Recoil = response + resolution
  std::cout << "recoilCorrections-experimental.h: hConfig.doRecoil() is " << hConfig.doRecoil() << std::endl;
  if (hConfig.isMC() && hConfig.doShifts() && hConfig.doRecoil()) {

    // Resolution and response, binned by number of recoil jets
    return df.Define("met_0j_resolution",   []() { return (std::string) "0j_resolution"; })
             .Define("met_1j_resolution",   []() { return (std::string) "1j_resolution"; })
             .Define("met_gt1j_resolution", []() { return (std::string) "gt1j_resolution"; })
             .Define("met_0j_response",   []() { return (std::string) "0j_response"; })
             .Define("met_1j_response",   []() { return (std::string) "1j_response"; })
             .Define("met_gt1j_response", []() { return (std::string) "gt1j_response"; })

             // met and metphi Resolution
             // order of arguments: see helper function above
             .Vary("met_nominal", getRecoil_Variations, {"nRecoilJets", "met_nominal", "met_resolutionUp", "met_resolutionDown", "met_0j_resolution"}, {"up", "down"},   "met_0j_resolution")
             .Vary("met_nominal", getRecoil_Variations, {"nRecoilJets", "met_nominal", "met_resolutionUp", "met_resolutionDown", "met_1j_resolution"}, {"up", "down"},   "met_1j_resolution")
             .Vary("met_nominal", getRecoil_Variations, {"nRecoilJets", "met_nominal", "met_resolutionUp", "met_resolutionDown", "met_gt1j_resolution"}, {"up", "down"}, "met_gt1j_resolution")
             .Vary("metphi_nominal", getRecoil_Variations, {"nRecoilJets", "metphi_nominal", "metphi_resolutionUp", "metphi_resolutionDown", "met_0j_resolution"}, {"up", "down"},   "met_0j_resolution")
             .Vary("metphi_nominal", getRecoil_Variations, {"nRecoilJets", "metphi_nominal", "metphi_resolutionUp", "metphi_resolutionDown", "met_1j_resolution"}, {"up", "down"},   "met_1j_resolution")
             .Vary("metphi_nominal", getRecoil_Variations, {"nRecoilJets", "metphi_nominal", "metphi_resolutionUp", "metphi_resolutionDown", "met_gt1j_resolution"}, {"up", "down"}, "met_gt1j_resolution")

             // met and metphi Response: same logic, just read different up/down branches
             .Vary("met_nominal", getRecoil_Variations, {"nRecoilJets", "met_nominal", "met_responseUp", "met_responseDown", "met_0j_response"}, {"up", "down"},   "met_0j_response")
             .Vary("met_nominal", getRecoil_Variations, {"nRecoilJets", "met_nominal", "met_responseUp", "met_responseDown", "met_1j_response"}, {"up", "down"},   "met_1j_response")
             .Vary("met_nominal", getRecoil_Variations, {"nRecoilJets", "met_nominal", "met_responseUp", "met_responseDown", "met_gt1j_response"}, {"up", "down"}, "met_gt1j_response")
             .Vary("metphi_nominal", getRecoil_Variations, {"nRecoilJets", "metphi_nominal", "metphi_responseUp", "metphi_responseDown", "met_0j_response"}, {"up", "down"},   "met_0j_response")
             .Vary("metphi_nominal", getRecoil_Variations, {"nRecoilJets", "metphi_nominal", "metphi_responseUp", "metphi_responseDown", "met_1j_response"}, {"up", "down"},   "met_1j_response")
             .Vary("metphi_nominal", getRecoil_Variations, {"nRecoilJets", "metphi_nominal", "metphi_responseUp", "metphi_responseDown", "met_gt1j_response"}, {"up", "down"}, "met_gt1j_response")

             // Cut-based event categorization variables which are shifted due to MET
             .Vary("mtMET_1_nominal", getRecoil_Variations, {"nRecoilJets", "mtMET_1_nominal", "mtMET_1_resolutionUp", "mtMET_1_resolutionDown", "met_0j_resolution"}, {"up", "down"},   "met_0j_resolution")
             .Vary("mtMET_1_nominal", getRecoil_Variations, {"nRecoilJets", "mtMET_1_nominal", "mtMET_1_resolutionUp", "mtMET_1_resolutionDown", "met_1j_resolution"}, {"up", "down"},   "met_1j_resolution")
             .Vary("mtMET_1_nominal", getRecoil_Variations, {"nRecoilJets", "mtMET_1_nominal", "mtMET_1_resolutionUp", "mtMET_1_resolutionDown", "met_gt1j_resolution"}, {"up", "down"}, "met_gt1j_resolution")
             .Vary("mtMET_1_nominal", getRecoil_Variations, {"nRecoilJets", "mtMET_1_nominal", "mtMET_1_responseUp",   "mtMET_1_responseDown",   "met_0j_response"}, {"up", "down"},   "met_0j_response")
             .Vary("mtMET_1_nominal", getRecoil_Variations, {"nRecoilJets", "mtMET_1_nominal", "mtMET_1_responseUp",   "mtMET_1_responseDown",   "met_1j_response"}, {"up", "down"},   "met_1j_response")
             .Vary("mtMET_1_nominal", getRecoil_Variations, {"nRecoilJets", "mtMET_1_nominal", "mtMET_1_responseUp",   "mtMET_1_responseDown",   "met_gt1j_response"}, {"up", "down"}, "met_gt1j_response")

             .Vary("mtMET_2_nominal", getRecoil_Variations, {"nRecoilJets", "mtMET_2_nominal", "mtMET_2_resolutionUp", "mtMET_2_resolutionDown", "met_0j_resolution"}, {"up", "down"},   "met_0j_resolution")
             .Vary("mtMET_2_nominal", getRecoil_Variations, {"nRecoilJets", "mtMET_2_nominal", "mtMET_2_resolutionUp", "mtMET_2_resolutionDown", "met_1j_resolution"}, {"up", "down"},   "met_1j_resolution")
             .Vary("mtMET_2_nominal", getRecoil_Variations, {"nRecoilJets", "mtMET_2_nominal", "mtMET_2_resolutionUp", "mtMET_2_resolutionDown", "met_gt1j_resolution"}, {"up", "down"}, "met_gt1j_resolution")
             .Vary("mtMET_2_nominal", getRecoil_Variations, {"nRecoilJets", "mtMET_2_nominal", "mtMET_2_responseUp",   "mtMET_2_responseDown",   "met_0j_response"}, {"up", "down"},   "met_0j_response")
             .Vary("mtMET_2_nominal", getRecoil_Variations, {"nRecoilJets", "mtMET_2_nominal", "mtMET_2_responseUp",   "mtMET_2_responseDown",   "met_1j_response"}, {"up", "down"},   "met_1j_response")
             .Vary("mtMET_2_nominal", getRecoil_Variations, {"nRecoilJets", "mtMET_2_nominal", "mtMET_2_responseUp",   "mtMET_2_responseDown",   "met_gt1j_response"}, {"up", "down"}, "met_gt1j_response")

             .Vary("D_zeta_nominal", getRecoil_Variations, {"nRecoilJets", "D_zeta_nominal", "D_zeta_resolutionUp", "D_zeta_resolutionDown", "met_0j_resolution"}, {"up", "down"},   "met_0j_resolution")
             .Vary("D_zeta_nominal", getRecoil_Variations, {"nRecoilJets", "D_zeta_nominal", "D_zeta_resolutionUp", "D_zeta_resolutionDown", "met_1j_resolution"}, {"up", "down"},   "met_1j_resolution")
             .Vary("D_zeta_nominal", getRecoil_Variations, {"nRecoilJets", "D_zeta_nominal", "D_zeta_resolutionUp", "D_zeta_resolutionDown", "met_gt1j_resolution"}, {"up", "down"}, "met_gt1j_resolution")
             .Vary("D_zeta_nominal", getRecoil_Variations, {"nRecoilJets", "D_zeta_nominal", "D_zeta_responseUp",   "D_zeta_responseDown",   "met_0j_response"}, {"up", "down"},   "met_0j_response")
             .Vary("D_zeta_nominal", getRecoil_Variations, {"nRecoilJets", "D_zeta_nominal", "D_zeta_responseUp",   "D_zeta_responseDown",   "met_1j_response"}, {"up", "down"},   "met_1j_response")
             .Vary("D_zeta_nominal", getRecoil_Variations, {"nRecoilJets", "D_zeta_nominal", "D_zeta_responseUp",   "D_zeta_responseDown",   "met_gt1j_response"}, {"up", "down"}, "met_gt1j_response")

             // SVFit mass: note the difference in capitalization (_Response instead of _response) for m_tt
             .Vary("m_tt_nominal", getRecoil_Variations, {"nRecoilJets", "m_tt_nominal", "m_tt_ResolutionUp", "m_tt_ResolutionDown", "met_0j_resolution"}, {"up", "down"}, "met_0j_resolution")
             .Vary("m_tt_nominal", getRecoil_Variations, {"nRecoilJets", "m_tt_nominal", "m_tt_ResolutionUp", "m_tt_ResolutionDown", "met_1j_resolution"}, {"up", "down"}, "met_1j_resolution")
             .Vary("m_tt_nominal", getRecoil_Variations, {"nRecoilJets", "m_tt_nominal", "m_tt_ResolutionUp", "m_tt_ResolutionDown", "met_gt1j_resolution"}, {"up", "down"}, "met_gt1j_resolution")
             .Vary("m_tt_nominal", getRecoil_Variations, {"nRecoilJets", "m_tt_nominal", "m_tt_ResponseUp", "m_tt_ResponseDown", "met_0j_response"}, {"up", "down"}, "met_0j_response")
             .Vary("m_tt_nominal", getRecoil_Variations, {"nRecoilJets", "m_tt_nominal", "m_tt_ResponseUp", "m_tt_ResponseDown", "met_1j_response"}, {"up", "down"}, "met_1j_response")
             .Vary("m_tt_nominal", getRecoil_Variations, {"nRecoilJets", "m_tt_nominal", "m_tt_ResponseUp", "m_tt_ResponseDown", "met_gt1j_response"}, {"up", "down"}, "met_gt1j_response")

             // Call .Vary() for "UES" but just provide met_nominal, so we can call .Vary() for "UES" without RDataFrame throwing errors
             .Vary("met_nominal", "ROOT::RVecF{met_nominal, met_nominal}", {"up", "down"}, "UES");
  }
  else if (hConfig.isMC() && hConfig.doShifts()) {
    // MC samples with no recoil corrections: use UES up/down shapes. This is quite simple so just use the JIT-compiled expressions
    return df.Vary({"met_nominal", "metphi_nominal"}, "ROOT::RVec<ROOT::RVecF>{{met_UESUp, met_UESDown}, {metphi_UESUp, metphi_UESDown}}", {"up", "down"}, "UES")

             // Cut-based event categorization variables
             .Vary("mtMET_1_nominal", "ROOT::RVecF{mtMET_1_UESUp, mtMET_1_UESDown}", {"up", "down"}, "UES")
             .Vary("mtMET_2_nominal", "ROOT::RVecF{mtMET_2_UESUp, mtMET_2_UESDown}", {"up", "down"}, "UES")
             .Vary("D_zeta_nominal",  "ROOT::RVecF{D_zeta_UESUp, D_zeta_UESDown}", {"up", "down"}, "UES")

             // SVFit mass
             .Vary("m_tt_nominal", "ROOT::RVecF{m_tt_UESUp, m_tt_UESDown}", {"up", "down"}, "UES")

             // Even though there is no resolution/response up/down shape for these samples, call .Vary()
             // and just provide nominal value for these systematics, otherwise .Vary() will throw errors about not finding this systematic
             .Vary("met_nominal", "ROOT::RVecF{met_nominal, met_nominal}", {"up", "down"}, "met_0j_resolution")
             .Vary("met_nominal", "ROOT::RVecF{met_nominal, met_nominal}", {"up", "down"}, "met_1j_resolution")
             .Vary("met_nominal", "ROOT::RVecF{met_nominal, met_nominal}", {"up", "down"}, "met_gt1j_resolution")
             .Vary("met_nominal", "ROOT::RVecF{met_nominal, met_nominal}", {"up", "down"}, "met_0j_response")
             .Vary("met_nominal", "ROOT::RVecF{met_nominal, met_nominal}", {"up", "down"}, "met_1j_response")
             .Vary("met_nominal", "ROOT::RVecF{met_nominal, met_nominal}", {"up", "down"}, "met_gt1j_response");
  }
  else {  // data or embedded
    return df;
  }

}

/*******************************************************************/


#endif
