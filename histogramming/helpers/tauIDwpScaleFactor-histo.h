// Systematic for different tau ID working point used in MC vs. data

#ifndef TAU_ID_WP_SCALE_FACTOR_HISTO_H_INCL
#define TAU_ID_WP_SCALE_FACTOR_HISTO_H_INCL

#include "histoConfig_class.h"

/*******************************************************************/

/*
 * "Analyses that use looser working point of VSe (looser than VLoose) and/or VSmu (looser than Tight) discriminators,
 * should add additional uncertainty. In this case, an additional 3% should be added to MC and 5% to embedding sample
 * for nominal tau pT < 100 GeV. For high pT tau, where tau pT > 100 GeV, an additional 15% should be added."
 * https://twiki.cern.ch/twiki/bin/viewauth/CMS/TauIDRecommendationForRun2#Tau_Identification
 *
 * For us this only affects the etau channel because it uses VLoose vs. muon.
 * We do not use the tau pt > 100 15% uncertainty because our signal has soft objects and this 15% uncertainty would
 * not affect our signal region.
 *
 * In postprocessing whether the SF should be non-unity or not, is already handled.
 */
template <typename T>
auto GetTauIDwpShifts(T &df, LUNA::histoConfig_t &hConfig) {
    if (hConfig.doShifts() && hConfig.doETau() && (hConfig.isMC() || hConfig.isEmbedded())) {
        return df.Vary("weight_tauidWP_nominal", "ROOT::RVecF{weight_tauidWP_tauidWP_et_Up, weight_tauidWP_tauidWP_et_Down}", {"up", "down"}, "tauidWP_et");
    }
    return df;
}



/*******************************************************************/

#endif
