#ifndef OPTIONAL_RECOMPUTE_TRIGGER_DECISIONS_TAUTAU_H_INCL
#define OPTIONAL_RECOMPUTE_TRIGGER_DECISIONS_TAUTAU_H_INCL


#include "helperFunctions.h"
#include "histoConfig_class.h"

/**********************************************************/


/*
 * Check if an event passes a single trigger.
 */
bool passes_single_trigger(bool isEmbedded, bool passes_hlt, bool passes_filter, bool is_trigObj_matched, float pt_1, float pt_threshold_1, float pt_2, float pt_threshold_2) {
                                // return (passes_hlt && passes_filter && is_trigObj_matched && (pt_1 > pt_threshold_1) && (pt_2 > pt_threshold_2));
                                (void) isEmbedded;
                                (void) passes_filter;
                                return (passes_hlt && is_trigObj_matched && (pt_1 > pt_threshold_1) && (pt_2 > pt_threshold_2));
                            }



/*
 * _1 and _2 refer to the two legs for the cross trigger.
 */
bool passes_cross_trigger(int channel, bool isEmbedded, bool passes_cross_hlt, bool passes_cross_filter,
                          bool is_trigObj_matched_1, bool is_trigObj_matched_2,
                          int filterBits1, int filterBits2,
                          float pt_1, float pt_1_min, float pt_1_max, // first leg has a min and max
                          float pt_2, float pt_2_min,                 // second leg only has a min
                          float eta_2, float eta_2_max) {
                            (void) channel;
                            (void) filterBits1;
                            (void) filterBits2;
                            (void) passes_cross_filter;
                            if (isEmbedded) {
                                return (is_trigObj_matched_1 && is_trigObj_matched_2 &&
                                    (pt_1 > pt_1_min) && (pt_1 < pt_1_max) &&
                                    (pt_2 > pt_2_min) &&
                                    (abs(eta_2) < eta_2_max));

                            }
                            else {
                                return (passes_cross_hlt &&
                                    is_trigObj_matched_1 && is_trigObj_matched_2 &&
                                    (pt_1 > pt_1_min) && (pt_1 < pt_1_max) &&
                                    (pt_2 > pt_2_min) &&
                                    (abs(eta_2) < eta_2_max));
                            }
                          }

/*
 * _1 and _2 refer to the two legs for the cross trigger. Simpler version of the above function, where we only have pT minima to check.
 */
bool passes_cross_trigger_pt_min_only(bool isEmbedded, bool passes_cross_hlt, bool passes_cross_filter,
                                      bool is_trigObj_matched_1, bool is_trigObj_matched_2,
                                      float pt_1, float pt_1_min,
                                      float pt_2, float pt_2_min) {
                                        (void) passes_cross_filter;
                                        if (isEmbedded) {
                                            return (is_trigObj_matched_1 && is_trigObj_matched_2 &&
                                                (pt_1 > pt_1_min) && (pt_2 > pt_2_min));
                                        }
                                        else {
                                            return (passes_cross_hlt &&
                                                    is_trigObj_matched_1 && is_trigObj_matched_2 &&
                                                    (pt_1 > pt_1_min) && (pt_2 > pt_2_min));
                                        }
                                      }


/**********************************************************/

/*
 * 2018: Goal: define passSingleTrigger_{mt, et}, passCrossTrigger_{mt, et, em}
 */
template <typename T>
auto RecomputeTauTauTriggers_2018(T &df, LUNA::histoConfig_t &hConfig) {

    if (hConfig.year() == 2018) {
        // Mutau 2018 nominal
        auto df2 = df.Define("mt_single_mupt_thres",  []() { return (float) 25.0; })
                 .Define("mt_single_taupt_thres", []() { return (float) 20.0; })
                 .Define("mt_cross_mupt_thres",  []() { return (float) 21.0; })
                 .Define("mt_cross_taupt_thres", []() { return (float) 35.0; })
                 .Define("mt_cross_eta_thres", []() { return (float) 2.1; })

                 .Redefine("passSingle_1_mt_nominal", passes_single_trigger, {"isEmbedded", "pass_mt_HLT_IsoMu24", "filter_mt_Mu24", "match1", "pt_1_nominal", "mt_single_mupt_thres", "pt_2_nominal", "mt_single_taupt_thres"})
                 .Redefine("passSingle_2_mt_nominal", passes_single_trigger, {"isEmbedded", "pass_mt_HLT_IsoMu27", "filter_mt_Mu27", "match1", "pt_1_nominal", "mt_single_mupt_thres", "pt_2_nominal", "mt_single_taupt_thres"})
                 .Redefine("passSingleTrigger_mt_nominal", [](int single1, int single2) { return (single1 || single2); }, {"passSingle_1_mt_nominal", "passSingle_2_mt_nominal"})
                 .Redefine("passCrossTrigger_nonHPS_mt_nominal", passes_cross_trigger, {"channel", "isEmbedded", "pass_mt_HLT_Mu20Tau27", "filter_mt_Mu20Tau27", "match1", "match2",
                                                    "filterBits1", "filterBits2",
                                                    "pt_1_nominal", "mt_cross_mupt_thres", "mt_single_mupt_thres",
                                                    "pt_2_nominal", "mt_cross_taupt_thres",
                                                    "eta_2",  "mt_cross_eta_thres"})
                 .Redefine("passCrossTrigger_HPS_mt_nominal", passes_cross_trigger, {"channel", "isEmbedded", "pass_mt_HLT_Mu20HPSTau27", "filter_mt_Mu20HPSTau27", "match1", "match2",
                                                    "filterBits1", "filterBits2",
                                                    "pt_1_nominal", "mt_cross_mupt_thres", "mt_single_mupt_thres",
                                                    "pt_2_nominal", "mt_cross_taupt_thres",
                                                    "eta_2",  "mt_cross_eta_thres"})
                 .Redefine("passCrossTrigger_mt_nominal", [](unsigned int run, bool isData, bool cross, bool crossHPS) {
                    if (isData && run<317509)                // Data early runs only: cone-based trigger
                        return (cross);
                    else
                        return (crossHPS);
                    }, {"run", "isData", "passCrossTrigger_nonHPS_mt_nominal", "passCrossTrigger_HPS_mt_nominal"})
    // Etau 2018 nominal
                .Define("et_single_elept_thres",  []() { return (float) 33.0; })
                .Define("et_single_taupt_thres", []() { return (float) 20.0; })
                .Define("et_cross_elept_thres",  []() { return (float) 25.0; })
                .Define("et_cross_taupt_thres", []() { return (float) 35.0; })
                .Define("et_cross_eta_thres",  []() { return (float) 2.1; })

                .Redefine("passSingle_1_et_nominal", passes_single_trigger, {"isEmbedded", "pass_et_HLT_Ele32", "filter_et_Ele32", "match1", "pt_1_nominal", "et_single_elept_thres", "pt_2_nominal", "et_single_taupt_thres"})
                .Redefine("passSingle_2_et_nominal", passes_single_trigger, {"isEmbedded", "pass_et_HLT_Ele35", "filter_et_Ele35", "match1", "pt_1_nominal", "et_single_elept_thres", "pt_2_nominal", "et_single_taupt_thres"})
                .Redefine("passSingleTrigger_et_nominal", [](int single1, int single2) { return (single1 || single2); }, {"passSingle_1_et_nominal", "passSingle_2_et_nominal"})
                .Redefine("passCrossTrigger_nonHPS_et_nominal", passes_cross_trigger, {"channel", "isEmbedded", "pass_et_HLT_Ele24Tau30",  "filter_et_Ele24Tau30", "match1", "match2",
                                                                        "filterBits1", "filterBits2",
                                                                        "pt_1_nominal", "et_cross_elept_thres", "et_single_elept_thres",
                                                                        "pt_2_nominal", "et_cross_taupt_thres",
                                                                        "eta_2", "et_cross_eta_thres"})
                .Redefine("passCrossTrigger_HPS_et_nominal", passes_cross_trigger, {"channel", "isEmbedded", "pass_et_HLT_Ele24HPSTau30",  "filter_et_Ele24HPSTau30", "match1", "match2",
                                                                        "filterBits1", "filterBits2",
                                                                        "pt_1_nominal", "et_cross_elept_thres", "et_single_elept_thres",
                                                                        "pt_2_nominal", "et_cross_taupt_thres",
                                                                        "eta_2", "et_cross_eta_thres"})
                .Redefine("passCrossTrigger_et_nominal", [](unsigned int run, bool isData, bool cross, bool crossHPS) {
                    if (isData && run<317509)                // Data early runs only: cone-based trigger
                        return (cross);
                    else
                        return (crossHPS);
                    }, {"run", "isData", "passCrossTrigger_nonHPS_et_nominal", "passCrossTrigger_HPS_et_nominal"})


    // Emu 2018 nominal
            // EMu 2018
                .Define("em_crossLeadingMuon_elept_thres", []() { return (float) 13.0; })  // for Mu23Ele12
                .Define("em_crossLeadingMuon_mupt_thres",  []() { return (float) 24; })
                .Define("em_crossLeadingEle_elept_thres", []() { return (float) 24; })  // for Mu8Ele23
                .Define("em_crossLeadingEle_mupt_thres",  []() { return (float) 13; })
                .Redefine("passCross_leadingMuon_em_nominal", passes_cross_trigger_pt_min_only, {"isEmbedded", "pass_em_HLT_Mu23Ele12", "filter_em_Mu23Ele12", "match1", "match2",
                                                                                       "pt_1_nominal", "em_crossLeadingMuon_elept_thres",
                                                                                       "pt_2_nominal", "em_crossLeadingMuon_mupt_thres"})
                .Redefine("passCross_leadingEle_em_nominal",  passes_cross_trigger_pt_min_only, {"isEmbedded", "pass_em_HLT_Mu8Ele23", "filter_em_Mu8Ele23", "match1", "match2",
                                                                                       "pt_1_nominal", "em_crossLeadingEle_elept_thres",
                                                                                       "pt_2_nominal", "em_crossLeadingEle_mupt_thres"})
                .Redefine("passCrossTrigger_em_nominal", [](bool cross1, bool cross2) { return (cross1 || cross2); }, {"passCross_leadingMuon_em_nominal", "passCross_leadingEle_em_nominal"});



    if (hConfig.doShifts()) {
                // mutau 2018: es1Up (shifts due to muon ES up/down): repeat this entire block for es1Up/Down, where pt_2 is nominal
        return df2.Redefine("passSingle_1_mt_es1Up", passes_single_trigger, {"isEmbedded", "pass_mt_HLT_IsoMu24", "filter_mt_Mu24", "match1", "pt_1_es1Up", "mt_single_mupt_thres", "pt_2_nominal", "mt_single_taupt_thres"})
                 .Redefine("passSingle_2_mt_es1Up", passes_single_trigger, {"isEmbedded", "pass_mt_HLT_IsoMu27", "filter_mt_Mu27", "match1", "pt_1_es1Up", "mt_single_mupt_thres", "pt_2_nominal", "mt_single_taupt_thres"})
                 .Redefine("passSingleTrigger_mt_es1Up", [](bool single1, bool single2) { return (single1 || single2); }, {"passSingle_1_mt_es1Up", "passSingle_2_mt_es1Up"})
                 .Redefine("passCrossTrigger_nonHPS_mt_es1Up", passes_cross_trigger, {"channel", "isEmbedded", "pass_mt_HLT_Mu20Tau27", "filter_mt_Mu20Tau27", "match1", "match2",
                                                    "filterBits1", "filterBits2",
                                                    "pt_1_es1Up", "mt_cross_mupt_thres", "mt_single_mupt_thres",
                                                    "pt_2_nominal", "mt_cross_taupt_thres",
                                                    "eta_2",  "mt_cross_eta_thres"})
                 .Redefine("passCrossTrigger_HPS_mt_es1Up", passes_cross_trigger, {"channel", "isEmbedded", "pass_mt_HLT_Mu20HPSTau27", "filter_mt_Mu20HPSTau27", "match1", "match2",
                                                    "filterBits1", "filterBits2",
                                                    "pt_1_es1Up", "mt_cross_mupt_thres", "mt_single_mupt_thres",
                                                    "pt_2_nominal", "mt_cross_taupt_thres",
                                                    "eta_2",  "mt_cross_eta_thres"})
                 .Redefine("passCrossTrigger_mt_es1Up", [](unsigned int run, bool isData, bool cross, bool crossHPS) {
                    if (isData && run<317509)                // Data early runs only: cone-based trigger
                        return (cross);
                    else
                        return (crossHPS);
                    }, {"run", "isData", "passCrossTrigger_nonHPS_mt_es1Up", "passCrossTrigger_HPS_mt_es1Up"})

                // mutau 2018: es1Down
                 .Redefine("passSingle_1_mt_es1Down", passes_single_trigger, {"isEmbedded", "pass_mt_HLT_IsoMu24", "filter_mt_Mu24", "match1", "pt_1_es1Down", "mt_single_mupt_thres", "pt_2_nominal", "mt_single_taupt_thres"})
                 .Redefine("passSingle_2_mt_es1Down", passes_single_trigger, {"isEmbedded", "pass_mt_HLT_IsoMu27", "filter_mt_Mu27", "match1", "pt_1_es1Down", "mt_single_mupt_thres", "pt_2_nominal", "mt_single_taupt_thres"})
                 .Redefine("passSingleTrigger_mt_es1Down", [](bool single1, bool single2) { return (single1 || single2); }, {"passSingle_1_mt_es1Down", "passSingle_2_mt_es1Down"})
                 .Redefine("passCrossTrigger_nonHPS_mt_es1Down", passes_cross_trigger, {"channel", "isEmbedded", "pass_mt_HLT_Mu20Tau27", "filter_mt_Mu20Tau27", "match1", "match2",
                                                    "filterBits1", "filterBits2",
                                                    "pt_1_es1Down", "mt_cross_mupt_thres", "mt_single_mupt_thres",
                                                    "pt_2_nominal", "mt_cross_taupt_thres",
                                                    "eta_2",  "mt_cross_eta_thres"})
                 .Redefine("passCrossTrigger_HPS_mt_es1Down", passes_cross_trigger, {"channel", "isEmbedded", "pass_mt_HLT_Mu20HPSTau27", "filter_mt_Mu20HPSTau27", "match1", "match2",
                                                    "filterBits1", "filterBits2",
                                                    "pt_1_es1Down", "mt_cross_mupt_thres", "mt_single_mupt_thres",
                                                    "pt_2_nominal", "mt_cross_taupt_thres",
                                                    "eta_2",  "mt_cross_eta_thres"})
                 .Redefine("passCrossTrigger_mt_es1Down", [](unsigned int run, bool isData, bool cross, bool crossHPS) {
                    if (isData && run<317509)                // Data early runs only: cone-based trigger
                        return (cross);
                    else
                        return (crossHPS);
                    }, {"run", "isData", "passCrossTrigger_nonHPS_mt_es1Down", "passCrossTrigger_HPS_mt_es1Down"})


                // mutau 2018: es2Up, using pt_1_nominal
                 .Redefine("passSingle_1_mt_es2Up", passes_single_trigger, {"isEmbedded", "pass_mt_HLT_IsoMu24", "filter_mt_Mu24", "match1", "pt_1_nominal", "mt_single_mupt_thres", "pt_2_es2Up", "mt_single_taupt_thres"})
                 .Redefine("passSingle_2_mt_es2Up", passes_single_trigger, {"isEmbedded", "pass_mt_HLT_IsoMu27", "filter_mt_Mu27", "match1", "pt_1_nominal", "mt_single_mupt_thres", "pt_2_es2Up", "mt_single_taupt_thres"})
                 .Redefine("passSingleTrigger_mt_es2Up", [](bool single1, bool single2) { return (single1 || single2); }, {"passSingle_1_mt_es2Up", "passSingle_2_mt_es2Up"})
                 .Redefine("passCrossTrigger_nonHPS_mt_es2Up", passes_cross_trigger, {"channel", "isEmbedded", "pass_mt_HLT_Mu20Tau27", "filter_mt_Mu20Tau27", "match1", "match2",
                                                                                    "filterBits1", "filterBits2",
                                                                                    "pt_1_nominal", "mt_cross_mupt_thres", "mt_single_mupt_thres",
                                                                                    "pt_2_es2Up", "mt_cross_taupt_thres",
                                                                                    "eta_2",  "mt_cross_eta_thres"})
                 .Redefine("passCrossTrigger_HPS_mt_es2Up", passes_cross_trigger, {"channel", "isEmbedded", "pass_mt_HLT_Mu20HPSTau27", "filter_mt_Mu20HPSTau27", "match1", "match2",
                                                                                 "filterBits1", "filterBits2",
                                                                                 "pt_1_nominal", "mt_cross_mupt_thres", "mt_single_mupt_thres",
                                                                                 "pt_2_es2Up", "mt_cross_taupt_thres",
                                                                                 "eta_2",  "mt_cross_eta_thres"})
                 .Redefine("passCrossTrigger_mt_es2Up", [](unsigned int run, bool isData, bool cross, bool crossHPS) {
                    if (isData && run<317509)                // Data early runs only: cone-based trigger
                        return (cross);
                    else
                        return (crossHPS);
                    }, {"run", "isData", "passCrossTrigger_nonHPS_mt_es2Up", "passCrossTrigger_HPS_mt_es2Up"})

                // mutau 2018: es2Down, using pt_1_nominal
                 .Redefine("passSingle_1_mt_es2Down", passes_single_trigger, {"isEmbedded", "pass_mt_HLT_IsoMu24", "filter_mt_Mu24", "match1", "pt_1_nominal", "mt_single_mupt_thres", "pt_2_es2Down", "mt_single_taupt_thres"})
                 .Redefine("passSingle_2_mt_es2Down", passes_single_trigger, {"isEmbedded", "pass_mt_HLT_IsoMu27", "filter_mt_Mu27", "match1", "pt_1_nominal", "mt_single_mupt_thres", "pt_2_es2Down", "mt_single_taupt_thres"})
                 .Redefine("passSingleTrigger_mt_es2Down", [](bool single1, bool single2) { return (single1 || single2); }, {"passSingle_1_mt_es2Down", "passSingle_2_mt_es2Down"})
                 .Redefine("passCrossTrigger_nonHPS_mt_es2Down", passes_cross_trigger, {"channel", "isEmbedded", "pass_mt_HLT_Mu20Tau27", "filter_mt_Mu20Tau27", "match1", "match2",
                                                                                      "filterBits1", "filterBits2",
                                                                                      "pt_1_nominal", "mt_cross_mupt_thres", "mt_single_mupt_thres",
                                                                                      "pt_2_es2Down", "mt_cross_taupt_thres",
                                                                                      "eta_2",  "mt_cross_eta_thres"})
                 .Redefine("passCrossTrigger_HPS_mt_es2Down", passes_cross_trigger, {"channel", "isEmbedded", "pass_mt_HLT_Mu20HPSTau27", "filter_mt_Mu20HPSTau27", "match1", "match2",
                                                                                   "filterBits1", "filterBits2",
                                                                                   "pt_1_nominal", "mt_cross_mupt_thres", "mt_single_mupt_thres",
                                                                                   "pt_2_es2Down", "mt_cross_taupt_thres",
                                                                                   "eta_2",  "mt_cross_eta_thres"})
                 .Redefine("passCrossTrigger_mt_es2Down", [](unsigned int run, bool isData, bool cross, bool crossHPS) {
                    if (isData && run<317509)                // Data early runs only: cone-based trigger
                        return (cross);
                    else
                        return (crossHPS);
                    }, {"run", "isData", "passCrossTrigger_nonHPS_mt_es2Down", "passCrossTrigger_HPS_mt_es2Down"})


                // etau 2018: shifts due to es1Up (electron ES), using pt_2_nominal
                .Redefine("passSingle_1_et_es1Up", passes_single_trigger, {"isEmbedded", "pass_et_HLT_Ele32", "filter_et_Ele32", "match1", "pt_1_es1Up", "et_single_elept_thres", "pt_2_nominal", "et_single_taupt_thres"})
                .Redefine("passSingle_2_et_es1Up", passes_single_trigger, {"isEmbedded", "pass_et_HLT_Ele35", "filter_et_Ele35", "match1", "pt_1_es1Up", "et_single_elept_thres", "pt_2_nominal", "et_single_taupt_thres"})
                .Redefine("passSingleTrigger_et_es1Up", [](bool single1, bool single2) { return (single1 || single2); }, {"passSingle_1_et_es1Up", "passSingle_2_et_es1Up"})
                .Redefine("passCrossTrigger_nonHPS_et_es1Up", passes_cross_trigger, {"channel", "isEmbedded", "pass_et_HLT_Ele24Tau30",  "filter_et_Ele24Tau30", "match1", "match2",
                                                                        "filterBits1", "filterBits2",
                                                                        "pt_1_es1Up", "et_cross_elept_thres", "et_single_elept_thres",
                                                                        "pt_2_nominal", "et_cross_taupt_thres",
                                                                        "eta_2", "et_cross_eta_thres"})
                .Redefine("passCrossTrigger_HPS_et_es1Up", passes_cross_trigger, {"channel", "isEmbedded", "pass_et_HLT_Ele24HPSTau30",  "filter_et_Ele24HPSTau30", "match1", "match2",
                                                                        "filterBits1", "filterBits2",
                                                                        "pt_1_es1Up", "et_cross_elept_thres", "et_single_elept_thres",
                                                                        "pt_2_nominal", "et_cross_taupt_thres",
                                                                        "eta_2", "et_cross_eta_thres"})
                 .Redefine("passCrossTrigger_et_es1Up", [](unsigned int run, bool isData, bool cross, bool crossHPS) {
                    if (isData && run<317509)                // Data early runs only: cone-based trigger
                        return (cross);
                    else
                        return (crossHPS);
                    }, {"run", "isData", "passCrossTrigger_nonHPS_et_es1Up", "passCrossTrigger_HPS_et_es1Up"})


                // etau 2018: shifts due to es1Down (electronES), using pt_2_nominal
                .Redefine("passSingle_1_et_es1Down", passes_single_trigger, {"isEmbedded", "pass_et_HLT_Ele32", "filter_et_Ele32", "match1", "pt_1_es1Down", "et_single_elept_thres", "pt_2_nominal", "et_single_taupt_thres"})
                .Redefine("passSingle_2_et_es1Down", passes_single_trigger, {"isEmbedded", "pass_et_HLT_Ele35", "filter_et_Ele35", "match1", "pt_1_es1Down", "et_single_elept_thres", "pt_2_nominal", "et_single_taupt_thres"})
                .Redefine("passSingleTrigger_et_es1Down", [](bool single1, bool single2) { return (single1 || single2); }, {"passSingle_1_et_es1Down", "passSingle_2_et_es1Down"})
                .Redefine("passCrossTrigger_nonHPS_et_es1Down", passes_cross_trigger, {"channel", "isEmbedded", "pass_et_HLT_Ele24Tau30",  "filter_et_Ele24Tau30", "match1", "match2",
                                                                        "filterBits1", "filterBits2",
                                                                        "pt_1_es1Down", "et_cross_elept_thres", "et_single_elept_thres",
                                                                        "pt_2_nominal", "et_cross_taupt_thres",
                                                                        "eta_2", "et_cross_eta_thres"})

                .Redefine("passCrossTrigger_HPS_et_es1Down", passes_cross_trigger, {"channel", "isEmbedded", "pass_et_HLT_Ele24HPSTau30",  "filter_et_Ele24HPSTau30", "match1", "match2",
                                                                        "filterBits1", "filterBits2",
                                                                        "pt_1_es1Down", "et_cross_elept_thres", "et_single_elept_thres",
                                                                        "pt_2_nominal", "et_cross_taupt_thres",
                                                                        "eta_2", "et_cross_eta_thres"})
                 .Redefine("passCrossTrigger_et_es1Down", [](unsigned int run, bool isData, bool cross, bool crossHPS) {
                    if (isData && run<317509)                // Data early runs only: cone-based trigger
                        return (cross);
                    else
                        return (crossHPS);
                    }, {"run", "isData", "passCrossTrigger_nonHPS_et_es1Down", "passCrossTrigger_HPS_et_es1Down"})

                // etau 2018: shifts due to es2Up (tauES), using pt_1_nominal
                .Redefine("passSingle_1_et_es2Up", passes_single_trigger, {"isEmbedded", "pass_et_HLT_Ele32", "filter_et_Ele32", "match1", "pt_1_nominal", "et_single_elept_thres", "pt_2_es2Up", "et_single_taupt_thres"})
                .Redefine("passSingle_2_et_es2Up", passes_single_trigger, {"isEmbedded", "pass_et_HLT_Ele35", "filter_et_Ele35", "match1", "pt_1_nominal", "et_single_elept_thres", "pt_2_es2Up", "et_single_taupt_thres"})
                .Redefine("passSingleTrigger_et_es2Up", [](bool single1, bool single2) { return (single1 || single2); }, {"passSingle_1_et_es2Up", "passSingle_2_et_es2Up"})
                .Redefine("passCrossTrigger_nonHPS_et_es2Up", passes_cross_trigger, {"channel", "isEmbedded", "pass_et_HLT_Ele24Tau30",  "filter_et_Ele24Tau30", "match1", "match2",
                                                                                   "filterBits1", "filterBits2",
                                                                                   "pt_1_nominal", "et_cross_elept_thres", "et_single_elept_thres",
                                                                                   "pt_2_es2Up", "et_cross_taupt_thres",
                                                                                   "eta_2", "et_cross_eta_thres"})
                .Redefine("passCrossTrigger_HPS_et_es2Up", passes_cross_trigger, {"channel", "isEmbedded", "pass_et_HLT_Ele24HPSTau30",  "filter_et_Ele24HPSTau30", "match1", "match2",
                                                                                "filterBits1", "filterBits2",
                                                                                "pt_1_nominal", "et_cross_elept_thres", "et_single_elept_thres",
                                                                                "pt_2_es2Up", "et_cross_taupt_thres",
                                                                                "eta_2", "et_cross_eta_thres"})
                 .Redefine("passCrossTrigger_et_es2Up", [](unsigned int run, bool isData, bool cross, bool crossHPS) {
                    if (isData && run<317509)                // Data early runs only: cone-based trigger
                        return (cross);
                    else
                        return (crossHPS);
                    }, {"run", "isData", "passCrossTrigger_nonHPS_et_es2Up", "passCrossTrigger_HPS_et_es2Up"})


                // etau 2018: shifts due to es2Down (tauES), using pt_1_nominal
                .Redefine("passSingle_1_et_es2Down", passes_single_trigger, {"isEmbedded", "pass_et_HLT_Ele32", "filter_et_Ele32", "match1", "pt_1_nominal", "et_single_elept_thres", "pt_2_es2Down", "et_single_taupt_thres"})
                .Redefine("passSingle_2_et_es2Down", passes_single_trigger, {"isEmbedded", "pass_et_HLT_Ele35", "filter_et_Ele35", "match1", "pt_1_nominal", "et_single_elept_thres", "pt_2_es2Down", "et_single_taupt_thres"})
                .Redefine("passSingleTrigger_et_es2Down", [](bool single1, bool single2) { return (single1 || single2); }, {"passSingle_1_et_es2Down", "passSingle_2_et_es2Down"})
                .Redefine("passCrossTrigger_nonHPS_et_es2Down", passes_cross_trigger, {"channel", "isEmbedded", "pass_et_HLT_Ele24Tau30",  "filter_et_Ele24Tau30", "match1", "match2",
                                                                                     "filterBits1", "filterBits2",
                                                                                     "pt_1_nominal", "et_cross_elept_thres", "et_single_elept_thres",
                                                                                     "pt_2_es2Down", "et_cross_taupt_thres",
                                                                                     "eta_2", "et_cross_eta_thres"})
                .Redefine("passCrossTrigger_HPS_et_es2Down", passes_cross_trigger, {"channel", "isEmbedded", "pass_et_HLT_Ele24HPSTau30",  "filter_et_Ele24HPSTau30", "match1", "match2",
                                                                                  "filterBits1", "filterBits2",
                                                                                  "pt_1_nominal", "et_cross_elept_thres", "et_single_elept_thres",
                                                                                  "pt_2_es2Down", "et_cross_taupt_thres",
                                                                                  "eta_2", "et_cross_eta_thres"})
                 .Redefine("passCrossTrigger_et_es2Down", [](unsigned int run, bool isData, bool cross, bool crossHPS) {
                    if (isData && run<317509)                // Data early runs only: cone-based trigger
                        return (cross);
                    else
                        return (crossHPS);
                    }, {"run", "isData", "passCrossTrigger_nonHPS_et_es2Down", "passCrossTrigger_HPS_et_es2Down"})


                // emu 2018: shifts due to es1Up, using pt_2_nominal
                .Redefine("passCross_leadingMuon_em_es1Up", passes_cross_trigger_pt_min_only, {"isEmbedded", "pass_em_HLT_Mu23Ele12", "filter_em_Mu23Ele12", "match1", "match2",
                                                                                       "pt_1_es1Up",   "em_crossLeadingMuon_elept_thres",
                                                                                       "pt_2_nominal", "em_crossLeadingMuon_mupt_thres"})
                .Redefine("passCross_leadingEle_em_es1Up",  passes_cross_trigger_pt_min_only, {"isEmbedded", "pass_em_HLT_Mu8Ele23", "filter_em_Mu8Ele23", "match1", "match2",
                                                                                       "pt_1_es1Up",   "em_crossLeadingEle_elept_thres",
                                                                                       "pt_2_nominal", "em_crossLeadingEle_mupt_thres"})
                .Redefine("passCrossTrigger_em_es1Up", [](bool cross1, bool cross2) { return (cross1 || cross2); }, {"passCross_leadingMuon_em_es1Up", "passCross_leadingEle_em_es1Up"})

                // emu 2018: shifts due to es1Down, using pt_2_nominal
                .Redefine("passCross_leadingMuon_em_es1Down", passes_cross_trigger_pt_min_only, {"isEmbedded", "pass_em_HLT_Mu23Ele12", "filter_em_Mu23Ele12", "match1", "match2",
                                                                                       "pt_1_es1Down",   "em_crossLeadingMuon_elept_thres",
                                                                                       "pt_2_nominal", "em_crossLeadingMuon_mupt_thres"})
                .Redefine("passCross_leadingEle_em_es1Down",  passes_cross_trigger_pt_min_only, {"isEmbedded", "pass_em_HLT_Mu8Ele23", "filter_em_Mu8Ele23", "match1", "match2",
                                                                                       "pt_1_es1Down",   "em_crossLeadingEle_elept_thres",
                                                                                       "pt_2_nominal", "em_crossLeadingEle_mupt_thres"})
                .Redefine("passCrossTrigger_em_es1Down", [](bool cross1, bool cross2) { return (cross1 || cross2); }, {"passCross_leadingMuon_em_es1Down", "passCross_leadingEle_em_es1Down"})

                 // emu 2018: shifts due to es2Up, using pt_1_nominal
                .Redefine("passCross_leadingMuon_em_es2Up", passes_cross_trigger_pt_min_only, {"isEmbedded", "pass_em_HLT_Mu23Ele12", "filter_em_Mu23Ele12", "match1", "match2",
                                                                                       "pt_1_nominal",   "em_crossLeadingMuon_elept_thres",
                                                                                       "pt_2_es2Up", "em_crossLeadingMuon_mupt_thres"})
                .Redefine("passCross_leadingEle_em_es2Up",  passes_cross_trigger_pt_min_only, {"isEmbedded", "pass_em_HLT_Mu8Ele23", "filter_em_Mu8Ele23", "match1", "match2",
                                                                                       "pt_1_nominal",   "em_crossLeadingEle_elept_thres",
                                                                                       "pt_2_es2Up", "em_crossLeadingEle_mupt_thres"})
                .Redefine("passCrossTrigger_em_es2Up", [](bool cross1, bool cross2) { return (cross1 || cross2); }, {"passCross_leadingMuon_em_es2Up", "passCross_leadingEle_em_es2Up"})

                // emu 2018: shifts due to es2Down, using pt_1_nominal
                .Redefine("passCross_leadingMuon_em_es2Down", passes_cross_trigger_pt_min_only, {"isEmbedded", "pass_em_HLT_Mu23Ele12", "filter_em_Mu23Ele12", "match1", "match2",
                                                                                       "pt_1_nominal",   "em_crossLeadingMuon_elept_thres",
                                                                                       "pt_2_es2Down", "em_crossLeadingMuon_mupt_thres"})
                .Redefine("passCross_leadingEle_em_es2Down",  passes_cross_trigger_pt_min_only, {"isEmbedded", "pass_em_HLT_Mu8Ele23", "filter_em_Mu8Ele23", "match1", "match2",
                                                                                       "pt_1_nominal",   "em_crossLeadingEle_elept_thres",
                                                                                       "pt_2_es2Down", "em_crossLeadingEle_mupt_thres"})
                .Redefine("passCrossTrigger_em_es2Down", [](bool cross1, bool cross2) { return (cross1 || cross2); }, {"passCross_leadingMuon_em_es2Down", "passCross_leadingEle_em_es2Down"});
        }
        else {
            return df2;
        }
    }
    else { // not 2018
        return df;
    }

}

// To-do: other years: et: https://github.com/hftsoi/aabbtt_finalselection/blob/master/selection_et_allyears.cc#L719-L732
// mt: https://github.com/hftsoi/aabbtt_finalselection/blob/master/selection_mt_allyears.cc#L741-L750
// et: https://github.com/hftsoi/aabbtt_finalselection/blob/cefb486856b4c817d20c6b4c49d17eaef115f78e/selection_em_allyears.cc#L716-L720
/**********************************************************/

/*
 * If indicated in the histoConfig, re-compute (re-define) the columns passSingleTrigger_mt_nominal, passCrossTrigger_mt_nominal, etc.
 */

template <typename T>
auto OptionalRecomputeTriggerDecision(T &df, LUNA::histoConfig_t &hConfig) {

    std::cout << ">>> optionalRecomputeTriggerDecisions-histo.h: hConfig.recomputeTrigger() is " << hConfig.recomputeTrigger() << std::endl;

    if (hConfig.recomputeTrigger()) {

        auto df2 = RecomputeTauTauTriggers_2018(df, hConfig);
        return df2;
    }
    else {
        return df;
    }
}


/**********************************************************/



#endif
