//Top pT reweighing

#ifndef TOP_PT_REWEIGHING_HISTO_H_INCL
#define TOP_PT_REWEIGHING_HISTO_H_INCL

#include "histoConfig_class.h"

/*******************************************************************/

template <typename T>
auto GetTopPtReweighingShifts(T &df, LUNA::histoConfig_t &hConfig) {

  if (hConfig.doShifts()) {
    if (hConfig.isMC() && hConfig.isTTbar()) {
      return df.Vary("weight_topPt_nominal", "ROOT::RVecF{weight_topPt_toppt_Up, weight_topPt_toppt_Down}", {"up", "down"}, "toppt");
    }
    else {
      return df.Vary("weight_topPt_nominal", "ROOT::RVecF{weight_topPt_nominal, weight_topPt_nominal}", {"up", "down"}, "toppt");
    }
  }
  return df;
}

/*******************************************************************/

#endif
