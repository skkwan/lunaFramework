#ifndef JET_FAKE_BACKGROUND_HISTO_H_INCL
#define JET_FAKE_BACKGROUND_HISTO_H_INCL

#include "helperFunctions.h"
#include "histoConfig_class.h"

/*******************************************************************/

/*
 * GetTauFR.h:
 * https://github.com/hftsoi/aabbtt_finalselection/blob/cefb486856b4c817d20c6b4c49d17eaef115f78e/GetTauFR.h
 * Each TGraphAsymmErrors has six bins ordered in [20, 25), [25, 30), [30, 35), [35, 40), [40, 50), [50, 60), [60, 80), [80, 100), [100, 120), [120, 150), and [150, +)
 *  [20, 25), [25, 30), [30, 35), [35, 40), [40, 50), [50, 60), [60, 80), [80, 100), [100, 120), [120, 150), and [150, +)
 */
ROOT::RVecF getJetFakeWeight_variations(
    float tauPt, float weight_ff_nominal, int byVVVLooseDeepVSjet_2, int byMediumDeepVSjet_2,
    float weight_ff_jetFR_pt0to25_Up, float weight_ff_jetFR_pt0to25_Down,
    float weight_ff_jetFR_pt25to30_Up, float weight_ff_jetFR_pt25to30_Down,
    float weight_ff_jetFR_pt30to35_Up, float weight_ff_jetFR_pt30to35_Down,
    float weight_ff_jetFR_pt35to40_Up, float weight_ff_jetFR_pt35to40_Down,
    float weight_ff_jetFR_pt40to50_Up, float weight_ff_jetFR_pt40to50_Down,
    float weight_ff_jetFR_pt50to60_Up, float weight_ff_jetFR_pt50to60_Down,
    float weight_ff_jetFR_pt60to80_Up, float weight_ff_jetFR_pt60to80_Down,
    float weight_ff_jetFR_pt80to100_Up,float weight_ff_jetFR_pt80to100Down,
    float weight_ff_jetFR_pt100to120_Up, float weight_ff_jetFR_pt100to120_Down,
    float weight_ff_jetFR_pt120to150_Up, float weight_ff_jetFR_pt120to150_Down,
    float weight_ff_jetFR_ptgt150_Up, float weight_ff_jetFR_ptgt150_Down,
    std::string sysToDo) {

      // Iso region:
      if ((byVVVLooseDeepVSjet_2 > 0) && (byMediumDeepVSjet_2 > 0)) {
        return ROOT::RVecF{weight_ff_nominal, weight_ff_nominal};
      }
      // Anti-isolation
      else if ((byVVVLooseDeepVSjet_2 > 0) && (byMediumDeepVSjet_2 > 0)) {
        if ((sysToDo == "jetFR_pt0to25") && (tauPt < 25)) {
          return ROOT::RVecF{weight_ff_jetFR_pt0to25_Up, weight_ff_jetFR_pt0to25_Down};
        } else if ((sysToDo == "jetFR_pt25to30") && (tauPt >= 25) && (tauPt < 30)) {
          return ROOT::RVecF{weight_ff_jetFR_pt25to30_Up, weight_ff_jetFR_pt25to30_Down};
        } else if ((sysToDo == "jetFR_pt30to35") && (tauPt >= 30) && (tauPt < 35)) {
          return ROOT::RVecF{weight_ff_jetFR_pt30to35_Up, weight_ff_jetFR_pt30to35_Down};
        } else if ((sysToDo == "jetFR_pt35to40") && (tauPt >= 35) && (tauPt < 40)) {
          return ROOT::RVecF{weight_ff_jetFR_pt35to40_Up, weight_ff_jetFR_pt35to40_Down};
        } else if ((sysToDo == "jetFR_pt40to50") && (tauPt >= 40) && (tauPt < 50)) {
          return ROOT::RVecF{weight_ff_jetFR_pt40to50_Up, weight_ff_jetFR_pt40to50_Down};
        } else if ((sysToDo == "jetFR_pt50to60") && (tauPt >= 50) && (tauPt < 60)) {
          return ROOT::RVecF{weight_ff_jetFR_pt50to60_Up, weight_ff_jetFR_pt50to60_Down};
        } else if ((sysToDo == "jetFR_pt60to80") && (tauPt >= 60) && (tauPt < 80)) {
          return ROOT::RVecF{weight_ff_jetFR_pt60to80_Up, weight_ff_jetFR_pt60to80_Down};
        } else if ((sysToDo == "jetFR_pt80to100")  && (tauPt >= 80) && (tauPt < 100)) {
          return ROOT::RVecF{weight_ff_jetFR_pt80to100_Up, weight_ff_jetFR_pt80to100Down};
        } else if ((sysToDo == "jetFR_pt100to120")  && (tauPt >= 100) && (tauPt < 120)) {
          return ROOT::RVecF{weight_ff_jetFR_pt100to120_Up, weight_ff_jetFR_pt100to120_Down};
        } else if ((sysToDo == "jetFR_pt120to150")  && (tauPt >= 120) && (tauPt < 150)) {
          return ROOT::RVecF{weight_ff_jetFR_pt120to150_Up, weight_ff_jetFR_pt120to150_Down};
        } else if ((sysToDo == "jetFR_ptgt150")  && (tauPt >= 150)) {
          return ROOT::RVecF{weight_ff_jetFR_ptgt150_Up, weight_ff_jetFR_ptgt150_Down};
        }
      }

      // Else default to the nominal weight
      return ROOT::RVecF{weight_ff_nominal, weight_ff_nominal};

}

/*******************************************************************/

/*
 * In the mutau and etau channels, the jet fake background is estimated by
 * re-weighing anti-isolated events by a scale factor, and subtracting the sum
 * of the MC re-weighted anti-iso histograms, from the data re-weighted anti-iso
 * histograms. This function calculates the weight that needs to be applied to
 * the anti-iso events, and the up/down shifts binned in pT. The anti-iso
 * selection needs to be done elsewhere in the code.
 */

template <typename T>
auto GetJetFakeBackgroundScaleFactorShifts(T &df, LUNA::histoConfig_t &hConfig) {

  if (hConfig.doShifts() && (hConfig.doMuTau() || hConfig.doETau())) {
    return df
        .Define("jetFR_pt0to25", [] { return (std::string) "jetFR_pt0to25"; })
        .Define("jetFR_pt25to30", [] { return (std::string) "jetFR_pt25to30"; })
        .Define("jetFR_pt30to35", [] { return (std::string) "jetFR_pt30to35"; })
        .Define("jetFR_pt35to40", [] { return (std::string) "jetFR_pt35to40"; })
        .Define("jetFR_pt40to50", [] { return (std::string) "jetFR_pt40to50"; })
        .Define("jetFR_pt50to60", [] { return (std::string) "jetFR_pt50to60"; })
        .Define("jetFR_pt60to80", [] { return (std::string) "jetFR_pt60to80"; })
        .Define("jetFR_pt80to100", [] { return (std::string) "jetFR_pt80to100"; })
        .Define("jetFR_pt100to120", [] { return (std::string) "jetFR_pt100to120"; })
        .Define("jetFR_pt120to150", [] { return (std::string) "jetFR_pt120to150"; })
        .Define("jetFR_ptgt150", [] { return (std::string) "jetFR_ptgt150"; })
        .Vary("weight_ff_nominal", getJetFakeWeight_variations,
              {"pt_2_nominal", "weight_ff_nominal", "byVVVLooseDeepVSjet_2", "byMediumDeepVSjet_2",
                "weight_ff_jetFR_pt0to25_Up", "weight_ff_jetFR_pt0to25_Down",
                "weight_ff_jetFR_pt25to30_Up", "weight_ff_jetFR_pt25to30_Down",
                "weight_ff_jetFR_pt30to35_Up", "weight_ff_jetFR_pt30to35_Down",
                "weight_ff_jetFR_pt35to40_Up", "weight_ff_jetFR_pt35to40_Down",
                "weight_ff_jetFR_pt40to50_Up", "weight_ff_jetFR_pt40to50_Down",
                "weight_ff_jetFR_pt50to60_Up", "weight_ff_jetFR_pt50to60_Down",
                "weight_ff_jetFR_pt60to80_Up", "weight_ff_jetFR_pt60to80_Down",
                "weight_ff_jetFR_pt80to100_Up", "weight_ff_jetFR_pt80to100_Down",
                "weight_ff_jetFR_pt100to120_Up", "weight_ff_jetFR_pt100to120_Down",
                "weight_ff_jetFR_pt120to150_Up", "weight_ff_jetFR_pt120to150_Down",
                "weight_ff_jetFR_ptgt150_Up", "weight_ff_jetFR_ptgt150_Down",
                "jetFR_pt0to25"}, {"up", "down"}, "jetFR_pt0to25")
          .Vary("weight_ff_nominal", getJetFakeWeight_variations,
              {"pt_2_nominal", "weight_ff_nominal", "byVVVLooseDeepVSjet_2", "byMediumDeepVSjet_2",
                "weight_ff_jetFR_pt0to25_Up", "weight_ff_jetFR_pt0to25_Down",
                "weight_ff_jetFR_pt25to30_Up", "weight_ff_jetFR_pt25to30_Down",
                "weight_ff_jetFR_pt30to35_Up", "weight_ff_jetFR_pt30to35_Down",
                "weight_ff_jetFR_pt35to40_Up", "weight_ff_jetFR_pt35to40_Down",
                "weight_ff_jetFR_pt40to50_Up", "weight_ff_jetFR_pt40to50_Down",
                "weight_ff_jetFR_pt50to60_Up", "weight_ff_jetFR_pt50to60_Down",
                "weight_ff_jetFR_pt60to80_Up", "weight_ff_jetFR_pt60to80_Down",
                "weight_ff_jetFR_pt80to100_Up", "weight_ff_jetFR_pt80to100_Down",
                "weight_ff_jetFR_pt100to120_Up", "weight_ff_jetFR_pt100to120_Down",
                "weight_ff_jetFR_pt120to150_Up", "weight_ff_jetFR_pt120to150_Down",
                "weight_ff_jetFR_ptgt150_Up", "weight_ff_jetFR_ptgt150_Down",
                "jetFR_pt25to30"}, {"up", "down"}, "jetFR_pt25to30")
          .Vary("weight_ff_nominal", getJetFakeWeight_variations,
              {"pt_2_nominal", "weight_ff_nominal", "byVVVLooseDeepVSjet_2", "byMediumDeepVSjet_2",
                "weight_ff_jetFR_pt0to25_Up", "weight_ff_jetFR_pt0to25_Down",
                "weight_ff_jetFR_pt25to30_Up", "weight_ff_jetFR_pt25to30_Down",
                "weight_ff_jetFR_pt30to35_Up", "weight_ff_jetFR_pt30to35_Down",
                "weight_ff_jetFR_pt35to40_Up", "weight_ff_jetFR_pt35to40_Down",
                "weight_ff_jetFR_pt40to50_Up", "weight_ff_jetFR_pt40to50_Down",
                "weight_ff_jetFR_pt50to60_Up", "weight_ff_jetFR_pt50to60_Down",
                "weight_ff_jetFR_pt60to80_Up", "weight_ff_jetFR_pt60to80_Down",
                "weight_ff_jetFR_pt80to100_Up", "weight_ff_jetFR_pt80to100_Down",
                "weight_ff_jetFR_pt100to120_Up", "weight_ff_jetFR_pt100to120_Down",
                "weight_ff_jetFR_pt120to150_Up", "weight_ff_jetFR_pt120to150_Down",
                "weight_ff_jetFR_ptgt150_Up", "weight_ff_jetFR_ptgt150_Down",
                "jetFR_pt30to35"}, {"up", "down"}, "jetFR_pt30to35")
          .Vary("weight_ff_nominal", getJetFakeWeight_variations,
              {"pt_2_nominal", "weight_ff_nominal", "byVVVLooseDeepVSjet_2", "byMediumDeepVSjet_2",
                "weight_ff_jetFR_pt0to25_Up", "weight_ff_jetFR_pt0to25_Down",
                "weight_ff_jetFR_pt25to30_Up", "weight_ff_jetFR_pt25to30_Down",
                "weight_ff_jetFR_pt30to35_Up", "weight_ff_jetFR_pt30to35_Down",
                "weight_ff_jetFR_pt35to40_Up", "weight_ff_jetFR_pt35to40_Down",
                "weight_ff_jetFR_pt40to50_Up", "weight_ff_jetFR_pt40to50_Down",
                "weight_ff_jetFR_pt50to60_Up", "weight_ff_jetFR_pt50to60_Down",
                "weight_ff_jetFR_pt60to80_Up", "weight_ff_jetFR_pt60to80_Down",
                "weight_ff_jetFR_pt80to100_Up", "weight_ff_jetFR_pt80to100_Down",
                "weight_ff_jetFR_pt100to120_Up", "weight_ff_jetFR_pt100to120_Down",
                "weight_ff_jetFR_pt120to150_Up", "weight_ff_jetFR_pt120to150_Down",
                "weight_ff_jetFR_ptgt150_Up", "weight_ff_jetFR_ptgt150_Down",
                "jetFR_pt35to40"}, {"up", "down"}, "jetFR_pt35to40")
          .Vary("weight_ff_nominal", getJetFakeWeight_variations,
              {"pt_2_nominal", "weight_ff_nominal", "byVVVLooseDeepVSjet_2", "byMediumDeepVSjet_2",
                "weight_ff_jetFR_pt0to25_Up", "weight_ff_jetFR_pt0to25_Down",
                "weight_ff_jetFR_pt25to30_Up", "weight_ff_jetFR_pt25to30_Down",
                "weight_ff_jetFR_pt30to35_Up", "weight_ff_jetFR_pt30to35_Down",
                "weight_ff_jetFR_pt35to40_Up", "weight_ff_jetFR_pt35to40_Down",
                "weight_ff_jetFR_pt40to50_Up", "weight_ff_jetFR_pt40to50_Down",
                "weight_ff_jetFR_pt50to60_Up", "weight_ff_jetFR_pt50to60_Down",
                "weight_ff_jetFR_pt60to80_Up", "weight_ff_jetFR_pt60to80_Down",
                "weight_ff_jetFR_pt80to100_Up", "weight_ff_jetFR_pt80to100_Down",
                "weight_ff_jetFR_pt100to120_Up", "weight_ff_jetFR_pt100to120_Down",
                "weight_ff_jetFR_pt120to150_Up", "weight_ff_jetFR_pt120to150_Down",
                "weight_ff_jetFR_ptgt150_Up", "weight_ff_jetFR_ptgt150_Down",
                "jetFR_pt40to50"}, {"up", "down"}, "jetFR_pt40to50")
          .Vary("weight_ff_nominal", getJetFakeWeight_variations,
              {"pt_2_nominal", "weight_ff_nominal", "byVVVLooseDeepVSjet_2", "byMediumDeepVSjet_2",
                "weight_ff_jetFR_pt0to25_Up", "weight_ff_jetFR_pt0to25_Down",
                "weight_ff_jetFR_pt25to30_Up", "weight_ff_jetFR_pt25to30_Down",
                "weight_ff_jetFR_pt30to35_Up", "weight_ff_jetFR_pt30to35_Down",
                "weight_ff_jetFR_pt35to40_Up", "weight_ff_jetFR_pt35to40_Down",
                "weight_ff_jetFR_pt40to50_Up", "weight_ff_jetFR_pt40to50_Down",
                "weight_ff_jetFR_pt50to60_Up", "weight_ff_jetFR_pt50to60_Down",
                "weight_ff_jetFR_pt60to80_Up", "weight_ff_jetFR_pt60to80_Down",
                "weight_ff_jetFR_pt80to100_Up", "weight_ff_jetFR_pt80to100_Down",
                "weight_ff_jetFR_pt100to120_Up", "weight_ff_jetFR_pt100to120_Down",
                "weight_ff_jetFR_pt120to150_Up", "weight_ff_jetFR_pt120to150_Down",
                "weight_ff_jetFR_ptgt150_Up", "weight_ff_jetFR_ptgt150_Down",
                "jetFR_pt50to60"}, {"up", "down"}, "jetFR_pt50to60")
          .Vary("weight_ff_nominal", getJetFakeWeight_variations,
              {"pt_2_nominal", "weight_ff_nominal", "byVVVLooseDeepVSjet_2", "byMediumDeepVSjet_2",
                "weight_ff_jetFR_pt0to25_Up", "weight_ff_jetFR_pt0to25_Down",
                "weight_ff_jetFR_pt25to30_Up", "weight_ff_jetFR_pt25to30_Down",
                "weight_ff_jetFR_pt30to35_Up", "weight_ff_jetFR_pt30to35_Down",
                "weight_ff_jetFR_pt35to40_Up", "weight_ff_jetFR_pt35to40_Down",
                "weight_ff_jetFR_pt40to50_Up", "weight_ff_jetFR_pt40to50_Down",
                "weight_ff_jetFR_pt50to60_Up", "weight_ff_jetFR_pt50to60_Down",
                "weight_ff_jetFR_pt60to80_Up", "weight_ff_jetFR_pt60to80_Down",
                "weight_ff_jetFR_pt80to100_Up", "weight_ff_jetFR_pt80to100_Down",
                "weight_ff_jetFR_pt100to120_Up", "weight_ff_jetFR_pt100to120_Down",
                "weight_ff_jetFR_pt120to150_Up", "weight_ff_jetFR_pt120to150_Down",
                "weight_ff_jetFR_ptgt150_Up", "weight_ff_jetFR_ptgt150_Down",
                "jetFR_pt60to80"}, {"up", "down"}, "jetFR_pt60to80")
            .Vary("weight_ff_nominal", getJetFakeWeight_variations,
              {"pt_2_nominal", "weight_ff_nominal", "byVVVLooseDeepVSjet_2", "byMediumDeepVSjet_2",
                "weight_ff_jetFR_pt0to25_Up", "weight_ff_jetFR_pt0to25_Down",
                "weight_ff_jetFR_pt25to30_Up", "weight_ff_jetFR_pt25to30_Down",
                "weight_ff_jetFR_pt30to35_Up", "weight_ff_jetFR_pt30to35_Down",
                "weight_ff_jetFR_pt35to40_Up", "weight_ff_jetFR_pt35to40_Down",
                "weight_ff_jetFR_pt40to50_Up", "weight_ff_jetFR_pt40to50_Down",
                "weight_ff_jetFR_pt50to60_Up", "weight_ff_jetFR_pt50to60_Down",
                "weight_ff_jetFR_pt60to80_Up", "weight_ff_jetFR_pt60to80_Down",
                "weight_ff_jetFR_pt80to100_Up", "weight_ff_jetFR_pt80to100_Down",
                "weight_ff_jetFR_pt100to120_Up", "weight_ff_jetFR_pt100to120_Down",
                "weight_ff_jetFR_pt120to150_Up", "weight_ff_jetFR_pt120to150_Down",
                "weight_ff_jetFR_ptgt150_Up", "weight_ff_jetFR_ptgt150_Down",
                "jetFR_pt80to100"}, {"up", "down"}, "jetFR_pt80to100")
            .Vary("weight_ff_nominal", getJetFakeWeight_variations,
              {"pt_2_nominal", "weight_ff_nominal", "byVVVLooseDeepVSjet_2", "byMediumDeepVSjet_2",
                "weight_ff_jetFR_pt0to25_Up", "weight_ff_jetFR_pt0to25_Down",
                "weight_ff_jetFR_pt25to30_Up", "weight_ff_jetFR_pt25to30_Down",
                "weight_ff_jetFR_pt30to35_Up", "weight_ff_jetFR_pt30to35_Down",
                "weight_ff_jetFR_pt35to40_Up", "weight_ff_jetFR_pt35to40_Down",
                "weight_ff_jetFR_pt40to50_Up", "weight_ff_jetFR_pt40to50_Down",
                "weight_ff_jetFR_pt50to60_Up", "weight_ff_jetFR_pt50to60_Down",
                "weight_ff_jetFR_pt60to80_Up", "weight_ff_jetFR_pt60to80_Down",
                "weight_ff_jetFR_pt80to100_Up", "weight_ff_jetFR_pt80to100_Down",
                "weight_ff_jetFR_pt100to120_Up", "weight_ff_jetFR_pt100to120_Down",
                "weight_ff_jetFR_pt120to150_Up", "weight_ff_jetFR_pt120to150_Down",
                "weight_ff_jetFR_ptgt150_Up", "weight_ff_jetFR_ptgt150_Down",
                "jetFR_pt100to120"}, {"up", "down"}, "jetFR_pt100to120")
            .Vary("weight_ff_nominal", getJetFakeWeight_variations,
              {"pt_2_nominal", "weight_ff_nominal", "byVVVLooseDeepVSjet_2", "byMediumDeepVSjet_2",
                "weight_ff_jetFR_pt0to25_Up", "weight_ff_jetFR_pt0to25_Down",
                "weight_ff_jetFR_pt25to30_Up", "weight_ff_jetFR_pt25to30_Down",
                "weight_ff_jetFR_pt30to35_Up", "weight_ff_jetFR_pt30to35_Down",
                "weight_ff_jetFR_pt35to40_Up", "weight_ff_jetFR_pt35to40_Down",
                "weight_ff_jetFR_pt40to50_Up", "weight_ff_jetFR_pt40to50_Down",
                "weight_ff_jetFR_pt50to60_Up", "weight_ff_jetFR_pt50to60_Down",
                "weight_ff_jetFR_pt60to80_Up", "weight_ff_jetFR_pt60to80_Down",
                "weight_ff_jetFR_pt80to100_Up", "weight_ff_jetFR_pt80to100_Down",
                "weight_ff_jetFR_pt100to120_Up", "weight_ff_jetFR_pt100to120_Down",
                "weight_ff_jetFR_pt120to150_Up", "weight_ff_jetFR_pt120to150_Down",
                "weight_ff_jetFR_ptgt150_Up", "weight_ff_jetFR_ptgt150_Down",
                "jetFR_pt120to150"}, {"up", "down"}, "jetFR_pt120to150")
            .Vary("weight_ff_nominal", getJetFakeWeight_variations,
              {"pt_2_nominal", "weight_ff_nominal", "byVVVLooseDeepVSjet_2", "byMediumDeepVSjet_2",
                "weight_ff_jetFR_pt0to25_Up", "weight_ff_jetFR_pt0to25_Down",
                "weight_ff_jetFR_pt25to30_Up", "weight_ff_jetFR_pt25to30_Down",
                "weight_ff_jetFR_pt30to35_Up", "weight_ff_jetFR_pt30to35_Down",
                "weight_ff_jetFR_pt35to40_Up", "weight_ff_jetFR_pt35to40_Down",
                "weight_ff_jetFR_pt40to50_Up", "weight_ff_jetFR_pt40to50_Down",
                "weight_ff_jetFR_pt50to60_Up", "weight_ff_jetFR_pt50to60_Down",
                "weight_ff_jetFR_pt60to80_Up", "weight_ff_jetFR_pt60to80_Down",
                "weight_ff_jetFR_pt80to100_Up", "weight_ff_jetFR_pt80to100_Down",
                "weight_ff_jetFR_pt100to120_Up", "weight_ff_jetFR_pt100to120_Down",
                "weight_ff_jetFR_pt120to150_Up", "weight_ff_jetFR_pt120to150_Down",
                "weight_ff_jetFR_ptgt150_Up", "weight_ff_jetFR_ptgt150_Down",
                "jetFR_ptgt150"}, {"up", "down"}, "jetFR_ptgt150");
        // n.b. mutau: shifts due to the pt_2 energy scale up/down variations, are handled already in tauES-histo.h
  }
  return df;
}

/*******************************************************************/

#endif
