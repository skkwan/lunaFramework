#ifndef TRIGGER_EFFICIENCIES_TAUTAU_HISTO_H_INCL
#define TRIGGER_EFFICIENCIES_TAUTAU_HISTO_H_INCL

#include "helperFunctions.h"
#include "histoConfig_class.h"

/**********************************************************/

/*
 * https://github.com/hftsoi/aabbtt_finalselection/blob/master/selection_mt_allyears.cc#L1578-L1602
 * sysToDo is trgeff_single or trgeff_cross
 * weight_nominal is already the single or cross one.
 */
ROOT::RVecF getSingleOrCrossTriggerSF_variations(std::string sysToDo, int passSingleTrigger, int passCrossTrigger,
                                                 float weight_nominal,
                                                 float weight_single_up, float weight_single_down,
                                                 float weight_cross_up,  float weight_cross_down) {

    // If the event passes the single trigger, and we are evaluating for the single trigger shift, return the up/down shift.
    // If the event passes the single trigger, and we are evaluating for the CROSS trigger shift, just apply the nominal single trigger weight.
    if ((sysToDo == "trgeff_single") && passSingleTrigger) {
        return ROOT::RVecF{weight_single_up, weight_single_down};
    }
    else if ((sysToDo == "trgeff_cross") && passCrossTrigger) {
        return ROOT::RVecF{weight_cross_up, weight_cross_down};
    }
    return ROOT::RVecF{weight_nominal, weight_nominal};
}

/**********************************************************/

/*
 * sysToDo == "trgeff_Mu8E23_em" or "trgeff_Mu23E12_em" or "trgeff_both_em"
 */
ROOT::RVecF getEMuCrossTriggerSF_variations(std::string sysToDo, int passLeadingEle, int passLeadingMu,
                                            float weight_nominal,
                                            float weight_leadingEle_up, float weight_leadingEle_down,
                                            float weight_leadingMuo_up, float weight_leadingMuo_down,
                                            float weight_both_up, float weight_both_down) {


    if ((sysToDo == "trgeff_Mu8E23_em") && (passLeadingEle && !(passLeadingMu))) {
        return ROOT::RVecF{weight_leadingEle_down, weight_leadingEle_up};
    }
    else if ((sysToDo == "trgeff_Mu23E12_em") && (!(passLeadingEle) && passLeadingMu)) {
        return ROOT::RVecF{weight_leadingMuo_down, weight_leadingMuo_up};
    }
    else if ((sysToDo == "trgeff_both_em")  && (passLeadingEle && passLeadingMu)) {
        return ROOT::RVec{weight_both_down, weight_both_up};
    }

    return ROOT::RVecF{weight_nominal, weight_nominal};
}

/**********************************************************/

// https://github.com/hftsoi/aabbtt_finalselection/blob/master/selection_mt_allyears.cc#L1530-L1578

//https://github.com/hftsoi/aabbtt_finalselection/blob/master/selection_et_allyears.cc#L1527-L1599

template <typename T>
auto GetTauTauTriggerEfficiencyShifts(T &df, LUNA::histoConfig_t &hConfig) {

    if (hConfig.doShifts() && (hConfig.isMC() || hConfig.isEmbedded())) {
        if (hConfig.doMuTau()) {
            return df.Define("trgeff_single", []() { return (std::string) "trgeff_single"; })
                     .Define("trgeff_cross",  []() { return (std::string) "trgeff_cross";  })
                     // n.b. The muon/tau energy scale shifts of the single trigger decision were already booked.
                     .Vary("weight_mt_trgsf_nominal", getSingleOrCrossTriggerSF_variations, {"trgeff_single", "passSingleTrigger_mt_nominal", "passCrossTrigger_mt_nominal", "weight_mt_trgsf_nominal",
                                                                                            "weight_mt_trgsf_trgeff_single_Up", "weight_mt_trgsf_trgeff_single_Down",
                                                                                            "weight_mt_trgsf_trgeff_cross_Up", "weight_mt_trgsf_trgeff_cross_Down"},
                                                                                            {"up", "down"}, "trgeff_single_mt")
                     .Vary("weight_mt_trgsf_nominal", getSingleOrCrossTriggerSF_variations, {"trgeff_cross", "passSingleTrigger_mt_nominal", "passCrossTrigger_mt_nominal", "weight_mt_trgsf_nominal",
                                                                                            "weight_mt_trgsf_trgeff_single_Up", "weight_mt_trgsf_trgeff_single_Down",
                                                                                            "weight_mt_trgsf_trgeff_cross_Up", "weight_mt_trgsf_trgeff_cross_Down"},
                                                                                            {"up", "down"}, "trgeff_cross_mt");
        }
        else if (hConfig.doETau()) {
            return df.Define("trgeff_single", []() { return (std::string) "trgeff_single"; })
                     .Define("trgeff_cross",  []() { return (std::string) "trgeff_cross";  })
                     // n.b. The muon/tau energy scale shifts of the single trigger decision were already booked.
                     .Vary("weight_et_trgsf_nominal", getSingleOrCrossTriggerSF_variations, {"trgeff_single", "passSingleTrigger_et_nominal", "passCrossTrigger_et_nominal", "weight_et_trgsf_nominal",
                                                                                            "weight_et_trgsf_trgeff_single_Up", "weight_et_trgsf_trgeff_single_Down",
                                                                                            "weight_et_trgsf_trgeff_cross_Up", "weight_et_trgsf_trgeff_cross_Down"},
                                                                                            {"up", "down"}, "trgeff_single_et")
                     .Vary("weight_et_trgsf_nominal", getSingleOrCrossTriggerSF_variations, {"trgeff_cross", "passSingleTrigger_et_nominal", "passCrossTrigger_et_nominal", "weight_et_trgsf_nominal",
                                                                                            "weight_et_trgsf_trgeff_single_Up", "weight_et_trgsf_trgeff_single_Down",
                                                                                            "weight_et_trgsf_trgeff_cross_Up", "weight_et_trgsf_trgeff_cross_Down"},
                                                                                            {"up", "down"}, "trgeff_cross_et");
        }
        else if (hConfig.doEMu()) {

            return df.Define("trgeff_Mu8E23_em",  [=]() { return (std::string) "trgeff_Mu8E23_em"; })
                     .Define("trgeff_Mu23E12_em", [=]() { return (std::string) "trgeff_Mu23E12_em"; })
                     .Define("trgeff_both_em",    [=]() { return (std::string) "trgeff_both_em"; })
                     .Vary("weight_em_trgsf_nominal", getEMuCrossTriggerSF_variations, {"trgeff_Mu8E23_em", "pass_em_HLT_Mu8Ele23", "pass_em_HLT_Mu23Ele12", "weight_em_trgsf_nominal",
                                                                                        "weight_em_trgsf_trgeff_Mu8E23_Up", "weight_em_trgsf_trgeff_Mu8E23_Down",
                                                                                        "weight_em_trgsf_trgeff_Mu23E12_Up", "weight_em_trgsf_trgeff_Mu23E12_Down",
                                                                                        "weight_em_trgsf_trgeff_both_Up", "weight_em_trgsf_trgeff_both_Down"},
                                                                                        {"up", "down"}, "trgeff_Mu8E23_em")
                     .Vary("weight_em_trgsf_nominal", getEMuCrossTriggerSF_variations, {"trgeff_Mu23E12_em", "pass_em_HLT_Mu8Ele23", "pass_em_HLT_Mu23Ele12", "weight_em_trgsf_nominal",
                                                                                        "weight_em_trgsf_trgeff_Mu8E23_Up", "weight_em_trgsf_trgeff_Mu8E23_Down",
                                                                                        "weight_em_trgsf_trgeff_Mu23E12_Up", "weight_em_trgsf_trgeff_Mu23E12_Down",
                                                                                        "weight_em_trgsf_trgeff_both_Up", "weight_em_trgsf_trgeff_both_Down"},
                                                                                        {"up", "down"}, "trgeff_Mu23E12_em")
                    .Vary("weight_em_trgsf_nominal", getEMuCrossTriggerSF_variations, {"trgeff_both_em", "pass_em_HLT_Mu8Ele23", "pass_em_HLT_Mu23Ele12", "weight_em_trgsf_nominal",
                                                                                        "weight_em_trgsf_trgeff_Mu8E23_Up", "weight_em_trgsf_trgeff_Mu8E23_Down",
                                                                                        "weight_em_trgsf_trgeff_Mu23E12_Up", "weight_em_trgsf_trgeff_Mu23E12_Down",
                                                                                        "weight_em_trgsf_trgeff_both_Up", "weight_em_trgsf_trgeff_both_Down"},
                                                                                        {"up", "down"}, "trgeff_both_em");
        }
    }
    return df;

}


/**********************************************************/

#endif
