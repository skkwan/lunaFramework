// btagEffScaleFactors-histo.h
// Handle b-tagging efficiency scale factors.

#ifndef B_TAG_EFF_SCALE_FACTORS_HISTO_H_INCL
#define B_TAG_EFF_SCALE_FACTORS_HISTO_H_INCL

#include "histoConfig_class.h"

/*******************************************************************/

/*
 * Get b-tag efficiency scale factor weight shifts, of the weight itself. Shifts due to the JER systematics are handled in the JER shift .h file.
 */

template <typename T>
auto GetBTagEfficiencyWeightShifts(T &df, LUNA::histoConfig_t &hConfig) {

  if (hConfig.isMC()) {
    if (hConfig.doShifts()) {
      return df.Vary("weight_btagEff_nominal", "ROOT::RVecF{weight_btagEff_btagsf_hf_Up,       weight_btagEff_btagsf_hf_Down}", {"up", "down"}, "btagsf_hf")
               .Vary("weight_btagEff_nominal", "ROOT::RVecF{weight_btagEff_btagsf_lf_Up,       weight_btagEff_btagsf_lf_Down}", {"up", "down"}, "btagsf_lf")
               .Vary("weight_btagEff_nominal", "ROOT::RVecF{weight_btagEff_btagsf_hfstats1_Up, weight_btagEff_btagsf_hfstats1_Down}", {"up", "down"}, "btagsf_hfstats1")
               .Vary("weight_btagEff_nominal", "ROOT::RVecF{weight_btagEff_btagsf_hfstats2_Up, weight_btagEff_btagsf_hfstats2_Down}", {"up", "down"}, "btagsf_hfstats2")
               .Vary("weight_btagEff_nominal", "ROOT::RVecF{weight_btagEff_btagsf_lfstats1_Up, weight_btagEff_btagsf_lfstats1_Down}", {"up", "down"}, "btagsf_lfstats1")
               .Vary("weight_btagEff_nominal", "ROOT::RVecF{weight_btagEff_btagsf_lfstats2_Up, weight_btagEff_btagsf_lfstats2_Down}", {"up", "down"}, "btagsf_lfstats2")
               .Vary("weight_btagEff_nominal", "ROOT::RVecF{weight_btagEff_btagsf_cferr1_Up,   weight_btagEff_btagsf_cferr1_Down}", {"up", "down"}, "btagsf_cferr1")
               .Vary("weight_btagEff_nominal", "ROOT::RVecF{weight_btagEff_btagsf_cferr2_Up,   weight_btagEff_btagsf_cferr2_Down}", {"up", "down"}, "btagsf_cferr2");
    }
  }
  return df;
}

/*******************************************************************/

#endif
