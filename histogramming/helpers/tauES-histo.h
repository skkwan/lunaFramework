// Helper functions for tau energy scale and e/mu faking tauh energy scales.

#ifndef TAU_ES_HISTO_H_INCL
#define TAU_ES_HISTO_H_INCL

#include "computePhysicsVariables.h"
#include "fileIO.h"
#include "helperFunctions.h"
#include "histoConfig_class.h"

#include "TFile.h"
#include "TH1F.h"
#include "TObject.h"
#include "Math/Vector4D.h"

#include <regex>

/******************************************************************************/

ROOT::RVecF getTauEnergyScale_variations(std::string sysToDo, int channel, int tau_dm, unsigned int tau_genMatch,
                                float val_nominal, float val_up, float val_down) {
  ROOT::RVecF variations = {val_nominal, val_nominal};
  if (channel == Helper::mt || channel == Helper::et) {
    // genuine tau
    if      ((tau_genMatch == 5) && (tau_dm == 0) && (sysToDo == "tauES_dm0"))   { variations = {val_up, val_down}; }
    else if ((tau_genMatch == 5) && (tau_dm == 1) && (sysToDo == "tauES_dm1"))   { variations = {val_up, val_down}; }
    else if ((tau_genMatch == 5) && (tau_dm == 10) && (sysToDo == "tauES_dm10")) { variations = {val_up, val_down}; }
    else if ((tau_genMatch == 5) && (tau_dm == 11) && (sysToDo == "tauES_dm11")) { variations = {val_up, val_down}; }

    // electron faking tau ES
    if      (((tau_genMatch == 1) || (tau_genMatch == 3)) && (tau_dm == 0) &&  (sysToDo == "eleTES_dm0"))  { variations = {val_up, val_down}; }
    else if (((tau_genMatch == 1) || (tau_genMatch == 3)) && (tau_dm == 1) &&  (sysToDo == "eleTES_dm1"))  { variations = {val_up, val_down}; }

    // muon faking tau ES
    if      (((tau_genMatch == 2) || (tau_genMatch == 4)) && (tau_dm == 0) &&  (sysToDo == "muTES_dm0"))  { variations = {val_up, val_down}; }
    else if (((tau_genMatch == 2) || (tau_genMatch == 4)) && (tau_dm == 1) &&  (sysToDo == "muTES_dm1"))  { variations = {val_up, val_down}; }

  }
  return variations;
}

/******************************************************************************/

ROOT::RVecI getTauEnergyScale_variations_int(std::string sysToDo, int channel, int tau_dm, unsigned int tau_genMatch,
                                int val_nominal, int val_up, int val_down) {
  ROOT::RVecI variations = {val_nominal, val_nominal};
  if ((channel == Helper::mt || channel == Helper::et)) {
    // genuine tau
    if      ((tau_genMatch == 5) && (tau_dm == 0) && (sysToDo == "tauES_dm0"))   { variations = {val_up, val_down}; }
    else if ((tau_genMatch == 5) && (tau_dm == 1) && (sysToDo == "tauES_dm1"))   { variations = {val_up, val_down}; }
    else if ((tau_genMatch == 5) && (tau_dm == 10) && (sysToDo == "tauES_dm10")) { variations = {val_up, val_down}; }
    else if ((tau_genMatch == 5) && (tau_dm == 11) && (sysToDo == "tauES_dm11")) { variations = {val_up, val_down}; }

    // electron faking tau ES
    if      (((tau_genMatch == 1) || (tau_genMatch == 3)) && (tau_dm == 0)  && (sysToDo == "eleTES_dm0"))  { variations = {val_up, val_down}; }
    else if (((tau_genMatch == 1) || (tau_genMatch == 3)) && (tau_dm == 1)  && (sysToDo == "eleTES_dm1"))  { variations = {val_up, val_down}; }

    // muon faking tau ES
    if      (((tau_genMatch == 2) || (tau_genMatch == 4)) && (tau_dm == 0)  && (sysToDo == "muTES_dm0"))  { variations = {val_up, val_down}; }
    else if (((tau_genMatch == 2) || (tau_genMatch == 4)) && (tau_dm == 1)  && (sysToDo == "muTES_dm1"))  { variations = {val_up, val_down}; }


  }
  return variations;
}
/******************************************************************************/

// Get the tau energy scales, mu->tau energy scales, and e->tau energy scales.
// Defines new pT branches (shifts will be aliased to pt_2 later on).
// Assumes that the nominal correction was already applied.

// To evaluate up/down shifts in the tau energy scale, we only need to consider the MET central values.
// For the MET shifts we need to have aliased them already.

template <typename T>
auto GetTauEnergyScaleShifts(T &df, LUNA::histoConfig_t &hConfig){

  std::cout << ">>> tauES-histo.h: GetTauEnergyScaleShifts: doShifts is " << hConfig.doShifts() << std::endl;

  if (hConfig.isMC() || hConfig.isEmbedded()) {
    if (hConfig.doShifts()) {
      auto df2 = df.Define("tauES_dm0", [=]() { return (std::string) "tauES_dm0"; })
                   .Define("tauES_dm1", [=]() { return (std::string) "tauES_dm1"; })
                   .Define("tauES_dm10", [=]() { return (std::string) "tauES_dm10"; })
                   .Define("tauES_dm11", [=]() { return (std::string) "tauES_dm11"; })

                   .Define("eleTES_dm0", [=]()  { return (std::string) "eleTES_dm0"; })
                   .Define("eleTES_dm1", [=]()  { return (std::string) "eleTES_dm1"; })

                   .Define("muTES_dm0", [=]()  { return (std::string) "muTES_dm0"; })
                   .Define("muTES_dm1", [=]()  { return (std::string) "muTES_dm1"; });

      if (hConfig.doMuTau() || hConfig.doETau()) {
        // Tau energy scale variations
        auto df3 = df2.Vary("pt_2_nominal", getTauEnergyScale_variations, {"tauES_dm0", "channel", "decayMode_2", "gen_match_2", "pt_2_nominal", "pt_2_es2Up", "pt_2_es2Down"}, {"up", "down"}, "tauES_dm0")
                      .Vary("pt_2_nominal", getTauEnergyScale_variations, {"tauES_dm1", "channel", "decayMode_2", "gen_match_2", "pt_2_nominal", "pt_2_es2Up", "pt_2_es2Down"}, {"up", "down"}, "tauES_dm1")
                      .Vary("pt_2_nominal", getTauEnergyScale_variations, {"tauES_dm10", "channel", "decayMode_2", "gen_match_2", "pt_2_nominal", "pt_2_es2Up", "pt_2_es2Down"}, {"up", "down"}, "tauES_dm10")
                      .Vary("pt_2_nominal", getTauEnergyScale_variations, {"tauES_dm11", "channel", "decayMode_2", "gen_match_2", "pt_2_nominal", "pt_2_es2Up", "pt_2_es2Down"}, {"up", "down"}, "tauES_dm11")

                      .Vary("m_2_nominal", getTauEnergyScale_variations, {"tauES_dm0", "channel", "decayMode_2", "gen_match_2", "m_2_nominal", "m_2_es2Up", "m_2_es2Down"}, {"up", "down"}, "tauES_dm0")
                      .Vary("m_2_nominal", getTauEnergyScale_variations, {"tauES_dm1", "channel", "decayMode_2", "gen_match_2", "m_2_nominal", "m_2_es2Up", "m_2_es2Down"}, {"up", "down"}, "tauES_dm1")
                      .Vary("m_2_nominal", getTauEnergyScale_variations, {"tauES_dm10", "channel", "decayMode_2", "gen_match_2", "m_2_nominal", "m_2_es2Up", "m_2_es2Down"}, {"up", "down"}, "tauES_dm10")
                      .Vary("m_2_nominal", getTauEnergyScale_variations, {"tauES_dm11", "channel", "decayMode_2", "gen_match_2", "m_2_nominal", "m_2_es2Up", "m_2_es2Down"}, {"up", "down"}, "tauES_dm11")

                      .Vary("met_nominal", getTauEnergyScale_variations, {"tauES_dm0", "channel", "decayMode_2", "gen_match_2", "met_nominal", "met_es2Up", "met_es2Down"}, {"up", "down"}, "tauES_dm0")
                      .Vary("met_nominal", getTauEnergyScale_variations, {"tauES_dm1", "channel", "decayMode_2", "gen_match_2", "met_nominal", "met_es2Up", "met_es2Down"}, {"up", "down"}, "tauES_dm1")
                      .Vary("met_nominal", getTauEnergyScale_variations, {"tauES_dm10", "channel", "decayMode_2", "gen_match_2", "met_nominal", "met_es2Up", "met_es2Down"}, {"up", "down"}, "tauES_dm10")
                      .Vary("met_nominal", getTauEnergyScale_variations, {"tauES_dm11", "channel", "decayMode_2", "gen_match_2", "met_nominal", "met_es2Up", "met_es2Down"}, {"up", "down"}, "tauES_dm11")

                      .Vary("metphi_nominal", getTauEnergyScale_variations, {"tauES_dm0", "channel", "decayMode_2", "gen_match_2", "metphi_nominal", "metphi_es2Up", "metphi_es2Down"}, {"up", "down"}, "tauES_dm0")
                      .Vary("metphi_nominal", getTauEnergyScale_variations, {"tauES_dm1", "channel", "decayMode_2", "gen_match_2", "metphi_nominal", "metphi_es2Up", "metphi_es2Down"}, {"up", "down"}, "tauES_dm1")
                      .Vary("metphi_nominal", getTauEnergyScale_variations, {"tauES_dm10", "channel", "decayMode_2", "gen_match_2", "metphi_nominal", "metphi_es2Up", "metphi_es2Down"}, {"up", "down"}, "tauES_dm10")
                      .Vary("metphi_nominal", getTauEnergyScale_variations, {"tauES_dm11", "channel", "decayMode_2", "gen_match_2", "metphi_nominal", "metphi_es2Up", "metphi_es2Down"}, {"up", "down"}, "tauES_dm11")

                      // SVFit mass if available (will be the same as m_vis if SVFit was not run)
                      .Vary("m_tt_nominal", getTauEnergyScale_variations, {"tauES_dm0", "channel", "decayMode_2", "gen_match_2", "m_tt_nominal", "m_tt_es2Up", "m_tt_es2Down"}, {"up", "down"}, "tauES_dm0")
                      .Vary("m_tt_nominal", getTauEnergyScale_variations, {"tauES_dm1", "channel", "decayMode_2", "gen_match_2", "m_tt_nominal", "m_tt_es2Up", "m_tt_es2Down"}, {"up", "down"}, "tauES_dm1")
                      .Vary("m_tt_nominal", getTauEnergyScale_variations, {"tauES_dm10", "channel", "decayMode_2", "gen_match_2", "m_tt_nominal", "m_tt_es2Up", "m_tt_es2Down"}, {"up", "down"}, "tauES_dm10")
                      .Vary("m_tt_nominal", getTauEnergyScale_variations, {"tauES_dm11", "channel", "decayMode_2", "gen_match_2", "m_tt_nominal", "m_tt_es2Up", "m_tt_es2Down"}, {"up", "down"}, "tauES_dm11")

                      .Vary("pt_tt_nominal", getTauEnergyScale_variations, {"tauES_dm0", "channel", "decayMode_2", "gen_match_2", "pt_tt_nominal", "pt_tt_es2Up", "pt_tt_es2Down"}, {"up", "down"}, "tauES_dm0")
                      .Vary("pt_tt_nominal", getTauEnergyScale_variations, {"tauES_dm1", "channel", "decayMode_2", "gen_match_2", "pt_tt_nominal", "pt_tt_es2Up", "pt_tt_es2Down"}, {"up", "down"}, "tauES_dm1")
                      .Vary("pt_tt_nominal", getTauEnergyScale_variations, {"tauES_dm10", "channel", "decayMode_2", "gen_match_2", "pt_tt_nominal", "pt_tt_es2Up", "pt_tt_es2Down"}, {"up", "down"}, "tauES_dm10")
                      .Vary("pt_tt_nominal", getTauEnergyScale_variations, {"tauES_dm11", "channel", "decayMode_2", "gen_match_2", "pt_tt_nominal", "pt_tt_es2Up", "pt_tt_es2Down"}, {"up", "down"}, "tauES_dm11")

                      // Visible mass
                      .Vary("m_vis_nominal", getTauEnergyScale_variations, {"tauES_dm0", "channel", "decayMode_2", "gen_match_2", "m_vis_nominal", "m_vis_es2Up", "m_vis_es2Down"}, {"up", "down"}, "tauES_dm0")
                      .Vary("m_vis_nominal", getTauEnergyScale_variations, {"tauES_dm1", "channel", "decayMode_2", "gen_match_2", "m_vis_nominal", "m_vis_es2Up", "m_vis_es2Down"}, {"up", "down"}, "tauES_dm1")
                      .Vary("m_vis_nominal", getTauEnergyScale_variations, {"tauES_dm10", "channel", "decayMode_2", "gen_match_2", "m_vis_nominal", "m_vis_es2Up", "m_vis_es2Down"}, {"up", "down"}, "tauES_dm10")
                      .Vary("m_vis_nominal", getTauEnergyScale_variations, {"tauES_dm11", "channel", "decayMode_2", "gen_match_2", "m_vis_nominal", "m_vis_es2Up", "m_vis_es2Down"}, {"up", "down"}, "tauES_dm11")

                      .Vary("pt_vis_nominal", getTauEnergyScale_variations, {"tauES_dm0", "channel", "decayMode_2", "gen_match_2", "pt_vis_nominal", "pt_vis_es2Up", "pt_vis_es2Down"}, {"up", "down"}, "tauES_dm0")
                      .Vary("pt_vis_nominal", getTauEnergyScale_variations, {"tauES_dm1", "channel", "decayMode_2", "gen_match_2", "pt_vis_nominal", "pt_vis_es2Up", "pt_vis_es2Down"}, {"up", "down"}, "tauES_dm1")
                      .Vary("pt_vis_nominal", getTauEnergyScale_variations, {"tauES_dm10", "channel", "decayMode_2", "gen_match_2", "pt_vis_nominal", "pt_vis_es2Up", "pt_vis_es2Down"}, {"up", "down"}, "tauES_dm10")
                      .Vary("pt_vis_nominal", getTauEnergyScale_variations, {"tauES_dm11", "channel", "decayMode_2", "gen_match_2", "pt_vis_nominal", "pt_vis_es2Up", "pt_vis_es2Down"}, {"up", "down"}, "tauES_dm11")


                      // cut-based categorization variables
                      .Vary("m_btautau_vis_nominal", getTauEnergyScale_variations, {"tauES_dm0",  "channel", "decayMode_2", "gen_match_2", "m_btautau_vis_nominal", "m_btautau_vis_es2Up", "m_btautau_vis_es2Down"}, {"up", "down"}, "tauES_dm0")
                      .Vary("m_btautau_vis_nominal", getTauEnergyScale_variations, {"tauES_dm1",  "channel", "decayMode_2", "gen_match_2", "m_btautau_vis_nominal", "m_btautau_vis_es2Up", "m_btautau_vis_es2Down"}, {"up", "down"}, "tauES_dm1")
                      .Vary("m_btautau_vis_nominal", getTauEnergyScale_variations, {"tauES_dm10", "channel", "decayMode_2", "gen_match_2", "m_btautau_vis_nominal", "m_btautau_vis_es2Up", "m_btautau_vis_es2Down"}, {"up", "down"}, "tauES_dm10")
                      .Vary("m_btautau_vis_nominal", getTauEnergyScale_variations, {"tauES_dm11", "channel", "decayMode_2", "gen_match_2", "m_btautau_vis_nominal", "m_btautau_vis_es2Up", "m_btautau_vis_es2Down"}, {"up", "down"}, "tauES_dm11")

                      .Vary("pt_btautau_vis_nominal", getTauEnergyScale_variations, {"tauES_dm0",  "channel", "decayMode_2", "gen_match_2", "pt_btautau_vis_nominal", "pt_btautau_vis_es2Up", "pt_btautau_vis_es2Down"}, {"up", "down"}, "tauES_dm0")
                      .Vary("pt_btautau_vis_nominal", getTauEnergyScale_variations, {"tauES_dm1",  "channel", "decayMode_2", "gen_match_2", "pt_btautau_vis_nominal", "pt_btautau_vis_es2Up", "pt_btautau_vis_es2Down"}, {"up", "down"}, "tauES_dm1")
                      .Vary("pt_btautau_vis_nominal", getTauEnergyScale_variations, {"tauES_dm10", "channel", "decayMode_2", "gen_match_2", "pt_btautau_vis_nominal", "pt_btautau_vis_es2Up", "pt_btautau_vis_es2Down"}, {"up", "down"}, "tauES_dm10")
                      .Vary("pt_btautau_vis_nominal", getTauEnergyScale_variations, {"tauES_dm11", "channel", "decayMode_2", "gen_match_2", "pt_btautau_vis_nominal", "pt_btautau_vis_es2Up", "pt_btautau_vis_es2Down"}, {"up", "down"}, "tauES_dm11")

                      .Vary("D_zeta_nominal", getTauEnergyScale_variations, {"tauES_dm0",  "channel", "decayMode_2", "gen_match_2", "D_zeta_nominal", "D_zeta_es2Up", "D_zeta_es2Down"}, {"up", "down"}, "tauES_dm0")
                      .Vary("D_zeta_nominal", getTauEnergyScale_variations, {"tauES_dm1",  "channel", "decayMode_2", "gen_match_2", "D_zeta_nominal", "D_zeta_es2Up", "D_zeta_es2Down"}, {"up", "down"}, "tauES_dm1")
                      .Vary("D_zeta_nominal", getTauEnergyScale_variations, {"tauES_dm10", "channel", "decayMode_2", "gen_match_2", "D_zeta_nominal", "D_zeta_es2Up", "D_zeta_es2Down"}, {"up", "down"}, "tauES_dm10")
                      .Vary("D_zeta_nominal", getTauEnergyScale_variations, {"tauES_dm11", "channel", "decayMode_2", "gen_match_2", "D_zeta_nominal", "D_zeta_es2Up", "D_zeta_es2Down"}, {"up", "down"}, "tauES_dm11")

                      .Vary("mtMET_2_nominal", getTauEnergyScale_variations, {"tauES_dm0",  "channel", "decayMode_2", "gen_match_2", "mtMET_2_nominal", "mtMET_2_es2Up", "mtMET_2_es2Down"}, {"up", "down"}, "tauES_dm0")
                      .Vary("mtMET_2_nominal", getTauEnergyScale_variations, {"tauES_dm1",  "channel", "decayMode_2", "gen_match_2", "mtMET_2_nominal", "mtMET_2_es2Up", "mtMET_2_es2Down"}, {"up", "down"}, "tauES_dm1")
                      .Vary("mtMET_2_nominal", getTauEnergyScale_variations, {"tauES_dm10", "channel", "decayMode_2", "gen_match_2", "mtMET_2_nominal", "mtMET_2_es2Up", "mtMET_2_es2Down"}, {"up", "down"}, "tauES_dm10")
                      .Vary("mtMET_2_nominal", getTauEnergyScale_variations, {"tauES_dm11", "channel", "decayMode_2", "gen_match_2", "mtMET_2_nominal", "mtMET_2_es2Up", "mtMET_2_es2Down"}, {"up", "down"}, "tauES_dm11")

                      // weights
                      .Vary("weight_tauID_nominal", getTauEnergyScale_variations, {"tauES_dm0", "channel", "decayMode_2", "gen_match_2", "weight_tauID_nominal", "weight_tauID_es2Up", "weight_tauID_es2Down"}, {"up", "down"}, "tauES_dm0")
                      .Vary("weight_tauID_nominal", getTauEnergyScale_variations, {"tauES_dm1", "channel", "decayMode_2", "gen_match_2", "weight_tauID_nominal", "weight_tauID_es2Up", "weight_tauID_es2Down"}, {"up", "down"}, "tauES_dm1")
                      .Vary("weight_tauID_nominal", getTauEnergyScale_variations, {"tauES_dm10", "channel", "decayMode_2", "gen_match_2", "weight_tauID_nominal", "weight_tauID_es2Up", "weight_tauID_es2Down"}, {"up", "down"}, "tauES_dm10")
                      .Vary("weight_tauID_nominal", getTauEnergyScale_variations, {"tauES_dm11", "channel", "decayMode_2", "gen_match_2", "weight_tauID_nominal", "weight_tauID_es2Up", "weight_tauID_es2Down"}, {"up", "down"}, "tauES_dm11")

                      .Vary("weight_ff_nominal", getTauEnergyScale_variations, {"tauES_dm0", "channel", "decayMode_2", "gen_match_2", "weight_ff_nominal", "weight_ff_es2Up", "weight_ff_es2Down"}, {"up", "down"}, "tauES_dm0")
                      .Vary("weight_ff_nominal", getTauEnergyScale_variations, {"tauES_dm1", "channel", "decayMode_2", "gen_match_2", "weight_ff_nominal", "weight_ff_es2Up", "weight_ff_es2Down"}, {"up", "down"}, "tauES_dm1")
                      .Vary("weight_ff_nominal", getTauEnergyScale_variations, {"tauES_dm10", "channel", "decayMode_2", "gen_match_2", "weight_ff_nominal", "weight_ff_es2Up", "weight_ff_es2Down"}, {"up", "down"}, "tauES_dm10")
                      .Vary("weight_ff_nominal", getTauEnergyScale_variations, {"tauES_dm11", "channel", "decayMode_2", "gen_match_2", "weight_ff_nominal", "weight_ff_es2Up", "weight_ff_es2Down"}, {"up", "down"}, "tauES_dm11")

        // electron faking tau energy scale
                      .Vary("pt_2_nominal", getTauEnergyScale_variations, {"eleTES_dm0", "channel", "decayMode_2", "gen_match_2", "pt_2_nominal", "pt_2_es2Up", "pt_2_es2Down"}, {"up", "down"},  "eleTES_dm0")
                      .Vary("pt_2_nominal", getTauEnergyScale_variations, {"eleTES_dm1", "channel", "decayMode_2", "gen_match_2", "pt_2_nominal", "pt_2_es2Up", "pt_2_es2Down"}, {"up", "down"},  "eleTES_dm1")

                      .Vary("m_2_nominal", getTauEnergyScale_variations, {"eleTES_dm0", "channel", "decayMode_2", "gen_match_2", "m_2_nominal", "m_2_es2Up", "m_2_es2Down"}, {"up", "down"}, "eleTES_dm0")
                      .Vary("m_2_nominal", getTauEnergyScale_variations, {"eleTES_dm1", "channel", "decayMode_2", "gen_match_2", "m_2_nominal", "m_2_es2Up", "m_2_es2Down"}, {"up", "down"}, "eleTES_dm1")

                      .Vary("met_nominal", getTauEnergyScale_variations, {"eleTES_dm0", "channel", "decayMode_2", "gen_match_2", "met_nominal", "met_es2Up", "met_es2Down"}, {"up", "down"}, "eleTES_dm0")
                      .Vary("met_nominal", getTauEnergyScale_variations, {"eleTES_dm1", "channel", "decayMode_2", "gen_match_2", "met_nominal", "met_es2Up", "met_es2Down"}, {"up", "down"}, "eleTES_dm1")

                      .Vary("metphi_nominal", getTauEnergyScale_variations, {"eleTES_dm0", "channel", "decayMode_2", "gen_match_2", "metphi_nominal", "metphi_es2Up", "metphi_es2Down"}, {"up", "down"}, "eleTES_dm0")
                      .Vary("metphi_nominal", getTauEnergyScale_variations, {"eleTES_dm1", "channel", "decayMode_2", "gen_match_2", "metphi_nominal", "metphi_es2Up", "metphi_es2Down"}, {"up", "down"}, "eleTES_dm1")

                      // SVFit mass if available (otherwise identical to visible mass)
                      .Vary("m_tt_nominal", getTauEnergyScale_variations, {"eleTES_dm0", "channel", "decayMode_2", "gen_match_2", "m_tt_nominal", "m_tt_es2Up", "m_tt_es2Down"}, {"up", "down"}, "eleTES_dm0")
                      .Vary("m_tt_nominal", getTauEnergyScale_variations, {"eleTES_dm1", "channel", "decayMode_2", "gen_match_2", "m_tt_nominal", "m_tt_es2Up", "m_tt_es2Down"}, {"up", "down"}, "eleTES_dm1")
                      .Vary("pt_tt_nominal", getTauEnergyScale_variations, {"eleTES_dm0", "channel", "decayMode_2", "gen_match_2", "pt_tt_nominal", "pt_tt_es2Up", "pt_tt_es2Down"}, {"up", "down"}, "eleTES_dm0")
                      .Vary("pt_tt_nominal", getTauEnergyScale_variations, {"eleTES_dm1", "channel", "decayMode_2", "gen_match_2", "pt_tt_nominal", "pt_tt_es2Up", "pt_tt_es2Down"}, {"up", "down"}, "eleTES_dm1")

                      // Visible di-tau mass
                      .Vary("m_vis_nominal", getTauEnergyScale_variations, {"eleTES_dm0", "channel", "decayMode_2", "gen_match_2", "m_vis_nominal", "m_vis_es2Up", "m_vis_es2Down"}, {"up", "down"}, "eleTES_dm0")
                      .Vary("m_vis_nominal", getTauEnergyScale_variations, {"eleTES_dm1", "channel", "decayMode_2", "gen_match_2", "m_vis_nominal", "m_vis_es2Up", "m_vis_es2Down"}, {"up", "down"}, "eleTES_dm1")
                      .Vary("pt_vis_nominal", getTauEnergyScale_variations, {"eleTES_dm0", "channel", "decayMode_2", "gen_match_2", "pt_vis_nominal", "pt_vis_es2Up", "pt_vis_es2Down"}, {"up", "down"}, "eleTES_dm0")
                      .Vary("pt_vis_nominal", getTauEnergyScale_variations, {"eleTES_dm1", "channel", "decayMode_2", "gen_match_2", "pt_vis_nominal", "pt_vis_es2Up", "pt_vis_es2Down"}, {"up", "down"}, "eleTES_dm1")

                      // cut-based categorization variables
                      .Vary("m_btautau_vis_nominal", getTauEnergyScale_variations, {"eleTES_dm0",  "channel", "decayMode_2", "gen_match_2", "m_btautau_vis_nominal", "m_btautau_vis_es2Up", "m_btautau_vis_es2Down"}, {"up", "down"}, "eleTES_dm0")
                      .Vary("m_btautau_vis_nominal", getTauEnergyScale_variations, {"eleTES_dm1",  "channel", "decayMode_2", "gen_match_2", "m_btautau_vis_nominal", "m_btautau_vis_es2Up", "m_btautau_vis_es2Down"}, {"up", "down"}, "eleTES_dm1")

                      .Vary("pt_btautau_vis_nominal", getTauEnergyScale_variations, {"eleTES_dm0",  "channel", "decayMode_2", "gen_match_2", "pt_btautau_vis_nominal", "pt_btautau_vis_es2Up", "pt_btautau_vis_es2Down"}, {"up", "down"}, "eleTES_dm0")
                      .Vary("pt_btautau_vis_nominal", getTauEnergyScale_variations, {"eleTES_dm1",  "channel", "decayMode_2", "gen_match_2", "pt_btautau_vis_nominal", "pt_btautau_vis_es2Up", "pt_btautau_vis_es2Down"}, {"up", "down"}, "eleTES_dm1")

                      .Vary("D_zeta_nominal", getTauEnergyScale_variations, {"eleTES_dm0",  "channel", "decayMode_2", "gen_match_2", "D_zeta_nominal", "D_zeta_es2Up", "D_zeta_es2Down"}, {"up", "down"}, "eleTES_dm0")
                      .Vary("D_zeta_nominal", getTauEnergyScale_variations, {"eleTES_dm1",  "channel", "decayMode_2", "gen_match_2", "D_zeta_nominal", "D_zeta_es2Up", "D_zeta_es2Down"}, {"up", "down"}, "eleTES_dm1")

                      .Vary("mtMET_2_nominal", getTauEnergyScale_variations, {"eleTES_dm0",  "channel", "decayMode_2", "gen_match_2", "mtMET_2_nominal", "mtMET_2_es2Up", "mtMET_2_es2Down"}, {"up", "down"}, "eleTES_dm0")
                      .Vary("mtMET_2_nominal", getTauEnergyScale_variations, {"eleTES_dm1",  "channel", "decayMode_2", "gen_match_2", "mtMET_2_nominal", "mtMET_2_es2Up", "mtMET_2_es2Down"}, {"up", "down"}, "eleTES_dm1")

                      // weights
                      .Vary("weight_tauID_nominal", getTauEnergyScale_variations, {"eleTES_dm0", "channel", "decayMode_2", "gen_match_2", "weight_tauID_nominal", "weight_tauID_es2Up", "weight_tauID_es2Down"}, {"up", "down"}, "eleTES_dm0")
                      .Vary("weight_tauID_nominal", getTauEnergyScale_variations, {"eleTES_dm1", "channel", "decayMode_2", "gen_match_2", "weight_tauID_nominal", "weight_tauID_es2Up", "weight_tauID_es2Down"}, {"up", "down"}, "eleTES_dm1")

                      .Vary("weight_ff_nominal", getTauEnergyScale_variations, {"eleTES_dm0", "channel", "decayMode_2", "gen_match_2", "weight_ff_nominal", "weight_ff_es2Up", "weight_ff_es2Down"}, {"up", "down"}, "eleTES_dm0")
                      .Vary("weight_ff_nominal", getTauEnergyScale_variations, {"eleTES_dm1", "channel", "decayMode_2", "gen_match_2", "weight_ff_nominal", "weight_ff_es2Up", "weight_ff_es2Down"}, {"up", "down"}, "eleTES_dm1")

        // muon faking tau energy scale
                      .Vary("pt_2_nominal", getTauEnergyScale_variations, {"muTES_dm0", "channel", "decayMode_2", "gen_match_2", "pt_2_nominal", "pt_2_es2Up", "pt_2_es2Down"}, {"up", "down"},  "muTES_dm0")
                      .Vary("pt_2_nominal", getTauEnergyScale_variations, {"muTES_dm1", "channel", "decayMode_2", "gen_match_2", "pt_2_nominal", "pt_2_es2Up", "pt_2_es2Down"}, {"up", "down"},  "muTES_dm1")

                      .Vary("m_2_nominal", getTauEnergyScale_variations, {"muTES_dm0", "channel", "decayMode_2", "gen_match_2", "m_2_nominal", "m_2_es2Up", "m_2_es2Down"}, {"up", "down"}, "muTES_dm0")
                      .Vary("m_2_nominal", getTauEnergyScale_variations, {"muTES_dm1", "channel", "decayMode_2", "gen_match_2", "m_2_nominal", "m_2_es2Up", "m_2_es2Down"}, {"up", "down"}, "muTES_dm1")

                      .Vary("met_nominal", getTauEnergyScale_variations, {"muTES_dm0", "channel", "decayMode_2", "gen_match_2", "met_nominal", "met_es2Up", "met_es2Down"}, {"up", "down"}, "muTES_dm0")
                      .Vary("met_nominal", getTauEnergyScale_variations, {"muTES_dm1", "channel", "decayMode_2", "gen_match_2", "met_nominal", "met_es2Up", "met_es2Down"}, {"up", "down"}, "muTES_dm1")

                      .Vary("metphi_nominal", getTauEnergyScale_variations, {"muTES_dm0", "channel", "decayMode_2", "gen_match_2", "metphi_nominal", "metphi_es2Up", "metphi_es2Down"}, {"up", "down"}, "muTES_dm0")
                      .Vary("metphi_nominal", getTauEnergyScale_variations, {"muTES_dm1", "channel", "decayMode_2", "gen_match_2", "metphi_nominal", "metphi_es2Up", "metphi_es2Down"}, {"up", "down"}, "muTES_dm1")

                      // SVFit mass if available (otherwise identical to visible mass)
                      .Vary("m_tt_nominal", getTauEnergyScale_variations, {"muTES_dm0", "channel", "decayMode_2", "gen_match_2", "m_tt_nominal", "m_tt_es2Up", "m_tt_es2Down"}, {"up", "down"}, "muTES_dm0")
                      .Vary("m_tt_nominal", getTauEnergyScale_variations, {"muTES_dm1", "channel", "decayMode_2", "gen_match_2", "m_tt_nominal", "m_tt_es2Up", "m_tt_es2Down"}, {"up", "down"}, "muTES_dm1")
                      .Vary("pt_tt_nominal", getTauEnergyScale_variations, {"muTES_dm0", "channel", "decayMode_2", "gen_match_2", "pt_tt_nominal", "pt_tt_es2Up", "pt_tt_es2Down"}, {"up", "down"}, "muTES_dm0")
                      .Vary("pt_tt_nominal", getTauEnergyScale_variations, {"muTES_dm1", "channel", "decayMode_2", "gen_match_2", "pt_tt_nominal", "pt_tt_es2Up", "pt_tt_es2Down"}, {"up", "down"}, "muTES_dm1")

                      // visible mass
                      .Vary("m_vis_nominal", getTauEnergyScale_variations, {"muTES_dm0", "channel", "decayMode_2", "gen_match_2", "m_vis_nominal", "m_vis_es2Up", "m_vis_es2Down"}, {"up", "down"}, "muTES_dm0")
                      .Vary("m_vis_nominal", getTauEnergyScale_variations, {"muTES_dm1", "channel", "decayMode_2", "gen_match_2", "m_vis_nominal", "m_vis_es2Up", "m_vis_es2Down"}, {"up", "down"}, "muTES_dm1")
                      .Vary("pt_vis_nominal", getTauEnergyScale_variations, {"muTES_dm0", "channel", "decayMode_2", "gen_match_2", "pt_vis_nominal", "pt_vis_es2Up", "pt_vis_es2Down"}, {"up", "down"}, "muTES_dm0")
                      .Vary("pt_vis_nominal", getTauEnergyScale_variations, {"muTES_dm1", "channel", "decayMode_2", "gen_match_2", "pt_vis_nominal", "pt_vis_es2Up", "pt_vis_es2Down"}, {"up", "down"}, "muTES_dm1")

                      // cut-based categorization variables
                      .Vary("m_btautau_vis_nominal", getTauEnergyScale_variations, {"muTES_dm0",  "channel", "decayMode_2", "gen_match_2", "m_btautau_vis_nominal", "m_btautau_vis_es2Up", "m_btautau_vis_es2Down"}, {"up", "down"}, "muTES_dm0")
                      .Vary("m_btautau_vis_nominal", getTauEnergyScale_variations, {"muTES_dm1",  "channel", "decayMode_2", "gen_match_2", "m_btautau_vis_nominal", "m_btautau_vis_es2Up", "m_btautau_vis_es2Down"}, {"up", "down"}, "muTES_dm1")

                      .Vary("pt_btautau_vis_nominal", getTauEnergyScale_variations, {"muTES_dm0",  "channel", "decayMode_2", "gen_match_2", "pt_btautau_vis_nominal", "pt_btautau_vis_es2Up", "pt_btautau_vis_es2Down"}, {"up", "down"}, "muTES_dm0")
                      .Vary("pt_btautau_vis_nominal", getTauEnergyScale_variations, {"muTES_dm1",  "channel", "decayMode_2", "gen_match_2", "pt_btautau_vis_nominal", "pt_btautau_vis_es2Up", "pt_btautau_vis_es2Down"}, {"up", "down"}, "muTES_dm1")

                      .Vary("D_zeta_nominal", getTauEnergyScale_variations, {"muTES_dm0",  "channel", "decayMode_2", "gen_match_2", "D_zeta_nominal", "D_zeta_es2Up", "D_zeta_es2Down"}, {"up", "down"}, "muTES_dm0")
                      .Vary("D_zeta_nominal", getTauEnergyScale_variations, {"muTES_dm1",  "channel", "decayMode_2", "gen_match_2", "D_zeta_nominal", "D_zeta_es2Up", "D_zeta_es2Down"}, {"up", "down"}, "muTES_dm1")

                      .Vary("mtMET_2_nominal", getTauEnergyScale_variations, {"muTES_dm0",  "channel", "decayMode_2", "gen_match_2", "mtMET_2_nominal", "mtMET_2_es2Up", "mtMET_2_es2Down"}, {"up", "down"}, "muTES_dm0")
                      .Vary("mtMET_2_nominal", getTauEnergyScale_variations, {"muTES_dm1",  "channel", "decayMode_2", "gen_match_2", "mtMET_2_nominal", "mtMET_2_es2Up", "mtMET_2_es2Down"}, {"up", "down"}, "muTES_dm1")

                      // weights
                      .Vary("weight_tauID_nominal", getTauEnergyScale_variations, {"muTES_dm0", "channel", "decayMode_2", "gen_match_2", "weight_tauID_nominal", "weight_tauID_es2Up", "weight_tauID_es2Down"}, {"up", "down"}, "muTES_dm0")
                      .Vary("weight_tauID_nominal", getTauEnergyScale_variations, {"muTES_dm1", "channel", "decayMode_2", "gen_match_2", "weight_tauID_nominal", "weight_tauID_es2Up", "weight_tauID_es2Down"}, {"up", "down"}, "muTES_dm1")

                      .Vary("weight_ff_nominal", getTauEnergyScale_variations, {"muTES_dm0", "channel", "decayMode_2", "gen_match_2", "weight_ff_nominal", "weight_ff_es2Up", "weight_ff_es2Down"}, {"up", "down"}, "muTES_dm0")
                      .Vary("weight_ff_nominal", getTauEnergyScale_variations, {"muTES_dm1", "channel", "decayMode_2", "gen_match_2", "weight_ff_nominal", "weight_ff_es2Up", "weight_ff_es2Down"}, {"up", "down"}, "muTES_dm1");

        if (hConfig.doMuTau()) {

          // Every variable that depends on the tau (true or fake) energy scale needs to have a variation booked here
          // tau ES
          return df3.Vary("passSingleTrigger_mt_nominal", getTauEnergyScale_variations_int, {"tauES_dm0", "channel", "decayMode_2", "gen_match_2", "passSingleTrigger_mt_nominal", "passSingleTrigger_mt_es2Up", "passSingleTrigger_mt_es2Down"}, {"up", "down"}, "tauES_dm0")
                    .Vary("passSingleTrigger_mt_nominal", getTauEnergyScale_variations_int, {"tauES_dm1", "channel", "decayMode_2", "gen_match_2", "passSingleTrigger_mt_nominal", "passSingleTrigger_mt_es2Up", "passSingleTrigger_mt_es2Down"}, {"up", "down"}, "tauES_dm1")
                    .Vary("passSingleTrigger_mt_nominal", getTauEnergyScale_variations_int, {"tauES_dm10", "channel", "decayMode_2", "gen_match_2", "passSingleTrigger_mt_nominal", "passSingleTrigger_mt_es2Up", "passSingleTrigger_mt_es2Down"}, {"up", "down"}, "tauES_dm10")
                    .Vary("passSingleTrigger_mt_nominal", getTauEnergyScale_variations_int, {"tauES_dm11", "channel", "decayMode_2", "gen_match_2", "passSingleTrigger_mt_nominal", "passSingleTrigger_mt_es2Up", "passSingleTrigger_mt_es2Down"}, {"up", "down"}, "tauES_dm11")

                    .Vary("passCrossTrigger_mt_nominal", getTauEnergyScale_variations_int, {"tauES_dm0", "channel", "decayMode_2", "gen_match_2", "passCrossTrigger_mt_nominal", "passCrossTrigger_mt_es2Up", "passCrossTrigger_mt_es2Down"}, {"up", "down"}, "tauES_dm0")
                    .Vary("passCrossTrigger_mt_nominal", getTauEnergyScale_variations_int, {"tauES_dm1", "channel", "decayMode_2", "gen_match_2", "passCrossTrigger_mt_nominal", "passCrossTrigger_mt_es2Up", "passCrossTrigger_mt_es2Down"}, {"up", "down"}, "tauES_dm1")
                    .Vary("passCrossTrigger_mt_nominal", getTauEnergyScale_variations_int, {"tauES_dm10", "channel", "decayMode_2", "gen_match_2", "passCrossTrigger_mt_nominal", "passCrossTrigger_mt_es2Up", "passCrossTrigger_mt_es2Down"}, {"up", "down"}, "tauES_dm10")
                    .Vary("passCrossTrigger_mt_nominal", getTauEnergyScale_variations_int, {"tauES_dm11", "channel", "decayMode_2", "gen_match_2", "passCrossTrigger_mt_nominal", "passCrossTrigger_mt_es2Up", "passCrossTrigger_mt_es2Down"}, {"up", "down"}, "tauES_dm11")

                    .Vary("weight_mt_trgsf_nominal", getTauEnergyScale_variations, {"tauES_dm0", "channel", "decayMode_2", "gen_match_2",  "weight_mt_trgsf_nominal", "weight_mt_trgsf_es2Up", "weight_mt_trgsf_es2Down"}, {"up", "down"}, "tauES_dm0")
                    .Vary("weight_mt_trgsf_nominal", getTauEnergyScale_variations, {"tauES_dm1", "channel", "decayMode_2", "gen_match_2",  "weight_mt_trgsf_nominal", "weight_mt_trgsf_es2Up", "weight_mt_trgsf_es2Down"}, {"up", "down"}, "tauES_dm1")
                    .Vary("weight_mt_trgsf_nominal", getTauEnergyScale_variations, {"tauES_dm10", "channel", "decayMode_2", "gen_match_2", "weight_mt_trgsf_nominal", "weight_mt_trgsf_es2Up", "weight_mt_trgsf_es2Down"}, {"up", "down"}, "tauES_dm10")
                    .Vary("weight_mt_trgsf_nominal", getTauEnergyScale_variations, {"tauES_dm11", "channel", "decayMode_2", "gen_match_2", "weight_mt_trgsf_nominal", "weight_mt_trgsf_es2Up", "weight_mt_trgsf_es2Down"}, {"up", "down"}, "tauES_dm11")

         // electron faking tau ES
                    .Vary("passSingleTrigger_mt_nominal", getTauEnergyScale_variations_int, {"eleTES_dm0", "channel", "decayMode_2", "gen_match_2", "passSingleTrigger_mt_nominal", "passSingleTrigger_mt_es2Up", "passSingleTrigger_mt_es2Down"}, {"up", "down"},  "eleTES_dm0")
                    .Vary("passSingleTrigger_mt_nominal", getTauEnergyScale_variations_int, {"eleTES_dm1", "channel", "decayMode_2", "gen_match_2", "passSingleTrigger_mt_nominal", "passSingleTrigger_mt_es2Up", "passSingleTrigger_mt_es2Down"}, {"up", "down"},  "eleTES_dm1")

                    .Vary("passCrossTrigger_mt_nominal", getTauEnergyScale_variations_int, {"eleTES_dm0", "channel", "decayMode_2", "gen_match_2", "passCrossTrigger_mt_nominal", "passCrossTrigger_mt_es2Up", "passCrossTrigger_mt_es2Down"}, {"up", "down"},  "eleTES_dm0")
                    .Vary("passCrossTrigger_mt_nominal", getTauEnergyScale_variations_int, {"eleTES_dm1", "channel", "decayMode_2", "gen_match_2", "passCrossTrigger_mt_nominal", "passCrossTrigger_mt_es2Up", "passCrossTrigger_mt_es2Down"}, {"up", "down"},  "eleTES_dm1")

                    .Vary("weight_mt_trgsf_nominal", getTauEnergyScale_variations, {"eleTES_dm0", "channel", "decayMode_2", "gen_match_2",  "weight_mt_trgsf_nominal", "weight_mt_trgsf_es2Up", "weight_mt_trgsf_es2Down"}, {"up", "down"},  "eleTES_dm0")
                    .Vary("weight_mt_trgsf_nominal", getTauEnergyScale_variations, {"eleTES_dm1", "channel", "decayMode_2", "gen_match_2",  "weight_mt_trgsf_nominal", "weight_mt_trgsf_es2Up", "weight_mt_trgsf_es2Down"}, {"up", "down"},  "eleTES_dm1")

        // muon faking tau ES
                    .Vary("passSingleTrigger_mt_nominal", getTauEnergyScale_variations_int, {"muTES_dm0", "channel", "decayMode_2", "gen_match_2", "passSingleTrigger_mt_nominal", "passSingleTrigger_mt_es2Up", "passSingleTrigger_mt_es2Down"}, {"up", "down"},  "muTES_dm0")
                    .Vary("passSingleTrigger_mt_nominal", getTauEnergyScale_variations_int, {"muTES_dm1", "channel", "decayMode_2", "gen_match_2", "passSingleTrigger_mt_nominal", "passSingleTrigger_mt_es2Up", "passSingleTrigger_mt_es2Down"}, {"up", "down"},  "muTES_dm1")

                    .Vary("passCrossTrigger_mt_nominal", getTauEnergyScale_variations_int, {"muTES_dm0", "channel", "decayMode_2", "gen_match_2", "passCrossTrigger_mt_nominal", "passCrossTrigger_mt_es2Up", "passCrossTrigger_mt_es2Down"}, {"up", "down"},  "muTES_dm0")
                    .Vary("passCrossTrigger_mt_nominal", getTauEnergyScale_variations_int, {"muTES_dm1", "channel", "decayMode_2", "gen_match_2", "passCrossTrigger_mt_nominal", "passCrossTrigger_mt_es2Up", "passCrossTrigger_mt_es2Down"}, {"up", "down"},  "muTES_dm1")

                    .Vary("weight_mt_trgsf_nominal", getTauEnergyScale_variations, {"muTES_dm0", "channel", "decayMode_2", "gen_match_2", "weight_mt_trgsf_nominal", "weight_mt_trgsf_es2Up", "weight_mt_trgsf_es2Down"}, {"up", "down"},  "muTES_dm0")
                    .Vary("weight_mt_trgsf_nominal", getTauEnergyScale_variations, {"muTES_dm1", "channel", "decayMode_2", "gen_match_2", "weight_mt_trgsf_nominal", "weight_mt_trgsf_es2Up", "weight_mt_trgsf_es2Down"}, {"up", "down"},  "muTES_dm1");

        }
        else if (hConfig.doETau()) {

          // tau ES
          return df3.Vary("passSingleTrigger_et_nominal", getTauEnergyScale_variations_int, {"tauES_dm0", "channel", "decayMode_2", "gen_match_2", "passSingleTrigger_et_nominal", "passSingleTrigger_et_es2Up", "passSingleTrigger_et_es2Down"}, {"up", "down"}, "tauES_dm0")
                    .Vary("passSingleTrigger_et_nominal", getTauEnergyScale_variations_int, {"tauES_dm1", "channel", "decayMode_2", "gen_match_2", "passSingleTrigger_et_nominal", "passSingleTrigger_et_es2Up", "passSingleTrigger_et_es2Down"}, {"up", "down"}, "tauES_dm1")
                    .Vary("passSingleTrigger_et_nominal", getTauEnergyScale_variations_int, {"tauES_dm10", "channel", "decayMode_2", "gen_match_2", "passSingleTrigger_et_nominal", "passSingleTrigger_et_es2Up", "passSingleTrigger_et_es2Down"}, {"up", "down"}, "tauES_dm10")
                    .Vary("passSingleTrigger_et_nominal", getTauEnergyScale_variations_int, {"tauES_dm11", "channel", "decayMode_2", "gen_match_2", "passSingleTrigger_et_nominal", "passSingleTrigger_et_es2Up", "passSingleTrigger_et_es2Down"}, {"up", "down"}, "tauES_dm11")

                    .Vary("passCrossTrigger_et_nominal", getTauEnergyScale_variations_int, {"tauES_dm0", "channel", "decayMode_2", "gen_match_2", "passCrossTrigger_et_nominal", "passCrossTrigger_et_es2Up", "passCrossTrigger_et_es2Down"}, {"up", "down"}, "tauES_dm0")
                    .Vary("passCrossTrigger_et_nominal", getTauEnergyScale_variations_int, {"tauES_dm1", "channel", "decayMode_2", "gen_match_2", "passCrossTrigger_et_nominal", "passCrossTrigger_et_es2Up", "passCrossTrigger_et_es2Down"}, {"up", "down"}, "tauES_dm1")
                    .Vary("passCrossTrigger_et_nominal", getTauEnergyScale_variations_int, {"tauES_dm10", "channel", "decayMode_2", "gen_match_2", "passCrossTrigger_et_nominal", "passCrossTrigger_et_es2Up", "passCrossTrigger_et_es2Down"}, {"up", "down"}, "tauES_dm10")
                    .Vary("passCrossTrigger_et_nominal", getTauEnergyScale_variations_int, {"tauES_dm11", "channel", "decayMode_2", "gen_match_2", "passCrossTrigger_et_nominal", "passCrossTrigger_et_es2Up", "passCrossTrigger_et_es2Down"}, {"up", "down"}, "tauES_dm11")

                    .Vary("weight_et_trgsf_nominal", getTauEnergyScale_variations, {"tauES_dm0", "channel", "decayMode_2", "gen_match_2",  "weight_et_trgsf_nominal", "weight_et_trgsf_es2Up", "weight_et_trgsf_es2Down"}, {"up", "down"}, "tauES_dm0")
                    .Vary("weight_et_trgsf_nominal", getTauEnergyScale_variations, {"tauES_dm1", "channel", "decayMode_2", "gen_match_2",  "weight_et_trgsf_nominal", "weight_et_trgsf_es2Up", "weight_et_trgsf_es2Down"}, {"up", "down"}, "tauES_dm1")
                    .Vary("weight_et_trgsf_nominal", getTauEnergyScale_variations, {"tauES_dm10", "channel", "decayMode_2", "gen_match_2", "weight_et_trgsf_nominal", "weight_et_trgsf_es2Up", "weight_et_trgsf_es2Down"}, {"up", "down"}, "tauES_dm10")
                    .Vary("weight_et_trgsf_nominal", getTauEnergyScale_variations, {"tauES_dm11", "channel", "decayMode_2", "gen_match_2", "weight_et_trgsf_nominal", "weight_et_trgsf_es2Up", "weight_et_trgsf_es2Down"}, {"up", "down"}, "tauES_dm11")


          // ele faking tau ES
                    .Vary("passSingleTrigger_et_nominal", getTauEnergyScale_variations_int, {"eleTES_dm0", "channel", "decayMode_2", "gen_match_2", "passSingleTrigger_et_nominal", "passSingleTrigger_et_es2Up", "passSingleTrigger_et_es2Down"}, {"up", "down"}, "eleTES_dm0")
                    .Vary("passSingleTrigger_et_nominal", getTauEnergyScale_variations_int, {"eleTES_dm1", "channel", "decayMode_2", "gen_match_2", "passSingleTrigger_et_nominal", "passSingleTrigger_et_es2Up", "passSingleTrigger_et_es2Down"}, {"up", "down"}, "eleTES_dm1")

                    .Vary("passCrossTrigger_et_nominal", getTauEnergyScale_variations_int, {"eleTES_dm0", "channel", "decayMode_2", "gen_match_2", "passCrossTrigger_et_nominal", "passCrossTrigger_et_es2Up", "passCrossTrigger_et_es2Down"}, {"up", "down"}, "eleTES_dm0")
                    .Vary("passCrossTrigger_et_nominal", getTauEnergyScale_variations_int, {"eleTES_dm1", "channel", "decayMode_2", "gen_match_2", "passCrossTrigger_et_nominal", "passCrossTrigger_et_es2Up", "passCrossTrigger_et_es2Down"}, {"up", "down"}, "eleTES_dm1")

                    .Vary("weight_et_trgsf_nominal", getTauEnergyScale_variations, {"eleTES_dm0", "channel", "decayMode_2", "gen_match_2", "weight_et_trgsf_nominal", "weight_et_trgsf_es2Up", "weight_et_trgsf_es2Down"}, {"up", "down"}, "eleTES_dm0")
                    .Vary("weight_et_trgsf_nominal", getTauEnergyScale_variations, {"eleTES_dm1", "channel", "decayMode_2", "gen_match_2", "weight_et_trgsf_nominal", "weight_et_trgsf_es2Up", "weight_et_trgsf_es2Down"}, {"up", "down"}, "eleTES_dm1")


          // muon faking tau ES
                    .Vary("passSingleTrigger_et_nominal", getTauEnergyScale_variations_int, {"muTES_dm0", "channel", "decayMode_2", "gen_match_2", "passSingleTrigger_et_nominal", "passSingleTrigger_et_es2Up", "passSingleTrigger_et_es2Down"}, {"up", "down"}, "muTES_dm0")
                    .Vary("passSingleTrigger_et_nominal", getTauEnergyScale_variations_int, {"muTES_dm1", "channel", "decayMode_2", "gen_match_2", "passSingleTrigger_et_nominal", "passSingleTrigger_et_es2Up", "passSingleTrigger_et_es2Down"}, {"up", "down"}, "muTES_dm1")

                    .Vary("passCrossTrigger_et_nominal", getTauEnergyScale_variations_int, {"muTES_dm0", "channel", "decayMode_2", "gen_match_2", "passCrossTrigger_et_nominal", "passCrossTrigger_et_es2Up", "passCrossTrigger_et_es2Down"}, {"up", "down"}, "muTES_dm0")
                    .Vary("passCrossTrigger_et_nominal", getTauEnergyScale_variations_int, {"muTES_dm1", "channel", "decayMode_2", "gen_match_2", "passCrossTrigger_et_nominal", "passCrossTrigger_et_es2Up", "passCrossTrigger_et_es2Down"}, {"up", "down"}, "muTES_dm1")

                    .Vary("weight_et_trgsf_nominal", getTauEnergyScale_variations, {"muTES_dm0", "channel", "decayMode_2", "gen_match_2", "weight_et_trgsf_nominal", "weight_et_trgsf_es2Up", "weight_et_trgsf_es2Down"}, {"up", "down"}, "eleTES_dm0")
                    .Vary("weight_et_trgsf_nominal", getTauEnergyScale_variations, {"muTES_dm1", "channel", "decayMode_2", "gen_match_2", "weight_et_trgsf_nominal", "weight_et_trgsf_es2Up", "weight_et_trgsf_es2Down"}, {"up", "down"}, "eleTES_dm1");


        }
      }
    }
  }
  return df;
}

/******************************************************************************/

#endif
