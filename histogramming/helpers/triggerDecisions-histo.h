#ifndef TRIGGER_DECISIONS_HISTO_H_INCL
#define TRIGGER_DECISIONS_HISTO_H_INCL


#include "helperFunctions.h"
#include "histoConfig_class.h"

/**********************************************************/

/*
 * Filter by trigger decision and offline selections.
 */

template <typename T>
auto FilterTauTauTriggerDecisions(T &df, LUNA::histoConfig_t &hConfig) {

    // passSingleTrigger_mt_nominal and passCrossTrigger_mt_nominal check the HLT path and the offline pT thresholds and eta cuts
    // The shifts of these decisions due to the muon and tau energy scale were already calculated in postprocessing, and are already .Vary()'d in histogramming muonES-histo.h
    if (hConfig.doMuTau()) {
        return df.Filter("passSingleTrigger_mt_nominal || passCrossTrigger_mt_nominal", "triggerDecisions-histo.h: mutau: event passes offline criteria for the single or cross-trigger");
    }
    else if (hConfig.doETau()) {
        return df.Filter("passSingleTrigger_et_nominal || passCrossTrigger_et_nominal", "triggerDecisions-histo.h: etau: event passes offline criteria for the single or cross trigger");
    }
    else if (hConfig.doEMu()) {
        return df.Filter("passCross_leadingMuon_em_nominal || passCross_leadingEle_em_nominal", "triggerDecisions-histo.h: emu: event passes one of the two offline cross triggers");
    }
    else {
        return df.Filter("(pt_1 > 0)", "triggerDecisions-histo.h: Dummy cut, channel not recognized!");
    }
}


/**********************************************************/



#endif
