#include "histoConfig_class.h"

/*************************************************************************/

namespace LUNA {

    // Constructor
    histoConfig_t::histoConfig_t(std::string processName, bool doDefaultShifts, bool hasDNNattached, int thisChannel) {
        histoChannel = thisChannel;
        sampleName = processName;

        if (!Helper::containsSubstring(sampleName, "Run")) { // MC
            isSampleMC = true;
            isSampleData = false;
            isSampleEmbedded = false;
        }
        else if (Helper::containsSubstring(sampleName, "Embedded")) { // Embedded
            isSampleMC = false;
            isSampleData = false;
            isSampleEmbedded = true;
        }
        else {  // Data
            isSampleMC = false;
            isSampleData = true;
            isSampleEmbedded = false;
        }

        hasDNN = hasDNNattached;
        doSys = doDefaultShifts;

        // Override doSys if the sample is Data
        if (isSampleData) {
            doSys = false;
        }
    }

    // Setters
    void histoConfig_t::setYear(int year) { sampleYear = year; }
    void histoConfig_t::setDoSystematics(bool performSys) { doSys = performSys; }
    void histoConfig_t::setRecomputeTrigger(bool redoTrigger) { redefineTrigger = redoTrigger; }
    // Getters
    int histoConfig_t::channel() { return histoChannel; }
    std::string histoConfig_t::name() { return sampleName; }
    bool histoConfig_t::isData()     { return isSampleData; }
    bool histoConfig_t::isEmbedded() { return isSampleEmbedded; }
    bool histoConfig_t::isMC()       { return isSampleMC; }
    int histoConfig_t::year() { return sampleYear; }
    std::string histoConfig_t::yearStr() { return std::to_string(year()); }
    bool histoConfig_t::doShifts() { return doSys; }

    bool histoConfig_t::isMuTauOnly() {
        bool data = (isData() && Helper::containsSubstring(sampleName, "SingleMuon-Run"));
        bool embed = (isEmbedded() && Helper::containsSubstring(sampleName, "MuTau"));
        return (data || embed);
    }

    bool histoConfig_t::isETauOnly() {
        bool data = (isData() && (Helper::containsSubstring(sampleName, "EGamma-Run") || (Helper::containsSubstring(sampleName, "SingleElectron-Run"))));
        bool embed = (isEmbedded() && Helper::containsSubstring(sampleName, "ElTau"));
        return (data || embed);
    }

    bool histoConfig_t::isEMuOnly() {
        bool data = (isData() && Helper::containsSubstring(sampleName, "MuonEG-Run"));
        int embed = (isEmbedded() && (Helper::containsSubstring(sampleName, "EMu") || Helper::containsSubstring(sampleName, "ElMu")));
        return (data || embed);
    }

    bool histoConfig_t::doMuTau() {
        return (channel() == Helper::mt);
    }

    bool histoConfig_t::doETau() {
        return (channel() == Helper::et);
    }

    bool histoConfig_t::doEMu() {
        return (channel() == Helper::em);
    }

    bool histoConfig_t::runSystematics() { return doSys; }

    bool histoConfig_t::isTTbar() { return Helper::containsSubstring(sampleName, "TTTo"); }
    bool histoConfig_t::isDY()    { return Helper::containsSubstring(sampleName, "DY"); }
    bool histoConfig_t::isWJets() {
        return (Helper::containsSubstring(sampleName, "W") && Helper::containsSubstring(sampleName, "JetsToLNu"));
    }

    // Recoil
    bool histoConfig_t::isHiggsRecoil() {
        return (Helper::containsSubstring(sampleName, "GluGluH")
        || Helper::containsSubstring(sampleName, "VBFH")
        || Helper::containsSubstring(sampleName, "SUSY"));
    }
    bool histoConfig_t::doRecoil() {
        return (isDY() ||  isWJets() || isHiggsRecoil());
    }
    int histoConfig_t::recoilBosonPdgId() {
        if (doRecoil()) {
            if (isDY())               { return Helper::pdgId_Z; }
            else if (isWJets())       { return Helper::pdgId_W; }
            else if (isHiggsRecoil()) { return Helper::pdgId_H; }
        }
        return -1;
    }
    int histoConfig_t::recoilType() {
        if (doRecoil()) {
            if (isWJets())                      { return 1; }
            else if (isDY() || isHiggsRecoil()) { return 2; }
        }
        return 0;
    }
    bool histoConfig_t::isSignal() {
        return (Helper::containsSubstring(sampleName, "SUSY") || Helper::containsSubstring(sampleName, "Cascade"));
    }
    bool histoConfig_t::recomputeTrigger() {
        return redefineTrigger;
    }
    bool histoConfig_t::isMCnonHiggs() {
        // bool isMCnonHiggs = (sample!="data_obs" && sample!="embedded" && !isSignal && name!="ggh_htt" && name!="ggh_hww" && name!="qqh_htt" && name!="qqh_hww" && name!="Zh_htt" && name!="Zh_hww" && name!="Wh_htt" && name!="Wh_hww" && name!="tth");
        return (isMC() && !isData() && !isEmbedded() && !isSignal()
                && !(Helper::containsSubstring(sampleName, "GluGluHToTauTau"))
                && !(Helper::containsSubstring(sampleName, "GluGluHToWW"))
                && !(Helper::containsSubstring(sampleName, "VBFHToTauTau"))
                && !(Helper::containsSubstring(sampleName, "VBFHToWW"))
                && !(Helper::containsSubstring(sampleName, "ZHToTauTau"))
                && !(Helper::containsSubstring(sampleName, "GluGluZH_HToWW"))
                && !(Helper::containsSubstring(sampleName, "HWplusJ_HToWW"))
                && !(Helper::containsSubstring(sampleName, "HWminusJ_HToWW"))
                && !(Helper::containsSubstring(sampleName, "WminusHToTauTau"))
                && !(Helper::containsSubstring(sampleName, "WplusHToTauTau"))
                && !(Helper::containsSubstring(sampleName, "ttHToNonbb"))
                && !(Helper::containsSubstring(sampleName, "ttHTobb"))
                && !(Helper::containsSubstring(sampleName, "HZJ_HToWW"))
                );
    }

    std::string histoConfig_t::getRunTag() {
        if (isData() || isEmbedded()) {
            // yes this code is awful
            if (Helper::containsAnyOfTheseSubstrings(sampleName, {"2018A"}))                   return "A";
            if (Helper::containsAnyOfTheseSubstrings(sampleName, {"2016B", "2017B", "2018B"})) return "B";
            if (Helper::containsAnyOfTheseSubstrings(sampleName, {"2016C", "2017C", "2018C"})) return "C";
            if (Helper::containsAnyOfTheseSubstrings(sampleName, {"2016D", "2017D", "2018D"})) return "D";
            if (Helper::containsAnyOfTheseSubstrings(sampleName, {"2016E", "2017E"}))          return "E";
            if (Helper::containsAnyOfTheseSubstrings(sampleName, {"2016F", "2017F"}))          return "F";
            if (Helper::containsAnyOfTheseSubstrings(sampleName, {"2016G"}))                   return "G";
            if (Helper::containsAnyOfTheseSubstrings(sampleName, {"2016H"}))                   return "H";
        }
        return "";
    }


}


/*************************************************************************/
