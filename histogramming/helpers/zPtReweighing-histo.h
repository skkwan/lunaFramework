// Z pT reweighing

#ifndef Z_PT_REWEIGHING_HISTO_H_INCL
#define Z_PT_REWEIGHING_HISTO_H_INCL

#include "histoConfig_class.h"

/*******************************************************************/

template <typename T>
auto GetZPtReweighingShifts(T &df, LUNA::histoConfig_t &hConfig) {
    if (hConfig.doShifts()) {
        if (hConfig.isMC() && hConfig.isDY()) {
            return df.Vary("weight_zPt_nominal", "ROOT::RVecF{weight_zPt_Zpt_Up, weight_zPt_Zpt_Down}", {"up", "down"}, "Zpt");
        }
        else {
            return df.Vary("weight_zPt_nominal", "ROOT::RVecF{weight_zPt_nominal, weight_zPt_nominal}", {"up", "down"}, "Zpt");
        }
    }
    return df;
}

/*******************************************************************/


#endif
