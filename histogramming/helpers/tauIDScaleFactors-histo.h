#ifndef TAU_ID_SCALE_FACTORS_HISTO_H_INCL
#define TAU_ID_SCALE_FACTORS_HISTO_H_INCL

#include "helperFunctions.h"
#include "histoConfig_class.h"

#include "TFile.h"
#include "TF1.h"

/*******************************************************************/

/*
 * Book the variations of the tau ID SF. Shifts of the tau ID SF due to the lepton energy scales are handled in the respective lepton energy scale header files.
 */

template <typename T>
auto GetTauIDShifts(T &df, LUNA::histoConfig_t &hConfig) {

  // MC or Embedded: any year
  if (hConfig.isMC() || hConfig.isEmbedded()) {
    if (hConfig.doShifts()) {
      return df.Vary("weight_tauID_nominal", "ROOT::RVecF{weight_tauID_tauideff_pt20to25_Up,    weight_tauID_tauideff_pt20to25_Down}"    , {"up", "down"}, "tauideff_pt20to25")
               .Vary("weight_tauID_nominal", "ROOT::RVecF{weight_tauID_tauideff_pt25to30_Up,    weight_tauID_tauideff_pt25to30_Down}"    , {"up", "down"}, "tauideff_pt25to30")
               .Vary("weight_tauID_nominal", "ROOT::RVecF{weight_tauID_tauideff_pt30to35_Up,    weight_tauID_tauideff_pt30to35_Down}"    , {"up", "down"}, "tauideff_pt30to35")
               .Vary("weight_tauID_nominal", "ROOT::RVecF{weight_tauID_tauideff_pt35to40_Up,    weight_tauID_tauideff_pt35to40_Down}"    , {"up", "down"}, "tauideff_pt35to40")
               .Vary("weight_tauID_nominal", "ROOT::RVecF{weight_tauID_tauideff_pt40to500_Up,   weight_tauID_tauideff_pt40to500_Down}"   , {"up", "down"}, "tauideff_pt40to500")
               .Vary("weight_tauID_nominal", "ROOT::RVecF{weight_tauID_tauideff_pt500to1000_Up, weight_tauID_tauideff_pt500to1000_Down}" , {"up", "down"}, "tauideff_pt500to1000")
               .Vary("weight_tauID_nominal", "ROOT::RVecF{weight_tauID_tauideff_ptgt1000_Up,    weight_tauID_tauideff_ptgt1000_Down}"    , {"up", "down"}, "tauideff_ptgt1000");
    }
  }
  return df;
}

/*******************************************************************/


#endif
