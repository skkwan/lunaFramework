// Helper functions for muon energy scale and shifts.

#ifndef MUON_ES_HISTO_H_INCL
#define MUON_ES_HISTO_H_INCL

#include "computePhysicsVariables.h"
#include "fileIO.h"
#include "helperFunctions.h"
#include "histoConfig_class.h"

#include "TFile.h"
#include "TH1F.h"
#include "TObject.h"
#include "Math/Vector4D.h"

/******************************************************************************/

/*
 * Return the up/down shifts of a variable, provided that the systematic to do applies to the muon in question. Otherwise return
 * the nominal values.
 * The muon ES itself is binned in the muon's eta. Other shifts do not depend on the eta.
 */

ROOT::RVecF getMuonEnergyScale_variations(std::string sysToDo, int channel, unsigned int muon_genMatch, float muonEta,
                                          float val_nominal, float val_up, float val_down) {

  ROOT::RVecF variations = {val_nominal, val_nominal};


  // Apply muon energy scale to all embedded and all MC if gen-matched to a muon.
  bool isTrueMuon = ((muon_genMatch == 2) || (muon_genMatch == 4));

  // Muon energy scale is binned in eta and the shifts are binned in eta and separated by MC vs. embedded
  // (although the shifts are the same).
  if (((channel == Helper::mt) || (channel == Helper::em)) && isTrueMuon) {
    if      ((abs(muonEta) > 0.0) && (abs(muonEta) < 1.2) && (sysToDo == "muES_eta0to1p2"))   { variations = {val_up, val_down}; }
    else if ((abs(muonEta) > 1.2) && (abs(muonEta) < 2.1) && (sysToDo == "muES_eta1p2to2p1")) { variations = {val_up, val_down}; }
    else if ((abs(muonEta) > 2.1) && (abs(muonEta) < 2.4) && (sysToDo == "muES_eta2p1to2p4")) { variations = {val_up, val_down}; }
  }

  return variations;
}

// The same thing but with bools
ROOT::RVecI getMuonEnergyScale_variations_int(std::string sysToDo, int channel, unsigned int muon_genMatch, float muonEta,
                                              int val_nominal, int val_up, int val_down) {

  ROOT::RVecI variations = {val_nominal, val_nominal};


  // Apply muon energy scale to all embedded and all MC if gen-matched to a muon.
  bool isTrueMuon = ((muon_genMatch == 2) || (muon_genMatch == 4));

  // Muon energy scale is binned in eta and the shifts are binned in eta and separated by MC vs. embedded
  // (although the shifts are the same).
  if (((channel == Helper::mt) || (channel == Helper::em)) && isTrueMuon) {
    if      ((abs(muonEta) > 0.0) && (abs(muonEta) < 1.2) && (sysToDo == "muES_eta0to1p2"))   { variations = {val_up, val_down}; }
    else if ((abs(muonEta) > 1.2) && (abs(muonEta) < 2.1) && (sysToDo == "muES_eta1p2to2p1")) { variations = {val_up, val_down}; }
    else if ((abs(muonEta) > 2.1) && (abs(muonEta) < 2.4) && (sysToDo == "muES_eta2p1to2p4")) { variations = {val_up, val_down}; }
  }

  return variations;
}
/******************************************************************************/

/*
 * Apply the muon energy scale (MES) and shifts to generator-matched muons in MC
 * and all muons in Embedded.
 */

template <typename T>
auto GetMuonEnergyScaleShifts(T &df, LUNA::histoConfig_t &hConfig) {

  std::cout << ">>> muonES.h: GetMuonEnergyScaleShifts: doShifts() is " << hConfig.doShifts() << std::endl;

  if (hConfig.isMC() || hConfig.isEmbedded()) {
    if (hConfig.doShifts()) {

      auto df2 = df.Define("muES_eta0to1p2", [=]() { return (std::string) "muES_eta0to1p2"; })
                   .Define("muES_eta1p2to2p1", [=]() { return (std::string) "muES_eta1p2to2p1"; })
                   .Define("muES_eta2p1to2p4", [=]() { return (std::string) "muES_eta2p1to2p4"; });

      // mutau: the muon is the first leg
      if (hConfig.doMuTau()) {
        // Shifts of kinematic variables due to the muon energy scale shifts

        return df2.Vary("pt_1_nominal", getMuonEnergyScale_variations,  {"muES_eta0to1p2",   "channel", "gen_match_1", "eta_1", "pt_1_nominal", "pt_1_es1Up", "pt_1_es1Down"}, {"up", "down"}, "muES_eta0to1p2"  )
                  .Vary("pt_1_nominal", getMuonEnergyScale_variations, {"muES_eta1p2to2p1", "channel", "gen_match_1", "eta_1", "pt_1_nominal", "pt_1_es1Up", "pt_1_es1Down"}, {"up", "down"}, "muES_eta1p2to2p1")
                  .Vary("pt_1_nominal", getMuonEnergyScale_variations, {"muES_eta2p1to2p4", "channel", "gen_match_1", "eta_1", "pt_1_nominal", "pt_1_es1Up", "pt_1_es1Down"}, {"up", "down"}, "muES_eta2p1to2p4")

                  .Vary("m_1_nominal", getMuonEnergyScale_variations, {"muES_eta0to1p2",   "channel", "gen_match_1", "eta_1", "m_1_nominal", "m_1_es1Up", "m_1_es1Down"}, {"up", "down"}, "muES_eta0to1p2")
                  .Vary("m_1_nominal", getMuonEnergyScale_variations, {"muES_eta1p2to2p1", "channel", "gen_match_1", "eta_1", "m_1_nominal", "m_1_es1Up", "m_1_es1Down"}, {"up", "down"}, "muES_eta1p2to2p1")
                  .Vary("m_1_nominal", getMuonEnergyScale_variations, {"muES_eta2p1to2p4", "channel", "gen_match_1", "eta_1", "m_1_nominal", "m_1_es1Up", "m_1_es1Down"}, {"up", "down"}, "muES_eta2p1to2p4")

                  .Vary("met_nominal", getMuonEnergyScale_variations, {"muES_eta0to1p2",   "channel", "gen_match_1", "eta_1", "met_nominal", "met_es1Up", "met_es1Down"}, {"up", "down"}, "muES_eta0to1p2")
                  .Vary("met_nominal", getMuonEnergyScale_variations, {"muES_eta1p2to2p1", "channel", "gen_match_1", "eta_1", "met_nominal", "met_es1Up", "met_es1Down"}, {"up", "down"}, "muES_eta1p2to2p1")
                  .Vary("met_nominal", getMuonEnergyScale_variations, {"muES_eta2p1to2p4", "channel", "gen_match_1", "eta_1", "met_nominal", "met_es1Up", "met_es1Down"}, {"up", "down"}, "muES_eta2p1to2p4")

                  .Vary("metphi_nominal", getMuonEnergyScale_variations, {"muES_eta0to1p2",   "channel", "gen_match_1", "eta_1", "metphi_nominal", "metphi_es1Up", "metphi_es1Down"}, {"up", "down"}, "muES_eta0to1p2")
                  .Vary("metphi_nominal", getMuonEnergyScale_variations, {"muES_eta1p2to2p1", "channel", "gen_match_1", "eta_1", "metphi_nominal", "metphi_es1Up", "metphi_es1Down"}, {"up", "down"}, "muES_eta1p2to2p1")
                  .Vary("metphi_nominal", getMuonEnergyScale_variations, {"muES_eta2p1to2p4", "channel", "gen_match_1", "eta_1", "metphi_nominal", "metphi_es1Up", "metphi_es1Down"}, {"up", "down"}, "muES_eta2p1to2p4")

                  // SVFit mass if available (otherwise these branches are identical to the visible mass)
                  .Vary("m_tt_nominal", getMuonEnergyScale_variations, {"muES_eta0to1p2",   "channel", "gen_match_1", "eta_1", "m_tt_nominal", "m_tt_es1Up", "m_tt_es1Down"}, {"up", "down"}, "muES_eta0to1p2")
                  .Vary("m_tt_nominal", getMuonEnergyScale_variations, {"muES_eta1p2to2p1", "channel", "gen_match_1", "eta_1", "m_tt_nominal", "m_tt_es1Up", "m_tt_es1Down"}, {"up", "down"}, "muES_eta1p2to2p1")
                  .Vary("m_tt_nominal", getMuonEnergyScale_variations, {"muES_eta2p1to2p4", "channel", "gen_match_1", "eta_1", "m_tt_nominal", "m_tt_es1Up", "m_tt_es1Down"}, {"up", "down"}, "muES_eta2p1to2p4")
                  .Vary("pt_tt_nominal", getMuonEnergyScale_variations, {"muES_eta0to1p2",   "channel", "gen_match_1", "eta_1", "pt_tt_nominal", "pt_tt_es1Up", "pt_tt_es1Down"}, {"up", "down"}, "muES_eta0to1p2")
                  .Vary("pt_tt_nominal", getMuonEnergyScale_variations, {"muES_eta1p2to2p1", "channel", "gen_match_1", "eta_1", "pt_tt_nominal", "pt_tt_es1Up", "pt_tt_es1Down"}, {"up", "down"}, "muES_eta1p2to2p1")
                  .Vary("pt_tt_nominal", getMuonEnergyScale_variations, {"muES_eta2p1to2p4", "channel", "gen_match_1", "eta_1", "pt_tt_nominal", "pt_tt_es1Up", "pt_tt_es1Down"}, {"up", "down"}, "muES_eta2p1to2p4")

                  // Visible mass
                  .Vary("m_vis_nominal", getMuonEnergyScale_variations, {"muES_eta0to1p2",   "channel", "gen_match_1", "eta_1", "m_vis_nominal", "m_vis_es1Up", "m_vis_es1Down"}, {"up", "down"}, "muES_eta0to1p2")
                  .Vary("m_vis_nominal", getMuonEnergyScale_variations, {"muES_eta1p2to2p1", "channel", "gen_match_1", "eta_1", "m_vis_nominal", "m_vis_es1Up", "m_vis_es1Down"}, {"up", "down"}, "muES_eta1p2to2p1")
                  .Vary("m_vis_nominal", getMuonEnergyScale_variations, {"muES_eta2p1to2p4", "channel", "gen_match_1", "eta_1", "m_vis_nominal", "m_vis_es1Up", "m_vis_es1Down"}, {"up", "down"}, "muES_eta2p1to2p4")
                  .Vary("pt_vis_nominal", getMuonEnergyScale_variations, {"muES_eta0to1p2",   "channel", "gen_match_1", "eta_1", "pt_vis_nominal", "pt_vis_es1Up", "pt_vis_es1Down"}, {"up", "down"}, "muES_eta0to1p2")
                  .Vary("pt_vis_nominal", getMuonEnergyScale_variations, {"muES_eta1p2to2p1", "channel", "gen_match_1", "eta_1", "pt_vis_nominal", "pt_vis_es1Up", "pt_vis_es1Down"}, {"up", "down"}, "muES_eta1p2to2p1")
                  .Vary("pt_vis_nominal", getMuonEnergyScale_variations, {"muES_eta2p1to2p4", "channel", "gen_match_1", "eta_1", "pt_vis_nominal", "pt_vis_es1Up", "pt_vis_es1Down"}, {"up", "down"}, "muES_eta2p1to2p4")

                  // Cut-based categorization variables
                  .Vary("m_btautau_vis_nominal", getMuonEnergyScale_variations, {"muES_eta0to1p2",   "channel", "gen_match_1", "eta_1", "m_btautau_vis_nominal", "m_btautau_vis_es1Up", "m_btautau_vis_es1Down"}, {"up", "down"}, "muES_eta0to1p2")
                  .Vary("m_btautau_vis_nominal", getMuonEnergyScale_variations, {"muES_eta1p2to2p1", "channel", "gen_match_1", "eta_1", "m_btautau_vis_nominal", "m_btautau_vis_es1Up", "m_btautau_vis_es1Down"}, {"up", "down"}, "muES_eta1p2to2p1")
                  .Vary("m_btautau_vis_nominal", getMuonEnergyScale_variations, {"muES_eta2p1to2p4", "channel", "gen_match_1", "eta_1", "m_btautau_vis_nominal", "m_btautau_vis_es1Up", "m_btautau_vis_es1Down"}, {"up", "down"}, "muES_eta2p1to2p4")

                  .Vary("pt_btautau_vis_nominal", getMuonEnergyScale_variations, {"muES_eta0to1p2",   "channel", "gen_match_1", "eta_1", "pt_btautau_vis_nominal", "pt_btautau_vis_es1Up", "pt_btautau_vis_es1Down"}, {"up", "down"}, "muES_eta0to1p2")
                  .Vary("pt_btautau_vis_nominal", getMuonEnergyScale_variations, {"muES_eta1p2to2p1", "channel", "gen_match_1", "eta_1", "pt_btautau_vis_nominal", "pt_btautau_vis_es1Up", "pt_btautau_vis_es1Down"}, {"up", "down"}, "muES_eta1p2to2p1")
                  .Vary("pt_btautau_vis_nominal", getMuonEnergyScale_variations, {"muES_eta2p1to2p4", "channel", "gen_match_1", "eta_1", "pt_btautau_vis_nominal", "pt_btautau_vis_es1Up", "pt_btautau_vis_es1Down"}, {"up", "down"}, "muES_eta2p1to2p4")

                  .Vary("D_zeta_nominal", getMuonEnergyScale_variations, {"muES_eta0to1p2",   "channel", "gen_match_1", "eta_1", "D_zeta_nominal", "D_zeta_es1Up", "D_zeta_es1Down"}, {"up", "down"}, "muES_eta0to1p2")
                  .Vary("D_zeta_nominal", getMuonEnergyScale_variations, {"muES_eta1p2to2p1", "channel", "gen_match_1", "eta_1", "D_zeta_nominal", "D_zeta_es1Up", "D_zeta_es1Down"}, {"up", "down"}, "muES_eta1p2to2p1")
                  .Vary("D_zeta_nominal", getMuonEnergyScale_variations, {"muES_eta2p1to2p4", "channel", "gen_match_1", "eta_1", "D_zeta_nominal", "D_zeta_es1Up", "D_zeta_es1Down"}, {"up", "down"}, "muES_eta2p1to2p4")

                  // For mutau channel, muon is leg 1
                  .Vary("mtMET_1_nominal", getMuonEnergyScale_variations, {"muES_eta0to1p2",   "channel", "gen_match_1", "eta_1", "mtMET_1_nominal", "mtMET_1_es1Up", "mtMET_1_es1Down"}, {"up", "down"}, "muES_eta0to1p2")
                  .Vary("mtMET_1_nominal", getMuonEnergyScale_variations, {"muES_eta1p2to2p1", "channel", "gen_match_1", "eta_1", "mtMET_1_nominal", "mtMET_1_es1Up", "mtMET_1_es1Down"}, {"up", "down"}, "muES_eta1p2to2p1")
                  .Vary("mtMET_1_nominal", getMuonEnergyScale_variations, {"muES_eta2p1to2p4", "channel", "gen_match_1", "eta_1", "mtMET_1_nominal", "mtMET_1_es1Up", "mtMET_1_es1Down"}, {"up", "down"}, "muES_eta2p1to2p4")

                  // Shifts of the trigger paths due to muon energy scale up/down. The muon is leg 1 in mutau, and leg 2 is emu
                  .Vary("passSingleTrigger_mt_nominal", getMuonEnergyScale_variations_int, {"muES_eta0to1p2",   "channel", "gen_match_1", "eta_1", "passSingleTrigger_mt_nominal", "passSingleTrigger_mt_es1Up", "passSingleTrigger_mt_es1Down"}, {"up", "down"}, "muES_eta0to1p2")
                  .Vary("passSingleTrigger_mt_nominal", getMuonEnergyScale_variations_int, {"muES_eta1p2to2p1", "channel", "gen_match_1", "eta_1", "passSingleTrigger_mt_nominal", "passSingleTrigger_mt_es1Up", "passSingleTrigger_mt_es1Down"}, {"up", "down"}, "muES_eta1p2to2p1")
                  .Vary("passSingleTrigger_mt_nominal", getMuonEnergyScale_variations_int, {"muES_eta2p1to2p4", "channel", "gen_match_1", "eta_1", "passSingleTrigger_mt_nominal", "passSingleTrigger_mt_es1Up", "passSingleTrigger_mt_es1Down"}, {"up", "down"}, "muES_eta2p1to2p4")

                  .Vary("passCrossTrigger_mt_nominal", getMuonEnergyScale_variations_int, {"muES_eta0to1p2",   "channel", "gen_match_1", "eta_1", "passCrossTrigger_mt_nominal", "passCrossTrigger_mt_es1Up", "passCrossTrigger_mt_es1Down"}, {"up", "down"}, "muES_eta0to1p2")
                  .Vary("passCrossTrigger_mt_nominal", getMuonEnergyScale_variations_int, {"muES_eta1p2to2p1", "channel", "gen_match_1", "eta_1", "passCrossTrigger_mt_nominal", "passCrossTrigger_mt_es1Up", "passCrossTrigger_mt_es1Down"}, {"up", "down"}, "muES_eta1p2to2p1")
                  .Vary("passCrossTrigger_mt_nominal", getMuonEnergyScale_variations_int, {"muES_eta2p1to2p4", "channel", "gen_match_1", "eta_1", "passCrossTrigger_mt_nominal", "passCrossTrigger_mt_es1Up", "passCrossTrigger_mt_es1Down"}, {"up", "down"}, "muES_eta2p1to2p4")

                  // Shifts of weights ONLY due to muon energy scale up/down: use the same procedure as above
                  .Vary("weight_mt_trgsf_nominal", getMuonEnergyScale_variations, {"muES_eta0to1p2",   "channel", "gen_match_1", "eta_1", "weight_mt_trgsf_nominal", "weight_mt_trgsf_es1Up", "weight_mt_trgsf_es1Down"}, {"up", "down"}, "muES_eta0to1p2")
                  .Vary("weight_mt_trgsf_nominal", getMuonEnergyScale_variations, {"muES_eta1p2to2p1", "channel", "gen_match_1", "eta_1", "weight_mt_trgsf_nominal", "weight_mt_trgsf_es1Up", "weight_mt_trgsf_es1Down"}, {"up", "down"}, "muES_eta1p2to2p1")
                  .Vary("weight_mt_trgsf_nominal", getMuonEnergyScale_variations, {"muES_eta2p1to2p4", "channel", "gen_match_1", "eta_1", "weight_mt_trgsf_nominal", "weight_mt_trgsf_es1Up", "weight_mt_trgsf_es1Down"}, {"up", "down"}, "muES_eta2p1to2p4")

                  .Vary("weight_muonIdIso_nominal", getMuonEnergyScale_variations, {"muES_eta0to1p2",   "channel", "gen_match_1", "eta_1", "weight_muonIdIso_nominal", "weight_muonIdIso_mt_es1Up", "weight_muonIdIso_mt_es1Down"}, {"up", "down"}, "muES_eta0to1p2")
                  .Vary("weight_muonIdIso_nominal", getMuonEnergyScale_variations, {"muES_eta1p2to2p1", "channel", "gen_match_1", "eta_1", "weight_muonIdIso_nominal", "weight_muonIdIso_mt_es1Up", "weight_muonIdIso_mt_es1Down"}, {"up", "down"}, "muES_eta1p2to2p1")
                  .Vary("weight_muonIdIso_nominal", getMuonEnergyScale_variations, {"muES_eta2p1to2p4", "channel", "gen_match_1", "eta_1", "weight_muonIdIso_nominal", "weight_muonIdIso_mt_es1Up", "weight_muonIdIso_mt_es1Down"}, {"up", "down"}, "muES_eta2p1to2p4");

      }
      else if (hConfig.doEMu()) {
        // EMu: use _2 because the muon is the second leg. And "es2" is the muon energy scale.
        return df2.Vary("pt_2_nominal", getMuonEnergyScale_variations,  {"muES_eta0to1p2",   "channel", "gen_match_2", "eta_2", "pt_2_nominal", "pt_2_es2Up", "pt_2_es2Down"}, {"up", "down"}, "muES_eta0to1p2")
                  .Vary("pt_2_nominal", getMuonEnergyScale_variations, {"muES_eta1p2to2p1", "channel", "gen_match_2", "eta_2", "pt_2_nominal", "pt_2_es2Up", "pt_2_es2Down"}, {"up", "down"}, "muES_eta1p2to2p1")
                  .Vary("pt_2_nominal", getMuonEnergyScale_variations, {"muES_eta2p1to2p4", "channel", "gen_match_2", "eta_2", "pt_2_nominal", "pt_2_es2Up", "pt_2_es2Down"}, {"up", "down"}, "muES_eta2p1to2p4")
                  .Vary("m_2_nominal", getMuonEnergyScale_variations, {"muES_eta0to1p2",   "channel", "gen_match_2", "eta_2", "m_2_nominal", "m_2_es2Up", "m_2_es2Down"}, {"up", "down"}, "muES_eta0to1p2")
                  .Vary("m_2_nominal", getMuonEnergyScale_variations, {"muES_eta1p2to2p1", "channel", "gen_match_2", "eta_2", "m_2_nominal", "m_2_es2Up", "m_2_es2Down"}, {"up", "down"}, "muES_eta1p2to2p1")
                  .Vary("m_2_nominal", getMuonEnergyScale_variations, {"muES_eta2p1to2p4", "channel", "gen_match_2", "eta_2", "m_2_nominal", "m_2_es2Up", "m_2_es2Down"}, {"up", "down"}, "muES_eta2p1to2p4")
                  .Vary("met_nominal", getMuonEnergyScale_variations, {"muES_eta0to1p2",   "channel", "gen_match_2", "eta_2", "met_nominal", "met_es2Up", "met_es2Down"}, {"up", "down"}, "muES_eta0to1p2")
                  .Vary("met_nominal", getMuonEnergyScale_variations, {"muES_eta1p2to2p1", "channel", "gen_match_2", "eta_2", "met_nominal", "met_es2Up", "met_es2Down"}, {"up", "down"}, "muES_eta1p2to2p1")
                  .Vary("met_nominal", getMuonEnergyScale_variations, {"muES_eta2p1to2p4", "channel", "gen_match_2", "eta_2", "met_nominal", "met_es2Up", "met_es2Down"}, {"up", "down"}, "muES_eta2p1to2p4")
                  .Vary("metphi_nominal", getMuonEnergyScale_variations, {"muES_eta0to1p2",   "channel", "gen_match_2", "eta_2", "metphi_nominal", "metphi_es2Up", "metphi_es2Down"}, {"up", "down"}, "muES_eta0to1p2")
                  .Vary("metphi_nominal", getMuonEnergyScale_variations, {"muES_eta1p2to2p1", "channel", "gen_match_2", "eta_2", "metphi_nominal", "metphi_es2Up", "metphi_es2Down"}, {"up", "down"}, "muES_eta1p2to2p1")
                  .Vary("metphi_nominal", getMuonEnergyScale_variations, {"muES_eta2p1to2p4", "channel", "gen_match_2", "eta_2", "metphi_nominal", "metphi_es2Up", "metphi_es2Down"}, {"up", "down"}, "muES_eta2p1to2p4")

                  // SVFit mass if available (otherwise these branches are identical to the visible mass)
                  .Vary("m_tt_nominal", getMuonEnergyScale_variations, {"muES_eta0to1p2",   "channel", "gen_match_2", "eta_2", "m_tt_nominal", "m_tt_es2Up", "m_tt_es2Down"}, {"up", "down"}, "muES_eta0to1p2")
                  .Vary("m_tt_nominal", getMuonEnergyScale_variations, {"muES_eta1p2to2p1", "channel", "gen_match_2", "eta_2", "m_tt_nominal", "m_tt_es2Up", "m_tt_es2Down"}, {"up", "down"}, "muES_eta1p2to2p1")
                  .Vary("m_tt_nominal", getMuonEnergyScale_variations, {"muES_eta2p1to2p4", "channel", "gen_match_2", "eta_2", "m_tt_nominal", "m_tt_es2Up", "m_tt_es2Down"}, {"up", "down"}, "muES_eta2p1to2p4")
                  .Vary("pt_tt_nominal", getMuonEnergyScale_variations, {"muES_eta0to1p2",   "channel", "gen_match_2", "eta_2", "pt_tt_nominal", "pt_tt_es2Up", "pt_tt_es2Down"}, {"up", "down"}, "muES_eta0to1p2")
                  .Vary("pt_tt_nominal", getMuonEnergyScale_variations, {"muES_eta1p2to2p1", "channel", "gen_match_2", "eta_2", "pt_tt_nominal", "pt_tt_es2Up", "pt_tt_es2Down"}, {"up", "down"}, "muES_eta1p2to2p1")
                  .Vary("pt_tt_nominal", getMuonEnergyScale_variations, {"muES_eta2p1to2p4", "channel", "gen_match_2", "eta_2", "pt_tt_nominal", "pt_tt_es2Up", "pt_tt_es2Down"}, {"up", "down"}, "muES_eta2p1to2p4")

                  // Visible mass
                  .Vary("m_vis_nominal", getMuonEnergyScale_variations, {"muES_eta0to1p2",   "channel", "gen_match_2", "eta_2", "m_vis_nominal", "m_vis_es2Up", "m_vis_es2Down"}, {"up", "down"}, "muES_eta0to1p2")
                  .Vary("m_vis_nominal", getMuonEnergyScale_variations, {"muES_eta1p2to2p1", "channel", "gen_match_2", "eta_2", "m_vis_nominal", "m_vis_es2Up", "m_vis_es2Down"}, {"up", "down"}, "muES_eta1p2to2p1")
                  .Vary("m_vis_nominal", getMuonEnergyScale_variations, {"muES_eta2p1to2p4", "channel", "gen_match_2", "eta_2", "m_vis_nominal", "m_vis_es2Up", "m_vis_es2Down"}, {"up", "down"}, "muES_eta2p1to2p4")
                  .Vary("pt_vis_nominal", getMuonEnergyScale_variations, {"muES_eta0to1p2",   "channel", "gen_match_2", "eta_2", "pt_vis_nominal", "pt_vis_es2Up", "pt_vis_es2Down"}, {"up", "down"}, "muES_eta0to1p2")
                  .Vary("pt_vis_nominal", getMuonEnergyScale_variations, {"muES_eta1p2to2p1", "channel", "gen_match_2", "eta_2", "pt_vis_nominal", "pt_vis_es2Up", "pt_vis_es2Down"}, {"up", "down"}, "muES_eta1p2to2p1")
                  .Vary("pt_vis_nominal", getMuonEnergyScale_variations, {"muES_eta2p1to2p4", "channel", "gen_match_2", "eta_2", "pt_vis_nominal", "pt_vis_es2Up", "pt_vis_es2Down"}, {"up", "down"}, "muES_eta2p1to2p4")

                  // Cut-based categorization variables, note that muon is second leg
                  .Vary("m_btautau_vis_nominal", getMuonEnergyScale_variations, {"muES_eta0to1p2",   "channel", "gen_match_2", "eta_2", "m_btautau_vis_nominal", "m_btautau_vis_es2Up", "m_btautau_vis_es2Down"}, {"up", "down"}, "muES_eta0to1p2")
                  .Vary("m_btautau_vis_nominal", getMuonEnergyScale_variations, {"muES_eta1p2to2p1", "channel", "gen_match_2", "eta_2", "m_btautau_vis_nominal", "m_btautau_vis_es2Up", "m_btautau_vis_es2Down"}, {"up", "down"}, "muES_eta1p2to2p1")
                  .Vary("m_btautau_vis_nominal", getMuonEnergyScale_variations, {"muES_eta2p1to2p4", "channel", "gen_match_2", "eta_2", "m_btautau_vis_nominal", "m_btautau_vis_es2Up", "m_btautau_vis_es2Down"}, {"up", "down"}, "muES_eta2p1to2p4")

                  .Vary("pt_btautau_vis_nominal", getMuonEnergyScale_variations, {"muES_eta0to1p2",   "channel", "gen_match_2", "eta_2", "pt_btautau_vis_nominal", "pt_btautau_vis_es2Up", "pt_btautau_vis_es2Down"}, {"up", "down"}, "muES_eta0to1p2")
                  .Vary("pt_btautau_vis_nominal", getMuonEnergyScale_variations, {"muES_eta1p2to2p1", "channel", "gen_match_2", "eta_2", "pt_btautau_vis_nominal", "pt_btautau_vis_es2Up", "pt_btautau_vis_es2Down"}, {"up", "down"}, "muES_eta1p2to2p1")
                  .Vary("pt_btautau_vis_nominal", getMuonEnergyScale_variations, {"muES_eta2p1to2p4", "channel", "gen_match_2", "eta_2", "pt_btautau_vis_nominal", "pt_btautau_vis_es2Up", "pt_btautau_vis_es2Down"}, {"up", "down"}, "muES_eta2p1to2p4")

                  .Vary("D_zeta_nominal", getMuonEnergyScale_variations, {"muES_eta0to1p2",   "channel", "gen_match_2", "eta_2", "D_zeta_nominal", "D_zeta_es2Up", "D_zeta_es2Down"}, {"up", "down"}, "muES_eta0to1p2")
                  .Vary("D_zeta_nominal", getMuonEnergyScale_variations, {"muES_eta1p2to2p1", "channel", "gen_match_2", "eta_2", "D_zeta_nominal", "D_zeta_es2Up", "D_zeta_es2Down"}, {"up", "down"}, "muES_eta1p2to2p1")
                  .Vary("D_zeta_nominal", getMuonEnergyScale_variations, {"muES_eta2p1to2p4", "channel", "gen_match_2", "eta_2", "D_zeta_nominal", "D_zeta_es2Up", "D_zeta_es2Down"}, {"up", "down"}, "muES_eta2p1to2p4")

                  .Vary("mtMET_2_nominal", getMuonEnergyScale_variations, {"muES_eta0to1p2",   "channel", "gen_match_2", "eta_2", "mtMET_2_nominal", "mtMET_2_es2Up", "mtMET_2_es2Down"}, {"up", "down"}, "muES_eta0to1p2")
                  .Vary("mtMET_2_nominal", getMuonEnergyScale_variations, {"muES_eta1p2to2p1", "channel", "gen_match_2", "eta_2", "mtMET_2_nominal", "mtMET_2_es2Up", "mtMET_2_es2Down"}, {"up", "down"}, "muES_eta1p2to2p1")
                  .Vary("mtMET_2_nominal", getMuonEnergyScale_variations, {"muES_eta2p1to2p4", "channel", "gen_match_2", "eta_2", "mtMET_2_nominal", "mtMET_2_es2Up", "mtMET_2_es2Down"}, {"up", "down"}, "muES_eta2p1to2p4")

                  // Trigger weights
                  .Vary("weight_em_trgsf_nominal", getMuonEnergyScale_variations, {"muES_eta0to1p2",   "channel", "gen_match_2", "eta_2", "weight_em_trgsf_nominal", "weight_em_trgsf_es2Up", "weight_em_trgsf_es2Down"}, {"up", "down"}, "muES_eta0to1p2")
                  .Vary("weight_em_trgsf_nominal", getMuonEnergyScale_variations, {"muES_eta1p2to2p1", "channel", "gen_match_2", "eta_2", "weight_em_trgsf_nominal", "weight_em_trgsf_es2Up", "weight_em_trgsf_es2Down"}, {"up", "down"}, "muES_eta1p2to2p1")
                  .Vary("weight_em_trgsf_nominal", getMuonEnergyScale_variations, {"muES_eta2p1to2p4", "channel", "gen_match_2", "eta_2", "weight_em_trgsf_nominal", "weight_em_trgsf_es2Up", "weight_em_trgsf_es2Down"}, {"up", "down"}, "muES_eta2p1to2p4")

                  // Muon ID/iso weights
                  .Vary("weight_muonIdIso_nominal", getMuonEnergyScale_variations, {"muES_eta0to1p2",   "channel", "gen_match_2", "eta_2", "weight_muonIdIso_nominal", "weight_muonIdIso_em_es2Up", "weight_muonIdIso_em_es2Down"}, {"up", "down"}, "muES_eta0to1p2")
                  .Vary("weight_muonIdIso_nominal", getMuonEnergyScale_variations, {"muES_eta1p2to2p1", "channel", "gen_match_2", "eta_2", "weight_muonIdIso_nominal", "weight_muonIdIso_em_es2Up", "weight_muonIdIso_em_es2Down"}, {"up", "down"}, "muES_eta1p2to2p1")
                  .Vary("weight_muonIdIso_nominal", getMuonEnergyScale_variations, {"muES_eta2p1to2p4", "channel", "gen_match_2", "eta_2", "weight_muonIdIso_nominal", "weight_muonIdIso_em_es2Up", "weight_muonIdIso_em_es2Down"}, {"up", "down"}, "muES_eta2p1to2p4")


                  // Triggers
                  .Vary("passCross_leadingMuon_em_nominal", getMuonEnergyScale_variations_int, {"muES_eta0to1p2",   "channel", "gen_match_2", "eta_2", "passCross_leadingMuon_em_nominal", "passCross_leadingMuon_em_es2Up", "passCross_leadingMuon_em_es2Down"}, {"up", "down"}, "muES_eta0to1p2")
                  .Vary("passCross_leadingMuon_em_nominal", getMuonEnergyScale_variations_int, {"muES_eta1p2to2p1", "channel", "gen_match_2", "eta_2", "passCross_leadingMuon_em_nominal", "passCross_leadingMuon_em_es2Up", "passCross_leadingMuon_em_es2Down"}, {"up", "down"}, "muES_eta1p2to2p1")
                  .Vary("passCross_leadingMuon_em_nominal", getMuonEnergyScale_variations_int, {"muES_eta2p1to2p4", "channel", "gen_match_2", "eta_2", "passCross_leadingMuon_em_nominal", "passCross_leadingMuon_em_es2Up", "passCross_leadingMuon_em_es2Down"}, {"up", "down"}, "muES_eta2p1to2p4")

                  .Vary("passCross_leadingEle_em_nominal", getMuonEnergyScale_variations_int, {"muES_eta0to1p2",   "channel", "gen_match_2", "eta_2", "passCross_leadingEle_em_nominal", "passCross_leadingEle_em_es2Up", "passCross_leadingEle_em_es2Down"}, {"up", "down"}, "muES_eta0to1p2")
                  .Vary("passCross_leadingEle_em_nominal", getMuonEnergyScale_variations_int, {"muES_eta1p2to2p1", "channel", "gen_match_2", "eta_2", "passCross_leadingEle_em_nominal", "passCross_leadingEle_em_es2Up", "passCross_leadingEle_em_es2Down"}, {"up", "down"}, "muES_eta1p2to2p1")
                  .Vary("passCross_leadingEle_em_nominal", getMuonEnergyScale_variations_int, {"muES_eta2p1to2p4", "channel", "gen_match_2", "eta_2", "passCross_leadingEle_em_nominal", "passCross_leadingEle_em_es2Up", "passCross_leadingEle_em_es2Down"}, {"up", "down"}, "muES_eta2p1to2p4")

                  .Vary("passCrossTrigger_em_nominal", getMuonEnergyScale_variations_int, {"muES_eta0to1p2",   "channel", "gen_match_2", "eta_2", "passCrossTrigger_em_nominal", "passCrossTrigger_em_es2Up", "passCrossTrigger_em_es2Down"}, {"up", "down"}, "muES_eta0to1p2")
                  .Vary("passCrossTrigger_em_nominal", getMuonEnergyScale_variations_int, {"muES_eta1p2to2p1", "channel", "gen_match_2", "eta_2", "passCrossTrigger_em_nominal", "passCrossTrigger_em_es2Up", "passCrossTrigger_em_es2Down"}, {"up", "down"}, "muES_eta1p2to2p1")
                  .Vary("passCrossTrigger_em_nominal", getMuonEnergyScale_variations_int, {"muES_eta2p1to2p4", "channel", "gen_match_2", "eta_2", "passCrossTrigger_em_nominal", "passCrossTrigger_em_es2Up", "passCrossTrigger_em_es2Down"}, {"up", "down"}, "muES_eta2p1to2p4")

                  // Other weights
                  .Vary("weight_correction_nominal", getMuonEnergyScale_variations, {"muES_eta0to1p2",   "channel", "gen_match_2", "eta_2", "weight_correction_nominal", "weight_correction_es2Up", "weight_correction_es2Down"}, {"up", "down"}, "muES_eta0to1p2")
                  .Vary("weight_correction_nominal", getMuonEnergyScale_variations, {"muES_eta1p2to2p1", "channel", "gen_match_2", "eta_2", "weight_correction_nominal", "weight_correction_es2Up", "weight_correction_es2Down"}, {"up", "down"}, "muES_eta1p2to2p1")
                  .Vary("weight_correction_nominal", getMuonEnergyScale_variations, {"muES_eta2p1to2p4", "channel", "gen_match_2", "eta_2", "weight_correction_nominal", "weight_correction_es2Up", "weight_correction_es2Down"}, {"up", "down"}, "muES_eta2p1to2p4")

                  .Vary("weight_closure_nominal", getMuonEnergyScale_variations, {"muES_eta0to1p2",   "channel", "gen_match_2", "eta_2", "weight_closure_nominal", "weight_closure_es2Up", "weight_closure_es2Down"}, {"up", "down"}, "muES_eta0to1p2")
                  .Vary("weight_closure_nominal", getMuonEnergyScale_variations, {"muES_eta1p2to2p1", "channel", "gen_match_2", "eta_2", "weight_closure_nominal", "weight_closure_es2Up", "weight_closure_es2Down"}, {"up", "down"}, "muES_eta1p2to2p1")
                  .Vary("weight_closure_nominal", getMuonEnergyScale_variations, {"muES_eta2p1to2p4", "channel", "gen_match_2", "eta_2", "weight_closure_nominal", "weight_closure_es2Up", "weight_closure_es2Down"}, {"up", "down"}, "muES_eta2p1to2p4");

      }
    }
  }
  return df;
}

/******************************************************************************/

#endif
