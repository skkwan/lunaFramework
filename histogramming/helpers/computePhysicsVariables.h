// computePhysicsVariables.h
// Helper functions for postprocess

#ifndef COMPUTE_PHYSICS_VARIABLES_H
#define COMPUTE_PHYSICS_VARIABLES_H

#include <TVector3.h>

#include "helperFunctions.h"

/******************************************************************************/

auto add_p4 = [](float pt, float eta, float phi, float mass)
{
  return ROOT::Math::PtEtaPhiMVector(pt, eta, phi, mass);
};

auto compute_mt = [](float pt_1, float phi_1, float pt_met, float phi_met)
{
  const auto dphi = Helper::compute_deltaPhi(phi_1, phi_met);
  return std::sqrt(2.0 * pt_1 * pt_met * (1.0 - std::cos(dphi)));
};

/******************************************************************************/




/*------------------------------------------------------------------*/

#endif
