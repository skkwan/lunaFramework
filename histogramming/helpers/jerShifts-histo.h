// Helper functions for b-tag jet counting with jet systematics.

#ifndef JER_SHIFS_HISTO_H_INCL
#define JER_SHIFS_HISTO_H_INCL

#include "histoConfig_class.h"

/******************************************************************************/

/*
 * B-jet counting: Defines hasLeadingBTagJet and hasSubleadBTagJet, with different
 * systematics hasLeadingBTagJet_{sysname}_{year}{Up/Down} since the b-jet pT systematics
 * may alter the b-jet counting.
 */

template <typename T>
auto GetJERShifts(T &df, LUNA::histoConfig_t &hConfig){

  if (hConfig.isMC() && hConfig.doShifts()) {
    // Simultaneous .Vary() of the two b-tag jet's pT, met, and metphi
    // In the end we did not change met/metphi with the tau energy scale
    return df.Vary({"bpt_deepflavour_1", "bm_deepflavour_1", "bpt_deepflavour_2", "bm_deepflavour_2", "met_nominal", "metphi_nominal", "nbtag20_nominal", "weight_btagEff_nominal"}, "ROOT::RVec<ROOT::RVecF>{{bpt_deepflavour_JERUp_1, bpt_deepflavour_JERDown_1}, {bm_deepflavour_JERUp_1, bm_deepflavour_JERDown_1}, {bpt_deepflavour_JERUp_2, bpt_deepflavour_JERDown_2}, {bm_deepflavour_JERUp_2, bm_deepflavour_JERDown_2}, {met_JERUp, met_JERDown}, {metphi_JERUp, metphi_JERDown}, {nbtag20_JER_Up, nbtag20_JER_Down}, {weight_btagEff_JER_Up, weight_btagEff_JER_Down}}", {"up", "down"}, "JER")
             .Vary({"bpt_deepflavour_1", "bm_deepflavour_1", "bpt_deepflavour_2", "bm_deepflavour_2", "met_nominal", "metphi_nominal", "nbtag20_nominal", "weight_btagEff_nominal"}, "ROOT::RVec<ROOT::RVecF>{{bpt_deepflavour_JetAbsoluteUp_1, bpt_deepflavour_JetAbsoluteDown_1}, {bm_deepflavour_JetAbsoluteUp_1, bm_deepflavour_JetAbsoluteDown_1}, {bpt_deepflavour_JetAbsoluteUp_2, bpt_deepflavour_JetAbsoluteDown_2}, {bm_deepflavour_JetAbsoluteUp_2, bm_deepflavour_JetAbsoluteDown_2}, {met_JetAbsoluteUp, met_JetAbsoluteDown}, {metphi_JetAbsoluteUp, metphi_JetAbsoluteDown}, {nbtag20_JetAbsolute_Up, nbtag20_JetAbsolute_Down}, {weight_btagEff_JetAbsolute_Up, weight_btagEff_JetAbsolute_Down}}", {"up", "down"}, "JetAbsolute")
             .Vary({"bpt_deepflavour_1", "bm_deepflavour_1", "bpt_deepflavour_2", "bm_deepflavour_2", "met_nominal", "metphi_nominal", "nbtag20_nominal", "weight_btagEff_nominal"}, "ROOT::RVec<ROOT::RVecF>{{bpt_deepflavour_JetAbsoluteyearUp_1, bpt_deepflavour_JetAbsoluteyearDown_1}, {bm_deepflavour_JetAbsoluteyearUp_1, bm_deepflavour_JetAbsoluteyearDown_1}, {bpt_deepflavour_JetAbsoluteyearUp_2, bpt_deepflavour_JetAbsoluteyearDown_2}, {bm_deepflavour_JetAbsoluteyearUp_2, bm_deepflavour_JetAbsoluteyearDown_2}, {met_JetAbsoluteyearUp, met_JetAbsoluteyearDown}, {metphi_JetAbsoluteyearUp, metphi_JetAbsoluteyearDown}, {nbtag20_JetAbsoluteyear_Up, nbtag20_JetAbsoluteyear_Down}, {weight_btagEff_JetAbsoluteyear_Up, weight_btagEff_JetAbsoluteyear_Down}}", {"up", "down"}, "JetAbsoluteyear")
             .Vary({"bpt_deepflavour_1", "bm_deepflavour_1", "bpt_deepflavour_2", "bm_deepflavour_2", "met_nominal", "metphi_nominal", "nbtag20_nominal", "weight_btagEff_nominal"}, "ROOT::RVec<ROOT::RVecF>{{bpt_deepflavour_JetBBEC1Up_1, bpt_deepflavour_JetBBEC1Down_1}, {bm_deepflavour_JetBBEC1Up_1, bm_deepflavour_JetBBEC1Down_1}, {bpt_deepflavour_JetBBEC1Up_2, bpt_deepflavour_JetBBEC1Down_2}, {bm_deepflavour_JetBBEC1Up_2, bm_deepflavour_JetBBEC1Down_2}, {met_JetBBEC1Up, met_JetBBEC1Down}, {metphi_JetBBEC1Up, metphi_JetBBEC1Down}, {nbtag20_JetBBEC1_Up, nbtag20_JetBBEC1_Down}, {weight_btagEff_JetBBEC1_Up, weight_btagEff_JetBBEC1_Down}}", {"up", "down"}, "JetBBEC1")
             .Vary({"bpt_deepflavour_1", "bm_deepflavour_1", "bpt_deepflavour_2", "bm_deepflavour_2", "met_nominal", "metphi_nominal", "nbtag20_nominal", "weight_btagEff_nominal"}, "ROOT::RVec<ROOT::RVecF>{{bpt_deepflavour_JetBBEC1yearUp_1, bpt_deepflavour_JetBBEC1yearDown_1}, {bm_deepflavour_JetBBEC1yearUp_1, bm_deepflavour_JetBBEC1yearDown_1}, {bpt_deepflavour_JetBBEC1yearUp_2, bpt_deepflavour_JetBBEC1yearDown_2}, {bm_deepflavour_JetBBEC1yearUp_2, bm_deepflavour_JetBBEC1yearDown_2}, {met_JetBBEC1yearUp, met_JetBBEC1yearDown}, {metphi_JetBBEC1yearUp, metphi_JetBBEC1yearDown}, {nbtag20_JetBBEC1year_Up, nbtag20_JetBBEC1year_Down}, {weight_btagEff_JetBBEC1year_Up, weight_btagEff_JetBBEC1year_Down}}", {"up", "down"}, "JetBBEC1year")
             .Vary({"bpt_deepflavour_1", "bm_deepflavour_1", "bpt_deepflavour_2", "bm_deepflavour_2", "met_nominal", "metphi_nominal", "nbtag20_nominal", "weight_btagEff_nominal"}, "ROOT::RVec<ROOT::RVecF>{{bpt_deepflavour_JetEC2Up_1, bpt_deepflavour_JetEC2Down_1}, {bm_deepflavour_JetEC2Up_1, bm_deepflavour_JetEC2Down_1}, {bpt_deepflavour_JetEC2Up_2, bpt_deepflavour_JetEC2Down_2}, {bm_deepflavour_JetEC2Up_2, bm_deepflavour_JetEC2Down_2}, {met_JetEC2Up, met_JetEC2Down}, {metphi_JetEC2Up, metphi_JetEC2Down}, {nbtag20_JetEC2_Up, nbtag20_JetEC2_Down}, {weight_btagEff_JetEC2_Up, weight_btagEff_JetEC2_Down}}", {"up", "down"}, "JetEC2")
             .Vary({"bpt_deepflavour_1", "bm_deepflavour_1", "bpt_deepflavour_2", "bm_deepflavour_2", "met_nominal", "metphi_nominal", "nbtag20_nominal", "weight_btagEff_nominal"}, "ROOT::RVec<ROOT::RVecF>{{bpt_deepflavour_JetEC2yearUp_1, bpt_deepflavour_JetEC2yearDown_1}, {bm_deepflavour_JetEC2yearUp_1, bm_deepflavour_JetEC2yearDown_1}, {bpt_deepflavour_JetEC2yearUp_2, bpt_deepflavour_JetEC2yearDown_2}, {bm_deepflavour_JetEC2yearUp_2, bm_deepflavour_JetEC2yearDown_2}, {met_JetEC2yearUp, met_JetEC2yearDown}, {metphi_JetEC2yearUp, metphi_JetEC2yearDown}, {nbtag20_JetEC2year_Up, nbtag20_JetEC2_Down}, {weight_btagEff_JetEC2year_Up, weight_btagEff_JetEC2year_Down}}", {"up", "down"}, "JetEC2year")
             .Vary({"bpt_deepflavour_1", "bm_deepflavour_1", "bpt_deepflavour_2", "bm_deepflavour_2", "met_nominal", "metphi_nominal", "nbtag20_nominal", "weight_btagEff_nominal"}, "ROOT::RVec<ROOT::RVecF>{{bpt_deepflavour_JetFlavorQCDUp_1, bpt_deepflavour_JetFlavorQCDDown_1}, {bm_deepflavour_JetFlavorQCDUp_1, bm_deepflavour_JetFlavorQCDDown_1}, {bpt_deepflavour_JetFlavorQCDUp_2, bpt_deepflavour_JetFlavorQCDDown_2}, {bm_deepflavour_JetFlavorQCDUp_2, bm_deepflavour_JetFlavorQCDDown_2}, {met_JetFlavorQCDUp, met_JetFlavorQCDDown}, {metphi_JetFlavorQCDUp, metphi_JetFlavorQCDDown}, {nbtag20_JetFlavorQCD_Up, nbtag20_JetFlavorQCD_Down}, {weight_btagEff_JetFlavorQCD_Up, weight_btagEff_JetFlavorQCD_Down}}", {"up", "down"}, "JetFlavorQCD")
             .Vary({"bpt_deepflavour_1", "bm_deepflavour_1", "bpt_deepflavour_2", "bm_deepflavour_2", "met_nominal", "metphi_nominal", "nbtag20_nominal", "weight_btagEff_nominal"}, "ROOT::RVec<ROOT::RVecF>{{bpt_deepflavour_JetHFUp_1, bpt_deepflavour_JetHFDown_1}, {bm_deepflavour_JetHFUp_1, bm_deepflavour_JetHFDown_1}, {bpt_deepflavour_JetHFUp_2, bpt_deepflavour_JetHFDown_2}, {bm_deepflavour_JetHFUp_2, bm_deepflavour_JetHFDown_2}, {met_JetHFUp, met_JetHFDown}, {metphi_JetHFUp, metphi_JetHFDown}, {nbtag20_JetHF_Up, nbtag20_JetHF_Down}, {weight_btagEff_JetHF_Up, weight_btagEff_JetHF_Down}}", {"up", "down"}, "JetHF")
             .Vary({"bpt_deepflavour_1", "bm_deepflavour_1", "bpt_deepflavour_2", "bm_deepflavour_2", "met_nominal", "metphi_nominal", "nbtag20_nominal", "weight_btagEff_nominal"}, "ROOT::RVec<ROOT::RVecF>{{bpt_deepflavour_JetHFyearUp_1, bpt_deepflavour_JetHFyearDown_1}, {bm_deepflavour_JetHFyearUp_1, bm_deepflavour_JetHFyearDown_1}, {bpt_deepflavour_JetHFyearUp_2, bpt_deepflavour_JetHFyearDown_2}, {bm_deepflavour_JetHFyearUp_2, bm_deepflavour_JetHFyearDown_2}, {met_JetHFyearUp, met_JetHFyearDown}, {metphi_JetHFyearUp, metphi_JetHFyearDown}, {nbtag20_JetHFyear_Up, nbtag20_JetHFyear_Down}, {weight_btagEff_JetHFyear_Up, weight_btagEff_JetHFyear_Down}}", {"up", "down"}, "JetHFyear")
             .Vary({"bpt_deepflavour_1", "bm_deepflavour_1", "bpt_deepflavour_2", "bm_deepflavour_2", "met_nominal", "metphi_nominal", "nbtag20_nominal", "weight_btagEff_nominal"}, "ROOT::RVec<ROOT::RVecF>{{bpt_deepflavour_JetRelativeBalUp_1, bpt_deepflavour_JetRelativeBalDown_1}, {bm_deepflavour_JetRelativeBalUp_1, bm_deepflavour_JetRelativeBalDown_1}, {bpt_deepflavour_JetRelativeBalUp_2, bpt_deepflavour_JetRelativeBalDown_2}, {bm_deepflavour_JetRelativeBalUp_2, bm_deepflavour_JetRelativeBalDown_2}, {met_JetRelativeBalUp, met_JetRelativeBalDown}, {metphi_JetRelativeBalUp, metphi_JetRelativeBalDown}, {nbtag20_JetRelativeBal_Up, nbtag20_JetRelativeBal_Down}, {weight_btagEff_JetRelativeBal_Up, weight_btagEff_JetRelativeBal_Down}}", {"up", "down"}, "JetRelativeBal")
             .Vary({"bpt_deepflavour_1", "bm_deepflavour_1", "bpt_deepflavour_2", "bm_deepflavour_2", "met_nominal", "metphi_nominal", "nbtag20_nominal", "weight_btagEff_nominal"}, "ROOT::RVec<ROOT::RVecF>{{bpt_deepflavour_JetRelativeSampleUp_1, bpt_deepflavour_JetRelativeSampleDown_1}, {bm_deepflavour_JetRelativeSampleUp_1, bm_deepflavour_JetRelativeSampleDown_1}, {bpt_deepflavour_JetRelativeSampleUp_2, bpt_deepflavour_JetRelativeSampleDown_2}, {bm_deepflavour_JetRelativeSampleUp_2, bm_deepflavour_JetRelativeSampleDown_2}, {met_JetRelativeSampleUp, met_JetRelativeSampleDown}, {metphi_JetRelativeSampleUp, metphi_JetRelativeSampleDown}, {nbtag20_JetRelativeSample_Up, nbtag20_JetRelativeSample_Down}, {weight_btagEff_JetRelativeSample_Up, weight_btagEff_JetRelativeSample_Down}}", {"up", "down"}, "JetRelativeSample")

              // Cut-based variables. Leading jet only goes into m_{btautau}
             .Vary("m_btautau_vis_nominal", "ROOT::RVecF{m_btautau_vis_JERUp_1,               m_btautau_vis_JERDown_1}", {"up", "down"}, "JER")
             .Vary("m_btautau_vis_nominal", "ROOT::RVecF{m_btautau_vis_JetAbsoluteUp_1,       m_btautau_vis_JetAbsoluteDown_1}", {"up", "down"}, "JetAbsolute")
             .Vary("m_btautau_vis_nominal", "ROOT::RVecF{m_btautau_vis_JetAbsoluteyearUp_1,   m_btautau_vis_JetAbsoluteyearDown_1}", {"up", "down"}, "JetAbsoluteyear")
             .Vary("m_btautau_vis_nominal", "ROOT::RVecF{m_btautau_vis_JetBBEC1Up_1,          m_btautau_vis_JetBBEC1Down_1}", {"up", "down"}, "JetBBEC1")
             .Vary("m_btautau_vis_nominal", "ROOT::RVecF{m_btautau_vis_JetBBEC1yearUp_1,      m_btautau_vis_JetBBEC1yearDown_1}", {"up", "down"}, "JetBBEC1year")
             .Vary("m_btautau_vis_nominal", "ROOT::RVecF{m_btautau_vis_JetEC2Up_1,            m_btautau_vis_JetEC2Down_1}", {"up", "down"}, "JetEC2")
             .Vary("m_btautau_vis_nominal", "ROOT::RVecF{m_btautau_vis_JetEC2yearUp_1,        m_btautau_vis_JetEC2yearDown_1}", {"up", "down"}, "JetEC2year")
             .Vary("m_btautau_vis_nominal", "ROOT::RVecF{m_btautau_vis_JetFlavorQCDUp_1,      m_btautau_vis_JetFlavorQCDDown_1}", {"up", "down"}, "JetFlavorQCD")
             .Vary("m_btautau_vis_nominal", "ROOT::RVecF{m_btautau_vis_JetHFUp_1,             m_btautau_vis_JetHFDown_1}", {"up", "down"}, "JetHF")
             .Vary("m_btautau_vis_nominal", "ROOT::RVecF{m_btautau_vis_JetHFyearUp_1,         m_btautau_vis_JetHFyearDown_1}", {"up", "down"}, "JetHFyear")
             .Vary("m_btautau_vis_nominal", "ROOT::RVecF{m_btautau_vis_JetRelativeBalUp_1,    m_btautau_vis_JetRelativeBalDown_1}", {"up", "down"}, "JetRelativeBal")
             .Vary("m_btautau_vis_nominal", "ROOT::RVecF{m_btautau_vis_JetRelativeSampleUp_1, m_btautau_vis_JetRelativeSampleDown_1}", {"up", "down"}, "JetRelativeSample")

             .Vary("pt_btautau_vis_nominal", "ROOT::RVecF{pt_btautau_vis_JERUp_1,               pt_btautau_vis_JERDown_1}", {"up", "down"}, "JER")
             .Vary("pt_btautau_vis_nominal", "ROOT::RVecF{pt_btautau_vis_JetAbsoluteUp_1,       pt_btautau_vis_JetAbsoluteDown_1}", {"up", "down"}, "JetAbsolute")
             .Vary("pt_btautau_vis_nominal", "ROOT::RVecF{pt_btautau_vis_JetAbsoluteyearUp_1,   pt_btautau_vis_JetAbsoluteyearDown_1}", {"up", "down"}, "JetAbsoluteyear")
             .Vary("pt_btautau_vis_nominal", "ROOT::RVecF{pt_btautau_vis_JetBBEC1Up_1,          pt_btautau_vis_JetBBEC1Down_1}", {"up", "down"}, "JetBBEC1")
             .Vary("pt_btautau_vis_nominal", "ROOT::RVecF{pt_btautau_vis_JetBBEC1yearUp_1,      pt_btautau_vis_JetBBEC1yearDown_1}", {"up", "down"}, "JetBBEC1year")
             .Vary("pt_btautau_vis_nominal", "ROOT::RVecF{pt_btautau_vis_JetEC2Up_1,            pt_btautau_vis_JetEC2Down_1}", {"up", "down"}, "JetEC2")
             .Vary("pt_btautau_vis_nominal", "ROOT::RVecF{pt_btautau_vis_JetEC2yearUp_1,        pt_btautau_vis_JetEC2yearDown_1}", {"up", "down"}, "JetEC2year")
             .Vary("pt_btautau_vis_nominal", "ROOT::RVecF{pt_btautau_vis_JetFlavorQCDUp_1,      pt_btautau_vis_JetFlavorQCDDown_1}", {"up", "down"}, "JetFlavorQCD")
             .Vary("pt_btautau_vis_nominal", "ROOT::RVecF{pt_btautau_vis_JetHFUp_1,             pt_btautau_vis_JetHFDown_1}", {"up", "down"}, "JetHF")
             .Vary("pt_btautau_vis_nominal", "ROOT::RVecF{pt_btautau_vis_JetHFyearUp_1,         pt_btautau_vis_JetHFyearDown_1}", {"up", "down"}, "JetHFyear")
             .Vary("pt_btautau_vis_nominal", "ROOT::RVecF{pt_btautau_vis_JetRelativeBalUp_1,    pt_btautau_vis_JetRelativeBalDown_1}", {"up", "down"}, "JetRelativeBal")
             .Vary("pt_btautau_vis_nominal", "ROOT::RVecF{pt_btautau_vis_JetRelativeSampleUp_1, pt_btautau_vis_JetRelativeSampleDown_1}", {"up", "down"}, "JetRelativeSample")

             // Shifts of D_zeta from MET
             .Vary("D_zeta_nominal", "ROOT::RVecF{D_zeta_JERUp,               D_zeta_JERDown}", {"up", "down"}, "JER")
             .Vary("D_zeta_nominal", "ROOT::RVecF{D_zeta_JetAbsoluteUp,       D_zeta_JetAbsoluteDown}", {"up", "down"}, "JetAbsolute")
             .Vary("D_zeta_nominal", "ROOT::RVecF{D_zeta_JetAbsoluteyearUp,   D_zeta_JetAbsoluteyearDown}", {"up", "down"}, "JetAbsoluteyear")
             .Vary("D_zeta_nominal", "ROOT::RVecF{D_zeta_JetBBEC1Up,          D_zeta_JetBBEC1Down}", {"up", "down"}, "JetBBEC1")
             .Vary("D_zeta_nominal", "ROOT::RVecF{D_zeta_JetBBEC1yearUp,      D_zeta_JetBBEC1yearDown}", {"up", "down"}, "JetBBEC1year")
             .Vary("D_zeta_nominal", "ROOT::RVecF{D_zeta_JetEC2Up,            D_zeta_JetEC2Down}", {"up", "down"}, "JetEC2")
             .Vary("D_zeta_nominal", "ROOT::RVecF{D_zeta_JetEC2yearUp,        D_zeta_JetEC2yearDown}", {"up", "down"}, "JetEC2year")
             .Vary("D_zeta_nominal", "ROOT::RVecF{D_zeta_JetFlavorQCDUp,      D_zeta_JetFlavorQCDDown}", {"up", "down"}, "JetFlavorQCD")
             .Vary("D_zeta_nominal", "ROOT::RVecF{D_zeta_JetHFUp,             D_zeta_JetHFDown}", {"up", "down"}, "JetHF")
             .Vary("D_zeta_nominal", "ROOT::RVecF{D_zeta_JetHFyearUp,         D_zeta_JetHFyearDown}", {"up", "down"}, "JetHFyear")
             .Vary("D_zeta_nominal", "ROOT::RVecF{D_zeta_JetRelativeBalUp,    D_zeta_JetRelativeBalDown}", {"up", "down"}, "JetRelativeBal")
             .Vary("D_zeta_nominal", "ROOT::RVecF{D_zeta_JetRelativeSampleUp, D_zeta_JetRelativeSampleDown}", {"up", "down"}, "JetRelativeSample");

  }
  return df;

}

/******************************************************************************/

#endif
