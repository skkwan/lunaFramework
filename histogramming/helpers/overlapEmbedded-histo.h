#ifndef OVERLAP_EMBEDDED_HISTO_H_INCL
#define OVERLAP_EMBEDDED_HISTO_H_INCL

#include "helperFunctions.h"
#include "histoConfig_class.h"

/**********************************************************/

/*
 * When selecting two muons for embedded events, we also selected around 2% of events from processes other than Z decays. Some of these, such as QCD or
 * Z->tautau->mumu, are restricted to a low mass range and are negligible in the kinematic regime defined by the cuts applied in the generator step.
 * Contamination from other processes, ttbar and diboson, spread across the whole mass range. These events should be vetoed. Veto all events in which both
 * top quarks decay into genuine tau leptons. These are contained in the embedded event sample.
 */

bool passFinalOverlapRemovalWithEmbedded(bool isEmbedOrDataOrSpecial, int channel, unsigned int gen_match_1, unsigned int gen_match_2) {

    // We Filter on the return value. If Embed or Data, do not remove overlap with Embedded
    if (isEmbedOrDataOrSpecial) return true;

    bool passFinalOverlapRemoval = true;

    // For each channel, take the event from Embedded if both legs came from a genuine tau, otherwise take it from MC
    if (channel == Helper::mt) {
        passFinalOverlapRemoval = !((gen_match_1 == 4) && (gen_match_2 == 5));
    }
    else if (channel == Helper::et) {
        passFinalOverlapRemoval = !((gen_match_1 == 3) && (gen_match_2 == 5));
    }
    else if (channel == Helper::em) {
        passFinalOverlapRemoval = !((gen_match_1 == 3) && (gen_match_2 == 4));
    }

    return passFinalOverlapRemoval;
}

/**********************************************************/


template <typename T>
auto PrepareBranchesFilterMCOverlapWithEmbedded(T &df, LUNA::histoConfig_t &hConfig) {

    // Only check the overlap, if the sample is not embedded, not data, not signal, and not SM Higgs
    bool isEmbedOrDataOrSpecial = (hConfig.isEmbedded() || hConfig.isData() || hConfig.isSignal() || (hConfig.name().find("HTo") != std::string::npos));
    bool isMC_nonHiggs_nonDY = hConfig.isMCnonHiggs() && !(hConfig.isDY());

    return df.Define("isEmbedOrDataOrSpecial", [isEmbedOrDataOrSpecial]() { return isEmbedOrDataOrSpecial; })
             .Define("isMC_nonHiggs_nonDY", [isMC_nonHiggs_nonDY]() { return isMC_nonHiggs_nonDY; })
             .Define("pass_final_embed_overlap_removal", passFinalOverlapRemovalWithEmbedded, {"isEmbedOrDataOrSpecial", "channel", "gen_match_1", "gen_match_2"});

}

/**********************************************************/


#endif
