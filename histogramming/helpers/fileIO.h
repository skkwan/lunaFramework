// Helper functions for file I/O (config file I/O) and also writing/reading
// from a map to access other n-tuples.

#ifndef FILE_IO_H_INCL
#define FILE_IO_H_INCL

#include <fstream>
#include <iostream>
#include <regex>
#include <string>

#include "helperFunctions.h"
#include "histoConfig_class.h"

#include "ROOT/RDataFrame.hxx"
#include "ROOT/RResultPtr.hxx"
#include "TChain.h"

/******************************************************************************/

/*
 * Initialize the dataframe with branches needed for systematics.
 */

template <typename T>
auto InitializeDataframe(T &df, LUNA::histoConfig_t &sConfig) {

  bool isSignal = sConfig.isSignal();
  bool isMCnonHiggs = sConfig.isMCnonHiggs();

  return df.Define("channel_mt", [=]() { return Helper::mt; })
           .Define("channel_et", [=]() { return Helper::et; })
           .Define("channel_em", [=]() { return Helper::em; })
           .Define("zero", [](){ return 0.0f; })   // kind of weird but we need it for add_p4
           .Define("isSignal", [isSignal]() { return isSignal; })
           .Define("isMCnonHiggs", [isMCnonHiggs]() { return isMCnonHiggs; });

}

/******************************************************************************/

// /*
//  * Check if a string ends in a substring or starts with a substring.
//  * Maybe some day we will have C++20 compatability with ROOT :(
//  */

// static bool strEndsWith(const std::string str, const std::string suffix)
// {
//   // .*dm10$ will match whether dm10 is at the end of the string
//   return std::regex_match(str, std::regex(".*" + suffix + "$"));
// }


/******************************************************************************/

/*
 * Return the contents of inFile (text file) as a string.
 */

std::string getString(std::string inFile) {

  std::string s;

  std::ifstream f(inFile);
  std::stringstream buffer;
  buffer << f.rdbuf();

  s = buffer.str();

  return s;
}

/******************************************************************************/

/*
 * In big string "subject", replace all instances of string "search" with the string "replace", without
 * creating a new copy of the string (performs the substitution in place).
 * From https://github.com/skkwan/DataCardCreator/blob/master/StatTools/interface/DataCardCreatorHThTh_2016_RDF.h#L201-L212
 */

void ReplaceStringInPlace(std::string& subject, const std::string& search,
			  const std::string& replace) {
  size_t pos = 0;
  while ((pos = subject.find(search, pos)) != std::string::npos) {
    subject.replace(pos, search.length(), replace);
    pos += replace.length();
  }
}

/******************************************************************************/

/*
 * Return true if the dataframe has a branch "branch" and false otherwise.
 * The branch name must match exactly.
 */

template <typename T>
bool HasExactBranch(T &df, std::string branch) {

  bool hasBranch = false;

  // Get df column names (branches in the NanoAOD)
  auto colNames = df.GetColumnNames();
  for (auto &&colName : colNames) {
    if (colName == branch) {
      hasBranch = true;
      break;
    }
  }

  return hasBranch;
}


/******************************************************************************/

// Returns whether dataset with sampleName is available this year.

bool isSampleAvailable(std::string sampleName, std::map<std::string, std::string> map) {

  // std::cout << "isSampleAvailable: looking for " << sampleName << std::endl;

  std::map<std::string, std::string>::iterator it;
  it = map.find(sampleName);

  bool hasSample = (it != map.end());
  return hasSample;

}

/******************************************************************************/

// Return genEventSumw given the hRuns histogram created in skim.cxx, which just
// tallies genEventSumw in each ROOT file.

float getGenEventSumw(TH1F *h) {

  float genEventSumw = (float) h->GetBinContent(1);
  return genEventSumw;
}

/******************************************************************************/

// Return nEntries given the hEvents histogram created in skim.cxx.

float getNEntries(TH1F *h) {

  float getnEntries = (float) h->GetBinContent(1);
  return getnEntries;
}


/******************************************************************************/

/*
 * Helper function for applying a vector of Defines to a dataframe (if df is a DataFrame it will be automatically
 * cast to a RNode).
 */

auto ApplyDefines(ROOT::RDF::RNode df, const std::vector<std::string> &names, const std::vector<std::string> &exprs,
                  unsigned int i = 0) {

  if (i == names.size())
    return df;

  return ApplyDefines(df.Define(names[i], exprs[i]), names, exprs, i + 1);
}


/*****************************************************************/

// Save the variables in finalVariables (checking if each one exists) from a
// dataframe df to a tree called treeName, in the output file.

template <typename T>
auto SnapshotFinalVariables(T &dfFinal,
          LUNA::histoConfig_t &sConfig,
			    std::string treeName,
			    std::string output,
			    std::vector<std::string> basicVariables,
          std::vector<std::string> shiftVariables,
			    std::vector<std::string> optionalVariables
			    ) {

  // The branches in "basicVariables" should be available
  std::vector<std::string> finalVariables = basicVariables;

  if (sConfig.doShifts()) {
    finalVariables.insert(finalVariables.end(), shiftVariables.begin(), shiftVariables.end());
  }

  // Now check if each branch in optionalVariables is in the dataframe
  for (std::string branch : optionalVariables) {
    std::cout << "Checking for optional variable " << branch << std::endl;
    if (HasExactBranch(dfFinal, branch)) {
      std::cout << "Optional variable found: " << branch << std::endl;
      finalVariables.push_back(branch);
    }
    else {
      std::cout << "Optional variable NOT found: " << branch << std::endl;
    }
  }

  // If you want to check if each jerSysVariables branch is there, use this block
  /* for (std::string branch : jerSysVariables) { */
  /*   if (HasExactBranch(dfFinal, branch)) { */
  /*     std::cout << branch << std::endl; */
  /*     finalVariables.push_back(branch); */
  /*   } */
  /* } */

  dfFinal.Snapshot(treeName, output, finalVariables);


}


/******************************************************************************/

#endif
