
// List of gen-level final variables in the skimmed n-tuple

#ifndef GEN_FINALVARIABLES_H
#define GEN_FINALVARIABLES_H

/*
 * Declare all variables which to appear in the final n-tuple. TO DO: make year-specific
 * designations.
 */

const std::vector<std::string> genFinalVariables = {



  // Ordered by particle type
  "genPt_tau1", "genPt_tau2",
  "genPt_b1", "genPt_b2",
  "genPt_trailing1", "genPt_trailing2",

  "genEta_tau1", "genEta_tau2",
  "genEta_b1", "genEta_b2",
  "genEta_trailing1", "genEta_trailing2",

  "genPhi_tau1", "genPhi_tau2",
  "genPhi_b1", "genPhi_b2",
  "genPhi_trailing1", "genPhi_trailing2",

  // Of the gen bb system
  "genPt_bb", "genEta_bb", "genPhi_bb", "genM_bb",

  // Matching to first bb
  "idx_ak4_matched_to_gen_bb", "idx_ak8_matched_to_gen_bb",
  "jetPt_ak4_matched", "jetEta_ak4_matched", "jetPhi_ak4_matched", "jetM_ak4_matched",
  "jetPt_ak8_matched", "jetEta_ak8_matched", "jetPhi_ak8_matched", "jetM_ak8_matched",

  "pt_resolution_ak4", "deltaR_ak4",
  "pt_resolution_ak8", "deltaR_ak8",

  // Matching to second bb (only meaningful if available)
  "idx_ak4_matched_to_second_gen_bb", "idx_ak8_matched_to_second_gen_bb",
  "jetPt_ak4_matched2", "jetEta_ak4_matched2", "jetPhi_ak4_matched2", "jetM_ak4_matched2",
  "jetPt_ak8_matched2", "jetEta_ak8_matched2", "jetPhi_ak8_matched2", "jetM_ak8_matched2",

  "pt_resolution_ak4_2", "deltaR_ak4_2",
  "pt_resolution_ak8_2", "deltaR_ak8_2",

  // Event-specific variables and variables for weighing
  "run", "evt", "lumi",
  "nEntries",
  "genWeight", "genEventCount", "genEventSumw",
  "scale",

};

#endif
