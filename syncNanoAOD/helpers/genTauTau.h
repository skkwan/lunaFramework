// Helper functions for the mu-tauh final state.

#ifndef GEN_TAU_TAU_H_INCL
#define GEN_TAU_TAU_H_INCL

#include "computePhysicsVariables.h"
#include "fileIO.h"
#include "finalVariables.h"
#include "genHelper.h"
#include "helperFunctions.h"
#include "muon.h"
#include "tau.h"

/***************************************************************************/

/*
 * Select a gen tau-tau pair (if it exists) and save its visible and total m(tautau).
 */

template <typename T>
auto FindGenTauTauPair(T &df, std::string output, LUNA::sampleConfig_t &sConfig) {

  // Depending on which sample, customize what we want to look for
  int targetParent_pdgId;        // Parent of the process (e.g. TTTo2L2Nu: top quark)
  int targetDirectParent_pdgId;  // DIRECT parent of the gen tau (e.g. for TTTo2L2Nu, since t-> bW: the direct parent is the W boson)
  bool lookForIntermediateBosonParent = true;    // Some MC samples may not save the intermediate boson parent
  if      (sConfig.name().find("SUSY") != std::string::npos) {
    targetParent_pdgId       = GenHelper::pdgId_a;
    targetDirectParent_pdgId = GenHelper::pdgId_a;
  }
  else if (sConfig.name().find("TTTo") != std::string::npos) {
    targetParent_pdgId       = GenHelper::pdgId_top;
    targetDirectParent_pdgId = GenHelper::pdgId_W; // DIFFERENT
  }
  else if (sConfig.name().find("DYJetsToLL_M-10to50") != std::string::npos) {
    targetParent_pdgId       = GenHelper::pdgId_Z;
    targetDirectParent_pdgId = GenHelper::pdgId_Z;
    lookForIntermediateBosonParent = false;        // Low-mass DY: DO NOT require direct parent
  }
  else if (sConfig.name().find("DY")   != std::string::npos) {
    targetParent_pdgId       = GenHelper::pdgId_Z;
    targetDirectParent_pdgId = GenHelper::pdgId_Z;
  }
  else {
    std::cout << ">>> [WARNING:] FindGenTauTauPair.h: default to finding pseudoscalar parent and direct parents" << std::endl;
    targetParent_pdgId = GenHelper::pdgId_a;
    targetDirectParent_pdgId = GenHelper::pdgId_a;
  }

  auto build_pair = [](RVec<int>& GenPart_genPartIdxMother,
		       RVec<int>& GenPart_pdgId, RVec<int>& GenPart_status,
		       RVec<int>& GenPart_statusFlags,
		       bool lookForIntermediateBosonParent,
		       const int targetParent_pdgId,
		       const int targetDirectParent_pdgId) { // only different in the case of TTTo2L2Nu
		       // unsigned int luminosityBlock, unsigned long long event) {

    std::vector<int> myTausFromParent;

    // Loop through genPart collection
    int nGenPart = GenPart_pdgId.size();
    for (int i = 0; i < nGenPart; i++) {

      // Only do this for taus
      if (abs(GenPart_pdgId[i]) == GenHelper::pdgId_tau) {

	// General check if it came from the parent we want (e.g. TTTo2L2Nu: the top quark)
	bool isFromParent = GenHelper::IsFromParent(GenPart_genPartIdxMother,
						    GenPart_pdgId,
						    targetParent_pdgId,
						    i);

	// Also check if it is a direct daughter of the boson in question (e.g. TTTo2L2Nu: the W boson)
	bool isDirectlyFromIntermediateBoson = GenHelper::IsDirectDecayProductOfParent(GenPart_genPartIdxMother,
									       GenPart_pdgId,
									       targetDirectParent_pdgId, // NOTE: different from the function call above!
									       i);
	// Check if it is from a hard process (n.b. use statusFlags not status)
	bool isFromHardProcess = GenHelper::fromHardProcess(GenPart_statusFlags[i]);

	// For most samples we can require the intermediate boson parent to be saved in the MC sample
	if (lookForIntermediateBosonParent) {
	  /* std::cout << "Looking for intermediate boson parent: "  */
	  /* 	    << "isFromParent: " << isFromParent << ", "  */
	  /* 	    << "isDirectlyFromIntermediateBoson: " << isDirectlyFromIntermediateBoson << ", "  */
	  /* 	    << "isFromHardProcess: " << isFromHardProcess << ", "  */
	  /* 	    << std::endl; */
	  if (isFromParent && isDirectlyFromIntermediateBoson && isFromHardProcess) {
	    myTausFromParent.push_back(i);
	  }
	}
	else { // if we do not require the intermediate boson parent, then just check if it is from a hard process
	  /* std::cout << "Not looking for intermediate boson parent: " */
	  /* 	    << "isFromHardProcess: " << isFromHardProcess */
	  /* 	    << std::endl; */
	  if (isFromHardProcess) {
	    myTausFromParent.push_back(i);
	  }
	}

      } // end of requirement that it is a gen tau
    } // end of loop over gen particles

    // If we failed to find sufficient gen-level taus, push back -1('s), otherwise we'll get a segfault later
    if (myTausFromParent.size() == 0) { myTausFromParent.push_back(-1); myTausFromParent.push_back(-1); }
    if (myTausFromParent.size() == 1) { myTausFromParent.push_back(-1); }

    // Print all the taus we found
    // std::cout << ">>> genTauTau.h: build_pair: lumi/event: " << luminosityBlock << ":" << event << ": taus from desired parent: ";
    // for (unsigned int i = 0; i < myTausFromParent.size(); i++) {
    //   std::cout << myTausFromParent[i] << " ";
    //}
    // std::cout << std::endl;

    // Return
    return myTausFromParent;
  };



  auto df2 = df.Define("targetParent_pdgId", [=]() { return targetParent_pdgId; }) // capture into dataframe
    .Define("targetDirectParent_pdgId", [=]() { return targetDirectParent_pdgId; }) // capture into dataframe
    .Define("lookForIntermediateBosonParent", [=]() { return lookForIntermediateBosonParent; }) // capture into dataframe
           .Define("gen_tautau_pairIdx", build_pair,
  		   {"GenPart_genPartIdxMother", "GenPart_pdgId", "GenPart_status", "GenPart_statusFlags",
		       "lookForIntermediateBosonParent",
  		       "targetParent_pdgId",
		       "targetDirectParent_pdgId"})
    .Define("genIdx_tau1", "gen_tautau_pairIdx[0]")
    .Define("genIdx_tau2", "gen_tautau_pairIdx[1]")
    .Filter("(genIdx_tau1 > -1) && (genIdx_tau2 > -1)", "Two gen-level taus found")

    // Kinematics and position information
    .Define("genPt_tau1",   "GenPart_pt[genIdx_tau1]")
    .Define("genEta_tau1",  "GenPart_eta[genIdx_tau1]")
    .Define("genPhi_tau1",  "GenPart_phi[genIdx_tau1]")
    .Define("genMass_tau1", "GenPart_mass[genIdx_tau1]")

    .Define("genPt_tau2",   "GenPart_pt[genIdx_tau2]")
    .Define("genEta_tau2",  "GenPart_eta[genIdx_tau2]")
    .Define("genPhi_tau2",  "GenPart_phi[genIdx_tau2]")
    .Define("genMass_tau2", "GenPart_mass[genIdx_tau2]")

    // See helpers/computePhysicsVariables.h for definitions
    .Define("genp4_tau1", add_p4, {"genPt_tau1", "genEta_tau1", "genPhi_tau1", "genMass_tau1"})
    .Define("genp4_tau2", add_p4, {"genPt_tau2", "genEta_tau2", "genPhi_tau2", "genMass_tau2"})
    .Define("gen_p4", "genp4_tau1 + genp4_tau2")
    .Define("gen_mtt",  "float(gen_p4.M())")
    .Define("gen_pTtt", "float(gen_p4.Pt())");

  return df2;

}



#endif
