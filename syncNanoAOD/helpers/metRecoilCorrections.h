// Helper functions for computing the MET recoil correction information at sync/skim-level.

#ifndef MET_RECOIL_CORRECTIONS_H_INCL
#define MET_RECOIL_CORRECTIONS_H_INCL

#include "computePhysicsVariables.h"
#include "fileIO.h"
#include "genHelper.h"
#include "helperFunctions.h"
#include "MEtSys.h"
#include "RecoilCorrector.h"
#include "recoilCorrectionsInfo.h"
#include "sampleConfig_class.h"

/***************************************************************************/

/*
 * Return the corrected MET 4-vector for the nominal recoil MET Type-1 correction.
 * Note that this takes the uncorrected met and metphi as arguments, not uncorrected met px and py.
 */
ROOT::Math::PtEtaPhiEVector getRecoilCorrectionNominal(RecoilCorrector recoilPFMetCorrector,
                                                       float uncorr_met, float uncorr_metphi,
                                                       float genpX, float genpY,
                                                       float vispX, float vispY,
                                                       float nRecoilJets) {
  float corrMetEx = 1.0;
  float corrMetEy = 1.0;
  recoilPFMetCorrector.CorrectByMeanResolution(uncorr_met * TMath::Cos(uncorr_metphi),
                                               uncorr_met * TMath::Sin(uncorr_metphi),
                                               genpX, genpY, vispX, vispY, nRecoilJets, corrMetEx, corrMetEy);
  ROOT::Math::PtEtaPhiEVector output;
  output.SetPxPyPzE(corrMetEx, corrMetEy, 0, 0);
  return output;
}

/***************************************************************************/


/*
 * Return the vector (corrected MET pX, corrected MET pY) after applying recoil
 * corrections. myMet{Px, Py} is the uncorrected MET. gen[pX, pY], vis[pX, pY] is the gen boson's total/visible p4.
 * sysType is "response" or "resolution". sysShift is "Up" or "Down".
 */

ROOT::Math::PtEtaPhiEVector getRecoilCorrectionShifts(
             MEtSys wMetSys_,
					   float myMetPx, float myMetPy,
					   float genpX,   float genpY,
					   float vispX,   float vispY,
					   float nRecoilJets, string sysType, string sysShift) {

  // Initialize the output vector
  ROOT::Math::PtEtaPhiEVector output;
  float corrMetEx = 1.0;
  float corrMetEy = 1.0;

  // For our recoil corrections, all are bosons
  int enumProcess = MEtSys::ProcessType::BOSON;
  int enumSys;
  int enumShift;

  if (sysType == "respose")    enumSys = MEtSys::SysType::Response;
  if (sysType == "resolution") enumSys = MEtSys::SysType::Resolution;
  if (sysShift == "Up")   enumShift =  MEtSys::SysShift::Up;
  if (sysShift == "Down") enumShift =  MEtSys::SysShift::Down;

  // Call applyMEtSys, which changes corrMetEx and corrMetEy
  wMetSys_.ApplyMEtSys(myMetPx, myMetPy, genpX, genpY,
		       vispX, vispY, nRecoilJets,
		       enumProcess,
		       enumSys, enumShift,
		       corrMetEx, corrMetEy);

  /* std::cout << "metRecoilCorrections.h: original METxy: "  */
  /* 	    << myMetPx << ", " << myMetPy << ". "  */
  /* 	    << sysType << " " << sysShift << ": "  */
  /* 	    << corrMetEx << ", " << corrMetEy << std::endl; */

  // Build and return the output
  output.SetPxPyPzE(corrMetEx, corrMetEy, 0, 0);
  return output;

}

/***************************************************************************/

/*
 * Get MET recoil corrections (response + resolution) for relevant samples.
 */

template <typename T>
auto GetMETRecoilCorrections(T &df, LUNA::sampleConfig_t &sConfig, RecoilCorrector &recoilPFMetCorrector_, MEtSys &wMetSys_) {



  std::cout << ">>> recoilCorrectionsInfo.h: get gen recoil boson information (do recoil)? " << sConfig.doRecoil() << std::endl;


  // Do recoil. "met_nominal" and "metphi_nominal" are the met/metphi with the nominal lepton energy scale correction applied. We use RDF "Redefine" on them.
  if (sConfig.doRecoil()) {

    // Save these branches before re-defining met_nominal and metphi_nominal.
    auto df2 = df.Define("metPx_noRecoilCorr", "(float) (met_nominal * TMath::Cos(metphi_nominal))")
                 .Define("metPy_noRecoilCorr", "(float) (met_nominal * TMath::Sin(metphi_nominal))")

                 .Define("p4met", [recoilPFMetCorrector_](float uncorr_met, float uncorr_metphi,
                                                       float genpX, float genpY,
                                                       float vispX, float vispY,
                                                       float nRecoilJets) { return getRecoilCorrectionNominal(recoilPFMetCorrector_, uncorr_met, uncorr_metphi, genpX, genpY, vispX, vispY, nRecoilJets); }, {"met_nominal", "metphi_nominal", "gen_recoilBoson_Px", "gen_recoilBoson_Py", "gen_recoilBoson_visPx", "gen_recoilBoson_visPy", "nRecoilJets"})
                 .Redefine("met_nominal",    "(float) p4met.Pt()")
                 .Redefine("metphi_nominal", "(float) p4met.Phi()")

                 // Systematic shifts
                 .Define("response",   [] { return (std::string) "response"; })
                 .Define("resolution", [] { return (std::string) "resolution"; })
                 .Define("Up",   [] { return (std::string) "Up"; })
                 .Define("Down", [] { return (std::string) "Down"; })
                 // Response Up
                 .Define("met_responseUp_4vec", [wMetSys_](float myMetPx, float myMetPy, float genpX, float genpY, float vispX, float vispY, float nRecoilJets, string sysType, string sysShift) {
                        return getRecoilCorrectionShifts(wMetSys_, myMetPx, myMetPy, genpX, genpY, vispX, vispY, nRecoilJets, sysType, sysShift); },
                                                                          {"metPx_noRecoilCorr", "metPy_noRecoilCorr", "gen_recoilBoson_Px", "gen_recoilBoson_Py", "gen_recoilBoson_visPx", "gen_recoilBoson_visPy",
	                                                                        "nRecoilJets", "response", "Up" })
                 .Define("met_responseUp",    "(float) met_responseUp_4vec.Pt()")
                 .Define("metphi_responseUp", "(float) met_responseUp_4vec.Phi()")
                 // Response Down
                 .Define("met_responseDown_4vec",  [wMetSys_](float myMetPx, float myMetPy, float genpX, float genpY, float vispX, float vispY, float nRecoilJets, string sysType, string sysShift) {
                        return getRecoilCorrectionShifts(wMetSys_, myMetPx, myMetPy, genpX, genpY, vispX, vispY, nRecoilJets, sysType, sysShift); },
                                                                          {"metPx_noRecoilCorr", "metPy_noRecoilCorr", "gen_recoilBoson_Px", "gen_recoilBoson_Py", "gen_recoilBoson_visPx", "gen_recoilBoson_visPy",
                                                                            "nRecoilJets", "response", "Down" })
                 .Define("met_responseDown",    "(float) met_responseDown_4vec.Pt()")
                 .Define("metphi_responseDown", "(float) met_responseDown_4vec.Phi()")
                 // Resolution Up
                 .Define("met_ResolutionUp_4vec",  [wMetSys_](float myMetPx, float myMetPy, float genpX, float genpY, float vispX, float vispY, float nRecoilJets, string sysType, string sysShift) {
                        return getRecoilCorrectionShifts(wMetSys_, myMetPx, myMetPy, genpX, genpY, vispX, vispY, nRecoilJets, sysType, sysShift); },
                                                                          {"metPx_noRecoilCorr", "metPy_noRecoilCorr", "gen_recoilBoson_Px", "gen_recoilBoson_Py", "gen_recoilBoson_visPx", "gen_recoilBoson_visPy",
                                                                            "nRecoilJets", "resolution", "Up" })
                 .Define("met_resolutionUp",    "(float) met_ResolutionUp_4vec.Pt()")  // unpack results
                 .Define("metphi_resolutionUp", "(float) met_ResolutionUp_4vec.Phi()") // unpack results
                 // Resolution Down
                 .Define("met_ResolutionDown_4vec",  [wMetSys_](float myMetPx, float myMetPy, float genpX, float genpY, float vispX, float vispY, float nRecoilJets, string sysType, string sysShift) {
                        return getRecoilCorrectionShifts(wMetSys_, myMetPx, myMetPy, genpX, genpY, vispX, vispY, nRecoilJets, sysType, sysShift); },
                                                                           {"metPx_noRecoilCorr", "metPy_noRecoilCorr", "gen_recoilBoson_Px", "gen_recoilBoson_Py", "gen_recoilBoson_visPx", "gen_recoilBoson_visPy",
                                                                            "nRecoilJets", "resolution", "Down" })
                 .Define("met_resolutionDown",    "(float) met_ResolutionDown_4vec.Pt()")   // unpack results
                 .Define("metphi_resolutionDown", "(float) met_ResolutionDown_4vec.Phi()"); // unpack results

    // If there are JER systematics present, also apply the (nominal) recoil correction
    int hasJERcorrections = HasExactBranch(df, "Jet_pt_nom");

    if (hasJERcorrections) {
      return df2.Define("p4met_recoil_jerUp", [recoilPFMetCorrector_](float uncorr_met, float uncorr_metphi,
                                                       float genpX, float genpY,
                                                       float vispX, float vispY,
                                                       float nRecoilJets) { return getRecoilCorrectionNominal(recoilPFMetCorrector_, uncorr_met, uncorr_metphi, genpX, genpY, vispX, vispY, nRecoilJets); }, {"met_JERUp", "metphi_JERUp",  "gen_recoilBoson_Px", "gen_recoilBoson_Py", "gen_recoilBoson_visPx", "gen_recoilBoson_visPy", "nRecoilJets_jerUp"})
                    .Redefine("met_JERUp",    [](ROOT::Math::PtEtaPhiEVector p4) { return (float) p4.Pt(); },  {"p4met_recoil_jerUp"})
                    .Redefine("metphi_JERUp", [](ROOT::Math::PtEtaPhiEVector p4) { return (float) p4.Phi(); }, {"p4met_recoil_jerUp"})
                    .Define("p4met_recoil_jerDown",[recoilPFMetCorrector_](float uncorr_met, float uncorr_metphi,
                                                       float genpX, float genpY,
                                                       float vispX, float vispY,
                                                       float nRecoilJets) { return getRecoilCorrectionNominal(recoilPFMetCorrector_, uncorr_met, uncorr_metphi, genpX, genpY, vispX, vispY, nRecoilJets); }, {"met_JERDown", "metphi_JERDown",  "gen_recoilBoson_Px", "gen_recoilBoson_Py", "gen_recoilBoson_visPx", "gen_recoilBoson_visPy", "nRecoilJets_jerDown"})
                    .Redefine("met_JERDown",    [](ROOT::Math::PtEtaPhiEVector p4) { return (float) p4.Pt(); },  {"p4met_recoil_jerDown"})
                    .Redefine("metphi_JERDown", [](ROOT::Math::PtEtaPhiEVector p4) { return (float) p4.Phi(); }, {"p4met_recoil_jerDown"})

                    .Define("p4met_recoil_jesAbsoluteUp", [recoilPFMetCorrector_](float uncorr_met, float uncorr_metphi,
                                                       float genpX, float genpY,
                                                       float vispX, float vispY,
                                                       float nRecoilJets) { return getRecoilCorrectionNominal(recoilPFMetCorrector_, uncorr_met, uncorr_metphi, genpX, genpY, vispX, vispY, nRecoilJets); }, {"met_JetAbsoluteUp", "metphi_JetAbsoluteUp",  "gen_recoilBoson_Px", "gen_recoilBoson_Py", "gen_recoilBoson_visPx", "gen_recoilBoson_visPy", "nRecoilJets_jesAbsoluteUp"})
                    .Redefine("met_JetAbsoluteUp",    [](ROOT::Math::PtEtaPhiEVector p4) { return (float) p4.Pt(); },  {"p4met_recoil_jesAbsoluteUp"})
                    .Redefine("metphi_JetAbsoluteUp", [](ROOT::Math::PtEtaPhiEVector p4) { return (float) p4.Phi(); }, {"p4met_recoil_jesAbsoluteUp"})
                    .Define("p4met_recoil_jesAbsoluteDown", [recoilPFMetCorrector_](float uncorr_met, float uncorr_metphi,
                                                       float genpX, float genpY,
                                                       float vispX, float vispY,
                                                       float nRecoilJets) { return getRecoilCorrectionNominal(recoilPFMetCorrector_, uncorr_met, uncorr_metphi, genpX, genpY, vispX, vispY, nRecoilJets); }, { "met_JetAbsoluteDown", "metphi_JetAbsoluteDown",  "gen_recoilBoson_Px", "gen_recoilBoson_Py", "gen_recoilBoson_visPx", "gen_recoilBoson_visPy", "nRecoilJets_jesAbsoluteDown"})
                    .Redefine("met_JetAbsoluteDown",    [](ROOT::Math::PtEtaPhiEVector p4) { return (float) p4.Pt(); },  {"p4met_recoil_jesAbsoluteDown"})
                    .Redefine("metphi_JetAbsoluteDown", [](ROOT::Math::PtEtaPhiEVector p4) { return (float) p4.Phi(); }, {"p4met_recoil_jesAbsoluteDown"})

                    .Define("p4met_recoil_jesAbsoluteyearUp", [recoilPFMetCorrector_](float uncorr_met, float uncorr_metphi,
                                                       float genpX, float genpY,
                                                       float vispX, float vispY,
                                                       float nRecoilJets) { return getRecoilCorrectionNominal(recoilPFMetCorrector_, uncorr_met, uncorr_metphi, genpX, genpY, vispX, vispY, nRecoilJets); }, {"met_JetAbsoluteyearUp", "metphi_JetAbsoluteyearUp",  "gen_recoilBoson_Px", "gen_recoilBoson_Py", "gen_recoilBoson_visPx", "gen_recoilBoson_visPy", "nRecoilJets_jesAbsoluteyearUp"})
                    .Redefine("met_JetAbsoluteyearUp",    [](ROOT::Math::PtEtaPhiEVector p4) { return (float) p4.Pt(); },  {"p4met_recoil_jesAbsoluteyearUp"})
                    .Redefine("metphi_JetAbsoluteyearUp", [](ROOT::Math::PtEtaPhiEVector p4) { return (float) p4.Phi(); }, {"p4met_recoil_jesAbsoluteyearUp"})
                    .Define("p4met_recoil_jesAbsoluteyearDown",[recoilPFMetCorrector_](float uncorr_met, float uncorr_metphi,
                                                       float genpX, float genpY,
                                                       float vispX, float vispY,
                                                       float nRecoilJets) { return getRecoilCorrectionNominal(recoilPFMetCorrector_, uncorr_met, uncorr_metphi, genpX, genpY, vispX, vispY, nRecoilJets); }, {"met_JetAbsoluteyearDown", "metphi_JetAbsoluteyearDown",  "gen_recoilBoson_Px", "gen_recoilBoson_Py", "gen_recoilBoson_visPx", "gen_recoilBoson_visPy", "nRecoilJets_jesAbsoluteyearDown"})
                    .Redefine("met_JetAbsoluteyearDown",    [](ROOT::Math::PtEtaPhiEVector p4) { return (float) p4.Pt(); },  {"p4met_recoil_jesAbsoluteyearDown"})
                    .Redefine("metphi_JetAbsoluteyearDown", [](ROOT::Math::PtEtaPhiEVector p4) { return (float) p4.Phi(); }, {"p4met_recoil_jesAbsoluteyearDown"})

                    .Define("p4met_recoil_jesBBEC1Up",[recoilPFMetCorrector_](float uncorr_met, float uncorr_metphi,
                                                       float genpX, float genpY,
                                                       float vispX, float vispY,
                                                       float nRecoilJets) { return getRecoilCorrectionNominal(recoilPFMetCorrector_, uncorr_met, uncorr_metphi, genpX, genpY, vispX, vispY, nRecoilJets); }, {"met_JetBBEC1Up", "metphi_JetBBEC1Up",  "gen_recoilBoson_Px", "gen_recoilBoson_Py", "gen_recoilBoson_visPx", "gen_recoilBoson_visPy", "nRecoilJets_jesBBEC1Up"})
                    .Redefine("met_JetBBEC1Up",    [](ROOT::Math::PtEtaPhiEVector p4) { return (float) p4.Pt(); },  {"p4met_recoil_jesBBEC1Up"})
                    .Redefine("metphi_JetBBEC1Up", [](ROOT::Math::PtEtaPhiEVector p4) { return (float) p4.Phi(); }, {"p4met_recoil_jesBBEC1Up"})
                    .Define("p4met_recoil_jesBBEC1Down", [recoilPFMetCorrector_](float uncorr_met, float uncorr_metphi,
                                                       float genpX, float genpY,
                                                       float vispX, float vispY,
                                                       float nRecoilJets) { return getRecoilCorrectionNominal(recoilPFMetCorrector_, uncorr_met, uncorr_metphi, genpX, genpY, vispX, vispY, nRecoilJets); }, {"met_JetBBEC1Down", "metphi_JetBBEC1Down",  "gen_recoilBoson_Px", "gen_recoilBoson_Py", "gen_recoilBoson_visPx", "gen_recoilBoson_visPy", "nRecoilJets_jesBBEC1Down"})
                    .Redefine("met_JetBBEC1Down",    [](ROOT::Math::PtEtaPhiEVector p4) { return (float) p4.Pt(); },  {"p4met_recoil_jesBBEC1Down"})
                    .Redefine("metphi_JetBBEC1Down", [](ROOT::Math::PtEtaPhiEVector p4) { return (float) p4.Phi(); }, {"p4met_recoil_jesBBEC1Down"})

                    .Define("p4met_recoil_jesBBEC1yearUp", [recoilPFMetCorrector_](float uncorr_met, float uncorr_metphi,
                                                       float genpX, float genpY,
                                                       float vispX, float vispY,
                                                       float nRecoilJets) { return getRecoilCorrectionNominal(recoilPFMetCorrector_, uncorr_met, uncorr_metphi, genpX, genpY, vispX, vispY, nRecoilJets); }, {"met_JetBBEC1yearUp", "metphi_JetBBEC1yearUp",  "gen_recoilBoson_Px", "gen_recoilBoson_Py", "gen_recoilBoson_visPx", "gen_recoilBoson_visPy", "nRecoilJets_jesBBEC1yearUp"})
                    .Redefine("met_JetBBEC1yearUp",    [](ROOT::Math::PtEtaPhiEVector p4) { return (float) p4.Pt(); },  {"p4met_recoil_jesBBEC1yearUp"})
                    .Redefine("metphi_JetBBEC1yearUp", [](ROOT::Math::PtEtaPhiEVector p4) { return (float) p4.Phi(); }, {"p4met_recoil_jesBBEC1yearUp"})
                    .Define("p4met_recoil_jesBBEC1yearDown", [recoilPFMetCorrector_](float uncorr_met, float uncorr_metphi,
                                                       float genpX, float genpY,
                                                       float vispX, float vispY,
                                                       float nRecoilJets) { return getRecoilCorrectionNominal(recoilPFMetCorrector_, uncorr_met, uncorr_metphi, genpX, genpY, vispX, vispY, nRecoilJets); }, {"met_JetBBEC1yearDown", "metphi_JetBBEC1yearDown",  "gen_recoilBoson_Px", "gen_recoilBoson_Py", "gen_recoilBoson_visPx", "gen_recoilBoson_visPy", "nRecoilJets_jesBBEC1yearDown"})
                    .Redefine("met_JetBBEC1yearDown",    [](ROOT::Math::PtEtaPhiEVector p4) { return (float) p4.Pt(); },  {"p4met_recoil_jesBBEC1yearDown"})
                    .Redefine("metphi_JetBBEC1yearDown", [](ROOT::Math::PtEtaPhiEVector p4) { return (float) p4.Phi(); }, {"p4met_recoil_jesBBEC1yearDown"})

                    .Define("p4met_recoil_jesEC2Up", [recoilPFMetCorrector_](float uncorr_met, float uncorr_metphi,
                                                       float genpX, float genpY,
                                                       float vispX, float vispY,
                                                       float nRecoilJets) { return getRecoilCorrectionNominal(recoilPFMetCorrector_, uncorr_met, uncorr_metphi, genpX, genpY, vispX, vispY, nRecoilJets); }, {"met_JetEC2Up", "metphi_JetEC2Up",  "gen_recoilBoson_Px", "gen_recoilBoson_Py", "gen_recoilBoson_visPx", "gen_recoilBoson_visPy", "nRecoilJets_jesEC2Up"})
                    .Redefine("met_JetEC2Up",    [](ROOT::Math::PtEtaPhiEVector p4) { return (float) p4.Pt(); },  {"p4met_recoil_jesEC2Up"})
                    .Redefine("metphi_JetEC2Up", [](ROOT::Math::PtEtaPhiEVector p4) { return (float) p4.Phi(); }, {"p4met_recoil_jesEC2Up"})
                    .Define("p4met_recoil_jesEC2Down", [recoilPFMetCorrector_](float uncorr_met, float uncorr_metphi,
                                                       float genpX, float genpY,
                                                       float vispX, float vispY,
                                                       float nRecoilJets) { return getRecoilCorrectionNominal(recoilPFMetCorrector_, uncorr_met, uncorr_metphi, genpX, genpY, vispX, vispY, nRecoilJets); }, {"met_JetEC2Down", "metphi_JetEC2Down",  "gen_recoilBoson_Px", "gen_recoilBoson_Py", "gen_recoilBoson_visPx", "gen_recoilBoson_visPy", "nRecoilJets_jesEC2Down"})
                    .Redefine("met_JetEC2Down",    [](ROOT::Math::PtEtaPhiEVector p4) { return (float) p4.Pt(); },  {"p4met_recoil_jesEC2Down"})
                    .Redefine("metphi_JetEC2Down", [](ROOT::Math::PtEtaPhiEVector p4) { return (float) p4.Phi(); }, {"p4met_recoil_jesEC2Down"})

                    .Define("p4met_recoil_jesEC2yearUp", [recoilPFMetCorrector_](float uncorr_met, float uncorr_metphi,
                                                       float genpX, float genpY,
                                                       float vispX, float vispY,
                                                       float nRecoilJets) { return getRecoilCorrectionNominal(recoilPFMetCorrector_, uncorr_met, uncorr_metphi, genpX, genpY, vispX, vispY, nRecoilJets); }, {"met_JetEC2yearUp", "metphi_JetEC2yearUp",  "gen_recoilBoson_Px", "gen_recoilBoson_Py", "gen_recoilBoson_visPx", "gen_recoilBoson_visPy", "nRecoilJets_jesEC2yearUp"})
                    .Redefine("met_JetEC2yearUp",    [](ROOT::Math::PtEtaPhiEVector p4) { return (float) p4.Pt(); },  {"p4met_recoil_jesEC2yearUp"})
                    .Redefine("metphi_JetEC2yearUp", [](ROOT::Math::PtEtaPhiEVector p4) { return (float) p4.Phi(); }, {"p4met_recoil_jesEC2yearUp"})
                    .Define("p4met_recoil_jesEC2yearDown", [recoilPFMetCorrector_](float uncorr_met, float uncorr_metphi,
                                                       float genpX, float genpY,
                                                       float vispX, float vispY,
                                                       float nRecoilJets) { return getRecoilCorrectionNominal(recoilPFMetCorrector_, uncorr_met, uncorr_metphi, genpX, genpY, vispX, vispY, nRecoilJets); }, {"met_JetEC2yearDown", "metphi_JetEC2yearDown",  "gen_recoilBoson_Px", "gen_recoilBoson_Py", "gen_recoilBoson_visPx", "gen_recoilBoson_visPy", "nRecoilJets_jesEC2yearDown"})
                    .Redefine("met_JetEC2yearDown",    [](ROOT::Math::PtEtaPhiEVector p4) { return (float) p4.Pt(); },  {"p4met_recoil_jesEC2yearDown"})
                    .Redefine("metphi_JetEC2yearDown", [](ROOT::Math::PtEtaPhiEVector p4) { return (float) p4.Phi(); }, {"p4met_recoil_jesEC2yearDown"})

                    .Define("p4met_recoil_jesFlavorQCDUp", [recoilPFMetCorrector_](float uncorr_met, float uncorr_metphi,
                                                       float genpX, float genpY,
                                                       float vispX, float vispY,
                                                       float nRecoilJets) { return getRecoilCorrectionNominal(recoilPFMetCorrector_, uncorr_met, uncorr_metphi, genpX, genpY, vispX, vispY, nRecoilJets); }, {"met_JetFlavorQCDUp", "metphi_JetFlavorQCDUp",  "gen_recoilBoson_Px", "gen_recoilBoson_Py", "gen_recoilBoson_visPx", "gen_recoilBoson_visPy", "nRecoilJets_jesFlavorQCDUp"})
                    .Redefine("met_JetFlavorQCDUp",    [](ROOT::Math::PtEtaPhiEVector p4) { return (float) p4.Pt(); },  {"p4met_recoil_jesFlavorQCDUp"})
                    .Redefine("metphi_JetFlavorQCDUp", [](ROOT::Math::PtEtaPhiEVector p4) { return (float) p4.Phi(); }, {"p4met_recoil_jesFlavorQCDUp"})
                    .Define("p4met_recoil_jesFlavorQCDDown", [recoilPFMetCorrector_](float uncorr_met, float uncorr_metphi,
                                                       float genpX, float genpY,
                                                       float vispX, float vispY,
                                                       float nRecoilJets) { return getRecoilCorrectionNominal(recoilPFMetCorrector_, uncorr_met, uncorr_metphi, genpX, genpY, vispX, vispY, nRecoilJets); }, {"met_JetFlavorQCDDown", "metphi_JetFlavorQCDDown",  "gen_recoilBoson_Px", "gen_recoilBoson_Py", "gen_recoilBoson_visPx", "gen_recoilBoson_visPy", "nRecoilJets_jesFlavorQCDDown"})
                    .Redefine("met_JetFlavorQCDDown",    [](ROOT::Math::PtEtaPhiEVector p4) { return (float) p4.Pt(); },  {"p4met_recoil_jesFlavorQCDDown"})
                    .Redefine("metphi_JetFlavorQCDDown", [](ROOT::Math::PtEtaPhiEVector p4) { return (float) p4.Phi(); }, {"p4met_recoil_jesFlavorQCDDown"})

                    .Define("p4met_recoil_jesHFUp", [recoilPFMetCorrector_](float uncorr_met, float uncorr_metphi,
                                                       float genpX, float genpY,
                                                       float vispX, float vispY,
                                                       float nRecoilJets) { return getRecoilCorrectionNominal(recoilPFMetCorrector_, uncorr_met, uncorr_metphi, genpX, genpY, vispX, vispY, nRecoilJets); }, {"met_JetHFUp", "metphi_JetHFUp",  "gen_recoilBoson_Px", "gen_recoilBoson_Py", "gen_recoilBoson_visPx", "gen_recoilBoson_visPy", "nRecoilJets_jesHFUp"})
                    .Redefine("met_JetHFUp",    [](ROOT::Math::PtEtaPhiEVector p4) { return (float) p4.Pt(); },  {"p4met_recoil_jesHFUp"})
                    .Redefine("metphi_JetHFUp", [](ROOT::Math::PtEtaPhiEVector p4) { return (float) p4.Phi(); }, {"p4met_recoil_jesHFUp"})
                    .Define("p4met_recoil_jesHFDown", [recoilPFMetCorrector_](float uncorr_met, float uncorr_metphi,
                                                       float genpX, float genpY,
                                                       float vispX, float vispY,
                                                       float nRecoilJets) { return getRecoilCorrectionNominal(recoilPFMetCorrector_, uncorr_met, uncorr_metphi, genpX, genpY, vispX, vispY, nRecoilJets); }, {"met_JetHFDown", "metphi_JetHFDown",  "gen_recoilBoson_Px", "gen_recoilBoson_Py", "gen_recoilBoson_visPx", "gen_recoilBoson_visPy", "nRecoilJets_jesHFDown"})
                    .Redefine("met_JetHFDown",    [](ROOT::Math::PtEtaPhiEVector p4) { return (float) p4.Pt(); },  {"p4met_recoil_jesHFDown"})
                    .Redefine("metphi_JetHFDown", [](ROOT::Math::PtEtaPhiEVector p4) { return (float) p4.Phi(); }, {"p4met_recoil_jesHFDown"})

                    .Define("p4met_recoil_jesHFyearUp", [recoilPFMetCorrector_](float uncorr_met, float uncorr_metphi,
                                                       float genpX, float genpY,
                                                       float vispX, float vispY,
                                                       float nRecoilJets) { return getRecoilCorrectionNominal(recoilPFMetCorrector_, uncorr_met, uncorr_metphi, genpX, genpY, vispX, vispY, nRecoilJets); }, {"met_JetHFyearUp", "metphi_JetHFyearUp",  "gen_recoilBoson_Px", "gen_recoilBoson_Py", "gen_recoilBoson_visPx", "gen_recoilBoson_visPy", "nRecoilJets_jesHFyearUp"})
                    .Redefine("met_JetHFyearUp",    [](ROOT::Math::PtEtaPhiEVector p4) { return (float) p4.Pt(); },  {"p4met_recoil_jesHFyearUp"})
                    .Redefine("metphi_JetHFyearUp", [](ROOT::Math::PtEtaPhiEVector p4) { return (float) p4.Phi(); }, {"p4met_recoil_jesHFyearUp"})
                    .Define("p4met_recoil_jesHFyearDown", [recoilPFMetCorrector_](float uncorr_met, float uncorr_metphi,
                                                       float genpX, float genpY,
                                                       float vispX, float vispY,
                                                       float nRecoilJets) { return getRecoilCorrectionNominal(recoilPFMetCorrector_, uncorr_met, uncorr_metphi, genpX, genpY, vispX, vispY, nRecoilJets); }, {"met_JetHFyearDown", "metphi_JetHFyearDown",  "gen_recoilBoson_Px", "gen_recoilBoson_Py", "gen_recoilBoson_visPx", "gen_recoilBoson_visPy", "nRecoilJets_jesHFyearDown"})
                    .Redefine("met_JetHFyearDown",    [](ROOT::Math::PtEtaPhiEVector p4) { return (float) p4.Pt(); },  {"p4met_recoil_jesHFyearDown"})
                    .Redefine("metphi_JetHFyearDown", [](ROOT::Math::PtEtaPhiEVector p4) { return (float) p4.Phi(); }, {"p4met_recoil_jesHFyearDown"})

                    .Define("p4met_recoil_jesRelativeBalUp", [recoilPFMetCorrector_](float uncorr_met, float uncorr_metphi,
                                                       float genpX, float genpY,
                                                       float vispX, float vispY,
                                                       float nRecoilJets) { return getRecoilCorrectionNominal(recoilPFMetCorrector_, uncorr_met, uncorr_metphi, genpX, genpY, vispX, vispY, nRecoilJets); }, {"met_JetRelativeBalUp", "metphi_JetRelativeBalUp",  "gen_recoilBoson_Px", "gen_recoilBoson_Py", "gen_recoilBoson_visPx", "gen_recoilBoson_visPy", "nRecoilJets_jesRelativeBalUp"})
                    .Redefine("met_JetRelativeBalUp",    [](ROOT::Math::PtEtaPhiEVector p4) { return (float) p4.Pt(); },  {"p4met_recoil_jesRelativeBalUp"})
                    .Redefine("metphi_JetRelativeBalUp", [](ROOT::Math::PtEtaPhiEVector p4) { return (float) p4.Phi(); }, {"p4met_recoil_jesRelativeBalUp"})
                    .Define("p4met_recoil_jesRelativeBalDown", [recoilPFMetCorrector_](float uncorr_met, float uncorr_metphi,
                                                       float genpX, float genpY,
                                                       float vispX, float vispY,
                                                       float nRecoilJets) { return getRecoilCorrectionNominal(recoilPFMetCorrector_, uncorr_met, uncorr_metphi, genpX, genpY, vispX, vispY, nRecoilJets); }, {"met_JetRelativeBalDown", "metphi_JetRelativeBalDown",  "gen_recoilBoson_Px", "gen_recoilBoson_Py", "gen_recoilBoson_visPx", "gen_recoilBoson_visPy", "nRecoilJets_jesRelativeBalDown"})
                    .Redefine("met_JetRelativeBalDown",    [](ROOT::Math::PtEtaPhiEVector p4) { return (float) p4.Pt(); },  {"p4met_recoil_jesRelativeBalDown"})
                    .Redefine("metphi_JetRelativeBalDown", [](ROOT::Math::PtEtaPhiEVector p4) { return (float) p4.Phi(); }, {"p4met_recoil_jesRelativeBalDown"})

                    .Define("p4met_recoil_jesRelativeSampleUp", [recoilPFMetCorrector_](float uncorr_met, float uncorr_metphi,
                                                       float genpX, float genpY,
                                                       float vispX, float vispY,
                                                       float nRecoilJets) { return getRecoilCorrectionNominal(recoilPFMetCorrector_, uncorr_met, uncorr_metphi, genpX, genpY, vispX, vispY, nRecoilJets); }, {"met_JetRelativeSampleUp", "metphi_JetRelativeSampleUp",  "gen_recoilBoson_Px", "gen_recoilBoson_Py", "gen_recoilBoson_visPx", "gen_recoilBoson_visPy", "nRecoilJets_jesRelativeSampleUp"})
                    .Redefine("met_JetRelativeSampleUp",    [](ROOT::Math::PtEtaPhiEVector p4) { return (float) p4.Pt(); },  {"p4met_recoil_jesRelativeSampleUp"})
                    .Redefine("metphi_JetRelativeSampleUp", [](ROOT::Math::PtEtaPhiEVector p4) { return (float) p4.Phi(); }, {"p4met_recoil_jesRelativeSampleUp"})
                    .Define("p4met_recoil_jesRelativeSampleDown", [recoilPFMetCorrector_](float uncorr_met, float uncorr_metphi,
                                                       float genpX, float genpY,
                                                       float vispX, float vispY,
                                                       float nRecoilJets) { return getRecoilCorrectionNominal(recoilPFMetCorrector_, uncorr_met, uncorr_metphi, genpX, genpY, vispX, vispY, nRecoilJets); }, {"met_JetRelativeSampleDown", "metphi_JetRelativeSampleDown",  "gen_recoilBoson_Px", "gen_recoilBoson_Py", "gen_recoilBoson_visPx", "gen_recoilBoson_visPy", "nRecoilJets_jesRelativeSampleDown"})
                    .Redefine("met_JetRelativeSampleDown",    [](ROOT::Math::PtEtaPhiEVector p4) { return (float) p4.Pt(); },  {"p4met_recoil_jesRelativeSampleDown"})
                    .Redefine("metphi_JetRelativeSampleDown", [](ROOT::Math::PtEtaPhiEVector p4) { return (float) p4.Phi(); }, {"p4met_recoil_jesRelativeSampleDown"});
    }
    else {
      return df2;
    }

  }
  else {
    // Data or Embed or MC with no recoil corrections necessary
    return df.Define("met_responseUp",        [](float met)    { return met; },    {"met"})
             .Define("metphi_responseUp",     [](float metphi) { return metphi; }, {"metphi"})
             .Define("met_responseDown",      [](float met)    { return met; },    {"met"})
             .Define("metphi_responseDown",   [](float metphi) { return metphi; }, {"metphi"})
             .Define("met_resolutionUp",      [](float met)    { return met; },    {"met"})
             .Define("metphi_resolutionUp",   [](float metphi) { return metphi; }, {"metphi"})
             .Define("met_resolutionDown",    [](float met)    { return met; },    {"met"})
             .Define("metphi_resolutionDown", [](float metphi) { return metphi; }, {"metphi"});

  }

}



/***************************************************************************/



#endif
