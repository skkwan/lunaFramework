// Helper functions for computing categorization variables.
#ifndef CATEGORY_VARIABLES_H
#define CATEGORY_VARIABLES_H

#include "computePhysicsVariables.h"
#include "sampleConfig_class.h"

/**********************************************************/

/* Return df with the variables used for categorizing events.
   The leading tau pair and leading b jet must be identified already.

   Also needs the "p4" 4-vector. */

template <typename T>
auto ComputeCategoryVariables(T &df, LUNA::sampleConfig_t &sConfig) {

  // Compute D_zeta
  auto df2 = df.Define("D_zeta_nominal", compute_D_zeta, {"met_nominal", "metphi_nominal", "pt_1_nominal", "phi_1", "pt_2_nominal", "phi_2"})
    // Compute m_btautau_vis
    .Define("p4_b_nominal", add_p4, {"bpt_deepflavour_1", "beta_deepflavour_1", "bphi_deepflavour_1", "bm_deepflavour_1"})
    .Define("p4_btautau_nominal", "p4_1_nominal + p4_2_nominal + p4_b_nominal")
    .Define("m_btautau_vis_nominal",  "float(p4_btautau_nominal.M())")
    .Define("pt_btautau_vis_nominal", "float(p4_btautau_nominal.Pt())")
    // Compute mT between leading tau leptons and the MET (use central value TES-corrected MET)
    .Define("mtMET_1_nominal", compute_mt, {"pt_1_nominal", "phi_1", "met_nominal", "metphi_nominal"})
    .Define("mtMET_2_nominal", compute_mt, {"pt_2_nominal", "phi_2", "met_nominal", "metphi_nominal"});

  if (sConfig.doSys() && (sConfig.isMC() || sConfig.isEmbedded())) {
    // Lepton energy scales: in MC and Embedded
    auto df3 = df2.Define("m_btautau_vis_es1Up",    "float((p4_1_es1Up   + p4_2_nominal + p4_b_nominal).M())")
                  .Define("m_btautau_vis_es1Down",  "float((p4_1_es1Down + p4_2_nominal + p4_b_nominal).M())")
                  .Define("m_btautau_vis_es2Up",    "float((p4_1_nominal + p4_2_es2Up   + p4_b_nominal).M())")
                  .Define("m_btautau_vis_es2Down",  "float((p4_1_nominal + p4_2_es2Down + p4_b_nominal).M())")
                  .Define("pt_btautau_vis_es1Up",   "float((p4_1_es1Up   + p4_2_nominal + p4_b_nominal).Pt())")
                  .Define("pt_btautau_vis_es1Down", "float((p4_1_es1Down + p4_2_nominal + p4_b_nominal).Pt())")
                  .Define("pt_btautau_vis_es2Up",   "float((p4_1_nominal + p4_2_es2Up   + p4_b_nominal).Pt())")
                  .Define("pt_btautau_vis_es2Down", "float((p4_1_nominal + p4_2_es2Down + p4_b_nominal).Pt())")

                  // Shifts of mtMET due to their respective lepton energy scales. Note the met and metphi is also shifted
                 .Define("mtMET_1_es1Up",   compute_mt, {"pt_1_es1Up",   "phi_1", "met_es1Up",   "metphi_es1Up"})
                 .Define("mtMET_1_es1Down", compute_mt, {"pt_1_es1Down", "phi_1", "met_es1Down", "metphi_es1Down"})
                 .Define("mtMET_2_es2Up",   compute_mt, {"pt_2_es2Up",   "phi_2", "met_es2Up",   "metphi_es2Up"})
                 .Define("mtMET_2_es2Down", compute_mt, {"pt_2_es2Down", "phi_2", "met_es2Down", "metphi_es2Down"})

                  // Shifts of D_zeta due to lepton energy shifts
                  .Define("D_zeta_es1Up",   compute_D_zeta, {"met_es1Up",   "metphi_es1Up",   "pt_1_es1Up",   "phi_1", "pt_2_nominal", "phi_2"})
                  .Define("D_zeta_es1Down", compute_D_zeta, {"met_es1Down", "metphi_es1Down", "pt_1_es1Down", "phi_1", "pt_2_nominal", "phi_2"})
                  .Define("D_zeta_es2Up",   compute_D_zeta, {"met_es2Up",   "metphi_es2Up",   "pt_1_nominal", "phi_1", "pt_2_es2Up",   "phi_2"})
                  .Define("D_zeta_es2Down", compute_D_zeta, {"met_es2Down", "metphi_es2Down", "pt_1_nominal", "phi_1", "pt_2_es2Down", "phi_2"});

    if (sConfig.isEmbedded()) {
      // no jet or met/metphi systematics for embedded
      return df3;
    }
    else if (sConfig.isMC()) {
      // Do all the shifts for jet and met/metphi systematics
      // Shifts of m_btautau_vis, pt_btautau_vis due to b-tag jet energy scale shifts
      // First need the p4_b shifted up. Note that we don't need _2 because mbtautau is only the leading b-tag jet.
      return df3.Define("p4_b_JERUp_1",               add_p4, {"bpt_deepflavour_JERUp_1",               "beta_deepflavour_1", "bphi_deepflavour_1", "bm_deepflavour_JERUp_1"              })
               .Define("p4_b_JetAbsoluteUp_1",       add_p4, {"bpt_deepflavour_JetAbsoluteUp_1",       "beta_deepflavour_1", "bphi_deepflavour_1", "bm_deepflavour_JetAbsoluteUp_1"      })
               .Define("p4_b_JetAbsoluteyearUp_1",   add_p4, {"bpt_deepflavour_JetAbsoluteyearUp_1",   "beta_deepflavour_1", "bphi_deepflavour_1", "bm_deepflavour_JetAbsoluteyearUp_1"  })
               .Define("p4_b_JetBBEC1Up_1",          add_p4, {"bpt_deepflavour_JetBBEC1Up_1",          "beta_deepflavour_1", "bphi_deepflavour_1", "bm_deepflavour_JetBBEC1Up_1"         })
               .Define("p4_b_JetBBEC1yearUp_1",      add_p4, {"bpt_deepflavour_JetBBEC1yearUp_1",      "beta_deepflavour_1", "bphi_deepflavour_1", "bm_deepflavour_JetBBEC1yearUp_1"     })
               .Define("p4_b_JetEC2Up_1",            add_p4, {"bpt_deepflavour_JetEC2Up_1",            "beta_deepflavour_1", "bphi_deepflavour_1", "bm_deepflavour_JetEC2Up_1"           })
               .Define("p4_b_JetEC2yearUp_1",        add_p4, {"bpt_deepflavour_JetEC2yearUp_1",        "beta_deepflavour_1", "bphi_deepflavour_1", "bm_deepflavour_JetEC2yearUp_1"       })
               .Define("p4_b_JetFlavorQCDUp_1",      add_p4, {"bpt_deepflavour_JetFlavorQCDUp_1",      "beta_deepflavour_1", "bphi_deepflavour_1", "bm_deepflavour_JetFlavorQCDUp_1"     })
               .Define("p4_b_JetHFUp_1",             add_p4, {"bpt_deepflavour_JetHFUp_1",             "beta_deepflavour_1", "bphi_deepflavour_1", "bm_deepflavour_JetHFUp_1"            })
               .Define("p4_b_JetHFyearUp_1",         add_p4, {"bpt_deepflavour_JetHFyearUp_1",         "beta_deepflavour_1", "bphi_deepflavour_1", "bm_deepflavour_JetHFyearUp_1"        })
               .Define("p4_b_JetRelativeBalUp_1",    add_p4, {"bpt_deepflavour_JetRelativeBalUp_1",    "beta_deepflavour_1", "bphi_deepflavour_1", "bm_deepflavour_JetRelativeBalUp_1"   })
               .Define("p4_b_JetRelativeSampleUp_1", add_p4, {"bpt_deepflavour_JetRelativeSampleUp_1", "beta_deepflavour_1", "bphi_deepflavour_1", "bm_deepflavour_JetRelativeSampleUp_1"})

               // p4_b shift down
               .Define("p4_b_JERDown_1",               add_p4, {"bpt_deepflavour_JERDown_1",               "beta_deepflavour_1", "bphi_deepflavour_1", "bm_deepflavour_JERDown_1"              })
               .Define("p4_b_JetAbsoluteDown_1",       add_p4, {"bpt_deepflavour_JetAbsoluteDown_1",       "beta_deepflavour_1", "bphi_deepflavour_1", "bm_deepflavour_JetAbsoluteDown_1"      })
               .Define("p4_b_JetAbsoluteyearDown_1",   add_p4, {"bpt_deepflavour_JetAbsoluteyearDown_1",   "beta_deepflavour_1", "bphi_deepflavour_1", "bm_deepflavour_JetAbsoluteyearDown_1"  })
               .Define("p4_b_JetBBEC1Down_1",          add_p4, {"bpt_deepflavour_JetBBEC1Down_1",          "beta_deepflavour_1", "bphi_deepflavour_1", "bm_deepflavour_JetBBEC1Down_1"         })
               .Define("p4_b_JetBBEC1yearDown_1",      add_p4, {"bpt_deepflavour_JetBBEC1yearDown_1",      "beta_deepflavour_1", "bphi_deepflavour_1", "bm_deepflavour_JetBBEC1yearDown_1"     })
               .Define("p4_b_JetEC2Down_1",            add_p4, {"bpt_deepflavour_JetEC2Down_1",            "beta_deepflavour_1", "bphi_deepflavour_1", "bm_deepflavour_JetEC2Down_1"           })
               .Define("p4_b_JetEC2yearDown_1",        add_p4, {"bpt_deepflavour_JetEC2yearDown_1",        "beta_deepflavour_1", "bphi_deepflavour_1", "bm_deepflavour_JetEC2yearDown_1"       })
               .Define("p4_b_JetFlavorQCDDown_1",      add_p4, {"bpt_deepflavour_JetFlavorQCDDown_1",      "beta_deepflavour_1", "bphi_deepflavour_1", "bm_deepflavour_JetFlavorQCDDown_1"     })
               .Define("p4_b_JetHFDown_1",             add_p4, {"bpt_deepflavour_JetHFDown_1",             "beta_deepflavour_1", "bphi_deepflavour_1", "bm_deepflavour_JetHFDown_1"            })
               .Define("p4_b_JetHFyearDown_1",         add_p4, {"bpt_deepflavour_JetHFyearDown_1",         "beta_deepflavour_1", "bphi_deepflavour_1", "bm_deepflavour_JetHFyearDown_1"        })
               .Define("p4_b_JetRelativeBalDown_1",    add_p4, {"bpt_deepflavour_JetRelativeBalDown_1",    "beta_deepflavour_1", "bphi_deepflavour_1", "bm_deepflavour_JetRelativeBalDown_1"   })
               .Define("p4_b_JetRelativeSampleDown_1", add_p4, {"bpt_deepflavour_JetRelativeSampleDown_1", "beta_deepflavour_1", "bphi_deepflavour_1", "bm_deepflavour_JetRelativeSampleDown_1"})

               // Now, finally can do shifts of m_btautau_vis due to the JER shifts :)
               .Define("m_btautau_vis_JERUp_1",               "float((p4_1_nominal + p4_2_nominal + p4_b_JERUp_1).M())")
               .Define("m_btautau_vis_JetAbsoluteUp_1",       "float((p4_1_nominal + p4_2_nominal + p4_b_JetAbsoluteUp_1).M())")
               .Define("m_btautau_vis_JetAbsoluteyearUp_1",   "float((p4_1_nominal + p4_2_nominal + p4_b_JetAbsoluteyearUp_1).M())")
               .Define("m_btautau_vis_JetBBEC1Up_1",          "float((p4_1_nominal + p4_2_nominal + p4_b_JetBBEC1Up_1).M())")
               .Define("m_btautau_vis_JetBBEC1yearUp_1",      "float((p4_1_nominal + p4_2_nominal + p4_b_JetBBEC1yearUp_1).M())")
               .Define("m_btautau_vis_JetEC2Up_1",            "float((p4_1_nominal + p4_2_nominal + p4_b_JetEC2Up_1).M())")
               .Define("m_btautau_vis_JetEC2yearUp_1",        "float((p4_1_nominal + p4_2_nominal + p4_b_JetEC2yearUp_1).M())")
               .Define("m_btautau_vis_JetFlavorQCDUp_1",      "float((p4_1_nominal + p4_2_nominal + p4_b_JetFlavorQCDUp_1).M())")
               .Define("m_btautau_vis_JetHFUp_1",             "float((p4_1_nominal + p4_2_nominal + p4_b_JetHFUp_1).M())")
               .Define("m_btautau_vis_JetHFyearUp_1",         "float((p4_1_nominal + p4_2_nominal + p4_b_JetHFyearUp_1).M())")
               .Define("m_btautau_vis_JetRelativeBalUp_1",    "float((p4_1_nominal + p4_2_nominal + p4_b_JetRelativeBalUp_1).M())")
               .Define("m_btautau_vis_JetRelativeSampleUp_1", "float((p4_1_nominal + p4_2_nominal + p4_b_JetRelativeSampleUp_1).M())")

               // Down shifts of the above
               .Define("m_btautau_vis_JERDown_1",               "float((p4_1_nominal + p4_2_nominal + p4_b_JERDown_1).M())")
               .Define("m_btautau_vis_JetAbsoluteDown_1",       "float((p4_1_nominal + p4_2_nominal + p4_b_JetAbsoluteDown_1).M())")
               .Define("m_btautau_vis_JetAbsoluteyearDown_1",   "float((p4_1_nominal + p4_2_nominal + p4_b_JetAbsoluteyearDown_1).M())")
               .Define("m_btautau_vis_JetBBEC1Down_1",          "float((p4_1_nominal + p4_2_nominal + p4_b_JetBBEC1Down_1).M())")
               .Define("m_btautau_vis_JetBBEC1yearDown_1",      "float((p4_1_nominal + p4_2_nominal + p4_b_JetBBEC1yearDown_1).M())")
               .Define("m_btautau_vis_JetEC2Down_1",            "float((p4_1_nominal + p4_2_nominal + p4_b_JetEC2Down_1).M())")
               .Define("m_btautau_vis_JetEC2yearDown_1",        "float((p4_1_nominal + p4_2_nominal + p4_b_JetEC2yearDown_1).M())")
               .Define("m_btautau_vis_JetFlavorQCDDown_1",      "float((p4_1_nominal + p4_2_nominal + p4_b_JetFlavorQCDDown_1).M())")
               .Define("m_btautau_vis_JetHFDown_1",             "float((p4_1_nominal + p4_2_nominal + p4_b_JetHFDown_1).M())")
               .Define("m_btautau_vis_JetHFyearDown_1",         "float((p4_1_nominal + p4_2_nominal + p4_b_JetHFyearDown_1).M())")
               .Define("m_btautau_vis_JetRelativeBalDown_1",    "float((p4_1_nominal + p4_2_nominal + p4_b_JetRelativeBalDown_1).M())")
               .Define("m_btautau_vis_JetRelativeSampleDown_1", "float((p4_1_nominal + p4_2_nominal + p4_b_JetRelativeSampleDown_1).M())")

               // Now, finally can do shifts of pt_btautau_vis due to the JER shifts :)
               .Define("pt_btautau_vis_JERUp_1",               "float((p4_1_nominal + p4_2_nominal + p4_b_JERUp_1).Pt())")
               .Define("pt_btautau_vis_JetAbsoluteUp_1",       "float((p4_1_nominal + p4_2_nominal + p4_b_JetAbsoluteUp_1).Pt())")
               .Define("pt_btautau_vis_JetAbsoluteyearUp_1",   "float((p4_1_nominal + p4_2_nominal + p4_b_JetAbsoluteyearUp_1).Pt())")
               .Define("pt_btautau_vis_JetBBEC1Up_1",          "float((p4_1_nominal + p4_2_nominal + p4_b_JetBBEC1Up_1).Pt())")
               .Define("pt_btautau_vis_JetBBEC1yearUp_1",      "float((p4_1_nominal + p4_2_nominal + p4_b_JetBBEC1yearUp_1).Pt())")
               .Define("pt_btautau_vis_JetEC2Up_1",            "float((p4_1_nominal + p4_2_nominal + p4_b_JetEC2Up_1).Pt())")
               .Define("pt_btautau_vis_JetEC2yearUp_1",        "float((p4_1_nominal + p4_2_nominal + p4_b_JetEC2yearUp_1).Pt())")
               .Define("pt_btautau_vis_JetFlavorQCDUp_1",      "float((p4_1_nominal + p4_2_nominal + p4_b_JetFlavorQCDUp_1).Pt())")
               .Define("pt_btautau_vis_JetHFUp_1",             "float((p4_1_nominal + p4_2_nominal + p4_b_JetHFUp_1).Pt())")
               .Define("pt_btautau_vis_JetHFyearUp_1",         "float((p4_1_nominal + p4_2_nominal + p4_b_JetHFyearUp_1).Pt())")
               .Define("pt_btautau_vis_JetRelativeBalUp_1",    "float((p4_1_nominal + p4_2_nominal + p4_b_JetRelativeBalUp_1).Pt())")
               .Define("pt_btautau_vis_JetRelativeSampleUp_1", "float((p4_1_nominal + p4_2_nominal + p4_b_JetRelativeSampleUp_1).Pt())")

               // Down shifts of the above
               .Define("pt_btautau_vis_JERDown_1",               "float((p4_1_nominal + p4_2_nominal + p4_b_JERDown_1).Pt())")
               .Define("pt_btautau_vis_JetAbsoluteDown_1",       "float((p4_1_nominal + p4_2_nominal + p4_b_JetAbsoluteDown_1).Pt())")
               .Define("pt_btautau_vis_JetAbsoluteyearDown_1",   "float((p4_1_nominal + p4_2_nominal + p4_b_JetAbsoluteyearDown_1).Pt())")
               .Define("pt_btautau_vis_JetBBEC1Down_1",          "float((p4_1_nominal + p4_2_nominal + p4_b_JetBBEC1Down_1).Pt())")
               .Define("pt_btautau_vis_JetBBEC1yearDown_1",      "float((p4_1_nominal + p4_2_nominal + p4_b_JetBBEC1yearDown_1).Pt())")
               .Define("pt_btautau_vis_JetEC2Down_1",            "float((p4_1_nominal + p4_2_nominal + p4_b_JetEC2Down_1).Pt())")
               .Define("pt_btautau_vis_JetEC2yearDown_1",        "float((p4_1_nominal + p4_2_nominal + p4_b_JetEC2yearDown_1).Pt())")
               .Define("pt_btautau_vis_JetFlavorQCDDown_1",      "float((p4_1_nominal + p4_2_nominal + p4_b_JetFlavorQCDDown_1).Pt())")
               .Define("pt_btautau_vis_JetHFDown_1",             "float((p4_1_nominal + p4_2_nominal + p4_b_JetHFDown_1).Pt())")
               .Define("pt_btautau_vis_JetHFyearDown_1",         "float((p4_1_nominal + p4_2_nominal + p4_b_JetHFyearDown_1).Pt())")
               .Define("pt_btautau_vis_JetRelativeBalDown_1",    "float((p4_1_nominal + p4_2_nominal + p4_b_JetRelativeBalDown_1).Pt())")
               .Define("pt_btautau_vis_JetRelativeSampleDown_1", "float((p4_1_nominal + p4_2_nominal + p4_b_JetRelativeSampleDown_1).Pt())")




               // Shifts of mtMET_1 due to MET systematics
               .Define("mtMET_1_responseUp",   compute_mt, {"pt_1_nominal", "phi_1", "met_responseUp",   "metphi_responseUp"})
               .Define("mtMET_1_resolutionUp", compute_mt, {"pt_1_nominal", "phi_1", "met_resolutionUp", "metphi_resolutionUp"})
               .Define("mtMET_1_UESUp", compute_mt, {"pt_1_nominal", "phi_1", "met_UESUp", "metphi_UESUp"})
               .Define("mtMET_1_JERUp",               compute_mt, {"pt_1_nominal", "phi_1", "met_JERUp",               "metphi_JERUp"})
               .Define("mtMET_1_JetAbsoluteUp",       compute_mt, {"pt_1_nominal", "phi_1", "met_JetAbsoluteUp",       "metphi_JetAbsoluteUp"})
               .Define("mtMET_1_JetAbsoluteyearUp",   compute_mt, {"pt_1_nominal", "phi_1", "met_JetAbsoluteyearUp",   "metphi_JetAbsoluteyearUp"})
               .Define("mtMET_1_JetBBEC1Up",          compute_mt, {"pt_1_nominal", "phi_1", "met_JetBBEC1Up",          "metphi_JetBBEC1Up"})
               .Define("mtMET_1_JetBBEC1yearUp",      compute_mt, {"pt_1_nominal", "phi_1", "met_JetBBEC1yearUp",      "metphi_JetBBEC1yearUp"})
               .Define("mtMET_1_JetEC2Up",            compute_mt, {"pt_1_nominal", "phi_1", "met_JetEC2Up",            "metphi_JetEC2Up"})
               .Define("mtMET_1_JetEC2yearUp",        compute_mt, {"pt_1_nominal", "phi_1", "met_JetEC2yearUp",        "metphi_JetEC2yearUp"})
               .Define("mtMET_1_JetFlavorQCDUp",      compute_mt, {"pt_1_nominal", "phi_1", "met_JetFlavorQCDUp",      "metphi_JetFlavorQCDUp"})
               .Define("mtMET_1_JetHFUp",             compute_mt, {"pt_1_nominal", "phi_1", "met_JetHFUp",             "metphi_JetHFUp"})
               .Define("mtMET_1_JetHFyearUp",         compute_mt, {"pt_1_nominal", "phi_1", "met_JetHFyearUp",         "metphi_JetHFyearUp"})
               .Define("mtMET_1_JetRelativeBalUp",    compute_mt, {"pt_1_nominal", "phi_1", "met_JetRelativeBalUp",    "metphi_JetRelativeBalUp"})
               .Define("mtMET_1_JetRelativeSampleUp", compute_mt, {"pt_1_nominal", "phi_1", "met_JetRelativeSampleUp", "metphi_JetRelativeSampleUp"})

               .Define("mtMET_1_responseDown",   compute_mt, {"pt_1_nominal", "phi_1", "met_responseDown",   "metphi_responseDown"})
               .Define("mtMET_1_resolutionDown", compute_mt, {"pt_1_nominal", "phi_1", "met_resolutionDown", "metphi_resolutionDown"})
               .Define("mtMET_1_UESDown", compute_mt, {"pt_1_nominal", "phi_1", "met_UESDown", "metphi_UESDown"})
               .Define("mtMET_1_JERDown",               compute_mt, {"pt_1_nominal", "phi_1", "met_JERDown",               "metphi_JERDown"})
               .Define("mtMET_1_JetAbsoluteDown",       compute_mt, {"pt_1_nominal", "phi_1", "met_JetAbsoluteDown",       "metphi_JetAbsoluteDown"})
               .Define("mtMET_1_JetAbsoluteyearDown",   compute_mt, {"pt_1_nominal", "phi_1", "met_JetAbsoluteyearDown",   "metphi_JetAbsoluteyearDown"})
               .Define("mtMET_1_JetBBEC1Down",          compute_mt, {"pt_1_nominal", "phi_1", "met_JetBBEC1Down",          "metphi_JetBBEC1Down"})
               .Define("mtMET_1_JetBBEC1yearDown",      compute_mt, {"pt_1_nominal", "phi_1", "met_JetBBEC1yearDown",      "metphi_JetBBEC1yearDown"})
               .Define("mtMET_1_JetEC2Down",            compute_mt, {"pt_1_nominal", "phi_1", "met_JetEC2Down",            "metphi_JetEC2Down"})
               .Define("mtMET_1_JetEC2yearDown",        compute_mt, {"pt_1_nominal", "phi_1", "met_JetEC2yearDown",        "metphi_JetEC2yearDown"})
               .Define("mtMET_1_JetFlavorQCDDown",      compute_mt, {"pt_1_nominal", "phi_1", "met_JetFlavorQCDDown",      "metphi_JetFlavorQCDDown"})
               .Define("mtMET_1_JetHFDown",             compute_mt, {"pt_1_nominal", "phi_1", "met_JetHFDown",             "metphi_JetHFDown"})
               .Define("mtMET_1_JetHFyearDown",         compute_mt, {"pt_1_nominal", "phi_1", "met_JetHFyearDown",         "metphi_JetHFyearDown"})
               .Define("mtMET_1_JetRelativeBalDown",    compute_mt, {"pt_1_nominal", "phi_1", "met_JetRelativeBalDown",    "metphi_JetRelativeBalDown"})
               .Define("mtMET_1_JetRelativeSampleDown", compute_mt, {"pt_1_nominal", "phi_1", "met_JetRelativeSampleDown", "metphi_JetRelativeSampleDown"})

               // Shifts of mtMET_2 due to MET systematics
               .Define("mtMET_2_responseUp",   compute_mt, {"pt_2_nominal", "phi_2", "met_responseUp",   "metphi_responseUp"})
               .Define("mtMET_2_resolutionUp", compute_mt, {"pt_2_nominal", "phi_2", "met_resolutionUp", "metphi_resolutionUp"})
               .Define("mtMET_2_UESUp",        compute_mt, {"pt_2_nominal", "phi_2", "met_UESUp", "metphi_UESUp"})
               .Define("mtMET_2_JERUp",               compute_mt, {"pt_2_nominal", "phi_2", "met_JERUp",               "metphi_JERUp"})
               .Define("mtMET_2_JetAbsoluteUp",       compute_mt, {"pt_2_nominal", "phi_2", "met_JetAbsoluteUp",       "metphi_JetAbsoluteUp"})
               .Define("mtMET_2_JetAbsoluteyearUp",   compute_mt, {"pt_2_nominal", "phi_2", "met_JetAbsoluteyearUp",   "metphi_JetAbsoluteyearUp"})
               .Define("mtMET_2_JetBBEC1Up",          compute_mt, {"pt_2_nominal", "phi_2", "met_JetBBEC1Up",          "metphi_JetBBEC1Up"})
               .Define("mtMET_2_JetBBEC1yearUp",      compute_mt, {"pt_2_nominal", "phi_2", "met_JetBBEC1yearUp",      "metphi_JetBBEC1yearUp"})
               .Define("mtMET_2_JetEC2Up",            compute_mt, {"pt_2_nominal", "phi_2", "met_JetEC2Up",            "metphi_JetEC2Up"})
               .Define("mtMET_2_JetEC2yearUp",        compute_mt, {"pt_2_nominal", "phi_2", "met_JetEC2yearUp",        "metphi_JetEC2yearUp"})
               .Define("mtMET_2_JetFlavorQCDUp",      compute_mt, {"pt_2_nominal", "phi_2", "met_JetFlavorQCDUp",      "metphi_JetFlavorQCDUp"})
               .Define("mtMET_2_JetHFUp",             compute_mt, {"pt_2_nominal", "phi_2", "met_JetHFUp",             "metphi_JetHFUp"})
               .Define("mtMET_2_JetHFyearUp",         compute_mt, {"pt_2_nominal", "phi_2", "met_JetHFyearUp",         "metphi_JetHFyearUp"})
               .Define("mtMET_2_JetRelativeBalUp",    compute_mt, {"pt_2_nominal", "phi_2", "met_JetRelativeBalUp",    "metphi_JetRelativeBalUp"})
               .Define("mtMET_2_JetRelativeSampleUp", compute_mt, {"pt_2_nominal", "phi_2", "met_JetRelativeSampleUp", "metphi_JetRelativeSampleUp"})

               .Define("mtMET_2_responseDown",   compute_mt, {"pt_2_nominal", "phi_2", "met_responseDown",   "metphi_responseDown"})
               .Define("mtMET_2_resolutionDown", compute_mt, {"pt_2_nominal", "phi_2", "met_resolutionDown", "metphi_resolutionDown"})
               .Define("mtMET_2_UESDown",        compute_mt, {"pt_2_nominal", "phi_2", "met_UESDown", "metphi_UESDown"})
               .Define("mtMET_2_JERDown",               compute_mt, {"pt_2_nominal", "phi_2", "met_JERDown",               "metphi_JERDown"})
               .Define("mtMET_2_JetAbsoluteDown",       compute_mt, {"pt_2_nominal", "phi_2", "met_JetAbsoluteDown",       "metphi_JetAbsoluteDown"})
               .Define("mtMET_2_JetAbsoluteyearDown",   compute_mt, {"pt_2_nominal", "phi_2", "met_JetAbsoluteyearDown",   "metphi_JetAbsoluteyearDown"})
               .Define("mtMET_2_JetBBEC1Down",          compute_mt, {"pt_2_nominal", "phi_2", "met_JetBBEC1Down",          "metphi_JetBBEC1Down"})
               .Define("mtMET_2_JetBBEC1yearDown",      compute_mt, {"pt_2_nominal", "phi_2", "met_JetBBEC1yearDown",      "metphi_JetBBEC1yearDown"})
               .Define("mtMET_2_JetEC2Down",            compute_mt, {"pt_2_nominal", "phi_2", "met_JetEC2Down",            "metphi_JetEC2Down"})
               .Define("mtMET_2_JetEC2yearDown",        compute_mt, {"pt_2_nominal", "phi_2", "met_JetEC2yearDown",        "metphi_JetEC2yearDown"})
               .Define("mtMET_2_JetFlavorQCDDown",      compute_mt, {"pt_2_nominal", "phi_2", "met_JetFlavorQCDDown",      "metphi_JetFlavorQCDDown"})
               .Define("mtMET_2_JetHFDown",             compute_mt, {"pt_2_nominal", "phi_2", "met_JetHFDown",             "metphi_JetHFDown"})
               .Define("mtMET_2_JetHFyearDown",         compute_mt, {"pt_2_nominal", "phi_2", "met_JetHFyearDown",         "metphi_JetHFyearDown"})
               .Define("mtMET_2_JetRelativeBalDown",    compute_mt, {"pt_2_nominal", "phi_2", "met_JetRelativeBalDown",    "metphi_JetRelativeBalDown"})
               .Define("mtMET_2_JetRelativeSampleDown", compute_mt, {"pt_2_nominal", "phi_2", "met_JetRelativeSampleDown", "metphi_JetRelativeSampleDown"})



               // Shifts of D_zeta due to MET systematics
               .Define("D_zeta_responseUp",          compute_D_zeta, {"met_responseUp",          "metphi_responseUp",          "pt_1_nominal", "phi_1", "pt_2_nominal", "phi_2"})
               .Define("D_zeta_resolutionUp",        compute_D_zeta, {"met_resolutionUp",        "metphi_resolutionUp",        "pt_1_nominal", "phi_1", "pt_2_nominal", "phi_2"})
               .Define("D_zeta_UESUp",               compute_D_zeta, {"met_UESUp",               "metphi_UESUp",               "pt_1_nominal", "phi_1", "pt_2_nominal", "phi_2"})
               .Define("D_zeta_JERUp",               compute_D_zeta, {"met_JERUp",               "metphi_JERUp",               "pt_1_nominal", "phi_1", "pt_2_nominal", "phi_2"})
               .Define("D_zeta_JetAbsoluteUp",       compute_D_zeta, {"met_JetAbsoluteUp",       "metphi_JetAbsoluteUp",       "pt_1_nominal", "phi_1", "pt_2_nominal", "phi_2"})
               .Define("D_zeta_JetAbsoluteyearUp",   compute_D_zeta, {"met_JetAbsoluteyearUp",   "metphi_JetAbsoluteyearUp",   "pt_1_nominal", "phi_1", "pt_2_nominal", "phi_2"})
               .Define("D_zeta_JetBBEC1Up",          compute_D_zeta, {"met_JetBBEC1Up",          "metphi_JetBBEC1Up",          "pt_1_nominal", "phi_1", "pt_2_nominal", "phi_2"})
               .Define("D_zeta_JetBBEC1yearUp",      compute_D_zeta, {"met_JetBBEC1yearUp",      "metphi_JetBBEC1yearUp",      "pt_1_nominal", "phi_1", "pt_2_nominal", "phi_2"})
               .Define("D_zeta_JetEC2Up",            compute_D_zeta, {"met_JetEC2Up",            "metphi_JetEC2Up",            "pt_1_nominal", "phi_1", "pt_2_nominal", "phi_2"})
               .Define("D_zeta_JetEC2yearUp",        compute_D_zeta, {"met_JetEC2yearUp",        "metphi_JetEC2yearUp",        "pt_1_nominal", "phi_1", "pt_2_nominal", "phi_2"})
               .Define("D_zeta_JetFlavorQCDUp",      compute_D_zeta, {"met_JetFlavorQCDUp",      "metphi_JetFlavorQCDUp",      "pt_1_nominal", "phi_1", "pt_2_nominal", "phi_2"})
               .Define("D_zeta_JetHFUp",             compute_D_zeta, {"met_JetHFUp",             "metphi_JetHFUp",             "pt_1_nominal", "phi_1", "pt_2_nominal", "phi_2"})
               .Define("D_zeta_JetHFyearUp",         compute_D_zeta, {"met_JetHFyearUp",         "metphi_JetHFyearUp",         "pt_1_nominal", "phi_1", "pt_2_nominal", "phi_2"})
               .Define("D_zeta_JetRelativeBalUp",    compute_D_zeta, {"met_JetRelativeBalUp",    "metphi_JetRelativeBalUp",    "pt_1_nominal", "phi_1", "pt_2_nominal", "phi_2"})
               .Define("D_zeta_JetRelativeSampleUp", compute_D_zeta, {"met_JetRelativeSampleUp", "metphi_JetRelativeSampleUp", "pt_1_nominal", "phi_1", "pt_2_nominal", "phi_2"})

               .Define("D_zeta_responseDown",          compute_D_zeta, {"met_responseDown",          "metphi_responseDown",          "pt_1_nominal", "phi_1", "pt_2_nominal", "phi_2"})
               .Define("D_zeta_resolutionDown",        compute_D_zeta, {"met_resolutionDown",        "metphi_resolutionDown",        "pt_1_nominal", "phi_1", "pt_2_nominal", "phi_2"})
               .Define("D_zeta_UESDown",               compute_D_zeta, {"met_UESDown",               "metphi_UESDown",               "pt_1_nominal", "phi_1", "pt_2_nominal", "phi_2"})
               .Define("D_zeta_JERDown",               compute_D_zeta, {"met_JERDown",               "metphi_JERDown",               "pt_1_nominal", "phi_1", "pt_2_nominal", "phi_2"})
               .Define("D_zeta_JetAbsoluteDown",       compute_D_zeta, {"met_JetAbsoluteDown",       "metphi_JetAbsoluteDown",       "pt_1_nominal", "phi_1", "pt_2_nominal", "phi_2"})
               .Define("D_zeta_JetAbsoluteyearDown",   compute_D_zeta, {"met_JetAbsoluteyearDown",   "metphi_JetAbsoluteyearDown",   "pt_1_nominal", "phi_1", "pt_2_nominal", "phi_2"})
               .Define("D_zeta_JetBBEC1Down",          compute_D_zeta, {"met_JetBBEC1Down",          "metphi_JetBBEC1Down",          "pt_1_nominal", "phi_1", "pt_2_nominal", "phi_2"})
               .Define("D_zeta_JetBBEC1yearDown",      compute_D_zeta, {"met_JetBBEC1yearDown",      "metphi_JetBBEC1yearDown",      "pt_1_nominal", "phi_1", "pt_2_nominal", "phi_2"})
               .Define("D_zeta_JetEC2Down",            compute_D_zeta, {"met_JetEC2Down",            "metphi_JetEC2Down",            "pt_1_nominal", "phi_1", "pt_2_nominal", "phi_2"})
               .Define("D_zeta_JetEC2yearDown",        compute_D_zeta, {"met_JetEC2yearDown",        "metphi_JetEC2yearDown",        "pt_1_nominal", "phi_1", "pt_2_nominal", "phi_2"})
               .Define("D_zeta_JetFlavorQCDDown",      compute_D_zeta, {"met_JetFlavorQCDDown",      "metphi_JetFlavorQCDDown",      "pt_1_nominal", "phi_1", "pt_2_nominal", "phi_2"})
               .Define("D_zeta_JetHFDown",             compute_D_zeta, {"met_JetHFDown",             "metphi_JetHFDown",             "pt_1_nominal", "phi_1", "pt_2_nominal", "phi_2"})
               .Define("D_zeta_JetHFyearDown",         compute_D_zeta, {"met_JetHFyearDown",         "metphi_JetHFyearDown",         "pt_1_nominal", "phi_1", "pt_2_nominal", "phi_2"})
               .Define("D_zeta_JetRelativeBalDown",    compute_D_zeta, {"met_JetRelativeBalDown",    "metphi_JetRelativeBalDown",    "pt_1_nominal", "phi_1", "pt_2_nominal", "phi_2"})
               .Define("D_zeta_JetRelativeSampleDown", compute_D_zeta, {"met_JetRelativeSampleDown", "metphi_JetRelativeSampleDown", "pt_1_nominal", "phi_1", "pt_2_nominal", "phi_2"});

    }
  }
  // do not do systematics, or is data
  return df2;

}


/**********************************************************/

#endif
