#include "sampleConfig_class.h"

/*************************************************************************/

namespace LUNA {

    // Constructor
    sampleConfig_t::sampleConfig_t(std::string processName) {
        sampleName = processName;

        if (Helper::containsSubstring(sampleName, "SUSY") || Helper::containsSubstring(sampleName, "Cascade")) { // signal
            isSampleSignal = true;
            isSampleMC = true;
            isSampleData = false;
            isSampleEmbedded = false;
        }
        else if (!Helper::containsSubstring(sampleName, "Run")) { // MC and not signal
            isSampleSignal = false;
            isSampleMC = true;
            isSampleData = false;
            isSampleEmbedded = false;
        }
        else if (Helper::containsSubstring(sampleName, "Embed")) { // Embedded
            isSampleSignal = false;
            isSampleMC = false;
            isSampleData = false;
            isSampleEmbedded = true;
        }
        else {  // Data
            isSampleSignal = false;
            isSampleMC = false;
            isSampleData = true;
            isSampleEmbedded = false;
        }
    }

    // Setters
    void sampleConfig_t::setEra(std::string era) {
        sampleEra = era;
        if (era == "2018") {
            sampleYear = 2018;
        }
        else if (era == "2017") {
            sampleYear = 2017;
        }
        else if ((era == "2016preVFP") || (era == "2016postVFP")) {
            sampleYear = 2016;
        }
        else {
            sampleYear = 2018;
        }
    }
    void sampleConfig_t::setDoSystematics(int performSys) { doSystematics = performSys; }

    // Getters
    std::string sampleConfig_t::name() { return sampleName; }
    int sampleConfig_t::isData()     { return isSampleData; }
    int sampleConfig_t::isEmbedded() { return isSampleEmbedded; }
    int sampleConfig_t::isMC()       { return isSampleMC; }
    int sampleConfig_t::isSignal()   { return isSampleSignal; }
    int sampleConfig_t::year() { return sampleYear; }
    std::string sampleConfig_t::yearStr() { return std::to_string(year()); }
    std::string sampleConfig_t::era() { return sampleEra; }

    int sampleConfig_t::isMuTauOnly() {
        int data = (isData() && Helper::containsSubstring(sampleName, "SingleMuon-Run"));
        int embed = (isEmbedded() && Helper::containsSubstring(sampleName, "MuTau"));
        return (data || embed);
    }

    int sampleConfig_t::isETauOnly() {
        int data = (isData() && (Helper::containsSubstring(sampleName, "EGamma-Run") || (Helper::containsSubstring(sampleName, "SingleElectron-Run"))));
        int embed = (isEmbedded() && Helper::containsSubstring(sampleName, "ElTau"));
        return (data || embed);
    }

    int sampleConfig_t::isEMuOnly() {
        int data = (isData() && Helper::containsSubstring(sampleName, "MuonEG-Run"));
        int embed = (isEmbedded() && (Helper::containsSubstring(sampleName, "EMu") || Helper::containsSubstring(sampleName, "ElMu")));
        return (data || embed);
    }

    int sampleConfig_t::doSys() { return doSystematics; }

    int sampleConfig_t::isTTbar() { return Helper::containsSubstring(sampleName, "TTTo"); }
    int sampleConfig_t::isDY()    { return Helper::containsSubstring(sampleName, "DY"); }
    int sampleConfig_t::isWJets() {
        return (Helper::containsSubstring(sampleName, "W") && Helper::containsSubstring(sampleName, "JetsToLNu"));
    }

    // Recoil
    int sampleConfig_t::isHiggsRecoil() {
        return (Helper::containsSubstring(sampleName, "GluGluH")
        || Helper::containsSubstring(sampleName, "VBFH")
        || Helper::containsSubstring(sampleName, "SUSY"));
    }
    int sampleConfig_t::doRecoil() {
        return (isDY() ||  isWJets() || isHiggsRecoil());
    }
    int sampleConfig_t::recoilBosonPdgId() {
        if (doRecoil()) {
            if (isDY())               { return GenHelper::pdgId_Z; }
            else if (isWJets())       { return GenHelper::pdgId_W; }
            else if (isHiggsRecoil()) { return GenHelper::pdgId_H; }
        }
        return -1;
    }
    int sampleConfig_t::recoilType() {
        if (doRecoil()) {
            if (isWJets())                      { return 1; }
            else if (isDY() || isHiggsRecoil()) { return 2; }
        }
        return 0;
    }

    int sampleConfig_t::isMCnonHiggs() {
        // int isMCnonHiggs = (sample!="data_obs" && sample!="embedded" && !isSignal && name!="ggh_htt" && name!="ggh_hww" && name!="qqh_htt" && name!="qqh_hww" && name!="Zh_htt" && name!="Zh_hww" && name!="Wh_htt" && name!="Wh_hww" && name!="tth");
        return (isMC() && !isData() && !isEmbedded() && !isSignal()
                && !(Helper::containsSubstring(sampleName, "GluGluHToTauTau"))
                && !(Helper::containsSubstring(sampleName, "GluGluHToWW"))
                && !(Helper::containsSubstring(sampleName, "VBFHToTauTau"))
                && !(Helper::containsSubstring(sampleName, "VBFHToWW"))
                && !(Helper::containsSubstring(sampleName, "ZHToTauTau"))
                && !(Helper::containsSubstring(sampleName, "GluGluZH_HToWW"))
                && !(Helper::containsSubstring(sampleName, "HWplusJ_HToWW"))
                && !(Helper::containsSubstring(sampleName, "HWminusJ_HToWW"))
                && !(Helper::containsSubstring(sampleName, "WminusHToTauTau"))
                && !(Helper::containsSubstring(sampleName, "WplusHToTauTau"))
                && !(Helper::containsSubstring(sampleName, "ttHToNonbb"))
                && !(Helper::containsSubstring(sampleName, "ttHTobb"))
                && !(Helper::containsSubstring(sampleName, "HZJ_HToWW"))
                );
    }

    std::string sampleConfig_t::getRunTag() {
        if (isData() || isEmbedded()) {
            // yes this code is awful
            if (Helper::containsAnyOfTheseSubstrings(sampleName, {"2018A"}))                   return "A";
            if (Helper::containsAnyOfTheseSubstrings(sampleName, {"2016B", "2017B", "2018B"})) return "B";
            if (Helper::containsAnyOfTheseSubstrings(sampleName, {"2016C", "2017C", "2018C"})) return "C";
            if (Helper::containsAnyOfTheseSubstrings(sampleName, {"2016D", "2017D", "2018D"})) return "D";
            if (Helper::containsAnyOfTheseSubstrings(sampleName, {"2016E", "2017E"}))          return "E";
            if (Helper::containsAnyOfTheseSubstrings(sampleName, {"2016F", "2017F"}))          return "F";
            if (Helper::containsAnyOfTheseSubstrings(sampleName, {"2016G"}))                   return "G";
            if (Helper::containsAnyOfTheseSubstrings(sampleName, {"2016H"}))                   return "H";
        }
        return "";
    }

}


/*************************************************************************/
