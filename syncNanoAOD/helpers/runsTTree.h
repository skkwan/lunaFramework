// runsTTree.h

// Helper functions for handling Runs TTree in NanoAOD files


#ifndef RUNS_TTREE_H_INCL
#define RUNS_TTREE_H_INCL


/******************************************************************************/

// Return the sum of the branch genEventSumw in the dataframe (assuming it was
// loaded from a TChain with the "Runs" TTree).

template <typename T>
double SumGenEventSumw(T &df, LUNA::sampleConfig_t &sConfig) {

  double genEventSumw = 1.0;

  if (sConfig.name().find("Run") == std::string::npos) {
    // Not data or embedded (MC)
    auto sumw = df.Sum("genEventSumw");
    genEventSumw = (double) *sumw;
  }
  std::cout << ">>> runsTTree.h: genEventSumw " << genEventSumw << std::endl;

  return genEventSumw;
}

/******************************************************************************/

// Return the sum of the branch getEventCount in the dataframe.

template <typename T>
double SumGenEventCount(T &df, LUNA::sampleConfig_t &sConfig) {

  double genEventCount = 1.0;

  if (sConfig.name().find("Run") == std::string::npos) {
    // Non-data
    auto sumEvt = df.Sum("genEventCount");
    genEventCount = (double) *sumEvt;
  }

  return genEventCount;

}

/******************************************************************************/

#endif
