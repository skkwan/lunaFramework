
// Helper functions for muons.

#ifndef MUON_H_INCL
#define MUON_H_INCL

using namespace ROOT::VecOps;

/******************************************************************************/

/*
 * For a muon (candidate for a decay from a tau), map Muon_genPartFlav to the
 * naming conventions for Tau_genPartFlav.
 */

unsigned int convertMuGenPartFlavToTauGenPartFlav(unsigned char Muon_genPartFlav)
{
  unsigned int newFlav;

  if (Muon_genPartFlav == 15)      // muon from prompt tau
    newFlav = 4;                   // tau->mu decay
  else if (Muon_genPartFlav == 1)  // prompt muon
    newFlav = 2;                   // prompt muon
  else                             // {5, 4, 3} muon from {b, c, light or unknown}, 0 = unmatched
    newFlav = 0;                   // unknown or unmatched

  return newFlav;
}


/******************************************************************************/


#endif
