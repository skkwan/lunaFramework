#include "genHelper.h"


/***************************************************************************/

namespace GenHelper {

  // fromHardProcess is 8th bit
  bool fromHardProcess(int statusFlag) {
    return ((statusFlag >> 8) & 0b1);
  }

  bool fromHardProcessFinalState(int status, int statusFlag) {
    return ((status == 1) && fromHardProcess(statusFlag));
  }

  // isDirectHardProcessTauDecayProduct is 10th bit
  bool isDirectHardProcessTauDecayProduct(int statusFlag) {
    return ((statusFlag >> 10) & 0b1);
  }

  // isLastCopy is 13th bit
  bool isLastCopy(int statusFlag) {
    return ((statusFlag >> 13) & 0b1);
  }

  // Is something a neutrino
  bool isNeutrino(int pdgId) {
    return ((abs(pdgId) == pdgId_nu_ele) || (abs(pdgId) == pdgId_nu_mu) || (abs(pdgId) == pdgId_nu_tau));
  }

  bool isMuon(int pdgId) {
    return (abs(pdgId) == pdgId_mu);
  }

  bool isElectron(int pdgId) {
    return (abs(pdgId) == pdgId_ele);
  }

/***************************************************************************/
  // Functions to check parents
  /***************************************************************************/

  /*
   * Returns whether a gen-level particle (whose index to GenPart is "this_genPartIdx")
   * came from the decay of a desired parent particle (not necessarily a direct decay product,
   * can be later in the chain).
   */
  bool IsFromParent(ROOT::RVec<int>& GenPart_genPartIdxMother,
		    ROOT::RVec<int>& GenPart_pdgId,
		    const int targetParent_pdgId, // e.g. GenHelper::pdgId_a
		    const int this_genPartIdx) {

    // Initialize the index of the parent
    int my_genPartIdx = GenPart_genPartIdxMother[this_genPartIdx];

    // While the current particle is not one of the initial particles... and
    // while there is still a mother, and while the my_genPartIdx is within bounds
    while ((my_genPartIdx > 0) &&
	   (GenPart_genPartIdxMother[my_genPartIdx] > 0) &&
	   (my_genPartIdx <= (int) GenPart_pdgId.size())) {

      // Current particle's PDG ID
      int pdgId_i = GenPart_pdgId[my_genPartIdx];
      //      std::cout << "pdgId: " << GenPart_pdgId[my_genPartIdx] << ", GenPart_status[my_genPartIdx]: " << GenPart_status[my_genPartIdx]<< std::endl;

      // See if it originated from desired parent; return if yes
      if (abs(pdgId_i) == targetParent_pdgId) {
        return true;
      }
      // Update which particle we are looking at
      my_genPartIdx = GenPart_genPartIdxMother[my_genPartIdx];
    }

    return false;
  }

  /**************************************/


  /*
   * Checks whether a gen-level particle came from directly from a Z boson (for DY samples).
   */
  bool IsDirectDecayProductOfParent(ROOT::RVec<int>& GenPart_genPartIdxMother,
				    ROOT::RVec<int>& GenPart_pdgId,
				    const int targetDirectParent_pdgId, // e.g. GenHelper::pdgId_a
				    const int this_genPartIdx) {
    // Get the parent INDEX
    int myParent_genPartIdx = GenPart_genPartIdxMother[this_genPartIdx];

    // Get the tau's parent PDG ID
    int myParent_pdgId = GenPart_pdgId[myParent_genPartIdx];

    // Return whether the parent is exactly the desired parent
    return (bool) (abs(myParent_pdgId) == targetDirectParent_pdgId);
  }


  /**************************************/

  int get_direct_genPartIdx(ROOT::RVec<int>& GenPart_genPartIdxMother, ROOT::RVec<int>& GenPart_pdgId,
                                          const int targetDirectParent_pdgId,
                                          const int targetDaughter_pdgId)
  {
    int direct_genPartIdx = -1;
    for (size_t genIdx = 0; genIdx < GenPart_pdgId.size(); genIdx++) {
      // Only look for the specific target daughter pdg Id
      if (GenPart_pdgId[genIdx] == targetDaughter_pdgId) {
        // If it is a direct decay product of the parent
        if (IsDirectDecayProductOfParent(GenPart_genPartIdxMother, GenPart_pdgId, targetDirectParent_pdgId, genIdx)) {
          direct_genPartIdx = genIdx;
        }
      }
    }
    assert(direct_genPartIdx != -1);
    return direct_genPartIdx;
  }


  /**************************************/

  std::vector<int> get_cascade_genPartIdx(ROOT::RVec<int>& GenPart_genPartIdxMother, ROOT::RVec<int>& GenPart_pdgId,
                                          const int targetDirectParent_pdgId,
                                          const int targetDaughter_pdgId)
  {
    std::vector<int> cascade_daughter_genPartIdx;

    for (size_t genIdx = 0; genIdx < GenPart_pdgId.size(); genIdx++) {
      // Only look for the specific target daughter pdg Id
      if (GenPart_pdgId[genIdx] == targetDaughter_pdgId) {
        // If it is a direct decay product of the parent
        if (IsDirectDecayProductOfParent(GenPart_genPartIdxMother, GenPart_pdgId, targetDirectParent_pdgId, genIdx)) {
          cascade_daughter_genPartIdx.push_back(genIdx);
        }
      }
    }
    // In the cascade MC there should only be two such daughters
    assert(cascade_daughter_genPartIdx.size() == 2);
    return cascade_daughter_genPartIdx;
  }

}
