#ifndef GEN_PARTICLE_H_INCL
#define GEN_PARTICLE_H_INCL

#include <Math/Vector4D.h>


namespace GenHelper {

    /*
     * Class to represent generic gen-level particle information.
     */
    class genParticle_t {
        private:
            int pdg_id;
            ROOT::Math::PtEtaPhiMVector gen_p4;

        public:
            // Constructors
            genParticle_t();
            genParticle_t(int pdgId, ROOT::Math::PtEtaPhiMVector p4);

            // Getters
            int pdgId() const { return pdg_id; };
            ROOT::Math::PtEtaPhiMVector genP4() const { return gen_p4; }
            float genMass() const { return gen_p4.M(); }
            float genPt() const { return gen_p4.Pt(); }
            float genEta() const { return gen_p4.Eta(); }
            float genPhi() const { return gen_p4.Phi(); }

    };

    /*
     * Compare pT of two particles
     */
    bool compareGenParticlePt(const genParticle_t& lhs, const genParticle_t& rhs);

}

#endif
