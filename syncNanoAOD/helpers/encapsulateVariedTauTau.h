#ifndef ENCAPSULATE_VARIED_TAUTAU_H_INCL
#define ENCAPSULATE_VARIED_TAUTAU_H_INCL

#include "helperFunctions.h"

/******************************************************************************/


/*
 * Since the TTree contains all channel's events mixed together, pt_1 will mean something different for each channel:
 * This is expressed in pt_1_nominal, which will get the muon energy scale-shifted pt_1 if the channel is mutau, etc.
 * Then later in the code, pt_1_nominal can be called in any function, and it will have the MuonES shift for mutau, Electron ES shift for emu, etc.
 * (Later functions still need to handle the three channels correctly.)
 */


template <typename T>
auto EncapsulateVariedTauTau(T &df, LUNA::sampleConfig_t &sConfig){


    if (sConfig.isMC() || sConfig.isEmbedded()) {
        auto df2 = df.Define("pt_1_nominal", Helper::getValueByChannel, {"channel", "pt_1_mt_withMuonES",      "pt_1_et_withEleES",      "pt_1_em_withEleES"})
                 .Define("m_1_nominal", Helper::getValueByChannel, {"channel", "m_1_mt_withMuonES",      "m_1_et_withEleES",      "m_1_em_withEleES"})
                 .Define("pt_2_nominal", Helper::getValueByChannel, {"channel", "pt_2_mt_withTauES",    "pt_2_et_withTauES",    "pt_2_em_withMuonES"})
                 .Define("m_2_nominal", Helper::getValueByChannel, {"channel", "m_2_mt_withTauES",    "m_2_et_withTauES",    "m_2_em_withMuonES"})
                 .Define("met_nominal",    Helper::getValueByChannel, {"channel", "met_mt_withMuonES_withTauES", "met_et_withEleES_withTauES", "met_em_withEleES_withMuonES"})
                 .Define("metphi_nominal", Helper::getValueByChannel, {"channel", "metphi_mt_withMuonES_withTauES", "metphi_et_withEleES_withTauES", "metphi_em_withEleES_withMuonES"})

                  // Channel-independent definitions
                  .Define("p4_1_nominal", add_p4, {"pt_1_nominal", "eta_1", "phi_1", "m_1_nominal"})
                  .Define("p4_2_nominal", add_p4, {"pt_2_nominal", "eta_2", "phi_2", "m_2_nominal"})
                  .Define("m_vis_nominal",  [](ROOT::Math::PtEtaPhiMVector p4_1, ROOT::Math::PtEtaPhiMVector p4_2) { return (float) (p4_1+p4_2).M();  }, {"p4_1_nominal", "p4_2_nominal"})
                  .Define("pt_vis_nominal", [](ROOT::Math::PtEtaPhiMVector p4_1, ROOT::Math::PtEtaPhiMVector p4_2) { return (float) (p4_1+p4_2).Pt(); }, {"p4_1_nominal", "p4_2_nominal"});

        if (sConfig.doSys()) {

        // the first leg cannot be shifted by es2
        return df2.Define("pt_1_es1Up",    Helper::getValueByChannel, {"channel", "pt_1_mt_withMuonES_es1Up",   "pt_1_et_withEleES_es1Up",   "pt_1_em_withEleES_es1Up"})
                 .Define("pt_1_es1Down",  Helper::getValueByChannel, {"channel", "pt_1_mt_withMuonES_es1Down", "pt_1_et_withEleES_es1Down", "pt_1_em_withEleES_es1Down"})
                 .Define("m_1_es1Up",    Helper::getValueByChannel, {"channel", "m_1_mt_withMuonES_es1Up",   "m_1_et_withEleES_es1Up",   "m_1_em_withEleES_es1Up"})
                 .Define("m_1_es1Down",  Helper::getValueByChannel, {"channel", "m_1_mt_withMuonES_es1Down", "m_1_et_withEleES_es1Down", "m_1_em_withEleES_es1Down"})

                 // shifts due to second leg's lepton energy scale
                 .Define("pt_2_es2Up",    Helper::getValueByChannel, {"channel", "pt_2_mt_withMuonES_withTauES_es2Up",   "pt_2_et_withEleES_withTauES_es2Up",   "pt_2_em_withEleES_withMuonES_es2Up"})
                 .Define("pt_2_es2Down",  Helper::getValueByChannel, {"channel", "pt_2_mt_withMuonES_withTauES_es2Down", "pt_2_et_withEleES_withTauES_es2Down", "pt_2_em_withEleES_withMuonES_es2Down"})
                 .Define("m_2_es2Up",    Helper::getValueByChannel, {"channel", "m_2_mt_withMuonES_withTauES_es2Up",   "m_2_et_withEleES_withTauES_es2Up",   "m_2_em_withEleES_withMuonES_es2Up"})
                 .Define("m_2_es2Down",  Helper::getValueByChannel, {"channel", "m_2_mt_withMuonES_withTauES_es2Down", "m_2_et_withEleES_withTauES_es2Down", "m_2_em_withEleES_withMuonES_es2Down"})

                 .Define("met_es2Up",    Helper::getValueByChannel, {"channel", "met_mt_withMuonES_withTauES_es2Up",   "met_et_withEleES_withTauES_es2Up",   "met_em_withEleES_withMuonES_es2Up"})
                 .Define("met_es2Down",  Helper::getValueByChannel, {"channel", "met_mt_withMuonES_withTauES_es2Down", "met_et_withEleES_withTauES_es2Down", "met_em_withEleES_withMuonES_es2Down"})
                 .Define("metphi_es2Up",    Helper::getValueByChannel, {"channel", "metphi_mt_withMuonES_withTauES_es2Up",   "metphi_et_withEleES_withTauES_es2Up",   "metphi_em_withEleES_withMuonES_es2Up"})
                 .Define("metphi_es2Down",  Helper::getValueByChannel, {"channel", "metphi_mt_withMuonES_withTauES_es2Down", "metphi_et_withEleES_withTauES_es2Down", "metphi_em_withEleES_withMuonES_es2Down"})


                // shifts due to the first leg's lepton energy scale up/downs. pt_2 and m_2 as written below are probably superfluous (it is the same as the nominal, but to be pedantic)
                // in emu, there is no way the electron ES can affect the muon pt or mass.
                 .Define("pt_2_es1Up",    Helper::getValueByChannel, {"channel", "pt_2_mt_withMuonES_withTauES_es1Up",   "pt_2_et_withEleES_withTauES_es1Up",   "pt_2_em_withMuonES"})
                 .Define("pt_2_es1Down",  Helper::getValueByChannel, {"channel", "pt_2_mt_withMuonES_withTauES_es1Down", "pt_2_et_withEleES_withTauES_es1Down", "pt_2_em_withMuonES"})
                 .Define("m_2_es1Up",    Helper::getValueByChannel, {"channel", "m_2_mt_withMuonES_withTauES_es1Up",   "m_2_et_withEleES_withTauES_es1Up",   "m_2_em_withMuonES"})
                 .Define("m_2_es1Down",  Helper::getValueByChannel, {"channel", "m_2_mt_withMuonES_withTauES_es1Down", "m_2_et_withEleES_withTauES_es1Down", "m_2_em_withMuonES"})

                .Define("met_es1Up",    Helper::getValueByChannel, {"channel", "met_mt_withMuonES_withTauES_es1Up", "met_et_withEleES_withTauES_es1Up", "met_em_withEleES_withMuonES_es1Up"})
                .Define("metphi_es1Up", Helper::getValueByChannel, {"channel", "metphi_mt_withMuonES_withTauES_es1Up", "metphi_et_withEleES_withTauES_es1Up", "metphi_em_withEleES_withMuonES_es1Up"})
                .Define("met_es1Down",    Helper::getValueByChannel, {"channel", "met_mt_withMuonES_withTauES_es1Down", "met_et_withEleES_withTauES_es1Down", "met_em_withEleES_withMuonES_es1Down"})
                .Define("metphi_es1Down", Helper::getValueByChannel, {"channel", "metphi_mt_withMuonES_withTauES_es1Down", "metphi_et_withEleES_withTauES_es1Down", "metphi_em_withEleES_withMuonES_es1Down"})

                // Channel-independent definitions
                  .Define("p4_1_es1Up", add_p4, {"pt_1_es1Up", "eta_1", "phi_1", "m_1_es1Up"})
                  .Define("p4_2_es2Up", add_p4, {"pt_2_es2Up", "eta_2", "phi_2", "m_2_es2Up"})

                  .Define("p4_1_es1Down", add_p4, {"pt_1_es1Down", "eta_1", "phi_1", "m_1_es1Down"})
                  .Define("p4_2_es2Down", add_p4, {"pt_2_es2Down", "eta_2", "phi_2", "m_2_es2Down"})

                  .Define("m_vis_es1Up",  [](ROOT::Math::PtEtaPhiMVector p4_1, ROOT::Math::PtEtaPhiMVector p4_2) { return (float) (p4_1+p4_2).M();  }, {"p4_1_es1Up", "p4_2_nominal"})
                  .Define("pt_vis_es1Up", [](ROOT::Math::PtEtaPhiMVector p4_1, ROOT::Math::PtEtaPhiMVector p4_2) { return (float) (p4_1+p4_2).Pt(); }, {"p4_1_es1Up", "p4_2_nominal"})
                  .Define("m_vis_es1Down",  [](ROOT::Math::PtEtaPhiMVector p4_1, ROOT::Math::PtEtaPhiMVector p4_2) { return (float) (p4_1+p4_2).M();  }, {"p4_1_es1Down", "p4_2_nominal"})
                  .Define("pt_vis_es1Down", [](ROOT::Math::PtEtaPhiMVector p4_1, ROOT::Math::PtEtaPhiMVector p4_2) { return (float) (p4_1+p4_2).Pt(); }, {"p4_1_es1Down", "p4_2_nominal"})

                  .Define("m_vis_es2Up",  [](ROOT::Math::PtEtaPhiMVector p4_1, ROOT::Math::PtEtaPhiMVector p4_2) { return (float) (p4_1+p4_2).M();  },   {"p4_1_nominal", "p4_2_es2Up"})
                  .Define("pt_vis_es2Up", [](ROOT::Math::PtEtaPhiMVector p4_1, ROOT::Math::PtEtaPhiMVector p4_2) { return (float) (p4_1+p4_2).Pt(); },   {"p4_1_nominal", "p4_2_es2Up"})
                  .Define("m_vis_es2Down",  [](ROOT::Math::PtEtaPhiMVector p4_1, ROOT::Math::PtEtaPhiMVector p4_2) { return (float) (p4_1+p4_2).M();  }, {"p4_1_nominal", "p4_2_es2Down"})
                  .Define("pt_vis_es2Down", [](ROOT::Math::PtEtaPhiMVector p4_1, ROOT::Math::PtEtaPhiMVector p4_2) { return (float) (p4_1+p4_2).Pt(); }, {"p4_1_nominal", "p4_2_es2Down"});


            }
            else {
                return df2;
            }
    }
    else {
        // Data
        return df.Define("pt_1_nominal", [](float pt_1) { return pt_1; }, {"pt_1"})
                 .Define("m_1_nominal",  [](float m_1)  { return m_1; }, {"m_1"})
                 .Define("pt_2_nominal", [](float pt_2) { return pt_2; }, {"pt_2"})
                 .Define("m_2_nominal",  [](float m_2)  { return m_2; }, {"m_2"})
                 .Define("met_nominal",  [](float met)  { return met; }, {"met"})
                 .Define("metphi_nominal",  [](float metphi)  { return metphi; }, {"metphi"})
                 .Define("p4_1_nominal", add_p4, {"pt_1_nominal", "eta_1", "phi_1", "m_1_nominal"})
                 .Define("p4_2_nominal", add_p4, {"pt_2_nominal", "eta_2", "phi_2", "m_2_nominal"})
                 .Define("m_vis_nominal",  [](ROOT::Math::PtEtaPhiMVector p4_1, ROOT::Math::PtEtaPhiMVector p4_2) { return (float) (p4_1+p4_2).M();  }, {"p4_1_nominal", "p4_2_nominal"})
                 .Define("pt_vis_nominal", [](ROOT::Math::PtEtaPhiMVector p4_1, ROOT::Math::PtEtaPhiMVector p4_2) { return (float) (p4_1+p4_2).Pt(); }, {"p4_1_nominal", "p4_2_nominal"});

    }
}

/******************************************************************************/

#endif
