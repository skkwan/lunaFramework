// finalVariables.h
//
// Declarations and functions to write the output n-tuple.

#ifndef FINALVARIABLES_H
#define FINALVARIABLES_H

#include "ROOT/RSnapshotOptions.hxx"

#include "eventLevelVariables.h"


/*
 * Declare all variables which should always appear in the final n-tuple. If one of the following
 * branches cannot be found, RDataFrame will exit with an error message.
 */

std::vector<std::string> commonVariables = {

  // Year and sample info
  "year",

  // See Helper::mt, Helper::et, Helper::em
  "channel",
  "isMuTauOnly", "isETauOnly", "isEMuOnly",

  // Event-specific variables and variables for weighing
  "run", "evt", "lumi",
  "nEntries",
  "genWeight", "genEventCount", "genEventSumw",

  // Leading legs (with and without nominal lepton energy scales)
  "idx_1", "idx_2",
  "pt_1", "eta_1", "phi_1", "m_1", "q_1", "iso_1",
  "pt_1_nominal", "m_1_nominal",
  "pt_2", "eta_2", "phi_2", "m_2", "q_2", "iso_2", "decayMode_2",
  "pt_2_nominal", "m_2_nominal",
  "m_vis_nominal", "pt_vis_nominal",

  "gen_match_1", "gen_match_2",

  // Discriminants for tauh
  "byVVVLooseDeepVSjet_2", "byVVLooseDeepVSjet_2", "byVLooseDeepVSjet_2", "byLooseDeepVSjet_2", "byMediumDeepVSjet_2", "byTightDeepVSjet_2", "byVTightDeepVSjet_2", "byVVTightDeepVSjet_2",
  "byVVVLooseDeepVSe_2", "byVVLooseDeepVSe_2", "byVLooseDeepVSe_2", "byLooseDeepVSe_2", "byMediumDeepVSe_2", "byTightDeepVSe_2", "byVTightDeepVSe_2", "byVVTightDeepVSe_2",
  "byVLooseDeepVSmu_2", "byLooseDeepVSmu_2", "byMediumDeepVSmu_2", "byTightDeepVSmu_2", // "byVTightDeepVSmu_2", "byVVTightDeepVSmu_2"

  // deepFlavour leading and sub-leading b-tagged jets
  "bpt_deepflavour_1", "beta_deepflavour_1", "bphi_deepflavour_1", "bm_deepflavour_1", "bscore_deepflavour_1", "bflavour_deepflavour_1",
  "bpt_deepflavour_2", "beta_deepflavour_2", "bphi_deepflavour_2", "bm_deepflavour_2", "bscore_deepflavour_2", "bflavour_deepflavour_2",

  // All jets
  "njets",
  "jpt_1", "jeta_1", "jphi_1",
  "jpt_2", "jeta_2", "jphi_2",
  "mjj",

  // PF MET
  "met", "metphi", "metcov00", "metcov01", "metcov10", "metcov11",
  "met_nominal", "metphi_nominal",

  // Discriminating variables
  "D_zeta_nominal", "m_btautau_vis_nominal", "pt_btautau_vis_nominal", "mtMET_1_nominal", "mtMET_2_nominal",

  // Trigger objects
  "match1", "match2",
  "trigObj_idx1", "trigObj_idx2", "type1", "type2",
  "filterBits1", "filterBits2",

  // Prefiring
  "weight_l1prefiring_nominal", "weight_l1prefiring_up", "weight_l1prefiring_down",

};


/********************************************************************/

// Variables specific to years
std::map<std::string, std::vector<std::string>> dictYearSpecificVariables = {
  {"2018",
    {
      "filter_mt_Mu24", "filter_mt_Mu27", "filter_mt_Mu20Tau27", "filter_mt_Mu20HPSTau27",
      "trigger_mt_Mu24", "trigger_mt_Mu27", "trigger_mt_Mu20Tau27", "trigger_mt_Mu20HPSTau27",
      "pass_mt_HLT_IsoMu24", "pass_mt_HLT_IsoMu27", "pass_mt_HLT_Mu20Tau27", "pass_mt_HLT_Mu20HPSTau27",
      "passMuTauTrigger",

      "filter_et_Ele32", "filter_et_Ele35", "filter_et_Ele24Tau30", "filter_et_Ele24HPSTau30",
      "trigger_et_Ele32", "trigger_et_Ele35", "trigger_et_Ele24Tau30", "trigger_et_Ele24HPSTau30",
      "pass_et_HLT_Ele32", "pass_et_HLT_Ele35", "pass_et_HLT_Ele24Tau30", "pass_et_HLT_Ele24HPSTau30",
      "passETauTrigger",

      "filter_em_Mu23Ele12", "filter_em_Mu8Ele23",
      "trigger_em_Mu23Ele12", "trigger_em_Mu8Ele23",
      "pass_em_HLT_Mu23Ele12", "pass_em_HLT_Mu8Ele23",
      "passEMuTrigger"
    }
  },

  {"2017",
    {
      "filter_mt_Mu27", "filter_mt_Mu20Tau27",
      "trigger_mt_Mu27", "trigger_mt_Mu20Tau27",
      "pass_mt_HLT_IsoMu27", "pass_mt_HLT_Mu20Tau27",
      "passMuTauTrigger",

      "filter_et_Ele32", "filter_et_Ele35", "filter_et_Ele24Tau30",
      "trigger_et_Ele32", "trigger_et_Ele35", "trigger_et_Ele24Tau30",
      "pass_et_HLT_Ele32", "pass_et_HLT_Ele35", "pass_et_HLT_Ele24Tau30",
      "passETauTrigger",

      "filter_em_Mu23Ele12", "filter_em_Mu8Ele23",
      "trigger_em_Mu23Ele12", "trigger_em_Mu8Ele23",
      "pass_em_HLT_Mu23Ele12", "pass_em_HLT_Mu8Ele23",
      "passEMuTrigger"
    }
  },

  {"2016",
    {
      "filter_mt_IsoMu22", "filter_mt_IsoTkMu22", "filter_mt_IsoMu19_eta2p1_LooseIsoPFTau20",
      "trigger_mt_IsoMu22", "trigger_mt_IsoMu22_eta2p1", "trigger_mt_IsoTkMu22", "trigger_mt_IsoTkMu22_eta2p1",
      "trigger_mt_IsoMu19_eta2p1_LooseIsoPFTau20", "trigger_mt_IsoMu19_eta2p1_LooseIsoPFTau20_SingleL1",

      "pass_mt_HLT_IsoMu22", "pass_mt_HLT_IsoMu22_eta2p1", "pass_mt_HLT_IsoTkMu22", "pass_mt_HLT_IsoTkMu22_eta2p1",
      "pass_mt_HLT_IsoMu19_eta2p1_LooseIsoPFTau20", "pass_mt_HLT_IsoMu19_eta2p1_LooseIsoPFTau20_SingleL1",
      "passMuTauTrigger",

      "filter_et_Ele25",
      "trigger_et_Ele25",
      "pass_et_HLT_Ele25_eta2p1_WPTight_Gsf",
      "passETauTrigger",

      "filter_em_Mu23Ele12", "filter_em_Mu8Ele23",
      "trigger_em_Mu23Ele12", "trigger_em_Mu8Ele23",
      "pass_em_HLT_Mu23Ele12", "pass_em_HLT_Mu8Ele23",
      "passEMuTrigger"
    }
  }

};

/********************************************************************/

// These branches may not appear in the final n-tuple; we will check if each one exists
// before adding it to the final list of branches to snapshot.

std::vector<std::string> optionalVariables = {
  // Pile-up (only meaningfully defined for MC)
  "npu", "nTrueInt",

  // Drell-Yan + Jets and W+Jets samples ONLY
  "lhe_njets_DY_W_jets",

  // Lepton energy scale shifts
  "pt_1_es1Up", "pt_1_es1Down",
  "m_1_es1Up", "m_1_es1Down",

  "pt_2_es2Up", "pt_2_es2Down",
  "m_2_es2Up", "m_2_es2Down",

  "met_es1Up", "met_es1Down",
  "met_es2Up", "met_es2Down",

  "metphi_es1Up", "metphi_es1Down",
  "metphi_es2Up", "metphi_es2Down",

  "m_vis_es1Up", "m_vis_es1Down",
  "m_vis_es2Up", "m_vis_es2Down",

  "pt_vis_es1Up", "pt_vis_es1Down",
  "pt_vis_es2Up", "pt_vis_es2Down",


  // JEC+JER corrections from applying NanoAODTools
  "bcorr_JEC_1", "bcorr_JER_1",
  "bcorr_JEC_2", "bcorr_JER_2",

  // Gen legs information for signal sample
  "genPt_tau1", "genEta_tau1", "genPhi_tau1", "genMass_tau1",
  "genPt_tau2", "genEta_tau2", "genPhi_tau2", "genMass_tau2",

  "gen_mtt", "gen_pTtt"

  // Experimental: b-jet regression information
  "bbRegCorr_deepflavour_1", "bbRegRes_deepflavour_1", "bnMuons_deepflavour_1",
  "bbRegCorr_deepflavour_2", "bbRegRes_deepflavour_2", "bnMuons_deepflavour_2",
  // Di-b-tag-jet mass, without and with b-jet regression correction, and the gen-jets di-b-tag-jet mass with neutrinos
  "mbb", "mbbCorr", "mbbGenWN",

  // Experimental: gen-jet information
  "bGenIdx_1", "bGenIdx_2",
  "bGenPt_1", "bGenPt_2",
  "bGenPtWN_1", "bGenPtWN_2",

  // Gen-top quark information (TTbar only)
  "gen_topPt_1", "gen_topPt_2",

  // Gen muon (equivalent to gen tau) information for embedded samples only
  "genPt_embLeg1", "genEta_embLeg1",
  "genPt_embLeg2", "genEta_embLeg2",

  // RECOIL CORRECTION
  // Gen recoil boson information (DY, W+Jets, ggZH, qqZH, signal only)
  "gen_recoilBoson_Px",    "gen_recoilBoson_Py",
  "gen_recoilBoson_visPx", "gen_recoilBoson_visPy",
  "nRecoilJets",
  // For Z pT reweighing (though these will be saved for all recoil correction samples)
  "gen_recoilBoson_M", "gen_recoilBoson_Pt",
  // For other datasets, these will just be equal to nominal met/metphi
  "met_responseUp", "met_responseDown",
  "met_resolutionUp", "met_resolutionDown",
  "metphi_responseUp", "metphi_responseDown",
  "metphi_resolutionUp", "metphi_resolutionDown",

  // Categorization variable shifts
  "mtMET_1_es1Up", "mtMET_1_es1Down",
  "mtMET_2_es2Up", "mtMET_2_es2Down",

  // mtMET shifts due to JER are in jetSysVariables

  // Shifts of m_btautau_vis due to lepton energy scales
  "m_btautau_vis_es1Up",     "pt_btautau_vis_es1Up",
  "m_btautau_vis_es1Down",   "pt_btautau_vis_es1Down",
  "m_btautau_vis_es2Up",     "pt_btautau_vis_es2Up",
  "m_btautau_vis_es2Down",   "pt_btautau_vis_es2Down",


  // Shifts of D_zeta due to lepton energy scales
  "D_zeta_es1Up",  "D_zeta_es1Down",
  "D_zeta_es2Up",  "D_zeta_es2Down",

  // Shifts of D_zeta due to JER are in jetSysVariables

};

/********************************************************************/

// Pre-approval checks only (work in progress)
const std::vector<std::string> genVars = {

  "genPt_tau1", "genEta_tau1", "genPhi_tau1", "genMass_tau1",
  "genPt_tau2", "genEta_tau2", "genPhi_tau2", "genMass_tau2",

  "gen_mtt", "gen_pTtt"
};

/*****************************************************************/

std::vector<std::string> jetSysVariables = {

  // JER systematic shifts of the pT
  "bpt_deepflavour_JERUp_1",               "bpt_deepflavour_JERDown_1",
  "bpt_deepflavour_JetAbsoluteUp_1",       "bpt_deepflavour_JetAbsoluteDown_1",
  "bpt_deepflavour_JetAbsoluteyearUp_1",   "bpt_deepflavour_JetAbsoluteyearDown_1",
  "bpt_deepflavour_JetBBEC1Up_1",          "bpt_deepflavour_JetBBEC1Down_1",
  "bpt_deepflavour_JetBBEC1yearUp_1",      "bpt_deepflavour_JetBBEC1yearDown_1",
  "bpt_deepflavour_JetEC2Up_1",            "bpt_deepflavour_JetEC2Down_1",
  "bpt_deepflavour_JetEC2yearUp_1",        "bpt_deepflavour_JetEC2yearDown_1",
  "bpt_deepflavour_JetFlavorQCDUp_1",      "bpt_deepflavour_JetFlavorQCDDown_1",
  "bpt_deepflavour_JetHFUp_1",             "bpt_deepflavour_JetHFDown_1",
  "bpt_deepflavour_JetHFyearUp_1",         "bpt_deepflavour_JetHFyearDown_1",
  "bpt_deepflavour_JetRelativeBalUp_1",    "bpt_deepflavour_JetRelativeBalDown_1",
  "bpt_deepflavour_JetRelativeSampleUp_1", "bpt_deepflavour_JetRelativeSampleDown_1",

  "bpt_deepflavour_JERUp_2",               "bpt_deepflavour_JERDown_2",
  "bpt_deepflavour_JetAbsoluteUp_2",       "bpt_deepflavour_JetAbsoluteDown_2",
  "bpt_deepflavour_JetAbsoluteyearUp_2",   "bpt_deepflavour_JetAbsoluteyearDown_2",
  "bpt_deepflavour_JetBBEC1Up_2",          "bpt_deepflavour_JetBBEC1Down_2",
  "bpt_deepflavour_JetBBEC1yearUp_2",      "bpt_deepflavour_JetBBEC1yearDown_2",
  "bpt_deepflavour_JetEC2Up_2",            "bpt_deepflavour_JetEC2Down_2",
  "bpt_deepflavour_JetEC2yearUp_2",        "bpt_deepflavour_JetEC2yearDown_2",
  "bpt_deepflavour_JetFlavorQCDUp_2",      "bpt_deepflavour_JetFlavorQCDDown_2",
  "bpt_deepflavour_JetHFUp_2",             "bpt_deepflavour_JetHFDown_2",
  "bpt_deepflavour_JetHFyearUp_2",         "bpt_deepflavour_JetHFyearDown_2",
  "bpt_deepflavour_JetRelativeBalUp_2",    "bpt_deepflavour_JetRelativeBalDown_2",
  "bpt_deepflavour_JetRelativeSampleUp_2", "bpt_deepflavour_JetRelativeSampleDown_2",

  // JER systematic shifts of the mass
  "bm_deepflavour_JERUp_1",               "bm_deepflavour_JERDown_1",
  "bm_deepflavour_JetAbsoluteUp_1",       "bm_deepflavour_JetAbsoluteDown_1",
  "bm_deepflavour_JetAbsoluteyearUp_1",   "bm_deepflavour_JetAbsoluteyearDown_1",
  "bm_deepflavour_JetBBEC1Up_1",          "bm_deepflavour_JetBBEC1Down_1",
  "bm_deepflavour_JetBBEC1yearUp_1",      "bm_deepflavour_JetBBEC1yearDown_1",
  "bm_deepflavour_JetEC2Up_1",            "bm_deepflavour_JetEC2Down_1",
  "bm_deepflavour_JetEC2yearUp_1",        "bm_deepflavour_JetEC2yearDown_1",
  "bm_deepflavour_JetFlavorQCDUp_1",      "bm_deepflavour_JetFlavorQCDDown_1",
  "bm_deepflavour_JetHFUp_1",             "bm_deepflavour_JetHFDown_1",
  "bm_deepflavour_JetHFyearUp_1",         "bm_deepflavour_JetHFyearDown_1",
  "bm_deepflavour_JetRelativeBalUp_1",    "bm_deepflavour_JetRelativeBalDown_1",
  "bm_deepflavour_JetRelativeSampleUp_1", "bm_deepflavour_JetRelativeSampleDown_1",

  "bm_deepflavour_JERUp_2",               "bm_deepflavour_JERDown_2",
  "bm_deepflavour_JetAbsoluteUp_2",       "bm_deepflavour_JetAbsoluteDown_2",
  "bm_deepflavour_JetAbsoluteyearUp_2",   "bm_deepflavour_JetAbsoluteyearDown_2",
  "bm_deepflavour_JetBBEC1Up_2",          "bm_deepflavour_JetBBEC1Down_2",
  "bm_deepflavour_JetBBEC1yearUp_2",      "bm_deepflavour_JetBBEC1yearDown_2",
  "bm_deepflavour_JetEC2Up_2",            "bm_deepflavour_JetEC2Down_2",
  "bm_deepflavour_JetEC2yearUp_2",        "bm_deepflavour_JetEC2yearDown_2",
  "bm_deepflavour_JetFlavorQCDUp_2",      "bm_deepflavour_JetFlavorQCDDown_2",
  "bm_deepflavour_JetHFUp_2",             "bm_deepflavour_JetHFDown_2",
  "bm_deepflavour_JetHFyearUp_2",         "bm_deepflavour_JetHFyearDown_2",
  "bm_deepflavour_JetRelativeBalUp_2",    "bm_deepflavour_JetRelativeBalDown_2",
  "bm_deepflavour_JetRelativeSampleUp_2", "bm_deepflavour_JetRelativeSampleDown_2",

  // PF MET JEC/JER systematic shifts
  "met_JERUp",               "met_JERDown",
  "met_JetAbsoluteUp",       "met_JetAbsoluteDown",
  "met_JetAbsoluteyearUp",   "met_JetAbsoluteyearDown",
  "met_JetBBEC1Up",          "met_JetBBEC1Down",
  "met_JetBBEC1yearUp",      "met_JetBBEC1yearDown",
  "met_JetEC2Up",            "met_JetEC2Down",
  "met_JetEC2yearUp",        "met_JetEC2yearDown",
  "met_JetFlavorQCDUp",      "met_JetFlavorQCDDown",
  "met_JetHFUp",             "met_JetHFDown",
  "met_JetHFyearUp",         "met_JetHFyearDown",
  "met_JetRelativeBalUp",    "met_JetRelativeBalDown",
  "met_JetRelativeSampleUp", "met_JetRelativeSampleDown",

  "metphi_JERUp",               "metphi_JERDown",
  "metphi_JetAbsoluteUp",       "metphi_JetAbsoluteDown",
  "metphi_JetAbsoluteyearUp",   "metphi_JetAbsoluteyearDown",
  "metphi_JetBBEC1Up",          "metphi_JetBBEC1Down",
  "metphi_JetBBEC1yearUp",      "metphi_JetBBEC1yearDown",
  "metphi_JetEC2Up",            "metphi_JetEC2Down",
  "metphi_JetEC2yearUp",        "metphi_JetEC2yearDown",
  "metphi_JetFlavorQCDUp",      "metphi_JetFlavorQCDDown",
  "metphi_JetHFUp",             "metphi_JetHFDown",
  "metphi_JetHFyearUp",         "metphi_JetHFyearDown",
  "metphi_JetRelativeBalUp",    "metphi_JetRelativeBalDown",
  "metphi_JetRelativeSampleUp", "metphi_JetRelativeSampleDown",

  // PF MET systematic shifts
  "met_UESUp",    "met_UESDown",
  "metphi_UESUp", "metphi_UESDown",

  // ADD: RECOIL CORRECTIONS
  "nRecoilJets_jerUp", "nRecoilJets_jerDown",
  "nRecoilJets_jesAbsoluteUp", "nRecoilJets_jesAbsoluteDown",
  "nRecoilJets_jesAbsoluteyearUp", "nRecoilJets_jesAbsoluteyearDown",
  "nRecoilJets_jesBBEC1Up", "nRecoilJets_jesBBEC1Down",
  "nRecoilJets_jesBBEC1yearUp", "nRecoilJets_jesBBEC1yearDown",
  "nRecoilJets_jesEC2Up", "nRecoilJets_jesEC2Down",
  "nRecoilJets_jesEC2yearUp", "nRecoilJets_jesEC2yearDown",
  "nRecoilJets_jesFlavorQCDUp", "nRecoilJets_jesFlavorQCDDown",
  "nRecoilJets_jesHFUp", "nRecoilJets_jesHFDown",
  "nRecoilJets_jesHFyearUp", "nRecoilJets_jesHFyearDown",
  "nRecoilJets_jesRelativeBalUp", "nRecoilJets_jesRelativeBalDown",
  "nRecoilJets_jesRelativeSampleUp", "nRecoilJets_jesRelativeSampleDown",

  "mtMET_1_responseUp",   "mtMET_1_responseDown",
  "mtMET_1_resolutionUp",   "mtMET_1_resolutionDown",
  "mtMET_1_UESUp",   "mtMET_1_UESDown",
  "mtMET_1_JERUp",                 "mtMET_1_JERDown",
  "mtMET_1_JetAbsoluteUp",         "mtMET_1_JetAbsoluteDown",
  "mtMET_1_JetAbsoluteyearUp",     "mtMET_1_JetAbsoluteyearDown",
  "mtMET_1_JetBBEC1Up",            "mtMET_1_JetBBEC1Down",
  "mtMET_1_JetBBEC1yearUp",        "mtMET_1_JetBBEC1yearDown",
  "mtMET_1_JetEC2Up",              "mtMET_1_JetEC2Down",
  "mtMET_1_JetEC2yearUp",          "mtMET_1_JetEC2yearDown",
  "mtMET_1_JetFlavorQCDUp",        "mtMET_1_JetFlavorQCDDown",
  "mtMET_1_JetHFUp",               "mtMET_1_JetHFDown",
  "mtMET_1_JetHFyearUp",           "mtMET_1_JetHFyearDown",
  "mtMET_1_JetRelativeBalUp",      "mtMET_1_JetRelativeBalDown",
  "mtMET_1_JetRelativeSampleUp",   "mtMET_1_JetRelativeSampleDown",

  "mtMET_2_responseUp", "mtMET_2_responseDown",
  "mtMET_2_resolutionUp",   "mtMET_2_resolutionDown",
  "mtMET_2_UESUp",   "mtMET_2_UESDown",
  "mtMET_2_JERUp",                 "mtMET_2_JERDown",
  "mtMET_2_JetAbsoluteUp",         "mtMET_2_JetAbsoluteDown",
  "mtMET_2_JetAbsoluteyearUp",     "mtMET_2_JetAbsoluteyearDown",
  "mtMET_2_JetBBEC1Up",            "mtMET_2_JetBBEC1Down",
  "mtMET_2_JetBBEC1yearUp",        "mtMET_2_JetBBEC1yearDown",
  "mtMET_2_JetEC2Up",              "mtMET_2_JetEC2Down",
  "mtMET_2_JetEC2yearUp",          "mtMET_2_JetEC2yearDown",
  "mtMET_2_JetFlavorQCDUp",        "mtMET_2_JetFlavorQCDDown",
  "mtMET_2_JetHFUp",               "mtMET_2_JetHFDown",
  "mtMET_2_JetHFyearUp",           "mtMET_2_JetHFyearDown",
  "mtMET_2_JetRelativeBalUp",      "mtMET_2_JetRelativeBalDown",
  "mtMET_2_JetRelativeSampleUp",   "mtMET_2_JetRelativeSampleDown",

  // Shifts of D_zeta due to JER ystematics
  "D_zeta_responseUp",          "D_zeta_responseDown",
  "D_zeta_resolutionUp",        "D_zeta_resolutionDown",
  "D_zeta_UESUp",               "D_zeta_UESDown",
  "D_zeta_JERUp",               "D_zeta_JERDown",
  "D_zeta_JetAbsoluteUp",       "D_zeta_JetAbsoluteDown",
  "D_zeta_JetAbsoluteyearUp",   "D_zeta_JetAbsoluteyearDown",
  "D_zeta_JetBBEC1Up",          "D_zeta_JetBBEC1Down",
  "D_zeta_JetBBEC1yearUp",      "D_zeta_JetBBEC1yearDown",
  "D_zeta_JetEC2Up",            "D_zeta_JetEC2Down",
  "D_zeta_JetEC2yearUp",        "D_zeta_JetEC2yearDown",
  "D_zeta_JetFlavorQCDUp",      "D_zeta_JetFlavorQCDDown",
  "D_zeta_JetHFUp",             "D_zeta_JetHFDown",
  "D_zeta_JetHFyearUp",         "D_zeta_JetHFyearDown",
  "D_zeta_JetRelativeBalUp",    "D_zeta_JetRelativeBalDown",
  "D_zeta_JetRelativeSampleUp", "D_zeta_JetRelativeSampleDown",

  // Shifts of m_btautau_vis and pt_btautau_vis due to JER systematics of the leading b-tag jet
  "m_btautau_vis_JERUp_1",               "m_btautau_vis_JERDown_1",
  "m_btautau_vis_JetAbsoluteUp_1",       "m_btautau_vis_JetAbsoluteDown_1",
  "m_btautau_vis_JetAbsoluteyearUp_1",   "m_btautau_vis_JetAbsoluteyearDown_1",
  "m_btautau_vis_JetBBEC1Up_1",          "m_btautau_vis_JetBBEC1Down_1",
  "m_btautau_vis_JetBBEC1yearUp_1",      "m_btautau_vis_JetBBEC1yearDown_1",
  "m_btautau_vis_JetEC2Up_1",            "m_btautau_vis_JetEC2Down_1",
  "m_btautau_vis_JetEC2yearUp_1",        "m_btautau_vis_JetEC2yearDown_1",
  "m_btautau_vis_JetFlavorQCDUp_1",      "m_btautau_vis_JetFlavorQCDDown_1",
  "m_btautau_vis_JetHFUp_1",             "m_btautau_vis_JetHFDown_1",
  "m_btautau_vis_JetHFyearUp_1",         "m_btautau_vis_JetHFyearDown_1",
  "m_btautau_vis_JetRelativeBalUp_1",    "m_btautau_vis_JetRelativeBalDown_1",
  "m_btautau_vis_JetRelativeSampleUp_1", "m_btautau_vis_JetRelativeSampleDown_1",

  "pt_btautau_vis_JERUp_1",               "pt_btautau_vis_JERDown_1",
  "pt_btautau_vis_JetAbsoluteUp_1",       "pt_btautau_vis_JetAbsoluteDown_1",
  "pt_btautau_vis_JetAbsoluteyearUp_1",   "pt_btautau_vis_JetAbsoluteyearDown_1",
  "pt_btautau_vis_JetBBEC1Up_1",          "pt_btautau_vis_JetBBEC1Down_1",
  "pt_btautau_vis_JetBBEC1yearUp_1",      "pt_btautau_vis_JetBBEC1yearDown_1",
  "pt_btautau_vis_JetEC2Up_1",            "pt_btautau_vis_JetEC2Down_1",
  "pt_btautau_vis_JetEC2yearUp_1",        "pt_btautau_vis_JetEC2yearDown_1",
  "pt_btautau_vis_JetFlavorQCDUp_1",      "pt_btautau_vis_JetFlavorQCDDown_1",
  "pt_btautau_vis_JetHFUp_1",             "pt_btautau_vis_JetHFDown_1",
  "pt_btautau_vis_JetHFyearUp_1",         "pt_btautau_vis_JetHFyearDown_1",
  "pt_btautau_vis_JetRelativeBalUp_1",    "pt_btautau_vis_JetRelativeBalDown_1",
  "pt_btautau_vis_JetRelativeSampleUp_1", "pt_btautau_vis_JetRelativeSampleDown_1",

};

/*****************************************************************/

// Save the variables in finalVariables (checking if each one exists) from a
// dataframe df to a tree called treeName, in the output file.

template <typename T>
auto SnapshotFinalVariables(T &dfFinal,
			    std::string treeName,
			    std::string output,
			    std::vector<std::string> basicVariables,
			    std::vector<std::string> optionalVariables,
			    std::vector<std::string> jerSysVariables) {

  // The branches in "basicVariables" should be available
  std::vector<std::string> finalVariables = basicVariables;

  // Now check if each branch in optionalVariables is in the dataframe
  for (std::string branch : optionalVariables) {
    if (HasExactBranch(dfFinal, branch)) {
      // std::cout << "adding.. " <<  branch << std::endl;
      finalVariables.push_back(branch);
    }
  }

  // // For jet systematic variables, just check if one of the shifts is there
  // if (HasExactBranch(dfFinal, "bpt_deepflavour_JERUp_1")) {
  //   for (std::string branch : jerSysVariables) {
  //     // std::cout << branch << std::endl;
  //     finalVariables.push_back(branch);
  //   }
  // }

  // If you want to check if each jerSysVariables branch is there, use this block
  for (std::string branch : jerSysVariables) {
    if (HasExactBranch(dfFinal, branch)) {
      // std::cout << "... adding " << branch << " to final variables " << std::endl;
      finalVariables.push_back(branch);
    }
  }

  ROOT::RDF::RSnapshotOptions opts;
  opts.fMode = "UPDATE";
  dfFinal.Snapshot(treeName, output, finalVariables, opts);


}

/*****************************************************************/

#endif
