// Helper functions for taus
// (tau energy scale is in tauES.h)

#ifndef TAU_H_INCL
#define TAU_H_INCL

#include "fileIO.h"

#include "TFile.h"
#include "TH1F.h"
#include "TObject.h"
#include "Math/Vector4D.h"


/******************************************************************************/

// Returns the Tau_genPartFlav using the Higgs to tautau naming convention:
// https://twiki.cern.ch/twiki/bin/view/CMS/HiggsToTauTauWorking2016#MC_Matching


unsigned int convertTauGenPartFlav(unsigned char Tau_genPartFlav)
{
  unsigned int newFlav;

  if (Tau_genPartFlav == 0) {
    // nanoAOD: 0 = unknown or unmatched
    newFlav = 6;
  }
  else {
    // NanoAOD documentation: Flavour of genParticle for MC matching to status==2 taus: 1 = prompt electron, 2 = prompt muon, 3 = tau->e decay, 4 = tau->mu decay, 5 = hadronic tau decay, 0 = unknown or unmatched
    newFlav = (unsigned int) Tau_genPartFlav;
  }
  return newFlav;
}


/******************************************************************************/

#endif
