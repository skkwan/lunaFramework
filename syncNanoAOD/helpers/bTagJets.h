
// Header file for b-tagged jet functions.
#ifndef BTAGJETS_H_INCL
#define BTAGJETS_H_INCL

#include "computePhysicsVariables.h"
#include "fileIO.h"
#include "finalVariables.h"
#include "helperFunctions.h"
#include "jets.h"

/*************************************************************************/

/*
 * Find all valid b-tagged jets from the jet collection.
 */

template <typename T>
auto FindGoodBTaggedJets(T &df, std::string configDir) {

  return df.Define("bTaggedFlavJets", getString(configDir + "btagJetDeepFlavourCandidates.txt"));

}

/*************************************************************************/

/*
 * Find b-tagged jets well-separated from muon and tau, with the highest
 * deepFlav score. Save the index only.
 */

template <typename T>
auto GetLeadingBTaggedJetPair(T &df) {
    using namespace ROOT::VecOps;

    /*------------------------------------------------------------------*/

    // Assuming that cuts have already been made on pT, abs(eta), jetID
    auto find_leading_pair = [](unsigned int nJet,
				RVec<int>& bTaggedJets, RVec<float>& btagScore,
				RVec<float>& Jet_eta, RVec<float>& Jet_phi,
				float eta_1,
				float eta_2,
				float phi_1,
				float phi_2){

      // Find valid jets based on delta r
      std::vector<int> validJets(nJet, 0);
      for (size_t i = 0; i < nJet; i++) {

	if (bTaggedJets[i] == 1) {
	  const auto deltar1 = Helper::DeltaR(Jet_eta[i], eta_1,
					      Jet_phi[i], phi_1);
	  const auto deltar2 = Helper::DeltaR(Jet_eta[i], eta_2,
					      Jet_phi[i], phi_2);
	  if (deltar1 > 0.5 && deltar2 > 0.5) {
	    validJets[i] = 1;
	  }
	}
      }

      // Find best two b jets based on b-tagging score
      int idx_1 = -1;
      int idx_2 = -1;
      float bestScore = -1;
      float secondBestScore = -1;
      for (size_t i = 0; i < nJet; i++) {

	if (validJets[i] == 0) continue;

	if (btagScore[i] > bestScore) {
	  // Move the previous best score to the second-best
	  secondBestScore = bestScore;
	  idx_2 = idx_1;
	  // Crown the new monarch
	  bestScore = btagScore[i];
	  idx_1 = i;
	}
	else if (btagScore[i] > secondBestScore) {
	  secondBestScore = btagScore[i];
	  idx_2 = i;
	}

      }

      return std::vector<int>({idx_1, idx_2});
    };

    /*------------------------------------------------------------------*/
    // nJet: branch in NanoAOD (slimmedJets, i.e. ak4 PFJets CHS with JECs applied, after basic selection (pt > 15))

    auto df2 = df.Define("bpairFlavIdx", find_leading_pair,
	      {"nJet", "bTaggedFlavJets", "Jet_btagDeepFlavB",
	       "Jet_eta", "Jet_phi",
	       "eta_1", "eta_2", "phi_1", "phi_2"})
      .Define("b_flav_idx_1", "bpairFlavIdx[0]")
      .Define("b_flav_idx_2", "bpairFlavIdx[1]");

      // .Filter("b_flav_idx_1 != -1", "Valid leading deepFlavour b-tag jet matched with mu-tau pair");
    return df2;
}

/*************************************************************************/

/*
 * Get the nominal JEC and JER-corrected b-jet variables if available, else
 * use the raw NanoAOD (JEC-only) values.
 */

template <typename T>
auto GetBTagNominalJERJECCorrections(T &df) {

  // Check if Jet_corr_JEC and Jet_corr_JER are available.
  if (HasExactBranch(df, "Jet_corr_JEC") && HasExactBranch(df, "Jet_corr_JER")) {

      // Leading jet for deepFlavour
    return df.Define("bpt_deepflavour_1",      Helper::GetFloat, {"Jet_pt_nom",        "b_flav_idx_1"})
      .Define("beta_deepflavour_1",     Helper::GetFloat, {"Jet_eta",           "b_flav_idx_1"})
      .Define("bphi_deepflavour_1",     Helper::GetFloat, {"Jet_phi",           "b_flav_idx_1"})
      .Define("bm_deepflavour_1",       Helper::GetFloat, {"Jet_mass_nom",      "b_flav_idx_1"})
      .Define("bscore_deepflavour_1",   Helper::GetFloat, {"Jet_btagDeepFlavB", "b_flav_idx_1"})

      // Sub-leading jet for deepFlavour
      .Define("bpt_deepflavour_2",      Helper::GetFloat, {"Jet_pt_nom",        "b_flav_idx_2"})
      .Define("beta_deepflavour_2",     Helper::GetFloat, {"Jet_eta",           "b_flav_idx_2"})
      .Define("bphi_deepflavour_2",     Helper::GetFloat, {"Jet_phi",           "b_flav_idx_2"})
      .Define("bm_deepflavour_2",       Helper::GetFloat, {"Jet_mass_nom",      "b_flav_idx_2"})
      .Define("bscore_deepflavour_2",   Helper::GetFloat, {"Jet_btagDeepFlavB", "b_flav_idx_2"})

      // Also save these in case we want to undo the JEC/JER
      .Define("bcorr_JEC_1", "Jet_corr_JEC[b_flav_idx_1]")
      .Define("bcorr_JER_1", "Jet_corr_JER[b_flav_idx_1]")
      .Define("bcorr_JEC_2", "Jet_corr_JEC[b_flav_idx_2]")
      .Define("bcorr_JER_2", "Jet_corr_JER[b_flav_idx_2]");

  }
  // Leading jet for deepFlavour

  return df.Define("bpt_deepflavour_1",      Helper::GetFloat, {"Jet_pt",            "b_flav_idx_1"}) // JEC-only if bare NanoAOD
    .Define("beta_deepflavour_1",     Helper::GetFloat, {"Jet_eta",           "b_flav_idx_1"})
    .Define("bphi_deepflavour_1",     Helper::GetFloat, {"Jet_phi",           "b_flav_idx_1"})
    .Define("bm_deepflavour_1",       Helper::GetFloat, {"Jet_mass",          "b_flav_idx_1"})
    .Define("bscore_deepflavour_1",   Helper::GetFloat, {"Jet_btagDeepFlavB", "b_flav_idx_1"})

    // Sub-leading jet for deepFlavour
    .Define("bpt_deepflavour_2",      Helper::GetFloat, {"Jet_pt",            "b_flav_idx_2"}) // JEC-only if bare NanoAOD
    .Define("beta_deepflavour_2",     Helper::GetFloat, {"Jet_eta",           "b_flav_idx_2"})
    .Define("bphi_deepflavour_2",     Helper::GetFloat, {"Jet_phi",           "b_flav_idx_2"})
    .Define("bm_deepflavour_2",       Helper::GetFloat, {"Jet_mass",          "b_flav_idx_2"})
    .Define("bscore_deepflavour_2",   Helper::GetFloat, {"Jet_btagDeepFlavB", "b_flav_idx_2"});

}

/*************************************************************************/

/*
 * Get nominal b-tag jet composite variables.
 */

template <typename T>
auto GetBTagNominalMasses(T &df) {

      // Di-b-jet invariant mass without b-jet regression correction
  return df.Define("hasTwoLeadingBTagJets", "(b_flav_idx_1 != -1) && (b_flav_idx_2 != -1)")
    .Define("bp4_1", add_p4, {"bpt_deepflavour_1", "beta_deepflavour_1", "bphi_deepflavour_1", "bm_deepflavour_1"})
    .Define("bp4_2", add_p4, {"bpt_deepflavour_2", "beta_deepflavour_2", "bphi_deepflavour_2", "bm_deepflavour_2"})
    .Define("bp4", "bp4_1 + bp4_2")
    .Define("mbb", compute_mjj, {"bp4", "hasTwoLeadingBTagJets"})

      // Experimental: b-jet regression information
      // Use the deepFlavour b-tagged jets
      .Define("bbRegCorr_deepflavour_1", "Jet_bRegCorr[b_flav_idx_1]")
      .Define("bbRegRes_deepflavour_1",  "Jet_bRegRes[b_flav_idx_1]")
      .Define("bnMuons_deepflavour_1",   "Jet_nMuons[b_flav_idx_1]")

      .Define("bbRegCorr_deepflavour_2", "Jet_bRegCorr[b_flav_idx_2]")
      .Define("bbRegRes_deepflavour_2",  "Jet_bRegRes[b_flav_idx_2]")
      .Define("bnMuons_deepflavour_2",    "Jet_nMuons[b_flav_idx_2]")

      // Experimental: b-jet regression-corrected di-b-jet invariant mass (using deep flavour)
      .Define("bptCorr_1", "bpt_deepflavour_1 * bbRegCorr_deepflavour_1")
      .Define("bptCorr_2", "bpt_deepflavour_2 * bbRegCorr_deepflavour_2")
      .Define("bp4Corr_1", add_p4, {"bptCorr_1", "beta_deepflavour_1", "bphi_deepflavour_1", "bm_deepflavour_1"})
      .Define("bp4Corr_2", add_p4, {"bptCorr_2", "beta_deepflavour_2", "bphi_deepflavour_2", "bm_deepflavour_2"})
      .Define("bp4Corr", "bp4Corr_1 + bp4Corr_2")
      .Define("mbbCorr", compute_mjj, {"bp4Corr", "hasTwoLeadingBTagJets"});

}

/*************************************************************************/

// Get additional JER systematic up/down shifts if available.

template <typename T>
auto GetBTagJERSystematics(T &df) {

  // We'll assume that if Jet_pt_jerUp exists in the input NanoAOD, the other systematic up/down
  // branches are in it
  if (HasExactBranch(df, "Jet_pt_jerUp")) {

    return df.Define("bpt_deepflavour_JERUp_1",   "Jet_pt_jerUp[b_flav_idx_1]")
             .Define("bpt_deepflavour_JERDown_1", "Jet_pt_jerDown[b_flav_idx_1]")
             .Define("bpt_deepflavour_JetAbsoluteUp_1",   "Jet_pt_jesAbsoluteUp[b_flav_idx_1]")
             .Define("bpt_deepflavour_JetAbsoluteDown_1", "Jet_pt_jesAbsoluteDown[b_flav_idx_1]")
             // Year-specific Absolute in different function
             .Define("bpt_deepflavour_JetBBEC1Up_1",   "Jet_pt_jesBBEC1Up[b_flav_idx_1]")
             .Define("bpt_deepflavour_JetBBEC1Down_1", "Jet_pt_jesBBEC1Down[b_flav_idx_1]")
             // Year-specific BBEC1 in different function
             .Define("bpt_deepflavour_JetEC2Up_1",   "Jet_pt_jesEC2Up[b_flav_idx_1]")
             .Define("bpt_deepflavour_JetEC2Down_1", "Jet_pt_jesEC2Down[b_flav_idx_1]")
             // Year-specific EC2 in different function
            .Define("bpt_deepflavour_JetFlavorQCDUp_1",   "Jet_pt_jesFlavorQCDUp[b_flav_idx_1]")
            .Define("bpt_deepflavour_JetFlavorQCDDown_1", "Jet_pt_jesFlavorQCDDown[b_flav_idx_1]")
            .Define("bpt_deepflavour_JetHFUp_1",   "Jet_pt_jesHFUp[b_flav_idx_1]")
            .Define("bpt_deepflavour_JetHFDown_1", "Jet_pt_jesHFDown[b_flav_idx_1]")
             // Year-specific HF in different function
            .Define("bpt_deepflavour_JetRelativeBalUp_1",   "Jet_pt_jesRelativeBalUp[b_flav_idx_1]")
            .Define("bpt_deepflavour_JetRelativeBalDown_1", "Jet_pt_jesRelativeBalDown[b_flav_idx_1]")
             // Year-specific RelativeSample in different function

              ///////// Same but for masses
             .Define("bm_deepflavour_JERUp_1",   "Jet_mass_jerUp[b_flav_idx_1]")
             .Define("bm_deepflavour_JERDown_1", "Jet_mass_jerDown[b_flav_idx_1]")
             .Define("bm_deepflavour_JetAbsoluteUp_1",   "Jet_mass_jesAbsoluteUp[b_flav_idx_1]")
             .Define("bm_deepflavour_JetAbsoluteDown_1", "Jet_mass_jesAbsoluteDown[b_flav_idx_1]")
             // Year-specific Absolute in different function
             .Define("bm_deepflavour_JetBBEC1Up_1",   "Jet_mass_jesBBEC1Up[b_flav_idx_1]")
             .Define("bm_deepflavour_JetBBEC1Down_1", "Jet_mass_jesBBEC1Down[b_flav_idx_1]")
             // Year-specific BBEC1 in different function
             .Define("bm_deepflavour_JetEC2Up_1",   "Jet_mass_jesEC2Up[b_flav_idx_1]")
             .Define("bm_deepflavour_JetEC2Down_1", "Jet_mass_jesEC2Down[b_flav_idx_1]")
             // Year-specific EC2 in different function
             .Define("bm_deepflavour_JetFlavorQCDUp_1",   "Jet_mass_jesFlavorQCDUp[b_flav_idx_1]")
             .Define("bm_deepflavour_JetFlavorQCDDown_1", "Jet_mass_jesFlavorQCDDown[b_flav_idx_1]")
             .Define("bm_deepflavour_JetHFUp_1",   "Jet_mass_jesHFUp[b_flav_idx_1]")
             .Define("bm_deepflavour_JetHFDown_1", "Jet_mass_jesHFDown[b_flav_idx_1]")
              // Year-specific HF in different function
             .Define("bm_deepflavour_JetRelativeBalUp_1",   "Jet_mass_jesRelativeBalUp[b_flav_idx_1]")
             .Define("bm_deepflavour_JetRelativeBalDown_1", "Jet_mass_jesRelativeBalDown[b_flav_idx_1]")
             // Year-specific RelativeSample in different function

            ///////// Repeat for sub-leading b-tag jet
            .Define("bpt_deepflavour_JERUp_2",   "Jet_pt_jerUp[b_flav_idx_2]")
            .Define("bpt_deepflavour_JERDown_2", "Jet_pt_jerDown[b_flav_idx_2]")
            .Define("bpt_deepflavour_JetAbsoluteUp_2",   "Jet_pt_jesAbsoluteUp[b_flav_idx_2]")
            .Define("bpt_deepflavour_JetAbsoluteDown_2", "Jet_pt_jesAbsoluteDown[b_flav_idx_2]")
             // Year-specific Absolute in different function
            .Define("bpt_deepflavour_JetBBEC1Up_2",   "Jet_pt_jesBBEC1Up[b_flav_idx_2]")
            .Define("bpt_deepflavour_JetBBEC1Down_2", "Jet_pt_jesBBEC1Down[b_flav_idx_2]")
             // Year-specific BBEC1 in different function
            .Define("bpt_deepflavour_JetEC2Up_2",   "Jet_pt_jesEC2Up[b_flav_idx_2]")
            .Define("bpt_deepflavour_JetEC2Down_2", "Jet_pt_jesEC2Down[b_flav_idx_2]")
            // Year-specific EC2 in different function
            .Define("bpt_deepflavour_JetFlavorQCDUp_2",   "Jet_pt_jesFlavorQCDUp[b_flav_idx_2]")
            .Define("bpt_deepflavour_JetFlavorQCDDown_2", "Jet_pt_jesFlavorQCDDown[b_flav_idx_2]")
            .Define("bpt_deepflavour_JetHFUp_2",   "Jet_pt_jesHFUp[b_flav_idx_2]")
            .Define("bpt_deepflavour_JetHFDown_2", "Jet_pt_jesHFDown[b_flav_idx_2]")
            // Year-specific HF in different function
            .Define("bpt_deepflavour_JetRelativeBalUp_2",   "Jet_pt_jesRelativeBalUp[b_flav_idx_2]")
            .Define("bpt_deepflavour_JetRelativeBalDown_2", "Jet_pt_jesRelativeBalDown[b_flav_idx_2]")
            // Year-specific RelativeSample in different function

            ///////// Repeat for sub-leading b-tag jet
            .Define("bm_deepflavour_JERUp_2",   "Jet_pt_jerUp[b_flav_idx_2]")
            .Define("bm_deepflavour_JERDown_2", "Jet_pt_jerDown[b_flav_idx_2]")
            .Define("bm_deepflavour_JetAbsoluteUp_2",   "Jet_pt_jesAbsoluteUp[b_flav_idx_2]")
            .Define("bm_deepflavour_JetAbsoluteDown_2", "Jet_pt_jesAbsoluteDown[b_flav_idx_2]")
             // Year-specific Absolute in different function
            .Define("bm_deepflavour_JetBBEC1Up_2",   "Jet_pt_jesBBEC1Up[b_flav_idx_2]")
            .Define("bm_deepflavour_JetBBEC1Down_2", "Jet_pt_jesBBEC1Down[b_flav_idx_2]")
             // Year-specific BBEC1 in different function
            .Define("bm_deepflavour_JetEC2Up_2",   "Jet_pt_jesEC2Up[b_flav_idx_2]")
            .Define("bm_deepflavour_JetEC2Down_2", "Jet_pt_jesEC2Down[b_flav_idx_2]")
            // Year-specific EC2 in different function
            .Define("bm_deepflavour_JetFlavorQCDUp_2",   "Jet_pt_jesFlavorQCDUp[b_flav_idx_2]")
            .Define("bm_deepflavour_JetFlavorQCDDown_2", "Jet_pt_jesFlavorQCDDown[b_flav_idx_2]")
            .Define("bm_deepflavour_JetHFUp_2",   "Jet_pt_jesHFUp[b_flav_idx_2]")
            .Define("bm_deepflavour_JetHFDown_2", "Jet_pt_jesHFDown[b_flav_idx_2]")
            // Year-specific HF in different function
            .Define("bm_deepflavour_JetRelativeBalUp_2",   "Jet_pt_jesRelativeBalUp[b_flav_idx_2]")
            .Define("bm_deepflavour_JetRelativeBalDown_2", "Jet_pt_jesRelativeBalDown[b_flav_idx_2]");
             // Year-specific RelativeSample in different function

  }
  return df;
}

/*************************************************************************/

// Get additional year-dependent JER systematic up/down shifts if available.
template <typename T>
auto GetBTagJERSystematicsYear(T &df, LUNA::sampleConfig_t &sConfig) {

  if ((sConfig.year() == 2018) &&
      (HasExactBranch(df, "Jet_pt_jesAbsolute_2018Up"))) {
    return df.Define("bpt_deepflavour_JetAbsoluteyearUp_1",   "Jet_pt_jesAbsolute_2018Up[b_flav_idx_1]")
             .Define("bpt_deepflavour_JetAbsoluteyearDown_1", "Jet_pt_jesAbsolute_2018Down[b_flav_idx_1]")
             .Define("bpt_deepflavour_JetBBEC1yearUp_1",   "Jet_pt_jesBBEC1_2018Up[b_flav_idx_1]")
             .Define("bpt_deepflavour_JetBBEC1yearDown_1", "Jet_pt_jesBBEC1_2018Down[b_flav_idx_1]")
             .Define("bpt_deepflavour_JetEC2yearUp_1",   "Jet_pt_jesEC2_2018Up[b_flav_idx_1]")
             .Define("bpt_deepflavour_JetEC2yearDown_1", "Jet_pt_jesEC2_2018Down[b_flav_idx_1]")
             .Define("bpt_deepflavour_JetHFyearUp_1",   "Jet_pt_jesHF_2018Up[b_flav_idx_1]")
             .Define("bpt_deepflavour_JetHFyearDown_1", "Jet_pt_jesHF_2018Down[b_flav_idx_1]")
              // Even though "Year" isn't in the name of the branch it seems to depend on year
             .Define("bpt_deepflavour_JetRelativeSampleUp_1",   "Jet_pt_jesRelativeSample_2018Up[b_flav_idx_1]")
             .Define("bpt_deepflavour_JetRelativeSampleDown_1", "Jet_pt_jesRelativeSample_2018Down[b_flav_idx_1]")
              ///// same for mass
             .Define("bm_deepflavour_JetAbsoluteyearUp_1",   "Jet_mass_jesAbsolute_2018Up[b_flav_idx_1]")
             .Define("bm_deepflavour_JetAbsoluteyearDown_1", "Jet_mass_jesAbsolute_2018Down[b_flav_idx_1]")
             .Define("bm_deepflavour_JetBBEC1yearUp_1",   "Jet_mass_jesBBEC1_2018Up[b_flav_idx_1]")
             .Define("bm_deepflavour_JetBBEC1yearDown_1", "Jet_mass_jesBBEC1_2018Down[b_flav_idx_1]")
             .Define("bm_deepflavour_JetEC2yearUp_1",   "Jet_mass_jesEC2_2018Up[b_flav_idx_1]")
             .Define("bm_deepflavour_JetEC2yearDown_1", "Jet_mass_jesEC2_2018Down[b_flav_idx_1]")
             .Define("bm_deepflavour_JetHFyearUp_1",   "Jet_mass_jesHF_2018Up[b_flav_idx_1]")
             .Define("bm_deepflavour_JetHFyearDown_1", "Jet_mass_jesHF_2018Down[b_flav_idx_1]")
              // Even though "Year" isn't in the name of the branch it seems to depend on year
             .Define("bm_deepflavour_JetRelativeSampleUp_1",   "Jet_mass_jesRelativeSample_2018Up[b_flav_idx_1]")
             .Define("bm_deepflavour_JetRelativeSampleDown_1", "Jet_mass_jesRelativeSample_2018Down[b_flav_idx_1]")


              ///////////// Repeat for sub-leading b-tag jet
             .Define("bpt_deepflavour_JetAbsoluteyearUp_2",   "Jet_pt_jesAbsolute_2018Up[b_flav_idx_2]")
             .Define("bpt_deepflavour_JetAbsoluteyearDown_2", "Jet_pt_jesAbsolute_2018Down[b_flav_idx_2]")
             .Define("bpt_deepflavour_JetBBEC1yearUp_2",   "Jet_pt_jesBBEC1_2018Up[b_flav_idx_2]")
             .Define("bpt_deepflavour_JetBBEC1yearDown_2", "Jet_pt_jesBBEC1_2018Down[b_flav_idx_2]")
             .Define("bpt_deepflavour_JetEC2yearUp_2",   "Jet_pt_jesEC2_2018Up[b_flav_idx_2]")
             .Define("bpt_deepflavour_JetEC2yearDown_2", "Jet_pt_jesEC2_2018Down[b_flav_idx_2]")
             .Define("bpt_deepflavour_JetHFyearUp_2",   "Jet_pt_jesHF_2018Up[b_flav_idx_2]")
             .Define("bpt_deepflavour_JetHFyearDown_2", "Jet_pt_jesHF_2018Down[b_flav_idx_2]")
             // Even though "Year" isn't in the name of the branch it seems to depend on year
             .Define("bpt_deepflavour_JetRelativeSampleUp_2",   "Jet_pt_jesRelativeSample_2018Up[b_flav_idx_2]")
             .Define("bpt_deepflavour_JetRelativeSampleDown_2", "Jet_pt_jesRelativeSample_2018Down[b_flav_idx_2]")

              //// same for mass
             .Define("bm_deepflavour_JetAbsoluteyearUp_2",   "Jet_mass_jesAbsolute_2018Up[b_flav_idx_2]")
             .Define("bm_deepflavour_JetAbsoluteyearDown_2", "Jet_mass_jesAbsolute_2018Down[b_flav_idx_2]")
             .Define("bm_deepflavour_JetBBEC1yearUp_2",   "Jet_mass_jesBBEC1_2018Up[b_flav_idx_2]")
             .Define("bm_deepflavour_JetBBEC1yearDown_2", "Jet_mass_jesBBEC1_2018Down[b_flav_idx_2]")
             .Define("bm_deepflavour_JetEC2yearUp_2",   "Jet_mass_jesEC2_2018Up[b_flav_idx_2]")
             .Define("bm_deepflavour_JetEC2yearDown_2", "Jet_mass_jesEC2_2018Down[b_flav_idx_2]")
             .Define("bm_deepflavour_JetHFyearUp_2",   "Jet_mass_jesHF_2018Up[b_flav_idx_2]")
             .Define("bm_deepflavour_JetHFyearDown_2", "Jet_mass_jesHF_2018Down[b_flav_idx_2]")
             // Even though "Year" isn't in the name of the branch it seems to depend on year
             .Define("bm_deepflavour_JetRelativeSampleUp_2",   "Jet_mass_jesRelativeSample_2018Up[b_flav_idx_2]")
             .Define("bm_deepflavour_JetRelativeSampleDown_2", "Jet_mass_jesRelativeSample_2018Down[b_flav_idx_2]");

  }
  else if ((sConfig.year() == 2017) &&
      (HasExactBranch(df, "Jet_pt_jesAbsolute_2017Up"))) {
    return df.Define("bpt_deepflavour_JetAbsoluteyearUp_1",   "Jet_pt_jesAbsolute_2017Up[b_flav_idx_1]")
             .Define("bpt_deepflavour_JetAbsoluteyearDown_1", "Jet_pt_jesAbsolute_2017Down[b_flav_idx_1]")
             .Define("bpt_deepflavour_JetBBEC1yearUp_1",   "Jet_pt_jesBBEC1_2017Up[b_flav_idx_1]")
             .Define("bpt_deepflavour_JetBBEC1yearDown_1", "Jet_pt_jesBBEC1_2017Down[b_flav_idx_1]")
             .Define("bpt_deepflavour_JetEC2yearUp_1",   "Jet_pt_jesEC2_2017Up[b_flav_idx_1]")
             .Define("bpt_deepflavour_JetEC2yearDown_1", "Jet_pt_jesEC2_2017Down[b_flav_idx_1]")
             .Define("bpt_deepflavour_JetHFyearUp_1",   "Jet_pt_jesHF_2017Up[b_flav_idx_1]")
             .Define("bpt_deepflavour_JetHFyearDown_1", "Jet_pt_jesHF_2017Down[b_flav_idx_1]")
              // Even though "Year" isn't in the name of the branch it seems to depend on year
             .Define("bpt_deepflavour_JetRelativeSampleUp_1",   "Jet_pt_jesRelativeSample_2017Up[b_flav_idx_1]")
             .Define("bpt_deepflavour_JetRelativeSampleDown_1", "Jet_pt_jesRelativeSample_2017Down[b_flav_idx_1]")
              ///// same for mass
             .Define("bm_deepflavour_JetAbsoluteyearUp_1",   "Jet_mass_jesAbsolute_2017Up[b_flav_idx_1]")
             .Define("bm_deepflavour_JetAbsoluteyearDown_1", "Jet_mass_jesAbsolute_2017Down[b_flav_idx_1]")
             .Define("bm_deepflavour_JetBBEC1yearUp_1",   "Jet_mass_jesBBEC1_2017Up[b_flav_idx_1]")
             .Define("bm_deepflavour_JetBBEC1yearDown_1", "Jet_mass_jesBBEC1_2017Down[b_flav_idx_1]")
             .Define("bm_deepflavour_JetEC2yearUp_1",   "Jet_mass_jesEC2_2017Up[b_flav_idx_1]")
             .Define("bm_deepflavour_JetEC2yearDown_1", "Jet_mass_jesEC2_2017Down[b_flav_idx_1]")
             .Define("bm_deepflavour_JetHFyearUp_1",   "Jet_mass_jesHF_2017Up[b_flav_idx_1]")
             .Define("bm_deepflavour_JetHFyearDown_1", "Jet_mass_jesHF_2017Down[b_flav_idx_1]")
              // Even though "Year" isn't in the name of the branch it seems to depend on year
             .Define("bm_deepflavour_JetRelativeSampleUp_1",   "Jet_mass_jesRelativeSample_2017Up[b_flav_idx_1]")
             .Define("bm_deepflavour_JetRelativeSampleDown_1", "Jet_mass_jesRelativeSample_2017Down[b_flav_idx_1]")


              ///////////// Repeat for sub-leading b-tag jet
             .Define("bpt_deepflavour_JetAbsoluteyearUp_2",   "Jet_pt_jesAbsolute_2017Up[b_flav_idx_2]")
             .Define("bpt_deepflavour_JetAbsoluteyearDown_2", "Jet_pt_jesAbsolute_2017Down[b_flav_idx_2]")
             .Define("bpt_deepflavour_JetBBEC1yearUp_2",   "Jet_pt_jesBBEC1_2017Up[b_flav_idx_2]")
             .Define("bpt_deepflavour_JetBBEC1yearDown_2", "Jet_pt_jesBBEC1_2017Down[b_flav_idx_2]")
             .Define("bpt_deepflavour_JetEC2yearUp_2",   "Jet_pt_jesEC2_2017Up[b_flav_idx_2]")
             .Define("bpt_deepflavour_JetEC2yearDown_2", "Jet_pt_jesEC2_2017Down[b_flav_idx_2]")
             .Define("bpt_deepflavour_JetHFyearUp_2",   "Jet_pt_jesHF_2017Up[b_flav_idx_2]")
             .Define("bpt_deepflavour_JetHFyearDown_2", "Jet_pt_jesHF_2017Down[b_flav_idx_2]")
             // Even though "Year" isn't in the name of the branch it seems to depend on year
             .Define("bpt_deepflavour_JetRelativeSampleUp_2",   "Jet_pt_jesRelativeSample_2017Up[b_flav_idx_2]")
             .Define("bpt_deepflavour_JetRelativeSampleDown_2", "Jet_pt_jesRelativeSample_2017Down[b_flav_idx_2]")

              //// same for mass
             .Define("bm_deepflavour_JetAbsoluteyearUp_2",   "Jet_mass_jesAbsolute_2017Up[b_flav_idx_2]")
             .Define("bm_deepflavour_JetAbsoluteyearDown_2", "Jet_mass_jesAbsolute_2017Down[b_flav_idx_2]")
             .Define("bm_deepflavour_JetBBEC1yearUp_2",   "Jet_mass_jesBBEC1_2017Up[b_flav_idx_2]")
             .Define("bm_deepflavour_JetBBEC1yearDown_2", "Jet_mass_jesBBEC1_2017Down[b_flav_idx_2]")
             .Define("bm_deepflavour_JetEC2yearUp_2",   "Jet_mass_jesEC2_2017Up[b_flav_idx_2]")
             .Define("bm_deepflavour_JetEC2yearDown_2", "Jet_mass_jesEC2_2017Down[b_flav_idx_2]")
             .Define("bm_deepflavour_JetHFyearUp_2",   "Jet_mass_jesHF_2017Up[b_flav_idx_2]")
             .Define("bm_deepflavour_JetHFyearDown_2", "Jet_mass_jesHF_2017Down[b_flav_idx_2]")
             // Even though "Year" isn't in the name of the branch it seems to depend on year
             .Define("bm_deepflavour_JetRelativeSampleUp_2",   "Jet_mass_jesRelativeSample_2017Up[b_flav_idx_2]")
             .Define("bm_deepflavour_JetRelativeSampleDown_2", "Jet_mass_jesRelativeSample_2017Down[b_flav_idx_2]");
      }
  else if ((sConfig.year() == 2016) && HasExactBranch(df, "Jet_pt_jesAbsolute_2016Up")) {
    return df.Define("bpt_deepflavour_JetAbsoluteyearUp_1",   "Jet_pt_jesAbsolute_2016Up[b_flav_idx_1]")
             .Define("bpt_deepflavour_JetAbsoluteyearDown_1", "Jet_pt_jesAbsolute_2016Down[b_flav_idx_1]")
             .Define("bpt_deepflavour_JetBBEC1yearUp_1",   "Jet_pt_jesBBEC1_2016Up[b_flav_idx_1]")
             .Define("bpt_deepflavour_JetBBEC1yearDown_1", "Jet_pt_jesBBEC1_2016Down[b_flav_idx_1]")
             .Define("bpt_deepflavour_JetEC2yearUp_1",   "Jet_pt_jesEC2_2016Up[b_flav_idx_1]")
             .Define("bpt_deepflavour_JetEC2yearDown_1", "Jet_pt_jesEC2_2016Down[b_flav_idx_1]")
             .Define("bpt_deepflavour_JetHFyearUp_1",   "Jet_pt_jesHF_2016Up[b_flav_idx_1]")
             .Define("bpt_deepflavour_JetHFyearDown_1", "Jet_pt_jesHF_2016Down[b_flav_idx_1]")
              // Even though "Year" isn't in the name of the branch it seems to depend on year
             .Define("bpt_deepflavour_JetRelativeSampleUp_1",   "Jet_pt_jesRelativeSample_2016Up[b_flav_idx_1]")
             .Define("bpt_deepflavour_JetRelativeSampleDown_1", "Jet_pt_jesRelativeSample_2016Down[b_flav_idx_1]")
              ///// same for mass
             .Define("bm_deepflavour_JetAbsoluteyearUp_1",   "Jet_mass_jesAbsolute_2016Up[b_flav_idx_1]")
             .Define("bm_deepflavour_JetAbsoluteyearDown_1", "Jet_mass_jesAbsolute_2016Down[b_flav_idx_1]")
             .Define("bm_deepflavour_JetBBEC1yearUp_1",   "Jet_mass_jesBBEC1_2016Up[b_flav_idx_1]")
             .Define("bm_deepflavour_JetBBEC1yearDown_1", "Jet_mass_jesBBEC1_2016Down[b_flav_idx_1]")
             .Define("bm_deepflavour_JetEC2yearUp_1",   "Jet_mass_jesEC2_2016Up[b_flav_idx_1]")
             .Define("bm_deepflavour_JetEC2yearDown_1", "Jet_mass_jesEC2_2016Down[b_flav_idx_1]")
             .Define("bm_deepflavour_JetHFyearUp_1",   "Jet_mass_jesHF_2016Up[b_flav_idx_1]")
             .Define("bm_deepflavour_JetHFyearDown_1", "Jet_mass_jesHF_2016Down[b_flav_idx_1]")
              // Even though "Year" isn't in the name of the branch it seems to depend on year
             .Define("bm_deepflavour_JetRelativeSampleUp_1",   "Jet_mass_jesRelativeSample_2016Up[b_flav_idx_1]")
             .Define("bm_deepflavour_JetRelativeSampleDown_1", "Jet_mass_jesRelativeSample_2016Down[b_flav_idx_1]")


              ///////////// Repeat for sub-leading b-tag jet
             .Define("bpt_deepflavour_JetAbsoluteyearUp_2",   "Jet_pt_jesAbsolute_2016Up[b_flav_idx_2]")
             .Define("bpt_deepflavour_JetAbsoluteyearDown_2", "Jet_pt_jesAbsolute_2016Down[b_flav_idx_2]")
             .Define("bpt_deepflavour_JetBBEC1yearUp_2",   "Jet_pt_jesBBEC1_2016Up[b_flav_idx_2]")
             .Define("bpt_deepflavour_JetBBEC1yearDown_2", "Jet_pt_jesBBEC1_2016Down[b_flav_idx_2]")
             .Define("bpt_deepflavour_JetEC2yearUp_2",   "Jet_pt_jesEC2_2016Up[b_flav_idx_2]")
             .Define("bpt_deepflavour_JetEC2yearDown_2", "Jet_pt_jesEC2_2016Down[b_flav_idx_2]")
             .Define("bpt_deepflavour_JetHFyearUp_2",   "Jet_pt_jesHF_2016Up[b_flav_idx_2]")
             .Define("bpt_deepflavour_JetHFyearDown_2", "Jet_pt_jesHF_2016Down[b_flav_idx_2]")
             // Even though "Year" isn't in the name of the branch it seems to depend on year
             .Define("bpt_deepflavour_JetRelativeSampleUp_2",   "Jet_pt_jesRelativeSample_2016Up[b_flav_idx_2]")
             .Define("bpt_deepflavour_JetRelativeSampleDown_2", "Jet_pt_jesRelativeSample_2016Down[b_flav_idx_2]")

              //// same for mass
             .Define("bm_deepflavour_JetAbsoluteyearUp_2",   "Jet_mass_jesAbsolute_2016Up[b_flav_idx_2]")
             .Define("bm_deepflavour_JetAbsoluteyearDown_2", "Jet_mass_jesAbsolute_2016Down[b_flav_idx_2]")
             .Define("bm_deepflavour_JetBBEC1yearUp_2",   "Jet_mass_jesBBEC1_2016Up[b_flav_idx_2]")
             .Define("bm_deepflavour_JetBBEC1yearDown_2", "Jet_mass_jesBBEC1_2016Down[b_flav_idx_2]")
             .Define("bm_deepflavour_JetEC2yearUp_2",   "Jet_mass_jesEC2_2016Up[b_flav_idx_2]")
             .Define("bm_deepflavour_JetEC2yearDown_2", "Jet_mass_jesEC2_2016Down[b_flav_idx_2]")
             .Define("bm_deepflavour_JetHFyearUp_2",   "Jet_mass_jesHF_2016Up[b_flav_idx_2]")
             .Define("bm_deepflavour_JetHFyearDown_2", "Jet_mass_jesHF_2016Down[b_flav_idx_2]")
             // Even though "Year" isn't in the name of the branch it seems to depend on year
             .Define("bm_deepflavour_JetRelativeSampleUp_2",   "Jet_mass_jesRelativeSample_2016Up[b_flav_idx_2]")
             .Define("bm_deepflavour_JetRelativeSampleDown_2", "Jet_mass_jesRelativeSample_2016Down[b_flav_idx_2]");
  }
  return df;

}

/*************************************************************************/

// Get generator-level information of the b-jets for MC samples.

template <typename T>
auto GetBTagGenInfo(T &df, LUNA::sampleConfig_t &sConfig) {

    if (sConfig.isMC()) {
      std::cout << "   >>> bTagJets.h: Processing MC (any year): get Jet_hadronFlavour." << std::endl;

      return df.Define("bflavour_deepflavour_1", Helper::GetInt, {"Jet_hadronFlavour", "b_flav_idx_1"})
	       .Define("bflavour_deepflavour_2", Helper::GetInt, {"Jet_hadronFlavour", "b_flav_idx_2"});
    }
    // Data or Embed: save null values
    return df.Define("bflavour_deepflavour_1", [](){ return (int) -9999; }, {})
             .Define("bflavour_deepflavour_2", [](){ return (int) -9999; }, {});
}

/*************************************************************************/

// Get neutrino info for b-jet regression studies, if the dataset is TTbar or the signal
// Call the helper function (in computePhysicsVariables.h) which adds back the neutrino energies

template <typename T>
auto GetBJetRegressionStudiesInfo(T &df, LUNA::sampleConfig_t &sConfig) {

    if ((sConfig.name().find("Run") == std::string::npos) && (((sConfig.name().find("TTTo") != std::string::npos) ||
							    (sConfig.name().find("SUSY") != std::string::npos )))) {
      std::cout << "    >>> bTagJets.h: TTbar or SUSY: add neutrinos to gen jets for b-jet regression study" << std::endl;
      return df.Define("bGenIdx_1", "Jet_genJetIdx[b_flav_idx_1]")  // get index into GenJetIdx
	.Define("bGenIdx_2", "Jet_genJetIdx[b_flav_idx_2]")
	.Define("bGenPt_1", "GenJet_pt[bGenIdx_1]")
	.Define("bGenPt_2", "GenJet_pt[bGenIdx_2]")
	.DefineSlot("bGenWNp4_1", get_gen_jet_with_neutrinos, {"bGenIdx_1",
	      "GenJet_pt", "GenJet_eta", "GenJet_phi", "GenJet_mass",
	      "GenPart_status", "GenPart_pdgId",
	      "GenPart_pt", "GenPart_eta", "GenPart_phi", "GenPart_mass",
	      "event", "luminosityBlock"})
	.DefineSlot("bGenWNp4_2", get_gen_jet_with_neutrinos, { "bGenIdx_2",
	      "GenJet_pt", "GenJet_eta", "GenJet_phi", "GenJet_mass",
	      "GenPart_status", "GenPart_pdgId",
	      "GenPart_pt", "GenPart_eta", "GenPart_phi", "GenPart_mass",
	      "event", "luminosityBlock"})
	// Get the genWN di-b-jet invariant mass, if there are two leading b-tag jets
	.Define("bGenWNp4", "bGenWNp4_1 + bGenWNp4_2")
	.Define("mbbGenWN", compute_mjj, {"bGenWNp4", "hasTwoLeadingBTagJets"})

	// And unpack the 4-vectors to get the gen with-neutrino pT
	.Define("bGenPtWN_1",  "bGenWNp4_1.Pt()")
	.Define("bGenPtWN_2",  "bGenWNp4_2.Pt()");
    }
    return df;
}

/*************************************************************************/

// Wrapper function for b-tag jets.

template <typename T>
auto FindLeadingBTaggedJetPair(T &df, LUNA::sampleConfig_t &sConfig) {

  auto df2 = GetLeadingBTaggedJetPair(df);     // must be done first
  auto df3 = GetBTagNominalJERJECCorrections(df2); // adds nominal pt, eta, phi, mass (using JER+JEC if available)
  auto df4 = GetBTagNominalMasses(df3);            // adds nominal m_bb etc. variables
  auto df5 = GetBTagJERSystematics(df4);           // JER systematics up/down Jet_pt if available
  auto df6 = GetBTagJERSystematicsYear(df5, sConfig);       // year-dependent JER systematics up/down Jet_pt if available
  auto df7 = GetBTagGenInfo(df6, sConfig);                  // adds branches if applicable
  auto df8 = GetBJetRegressionStudiesInfo(df7, sConfig);  // only for studies (TTbar and signal)
  return df8;
}


/*************************************************************************/

#endif
