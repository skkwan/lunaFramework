// Helper functions for muon energy scale and shifts.

#ifndef MUON_ES_H_INCL
#define MUON_ES_H_INCL

#include "computePhysicsVariables.h"
#include "fileIO.h"
#include "helperFunctions.h"
#include "sampleConfig_class.h"

#include "TFile.h"
#include "TH1F.h"
#include "TObject.h"
#include "Math/Vector4D.h"


/******************************************************************************/

/*
 * Return {muonES_up, muonES_down}. Check if  requirement that channe == Helper::mt
 */

ROOT::RVec<ROOT::RVec<ROOT::Math::PtEtaPhiMVector>> getMuonEnergyScale_variations(int channel, unsigned int muon_genMatch, float muonEta, int isMC, int isEmbedded, int year,
                                         ROOT::Math::PtEtaPhiMVector uncorrected_muon_p4, ROOT::Math::PtEtaPhiMVector uncorrected_met_p4) {

  (void) year;

  // Nominal muonES = 1.0
  float muonES_Up   = 1.0;
  float muonES_Down = 1.0;

  // Apply muon energy scale to all embedded and all MC if gen-matched to a muon.
  int isTrueMuon = (isEmbedded || (isMC && ( (muon_genMatch == 2) || (muon_genMatch == 4) )) );

  // Muon energy scale is binned in eta and the shifts are binned in eta and separated by MC vs. embedded
  // (although the shifts are the same).
  if (((channel == Helper::mt) || (channel == Helper::em)) && isTrueMuon) {
    if      ((abs(muonEta) > 0.0) && (abs(muonEta) < 1.2)) { muonES_Up = 1.004; muonES_Down = 0.996; }
    else if ((abs(muonEta) > 1.2) && (abs(muonEta) < 2.1)) { muonES_Up = 1.009; muonES_Down = 0.991; }
    else if ((abs(muonEta) > 2.1) && (abs(muonEta) < 2.4)) { muonES_Up = 1.027; muonES_Down = 0.973; }
  }

  ROOT::Math::PtEtaPhiMVector corrected_muon_p4_up = (muonES_Up * uncorrected_muon_p4);
  ROOT::Math::PtEtaPhiMVector corrected_met_p4_up  = (muonES_Up * uncorrected_met_p4);

  ROOT::Math::PtEtaPhiMVector corrected_muon_p4_down = (muonES_Down * uncorrected_muon_p4);
  ROOT::Math::PtEtaPhiMVector corrected_met_p4_down  = (muonES_Down * uncorrected_met_p4);

  return ROOT::RVec<ROOT::RVec<ROOT::Math::PtEtaPhiMVector>>{{corrected_muon_p4_up, corrected_muon_p4_down}, {corrected_met_p4_up, corrected_met_p4_down}};
}

/******************************************************************************/

/*
 * Apply the muon energy scale (MES) and shifts to generator-matched muons in MC
 * and all muons in Embedded.
 */

template <typename T>
auto GetMuonEnergyScaleShifts(T &df, LUNA::sampleConfig_t &sConfig) {

  std::cout << ">>> muonES.h: GetMuonEnergyScaleShifts: doSys() is " << sConfig.doSys() << std::endl;

  if (sConfig.isMC() || sConfig.isEmbedded()) {
    auto df2 = df.Define("muonES", [] { return 1.0f; })
                 .Define("p4_1_mt_withMuonES",   add_p4, {"pt_1", "eta_1", "phi_1", "m_1"})  // nominal is 1.0
                 .Define("p4_met_mt_withMuonES", add_p4, {"met", "zero", "metphi", "zero"})   // nominal is 1.0
                 .Define("p4_2_em_withMuonES", add_p4, {"pt_2", "eta_2", "phi_2", "m_2"})
                 .Define("p4_met_em_withEleES_withMuonES", add_p4, {"met_em_withEleES", "zero", "metphi_em_withEleES", "zero"})
                  // unpack
                 .Define("pt_1_mt_withMuonES",   [](ROOT::Math::PtEtaPhiMVector p4) { return (float) p4.Pt(); }, {"p4_1_mt_withMuonES"})
                 .Define("m_1_mt_withMuonES",    [](ROOT::Math::PtEtaPhiMVector p4) { return (float) p4.M(); },  {"p4_1_mt_withMuonES"})
                 .Define("met_mt_withMuonES",    [](ROOT::Math::PtEtaPhiMVector p4) { return (float) p4.Pt(); },  {"p4_met_mt_withMuonES"})
                 .Define("metphi_mt_withMuonES", [](ROOT::Math::PtEtaPhiMVector p4) { return (float) p4.Phi(); }, {"p4_met_mt_withMuonES"})
                  // emu: second leg is muon
                 .Define("pt_2_em_withMuonES", [](ROOT::Math::PtEtaPhiMVector p4) { return (float) p4.Pt(); }, {"p4_2_em_withMuonES"})
                 .Define("m_2_em_withMuonES",  [](ROOT::Math::PtEtaPhiMVector p4) { return (float) p4.M();  }, {"p4_2_em_withMuonES"})
                 .Define("met_em_withEleES_withMuonES",    [](ROOT::Math::PtEtaPhiMVector p4) { return (float) p4.Pt();  }, {"p4_met_em_withEleES_withMuonES"})
                 .Define("metphi_em_withEleES_withMuonES", [](ROOT::Math::PtEtaPhiMVector p4) { return (float) p4.Phi(); }, {"p4_met_em_withEleES_withMuonES"});

    if (sConfig.doSys()) {
      // The function returns a vector of vectors
      return df2.Define("p4_pair_mt_withMuonES", getMuonEnergyScale_variations, {"channel", "gen_match_1", "eta_1", "isMC", "isEmbedded", "year", "p4_1_mt_withMuonES", "p4_met_mt_withMuonES"})
                .Define("p4_1_mt_withMuonES_es1Up",     [](ROOT::RVec<ROOT::RVec<ROOT::Math::PtEtaPhiMVector>> vec) { return vec[0][0]; }, {"p4_pair_mt_withMuonES"})
                .Define("p4_1_mt_withMuonES_es1Down",   [](ROOT::RVec<ROOT::RVec<ROOT::Math::PtEtaPhiMVector>> vec) { return vec[0][1]; }, {"p4_pair_mt_withMuonES"})
                .Define("p4_met_mt_withMuonES_es1Up",   [](ROOT::RVec<ROOT::RVec<ROOT::Math::PtEtaPhiMVector>> vec) { return vec[1][0]; }, {"p4_pair_mt_withMuonES"})
                .Define("p4_met_mt_withMuonES_es1Down", [](ROOT::RVec<ROOT::RVec<ROOT::Math::PtEtaPhiMVector>> vec) { return vec[1][1]; }, {"p4_pair_mt_withMuonES"})

                // Unpack the 4-vectors
                .Define("pt_1_mt_withMuonES_es1Up",   [](ROOT::Math::PtEtaPhiMVector p4) { return (float) p4.Pt(); }, {"p4_1_mt_withMuonES_es1Up"})
                .Define("pt_1_mt_withMuonES_es1Down", [](ROOT::Math::PtEtaPhiMVector p4) { return (float) p4.Pt(); }, {"p4_1_mt_withMuonES_es1Down"})
                .Define("m_1_mt_withMuonES_es1Up",    [](ROOT::Math::PtEtaPhiMVector p4) { return (float) p4.M();  }, {"p4_1_mt_withMuonES_es1Up"})
                .Define("m_1_mt_withMuonES_es1Down",  [](ROOT::Math::PtEtaPhiMVector p4) { return (float) p4.M();  }, {"p4_1_mt_withMuonES_es1Down"})
                // For the mutau channel, the muon ES is the first set of lepton energy scales applied.
                .Define("met_mt_withMuonES_es1Up",      [](ROOT::Math::PtEtaPhiMVector p4) { return (float) p4.Pt(); },  {"p4_met_mt_withMuonES_es1Up"})
                .Define("metphi_mt_withMuonES_es1Up",   [](ROOT::Math::PtEtaPhiMVector p4) { return (float) p4.Phi(); }, {"p4_met_mt_withMuonES_es1Up"})
                .Define("met_mt_withMuonES_es1Down",    [](ROOT::Math::PtEtaPhiMVector p4) { return (float) p4.Pt(); },  {"p4_met_mt_withMuonES_es1Down"})
                .Define("metphi_mt_withMuonES_es1Down", [](ROOT::Math::PtEtaPhiMVector p4) { return (float) p4.Phi(); }, {"p4_met_mt_withMuonES_es1Down"})

                // emu: muon is second leg. For the shifted MET due to the muon energy scale variation, use the MET branch that has the nominal electron ES already applied (technically it's 1.0)
                .Define("p4_pair_em_withElectronES_withMuonES", getMuonEnergyScale_variations, {"channel", "gen_match_2", "eta_2", "isMC", "isEmbedded", "year", "p4_2_em_withMuonES", "p4_met_em_withEleES_withMuonES"})
                // emu: contd.: second leg
                .Define("p4_2_em_withEleES_withMuonES_es2Up",     [](ROOT::RVec<ROOT::RVec<ROOT::Math::PtEtaPhiMVector>> vec) { return vec[0][0]; }, {"p4_pair_em_withElectronES_withMuonES"})
                .Define("p4_2_em_withEleES_withMuonES_es2Down",   [](ROOT::RVec<ROOT::RVec<ROOT::Math::PtEtaPhiMVector>> vec) { return vec[0][1]; }, {"p4_pair_em_withElectronES_withMuonES"})
                .Define("p4_met_em_withEleES_withMuonES_es2Up",   [](ROOT::RVec<ROOT::RVec<ROOT::Math::PtEtaPhiMVector>> vec) { return vec[1][0]; }, {"p4_pair_em_withElectronES_withMuonES"})
                .Define("p4_met_em_withEleES_withMuonES_es2Down", [](ROOT::RVec<ROOT::RVec<ROOT::Math::PtEtaPhiMVector>> vec) { return vec[1][1]; }, {"p4_pair_em_withElectronES_withMuonES"})
                // Unpack
                .Define("pt_2_em_withEleES_withMuonES_es2Up",   [](ROOT::Math::PtEtaPhiMVector p4) { return (float) p4.Pt(); }, {"p4_2_em_withEleES_withMuonES_es2Up"})
                .Define("pt_2_em_withEleES_withMuonES_es2Down", [](ROOT::Math::PtEtaPhiMVector p4) { return (float) p4.Pt(); }, {"p4_2_em_withEleES_withMuonES_es2Down"})
                .Define("m_2_em_withEleES_withMuonES_es2Up",    [](ROOT::Math::PtEtaPhiMVector p4) { return (float) p4.M();  }, {"p4_2_em_withEleES_withMuonES_es2Up"})
                .Define("m_2_em_withEleES_withMuonES_es2Down",  [](ROOT::Math::PtEtaPhiMVector p4) { return (float) p4.M();  }, {"p4_2_em_withEleES_withMuonES_es2Down"})
                // These are the shifts of the met/metphi due to the muon energy scale up/down only
                .Define("met_em_withEleES_withMuonES_es2Up",      [](ROOT::Math::PtEtaPhiMVector p4) { return (float) p4.Pt();  }, {"p4_met_em_withEleES_withMuonES_es2Up"})
                .Define("metphi_em_withEleES_withMuonES_es2Up",   [](ROOT::Math::PtEtaPhiMVector p4) { return (float) p4.Phi(); }, {"p4_met_em_withEleES_withMuonES_es2Up"})
                .Define("met_em_withEleES_withMuonES_es2Down",    [](ROOT::Math::PtEtaPhiMVector p4) { return (float) p4.Pt();  }, {"p4_met_em_withEleES_withMuonES_es2Down"})
                .Define("metphi_em_withEleES_withMuonES_es2Down", [](ROOT::Math::PtEtaPhiMVector p4) { return (float) p4.Phi(); }, {"p4_met_em_withEleES_withMuonES_es2Down"})

                // But also don't forget that we have some Electron ES shifts of the met/metphi, while keeping the muon energy scale nominal, but thankfully this is 1 so let's just cheat a little
                // pt_2 is unaffected by the electron ES shift, use nominal
                .Define("met_em_withEleES_withMuonES_es1Up",      [](float f) { return f; }, {"met_em_withEleES_es1Up"})
                .Define("met_em_withEleES_withMuonES_es1Down",    [](float f) { return f; }, {"met_em_withEleES_es1Down"})
                .Define("metphi_em_withEleES_withMuonES_es1Up",   [](float f) { return f; }, {"metphi_em_withEleES_es1Up"})
                .Define("metphi_em_withEleES_withMuonES_es1Down", [](float f) { return f; }, {"metphi_em_withEleES_es1Down"});

    }
    else {
      return df2;
    }
  }

  return df;


}

/******************************************************************************/

#endif
