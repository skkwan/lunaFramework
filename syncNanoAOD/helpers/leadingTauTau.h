#ifndef LEADING_TAU_TAU_H
#define LEADING_TAU_TAU_H

#include "helperFunctions.h"
#include "leadingTauTauGenInfo.h"
#include "tauES.h"

/**********************************************************/

template <typename T>
auto GetLeadingTauTauPairInfo(T &df, LUNA::sampleConfig_t &sConfig) {

    auto df2 = df.Define("pt_1", [](int ch, int idx, RVec<float>& Muon_pt,   RVec<float>& Electron_pt)   { if (ch == Helper::mt) return Muon_pt[idx];     else if (ch == Helper::et || ch == Helper::em) return Electron_pt[idx];     else return (float) 0; }, {"channel", "idx_1", "Muon_pt",     "Electron_pt"})
                 .Define("eta_1", [](int ch, int idx, RVec<float>& Muon_eta, RVec<float>& Electron_eta)  { if (ch == Helper::mt) return Muon_eta[idx];    else if (ch == Helper::et || ch == Helper::em) return Electron_eta[idx];    else return (float) 0; }, {"channel", "idx_1", "Muon_eta",    "Electron_eta"})
                 .Define("phi_1", [](int ch, int idx, RVec<float>& Muon_phi, RVec<float>& Electron_phi)  { if (ch == Helper::mt) return Muon_phi[idx];    else if (ch == Helper::et || ch == Helper::em) return Electron_phi[idx];    else return (float) 0; }, {"channel", "idx_1", "Muon_phi",    "Electron_phi"})
                 .Define("m_1", [](int ch, int idx, RVec<float>& Muon_mass,  RVec<float>& Electron_mass) { if (ch == Helper::mt) return Muon_mass[idx];   else if (ch == Helper::et || ch == Helper::em) return Electron_mass[idx];   else return (float) 0; }, {"channel", "idx_1", "Muon_mass",   "Electron_mass"})
                 .Define("q_1", [](int ch, int idx, RVec<int>& Muon_charge,  RVec<int>& Electron_charge) { if (ch == Helper::mt) return Muon_charge[idx]; else if (ch == Helper::et || ch == Helper::em) return Electron_charge[idx]; else return (int) 0; },   {"channel", "idx_1", "Muon_charge", "Electron_charge"})
                 .Define("iso_1", [](int ch, int idx, RVec<float>& Muon_iso, RVec<float>& Electron_iso)  { if (ch == Helper::mt) return Muon_iso[idx];    else if (ch == Helper::et || ch == Helper::em) return Electron_iso[idx];    else return (float) 0.0; }, {"channel", "idx_1", "Muon_pfRelIso04_all", "Electron_pfRelIso03_all"})
                 .Define("gain_1", [](int ch, int idx, RVec<unsigned char>& Electron_seedGain)           { if (ch == Helper::et || ch == Helper::em)  return (int) Electron_seedGain[idx]; else { return (int) 1; } }, {"channel", "idx_1", "Electron_seedGain"})

                 .Define("pt_2",  [](int ch, int idx, RVec<float>& Tau_pt,   RVec<float>& Muon_pt)   { if (ch == Helper::mt || ch == Helper::et) return Tau_pt[idx];        else if (ch == Helper::em) return Muon_pt[idx];     else return (float) 0; }, {"channel", "idx_2", "Tau_pt",     "Muon_pt"})
                 .Define("eta_2", [](int ch, int idx, RVec<float>& Tau_eta,  RVec<float>& Muon_eta)  { if (ch == Helper::mt || ch == Helper::et) return Tau_eta[idx];       else if (ch == Helper::em) return Muon_eta[idx];    else return (float) 0; }, {"channel", "idx_2", "Tau_eta",    "Muon_eta"})
                 .Define("phi_2", [](int ch, int idx, RVec<float>& Tau_phi,  RVec<float>& Muon_phi)  { if (ch == Helper::mt || ch == Helper::et) return Tau_phi[idx];       else if (ch == Helper::em) return Muon_phi[idx];    else return (float) 0; }, {"channel", "idx_2", "Tau_phi",    "Muon_phi"})
                 .Define("m_2",   [](int ch, int idx, RVec<float>& Tau_mass, RVec<float>& Muon_mass) { if (ch == Helper::mt || ch == Helper::et) return Tau_mass[idx];      else if (ch == Helper::em) return Muon_mass[idx];   else return (float) 0; }, {"channel", "idx_2", "Tau_mass",   "Muon_mass"})
                 .Define("q_2",         [](int ch, int idx, RVec<int>& Tau_charge, RVec<int>&   Muon_charge) { if (ch == Helper::mt || ch == Helper::et) return Tau_charge[idx];  else if (ch == Helper::em) return Muon_charge[idx]; else return (int) 0;   }, {"channel", "idx_2", "Tau_charge", "Muon_charge"})
                 .Define("iso_2",       [](int ch, int idx, RVec<float>& Tau_iso,  RVec<float>& Muon_iso)  { if (ch == Helper::mt || ch == Helper::et) return Tau_iso[idx];       else if (ch == Helper::em) return Muon_iso[idx];    else return (float) 0; }, {"channel", "idx_2", "Tau_rawDeepTau2017v2p1VSjet", "Muon_pfRelIso04_all"})
                 .Define("decayMode_2", [](int ch, int idx, RVec<int>& Tau_decayMode) { if (ch == Helper::mt || ch == Helper::et) return Tau_decayMode[idx]; else return (int) -1;  }, {"channel", "idx_2", "Tau_decayMode"})

                 // If tau is the second leg, get the deepTauID
                 .Define("deepTauVSjet", [](int ch, int idx, RVec<unsigned char>& vsWP) { if  (ch == Helper::mt || ch == Helper::et) return vsWP[idx]; else return (unsigned char) 0; }, {"channel", "idx_2", "Tau_idDeepTau2017v2p1VSjet"})
                 .Define("deepTauVSmu",  [](int ch, int idx, RVec<unsigned char>& vsWP) { if  (ch == Helper::mt || ch == Helper::et) return vsWP[idx]; else return (unsigned char) 0; }, {"channel", "idx_2", "Tau_idDeepTau2017v2p1VSmu"})
                 .Define("deepTauVSe",   [](int ch, int idx, RVec<unsigned char>& vsWP) { if  (ch == Helper::mt || ch == Helper::et) return vsWP[idx]; else return (unsigned char) 0; }, {"channel", "idx_2", "Tau_idDeepTau2017v2p1VSe"})

                 // Unpack the deepTauID working points
                 // DeepTau2017v2p1 vs. jet
                 .Define("byVVVLooseDeepVSjet_2", [](unsigned char deepID) { return ((deepID >> 0) & 0x01); }, {"deepTauVSjet"})
                 .Define("byVVLooseDeepVSjet_2",  [](unsigned char deepID) { return ((deepID >> 1) & 0x01); }, {"deepTauVSjet"})
                 .Define("byVLooseDeepVSjet_2",   [](unsigned char deepID) { return ((deepID >> 2) & 0x01); }, {"deepTauVSjet"})
                 .Define("byLooseDeepVSjet_2",    [](unsigned char deepID) { return ((deepID >> 3) & 0x01); }, {"deepTauVSjet"})
                 .Define("byMediumDeepVSjet_2",   [](unsigned char deepID) { return ((deepID >> 4) & 0x01); }, {"deepTauVSjet"})
                 .Define("byTightDeepVSjet_2",    [](unsigned char deepID) { return ((deepID >> 5) & 0x01); }, {"deepTauVSjet"})
                 .Define("byVTightDeepVSjet_2",   [](unsigned char deepID) { return ((deepID >> 6) & 0x01); }, {"deepTauVSjet"})
                 .Define("byVVTightDeepVSjet_2",  [](unsigned char deepID) { return ((deepID >> 7) & 0x01); }, {"deepTauVSjet"})
                 // DeepTau2017v2p1 vs. mu
                 .Define("byVLooseDeepVSmu_2",    [](unsigned char deepID) { return ((deepID >> 0) & 0x01); }, {"deepTauVSmu"})
                 .Define("byLooseDeepVSmu_2",     [](unsigned char deepID) { return ((deepID >> 1) & 0x01); }, {"deepTauVSmu"})
                 .Define("byMediumDeepVSmu_2",    [](unsigned char deepID) { return ((deepID >> 2) & 0x01); }, {"deepTauVSmu"})
                 .Define("byTightDeepVSmu_2",     [](unsigned char deepID) { return ((deepID >> 3) & 0x01); }, {"deepTauVSmu"})
                 // DeepTau2017v2p1 vs. e
                 .Define("byVVVLooseDeepVSe_2",   [](unsigned char deepID) { return ((deepID >> 0) & 0x01); }, {"deepTauVSe"})
                 .Define("byVVLooseDeepVSe_2",    [](unsigned char deepID) { return ((deepID >> 1) & 0x01); }, {"deepTauVSe"})
                 .Define("byVLooseDeepVSe_2",     [](unsigned char deepID) { return ((deepID >> 2) & 0x01); }, {"deepTauVSe"})
                 .Define("byLooseDeepVSe_2",      [](unsigned char deepID) { return ((deepID >> 3) & 0x01); }, {"deepTauVSe"})
                 .Define("byMediumDeepVSe_2",     [](unsigned char deepID) { return ((deepID >> 4) & 0x01); }, {"deepTauVSe"})
                 .Define("byTightDeepVSe_2",      [](unsigned char deepID) { return ((deepID >> 5) & 0x01); }, {"deepTauVSe"})
                 .Define("byVTightDeepVSe_2",     [](unsigned char deepID) { return ((deepID >> 6) & 0x01); }, {"deepTauVSe"})
                 .Define("byVVTightDeepVSe_2",    [](unsigned char deepID) { return ((deepID >> 7) & 0x01); }, {"deepTauVSe"});

    // Defined in leadingTauTauGenInfo.h
    auto df3 = GetGenMatchInfo(df2, sConfig);

    return df3.Define("genPartFlav_1", [](std::vector<std::vector<unsigned int>> pairPairIdx) { return pairPairIdx[0][0]; }, {"gen_match_info"}) // gen part flav is the first vector
              .Define("genPartFlav_2", [](std::vector<std::vector<unsigned int>> pairPairIdx) { return pairPairIdx[0][1]; }, {"gen_match_info"})
              .Define("gen_match_1",   [](std::vector<std::vector<unsigned int>> pairPairIdx) { return pairPairIdx[1][0]; }, {"gen_match_info"})  // gen match is the second vector
              .Define("gen_match_2",   [](std::vector<std::vector<unsigned int>> pairPairIdx) { return pairPairIdx[1][1]; }, {"gen_match_info"});

}

/**********************************************************/

/*
 * Check if leading legs pass the eta requirements (note: for cross-triggers in the mutau channel there may be a stricter cut abs(eta)<2.1 on the tau.
 */
bool checkEtaRequirements(const int channel, float eta_1, float eta_2) {
    if (channel == Helper::mt) {
        return (abs(eta_1) < 2.1) && (abs(eta_2) < 2.3);
    }
    else if (channel == Helper::et) {
        return (abs(eta_1) < 2.1) && (abs(eta_2) < 2.3);
    }
    else if (channel == Helper::em) {
        return (abs(eta_1) < 2.4) && (abs(eta_2) < 2.4);
    }
    else {
        std::cout << "leadingTauTau.h checkOfflineEtaRequirements [Error:] Channel not recognized, returning false" << std::endl;
        return false;
    }
}

/**********************************************************/

/*
 * Skim-level isolation requirements.
 */
bool checkIsoRequirements(const int channel, float iso_1, float iso_2) {
    if ((channel == Helper::mt) || (channel == Helper::et)) {
        return (iso_1 < 0.15);
    }
    else if (channel == Helper::em) {
        return ((iso_1 < 0.1) && (iso_2 < 0.15));
    }
    else {
        return false;
    }
}

/**********************************************************/

/*
 * Skim-level discriminants against e/mu faking tauh, for mutauh and etauh channels. Arguments are vs. e VLOOSE and TIGHT, then vs. muon VLOOSE and TIGHT.
 */
bool checkDiscriminantRequirements(const int channel, int byVLooseDeepVSe_2, int byTightDeepVSe_2, int byVLooseDeepVSmu_2, int byTightDeepVSmu_2) {
    if (channel == Helper::mt) {
        return (byVLooseDeepVSe_2 && byTightDeepVSmu_2);
    }
    else if (channel == Helper::et) {
        return (byTightDeepVSe_2 && byVLooseDeepVSmu_2);
    }
    else {
        return true;
    }
}

/**********************************************************/

/*
 * Apply eta and isolation skim-level cuts.
 */
template <typename T>
auto FilterOfflineTauTauPropertiesByChannel(T &df) {
    return df.Filter(checkEtaRequirements, {"channel", "eta_1", "eta_2"}, "leadingTauTau.h: all channels: Loose skim-level eta requirements by channel (does not include stricter eta requirements from cross-trigger)")
             .Filter(checkIsoRequirements, {"channel", "iso_1", "iso_2"}, "leadingTauTau.h: mutauh and etauh only: Skim-level iso_1 < 0.15. emu: loose skim-level cuts: iso_1<0.3 and iso_2<0.3")
             .Filter(checkDiscriminantRequirements, {"channel", "byVLooseDeepVSe_2", "byTightDeepVSe_2", "byVLooseDeepVSmu_2", "byTightDeepVSmu_2"}, "leadingTauTau.h: mutauh and etauh only: Skim-level discriminants against e/mu faking tauh");

}

/**********************************************************/

/*
 * Once the leading legs are found, check that they have opposite sign (mutau and etau only). and pass offline eta requirements.
 */
template <typename T>
auto FilterOSByChannel(T &df) {

    return df.Filter([](int ch, int q_1, int q_2) {
        if ((ch == Helper::et) || (ch == Helper::mt)) { return (q_1 * q_2) < 0;  }
        else { return true; }
        },
        {"channel", "q_1", "q_2"}, "leadingTauTau.h: OS opposite-sign for mutau and etau only, no OS requirement at skim stage for emu channel");

}

/**********************************************************/

#endif
