// Header file for getting and handling the pile-up jet ID cuts, since CMSSW 10_2_X NanoAOD
// doesn't have flags for the pile-up jet ID 102X training.
//
// Source: https://github.com/cms-sw/cmssw/blob/CMSSW_10_2_X/RecoJets/JetProducers/python/PileupJetIDCutParams_cfi.py#L6-L31

#ifndef PILEUP_JET_ID_102X_TRAINING_H_INCL
#define PILEUP_JET_ID_102X_TRAINING_H_INCL

/**********************************************************/

/*
 * Helper function to return the jet pile-up ID discriminant (102X training, 2018) as a float,
 * where the discriminant is dependent on the jet's pT and eta.
 * "idString" must be "Loose", "Medium", or "Tight" (case-sensitive).
 *
 * Should only be applied to jets that already passed the pT/eta baseline cuts, and:
 * PU jet ID (such as this one) should only be applied to JEC corrected jets with pT<50 GeV.
 *
 * https://twiki.cern.ch/twiki/bin/view/CMS/PileupJetID#9X_2017_and_10X_2018_recipes
 */

float getPileup102Xwp(string idString, float jetPt, float jetEta) {

  float wp = -999;
  int idx_row = -1;
  int idx_col = -1;

  // Check that the jet pT is less than 50 GeV, and that the jet eta is
  // within bounds.
  assert(jetPt < 50.0);
  assert(abs(jetEta) < 5.0);
  assert((idString == "Tight") || (idString == "Medium") || (idString == "Loose"));

  // Idx_Row: 4 pt categories: 0-10 GeV, 10-20 GeV, 20-30 GeV, and 30-50 GeV
  // Idx_Column: 4 eta categories: 0-2.5, 2.5-2.75, 2.75-3.0, 3.0-5.0

  if (jetPt < 10)
    idx_row = 0;
  else if (jetPt < 20)
    idx_row = 1;
  else if (jetPt < 30)
    idx_row = 2;
  else if (jetPt <= 50)
    idx_row = 3;

  if (abs(jetEta) < 2.5)
    idx_col = 0;
  else if (abs(jetEta) < 2.75)
    idx_col = 1;
  else if (abs(jetEta) < 3.0)
    idx_col = 2;
  else if (abs(jetEta) < 5.0)
    idx_col = 3;

  assert(idx_row != -1);
  assert(idx_col != -1);

  // Declare working points: https://twiki.cern.ch/twiki/bin/view/CMS/PileupJetID#9X_2017_and_10X_2018_recipes
  float looseWP[4][4] = {{-0.97, -0.68, -0.53, -0.47},
			 {-0.97, -0.68, -0.53, -0.47},
			 {-0.97, -0.68, -0.53, -0.47},
			 {-0.89, -0.52, -0.38, -0.30}};
  float mediumWP[4][4] = {{0.18, -0.55, -0.42, -0.36},
			  {0.18, -0.55, -0.42, -0.36},
			  {0.18, -0.55, -0.42, -0.36},
			  {0.61, -0.35, -0.23, -0.17}};
  float tightWP[4][4] = {{0.69, -0.35, -0.26, -0.21},
			 {0.69, -0.35, -0.26, -0.21},
			 {0.69, -0.35, -0.26, -0.21},
			 {0.86, -0.10, -0.05, -0.01}};

  if (idString == "Loose") {
    wp = looseWP[idx_row][idx_col];
  }
  else if (idString == "Medium") {
    wp = mediumWP[idx_row][idx_col];
  }
  else if (idString == "Tight") {
    wp = tightWP[idx_row][idx_col];
  }

  return wp;
}

/**********************************************************/

/*
 * Helper function that returns a ROOT::RVec<int> indicating whether
 * the jets passing basic baseline cuts (Jet_passBaselineCuts,
 * df.Define'd in jets.h)
 * pass the 102X training jet pile-up ID. wpString should be
 * "Tight", "Medium", or "Loose" (case-sensitive).
 */

auto checkPassingJetPileupID_102X = [](string wpString,
				       ROOT::RVec<int>& Jet_passBaselineCuts,
				       ROOT::RVec<float>& Jet_puIdDisc,
				       ROOT::RVec<float>& Jet_pt,
				       ROOT::RVec<float>& Jet_eta) {
  using namespace ROOT::VecOps;

  unsigned int nJets = Jet_pt.size();  // equivalent to the nJet NanoAOD branch

  // Initialize the vector of booleans (length = number of jets)
  ROOT::RVec<int> passingJets(nJets, 0);

  // Check if each jet which passes the baseline cuts, also
  // passes the pile-up Loose ID with 102X training, based on
  // its pt and eta. If Jet pT > 50 GeV, automatically pass it.
  for (size_t i = 0; i < nJets; i++) {

    if (Jet_passBaselineCuts[i]) {
      if (Jet_pt[i] >= 50) {
	passingJets[i] = 1;
      }
      else {
	float wp = getPileup102Xwp(wpString, Jet_pt[i], Jet_eta[i]);
	passingJets[i] = (Jet_puIdDisc[i] > wp);
	//	std::cout << "Jet_pt, Jet_eta: " << Jet_pt[i] << ", " << Jet_eta[i] << ". Corresponding working point is: " << wp << ". Jet_puIdDisc: " << Jet_puIdDisc[i] << std::endl;
      }

    }

  }


  return passingJets;

};

/**********************************************************/

/*
 * Wrapper function for checkPassingJetPileupID_102X, specifying the Loose working point.
 */

auto checkPassingJetPileupID_102X_LooseWP = [](ROOT::RVec<int>& Jet_passBaselineCuts,
					       ROOT::RVec<float>& Jet_puIdDisc,
					       ROOT::RVec<float>& Jet_pt,
					       ROOT::RVec<float>& Jet_eta) {

  return checkPassingJetPileupID_102X("Loose", Jet_passBaselineCuts,
				      Jet_puIdDisc, Jet_pt, Jet_eta);

};

/**********************************************************/

#endif
