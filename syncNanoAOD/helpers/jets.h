
// Header file for jet functions.

#ifndef JETS_H_INCL
#define JETS_H_INCL

#include "computePhysicsVariables.h"
#include "eventLevelVariables.h"
#include "helperFunctions.h"

#include "pileupJetID_102Xtraining.h"

/*************************************************************************/

/*
 * Find all valid jets from the jet collection.
 */

template <typename T>
auto FindGoodJets(T &df, std::string configDir) {

  string jetSuffix = "";
  string configFile = "jetsBaseline.txt";

  // If NanoAODTools was applied, use the branches for the baseline cut
  if (HasExactBranch(df, "Jet_pt_nom") && HasExactBranch(df, "Jet_mass_nom")) {
    jetSuffix = "_nom";
    configFile = "jetsBaseline_corrected.txt";
  }
  std::cout << "   >>> jets.h: FindGoodJets baseline cut: Using Jet_pt" + jetSuffix + " and config " + configFile << std::endl;

  return df.Define("jets", getString(configDir + configFile));

}

/*************************************************************************/

/*
 * Find leading and sub-leading jets well-separated from muon and tau,
 * with the highest pT.
 */

template <typename T>
auto FindLeadingJetPair(T &df) {
    using namespace ROOT::VecOps;

    // Assuming that cuts have already been made on pT, abs(eta), jetID, puID
    // "jets" is a boolean vector indicating whether the jets in each event
    // have passed the baseline cuts
    auto find_leading_pair = [](unsigned int nJet,
				RVec<int>& jets, RVec<float>& Jet_pt,
				RVec<float>& Jet_eta, RVec<float>& Jet_phi,
				float eta_1,
				float eta_2,
				float phi_1,
				float phi_2){

      // Find valid jets based on delta r
      std::vector<int> validJets(nJet, 0);

      // Count number of good jets passing pT > 30 and deltaR separation
      int nGoodJets = 0;

      for (size_t i = 0; i < nJet; i++) {

	if (jets[i] == 1) {
	  const auto deltar1 = Helper::DeltaR(Jet_eta[i], eta_1,
					      Jet_phi[i], phi_1);
	  const auto deltar2 = Helper::DeltaR(Jet_eta[i], eta_2,
					      Jet_phi[i], phi_2);
	  if (deltar1 > 0.5 && deltar2 > 0.5) {
	    validJets[i] = 1;
	    if (Jet_pt[i] > 30) {
	      nGoodJets++;
	    }
	  }
	}
      }

      // Find two leading jets (in pT) that meet DeltaR requirement
      int idx_1 = -1;
      int idx_2 = -1;
      float bestpT = -1;
      float secondBestpT = -1;
      for (size_t i = 0; i < nJet; i++) {

	if (validJets[i] == 0) continue;

	if (Jet_pt[i] > bestpT) {
	  // Move the previous best pT to the second-best
	  secondBestpT = bestpT;
	  idx_2 = idx_1;
	  // Save the new best pT
	  bestpT = Jet_pt[i];
	  idx_1 = i;
	}
	else if (Jet_pt[i] > secondBestpT) {
	  secondBestpT = Jet_pt[i];
	  idx_2 = i;
	}
	/* std::cout << "Leading jet pTs are: " << bestpT << " with j_idx_1 " << idx_1  */
	/* 	  << " and " << secondBestpT << " with j_idx_2 " << idx_2 << std::endl; */
      }

      return std::vector<int>({idx_1, idx_2, nGoodJets});
    };

    std::string jetSuffix = "";
    if (HasExactBranch(df, "Jet_pt_nom") && HasExactBranch(df, "Jet_mass_nom"))
      jetSuffix = "_nom";
    std::cout << "   >>> jets.h: Using Jet_pt" + jetSuffix << std::endl;


    auto df2 = df.Define("jPairIdxInfo", find_leading_pair,
			 {"nJet", "jets", "Jet_pt" + jetSuffix,   // second argument was originally "jets"
			  "Jet_eta", "Jet_phi",
			  "eta_1", "eta_2", "phi_1", "phi_2"})
             .Define("j_idx_1", "jPairIdxInfo[0]")
             .Define("j_idx_2", "jPairIdxInfo[1]")
             .Define("njets",   "jPairIdxInfo[2]")
      //     .Filter("j_idx_1 != -1", "Valid leading b-tagged jet")
      //     .Filter("j_idx_2 != -1", "Valid sub-leading b-tagged jet")
             .Define("hasTwoLeadingJets", "j_idx_1 != -1 && j_idx_2 != -1")

      .Define("jpt_1", Helper::GetFloat, {"Jet_pt" + jetSuffix,   "j_idx_1"})
      .Define("jpt_2", Helper::GetFloat, {"Jet_pt" + jetSuffix,   "j_idx_2"})
      .Define("jm_1",  Helper::GetFloat, {"Jet_mass" + jetSuffix, "j_idx_1"})
      .Define("jm_2",  Helper::GetFloat, {"Jet_mass" + jetSuffix, "j_idx_2"})

      .Define("jeta_1", Helper::GetFloat, {"Jet_eta", "j_idx_1"})
      .Define("jphi_1", Helper::GetFloat, {"Jet_phi", "j_idx_1"})
      .Define("jeta_2", Helper::GetFloat, {"Jet_eta", "j_idx_2"})
      .Define("jphi_2", Helper::GetFloat, {"Jet_phi", "j_idx_2"})
      .Define("jp4_1", add_p4, {"jpt_1", "jeta_1", "jphi_1", "jm_1"})
      .Define("jp4_2", add_p4, {"jpt_2", "jeta_2", "jphi_2", "jm_2"})
      .Define("jp4", "jp4_1 + jp4_2")
      .Define("mjj", compute_mjj, {"jp4", "hasTwoLeadingJets"});

    return df2;

    // If MC
    /* if (sampleName_.find("Run") == std::string::npos) { */

    /*   std::cout << "   >>> genJets.h: Processing MC (any year): accessing gen info for " << sampleName_ << std::endl; */
    /*   auto df3 = df2.Define("jGenJetIdx_1", "Jet_genJetIdx[j_idx_1]") */
    /* 	.Define("jGenPt_1", "GenJet_pt[jGenJetIdx_1]") */
    /* 	.Define("jGenJetIdx_2", "Jet_genJetIdx[j_idx_2]") */
    /* 	.Define("jGenPt_2", "GenJet_pt[jGenJetIdx_2]"); */
    /*   return df3; */
    /* } */
    /* else { */

    /*   std::cout << "   >>> genJets.h: Processing data (any year):, set 'GenJet' variables to null for " << sampleName_ << std::endl; */
    /*   auto df3 = df2.Define("jGenJetIdx_1", [](){ return -1; }) */
    /* 	.Define("jGenPt_1", [](){ return -999.;} ) */
    /* 	.Define("jGenJetIdx_2", [](){ return -1; }) */
    /* 	.Define("jGenPt_2", [](){ return -999.;} ); */
    /*   return df3; */
    /* } */

}



/*************************************************************************/

#endif
