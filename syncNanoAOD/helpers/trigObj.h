// Helper functions for handling trigger objects.

#ifndef TRIG_OBJ_H
#define TRIG_OBJ_H

#include "helperFunctions.h"

/**********************************************************/

// Given a reco object with recoPhi and recoEta, of type
// objType (e.g. muon: 13, tau: 15), return the index into trigObj if the
// reco object is within deltaR of a valid trigger object,
// or -99 if not found.

using namespace ROOT::VecOps;
int getTrigObjIdx(int type, float recoEta, float recoPhi,
		  RVec<int> TrigObj_id,	RVec<float> TrigObj_eta, RVec<float> TrigObj_phi) {

  int idx = -99;
  for (int i = 0; i < (int) TrigObj_id.size(); i++) {
    idx = -95;  // inside loop
    if (TrigObj_id[i] == type) {
      idx = -90; // trig object found
      const auto deltar = Helper::DeltaR(recoEta, TrigObj_eta[i], recoPhi, TrigObj_phi[i]);
      if (deltar < 0.5) {
        idx = i;
        //	std::cout << "Matching trigObj found, index " << idx << std::endl;
        break;
      }
    }
  }

  return idx;
}

/**********************************************************/

// Baby helper function
int getTrigObjFilterBits(RVec<int>& TrigObj_filterBits, int idx) {

  if ((TrigObj_filterBits.size() > 0) && (idx >= 0)) {
    return TrigObj_filterBits[idx];
  }
  else {
    return 0;
  }
}


/**********************************************************/

// Get trigObj indexes and match variables for leading pair
// with TrigObj_id type1 and type2 (e.g. 13 for muon, 15 for tau)


template <typename T>
auto GetLeadingPairTrigObjInfo(T &df) {

 return df.Define("type1", [](int ch) { if (ch == Helper::mt) return Helper::pdgId_muon; else if (ch == Helper::et) return Helper::pdgId_electron; else if (ch == Helper::em) return Helper::pdgId_electron; else return 0; }, {"channel"})
          .Define("type2", [](int ch) { if (ch == Helper::mt) return Helper::pdgId_tau;  else if (ch == Helper::et) return Helper::pdgId_tau;      else if (ch == Helper::em) return Helper::pdgId_muon; else return 0; },     {"channel"})
          // do deltaR matching to trig objects
          .Define("trigObj_idx1", getTrigObjIdx, {"type1", "eta_1", "phi_1", "TrigObj_id", "TrigObj_eta", "TrigObj_phi"})
          .Define("trigObj_idx2", getTrigObjIdx, {"type2", "eta_2", "phi_2", "TrigObj_id", "TrigObj_eta", "TrigObj_phi"})
          // was object matched in deltaR?
          .Define("match1", [](int idx) { return (int) (idx >= 0); }, {"trigObj_idx1"})
          .Define("match2", [](int idx) { return (int) (idx >= 0); }, {"trigObj_idx2"})
          // get the filter bits for this object
          .Define("filterBits1",  getTrigObjFilterBits, {"TrigObj_filterBits", "trigObj_idx1"})  // should be 0 if not applicable
          .Define("filterBits2",  getTrigObjFilterBits, {"TrigObj_filterBits", "trigObj_idx2"});

}

/**********************************************************/

#endif
