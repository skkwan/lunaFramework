#include "computePhysicsVariables.h"

ROOT::Math::PtEtaPhiMVector add_p4(float pt, float eta, float phi, float mass) {
  return ROOT::Math::PtEtaPhiMVector(pt, eta, phi, mass);
}


float compute_mjj(ROOT::Math::PtEtaPhiMVector& p4, bool isValidPair) {
      if (isValidPair) return float(p4.M());
      return -9999.f;
}

float compute_ptjj(ROOT::Math::PtEtaPhiMVector& p4, ROOT::RVec<int>& g)
{
  if (Sum(g) >= 2) return float(p4.Pt());
  return -9999.f;
}

float compute_jdeta(float x, float y, ROOT::RVec<int>& g)
{
  if (Sum(g) >= 2) return x - y;
  return -9999.f;
}

float compute_mt(float pt_1, float phi_1, float pt_met, float phi_met)
{
  const auto dphi = Helper::DeltaPhi(phi_1, phi_met);
  return std::sqrt(2.0 * pt_1 * pt_met * (1.0 - std::cos(dphi)));
}


float compute_pt_scalar_sum(float pt_1, float pt_2)
{
  return (pt_1 + pt_2);
}

ROOT::Math::PtEtaPhiMVector get_p4(int index, ROOT::RVec<float>& Vec_pt, ROOT::RVec<float>& Vec_eta, ROOT::RVec<float>& Vec_phi, ROOT::RVec<float>& Vec_mass) {
    ROOT::Math::PtEtaPhiMVector p4;
    if (index < 0) {
        p4 = ROOT::Math::PtEtaPhiMVector(0, -99, -999, 0);
    }
    else {
        assert(index < ((int) Vec_pt.size()));
        p4 = ROOT::Math::PtEtaPhiMVector(Vec_pt[index], Vec_eta[index], Vec_phi[index], Vec_mass[index]);
    }

    return p4;
}

/**********************************************************/

/*
 * Clean objects_toClean from objects_ref using deltaR
 */
ROOT::RVec<int> clean_collection(ROOT::RVec<int>& objects_toClean, ROOT::RVec<float>& eta_1, ROOT::RVec<float>& phi_1,
			                           ROOT::RVec<int>& objects_ref, ROOT::RVec<float>& eta_2, ROOT::RVec<float>& phi_2)
{

  float minDeltaR = 0.4;

  // Initialize and default to true
  size_t nObjects_toClean = objects_toClean.size();
  size_t nObjects_ref = objects_ref.size();
  auto objects_isCleaned = objects_toClean;

  // For each object in the collection to clean (nObjects_toClean), compare it with the reference objects
  for (size_t i1 = 0; i1 < nObjects_toClean; i1++) {
    // Ignore if the object is already invalid
    if (objects_toClean[i1] == 0) continue;

    // If the object passed loose selections, check against each one of the ref objects
    for (size_t i2 = 0; i2 < nObjects_ref; i2++) {
      const auto deltaR = Helper::DeltaR(eta_1[i1], eta_2[i2], phi_1[i1], phi_2[i2]);
      // If a ref object is less than <0.4 from the object to check
      if (deltaR < minDeltaR) {
        // Make the object to check, invalid, and skip the rest of this inner loop
        objects_toClean[i1] = 0;
        continue;
      }
    }
  }

  return objects_isCleaned;
}


/**********************************************************/


/*
 * Select a two-leg pair (e.g. muon and tauh for the mutauh final state) from the collections of objects (denoted _1 and _2) passing the
 * very loose baseline selection. The selected pair represents the candidate for this event
 * for a Higgs boson decay to two tau leptons of which one decays to a hadronic
 * final state (most likely a combination of pions) and one decays to a muon and
 * a neutrino.
 */

using namespace ROOT::VecOps;
std::vector<int> build_pair(RVec<int>& goodObjects_1, RVec<float>& pt_1, RVec<float>& eta_1, RVec<float>& phi_1, RVec<float>& iso_1,
			                      RVec<int>& goodObjects_2, RVec<float>& pt_2, RVec<float>& eta_2, RVec<float>& phi_2, RVec<float>& iso_2,
                            float minDeltaR, bool reverseIndices)
{


  // Get indices of all possible combinations
  auto comb = Combinations(pt_1, pt_2);
  const auto numComb = comb[0].size();

  // Find valid pairs based on delta r
  std::vector<int> validPair(numComb, 0);
  for(size_t i = 0; i < numComb; i++) {
    const auto i1 = comb[0][i];
    const auto i2 = comb[1][i];
    if (goodObjects_1[i1] == 1 && goodObjects_2[i2] == 1) {
      const auto deltar = Helper::DeltaR(eta_1[i1], eta_2[i2], phi_1[i1], phi_2[i2]);
      if (deltar > minDeltaR) {
	      validPair[i] = 1;
      }
    }
  }

  // Find best leading pair based on pair selection algorithm: https://twiki.cern.ch/twiki/bin/viewauth/CMS/HiggsToTauTauWorkingLegacyRun2#Pair_Selection_Algorithm

  //////////////////////STEP 1 ////////////////////
  // "First prefer the pair with the most isolated candidate 1"
  int idx_1 = -1;
  int idx_2 = -1;
  float bestIso_1 = 9999999;
  // First find best isolation in the first leg
  for (size_t i = 0; i < numComb; i++) {
    if (validPair[i] == 1) {
      const int tmp1 = comb[0][i];
      const int tmp2 = comb[1][i];
      if (iso_1[tmp1] < bestIso_1) {
        bestIso_1 = iso_1[tmp1];
        idx_1 = tmp1;
        idx_2 = tmp2;
      }
    }
  }
  // Save the indices of the pairs where the isolation of the first leg is equal to the max
  std::vector<int> idxWithBestIso;
  for (size_t i = 0; i < numComb; i++) {
    if (validPair[i] == 1) {
      const int tmp1 = comb[0][i];
      if (iso_1[tmp1] == bestIso_1) {
        idxWithBestIso.push_back(i);
      }
    }
  }
  // If only one pair has the best isolation value, return the pair
  if (idxWithBestIso.size() == 1) {
    int bestIso = idxWithBestIso[0];
    return std::vector<int>({(int) comb[0][bestIso], (int) comb[1][bestIso]});
  }

  //////////////////////STEP 2 ////////////////////
  // Else, if the isolation of candidate 1 is the same in both pairs, prefer the pair with the highest candidate_1 pt
  // (for cases of genuinely the same isolation value but different possible candidate 1).
  float bestPt_1 = -99;
  idx_1 = -1;
  idx_2 = -1;
  for (size_t i = 0; i < idxWithBestIso.size(); i++) { //idxWithBestIso are the indices into Combination
    const int tmp1 = comb[0][i];
    const int tmp2 = comb[1][i];
    if (pt_1[tmp1] > bestPt_1) {
      bestPt_1 = pt_1[tmp1];
      idx_1 = tmp1;
      idx_2 = tmp2;
    }
  }
  // Save the indices of the pairs where the pT of the first leg is equal to the max
  std::vector<int> idxWithBestPt1;
  for (size_t i = 0; i < numComb; i++) {
    if (validPair[i] == 1) {
      const int tmp1 = comb[0][i];
      if (pt_1[tmp1] == bestPt_1) {
        idxWithBestPt1.push_back(i);
      }
    }
  }
  // If only one pair has the best leading pT, return the pair
  if (idxWithBestPt1.size() == 1) {
    int bestPt1 = idxWithBestPt1[0];
    return std::vector<int>({(int) comb[0][bestPt1], (int) comb[1][bestPt1]});
  }

  //////////////////////STEP 3 ////////////////////
  // If the pt of candidate 1 in both pairs is the same (likely because it's the same object) then prefer the pair with the most isolated candidate 2
  idx_1 = -1;
  idx_2 = -1;
  float bestIso_2 = 9999999;
  // First find best isolation in the second leg
  for (size_t i = 0; i < numComb; i++) {
    if (validPair[i] == 1) {
      const int tmp1 = comb[0][i];
      const int tmp2 = comb[1][i];
      if (iso_2[tmp2] < bestIso_2) {
        bestIso_2 = iso_2[tmp2];
        idx_1 = tmp1;
        idx_2 = tmp2;
      }
    }
  }
  // Save the indices of the pairs where the isolation of the second leg is equal to the max
  std::vector<int> idxWithBestIso2;
  for (size_t i = 0; i < numComb; i++) {
    if (validPair[i] == 1) {
      const int tmp2 = comb[1][i];
      if (iso_2[tmp2] == bestIso_2) {
        idxWithBestIso2.push_back(i);
      }
    }
  }
  // If only one pair has the best isolation value, return the pair
  if (idxWithBestIso2.size() == 1) {
    int bestIso = idxWithBestIso2[0];
    return std::vector<int>({(int) comb[0][bestIso], (int) comb[1][bestIso]});
  }

  //////////////////////STEP 4 ////////////////////
  // "If the isolation of candidate 2 is the same, prefer the pair with highest candidate 2 pt (for cases of genuinely the same isolation value but different possible candidate 2)."
  float bestPt_2 = -99;
  idx_1 = -1;
  idx_2 = -1;
  for (size_t i = 0; i < idxWithBestIso2.size(); i++) { //idxWithBestIso are the indices into Combination
    const int tmp1 = comb[0][i];
    const int tmp2 = comb[1][i];
    if (pt_2[tmp2] > bestPt_2) {
      bestPt_2 = pt_2[tmp2];
      idx_1 = tmp1;
      idx_2 = tmp2;
    }
  }
  // Save the indices of the pairs where the pT of the second leg is equal to the max
  std::vector<int> idxWithBestPt2;
  for (size_t i = 0; i < numComb; i++) {
    if (validPair[i] == 1) {
      const int tmp2 = comb[1][i];
      if (pt_2[tmp2] == bestPt_2) {
        idxWithBestPt2.push_back(i);
      }
    }
  }
  // If only one pair has the best leading pT, return the pair
  if (idxWithBestPt2.size() == 1) {
    int bestPt2 = idxWithBestPt2[0];
    return std::vector<int>({(int) comb[0][bestPt2], (int) comb[1][bestPt2]});
  }

  idx_1 = -1;
  idx_2 = -1;

  // Special to e+mu channel: select pair based on muon iso and pT first, but subsequently we treat the electron as leg 1 and the muon as leg 2.
  // i.e. Reverse the indices that we return.
  if (reverseIndices) {
    int idx_temp = idx_1;
    idx_1 = idx_2;
    idx_2 = idx_temp;
  }


  return std::vector<int>({idx_1, idx_2});
}


/*------------------------------------------------------------------*/

// Variable D_zeta, defined in Equation 3 in HIG-17-024.
// D_zeta is defined to be p_xi - 0.85 p_xi^{vis}
float compute_D_zeta(float pt_met, float phi_met,
		      float pt_1, float phi_1,
		      float pt_2, float phi_2)
{

  // First compute the bisector of the transverse momenta
  TVector3 u1(std::cos(phi_1), std::sin(phi_1), 0);
  TVector3 u2(std::cos(phi_2), std::sin(phi_2), 0);

  TVector3 uBisector = (u1 + u2).Unit();

  // p_xi is the component of \vec{p}_T^{miss} along the uBisector of the
  // transverse momenta of the two tau candidates
  TVector3 ptMiss(pt_met * std::cos(phi_met),
		  pt_met * std::sin(phi_met),
		  0);
  //  std::cout << "ptMiss: " << ptMiss.X() << " "<< ptMiss.Y() << " " <<  ptMiss.Z() << std::endl;
  float p_xi = ptMiss.Dot(uBisector);
  // std::cout << "p_xi: " << p_xi << std::endl;

  // Now, compute p_xi^{vis}
  TVector3 leptonPairTotalPt = (pt_1 * u1) + (pt_2 * u2);

  //  std::cout << "leptonPairTotalPt: " << leptonPairTotalPt.X()<< " "<< leptonPairTotalPt.Y() << " " <<  leptonPairTotalPt.Z() << std::endl;

  float p_xi_vis = leptonPairTotalPt.Dot(uBisector);

  // Compute and return D_zeta
  float D_zeta = p_xi - (0.85 * p_xi_vis);
  // std::cout << "D_zeta: " << D_zeta << std::endl;
  return D_zeta;
}

/*------------------------------------------------------------------*/

// For a given gen jet, find the associated neutrino (deltaR < 0.4) and return
// the total 4-vector (pT, eta, phi, mass)

ROOT::Math::PtEtaPhiMVector get_gen_jet_with_neutrinos(unsigned int slot,
						       int jIdx,
						       ROOT::RVec<float>& GenJet_pt,
						       ROOT::RVec<float>& GenJet_eta,
						       ROOT::RVec<float>& GenJet_phi,
						       ROOT::RVec<float>& GenJet_mass,
						       ROOT::RVec<int>& GenPart_status,
						       ROOT::RVec<int>& GenPart_pdgId,
						       ROOT::RVec<float>& GenPart_pt,
						       ROOT::RVec<float>& GenPart_eta,
						       ROOT::RVec<float>& GenPart_phi,
						       ROOT::RVec<float>& GenPart_mass,
						       unsigned long long event,
						       unsigned int lumi) {

  ROOT::Math::PtEtaPhiMVector genJet(GenJet_pt[jIdx],
				     GenJet_eta[jIdx],
				     GenJet_phi[jIdx],
				     GenJet_mass[jIdx]);
  // std::cout << "Lumi " << lumi << "Event " << event << ": Original genJet is " << genJet.Pt() << " for jIdx " << jIdx << " with GenPart_pdgId.size() " << GenPart_pdgId.size() << std::endl;

  (void) slot; // avoid bogus unused variable warning
  (void) event;   // avoid bogus unused variable warning
  (void) lumi;

  ROOT::Math::PtEtaPhiMVector genJetWN(genJet);

  // Loop through gen particles
  int nGenPart = GenPart_pdgId.size();
  for (int i = 0; i < nGenPart; i++) {
    if ((GenPart_status[i] == 1) &&
	((abs(GenPart_pdgId[i]) == 12) || (abs(GenPart_pdgId[i]) == 14) || (abs(GenPart_pdgId[i]) == 16))) {

      ROOT::Math::PtEtaPhiMVector neutrino(GenPart_pt[i],
					   GenPart_eta[i],
					   GenPart_phi[i],
					   GenPart_mass[i]);
      auto deltar = Helper::DeltaR(neutrino.Eta(), genJet.Eta(), neutrino.Phi(), genJet.Phi());
      if (deltar > 0.4) {
	continue;
      }
      genJetWN = genJetWN + neutrino;
      // std::cout << "Event " << event << ": Neutrino found for jet pT " << genJet.Pt() << " with pT " << neutrino.Pt()
      //		<< ", new pT is " << genJetWN.Pt() << std::endl;
    }


  }

  return genJetWN;

}
