#ifndef GEN_LEVEL_EMBED_H_INCL
#define GEN_LEVEL_EMBED_H_INCL

#include "computePhysicsVariables.h"
#include "fileIO.h"
#include "genHelper.h"
#include "helperFunctions.h"
#include "sampleConfig_class.h"

/***************************************************************************/

std::vector<int> find_gen_taus_from_Z(unsigned long long event,
                        unsigned int nGenPart,
                        RVec<int>&   GenPart_pdgId,
                        RVec<int>&   GenPart_genPartIdxMother) {

    (void) event; // in case I need it for debugging

    std::vector<int> output;

    // Loop through genParticle collection
    for (unsigned int i = 0; i < nGenPart; i++) {

        // Only do this for the first two gen taus fro the Z
        if (output.size() == 2) {
            continue;
        }
        // For all gen taus, Check if the taus came from a Z boson
        if (abs(GenPart_pdgId[i]) == GenHelper::pdgId_tau) {
            if (GenPart_pdgId[GenPart_genPartIdxMother[i]] == GenHelper::pdgId_Z) {
                output.push_back(i);
            }
        }
    }

    return output;

}
/***************************************************************************/

/*
 * Get gen-level muon (i.e. gen-level tau) pT and eta, only for Embedded samples.
 */

template <typename T>
auto GetEmbedGenLevelLegs(T &df, LUNA::sampleConfig_t &sConfig) {


  // Define function that we will use to get the gen top quarks pT in a vector (-10.0 if no top quark found)
  using namespace ROOT::VecOps;


    if (sConfig.isEmbedded()) {

        return df.Define("genIdx_of_taus_from_Z", find_gen_taus_from_Z, {"event", "nGenPart", "GenPart_pdgId", "GenPart_genPartIdxMother"})
                 .Define("genPt_embLeg1", [](std::vector<int>& idxVector, RVec<float>& GenPart_pt) { return GenPart_pt[idxVector[0]]; }, {"genIdx_of_taus_from_Z", "GenPart_pt"})
                 .Define("genPt_embLeg2", [](std::vector<int>& idxVector, RVec<float>& GenPart_pt) { return GenPart_pt[idxVector[1]]; }, {"genIdx_of_taus_from_Z", "GenPart_pt"})
                 .Define("genEta_embLeg1", [](std::vector<int>& idxVector, RVec<float>& GenPart_eta) { return GenPart_eta[idxVector[0]]; }, {"genIdx_of_taus_from_Z", "GenPart_eta"})
                 .Define("genEta_embLeg2", [](std::vector<int>& idxVector, RVec<float>& GenPart_eta) { return GenPart_eta[idxVector[1]]; }, {"genIdx_of_taus_from_Z", "GenPart_eta"});

    }
    else {
        return df;
    }


}

/***************************************************************************/


#endif
