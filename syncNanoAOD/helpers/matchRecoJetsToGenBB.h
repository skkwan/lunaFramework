#ifndef MATCH_RECO_JETS_TO_GEN_BB_H_INCL
#define MATCH_RECO_JETS_TO_GEN_BB_H_INCL

#include "genCascade_class.h"
#include "genParticle_class.h"
#include "genHelper.h"
#include "helperFunctions.h"

/***************************************************************************/

/*
 * Return the Jet_* index of the jet that is closest to the given gen jet's eta and phi.
 */

using namespace ROOT::VecOps;


int get_index_of_best_deltaR_match(bool isValid, unsigned int nJet,
                                    RVec<float>& Jet_eta, RVec<float>& Jet_phi,
                                    float genEta, float genPhi)
{
    // Initialize
    int iBest = -1;
    float bestDeltaR = 10000;

    // Loop over reco jets
    if (isValid) {
        for (unsigned int i = 0; i < nJet; ++i) {
            float thisDeltaR = Helper::DeltaR(Jet_eta[i], genEta, Jet_phi[i], genPhi);
            if (thisDeltaR < bestDeltaR) {
                bestDeltaR = thisDeltaR;
                iBest = i;
            }
        }
    }
    return iBest;
}

/***************************************************************************/

/*
 * Match reco jets to the gen bb system.
 */

template <typename T>
auto MatchRecoJetsToGenBB(T &df, std::string sampleName) {

    bool isCascade = (Helper::containsSubstring(sampleName, "Cascade") && !(Helper::containsSubstring(sampleName, "NonCascade")));

    if (isCascade) {
//        auto df = df.Filter("evt==107880", "Filter on one event evt==107880 for testing");
        auto df2 = df.Define("has_one_gen_bb_pair", "n_gen_bb_pairs > 0")
                     .Define("has_second_gen_bb_pair", "n_gen_bb_pairs > 1")
                     .Define("idx_ak4_matched_to_gen_bb", get_index_of_best_deltaR_match, {"has_one_gen_bb_pair", "nJet", "Jet_eta", "Jet_phi", "genEta_bb", "genPhi_bb"})
                     .Define("idx_ak8_matched_to_gen_bb", get_index_of_best_deltaR_match, {"has_one_gen_bb_pair", "nFatJet", "FatJet_eta", "FatJet_phi", "genEta_bb", "genPhi_bb"})
                     .Define("idx_ak4_matched_to_second_gen_bb", get_index_of_best_deltaR_match, {"has_second_gen_bb_pair", "nJet", "Jet_eta", "Jet_phi", "genEta_bb", "genPhi_bb"})
                     .Define("idx_ak8_matched_to_second_gen_bb", get_index_of_best_deltaR_match, {"has_second_gen_bb_pair", "nFatJet", "FatJet_eta", "FatJet_phi", "genEta_bb", "genPhi_bb"});

    // Info for first bb pair
        auto df3 = df2.Define("jetP4_ak4_matched", get_p4, {"idx_ak4_matched_to_gen_bb", "Jet_pt", "Jet_eta", "Jet_phi", "Jet_mass"})
                      .Define("jetEta_ak4_matched", "(float) jetP4_ak4_matched.Eta()")
                      .Define("jetPhi_ak4_matched", "(float) jetP4_ak4_matched.Phi()")
                      .Define("jetPt_ak4_matched",  "(float) jetP4_ak4_matched.Pt()")
                      .Define("jetM_ak4_matched",   "(float) jetP4_ak4_matched.M()")

                      // AK8 jets
                      .Define("jetP4_ak8_matched", get_p4, {"idx_ak8_matched_to_gen_bb", "FatJet_pt", "FatJet_eta", "FatJet_phi", "FatJet_mass"})
                      .Define("jetEta_ak8_matched", "(float) jetP4_ak8_matched.Eta()")
                      .Define("jetPhi_ak8_matched", "(float) jetP4_ak8_matched.Phi()")
                      .Define("jetPt_ak8_matched",  "(float) jetP4_ak8_matched.Pt()")
                      .Define("jetM_ak8_matched",   "(float) jetP4_ak8_matched.M()")

                      // pT resolution
                      .Define("pt_resolution_ak4", "(jetPt_ak4_matched - genPt_bb)/genPt_bb")
                      .Define("pt_resolution_ak8", "(jetPt_ak8_matched - genPt_bb)/genPt_bb")

                      // DeltaR
                      .Define("deltaR_ak4", Helper::compute_deltaR, {"genEta_bb", "jetEta_ak4_matched", "genPhi_bb", "jetPhi_ak4_matched"})
                      .Define("deltaR_ak8", Helper::compute_deltaR, {"genEta_bb", "jetEta_ak8_matched", "genPhi_bb", "jetPhi_ak8_matched"});

    // Info for second bb pair (if available)
        auto df4 = df3.Define("jetP4_ak4_matched2", get_p4, {"idx_ak4_matched_to_second_gen_bb", "Jet_pt", "Jet_eta", "Jet_phi", "Jet_mass"})
                      .Define("jetEta_ak4_matched2", "(float) jetP4_ak4_matched2.Eta()")
                      .Define("jetPhi_ak4_matched2", "(float) jetP4_ak4_matched2.Phi()")
                      .Define("jetPt_ak4_matched2",  "(float) jetP4_ak4_matched2.Pt()")
                      .Define("jetM_ak4_matched2",   "(float) jetP4_ak4_matched2.M()")

                      // AK8 jets
                      .Define("jetP4_ak8_matched2", get_p4, {"idx_ak8_matched_to_second_gen_bb", "FatJet_pt", "FatJet_eta", "FatJet_phi", "FatJet_mass"})
                      .Define("jetEta_ak8_matched2", "(float) jetP4_ak8_matched2.Eta()")
                      .Define("jetPhi_ak8_matched2", "(float) jetP4_ak8_matched2.Phi()")
                      .Define("jetPt_ak8_matched2",  "(float) jetP4_ak8_matched2.Pt()")
                      .Define("jetM_ak8_matched2",   "(float) jetP4_ak8_matched2.M()")

                      // pT resolution
                      .Define("pt_resolution_ak4_2", "(jetPt_ak4_matched2 - genPt_bb)/genPt_bb")
                      .Define("pt_resolution_ak8_2", "(jetPt_ak8_matched2 - genPt_bb)/genPt_bb")

                      // DeltaR
                      .Define("deltaR_ak4_2", Helper::compute_deltaR, {"genEta_bb", "jetEta_ak4_matched2", "genPhi_bb", "jetPhi_ak4_matched2"})
                      .Define("deltaR_ak8_2", Helper::compute_deltaR, {"genEta_bb", "jetEta_ak8_matched2", "genPhi_bb", "jetPhi_ak8_matched2"});


        return df4;
    }
    else {
        return df;
    }
}

/***************************************************************************/


#endif
