// Helper functions for file I/O (config file I/O).

#ifndef FILE_IO_H_INCL
#define FILE_IO_H_INCL

#include <iostream>
#include <fstream>
#include <string>

#include "ROOT/RDataFrame.hxx"
#include "ROOT/RResultPtr.hxx"

#include "TChain.h"
#include "TChainElement.h"
#include "TH1.h"
#include "TF1.h"

#include "sampleConfig_class.h"

using namespace std;

/******************************************************************************/

/*
 * Return the contents of inFile (text file) as a string.
 */

std::string getString(string inFile) {

  // std::cout << "Inside getString for " << inFile << "..." << std::endl;
  std::string s;

  std::ifstream f(inFile);
  std::stringstream buffer;
  buffer << f.rdbuf();

  s = buffer.str();

  if (s != "") {
    std::cout << "  >>> (" << inFile << "):" << s << std::endl;
  }
  else {
    std::cout << "  >>> getString [ERROR:] file is empty!!" << std::endl;
  }

  return s;
}

/******************************************************************************/

/*
 * Helper function for applying a vector of Defines to a dataframe (if df is a DataFrame it will be automatically
 * cast to a RNode).
 */

auto ApplyDefines(ROOT::RDF::RNode df, const std::vector<std::string> &names, const std::vector<std::string> &exprs,
                  unsigned int i = 0)
{
  if (i == names.size())
    return df;

  return ApplyDefines(df.Define(names[i], exprs[i]), names, exprs, i + 1);
}

/******************************************************************************/

/*
 * Check if the current TChain contains any of the files listed in specialFiles.
 */
bool containsProblematicFiles(TChain *ch, LUNA::sampleConfig_t &sConfig) {

  bool containsProblemFile = false;

  // Only check this for data ETau 2018
  if (sConfig.isData() && sConfig.isETauOnly() && (sConfig.year() == 2018)) {

    std::vector<std::string> specialFiles = {"/store/data/Run2018B/EGamma/NANOAOD/02Apr2020-v1/40000/19F02981-E639-274D-ADF9-069A4C7ECC48.root",
                                              "/store/data/Run2018B/EGamma/NANOAOD/02Apr2020-v1/240000/3878DDCD-721D-F740-B24C-534C8098B143.root",
                                              "/store/data/Run2018B/EGamma/NANOAOD/02Apr2020-v1/20000/39BBAE9D-B748-8B4F-B6E8-BC4F66A5AEBC.root"};

    for (std::string specialFile : specialFiles) {
      TObjArray* listOfFiles = ch->GetListOfFiles();
      TIter next(listOfFiles);
      TChainElement *chEl = 0;
      while ((chEl = (TChainElement*) next() )) {

        std::string chTitle = chEl->GetTitle();
        std::cout << chTitle << std::endl;

        if (chTitle.find(specialFile) != std::string::npos) {
            std::cout << "problem found!" << std::endl;
            containsProblemFile = true;
            return containsProblemFile;
        }
      }
    }
  }

  return containsProblemFile;
}

/******************************************************************************/


#endif
