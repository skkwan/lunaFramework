// Helper functions for the mu-tauh final state.
#ifndef GEN_LEVEL_MUTAU_H_INCL
#define GEN_LEVEL_MUTAU_H_INCL

#include "computePhysicsVariables.h"
#include "fileIO.h"
#include "genHelper.h"
#include "helperFunctions.h"
#include "muon.h"
#include "tau.h"

/***************************************************************************/

namespace GenHelperMuTau {

  using namespace ROOT::VecOps;

  /***************************************************************************/

  // Returns 1 if a muon (pdgId MUST agree with above-defined const value) originated
  // from a tau decay, from a pseudoscalar. PDG IDs are defined above in the
  // GenHelperMuTau namespace. Returns 0 otherwise.
  // RVec names are the same as their NanoAOD counterparts, muon_genPartIdx is the
  // muon's index in the GenPart NanoAOD vector.

  int IsMuonFromAToTauTau(RVec<int>& GenPart_genPartIdxMother,
			  RVec<int>& GenPart_pdgId, RVec<int>& GenPart_status,
			  const int muon_genPartIdx) {

    // Sanity check: do not exceed bounds of list of gen particles
    assert(muon_genPartIdx <= (int) GenPart_pdgId.size());

    // Sanity check: input is a gen-level muon
    assert(abs(GenPart_pdgId[muon_genPartIdx]) == GenHelper::pdgId_mu);

    // We only need to keep track of one variable: genPart idx of the current
    // particle.
    // Initialize:
    int idxGenPart = muon_genPartIdx;
    bool muonIsFromTau = 0;
    bool muonIsFromA = 0;

    // While the current particle is not one of the initial particles...
    while (GenPart_status[idxGenPart] > 0) {

      // Current particle's PDG ID
      int pdgId_i = GenPart_pdgId[idxGenPart];

      // See if it originated from a tau; set status flag if yes
      if (abs(pdgId_i) == GenHelper::pdgId_tau) {
	muonIsFromTau = true;
      }

      // See if it originated from psuedoscalar; set status flag if yes
      if (abs(pdgId_i) == GenHelper::pdgId_a) {
	muonIsFromA = true;
      }

      // If both flags are set, return 1 (successful)
      if (muonIsFromTau && muonIsFromA) {
	//	std::cout << "Found muon from tau from psuedoscalar" << std::endl;
	return 1;
      }

      // Move up by one particle
      idxGenPart = GenPart_genPartIdxMother[idxGenPart];


    }

    return 0;

  }

  /***************************************************************************/

  // Looks for a gen-level hadronic tau that decayed from a pseudoscalar (pdg id
  // defined above in this namespace). Returns the index (within GenVisTau) of
  // the tau found (more specifically, the first one found), and returns -1 if
  // none found.

  int FindGenVisTauIdxFromA(RVec<int>& GenVisTau_genPartIdxMother,
			    RVec<int>& GenPart_genPartIdxMother,
			    RVec<int>& GenPart_pdgId, RVec<int>& GenPart_status,
			    const int tau_genVisTauIdx) {

    // Go to the tauh's parent:
    int idxGenPart = GenVisTau_genPartIdxMother[tau_genVisTauIdx];

    // While the current particle is not one of the initial particles... and
    // while there is still a mother, and while the idxGenPart is within bounds
    while ((idxGenPart > 0) &&
	   (GenPart_status[idxGenPart] > 0) &&
	   (GenPart_genPartIdxMother[idxGenPart] > 0) &&
	   (idxGenPart <= (int) GenPart_pdgId.size())) {

      // Current particle's PDG ID
      int pdgId_i = GenPart_pdgId[idxGenPart];

      //      std::cout << "pdgId: " << GenPart_pdgId[idxGenPart] << ", GenPart_status[idxGenPart]: " << GenPart_status[idxGenPart]<< std::endl;

      // See if it originated from psuedoscalar; return if yes
      if (abs(pdgId_i) == GenHelper::pdgId_a) {
	//std::cout << "Found gen vis tau from pseudoscalar" << std::endl;
      	return 1;
      }

      idxGenPart = GenPart_genPartIdxMother[idxGenPart];
    } // end of while loop

    //    std::cout << "Pseudoscalar parent not found" <<std::endl;
    return 0;

  } // end of function

} // end of namespace

/**********************************************************/
/*
 * Select a muon-tau pair from the collections of muons and taus passing the
 * initial selection. The selected pair represents the candidate for this event
 * for a Higgs boson decay to two tau leptons of which one decays to a hadronic
 * final state (most likely a combination of pions) and one decays to a muon and
 * a neutrino.
 */
template <typename T>
auto FindGenMuonTauPair(T &df) {
  using namespace ROOT::VecOps;
  auto build_pair = [](RVec<int>& GenPart_genPartIdxMother,
		       RVec<int>& GenPart_pdgId, RVec<int>& GenPart_status,
		       unsigned int nGenVisTau,
		       RVec<int>& GenVisTau_genPartIdxMother,
		       unsigned int luminosityBlock, unsigned long long event)
    {
      // Index into GenPart
      int idx_1 = -1;
      int idx_2 = -1;

      // h -> aa-> bbtautau -> mutauh
      // Loop through genPart collection
      int nGenPart = GenPart_pdgId.size();

      for (int i = 0; i < nGenPart; i++) {

       	// First get the muon
      	if (abs(GenPart_pdgId[i]) == GenHelper::pdgId_mu) {
      	  if (GenHelperMuTau::IsMuonFromAToTauTau(GenPart_genPartIdxMother, GenPart_pdgId, GenPart_status,
      					     i)) {
      	    idx_1 = i;
      	  }
      	}
      } // end of loop over gen particles

      // Next get the hadronic vis tau
      unsigned int nGenVisTaus = GenVisTau_genPartIdxMother.size();
      assert(nGenVisTaus == nGenVisTau);

      //      std::cout << " ------ " << luminosityBlock << " ------ " << event << " ------------ " <<std::endl;
      for (unsigned int i = 0; i < nGenVisTaus; i++) {
      	if (GenHelperMuTau::FindGenVisTauIdxFromA(GenVisTau_genPartIdxMother, GenPart_genPartIdxMother,
      	 				     GenPart_pdgId, GenPart_status, i)) {
      	  idx_2 = i;
      	}
      } // end of loop over hadronic vis taus

      //      std::cout << "idx_1, 2 are:" << idx_1 << ", " << idx_2 << std::endl;

      return std::vector<int>({idx_1, idx_2});
    };

  return df.Define("pairIdx", build_pair,
		   {"GenPart_genPartIdxMother", "GenPart_pdgId", "GenPart_status",
		       "nGenVisTau", "GenVisTau_genPartIdxMother",
		       "luminosityBlock", "event"})
    .Define("idx_1", "pairIdx[0]")
    .Define("idx_2", "pairIdx[1]")
    .Filter("idx_1 > -1 && idx_2 > -1", "Mutauh pair from pseudoscalar found")

    // Kinematics and position information
    .Define("pt_1",  "GenPart_pt[idx_1]")
    .Define("eta_1", "GenPart_eta[idx_1]")
    .Define("phi_1", "GenPart_phi[idx_1]")

    /* .Define("pt_2",  "GenPart_pt[idx_1]") */
    /* .Define("eta_2", "GenPart_eta[idx_1]") */
    /* .Define("phi_2", "GenPart_phi[idx_1]"); */

    .Define("pt_2", "GenVisTau_pt[idx_2]")
    .Define("eta_2", "GenVisTau_eta[idx_2]")
    .Define("phi_2", "GenVisTau_phi[idx_2]");


    /* .Define("q_2", "Tau_charge[idx_2]") */
    /* .Define("m_2", "Tau_mass[idx_2]") */

    /* .Define("decayMode_2", "Tau_decayMode[idx_2]") */

    /* // See helpers/computePhysicsVariables.h for definitions */
    /* .Define("pt_scalar_sum", compute_pt_scalar_sum, {"pt_1", "pt_2"}) */

    /* .Define("p4_1", add_p4, {"pt_1", "eta_1", "phi_1", "m_1"})   */
    /* .Define("p4_2", add_p4, {"pt_2", "eta_2", "phi_2", "m_2"}) */
    /* .Define("p4", "p4_1 + p4_2") */
    /* .Define("m_vis", "float(p4.M())") */
    /* .Define("pt_vis", "float(p4.Pt())"); */


}



#endif
