
// Helper functions for electrons.

#ifndef ELECTRON_H_INCL
#define ELECTRON_H_INCL

/******************************************************************************/

/*
 * Map Electron_genPartFlav to the naming conventions for Tau_genPartFlav.
 * See https://twiki.cern.ch/twiki/bin/viewauth/CMS/HiggsToTauTauWorking2016#MC_Matching.
 */

unsigned int convertElectronGenPartFlavToTauGenPartFlav(unsigned char Electron_genPartFlav)
{
  unsigned int newFlav;

  // Flavour of genParticle (DressedLeptons for electrons) for MC matching to status==1 electrons or photons:
  // 1 = prompt electron (including gamma*->mu mu),
  // 15 = electron from prompt tau,
  // 22 = prompt photon (likely conversion),
  // 5 = electron from b, 4 = electron from c, 3 = electron from light or unknown, 0 = unmatched

  if (Electron_genPartFlav == 1)   // prompt electron (including gamma*->mumu)
    newFlav = 1;                   // prompt electron
  else if (Electron_genPartFlav == 15)  // electron from prompt tau
    newFlav = 3;                        // tau->e
  else            // 22 = prompt photon (likely conversion), {5, 4, 3} electron from {b, c, light or unknown}, 0 = unmatched
    newFlav = 0;  // unknown or unmatched

  return newFlav;
}


/******************************************************************************/


#endif
