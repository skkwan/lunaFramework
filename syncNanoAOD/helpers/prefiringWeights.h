/*
 * Helper file for getting prefiring weight branches from NanoAOD.
 */

#ifndef PREFIRING_WEIGHTS_H
#define PREFIRING_WEIGHTS_H

#include "sampleConfig_class.h"

/*************************************************************************/

/*
 * Get prefiring weight branches.
 */
template <typename T>
auto GetPrefiringWeights(T &df, LUNA::sampleConfig_t &sConfig) {

    if (sConfig.year() == 2018) {
        return df.Define("weight_l1prefiring_nominal", []() { return 1.0f; })
                 .Define("weight_l1prefiring_up",      []() { return 1.0f; })
                 .Define("weight_l1prefiring_down",    []() { return 1.0f; });
    }
    else {
        return df.Define("weight_l1prefiring_nominal", [](float f) { return f; }, {"L1PreFiringWeight_Nom"})
                 .Define("weight_l1prefiring_up",      [](float f) { return f; }, {"L1PreFiringWeight_Up"})
                 .Define("weight_l1prefiring_down",    [](float f) { return f; }, {"L1PreFiringWeight_Dn"});
    }
}

/*************************************************************************/

#endif
