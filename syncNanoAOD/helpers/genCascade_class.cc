#include "genCascade_class.h"

namespace GenHelper {

    genCascade_t::genCascade_t() {
        n_gen_bb_pair = -1;
    }


    // Constructor from a vector of provided indices
    genCascade_t::genCascade_t(std::vector<int> genPart_idx_legs,
                      ROOT::VecOps::RVec<int>& GenPart_pdgId,
                      ROOT::VecOps::RVec<int>& GenPart_genPartIdxMother,
                      ROOT::VecOps::RVec<int>& GenPart_statusFlags,
                      ROOT::VecOps::RVec<float>& GenPart_pt,
                      ROOT::VecOps::RVec<float>& GenPart_eta,
                      ROOT::VecOps::RVec<float>& GenPart_phi,
                      ROOT::VecOps::RVec<float>& GenPart_mass
                      ) {
                        int genPartIdx_a1_direct = GenHelper::get_direct_genPartIdx(GenPart_genPartIdxMother, GenPart_pdgId, GenHelper::pdgId_a, GenHelper::pdgId_a1);

                        // a2 will give rise to two a1
                        std::vector<int> cascade_genPartIdx = GenHelper::get_cascade_genPartIdx(GenPart_genPartIdxMother, GenPart_pdgId, GenHelper::pdgId_a2, GenHelper::pdgId_a1);

                        int genPartIdx_a1_cascade1, genPartIdx_a1_cascade2;

                        if (cascade_genPartIdx.size() == 2) {
                            genPartIdx_a1_cascade1 = cascade_genPartIdx[0];
                            genPartIdx_a1_cascade2 = cascade_genPartIdx[1];
                        }
                        else {
                            std::cout << "genCascade_class.cc: [ERROR] Constructor: unexpected cascade_genPartIdx size of " << cascade_genPartIdx.size() << std::endl;
                            genPartIdx_a1_cascade1 = -1;
                            genPartIdx_a1_cascade2 = -1;
                        }

                        // genParticle_t's from the three pseudoscalars
                        std::vector<genParticle_t> v_from_a1_direct;
                        std::vector<genParticle_t> v_from_a1_cascade1;
                        std::vector<genParticle_t> v_from_a1_cascade2;


                        // Now loop through the legs
                        for (int genIdx : genPart_idx_legs) {

                             bool isFromHardProcess = GenHelper::fromHardProcess(GenPart_statusFlags[genIdx]);

                            // Only consider taus and bottom quarks
                            if (isFromHardProcess && ( ((abs(GenPart_pdgId[genIdx]) ==  GenHelper::pdgId_tau) || (abs(GenPart_pdgId[genIdx]) == GenHelper::pdgId_b)) ) ){
                                genParticle_t thisParticle = genParticle_t(GenPart_pdgId[genIdx], ROOT::Math::PtEtaPhiMVector(GenPart_pt[genIdx], GenPart_eta[genIdx], GenPart_phi[genIdx], GenPart_mass[genIdx]));

                                // If the gen particle came directly from the a1_direct,
                                if (GenPart_genPartIdxMother[genIdx] == genPartIdx_a1_direct) {
                                    v_from_a1_direct.push_back(thisParticle);
                                }
                                else if (GenPart_genPartIdxMother[genIdx] == genPartIdx_a1_cascade1) {
                                    v_from_a1_cascade1.push_back(thisParticle);
                                }
                                else if (GenPart_genPartIdxMother[genIdx] == genPartIdx_a1_cascade2) {
                                    v_from_a1_cascade2.push_back(thisParticle);
                                }
                            }
                        }
                        assert(v_from_a1_direct.size() == 2);
                        assert(v_from_a1_cascade1.size() == 2);
                        assert(v_from_a1_cascade2.size() == 2);

                        // PDG ID of these should tell us whether they were from bb or tautau
                        int a1_direct_is_bb = (v_from_a1_direct[0].pdgId() == GenHelper::pdgId_b);
                        int a1_cascade1_is_bb = (v_from_a1_cascade1[0].pdgId() == GenHelper::pdgId_b);
                        int a1_cascade2_is_bb = (v_from_a1_cascade2[0].pdgId() == GenHelper::pdgId_b);

                        // Build up vv_legs_bb and vv_legs_tautau
                        if (a1_direct_is_bb)   { vv_legs_bb.push_back(v_from_a1_direct); }   else { vv_legs_tautau.push_back(v_from_a1_direct); }
                        if (a1_cascade1_is_bb) { vv_legs_bb.push_back(v_from_a1_cascade1); } else { vv_legs_tautau.push_back(v_from_a1_cascade1); }
                        if (a1_cascade2_is_bb) { vv_legs_bb.push_back(v_from_a1_cascade2); } else { vv_legs_tautau.push_back(v_from_a1_cascade2); }

                        n_gen_bb_pair = vv_legs_bb.size();
                    }

    // Directly return the legs
    ROOT::Math::PtEtaPhiMVector genCascade_t::genP4_tau1() {
        if (vv_legs_tautau.size() > 0) {
            if (vv_legs_tautau[0].size() == 2) {
                return vv_legs_tautau[0][0].genP4();
            }
        }
        return ROOT::Math::PtEtaPhiMVector(-999, -999, -999, -999);
    }

    ROOT::Math::PtEtaPhiMVector genCascade_t::genP4_tau2() {
        if (vv_legs_tautau.size() > 0) {
            if (vv_legs_tautau[0].size() == 2) {
                return vv_legs_tautau[0][1].genP4();
            }
        }
        return ROOT::Math::PtEtaPhiMVector(-999, -999, -999, -999);
    }

    ROOT::Math::PtEtaPhiMVector genCascade_t::genP4_b1() {
        if (vv_legs_bb.size() > 0) {
            if (vv_legs_bb[0].size() == 2) {
                return vv_legs_bb[0][0].genP4();
            }
        }
        return ROOT::Math::PtEtaPhiMVector(-999, -999, -999, -999);
    }


    ROOT::Math::PtEtaPhiMVector genCascade_t::genP4_b2() {
        if (vv_legs_bb.size() > 0) {
            if (vv_legs_bb[0].size() == 2) {
                return vv_legs_bb[0][1].genP4();
            }
        }
        return ROOT::Math::PtEtaPhiMVector(-999, -999, -999, -999);
    }


    ROOT::Math::PtEtaPhiMVector genCascade_t::genP4_trailing1() {
        if (vv_legs_bb.size() > 1) {
            if (vv_legs_bb[1].size() == 2) {
                return vv_legs_bb[1][0].genP4();
            }
        }
        if (vv_legs_tautau.size() > 1) {
            if (vv_legs_tautau[1].size() == 2) {
                return vv_legs_tautau[1][0].genP4();
            }
        }
        return ROOT::Math::PtEtaPhiMVector(-999, -999, -999, -999);
    }

    ROOT::Math::PtEtaPhiMVector genCascade_t::genP4_trailing2() {
        if (vv_legs_bb.size() > 1) {
            if (vv_legs_bb[1].size() == 2) {
                return vv_legs_bb[1][1].genP4();
            }
        }
        if (vv_legs_tautau.size() > 1) {
            if (vv_legs_tautau[1].size() == 2) {
                return vv_legs_tautau[1][1].genP4();
            }
        }
        return ROOT::Math::PtEtaPhiMVector(-999, -999, -999, -999);
    }

}
