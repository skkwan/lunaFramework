// Helper functions for gen-level information.

#ifndef GEN_HELPER_H_INCL
#define GEN_HELPER_H_INCL

#include "computePhysicsVariables.h"


/***************************************************************************/

namespace GenHelper {

  // From PDG
  const int pdgId_b = 5;
  const int pdgId_top = 6;
  const int pdgId_ele = 11;
  const int pdgId_mu  = 13;
  const int pdgId_tau = 15;

  const int pdgId_nu_ele = 12;
  const int pdgId_nu_mu  = 14;
  const int pdgId_nu_tau = 16;

  const int pdgId_Z = 23;
  const int pdgId_W = 24;
  const int pdgId_H = 25;

  // For symmetric case h->a1a1 and asymmetric case h->a1a2, pdgId_a = 36 is the "h"
  const int pdgId_a = 36;

  const int pdgId_a1 = 25;
  const int pdgId_a2 = 35; // h->a2a1 cascade where a2->a1a1


  /***************************************************************************/
  // Functions to check statuses
  // https://cms-nanoaod-integration.web.cern.ch/integration/master-102X/mc102X_doc.html#GenPart
  /***************************************************************************/

  bool fromHardProcess(int statusFlag);
  bool fromHardProcessFinalState(int status, int statusFlag);
  bool isDirectHardProcessTauDecayProduct(int statusFlag);
  bool isLastCopy(int statusFlag);
  bool isElectron(int pdgId);
  bool isMuon(int pdgId);
  bool isNeutrino(int pdgId);

  /***************************************************************************/
  // Functions to check parents
  /***************************************************************************/

  /*
   * Returns whether a gen-level particle (whose index to GenPart is "this_genPartIdx")
   * came from the decay of a desired parent particle (not necessarily a direct decay product,
   * can be later in the chain).
   */
  bool IsFromParent(ROOT::RVec<int>& GenPart_genPartIdxMother,
		    ROOT::RVec<int>& GenPart_pdgId,
		    const int targetParent_pdgId, // e.g. GenHelper::pdgId_a
		    const int this_genPartIdx);

  /**************************************/

  /*
   * Checks whether a gen-level particle came from directly from a Z boson (for DY samples).
   */
  bool IsDirectDecayProductOfParent(ROOT::RVec<int>& GenPart_genPartIdxMother,
				    ROOT::RVec<int>& GenPart_pdgId,
				    const int targetDirectParent_pdgId, // e.g. GenHelper::pdgId_a
				    const int this_genPartIdx);

  /**************************************/

  /*
   * Specific to cascade case: get the genPartIdx of the pseudoscalar that came from directly from big H in H->a1a2.
   */
  int get_direct_genPartIdx(ROOT::VecOps::RVec<int>& GenPart_genPartIdxMother, ROOT::VecOps::RVec<int>& GenPart_pdgId,
                                          const int targetDirectParent_pdgId,
                                          const int targetDaughter_pdgId);

  /**************************************/

  /*
   * Specific to cascade case: get the genPartIdx's of the two pseudoscalars that came from the pseudoscalar decay
   * Usage: (the two with PDG ID 25 that came from PDG ID 35).
   */

  std::vector<int> get_cascade_genPartIdx(ROOT::VecOps::RVec<int>& GenPart_genPartIdxMother, ROOT::VecOps::RVec<int>& GenPart_pdgId,
                                          const int targetDirectParent_pdgId,
                                          const int targetDaughter_pdgId);


}

/**********************************************************/

#endif
