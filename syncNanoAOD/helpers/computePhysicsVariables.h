#ifndef COMPUTE_PHYSICS_VARIABLES_H
#define COMPUTE_PHYSICS_VARIABLES_H

#include "helperFunctions.h"

#include <cmath>
#include <ROOT/RVec.hxx>
#include <TVector3.h>
#include <Math/Vector4D.h>

// Add two 4-vectors
ROOT::Math::PtEtaPhiMVector add_p4(float pt, float eta, float phi, float mass);

// Compute m_{jj} from a list of 4-vectors
float compute_mjj_fromlist(ROOT::Math::PtEtaPhiMVector& p4, ROOT::RVec<int>& g);

// Compute m_{jj} if pair is valid
float compute_mjj(ROOT::Math::PtEtaPhiMVector& p4, bool isValidPair);

float compute_ptjj(ROOT::Math::PtEtaPhiMVector& p4, ROOT::RVec<int>& g);

float compute_jdeta(float x, float y, ROOT::RVec<int>& g);

float compute_mt(float pt_1, float phi_1, float pt_met, float phi_met);

float compute_pt_scalar_sum(float pt_1, float pt_2);

// Get a 4-vector; if index = -1 then return a (0, -99, -99, 0) vector.
ROOT::Math::PtEtaPhiMVector get_p4(int index, ROOT::RVec<float>& Vec_pt, ROOT::RVec<float>& Vec_eta, ROOT::RVec<float>& Vec_phi, ROOT::RVec<float>& Vec_mass);

std::vector<int> build_pair(ROOT::RVec<int>& goodObjects_1, ROOT::RVec<float>& pt_1, ROOT::RVec<float>& eta_1, ROOT::RVec<float>& phi_1, ROOT::RVec<float>& iso_1,
			                ROOT::RVec<int>& goodObjects_2, ROOT::RVec<float>& pt_2, ROOT::RVec<float>& eta_2, ROOT::RVec<float>& phi_2, ROOT::RVec<float>& iso_2,
							float minDeltaR, bool reverseIndices = false);


ROOT::RVec<int> clean_collection(ROOT::RVec<int>& goodObjects_toClean, ROOT::RVec<float>& eta_1, ROOT::RVec<float>& phi_1,
			                	 ROOT::RVec<int>& goodObjects_ref, ROOT::RVec<float>& eta_2, ROOT::RVec<float>& phi_2);

/*------------------------------------------------------------------*/

// Variable D_zeta, defined in Equation 3 in HIG-17-024.
// D_zeta is defined to be p_xi - 0.85 p_xi^{vis}
float compute_D_zeta(float pt_met, float phi_met,
					float pt_1, float phi_1,
					float pt_2, float phi_2);

/*------------------------------------------------------------------*/

// For a given gen jet, find the associated neutrino (deltaR < 0.4) and return
// the total 4-vector (pT, eta, phi, mass)

ROOT::Math::PtEtaPhiMVector get_gen_jet_with_neutrinos(unsigned int slot,
						       int jIdx,
						       ROOT::RVec<float>& GenJet_pt,
						       ROOT::RVec<float>& GenJet_eta,
						       ROOT::RVec<float>& GenJet_phi,
						       ROOT::RVec<float>& GenJet_mass,
						       ROOT::RVec<int>& GenPart_status,
						       ROOT::RVec<int>& GenPart_pdgId,
						       ROOT::RVec<float>& GenPart_pt,
						       ROOT::RVec<float>& GenPart_eta,
						       ROOT::RVec<float>& GenPart_phi,
						       ROOT::RVec<float>& GenPart_mass,
						       unsigned long long event,
						       unsigned int lumi);

/*------------------------------------------------------------------*/

#endif
