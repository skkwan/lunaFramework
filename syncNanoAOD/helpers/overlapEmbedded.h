#ifndef OVERLAP_EMBEDDED_H_INCL
#define OVERLAP_EMBEDDED_H_INCL

#include "helperFunctions.h"

/**********************************************************/

/*
 * When selecting two muons for embedded events, we also selected around 2% of events from processes other than Z decays. Some of these, such as QCD or
 * Z->tautau->mumu, are restricted to a low mass range and are negligible in the kinematic regime defined by the cuts applied in the generator step.
 * Contamination from other processes, ttbar and diboson, spread across the whole mass range. These events should be vetoed. Veto all events in which both
 * top quarks decay into genuine tau leptons. These are contained in the embedded event sample.
 */

bool takeEventFromThisSample(bool isEmbedOrDataOrSpecial, bool isMC_nonHiggs_nonDY, int channel, unsigned int gen_match_1, unsigned int gen_match_2) {

    // We Filter on the return value. If Embed or Data, do not remove overlap with Embedded. If this is a special sample (non Higgs, non DY) which we need
    // to keep all events to estimate the non-DY contamination to Embedded, also keep all events.
    if (isEmbedOrDataOrSpecial || isMC_nonHiggs_nonDY) return true;

    bool takeEventFromMC = true;

    // For each channel, take the event from Embedded if both legs came from a genuine tau, otherwise take it from MC
    if (channel == Helper::mt) {
        takeEventFromMC = !((gen_match_1 == 4) && (gen_match_2 == 5));
    }
    else if (channel == Helper::et) {
        takeEventFromMC = !((gen_match_1 == 3) && (gen_match_2 == 5));
    }
    else if (channel == Helper::em) {
        takeEventFromMC = !((gen_match_1 == 3) && (gen_match_2 == 4));
    }

    return takeEventFromMC;
}

/**********************************************************/


template <typename T>
auto FilterMCOverlapWithEmbedded(T &df, LUNA::sampleConfig_t &sConfig) {

    // Only check the overlap, if the sample is not embedded, not data, not signal, and not SM Higgs
    bool isEmbedOrDataOrSpecial = (sConfig.isEmbedded() || sConfig.isData() || sConfig.isSignal() || (sConfig.name().find("HTo") != std::string::npos));
    // If this is a sample for which to consider overlap with embedded contamination, do not filter
    bool isMC_nonHiggs_nonDY = sConfig.isMCnonHiggs() && !(sConfig.isDY());

    std::cout << "[overlapEmbedded.h] isEmbedOrDataOrSpecial: " << isEmbedOrDataOrSpecial << ", isMC_nonHiggs_nonDY: " << isMC_nonHiggs_nonDY << std::endl;

    return df.Define("isEmbedOrDataOrSpecial", [isEmbedOrDataOrSpecial]() { return isEmbedOrDataOrSpecial; })
             .Define("isMC_nonHiggs_nonDY", [isMC_nonHiggs_nonDY]() { return isMC_nonHiggs_nonDY; })
             .Filter(takeEventFromThisSample, {"isEmbedOrDataOrSpecial", "isMC_nonHiggs_nonDY", "channel", "gen_match_1", "gen_match_2"}, "[overlapEmbedded.h:] Remove overlap from Embed (reject genuine tautau), only for non-SM-Higgs and non-DY MC.");

}

/**********************************************************/


#endif
