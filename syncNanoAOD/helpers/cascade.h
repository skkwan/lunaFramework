#ifndef CASCADE_H_INCL
#define CASCADE_H_INCL

#include "genCascade_class.h"
#include "genParticle_class.h"
#include "genHelper.h"
#include "helperFunctions.h"

/***************************************************************************/

/*
 * Get cascade information for h3 -> h1 h2 and h2 -> h1 h1 scenario
 */

template <typename T>
auto FindCascadeLegs(T &df, std::string cascadePtrBranchName = "genCascade_t_ptr") {

    using namespace ROOT::VecOps;

    auto get_cascade = [](RVec<int>& GenPart_pdgId,
                          RVec<int>& GenPart_genPartIdxMother,
                          RVec<int>& GenPart_status,
                          RVec<int>& GenPart_statusFlags,
                          RVec<float>& GenPart_pt,
                          RVec<float>& GenPart_eta,
                          RVec<float>& GenPart_phi,
                          RVec<float>& GenPart_mass) {

                            std::vector<int> legsFromDirect;  // expect this to have length == 2 at the end
                            std::vector<int> legsFromCascade; // expect this to have length == 4 at the end
                            std::vector<int> legs;

                            // Loop through genPart collection and get the indices of the legs
                            int nGenPart = GenPart_pdgId.size();
                            for (int i = 0; i < nGenPart; i++) {

                                // Only consider taus and bottom quarks
                                if ((abs(GenPart_pdgId[i]) ==  GenHelper::pdgId_tau) || (abs(GenPart_pdgId[i]) == GenHelper::pdgId_b)) {

                                    // Check if it is from a hard process: use statusFlags not status
                                    bool isFromHardProcess = GenHelper::fromHardProcess(GenPart_statusFlags[i]);

                                    // Did these legs come from the original a3 particle
                                    bool isFromA3 = GenHelper::IsFromParent(GenPart_genPartIdxMother, GenPart_pdgId, GenHelper::pdgId_a, i);

                                    // Is it a direct daughter of an a1 particle? All six legs must come directly from an a1 (PDG ID 25)
                                    bool isDirectlyFromA1 = GenHelper::IsDirectDecayProductOfParent(GenPart_genPartIdxMother, GenPart_pdgId,
                                                                                                    GenHelper::pdgId_a1, i);

                                    // Next, is the parent of the a1, the a2 particle?
                                    bool isFromA2 = GenHelper::IsFromParent(GenPart_genPartIdxMother, GenPart_pdgId, GenHelper::pdgId_a2, i);

                                    // With this, we have enough information: if the particle had an a2 as a parent, it came from the cascade part of the decay
                                    if (isFromHardProcess && isFromA3 && isDirectlyFromA1) {
                                        if (isFromA2) {
                                            legsFromCascade.push_back(i);
                                        }
                                        else {
                                            legsFromDirect.push_back(i);
                                        }
                                    }
                                }
                            }
                            // Now fill the genCascade_t object with our information
                            if ((legsFromDirect.size() != 2) || (legsFromCascade.size() != 4)) {
                                std::cout << "genCascade.h: [ERROR:] Could not find the right number of legs! Returning a null GenHelper::genCascade_t object" << std::endl;
                                return GenHelper::genCascade_t();
                            }

                            // By arbitrary convention, declare legs in this order
                            for (size_t i = 0; i < legsFromDirect.size(); i++) {
                                legs.push_back(legsFromDirect[i]);
                            }

                            for (size_t i = 0; i < legsFromCascade.size(); i++){
                                legs.push_back(legsFromCascade[i]);
                            }

                            GenHelper::genCascade_t cascade = GenHelper::genCascade_t(legs, GenPart_pdgId, GenPart_genPartIdxMother, GenPart_statusFlags,
                                                GenPart_pt, GenPart_eta, GenPart_phi, GenPart_mass);

                            return cascade;
                        };


    return df.Define(cascadePtrBranchName, get_cascade, {"GenPart_pdgId", "GenPart_genPartIdxMother", "GenPart_status", "GenPart_statusFlags",
                                                         "GenPart_pt", "GenPart_eta", "GenPart_phi", "GenPart_mass"})
              .Define("n_gen_bb_pairs",  [](GenHelper::genCascade_t& c) { return c.n_gen_bb_pairs(); },  {cascadePtrBranchName});
}

/***************************************************************************/


/*
 * Define the branches for the leading tautau and leading bb and sub-leading objects
 */

template <typename T>
auto DefineCascadeBranches(T &df, std::string cascadePtrBranchName = "genCascade_t_ptr") {

    return df.Define("genP4_tau1", [](GenHelper::genCascade_t& c) { return c.genP4_tau1(); }, {cascadePtrBranchName})
            .Define("genP4_tau2", [](GenHelper::genCascade_t& c) { return c.genP4_tau2(); }, {cascadePtrBranchName})

            .Define("genP4_b1",   [](GenHelper::genCascade_t& c) { return c.genP4_b1(); }, {cascadePtrBranchName})
            .Define("genP4_b2",   [](GenHelper::genCascade_t& c) { return c.genP4_b2(); }, {cascadePtrBranchName})

            .Define("genP4_trailing1", [](GenHelper::genCascade_t& c) { return c.genP4_trailing1(); }, {cascadePtrBranchName})
            .Define("genP4_trailing2", [](GenHelper::genCascade_t& c) { return c.genP4_trailing2(); }, {cascadePtrBranchName})

            // Get the total gen P4 of the b quarks
            .Define("genP4_bb", "genP4_b1 + genP4_b2")

            // Unpack the 4-vectors
            // pt of legs
            .Define("genPt_tau1", "(float) genP4_tau1.Pt()")
            .Define("genPt_tau2", "(float) genP4_tau2.Pt()")

            .Define("genPt_b1", "(float) genP4_b1.Pt()")
            .Define("genPt_b2", "(float) genP4_b2.Pt()")

            .Define("genPt_trailing1", "(float) genP4_trailing1.Pt()")
            .Define("genPt_trailing2", "(float) genP4_trailing2.Pt()")

            // eta of legs
            .Define("genEta_tau1", "(float) genP4_tau1.Eta()")
            .Define("genEta_tau2", "(float) genP4_tau2.Eta()")

            .Define("genEta_b1", "(float) genP4_b1.Eta()")
            .Define("genEta_b2", "(float) genP4_b2.Eta()")

            .Define("genEta_trailing1", "(float) genP4_trailing1.Eta()")
            .Define("genEta_trailing2", "(float) genP4_trailing2.Eta()")

            // phi of legs
            .Define("genPhi_tau1", "(float) genP4_tau1.Phi()")
            .Define("genPhi_tau2", "(float) genP4_tau2.Phi()")

            .Define("genPhi_b1", "(float) genP4_b1.Phi()")
            .Define("genPhi_b2", "(float) genP4_b2.Phi()")

            .Define("genPhi_trailing1", "(float) genP4_trailing1.Phi()")
            .Define("genPhi_trailing2", "(float) genP4_trailing2.Phi()")

            // Unpack total gen P4 of the b quarks
            .Define("genPt_bb",  "(float) genP4_bb.Pt()")
            .Define("genEta_bb", "(float) genP4_bb.Eta()")
            .Define("genPhi_bb", "(float) genP4_bb.Phi()")
            .Define("genM_bb",   "(float) genP4_bb.M()");


}

/***************************************************************************/

/*
 *
 */

template <typename T>
auto GetCascadeInformation(T &df, std::string sampleName) {

    bool isCascade = (Helper::containsSubstring(sampleName, "Cascade") && !(Helper::containsSubstring(sampleName, "NonCascade")));

    if (isCascade) {
        auto df2 = FindCascadeLegs(df, "genCascade_t_ptr");

        // Reject events with all six legs being b quarks
        auto df3 = df2.Filter("n_gen_bb_pairs < 3",
                              "genCascade.h: Reject events with all six bottom quarks");
       auto df4 = DefineCascadeBranches(df3, "genCascade_t_ptr");

       return df4;
    }
    else {
        return df.Filter("true", "genCascade.h: Non-cascade sample: no filter on cascade product PDG IDs");
    }


}

/***************************************************************************/


#endif
