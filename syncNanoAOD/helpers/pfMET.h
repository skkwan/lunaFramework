// Helper functions for ParticleFlow (PF) MET.

#ifndef PFMET_H_INCL
#define PFMET_H_INCL

#include "computePhysicsVariables.h"
#include "eventLevelVariables.h"
#include "sampleConfig_class.h"

/**********************************************************/

/*
 * Get the nominal MET and metphi before tau energy scale corrections.
 */

template <typename T>
auto GetPFMETBeforeLeptonEnergyScales(T &df) {

  auto df1 = df.Define("metcov00", "(float) MET_covXX")
               .Define("metcov01", "(float) MET_covXY")
               .Define("metcov10", "(float) MET_covXY")
               .Define("metcov11", "(float) MET_covYY");

  // In order of preference: (1) use the JEC-recorrected and JER-corrected MET and MET phi
  //                         (2) use the JEC-corrected default MET and MET phi from NanoAOD
  if (HasExactBranch(df, "MET_T1Smear_pt") && HasExactBranch(df, "MET_T1Smear_phi")) {
    std::cout << "   >>> pfMET.h: Using JEC and JER-corrected MET and MET phi" << std::endl;
    // If doing a recoil sample, we will use Redefine "met" later after lepton energy scales
    return df1.Define("met",    "(float) MET_T1Smear_pt")
              .Define("metphi", "(float) MET_T1Smear_phi");
  }
  else {
    std::cout << "   >>> pfMET.h: Using default NanoAOD MET and MET phi values (no JER, and JEC not re-run), and no recoil corrections: define met and metphi" << std::endl;
    return df1.Define("met",    "(float) MET_pt")
              .Define("metphi", "(float) MET_phi");
  }
}


/**********************************************************/

/*
 * Get MET JET-related systematics (year-independent ones) if available.
 */

template <typename T>
auto GetMETJetSystematics(T &df, LUNA::sampleConfig_t &sConfig) {

  std::string year = std::to_string(sConfig.year());

  // We'll assume that if MET_T1Smear_pt_jerUp exists in the input n-tuple, the other
  // systematics are in it.

  sConfig.hasJERsysInputs = HasExactBranch(df, "MET_T1Smear_pt_jerUp");
  if (sConfig.hasJERsysInputs) {

    // If there are recoil corrections, metRecoilCorrections.h will .Redefine the following branches
    return df.Define("met_JERUp",                 "(float) MET_T1Smear_pt_jerUp")
              .Define("met_JERDown",               "(float) MET_T1Smear_pt_jerDown")
              .Define("met_JetAbsoluteUp",         "(float) MET_T1Smear_pt_jesAbsoluteUp")
              .Define("met_JetAbsoluteDown",       "(float) MET_T1Smear_pt_jesAbsoluteDown")
              .Define("met_JetBBEC1Up",            "(float) MET_T1Smear_pt_jesBBEC1Up")
              .Define("met_JetBBEC1Down",          "(float) MET_T1Smear_pt_jesBBEC1Down")
              .Define("met_JetEC2Up",              "(float) MET_T1Smear_pt_jesEC2Up")
              .Define("met_JetEC2Down",            "(float) MET_T1Smear_pt_jesEC2Down")
              .Define("met_JetFlavorQCDUp",        "(float) MET_T1Smear_pt_jesFlavorQCDUp")
              .Define("met_JetFlavorQCDDown",      "(float) MET_T1Smear_pt_jesFlavorQCDDown")
              .Define("met_JetHFUp",               "(float) MET_T1Smear_pt_jesHFUp")
              .Define("met_JetHFDown",             "(float) MET_T1Smear_pt_jesHFDown")
              .Define("met_JetRelativeBalUp",      "(float) MET_T1Smear_pt_jesRelativeBalUp")
              .Define("met_JetRelativeBalDown",    "(float) MET_T1Smear_pt_jesRelativeBalDown")
                .Define("metphi_JERUp",                 "(float) MET_T1Smear_phi_jerUp")
                .Define("metphi_JERDown",               "(float) MET_T1Smear_phi_jerDown")
                .Define("metphi_JetAbsoluteUp",         "(float) MET_T1Smear_phi_jesAbsoluteUp")
                .Define("metphi_JetAbsoluteDown",       "(float) MET_T1Smear_phi_jesAbsoluteDown")
                .Define("metphi_JetBBEC1Up",            "(float) MET_T1Smear_phi_jesBBEC1Up")
                .Define("metphi_JetBBEC1Down",          "(float) MET_T1Smear_phi_jesBBEC1Down")
                .Define("metphi_JetEC2Up",              "(float) MET_T1Smear_phi_jesEC2Up")
                .Define("metphi_JetEC2Down",            "(float) MET_T1Smear_phi_jesEC2Down")
                .Define("metphi_JetFlavorQCDUp",        "(float) MET_T1Smear_phi_jesFlavorQCDUp")
                .Define("metphi_JetFlavorQCDDown",      "(float) MET_T1Smear_phi_jesFlavorQCDDown")
                .Define("metphi_JetHFUp",               "(float) MET_T1Smear_phi_jesHFUp")
                .Define("metphi_JetHFDown",             "(float) MET_T1Smear_phi_jesHFDown")
                .Define("metphi_JetRelativeBalUp",      "(float) MET_T1Smear_phi_jesRelativeBalUp")
                .Define("metphi_JetRelativeBalDown",    "(float) MET_T1Smear_phi_jesRelativeBalDown")
             .Define("met_JetAbsoluteyearUp",     "(float) MET_T1Smear_pt_jesAbsolute_" + year + "Up")
             .Define("met_JetAbsoluteyearDown",   "(float) MET_T1Smear_pt_jesAbsolute_" + year + "Down")
             .Define("met_JetBBEC1yearUp",        "(float) MET_T1Smear_pt_jesBBEC1_" + year + "Up")
             .Define("met_JetBBEC1yearDown",      "(float) MET_T1Smear_pt_jesBBEC1_" + year + "Down")
             .Define("met_JetEC2yearUp",          "(float) MET_T1Smear_pt_jesEC2_" + year + "Up")
             .Define("met_JetEC2yearDown",        "(float) MET_T1Smear_pt_jesEC2_" + year + "Down")
             .Define("met_JetHFyearUp",           "(float) MET_T1Smear_pt_jesHF_" + year + "Up")
             .Define("met_JetHFyearDown",         "(float) MET_T1Smear_pt_jesHF_" + year + "Down")
             .Define("met_JetRelativeSampleUp",   "(float) MET_T1Smear_pt_jesRelativeSample_" + year + "Up")
             .Define("met_JetRelativeSampleDown", "(float) MET_T1Smear_pt_jesRelativeSample_" + year + "Down")
             .Define("metphi_JetAbsoluteyearUp",     "(float) MET_T1Smear_phi_jesAbsolute_" + year + "Up")
             .Define("metphi_JetAbsoluteyearDown",   "(float) MET_T1Smear_phi_jesAbsolute_" + year + "Down")
             .Define("metphi_JetBBEC1yearUp",        "(float) MET_T1Smear_phi_jesBBEC1_" + year + "Up")
             .Define("metphi_JetBBEC1yearDown",      "(float) MET_T1Smear_phi_jesBBEC1_" + year + "Down")
             .Define("metphi_JetEC2yearUp",          "(float) MET_T1Smear_phi_jesEC2_" + year + "Up")
             .Define("metphi_JetEC2yearDown",        "(float) MET_T1Smear_phi_jesEC2_" + year + "Down")
             .Define("metphi_JetHFyearUp",           "(float) MET_T1Smear_phi_jesHF_" + year + "Up")
             .Define("metphi_JetHFyearDown",         "(float) MET_T1Smear_phi_jesHF_" + year + "Down")
             .Define("metphi_JetRelativeSampleUp",   "(float) MET_T1Smear_phi_jesRelativeSample_" + year + "Up")
             .Define("metphi_JetRelativeSampleDown", "(float) MET_T1Smear_phi_jesRelativeSample_" + year + "Down");
  }
  else {
    return df;
  }
}


/**********************************************************/

/*
 * Get MET UES uncertainties which were computed in NanoAODTools.
 * The MET recoil uncertainties are computed in a different header file.
 */
template <typename T>
auto GetMETUnclusteredEnergyUncertainties(T &df, LUNA::sampleConfig_t &sConfig){

  sConfig.hasMetUESInputs = HasExactBranch(df, "MET_T1Smear_pt_unclustEnUp");

  if (sConfig.hasMetUESInputs) {
    return df.Define("met_UESUp",   "(float) MET_T1Smear_pt_unclustEnUp")
             .Define("met_UESDown", "(float) MET_T1Smear_pt_unclustEnDown")

             .Define("metphi_UESUp",   "(float) MET_T1Smear_phi_unclustEnUp")
             .Define("metphi_UESDown", "(float) MET_T1Smear_phi_unclustEnDown");
  }
  else {
    return df;
  }
}

/**********************************************************/

/*
 * Wrapper function for PF MET quantities.
 */

template <typename T>
auto GetPFMETVariables(T &df, LUNA::sampleConfig_t &sConfig) {

  auto df1 = GetPFMETBeforeLeptonEnergyScales(df);
  auto df2 = GetMETJetSystematics(df1, sConfig);
  auto df3 = GetMETUnclusteredEnergyUncertainties(df2, sConfig);
  auto dfF = df3;

  return dfF;

}

/**********************************************************/
#endif
