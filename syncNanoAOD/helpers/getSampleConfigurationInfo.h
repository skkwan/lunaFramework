// Helper function to define booleans identifying the sample
// (isData, isMC, etc.) based on the input sample name.


#ifndef GET_SAMPLE_CONFIGURATION_INFO_H_INCL
#define GET_SAMPLE_CONFIGURATION_INFO_H_INCL

#include "eventLevelVariables.h"
#include "helperFunctions.h"
#include "sampleConfig_class.h"

/**********************************************************/

/*
 * Store year (as float) in branch "year" depending on
 * configDir_.
 */

std::string WhichEra(std::string configDir) {

  if (configDir.find("2018") != std::string::npos) {
    return "2018";
  }
  else if (configDir.find("2017") != std::string::npos) {
    return "2017";
  }
  else if (configDir.find("2016preVFP") != std::string::npos) {
    return "2016preVFP";
  }
  else if (configDir.find("2016postVFP") != std::string::npos) {
    return "2016postVFP";
  }
  else {
    std::cout << "WARNING: WhichEra: year could not be determined, defaulting to 2018" << std::endl;
    return "2018";
  }
}

/**********************************************************/

/*
 * Set boolean branches isData, isMC, and isEmbedded.
 */

template <typename T>
auto IsDataEmbeddedOrMC(T &df, LUNA::sampleConfig_t &sConfig) {

  if (sConfig.isData()) {
    // MC
    //    std::cout << "   >>> whichSample.h: " << sConfig.name() << " isMC: true " << std::endl;

    auto df2 = df.Define("isMC",       [](){ return (int) false;  })
                 .Define("isData",     [](){ return (int) true; })
                 .Define("isEmbedded", [](){ return (int) false; });

    return df2;
  }
  else if (sConfig.isEmbedded()) {
    // Embedded
    // std::cout << "   >>> whichSample.h: " << sConfig.name() << " isEmbedded: true " << std::endl;

    auto df2 = df.Define("isMC",       [](){ return (int) false; })
                 .Define("isData",     [](){ return (int) false; })
                 .Define("isEmbedded", [](){ return (int) true;  });
    return df2;
  }
  else  {
    // MC
    //    std::cout << "   >>> whichSample.h: " << sConfig.name() << " isData: true" << std::endl;

    auto df2 = df.Define("isMC",       [](){ return (int) true; })
                 .Define("isData",     [](){ return (int) true;  })
                 .Define("isEmbedded", [](){ return (int) false; });
    return df2;
  }

}

/**********************************************************/


template <typename T>
auto IsChannelSpecific(T &df, LUNA::sampleConfig_t &sConfig) {

  if (sConfig.isMuTauOnly()) {
    return df.Define("isMuTauOnly", []() { return (int) true; })
             .Define("isETauOnly",  []() { return (int) false; })
             .Define("isEMuOnly",   []() { return (int) false; });
  }
  else if (sConfig.isETauOnly()) {
    return df.Define("isMuTauOnly", []() { return (int) false; })
             .Define("isETauOnly",  []() { return (int) true; })
             .Define("isEMuOnly",   []() { return (int) false; });
  }
  else if (sConfig.isEMuOnly()) {
    return df.Define("isMuTauOnly", []() { return (int) false; })
             .Define("isETauOnly",  []() { return (int) false; })
             .Define("isEMuOnly",   []() { return (int) true; });
  }
  else {
    return df.Define("isMuTauOnly", []() { return (int) false; })
             .Define("isETauOnly",  []() { return (int) false; })
             .Define("isEMuOnly",   []() { return (int) false; });
  }
}

/**********************************************************/

// Combine the above functions into one.

template <typename T>
auto GetSampleConfigurationInfo(T &df, LUNA::sampleConfig_t &sConfig) {

  int year = sConfig.year();

  auto df2 = IsDataEmbeddedOrMC(df, sConfig);
  auto df3 = IsChannelSpecific(df2, sConfig);
  auto df4 = df3.Define("year", [year]() { return year; });

  return df4;

}

/**********************************************************/

#endif
