// Helper functions for HLT trigger paths

#ifndef HLT_PATHS_H_INCL
#define HLT_PATHS_H_INCL

#include "computePhysicsVariables.h"
#include "fileIO.h"
#include "helperFunctions.h"


/**********************************************************/

template <typename T>
auto FilterDataMCByHLTPaths(T &df, LUNA::sampleConfig_t &sConfig) {

  // If data or embed, need to handle overlap
  if (sConfig.isData() || sConfig.isEmbedded()) {
    // Data/embed: Preferentially do mutau first
    if (sConfig.isMuTauOnly()) {
      return df.Filter("is_mt_trigger", "hltPaths.h: MuTau data: filter only those passing mutau triggers");
    }
    // Data/embed: Next do etau, but remove any overlap with mutau
    else if (sConfig.isETauOnly()) {
      return df.Filter("is_et_trigger", "hltPaths.h: ETau data: filter only those passing etau triggers");
    }
    // Data/embed: Lastly, do emu, but remove any overlap with mutau and etau
    else if (sConfig.isEMuOnly())  {
      return df.Filter("is_em_trigger", "hltPaths.h: EMu data: filter only those passing emu triggers");
    }
  }
  // MC: default behaviour
  return df.Filter("is_mt_trigger || is_et_trigger || is_em_trigger", "hltPaths.h: MC: filter OR of mutau etau or emu triggers");

}

/**********************************************************/

/*
 * Get 2018 mu-tau HLT trigger paths (depending on year and run number).
 * Inactive/unavailable triggers are set to 0 (false). If the sample is embedded, always return true.
 */


template <typename T>
auto DefinePassHLTPaths_MuTau_2018(T &df, LUNA::sampleConfig_t &sConfig) {

  (void) sConfig;

  bool hasNonHPS = false;
  bool hasHPS = false;

  if (HasExactBranch(df, "HLT_IsoMu20_eta2p1_LooseChargedIsoPFTau27_eta2p1_CrossL1")) {
    hasNonHPS = true;
  }
  else if (HasExactBranch(df, "HLT_IsoMu20_eta2p1_LooseChargedIsoPFTauHPS27_eta2p1_CrossL1")) {
    hasHPS = true;
  }

  std::cout << "   >>> hltPaths.h: " << sConfig.name() << " has mutau nonHPS and mutau HPS " << hasNonHPS << ", " << hasHPS << std::endl;

  auto df2 = df.Define("pass_mt_HLT_IsoMu24", [](bool b) { return (int) b; }, {"HLT_IsoMu24"})
               .Define("pass_mt_HLT_IsoMu27", [](bool b) { return (int) b; }, {"HLT_IsoMu27"});

  // Needs this formatting because sometimes the branches do not exist, e.g. for Embedded
  if (hasHPS && hasNonHPS) {
    return df2.Define("pass_mt_HLT_Mu20Tau27",    [](bool b) { return (int) b; }, {"HLT_IsoMu20_eta2p1_LooseChargedIsoPFTau27_eta2p1_CrossL1"})
              .Define("pass_mt_HLT_Mu20HPSTau27", [](bool b) { return (int) b; }, {"HLT_IsoMu20_eta2p1_LooseChargedIsoPFTauHPS27_eta2p1_CrossL1"});
  }
  else if (!hasHPS && hasNonHPS) {
    return df2.Define("pass_mt_HLT_Mu20Tau27",    [](bool b) { return (int) b; }, {"HLT_IsoMu20_eta2p1_LooseChargedIsoPFTau27_eta2p1_CrossL1"})
              .Define("pass_mt_HLT_Mu20HPSTau27", [] { return (int) false; });
  }
  else if (hasHPS && !hasNonHPS) {
    return df2.Define("pass_mt_HLT_Mu20Tau27",    [] { return (int) false; })
              .Define("pass_mt_HLT_Mu20HPSTau27", [](bool b) { return (int) b; }, {"HLT_IsoMu20_eta2p1_LooseChargedIsoPFTauHPS27_eta2p1_CrossL1"});
  }

  std::cout << "  >>> hltPaths.h: [WARNING:] Neither non-HPS nor HPS tau reconstruction HLT trigger path found for " << sConfig.name() << "! Returning false for both" << std::endl;
  return df2.Define("pass_mt_HLT_Mu20Tau27",    [] { return (int) false; })
            .Define("pass_mt_HLT_Mu20HPSTau27", [] { return (int) false; });

}

/**********************************************************/

/*
 * Get 2018 e-tau HLT trigger paths (depending on year and run number).
 * Inactive/unavailable triggers are set to 0 (false).
 * Note: for 2018 ETau if the TChain contains even one file which has NO runs after 317509 (the run after which HPS Tau is available),
 *       the TChain will appear to have the HPS Tau cross-trigger, but ROOT will throw an error when it gets to the file missing the HPS Tau
 *       cross-trigger branch. The three files in Run 2018 B to watch for, should be handled in their own batch job, and a helper function
 *       will check for the presence of one of the problematic files (as a proxy for detecting that special batch job).
 */

template <typename T>
auto DefinePassHLTPaths_ETau_2018(T &df, LUNA::sampleConfig_t &sConfig) {

  // std::cout << " >>> hltPaths.h: sConfig.getRunTag() returns " << sConfig.getRunTag() << std::endl;

  auto df2 = df.Define("pass_et_HLT_Ele32", [](bool b) { return (int) b; }, {"HLT_Ele32_WPTight_Gsf"})
               .Define("pass_et_HLT_Ele35", [](bool b) { return (int) b; }, {"HLT_Ele35_WPTight_Gsf"});

  bool hasNonHPS = HasExactBranch(df, "HLT_Ele24_eta2p1_WPTight_Gsf_LooseChargedIsoPFTau30_eta2p1_CrossL1");
  bool hasHPS    = HasExactBranch(df, "HLT_Ele24_eta2p1_WPTight_Gsf_LooseChargedIsoPFTauHPS30_eta2p1_CrossL1");

  // // Override for EGamma Run2018A: not necessary now because this is designed to run over one file at a time
  // if (sConfig.getRunTag() == "A") {
  //   std::cout << "   >>> hltPaths.h: EGamma Run2018A: override and never get HPS trigger path" << std::endl;
  //   hasHPS = false;
  // }

  std::cout << "   >>> hltPaths.h: " << sConfig.name() << " has etau non-HPS " << hasNonHPS << ", has etau HPS " << hasHPS << std::endl;

  // If the HLT branches are available in NanoAOD, save them, and do the MC/ Embed/ data run check logic later in a later function
  if (hasNonHPS && hasHPS) {
    return df2.Define("pass_et_HLT_Ele24Tau30",    [](bool b) { return (int) b; }, {"HLT_Ele24_eta2p1_WPTight_Gsf_LooseChargedIsoPFTau30_eta2p1_CrossL1"})
              .Define("pass_et_HLT_Ele24HPSTau30", [](bool b) { return (int) b; }, {"HLT_Ele24_eta2p1_WPTight_Gsf_LooseChargedIsoPFTauHPS30_eta2p1_CrossL1"});
  }
  else if (!hasNonHPS && hasHPS) {
    return df2.Define("pass_et_HLT_Ele24Tau30",    [] { return (int) false; })
              .Define("pass_et_HLT_Ele24HPSTau30", [](bool b) { return (int) b; }, {"HLT_Ele24_eta2p1_WPTight_Gsf_LooseChargedIsoPFTauHPS30_eta2p1_CrossL1"});
  }
  else if (hasNonHPS && !hasHPS) {
    return df2.Define("pass_et_HLT_Ele24Tau30",    [](bool b) { return (int) b; }, {"HLT_Ele24_eta2p1_WPTight_Gsf_LooseChargedIsoPFTau30_eta2p1_CrossL1"})
              .Define("pass_et_HLT_Ele24HPSTau30", [] { return (int) false; });
  }
  else {
    std::cout << "  >>> hltPaths.h: [WARNING] Neither HLT etauh cross-trigger was found for " << sConfig.name() << "! Returning false for both" << std::endl;
    return df2.Define("pass_et_HLT_Ele24Tau30",    [] { return (int) false; })
              .Define("pass_et_HLT_Ele24HPSTau30", [] { return (int) false; });
  }

}


/**********************************************************/

/*
 * Get e-mu HLT trigger paths (same for 2018 and 2017).
 */

template <typename T>
auto DefinePassHLTPaths_EMu_2018_and_2017(T &df, LUNA::sampleConfig_t &sConfig) {

  (void) sConfig;

  return df.Define("pass_em_HLT_Mu23Ele12", [](bool b) { return (int) b; }, {"HLT_Mu23_TrkIsoVVL_Ele12_CaloIdL_TrackIdL_IsoVL_DZ"})
           .Define("pass_em_HLT_Mu8Ele23",  [](bool b) { return (int) b; }, {"HLT_Mu8_TrkIsoVVL_Ele23_CaloIdL_TrackIdL_IsoVL_DZ"});

}



/**********************************************************/

/*
 * Get 2017 mu-tau HLT trigger paths (same for the entire year).
 */

template <typename T>
auto DefinePassHLTPaths_MuTau_2017(T &df, LUNA::sampleConfig_t &sConfig) {

  (void) sConfig;

  return df.Define("pass_mt_HLT_IsoMu27",   [](bool b) { return (int) b; }, {"HLT_IsoMu27"})
           .Define("pass_mt_HLT_Mu20Tau27", [](bool b) { return (int) b; }, {"HLT_IsoMu20_eta2p1_LooseChargedIsoPFTau27_eta2p1_CrossL1"});

}

/**********************************************************/

/*
 * Get 2017 e-tau HLT trigger paths (same for entire year).
 */

template <typename T>
auto DefinePassHLTPaths_ETau_2017(T &df, LUNA::sampleConfig_t &sConfig) {

  (void) sConfig;

  return df.Define("pass_et_HLT_Ele32", [](bool b) { return (int) b; }, {"HLT_Ele32_WPTight_Gsf_L1DoubleEG"})
           .Define("pass_et_HLT_Ele35", [](bool b) { return (int) b; }, {"HLT_Ele35_WPTight_Gsf"})
           .Define("pass_et_HLT_Ele24Tau30", [](bool b) { return (int) b; }, {"HLT_Ele24_eta2p1_WPTight_Gsf_LooseChargedIsoPFTau30_eta2p1_CrossL1"})
           .Define("pass_et_HLT_Ele24HPSTau30", []() { return (int) false; });

}


/**********************************************************/

// emu is the same in 2017 as in 2018, combined into one function

/**********************************************************/

/*
 * Get 2016 mutau HLT paths.
 */

template <typename T>
auto DefinePassHLTPaths_MuTau_2016(T &df, LUNA::sampleConfig_t &sConfig) {

  (void) sConfig;

  // These HLT paths are not present in some 2016 Run B files
  bool has_IsoMu22_eta2p1 = HasExactBranch(df, "HLT_IsoMu22_eta2p1");
  bool has_IsoTkMu22_eta2p1 = HasExactBranch(df, "HLT_IsoTkMu22_eta2p1");

  auto df2 = df.Define("pass_mt_HLT_IsoMu22", [](bool b) { return (int) b; }, {"HLT_IsoMu22"})
           .Define("pass_mt_HLT_IsoTkMu22",   [](bool b) { return (int) b; }, {"HLT_IsoTkMu22"})
           .Define("pass_mt_HLT_IsoMu19_eta2p1_LooseIsoPFTau20", [](bool b) { return (int) b; }, {"HLT_IsoMu19_eta2p1_LooseIsoPFTau20"})
           .Define("pass_mt_HLT_IsoMu19_eta2p1_LooseIsoPFTau20_SingleL1", [](bool b) { return (int) b; }, {"HLT_IsoMu19_eta2p1_LooseIsoPFTau20_SingleL1"});

  if (has_IsoMu22_eta2p1 && has_IsoTkMu22_eta2p1) {
    return df2.Define("pass_mt_HLT_IsoMu22_eta2p1", [](bool b) { return (int) b; }, {"HLT_IsoMu22_eta2p1"})
              .Define("pass_mt_HLT_IsoTkMu22_eta2p1",  [](bool b) { return (int) b; }, {"HLT_IsoTkMu22_eta2p1"});
  }
  else if (has_IsoMu22_eta2p1 && !has_IsoTkMu22_eta2p1) {
    return df2.Define("pass_mt_HLT_IsoMu22_eta2p1", [](bool b) { return (int) b; }, {"HLT_IsoMu22_eta2p1"})
              .Define("pass_mt_HLT_IsoTkMu22_eta2p1",  [] { return (int) false; });
  }
  else if (!has_IsoMu22_eta2p1 && has_IsoTkMu22_eta2p1) {
    return df2.Define("pass_mt_HLT_IsoMu22_eta2p1", [] { return (int) false; })
              .Define("pass_mt_HLT_IsoTkMu22_eta2p1",  [](bool b) { return (int) b; }, {"HLT_IsoTkMu22_eta2p1"});
  }
  else {
    return df2.Define("pass_mt_HLT_IsoMu22_eta2p1", [] { return (int) false; })
              .Define("pass_mt_HLT_IsoTkMu22_eta2p1",  [] { return (int) false; });
  }

}

/**********************************************************/

/*
 * Get the 2016 etau path.
 */

template <typename T>
auto DefinePassHLTPaths_ETau_2016(T &df, LUNA::sampleConfig_t &sConfig) {

  (void) sConfig;

  return df.Define("pass_et_HLT_Ele25_eta2p1_WPTight_Gsf", [](bool b) { return (int) b; }, {"HLT_Ele25_eta2p1_WPTight_Gsf"});
}

/**********************************************************/

/*
 * Get the 2016 emu path.
 */

template <typename T>
auto DefinePassHLTPaths_EMu_2016(T &df, LUNA::sampleConfig_t &sConfig) {


  std::string tag = sConfig.getRunTag();
  // MC, or data/embed Runs B-F
  if (sConfig.isMC() || (  (sConfig.isData() || sConfig.isEmbedded()) && ((tag == "B") || (tag == "C") || (tag == "D") || (tag == "E") || (tag == "F")) ) ) {
    return df.Define("pass_em_HLT_Mu23Ele12", [](bool b) { return (int) b; }, {"HLT_Mu23_TrkIsoVVL_Ele12_CaloIdL_TrackIdL_IsoVL"})
             .Define("pass_em_HLT_Mu8Ele23",  [](bool b) { return (int) b; }, {"HLT_Mu8_TrkIsoVVL_Ele23_CaloIdL_TrackIdL_IsoVL"});
  }
  // Data/Embed Runs G and H
  else if ( (sConfig.isData() || sConfig.isEmbedded()) && ( (tag == "G") || (tag == "H") ) ) {
    return df.Define("pass_em_HLT_Mu23Ele12", [](bool b) { return (int) b; }, {"HLT_Mu23_TrkIsoVVL_Ele12_CaloIdL_TrackIdL_IsoVL_DZ"})
             .Define("pass_em_HLT_Mu8Ele23",  [](bool b) { return (int) b; }, {"HLT_Mu8_TrkIsoVVL_Ele23_CaloIdL_TrackIdL_IsoVL_DZ"});
  }
  else {
    return df;
  }
}


/**********************************************************/

/*
 * Get the channel from the HLT paths, prioritizing in the order of mutau, etau, and then emu.
 */

template <typename T>
auto FilterUsingHLTPaths(T &df, LUNA::sampleConfig_t &sConfig) {

  std::cout << " >>> hltPaths.h: sConfig.getRunTag() returns " << sConfig.getRunTag() << std::endl;
  std::cout << " >>> hltPaths.h: sConfig.isMuTauOnly(), isETauOnly(), isEMuOnly(): " << sConfig.isMuTauOnly() << ", " << sConfig.isETauOnly() << ", " << sConfig.isEMuOnly() << std::endl;
  std::cout << " >>> hltPaths.h: isEmbedded && isMuTauOnly: " << sConfig.isEmbedded() << ", " << sConfig.isMuTauOnly() << std::endl;
  std::cout << " >>> hltPaths.h: isEmbedded && isETauOnly: " << sConfig.isEmbedded() << ", " << sConfig.isETauOnly() << std::endl;

  /*
    * 0: mu+tauh, 1: e+tauh, 2: e+mu, 3: undefined
    */
  if (sConfig.year() == 2018) {
    auto df2 = DefinePassHLTPaths_MuTau_2018(df, sConfig);
    auto df3 = DefinePassHLTPaths_ETau_2018(df2, sConfig);
    auto df4 = DefinePassHLTPaths_EMu_2018_and_2017(df3, sConfig);
    auto df5 = df4.Define("is_mt_trigger", [](int p1, int p2, int p3, int p4) { return (bool) (p1 || p2 || p3 || p4); }, {"pass_mt_HLT_IsoMu24", "pass_mt_HLT_IsoMu27", "pass_mt_HLT_Mu20Tau27", "pass_mt_HLT_Mu20HPSTau27"})
                  .Define("is_et_trigger", [](int p1, int p2, int p3, int p4) { return (bool) (p1 || p2 || p3 || p4); }, {"pass_et_HLT_Ele32", "pass_et_HLT_Ele35", "pass_et_HLT_Ele24Tau30", "pass_et_HLT_Ele24HPSTau30"})
                  .Define("is_em_trigger", [](int p1, int p2) { return (bool) (p1 || p2); }, {"pass_em_HLT_Mu23Ele12", "pass_em_HLT_Mu8Ele23"});
    auto df6 = FilterDataMCByHLTPaths(df5, sConfig);
    return df6;
  }
  else if (sConfig.year() == 2017) {
    auto df2 = DefinePassHLTPaths_MuTau_2017(df, sConfig);
    auto df3 = DefinePassHLTPaths_ETau_2017(df2, sConfig);
    auto df4 = DefinePassHLTPaths_EMu_2018_and_2017(df3, sConfig);
    auto df5 = df4.Define("is_mt_trigger", [](int p1, int p2) { return (bool) (p1 || p2); }, {"pass_mt_HLT_IsoMu27", "pass_mt_HLT_Mu20Tau27"})
                  .Define("is_et_trigger",  [](int p1, int p2, int p3) { return (bool) (p1 || p2 || p3); }, {"pass_et_HLT_Ele32",   "pass_et_HLT_Ele35",   "pass_et_HLT_Ele24Tau30"})
                  .Define("is_em_trigger", [](int p1, int p2) { return (bool) (p1 || p2); }, {"pass_em_HLT_Mu23Ele12", "pass_em_HLT_Mu8Ele23"});
    auto df6 = FilterDataMCByHLTPaths(df5, sConfig);
    return df6;
  }
  else {
    // 2016
    auto df2 = DefinePassHLTPaths_MuTau_2016(df, sConfig);
    auto df3 = DefinePassHLTPaths_ETau_2016(df2, sConfig);
    auto df4 = DefinePassHLTPaths_EMu_2016(df3, sConfig);
    auto df5 = df4.Define("is_mt_trigger", [](int p1, int p2, int p3, int p4, int p5, int p6) { return (bool) (p1 || p2 || p3 || p4 || p5 || p6); },
                      {"pass_mt_HLT_IsoMu22", "pass_mt_HLT_IsoMu22_eta2p1", "pass_mt_HLT_IsoTkMu22", "pass_mt_HLT_IsoTkMu22_eta2p1", "pass_mt_HLT_IsoMu19_eta2p1_LooseIsoPFTau20", "pass_mt_HLT_IsoMu19_eta2p1_LooseIsoPFTau20_SingleL1"})
              .Define("is_et_trigger", [](int p1) { return (bool) p1; }, {"pass_et_HLT_Ele25_eta2p1_WPTight_Gsf"})
              .Define("is_em_trigger", [](int p1, int p2) { return (bool) (p1 || p2); }, {"pass_em_HLT_Mu23Ele12", "pass_em_HLT_Mu8Ele23"});
    auto df6 = FilterDataMCByHLTPaths(df5, sConfig);
    return df6;

  }
}



/**********************************************************/


#endif
