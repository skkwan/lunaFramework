#ifndef CLEANING_H_INCL
#define CLEANING_H_INCL

#include "computePhysicsVariables.h"
#include "helperFunctions.h"

/******************************************************************************/


/* Check if event has loose baseline objects. Taken from the channels under https://twiki.cern.ch/twiki/bin/viewauth/CMS/HiggsToTauTauWorkingLegacyRun2#Baseline_selection. */

ROOT::RVec<int> findVeryLooseOfflineMuons(ROOT::RVec<float>& Muon_pt, ROOT::RVec<float>& Muon_eta,
                                             ROOT::RVec<float>& Muon_dz, ROOT::RVec<float>& Muon_dxy,
                                             ROOT::RVec<bool>& Muon_mediumId, ROOT::RVec<float>& Muon_pfRelIso04_all) {
                                               return (Muon_pt > 10) && (abs(Muon_eta) < 2.4) && (Muon_mediumId == true) && (abs(Muon_dz) < 0.2) && (abs(Muon_dxy) < 0.045) && (Muon_pfRelIso04_all < 0.5);
                                             }

ROOT::RVec<int> findVeryLooseOfflineElectrons(ROOT::RVec<float>& Electron_pt, ROOT::RVec<float>& Electron_eta,
                                                ROOT::RVec<float>& Electron_dz, ROOT::RVec<float>& Electron_dxy,
                                                ROOT::RVec<unsigned char>& Electron_lostHits, ROOT::RVec<bool>& Electron_convVeto,
                                                ROOT::RVec<float>& Electron_pfRelIso03_all) {
                                                  return (Electron_pt > 10) && (abs(Electron_eta) < 2.1) && (abs(Electron_dz) < 0.2) && (abs(Electron_dxy) < 0.045) &&
                                                         (Electron_lostHits <= 1) && (Electron_convVeto) && (Electron_pfRelIso03_all < 0.5);
                                                }


ROOT::RVec<int> findVeryLooseOfflineTaus(ROOT::RVec<float>& Tau_pt, ROOT::RVec<float>& Tau_eta,
                                                      ROOT::RVec<float>& Tau_dz,
                                                      ROOT::RVec<unsigned char>& Tau_idDeepTau2017v2p1VSe,
                                                      ROOT::RVec<unsigned char>& Tau_idDeepTau2017v2p1VSmu,
                                                      ROOT::RVec<unsigned char>& Tau_idDeepTau2017v2p1VSjet,
                                                      ROOT::RVec<int>& Tau_decayMode) {
                                        return  (Tau_pt > 10 && abs(Tau_eta) < 2.3 && abs(Tau_dz) < 0.2 && (Tau_idDeepTau2017v2p1VSe & 0x01) && (Tau_idDeepTau2017v2p1VSmu & 0x01) && (Tau_idDeepTau2017v2p1VSjet & 0x01)
                                                && Tau_decayMode != 5 && Tau_decayMode != 6);
                                      }


ROOT::RVec<int> findVeryLooseOfflineJets(ROOT::RVec<float>& Jet_pt, ROOT::RVec<float>& Jet_eta, ROOT::RVec<int>& Jet_jetId) {
  // Use uncorrected jet pt with a VERY loose cut
  return (Jet_pt > 10) && (abs(Jet_eta) < 4.7) && (Jet_jetId & 0x2);
}


/*************************************************************/


template <typename T>
auto CleanOfflineObjects(T &df, LUNA::sampleConfig_t &sConfig) {

    (void) sConfig;

    // Very loose selection
    auto df2 = df.Define("goodVeryLooseMuons", findVeryLooseOfflineMuons, {"Muon_pt", "Muon_eta", "Muon_dz", "Muon_dxy", "Muon_mediumId", "Muon_pfRelIso04_all"})
           .Define("goodVeryLooseElectrons", findVeryLooseOfflineElectrons, {"Electron_pt", "Electron_eta", "Electron_dz", "Electron_dxy",
                                                                        "Electron_lostHits", "Electron_convVeto", "Electron_pfRelIso03_all"})
           .Define("goodVeryLooseTaus", findVeryLooseOfflineTaus, {"Tau_pt", "Tau_eta", "Tau_dz", "Tau_idDeepTau2017v2p1VSe", "Tau_idDeepTau2017v2p1VSmu",
                                                                  "Tau_idDeepTau2017v2p1VSjet", "Tau_decayMode"})
           .Define("goodVeryLooseJets", findVeryLooseOfflineJets, {"Jet_pt", "Jet_eta", "Jet_jetId"});


    // Perform cleaning
    return df2.Define("cleaned_ele_from_mu", clean_collection, {"goodVeryLooseElectrons", "Electron_eta", "Electron_phi", "goodVeryLooseMuons", "Muon_eta", "Muon_phi"})
              // taus
              .Define("cleaned_tau_from_mu", clean_collection, {"goodVeryLooseTaus", "Tau_eta", "Tau_phi", "goodVeryLooseMuons", "Muon_eta", "Muon_phi"})
              // note we pass cleaned_tau_from_mu, and cleaned_ele_from_mu to the next line
              .Define("cleaned_tau_from_mu_ele", clean_collection, {"cleaned_tau_from_mu", "Tau_eta", "Tau_phi",  "cleaned_ele_from_mu", "Electron_eta", "Electron_phi"})
              // jets
              .Define("cleaned_jet_from_mu", clean_collection, {"goodVeryLooseJets", "Jet_eta", "Jet_phi", "goodVeryLooseMuons", "Muon_eta", "Muon_phi"})
              .Define("cleaned_jet_from_mu_ele", clean_collection, {"cleaned_jet_from_mu", "Jet_eta", "Jet_phi", "cleaned_ele_from_mu", "Electron_eta", "Electron_phi"})
              .Define("cleaned_jet_from_mu_ele_tau", clean_collection, {"cleaned_jet_from_mu_ele", "Jet_eta", "Jet_phi", "cleaned_tau_from_mu_ele", "Tau_eta", "Tau_phi"});


}

/******************************************************************************/

#endif
