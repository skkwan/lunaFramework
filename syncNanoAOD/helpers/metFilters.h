#ifndef MET_FILTERS_H_INCL
#define MET_FILTERS_H_INCL

#include "helperFunctions.h"

/**********************************************************/

/*
 * Apply MET filters: https://gitlab.cern.ch/skkwan/lunaFramework/-/issues/88
 * https://twiki.cern.ch/twiki/bin/view/CMS/MissingETOptionalFiltersRun2
 */
template <typename T>
auto ApplyMETFilters(T &df, LUNA::sampleConfig_t &sConfig) {

    auto df2 = df.Filter("Flag_goodVertices", "metFilters.h: passes Flag_goodVertices")
                 .Filter("Flag_globalSuperTightHalo2016Filter", "metFilters.h: passes Flag_globalSuperTightHalo2016Filter")
                 .Filter("Flag_HBHENoiseFilter", "metFilters.h: passes Flag_HBHENoiseFilter")
                 .Filter("Flag_HBHENoiseIsoFilter", "metFilters.h: passes Flag_HBHENoiseIsoFilter")
                 .Filter("Flag_EcalDeadCellTriggerPrimitiveFilter", "metFilters.h: passes Flag_EcalDeadCellTriggerPrimitiveFilter")
                 .Filter("Flag_BadPFMuonFilter", "metFilters.h: passes Flag_BadPFMuonFilter")
                 .Filter("Flag_BadPFMuonDzFilter", "metFilters.h: passes Flag_BadPFMuonDzFilter")
                 .Filter("Flag_hfNoisyHitsFilter", "metFilters.h: passes Flag_hfNoisyHitsFilter")
                 .Filter("Flag_eeBadScFilter", "metFilters.h: passes Flag_eeBadScFilter");

    if ((sConfig.year() == 2017) || (sConfig.year() == 2018)) {
        // Additional filter
        return df2.Filter("Flag_ecalBadCalibFilter", "metFilters.h: 2017 and 2018 only: passes Flag_ecalBadCalibFilter");
    }
    else {
        return df2;
    }


}

/**********************************************************/

#endif
