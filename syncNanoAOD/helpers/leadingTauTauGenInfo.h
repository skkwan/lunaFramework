#ifndef LEADING_TAU_TAU_GEN_INFO_H
#define LEADING_TAU_TAU_GEN_INFO_H

#include "helperFunctions.h"
#include "electron.h"
#include "leadingTauTauGenInfo.h"
#include "muon.h"
#include "tau.h"

/**********************************************************/

/*
 * leg is 1 or 2 (the leg to get): together with channel, tells us which leg to access
 */
std::vector<std::vector<unsigned int>> get_gen_match_MC(int ch, int idx_1, int idx_2,  int isEmbedded, int isMC, RVec<unsigned char>& Electron_genPartFlav, RVec<unsigned char>& Muon_genPartFlav, RVec<unsigned char>& Tau_genPartFlav) {
    std::vector<unsigned int> genPartFlavPair;
    std::vector<unsigned int> genMatchPair;

    if (ch == Helper::mt) {
       //  std::cout << "   >>> leadingTauTauGenInfo.h: Processing mutau Embed: set gen_match (do not set genPartFlav)." << std::endl;

        if (isEmbedded) {
            genPartFlavPair.push_back(9999); // _1
            genPartFlavPair.push_back(9999); // _2
            genMatchPair.push_back(4); // mu
            genMatchPair.push_back(5); // tau
        }
        else if (isMC) {
           //  std::cout << "   >>> leadingTauTauGenInfo.h: Processing mutau MC: get gen_match and genPartFlav." << std::endl;
            genPartFlavPair.push_back(Muon_genPartFlav[idx_1]);
            genPartFlavPair.push_back(Tau_genPartFlav[idx_2]);
            genMatchPair.push_back(convertMuGenPartFlavToTauGenPartFlav(Muon_genPartFlav[idx_1]));
            genMatchPair.push_back(convertTauGenPartFlav(Tau_genPartFlav[idx_2]));
        }
    }
    else if (ch == Helper::et) {
       //  std::cout << "   >>> leadingTauTauGenInfo.h: Processing etau Embed: set gen_match (do not set genPartFlav)." << std::endl;
        if (isEmbedded){
            genPartFlavPair.push_back(9999); // _1
            genPartFlavPair.push_back(9999); // _2
            genMatchPair.push_back(3); // e
            genMatchPair.push_back(5); // tau
        }
        else if (isMC) {
            genPartFlavPair.push_back(Electron_genPartFlav[idx_1]);
            genPartFlavPair.push_back(Tau_genPartFlav[idx_2]);
            genMatchPair.push_back(convertElectronGenPartFlavToTauGenPartFlav(Electron_genPartFlav[idx_1]));
            genMatchPair.push_back(convertTauGenPartFlav(Tau_genPartFlav[idx_2]));
        }
    }
    else if (ch == Helper::em) {
        if (isEmbedded) {
            genPartFlavPair.push_back(9999); // _1
            genPartFlavPair.push_back(9999); // _2
            genMatchPair.push_back(3); // e
            genMatchPair.push_back(4); // mu
        }
        else if (isMC) {
            genPartFlavPair.push_back(Electron_genPartFlav[idx_1]);
            genPartFlavPair.push_back(Muon_genPartFlav[idx_2]);
            genMatchPair.push_back(convertElectronGenPartFlavToTauGenPartFlav(Electron_genPartFlav[idx_1]));
            genMatchPair.push_back(convertMuGenPartFlavToTauGenPartFlav(Muon_genPartFlav[idx_2]));
        }
    }

    // Push back two vectors to one vector
    std::vector<std::vector<unsigned int>> genInfo;
    genInfo.push_back(genPartFlavPair);
    genInfo.push_back(genMatchPair);
    return genInfo;
}

/**********************************************************/

template <typename T>
auto GetGenMatchInfo(T &df, LUNA::sampleConfig_t &sConfig) {

    if (sConfig.isData()) {
        std::cout << "  >>> leadingTauTauGenInfo.h: Data, do not define gen_match_info" << std::endl;
        return df.Define("gen_match_info", []() { std::vector<std::vector<unsigned int>> genInfo = {{9999, 9999}, {9999, 9999}}; return genInfo; });
    }
    else {
        std::cout << "  >>> leadingTauTauGenInfo.h: MC or Embedded, do not define gen_match_info" << std::endl;
        return df.Define("gen_match_info", get_gen_match_MC, {"channel", "idx_1", "idx_2", "isEmbedded", "isMC", "Electron_genPartFlav", "Muon_genPartFlav", "Tau_genPartFlav"});
    }

}

#endif
