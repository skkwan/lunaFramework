// Helper functions for trigger branches.

#ifndef EXTRALEPTONS_H_INCL
#define EXTRALEPTONS_H_INCL

#include "computePhysicsVariables.h"
#include "helperFunctions.h"
#include "fileIO.h"

/**********************************************************/

/*
 * Third lepton veto for the OTHER type of lepton: i.e. For mutauh, check if there are any electrons passing the third lepton filter.
 * For etauh, check if there are any muons passing the third lepton filter.
 * This check does not apply for e+mu.
 */
bool passes_veto_third_lepton_other_type(int channel, ROOT::RVec<int>& electrons_for_lepton_veto, ROOT::RVec<int>& muons_for_lepton_veto) {
                              if (channel == Helper::mt) {
                                return (Sum(electrons_for_lepton_veto) == 0);
                              }
                              else if (channel == Helper::et) {
                                return (Sum(muons_for_lepton_veto) == 0);
                              }
                              return true;
                            }

/**********************************************************/

/*
 * Third lepton veto for the SAME type of lepton: i.e.
 * For mutauh, check if there are MUONS passing the third lepton baseline, that are NOT the leading muon.
 *  - idx_lepton is the idx of the muon into Muon_* branches. There is no idx_second_lepton.
 * For etauh, check if there are ELECTRONS passing the third lepton baseline, that are not the leading electron.
 *  - idx_lepton is the idx of the electron into Electron_* branches. There is no idx_second_lepton.
 * For emu, check if there are ELECTRONS passing the third lepton baseline, that are not the leading electron, and check if there are MUONS passing the third lepton baseline, that are not the leading muon.
 *  - idx_lepton is the idx of the electron into Electron_* branches, and idx_second_lepton is the idx of the MUON into Muon_* branches.
 */

bool passes_veto_third_lepton_same_type(int channel, int idx_lepton, int idx_second_lepton, ROOT::RVec<int>& electrons_for_lepton_veto, ROOT::RVec<int>& muons_for_lepton_veto) {
  bool passes_third_lepton_same_type = true;

  // In the mutauh channel, check if there are muons that pass the lepton veto baseline, that are NOT the leading muon
  if (channel == Helper::mt) {
    for (int i = 0; i < (int) muons_for_lepton_veto.size(); i++) {
      if (muons_for_lepton_veto[i] == 1) {  // if a muon passes the lepton veto baseline
        if (i != idx_lepton) {       // check if it's the leading lepton
          passes_third_lepton_same_type = false;
          break;
        }
      }
    }
  }
  // etauh channel: check electron
  else if (channel == Helper::et) {
    for (int i = 0; i < (int) electrons_for_lepton_veto.size(); i++) {
      if (electrons_for_lepton_veto[i] == 1) {
        if (i != idx_lepton) {
          passes_third_lepton_same_type = false;
          break;
        }
      }
    }
  }
  // emu channel: Note an additional loop here because e+mu has two leptons! Check the muon (idx_second_lepton)
  else if (channel == Helper::em) {
    for (int i = 0; i < (int) electrons_for_lepton_veto.size(); i++) {
      if (electrons_for_lepton_veto[i] == 1) {
        if (i != idx_lepton) {
          passes_third_lepton_same_type = false;
          break;
        }
      }
    }
    for (int i = 0; i < (int) muons_for_lepton_veto.size(); i++) {
      if (muons_for_lepton_veto[i] == 1) {
        if (i != idx_second_lepton) {
          passes_third_lepton_same_type = false;
          break;
        }
      }
    }
  }
  return passes_third_lepton_same_type;
}


/**********************************************************/

/*
 * Return one pair of indices for an opposite-sign pair of the
 * same type of lepton separated by deltaR > 0.15.
 *
 * Arguments: the RVec<int> specifying which leptons passed the baseline cut,
 * and the lepton charges, eta, and phi.
 *
 * If no such pair is found, return (-1, -1). Else, return the
 * indices of the LAST such pair found (no sorting is done here).
 */

bool passes_veto_OS_Lepton_Pair(ROOT::RVec<int>& leptonCandidates, ROOT::RVec<int>& charge, ROOT::RVec<float>& eta, ROOT::RVec<float>& phi) {
  bool passes_lepton_pair_veto = true;

  // Get indices of all possible combinations
  auto comb = Combinations(charge, charge);
  const auto numComb = comb[0].size();

  // Check all unique combinations if they are separated in deltaR and have OS
  for (size_t i = 0; i < numComb; i++) {
    const auto i1 = comb[0][i];
    const auto i2 = comb[1][i];
    if (leptonCandidates[i1] == 1 && leptonCandidates[i2] == 1) {
      const auto deltar = Helper::DeltaR(eta[i1], eta[i2], phi[i1], phi[i2]);
      if ((deltar > 0.15) && (charge[i1] * charge[i2] < 0)) {
	      passes_lepton_pair_veto = false;
        break;
      }
    }
  }

  return passes_lepton_pair_veto;
}

/**********************************************************/

// Loose selection for extra muon/ electron checks.

ROOT::RVec<int> findExtraMuonCandidates(ROOT::RVec<float>& Muon_pt, ROOT::RVec<float>& Muon_eta,
                                        ROOT::RVec<float>& Muon_dz, ROOT::RVec<float>& Muon_dxy,
                                        ROOT::RVec<bool>& Muon_mediumId, ROOT::RVec<float>& Muon_pfRelIso04_all) {
                                          return (Muon_pt > 10) && (abs(Muon_eta) < 2.4) && (abs(Muon_dxy) < 0.045) && (abs(Muon_dz) < 0.2) && Muon_mediumId && (Muon_pfRelIso04_all < 0.3);
                                      }


ROOT::RVec<int> findExtraElectronCandidates(ROOT::RVec<float>& Electron_pt, ROOT::RVec<float>& Electron_eta,
                                            ROOT::RVec<float>& Electron_dz, ROOT::RVec<float>& Electron_dxy,
                                            ROOT::RVec<bool>& Electron_mvaFall17V2noIso_WP90,  ROOT::RVec<unsigned char>& Electron_lostHits, ROOT::RVec<bool>& Electron_convVeto,
                                            ROOT::RVec<float>& Electron_pfRelIso03_all) {
                                              return (Electron_pt > 10) && (abs(Electron_eta) < 2.5) && (abs(Electron_dxy) < 0.045) && (abs(Electron_dz) < 0.2) &&
                                                      Electron_mvaFall17V2noIso_WP90 && (Electron_convVeto) && (Electron_lostHits <= 1) && (Electron_pfRelIso03_all < 0.3);
                                            }

ROOT::RVec<int> findDiMuonVetoCandidates(ROOT::RVec<float>& Muon_pt, ROOT::RVec<float>& Muon_eta,
                                        ROOT::RVec<bool>& Muon_isGlobal, ROOT::RVec<bool>& Muon_isTracker, ROOT::RVec<bool>& Muon_isPFcand,
                                        ROOT::RVec<float>& Muon_dz, ROOT::RVec<float>& Muon_dxy, ROOT::RVec<float>& Muon_pfRelIso04_all) {
                                          return (Muon_pt > 15) && (abs(Muon_eta) < 2.4) && (Muon_isGlobal) && (Muon_isTracker) && (Muon_isPFcand) &&
                                                 (abs(Muon_dz) < 0.2) && (abs(Muon_dxy) < 0.045) && (Muon_pfRelIso04_all < 0.3);
                                        }

ROOT::RVec<int> findDiElectronVetoCandidates(ROOT::RVec<float>& Electron_pt, ROOT::RVec<float>& Electron_eta,
                                            ROOT::RVec<float>& Electron_dz, ROOT::RVec<float>& Electron_dxy,
                                            ROOT::RVec<bool>& Electron_mvaFall17V2noIso_WP90, ROOT::RVec<float>& Electron_pfRelIso03_all) {
                                              return (Electron_pt > 15) && (abs(Electron_eta) < 2.5) && (Electron_mvaFall17V2noIso_WP90) && (abs(Electron_dz) < 0.2) && (abs(Electron_dxy) < 0.045) && (Electron_pfRelIso03_all < 0.3);

                                            }

/**********************************************************/
/*
 * Third lepton veto.
 */

template <typename T>
auto FilterThirdLeptonAndDiLeptonVetoes(T &df) {

  // Third lepton vetoes
  return df.Define("extraMuonCandidates",     findExtraMuonCandidates,     {"Muon_pt", "Muon_eta", "Muon_dz", "Muon_dxy", "Muon_mediumId", "Muon_pfRelIso04_all"})
           .Define("extraElectronCandidates", findExtraElectronCandidates, {"Electron_pt", "Electron_eta", "Electron_dz", "Electron_dxy",
                                                                            "Electron_mvaFall17V2noIso_WP90", "Electron_lostHits", "Electron_convVeto", "Electron_miniPFRelIso_all"})
           .Filter(passes_veto_third_lepton_other_type, {"channel", "extraElectronCandidates", "extraMuonCandidates"},
                                                        "extraLeptons.h: Passes veto for extra lepton of the other type (muon for etauh, electron for mutauh)")
           .Filter(passes_veto_third_lepton_same_type, {"channel", "idx_1", "idx_2", "extraElectronCandidates", "extraMuonCandidates"},
                                                        "extraLeptons.h: Passes veto for extra lepton of the same type (electron for etauh, muon for mutauh, electron or muon for emu)")
  // Di-muon vetoes
           .Define("dimuonVetoCandidates", findDiMuonVetoCandidates, {"Muon_pt", "Muon_eta", "Muon_isGlobal", "Muon_isTracker", "Muon_isPFcand", "Muon_dz", "Muon_dxy", "Muon_pfRelIso04_all"})
           .Filter(passes_veto_OS_Lepton_Pair, {"dimuonVetoCandidates", "Muon_charge", "Muon_eta", "Muon_phi"},
                                                "extraLeptons.h: Passes veto for an OS, close in deltaR di-muon pair")
  // Di-electron vetoes
           .Define("dielectronVetoCandidates", findDiElectronVetoCandidates, {"Electron_pt", "Electron_eta", "Electron_dz", "Electron_dxy", "Electron_mvaFall17V2noIso_WP90", "Electron_pfRelIso03_all"})
           .Filter(passes_veto_OS_Lepton_Pair, {"dielectronVetoCandidates", "Electron_charge", "Electron_eta", "Electron_phi"},
                                                "extraLeptons.h: Passes veto for an OS, close in deltaR di-electron pair");

}

/**********************************************************/

#endif
