#ifndef SAMPLE_CONFIG_CLASS_H_INCL
#define SAMPLE_CONFIG_CLASS_H_INCL

#include <string>

#include "genHelper.h"
#include "helperFunctions.h"

/*************************************************************************/

namespace LUNA {

    class sampleConfig_t {

        private:
            std::string sampleName;
            int isSampleData;
            int isSampleEmbedded;
            int isSampleMC;
            int sampleYear;
            std::string runTag;
            std::string sampleEra;

            // Do systematics at all?
            int doSystematics;

            // Do JER systematics for this dataset?
            int doJERsystematics;

            int isSampleTTbar;
            int isSampleDY;
            int isSampleSignal;

            int isSampleDataEmbedMuTau;
            int isSampleDataEmbedETau;
            int isSampleDataEmbedEMu;

        public:
            ///////////////////////
            // Public members
            ///////////////////////

            // Are the JER systematic jet pT and MET branches in the file?
            int hasJERsysInputs; // has "MET_T1Smear_pt_jerUp"
            int hasJERyearsysInputs; // has "MET_T1Smear_pt_jesAbsolute_2018Up"
            int hasMetUESInputs; // has "MET_T1Smear_pt_unclustEnUp"

              ///////////////////////
            // Public methods
            ///////////////////////

            // To initialize a sample config
            sampleConfig_t(std::string processName);

            // Setters
            void setEra(std::string era);
            void setDoSystematics(int performSys);

            // Getters
            std::string name();
            int isData();
            int isEmbedded();
            int isMC();
            int isSignal();
            int year();
            std::string yearStr();
            std::string era();

            // Is the sample data or embed that is specific to a final state
            int isMuTauOnly();
            int isETauOnly();
            int isEMuOnly();

            int doSys();
            int isTTbar();
            int isDY();
            int isWJets();
            int isHiggsRecoil();
            int doRecoil();
            int recoilBosonPdgId();

            // Recoil type: 0 for none, 1 for W recoil boson, 2 for Z/H recoil boson (electrically neutral)
            int recoilType();

            // Is MC and not a Higgs sample (for contamination to Embedded from non-DYjets samples)
            int isMCnonHiggs();

            std::string getRunTag();
    };

}
/*************************************************************************/

#endif
