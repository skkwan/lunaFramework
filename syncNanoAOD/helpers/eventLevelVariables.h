/*
 * Helper file for event-level variable n-tuple branches.
 */

#ifndef EVENT_LEVEL_VARIABLES_H
#define EVENT_LEVEL_VARIABLES_H

#include "sampleConfig_class.h"

/*************************************************************************/

/*
 * Add the event weight to the dataset as the column "weight"
 */
template <typename T>
auto AddEventWeightAndPUInfo(T &df,
           LUNA::sampleConfig_t &sConfig,
			     const double nEntries,
			     const double genEventCount, const double genEventSumw) {

  auto df2 = df.Define("nEntries", [nEntries] { return nEntries; })
               .Define("genEventCount", [genEventCount] { return genEventCount; })
               .Define("genEventSumw",  [genEventSumw] { return genEventSumw; });

  if (sConfig.isData()) {
    // Data: define genWeight. No simulated pileup.
    return df2.Define("genWeight", [] { return 1.0f; });
  }
  else if (sConfig.isEmbedded()) {
    // Embedded: genWeight is already a branch. No simulated pileup.
    return df2;
  }
  else {
    // MC: genWeight is already a branch. Get simulated number of pileup interactions that have been added to the event in the current bunch crossing,
    // and the true mean number of the poisson distribution for this event, from which the number of interactions each bunch crossing has been sampled
    return df2.Define("npu", [](int i) { return i; }, {"Pileup_nPU"})
              .Define("nTrueInt", [](float f) { return f; }, {"Pileup_nTrueInt"});
  }
}



/*************************************************************************/

/*
 * Save the lumi block, and event number with the desired naming
 * convention. ("run" not included because its name is unchanged).
 */

template <typename T>
auto GetRunLumiEvent(T &df) {

  return df.Define("lumi", [](unsigned int i) { return i; }, {"luminosityBlock"})
           .Define("evt", [](unsigned long long i) { return (unsigned int) i; }, {"event"});


}

/*************************************************************************/

/* Equivalent to the above, but using unsigned long long for evt. */

template <typename T>
auto GetGenRunLumiEvent(T &df) {
  return df.Define("lumi", "(unsigned int) luminosityBlock")
           .Define("evt", "(unsigned int) event");
}

/*************************************************************************/

/*
 * Return true if the dataframe has a branch "branch" and false otherwise.
 * The branch name must match exactly.
 */

template <typename T>
bool HasExactBranch(T &df, std::string branch) {

  bool hasBranch = false;

  // Get df column names (branches in the NanoAOD)
  auto colNames = df.GetColumnNames();
  for (auto &&colName : colNames) {
    if (colName == branch) {
      hasBranch = true;
      break;
    }
  }

  return hasBranch;
}

/*************************************************************************/


#endif
