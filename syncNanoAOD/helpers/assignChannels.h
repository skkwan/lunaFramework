#ifndef ASSIGN_CHANNELS_H_INCL
#define ASSIGN_CHANNELS_H_INCL

#include "computePhysicsVariables.h"
#include "helperFunctions.h"
#include "sampleConfig_class.h"


/******************************************************************************/

bool isMuTau(ROOT::RVec<int> goodMu, ROOT::RVec<int> goodEle) {
    return (Sum(goodMu) >= 1) && (Sum(goodEle) == 0);
}

bool isETau(ROOT::RVec<int> goodMu, ROOT::RVec<int> goodEle) {
    return (Sum(goodMu) == 0) && (Sum(goodEle) >= 1);
}

bool isEMu(ROOT::RVec<int> goodMu, ROOT::RVec<int> goodEle) {
    return (Sum(goodMu) >= 1) && (Sum(goodEle) >= 1);
}

/******************************************************************************/

// Then if n(electrons) = 0 and n(muons)>=1 assign to mutau
// if n(muons) = 0 and n(electrons)>=1 assign to etau,
/// and if n(electrons)>=1 and n(muons)>=1 assign to emu

template <typename T>
auto AssignChannels(T &df, LUNA::sampleConfig_t &sConfig) {

    // If data or embed, need to handle overlap
    if (sConfig.isData() || sConfig.isEmbedded()) {
        if (sConfig.isMuTauOnly()) {
            return df.Define("channel", [](ROOT::RVec<int> goodMu, ROOT::RVec<int> goodEle) {
                if (isMuTau(goodMu, goodEle)) return Helper::mt;
                else return Helper::none;
            }, {"goodVeryLooseMuons", "cleaned_ele_from_mu"});
        }
        else if (sConfig.isETauOnly()) {
            return df.Define("channel", [](ROOT::RVec<int> goodMu, ROOT::RVec<int> goodEle) {
                if (isETau(goodMu, goodEle)) return Helper::et;
                else return Helper::none;
            }, {"goodVeryLooseMuons", "cleaned_ele_from_mu"});
        }
        else if (sConfig.isEMuOnly())  {
            return df.Define("channel", [](ROOT::RVec<int> goodMu, ROOT::RVec<int> goodEle) {
                if (isEMu(goodMu, goodEle)) return Helper::em;
                else return Helper::none;
            }, {"goodVeryLooseMuons", "cleaned_ele_from_mu"});
        }
    }
    // MC: default behaviour
    return df.Define("channel", [](ROOT::RVec<int> goodMu, ROOT::RVec<int> goodEle) {
            if (isMuTau(goodMu, goodEle)) return Helper::mt;
            else if (isETau(goodMu, goodEle)) return Helper::et;
            else if (isEMu(goodMu, goodEle)) return Helper::em;
            else return Helper::none;
        }, {"goodVeryLooseMuons", "cleaned_ele_from_mu"});
}

/******************************************************************************/

#endif
