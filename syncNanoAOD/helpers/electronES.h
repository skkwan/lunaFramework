// Electron energy scale and shifts

#ifndef ELECTRON_ES_H_INCL
#define ELECTRON_ES_H_INCL

#include "correction.h"
#include "helperFunctions.h"
#include "sampleConfig_class.h"

using correction::CorrectionSet;



/******************************************************************************/

/*
 * Ultralegacy: https://twiki.cern.ch/twiki/bin/view/CMS/EgammaUL2016To2018
 */
std::vector<ROOT::Math::PtEtaPhiMVector> applyElectronEnergyScale_nominal(float eleES, ROOT::Math::PtEtaPhiMVector uncorrected_ele_p4, ROOT::Math::PtEtaPhiMVector uncorrected_met_p4) {
    std::vector<ROOT::Math::PtEtaPhiMVector> corrected_pair;
    ROOT::Math::PtEtaPhiMVector corrected_ele_p4 = (eleES * uncorrected_ele_p4);
    ROOT::Math::PtEtaPhiMVector corrected_met_p4 = (uncorrected_met_p4 - (corrected_ele_p4 - uncorrected_ele_p4));
    corrected_pair.push_back(corrected_ele_p4);
    corrected_pair.push_back(corrected_met_p4);
    return corrected_pair;
}

/******************************************************************************/

ROOT::RVec<ROOT::RVec<ROOT::Math::PtEtaPhiMVector>> applyElectronEnergyScale_variations(int channel, int isMC, int isEmbedded, int year, float eleES_up, float eleES_down,
                                             ROOT::Math::PtEtaPhiMVector uncorrected_ele_p4,
                                             ROOT::Math::PtEtaPhiMVector uncorrected_met_p4) {

    (void) year;
    (void) isMC;
    (void) isEmbedded;

    if (!((channel == Helper::et) || (channel == Helper::em))) {
        // If not etau or emu channel, do not apply corrections
        eleES_up = 1.0;
        eleES_down = 1.0;
    }
    ROOT::Math::PtEtaPhiMVector corrected_ele_p4_up = (eleES_up * uncorrected_ele_p4);
    ROOT::Math::PtEtaPhiMVector corrected_met_p4_up = (eleES_up * uncorrected_met_p4);

    ROOT::Math::PtEtaPhiMVector corrected_ele_p4_down = (eleES_down * uncorrected_ele_p4);
    ROOT::Math::PtEtaPhiMVector corrected_met_p4_down = (eleES_down * uncorrected_met_p4);

    return ROOT::RVec<ROOT::RVec<ROOT::Math::PtEtaPhiMVector>>{{corrected_ele_p4_up, corrected_ele_p4_down}, {corrected_met_p4_up, corrected_met_p4_down}};
}

/******************************************************************************/

template <typename T>
auto GetElectronEnergyScaleShifts(T &df, LUNA::sampleConfig_t &sConfig, TString jsonDir, TString jsonDirEmbed) {

    // Default behaviour: MC
    TString cset_dir;
    cset_dir.Form("%sPOG/EGM/%s_UL/EGM_ScaleUnc.json", jsonDir.Data(), sConfig.era().c_str());
    auto cset = CorrectionSet::from_file(cset_dir.Data());
    auto cset_egES = cset->at("UL-EGM_ScaleUnc");

    if (sConfig.isEmbedded()) {
        std::cout << "[electronES.h]: Using embedded-specific electron energy scales" << std::endl;
        cset_dir.Form("%seleES_%sUL.json", jsonDirEmbed.Data(), sConfig.era().c_str());
        cset = CorrectionSet::from_file(cset_dir.Data());
        cset_egES = cset->at("eleES");
    }

    // etau
    if (sConfig.isMC() || sConfig.isEmbedded()) {
        // etau
        auto df2 = df.Define("p4_1_et_noEleES", add_p4, {"pt_1", "eta_1", "phi_1", "m_1"})
                     .Define("p4_met_et_noEleES", add_p4, {"met", "zero", "metphi", "zero"})
                     .Define("eleES", [cset_egES](const int channel, const int isMC, const int isEmbedded, const float ele_eta) {
                        if ((channel == Helper::et) || (channel == Helper::em)){
                            if (isMC) return 1.0f; // see https://cms-talk.web.cern.ch/t/pnoton-energy-corrections-in-nanoaod-v11/34327
                            else if (isEmbedded) {
                                if (abs(ele_eta) < 1.442) return (float) cset_egES->evaluate({"barrel", "nom"});
                                else                      return (float) cset_egES->evaluate({"endcap", "nom"});
                            }
                        }
                        return 1.0f;
                        }, {"channel", "isMC", "isEmbedded", "eta_1"})
                     .Define("p4_pair_et_withEleES", applyElectronEnergyScale_nominal, {"eleES", "p4_1_et_noEleES", "p4_met_et_noEleES"})
                     // Unpack
                     .Define("p4_1_et_withEleES",   [](std::vector<ROOT::Math::PtEtaPhiMVector> vec) { return vec[0]; }, {"p4_pair_et_withEleES"})
                     .Define("p4_met_et_withEleES", [](std::vector<ROOT::Math::PtEtaPhiMVector> vec) { return vec[1]; }, {"p4_pair_et_withEleES"})
        // emu
                     .Define("p4_1_em_noEleES", add_p4, {"pt_1", "eta_1", "phi_1", "m_1"})
                     .Define("p4_met_em_noEleES", add_p4, {"met", "zero", "metphi", "zero"})
                     .Define("p4_pair_em_withEleES", applyElectronEnergyScale_nominal, {"eleES", "p4_1_em_noEleES", "p4_met_em_noEleES"})
                     .Define("p4_1_em_withEleES",   [](std::vector<ROOT::Math::PtEtaPhiMVector> vec) { return vec[0]; }, {"p4_pair_em_withEleES"})
                     .Define("p4_met_em_withEleES", [](std::vector<ROOT::Math::PtEtaPhiMVector> vec) { return vec[1]; }, {"p4_pair_em_withEleES"})

            // unpack mutau
                      .Define("pt_1_et_withEleES",   "(float) p4_1_et_withEleES.Pt()")
                      .Define("m_1_et_withEleES",    "(float) p4_1_et_withEleES.M()")
                      .Define("met_et_withEleES",    "(float) p4_met_et_withEleES.Pt()")
                      .Define("metphi_et_withEleES", "(float) p4_met_et_withEleES.Phi()")
            // unpack emu
                      .Define("pt_1_em_withEleES",   "(float) p4_1_em_withEleES.Pt()")
                      .Define("m_1_em_withEleES",    "(float) p4_1_em_withEleES.M()")
                      .Define("met_em_withEleES",    "(float) p4_met_em_withEleES.Pt()")
                      .Define("metphi_em_withEleES", "(float) p4_met_em_withEleES.Phi()");

        if (sConfig.doSys()) {
            // note that we pass the uncorrected met
            // etau: shifts of the electron energy scale
            return df2.Define("eleES_up", [cset_egES] (const int channel, const int isMC, const int isEmbedded, const std::string era, const float eta, const int gain) {
                            if ((channel == Helper::et) || (channel == Helper::em)) {
                                if (isMC) return (float) cset_egES->evaluate({era, "scaleup", eta, gain});
                                else if (isEmbedded) {
                                    if (abs(eta) < 1.442) return (float) cset_egES->evaluate({"barrel", "up"});
                                    else                  return (float) cset_egES->evaluate({"endcap", "up"});
                                }
                            }
                            return 1.0f;
                        }, {"channel", "isMC", "isEmbedded", "era", "eta_1", "gain_1"})
                    .Define("eleES_down", [cset_egES] (const int channel, const int isMC, const int isEmbedded, const std::string era, const float eta, const int gain) {
                            if ((channel == Helper::et) || (channel == Helper::em)) {
                                if (isMC) return (float) cset_egES->evaluate({era, "scaledown", eta, gain});
                                else if (isEmbedded) {
                                    if (abs(eta) < 1.442) return (float) cset_egES->evaluate({"barrel", "down"});
                                    else                    return (float) cset_egES->evaluate({"endcap", "down"});
                                }
                            }
                            return 1.0f;
                        }, {"channel", "isMC", "isEmbedded", "era", "eta_1", "gain_1"})
                    .Define("p4_pair_et_withEleES_variations", applyElectronEnergyScale_variations, {"channel", "isMC", "isEmbedded", "year", "eleES_up", "eleES_down", "p4_1_et_noEleES", "p4_met_et_noEleES"})
                    // unpack
                    .Define("p4_1_et_withEleES_es1Up",     "p4_pair_et_withEleES_variations[0][0]")
                    .Define("p4_1_et_withEleES_es1Down",   "p4_pair_et_withEleES_variations[0][1]")
                    .Define("p4_met_et_withEleES_es1Up",   "p4_pair_et_withEleES_variations[1][0]")
                    .Define("p4_met_et_withEleES_es1Down", "p4_pair_et_withEleES_variations[1][1]")

                    // etau contd.: unpack shifts due to electron energy scale
                    .Define("pt_1_et_withEleES_es1Up",    "(float) p4_1_et_withEleES_es1Up.Pt()")
                    .Define("pt_1_et_withEleES_es1Down",  "(float) p4_1_et_withEleES_es1Down.Pt()")
                    .Define("m_1_et_withEleES_es1Up",    "(float) p4_1_et_withEleES_es1Up.M()")
                    .Define("m_1_et_withEleES_es1Down",  "(float) p4_1_et_withEleES_es1Down.M()")

                    .Define("met_et_withEleES_es1Up",    "(float) p4_met_et_withEleES_es1Up.Pt()")
                    .Define("metphi_et_withEleES_es1Up", "(float) p4_met_et_withEleES_es1Up.Phi()")
                    .Define("met_et_withEleES_es1Down",    "(float) p4_met_et_withEleES_es1Down.Pt()")
                    .Define("metphi_et_withEleES_es1Down", "(float) p4_met_et_withEleES_es1Down.Phi()")

                    // emu
                    .Define("p4_pair_em_withEleES_variations", applyElectronEnergyScale_variations, {"channel", "isMC", "isEmbedded", "year", "eleES_up", "eleES_down", "p4_1_em_noEleES", "p4_met_em_noEleES"})
                    // unpack
                    .Define("p4_1_em_withEleES_es1Up",     "p4_pair_em_withEleES_variations[0][0]")
                    .Define("p4_1_em_withEleES_es1Down",   "p4_pair_em_withEleES_variations[0][1]")
                    .Define("p4_met_em_withEleES_es1Up",   "p4_pair_em_withEleES_variations[1][0]")
                    .Define("p4_met_em_withEleES_es1Down", "p4_pair_em_withEleES_variations[1][1]")

                    // etau contd.: The next four branches are read into encapsulateVariedTauTau-experimental.h
                    .Define("pt_1_em_withEleES_es1Up",    "(float) p4_1_em_withEleES_es1Up.Pt()")
                    .Define("pt_1_em_withEleES_es1Down",  "(float) p4_1_em_withEleES_es1Down.Pt()")
                    .Define("m_1_em_withEleES_es1Up",    "(float) p4_1_em_withEleES_es1Up.M()")
                    .Define("m_1_em_withEleES_es1Down",  "(float) p4_1_em_withEleES_es1Down.M()")

                    // etau: shifts of the met/metphi due to the electron ES only
                    .Define("met_em_withEleES_es1Up",    "(float) p4_met_em_withEleES_es1Up.Pt()")
                    .Define("metphi_em_withEleES_es1Up", "(float) p4_met_em_withEleES_es1Up.Phi()")
                    .Define("met_em_withEleES_es1Down",    "(float) p4_met_em_withEleES_es1Down.Pt()")
                    .Define("metphi_em_withEleES_es1Down", "(float) p4_met_em_withEleES_es1Down.Phi()");


        }
        else {

            return df2;

        }
    }

    return df;


}

/******************************************************************************/

#endif
