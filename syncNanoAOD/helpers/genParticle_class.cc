#include "genParticle_class.h"

namespace GenHelper {


    genParticle_t::genParticle_t() {
        pdg_id = -999;
        gen_p4 = ROOT::Math::PtEtaPhiMVector(0, -999, -999, 0);
    }

    genParticle_t::genParticle_t(int pdgId, ROOT::Math::PtEtaPhiMVector p4) {
        pdg_id = pdgId;
        gen_p4 = p4;
    }

    /*
     * Compare pT of two particles
     */
    bool compareGenParticlePt(const genParticle_t& lhs, const genParticle_t& rhs) {
        return (lhs.genPt() > rhs.genPt());
    }
}
