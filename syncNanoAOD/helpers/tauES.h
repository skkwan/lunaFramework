// Helper functions for tau energy scale and e/mu faking tauh energy scales.

#ifndef TAU_ES_H_INCL
#define TAU_ES_H_INCL

#include "correction.h"
#include "computePhysicsVariables.h"
#include "fileIO.h"
#include "helperFunctions.h"
#include "sampleConfig_class.h"

#include "TFile.h"
#include "TH1F.h"
#include "TObject.h"
#include "Math/Vector4D.h"

#include <regex>

using correction::CorrectionSet;


/******************************************************************************/
/*
 * Return {corrected_tau_p4, corrected_met_p4} after applying the tau energy scale and shifts for genuine taus. The central value only
 * depends on the decay mode; (TO-DO) the up  /down shifts depends on the decay mode and pT bin (3 bins) for non-Embedded MC,
 * and the up/down shifts depend only on the decay mode for Embedded MC.
 */

std::vector<ROOT::Math::PtEtaPhiMVector> applyTauEnergyScale_nominal(float tauES, int tau_dm, ROOT::Math::PtEtaPhiMVector uncorrected_tau_p4, ROOT::Math::PtEtaPhiMVector uncorrected_met_p4) {


  std::vector<ROOT::Math::PtEtaPhiMVector> corrected_pair;


  ROOT::Math::PtEtaPhiMVector corrected_tau_p4 = (tauES * uncorrected_tau_p4);
  ROOT::Math::PtEtaPhiMVector corrected_met_p4 = (uncorrected_met_p4 - (corrected_tau_p4 - uncorrected_tau_p4));

  // DM = 0 (1-prong decay mode) set mass to charged pion: https://twiki.cern.ch/twiki/bin/viewauth/CMS/HiggsToTauTauWorkingLegacyRun2#Tau_ES
  if (tau_dm == 0) {
    corrected_tau_p4.SetM(Helper::piPMmass);
  }
  corrected_pair.push_back(corrected_tau_p4);
  corrected_pair.push_back(corrected_met_p4);

  return corrected_pair;
}


/*
 * Get the tau energy scale and shifts for genuine taus.
 * Note that we passed the UNCORRECTED p4's to this function.
 */

ROOT::RVec<ROOT::RVec<ROOT::Math::PtEtaPhiMVector>> applyTauEnergyScale_variations(int tau_dm, float tauES_up, float tauES_down,
                                                                                  ROOT::Math::PtEtaPhiMVector uncorrected_tau_p4,
                                                                                  ROOT::Math::PtEtaPhiMVector uncorrected_met_p4) {


  ROOT::Math::PtEtaPhiMVector corrected_tau_p4_up = (tauES_up * uncorrected_tau_p4);
  ROOT::Math::PtEtaPhiMVector corrected_met_p4_up = (uncorrected_met_p4 - (corrected_tau_p4_up - uncorrected_tau_p4));

  ROOT::Math::PtEtaPhiMVector corrected_tau_p4_down = (tauES_down * uncorrected_tau_p4);
  ROOT::Math::PtEtaPhiMVector corrected_met_p4_down = (uncorrected_met_p4 - (corrected_tau_p4_down - uncorrected_tau_p4));

  // DM = 0: set mass to pi0: https://twiki.cern.ch/twiki/bin/viewauth/CMS/HiggsToTauTauWorkingLegacyRun2#Tau_ES
  if (tau_dm == 0) {
    corrected_tau_p4_up.SetM(Helper::piPMmass);
    corrected_tau_p4_down.SetM(Helper::piPMmass);
  }

  return ROOT::RVec<ROOT::RVec<ROOT::Math::PtEtaPhiMVector>>{{corrected_tau_p4_up, corrected_tau_p4_down}, {corrected_met_p4_up, corrected_met_p4_down}};

}

/******************************************************************************/

// Get the tau energy scales, mu->tau energy scales, and e->tau energy scales.
// Defines new pT branches (shifts will be aliased to pt_2 later on).
// Assumes that the nominal correction was already applied.

// To evaluate up/down shifts in the tau energy scale, we only need to consider the MET central values.
// For the MET shifts we need to have aliased them already.

template <typename T>
auto GetTauEnergyScaleShifts(T &df, LUNA::sampleConfig_t &sConfig, TString jsonDir){

  std::cout << ">>> tauES.h: GetTauEnergyScaleShifts: doSys is " << sConfig.doSys() << std::endl;


  if (sConfig.isMC() || sConfig.isEmbedded()) {

    TString cset_dir;
    cset_dir.Form("%sPOG/TAU/%s_UL/tau.json", jsonDir.Data(), sConfig.era().c_str());
    std::cout << cset_dir << std::endl;
    auto cset = CorrectionSet::from_file(cset_dir.Data());
    auto cset_tauES = cset->at("tau_energy_scale");

    auto df2 = df.Define("tauES", [cset_tauES] (const int channel, const float pt, const float eta, const int dm, const unsigned int genmatch) {
                    if ((channel == Helper::mt) || (channel == Helper::et)) {
                      return (float) cset_tauES->evaluate({pt, eta, dm, (int) genmatch, "DeepTau2017v2p1", "nom"});
                    }
                    else {
                      return 1.0f;
                    }
                  }, {"channel", "pt_2", "eta_2", "decayMode_2", "gen_match_2"})
                 .Define("p4_2_noTauES", add_p4, {"pt_2", "eta_2", "phi_2", "m_2"})
                // mutau: tau ES: MUST follow muonES application: muonES will shift the met
                 .Define("p4_met_mt_withMuonES_noTauES", add_p4, {"met_mt_withMuonES", "zero", "metphi_mt_withMuonES", "zero"})
                 .Define("p4_pair_mt_withTauES", applyTauEnergyScale_nominal, {"tauES", "decayMode_2", "p4_2_noTauES", "p4_met_mt_withMuonES_noTauES"})
                 // mutau: Unpack the nominal 4-vectors for mutau
                 .Define("p4_2_mt_withTauES",              [](std::vector<ROOT::Math::PtEtaPhiMVector> vec) { return vec[0]; }, {"p4_pair_mt_withTauES"})
                 .Define("p4_met_mt_withMuonES_withTauES", [](std::vector<ROOT::Math::PtEtaPhiMVector> vec) { return vec[1]; }, {"p4_pair_mt_withTauES"})

                 // etau: tauES: MUST follow eleES appliction: eleES will shift the met. Note the "_et_" (etau)
                 .Define("p4_met_et_withEleES_noTauES", add_p4, {"met_et_withEleES", "zero", "metphi_et_withEleES", "zero"})
                 .Define("p4_pair_et_withTauES", applyTauEnergyScale_nominal, {"tauES", "decayMode_2", "p4_2_noTauES", "p4_met_et_withEleES_noTauES"})
                 // etau: Unpack the nominal 4-vectors for etau
                 .Define("p4_2_et_withTauES",             [](std::vector<ROOT::Math::PtEtaPhiMVector> vec) { return vec[0]; }, {"p4_pair_et_withTauES"})
                 .Define("p4_met_et_withEleES_withTauES", [](std::vector<ROOT::Math::PtEtaPhiMVector> vec) { return vec[1]; }, {"p4_pair_et_withTauES"})

                  // fully unpack four-vectors
                .Define("pt_2_mt_withTauES", [](ROOT::Math::PtEtaPhiMVector p4) { return (float) p4.Pt(); }, {"p4_2_mt_withTauES"})
                .Define("m_2_mt_withTauES",  [](ROOT::Math::PtEtaPhiMVector p4) { return (float) p4.M(); }, {"p4_2_mt_withTauES"})
                .Define("met_mt_withMuonES_withTauES",    [](ROOT::Math::PtEtaPhiMVector p4) { return (float) p4.Pt(); }, {"p4_met_mt_withMuonES_withTauES"})
                .Define("metphi_mt_withMuonES_withTauES", [](ROOT::Math::PtEtaPhiMVector p4) { return (float) p4.Phi(); }, {"p4_met_mt_withMuonES_withTauES"})
                // fully unpack four-vectors for etau
                .Define("pt_2_et_withTauES", [](ROOT::Math::PtEtaPhiMVector p4) { return (float) p4.Pt(); }, {"p4_2_et_withTauES"})
                .Define("m_2_et_withTauES",  [](ROOT::Math::PtEtaPhiMVector p4) { return (float) p4.M(); }, {"p4_2_et_withTauES"})
                .Define("met_et_withEleES_withTauES",    [](ROOT::Math::PtEtaPhiMVector p4) { return (float) p4.Pt(); }, {"p4_met_et_withEleES_withTauES"})
                .Define("metphi_et_withEleES_withTauES", [](ROOT::Math::PtEtaPhiMVector p4) { return (float) p4.Phi(); },  {"p4_met_et_withEleES_withTauES"});

    if (sConfig.doSys()) {

      return df2.Define("dm0", [] { return (int) 0; })
                .Define("dm1", [] { return (int) 1; })
                .Define("dm10", [] { return (int) 10; })
                .Define("dm11", [] { return (int) 11; })
                .Define("tauES_up", [cset_tauES] (const int channel, const float pt, const float eta, const int dm, const unsigned int genmatch) {
                    if ((channel == Helper::mt) || (channel == Helper::et)) {
                      return (float) cset_tauES->evaluate({pt, eta, dm, (int) genmatch, "DeepTau2017v2p1", "up"});
                    }
                    else {
                      return 1.0f;
                    }
                  }, {"channel", "pt_2", "eta_2", "decayMode_2", "gen_match_2"})
                .Define("tauES_down", [cset_tauES] (const int channel, const float pt, const float eta, const int dm, const unsigned int genmatch) {
                    if ((channel == Helper::mt) || (channel == Helper::et)) {
                      return (float) cset_tauES->evaluate({pt, eta, dm, (int) genmatch, "DeepTau2017v2p1", "down"});
                    }
                    else {
                      return 1.0f;
                    }
                  }, {"channel", "pt_2", "eta_2", "decayMode_2", "gen_match_2"})
                // mutau: shifts due to the tau energy scale up/down, using the nominal muon ES
                .Define("p4_pair_mt_withMuonES_withTauES_variations", applyTauEnergyScale_variations, {"decayMode_2", "tauES_up", "tauES_down", "p4_2_noTauES", "p4_met_mt_withMuonES_noTauES"})
                // unpack Shifts due to the tau energy scale up/down
                .Define("pt_2_mt_withMuonES_withTauES_es2Up",     "(float) p4_pair_mt_withMuonES_withTauES_variations[0][0].Pt()")
                .Define("m_2_mt_withMuonES_withTauES_es2Up",      "(float) p4_pair_mt_withMuonES_withTauES_variations[0][0].M()")
                .Define("pt_2_mt_withMuonES_withTauES_es2Down",   "(float) p4_pair_mt_withMuonES_withTauES_variations[0][1].Pt()")
                .Define("m_2_mt_withMuonES_withTauES_es2Down",    "(float) p4_pair_mt_withMuonES_withTauES_variations[0][1].M()")
                .Define("met_mt_withMuonES_withTauES_es2Up",      "(float) p4_pair_mt_withMuonES_withTauES_variations[1][0].Pt()")
                .Define("metphi_mt_withMuonES_withTauES_es2Up",   "(float) p4_pair_mt_withMuonES_withTauES_variations[1][0].Phi()")
                .Define("met_mt_withMuonES_withTauES_es2Down",    "(float) p4_pair_mt_withMuonES_withTauES_variations[1][1].Pt()")
                .Define("metphi_mt_withMuonES_withTauES_es2Down", "(float) p4_pair_mt_withMuonES_withTauES_variations[1][1].Phi()")

                // mutau cont.d: Shifts due to the muon energy scale up and down
                .Define("p4_pair_mt_withMuonES_withTauES_es1Up",   applyTauEnergyScale_nominal, {"tauES", "decayMode_2", "p4_2_noTauES", "p4_met_mt_withMuonES_es1Up"})
                .Define("p4_pair_mt_withMuonES_withTauES_es1Down", applyTauEnergyScale_nominal, {"tauES", "decayMode_2", "p4_2_noTauES", "p4_met_mt_withMuonES_es1Down"})
                // unpack the above results: Shifts due to muon energy scale up and down
                // unpacking pt_2 and m_2 is probably superfluous because those will be the nominal values, but to be pedantic, here it is
                .Define("pt_2_mt_withMuonES_withTauES_es1Up",   "(float) p4_pair_mt_withMuonES_withTauES_es1Up[0].Pt()")
                .Define("m_2_mt_withMuonES_withTauES_es1Up",    "(float) p4_pair_mt_withMuonES_withTauES_es1Up[0].M()")
                .Define("met_mt_withMuonES_withTauES_es1Up",    "(float) p4_pair_mt_withMuonES_withTauES_es1Up[1].Pt()")
                .Define("metphi_mt_withMuonES_withTauES_es1Up", "(float) p4_pair_mt_withMuonES_withTauES_es1Up[1].Phi()")

                .Define("pt_2_mt_withMuonES_withTauES_es1Down",   "(float) p4_pair_mt_withMuonES_withTauES_es1Down[0].Pt()")
                .Define("m_2_mt_withMuonES_withTauES_es1Down",    "(float) p4_pair_mt_withMuonES_withTauES_es1Down[0].M()")
                .Define("met_mt_withMuonES_withTauES_es1Down",    "(float) p4_pair_mt_withMuonES_withTauES_es1Down[1].Pt()")
                .Define("metphi_mt_withMuonES_withTauES_es1Down", "(float) p4_pair_mt_withMuonES_withTauES_es1Down[1].Phi()")


                // etau: again note that the non-corrected muon and met p4's are passed as arguments
                // Shifts due to the tau energy scale up/down
                .Define("p4_pair_et_withTauES_variations", applyTauEnergyScale_variations, {"decayMode_2", "tauES_up", "tauES_down", "p4_2_noTauES", "p4_met_et_withEleES_noTauES"})
                .Define("pt_2_et_withEleES_withTauES_es2Up",    "(float) p4_pair_et_withTauES_variations[0][0].Pt()")
                .Define("m_2_et_withEleES_withTauES_es2Up",     "(float) p4_pair_et_withTauES_variations[0][0].M()")
                .Define("pt_2_et_withEleES_withTauES_es2Down",  "(float) p4_pair_et_withTauES_variations[0][1].Pt()")
                .Define("m_2_et_withEleES_withTauES_es2Down",   "(float) p4_pair_et_withTauES_variations[0][1].M()")

                .Define("met_et_withEleES_withTauES_es2Up",      "(float) p4_pair_et_withTauES_variations[1][0].Pt()")
                .Define("metphi_et_withEleES_withTauES_es2Up",   "(float) p4_pair_et_withTauES_variations[1][0].Phi()")
                .Define("met_et_withEleES_withTauES_es2Down",    "(float) p4_pair_et_withTauES_variations[1][1].Pt()")
                .Define("metphi_et_withEleES_withTauES_es2Down", "(float) p4_pair_et_withTauES_variations[1][1].Phi()")

                // etau cont.d: Shifts due to the electron energy scale up/down
                .Define("p4_pair_et_withEleES_withTauES_es1Up",   applyTauEnergyScale_nominal, {"tauES", "decayMode_2", "p4_2_noTauES", "p4_met_et_withEleES_es1Up"})
                .Define("p4_pair_et_withEleES_withTauES_es1Down", applyTauEnergyScale_nominal, {"tauES", "decayMode_2", "p4_2_noTauES", "p4_met_et_withEleES_es1Down"})
                // unpack shifts due to the electron energy scale up/down
                .Define("pt_2_et_withEleES_withTauES_es1Up",   "(float) p4_pair_et_withEleES_withTauES_es1Up[0].Pt()")
                .Define("m_2_et_withEleES_withTauES_es1Up",    "(float) p4_pair_et_withEleES_withTauES_es1Up[0].M()")
                .Define("met_et_withEleES_withTauES_es1Up",    "(float) p4_pair_et_withEleES_withTauES_es1Up[1].Pt()")
                .Define("metphi_et_withEleES_withTauES_es1Up", "(float) p4_pair_et_withEleES_withTauES_es1Up[1].Phi()")

                .Define("pt_2_et_withEleES_withTauES_es1Down",   "(float) p4_pair_et_withEleES_withTauES_es1Down[0].Pt()")
                .Define("m_2_et_withEleES_withTauES_es1Down",    "(float) p4_pair_et_withEleES_withTauES_es1Down[0].M()")
                .Define("met_et_withEleES_withTauES_es1Down",    "(float) p4_pair_et_withEleES_withTauES_es1Down[1].Pt()")
                .Define("metphi_et_withEleES_withTauES_es1Down", "(float) p4_pair_et_withEleES_withTauES_es1Down[1].Phi()");

    }
    else {
      return df2;
    }
  }
  else {
    return df;
  }

}

/******************************************************************************/

#endif
