#ifndef LOOSE_SELECTION_OFFLINE_OBJECTS_H_INCL
#define LOOSE_SELECTION_OFFLINE_OBJECTS_H_INCL

#include "fileIO.h"
#include "helperFunctions.h"

/**********************************************************/


/* Check if event has loose baseline objects. Taken from the channels under https://twiki.cern.ch/twiki/bin/viewauth/CMS/HiggsToTauTauWorkingLegacyRun2#Baseline_selection. */

ROOT::RVec<int> findLooseOfflineMuons_mutauh(ROOT::RVec<int>& isCleanedMuon,
                                             ROOT::RVec<float>& Muon_pt, ROOT::RVec<float>& Muon_eta,
                                             ROOT::RVec<float>& Muon_dz, ROOT::RVec<float>& Muon_dxy,
                                             ROOT::RVec<bool>& Muon_mediumId, ROOT::RVec<float>& Muon_pfRelIso04_all) {
                                               return isCleanedMuon && (Muon_pt > 19) && (abs(Muon_eta) < 2.4) && (Muon_mediumId == true) && (abs(Muon_dz) < 0.2) && (abs(Muon_dxy) < 0.045) && (Muon_pfRelIso04_all < 0.15);
                                             }

// Same loose selection for mutauh and etauh
ROOT::RVec<int> findLooseOfflineTaus_mutauh_and_etauh(ROOT::RVec<int>& isCleanedTau,
                                                      ROOT::RVec<float>& Tau_pt, ROOT::RVec<float>& Tau_eta,
                                                      ROOT::RVec<float>& Tau_dz,
                                                      ROOT::RVec<unsigned char>& Tau_idDeepTau2017v2p1VSe,
                                                      ROOT::RVec<unsigned char>& Tau_idDeepTau2017v2p1VSmu,
                                                      ROOT::RVec<unsigned char>& Tau_idDeepTau2017v2p1VSjet,
                                                      ROOT::RVec<int>& Tau_decayMode) {
                                        return  isCleanedTau && (Tau_pt > 18 && abs(Tau_eta) < 2.3 && abs(Tau_dz) < 0.2 && (Tau_idDeepTau2017v2p1VSe & 0x04) && (Tau_idDeepTau2017v2p1VSmu & 0x01) && (Tau_idDeepTau2017v2p1VSjet & 0x01)
                                                && Tau_decayMode != 5 && Tau_decayMode != 6);
                                      }

ROOT::RVec<int> findLooseOfflineElectrons_etauh(ROOT::RVec<int>& isCleanedEle,
                                                ROOT::RVec<float>& Electron_pt, ROOT::RVec<float>& Electron_eta,
                                                ROOT::RVec<float>& Electron_dz, ROOT::RVec<float>& Electron_dxy,
                                                ROOT::RVec<bool>& Electron_mvaFall17V2noIso_WP90,  ROOT::RVec<unsigned char>& Electron_lostHits, ROOT::RVec<bool>& Electron_convVeto,
                                                ROOT::RVec<float>& Electron_pfRelIso03_all) {
                                                  return isCleanedEle && (abs(Electron_eta) < 2.1) && (Electron_pt > 20) && (abs(Electron_dz) < 0.2) && (abs(Electron_dxy) < 0.045) &&
                                                          Electron_mvaFall17V2noIso_WP90 && (Electron_lostHits <= 1) && (Electron_convVeto) && (Electron_pfRelIso03_all < 0.15);
                                                }

// For the emu channel, looser pT and iso baseline selections
ROOT::RVec<int> findLooseOfflineElectrons_emu(ROOT::RVec<int>& isCleanedEle,
                                              ROOT::RVec<float>& Electron_pt, ROOT::RVec<float>& Electron_eta,
                                              ROOT::RVec<float>& Electron_dz, ROOT::RVec<float>& Electron_dxy,
                                              ROOT::RVec<bool>& Electron_mvaFall17V2noIso_WP90,  ROOT::RVec<unsigned char>& Electron_lostHits, ROOT::RVec<bool>& Electron_convVeto,
                                              ROOT::RVec<float>& Electron_pfRelIso03_all) {
                                                  return isCleanedEle && (abs(Electron_eta) < 2.4) && (Electron_pt > 12) && (abs(Electron_dz) < 0.2) && (abs(Electron_dxy) < 0.045) &&
                                                          Electron_mvaFall17V2noIso_WP90 && (Electron_lostHits <= 1) && (Electron_convVeto) && (Electron_pfRelIso03_all < 0.3);
                                              }

ROOT::RVec<int> findLooseOfflineMuons_emu(ROOT::RVec<int>& isCleanedMuon,
                                          ROOT::RVec<float>& Muon_pt, ROOT::RVec<float>& Muon_eta,
                                          ROOT::RVec<float>& Muon_dz, ROOT::RVec<float>& Muon_dxy,
                                          ROOT::RVec<bool>& Muon_mediumId, ROOT::RVec<float>& Muon_pfRelIso04_all) {
                                            return isCleanedMuon && (abs(Muon_eta) < 2.4) && (Muon_pt > 12) && (Muon_mediumId == true) && (abs(Muon_dz) < 0.2) && (abs(Muon_dxy) < 0.045) && (Muon_pfRelIso04_all < 0.3);
                                          }


bool has_loose_baseline_objects(int channel,
                                unsigned int nMuon, unsigned int nTau, unsigned int nElectron,
                                ROOT::RVec<int>& looseOfflineMuons_mutauh, ROOT::RVec<int>& looseOfflineTaus_mutauh_and_etauh,
                                ROOT::RVec<int>& looseOfflineElectrons_etauh,
                                ROOT::RVec<int>& looseOfflineElectrons_emu, ROOT::RVec<int>& looseOfflineMuons_emu
                                ) {

                                if (channel == Helper::mt) {
                                    return ((nMuon > 0) && (nTau > 0) && (Sum(looseOfflineMuons_mutauh) > 0) && (Sum(looseOfflineTaus_mutauh_and_etauh) > 0));
                                }
                                else if (channel == Helper::et) {
                                  return ((nElectron > 0) && (nTau > 0) && (Sum(looseOfflineElectrons_etauh) > 0 ) && (Sum(looseOfflineTaus_mutauh_and_etauh) > 0));
                                }
                                else if (channel == Helper::em) {
                                  return ((nElectron > 0) && (nMuon > 0) && (Sum(looseOfflineElectrons_emu) > 0) && (Sum(looseOfflineMuons_emu) > 0));
                                }
                                return false;
                                }

/**********************************************************/

template <typename T>
auto FilterLooseBaselineObjects(T &df) {

  return df.Define("goodMuons_mutauh", findLooseOfflineMuons_mutauh, {"goodVeryLooseMuons", "Muon_pt", "Muon_eta", "Muon_dz", "Muon_dxy", "Muon_mediumId", "Muon_pfRelIso04_all"})
           .Define("goodTaus_mutauh_and_etauh", findLooseOfflineTaus_mutauh_and_etauh, {"cleaned_tau_from_mu_ele", "Tau_pt", "Tau_eta", "Tau_dz",
                                                                                        "Tau_idDeepTau2017v2p1VSe", "Tau_idDeepTau2017v2p1VSmu", "Tau_idDeepTau2017v2p1VSjet", "Tau_decayMode"})
           .Define("goodElectrons_etauh", findLooseOfflineElectrons_etauh, {"cleaned_ele_from_mu", "Electron_pt", "Electron_eta", "Electron_dz", "Electron_dxy",
                                                                            "Electron_mvaFall17V2noIso_WP90", "Electron_lostHits", "Electron_convVeto", "Electron_pfRelIso03_all"})
           .Define("goodElectrons_emu", findLooseOfflineElectrons_emu, {"cleaned_ele_from_mu", "Electron_pt", "Electron_eta", "Electron_dz", "Electron_dxy",
                                                                        "Electron_mvaFall17V2noIso_WP90", "Electron_lostHits", "Electron_convVeto", "Electron_pfRelIso03_all"})
           .Define("goodMuons_emu", findLooseOfflineMuons_emu,  {"goodVeryLooseMuons", "Muon_pt", "Muon_eta", "Muon_dz", "Muon_dxy", "Muon_mediumId", "Muon_pfRelIso04_all"})
    .Filter("channel != 3", "looseSelectionOfflineObjects.h: Event must belong to one of the channels (!= 3)")
    .Filter(has_loose_baseline_objects, {"channel", "nMuon", "nTau", "nElectron",
                                        "goodMuons_mutauh", "goodTaus_mutauh_and_etauh", "goodElectrons_etauh", "goodElectrons_emu", "goodMuons_emu"},
                                        "looseSelectionOfflineObjects.h: Has offline objects passing loose selection for this channel");

}

/*************************************************************/


#endif
