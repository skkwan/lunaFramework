#ifndef GEN_CASCADE_CLASS_H_INCL
#define GEN_CASCADE_CLASS_H_INCL

#include <ROOT/RVec.hxx>
#include <Math/Vector4D.h>
#include "genHelper.h"
#include "genParticle_class.h"

namespace GenHelper {

    const unsigned int nLegsInCascade = 6;

    /*
     * Class to represent h3->a1a2, where the first a1 is just "h1", and the second and third a1 from
     * a2->a1a1 are "h1_cas1" and "h1_cas2".
     */
    class genCascade_t {
        private:

            // vector of pairs of genParticle_t legs
            std::vector<std::vector<genParticle_t>> vv_legs_bb;
            std::vector<std::vector<genParticle_t>> vv_legs_tautau;

            // Number of gen-level bb pairs
            int n_gen_bb_pair;

        public:

            int n_gen_bb_pairs() { return n_gen_bb_pair; }

            // Default, null constructor
            genCascade_t();

            // Constructor from a vector of provided indices
            genCascade_t(std::vector<int> genPart_idx_legs,
                         ROOT::VecOps::RVec<int>& GenPart_pdgId,
                         ROOT::VecOps::RVec<int>& GenPart_genPartIdxMother,
                         ROOT::VecOps::RVec<int>& GenPart_statusFlags,
                         ROOT::VecOps::RVec<float>& GenPart_pt,
                         ROOT::VecOps::RVec<float>& GenPart_eta,
                         ROOT::VecOps::RVec<float>& GenPart_phi,
                         ROOT::VecOps::RVec<float>& GenPart_mass);

            // Get the P4 of the legs (if not available, the 4-vector is (-999, -999, -999, -999))
            ROOT::Math::PtEtaPhiMVector genP4_tau1();
            ROOT::Math::PtEtaPhiMVector genP4_tau2();
            ROOT::Math::PtEtaPhiMVector genP4_b1();
            ROOT::Math::PtEtaPhiMVector genP4_b2();
            ROOT::Math::PtEtaPhiMVector genP4_trailing1();
            ROOT::Math::PtEtaPhiMVector genP4_trailing2();

    };

}

#endif
