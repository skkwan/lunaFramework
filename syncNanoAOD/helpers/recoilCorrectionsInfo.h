// Helper functions for the recoil correction information at sync/skim-level.
#ifndef RECOIL_CORRECTIONS_INFO_H_INCL
#define RECOIL_CORRECTIONS_INFO_H_INCL


#include "computePhysicsVariables.h"
#include "fileIO.h"
#include "genHelper.h"
#include "helperFunctions.h"
#include "sampleConfig_class.h"

/***************************************************************************/

// Count jets passing veto and deltaR separated from leading tau legs
float get_nRecoilJets(unsigned int nJet,
        ROOT::RVec<int>& jets, // already pre-selected kinematics (pT, eta, loose pileup ID)
        float deltaR_separation,
        ROOT::RVec<float>& Jet_eta, ROOT::RVec<float>& Jet_phi,
        float eta_1, float phi_1,
        float eta_2, float phi_2,
        int isWJets) {

  // Count number of jets passing deltaR separation
  float nRecoilJets = 0;

  for (size_t i = 0; i < nJet; i++) {
    if (jets[i] == 1) {  // if passing pre-selected kinematics
const auto deltar1 = Helper::DeltaR(Jet_eta[i], eta_1,
            Jet_phi[i], phi_1);
const auto deltar2 = Helper::DeltaR(Jet_eta[i], eta_2,
            Jet_phi[i], phi_2);
if (deltar1 > deltaR_separation && deltar2 > deltaR_separation) {
  nRecoilJets += 1;
}
    }
  }
  // std::cout << ">>> recoilCorrectionsInfo.h: get_nRecoilJets: " << nPassVeto << std::endl;

  if (isWJets) {
    nRecoilJets += 1;
  }
  return nRecoilJets;
}

/***************************************************************************/

/*
 * Get gen-level recoil boson information for recoil corrections.
 * - gen pT of W/Z/Higgs
 * - gen vis pT of W/Z/Higgs
 */

template <typename T>
auto GetGenRecoilBoson(T &df, LUNA::sampleConfig_t &sConfig, std::string configDir) {

  std::string year = std::to_string(sConfig.year());

  //--------------------------------------------
  // Define function that we will use to get the gen boson pT and gen boson visible pT
  //--------------------------------------------
  auto find_gen_recoil_boson_p4 = [](unsigned long long event,
					       unsigned int nGenPart,   // number of gen particles (NanoAOD branch)
					       ROOT::RVec<int>&   GenPart_pdgId,
                 ROOT::RVec<int>&   GenPart_status,
					       ROOT::RVec<int>&   GenPart_statusFlags,
					       ROOT::RVec<float>& GenPart_pt,
					       ROOT::RVec<float>& GenPart_eta,
					       ROOT::RVec<float>& GenPart_phi,
					       ROOT::RVec<float>& GenPart_mass) {

    (void) event;  // unless I need it for debugging

    std::vector<ROOT::Math::PtEtaPhiMVector> output;
    ROOT::Math::PtEtaPhiMVector p4vis;
    ROOT::Math::PtEtaPhiMVector p4tot;

    // Find all particles which are the last copy, and have the gen boson as a parent
    for (unsigned int i = 0; i < nGenPart; i++) {
      int pdgId      = GenPart_pdgId[i];
      int status     = GenPart_status[i];
      int statusFlag = GenPart_statusFlags[i];

      bool fromHardProcessFinalState = GenHelper::fromHardProcessFinalState(status, statusFlag);
      bool isDirectHardProcessTauDecayProduct = GenHelper::isDirectHardProcessTauDecayProduct(statusFlag);
      bool isMuon     = GenHelper::isMuon(pdgId);
      bool isElectron = GenHelper::isElectron(pdgId);
      bool isNeutrino = GenHelper::isNeutrino(pdgId);

      if ((fromHardProcessFinalState && (isMuon || isElectron || isNeutrino)) || isDirectHardProcessTauDecayProduct) {
        p4tot += ROOT::Math::PtEtaPhiMVector(GenPart_pt[i], GenPart_eta[i], GenPart_phi[i], GenPart_mass[i]);
      }
      if ((fromHardProcessFinalState && (isMuon || isElectron)) || (isDirectHardProcessTauDecayProduct && !isNeutrino)) {
        p4vis += ROOT::Math::PtEtaPhiMVector(GenPart_pt[i], GenPart_eta[i], GenPart_phi[i], GenPart_mass[i]);
      }
    }

    output.push_back(p4tot);
    output.push_back(p4vis);

    // std::cout << ">>> recoilCorrectionsInfo.h: find_gen_recoil_boson_p4: event " << event
    //           << " gen px/py: " << output[0].Px() << ", " << output[0].Py()
    //           << " gen vis px/py: " << output[1].Px() << ", " << output[1].Py() << std::endl;


    // std::cout << ">>> recoilCorrectionsInfo.h: find_gen_recoil_boson_p4: event " << event
    //           << " gen pt/eta/phi/m "    << output[0].Pt() << ", " << output[0].Eta() << ", " << output[0].Phi() << ", " << output[0].M()
    //           << " gen vis pt/eta/phi/m " << output[1].Pt() << ", " << output[1].Eta() << ", " << output[1].Phi() << ", " << output[1].M() << std::endl;

    return output;
  };


  //--------------------------------------------//
  // Main body of function: n.b. the "do recoil" condition is also coded separately in metRecoilCorrections.h
  // which isn't ideal but..
  //--------------------------------------------//

  std::cout << ">>> recoilCorrectionsInfo.h: get gen recoil boson information (do recoil)? " << sConfig.doRecoil() << std::endl;

  // Do recoil
  if (sConfig.doRecoil()) {
    int isWJets = sConfig.isWJets();

    // Get recoil boson gen pT and gen vis pT
    auto df2 = df.Define("gen_recoil_boson_pT_visPt", find_gen_recoil_boson_p4,
			    {"event", "nGenPart",
			     "GenPart_pdgId", "GenPart_status", "GenPart_statusFlags",
			     "GenPart_pt", "GenPart_eta", "GenPart_phi", "GenPart_mass"})
                 .Define("gen_recoilBoson_Px",    "(float) gen_recoil_boson_pT_visPt[0].Px()")
                 .Define("gen_recoilBoson_Py",    "(float) gen_recoil_boson_pT_visPt[0].Py()")
                 .Define("gen_recoilBoson_M",     "(float) gen_recoil_boson_pT_visPt[0].M()")
                 .Define("gen_recoilBoson_Pt",    "(float) gen_recoil_boson_pT_visPt[0].Pt()")
                 .Define("gen_recoilBoson_visPx", "(float) gen_recoil_boson_pT_visPt[1].Px()")
                 .Define("gen_recoilBoson_visPy", "(float) gen_recoil_boson_pT_visPt[1].Py()")
                 .Define("isWJets", [=]() { return isWJets; });

    std::string selection = getString(configDir + "jetsVeto30KinematicsCuts.txt");
    //  "(Jet_pt > 30) && (abs(Jet_eta) < 4.7) && ((Jet_jetId >> 2) & 0b1) && ((Jet_pt >= 50) || ((Jet_pt < 50) && (Jet_puId > 3)))";

    int hasJERcorrections = HasExactBranch(df, "Jet_pt_nom");
    std::cout << ">>> recoilCorrections.h: Has JER? " << hasJERcorrections << std::endl;
    std::string sel_jerUp   = selection;
    std::string sel_jerDown = selection;
    std::string sel_jesAbsoluteUp   = selection;
    std::string sel_jesAbsoluteDown = selection;
    std::string sel_jesAbsoluteyearUp   = selection;
    std::string sel_jesAbsoluteyearDown = selection;
    std::string sel_jesBBEC1Up   = selection;
    std::string sel_jesBBEC1Down = selection;
    std::string sel_jesBBEC1yearUp   = selection;
    std::string sel_jesBBEC1yearDown = selection;
    std::string sel_jesEC2Up   = selection;
    std::string sel_jesEC2Down = selection;
    std::string sel_jesEC2yearUp   = selection;
    std::string sel_jesEC2yearDown = selection;
    std::string sel_jesFlavorQCDUp   = selection;
    std::string sel_jesFlavorQCDDown = selection;
    std::string sel_jesHFUp   = selection;
    std::string sel_jesHFDown = selection;
    std::string sel_jesHFyearUp   = selection;
    std::string sel_jesHFyearDown = selection;
    std::string sel_jesRelativeBalUp   = selection;
    std::string sel_jesRelativeBalDown = selection;
    std::string sel_jesRelativeSampleUp   = selection;
    std::string sel_jesRelativeSampleDown = selection;

    Helper::replaceStringInPlace(sel_jerUp,   "Jet_pt", "Jet_pt_jerUp");
    Helper::replaceStringInPlace(sel_jerDown, "Jet_pt", "Jet_pt_jerDown");
    Helper::replaceStringInPlace(sel_jesAbsoluteUp,   "Jet_pt", "Jet_pt_jesAbsoluteUp");
    Helper::replaceStringInPlace(sel_jesAbsoluteDown, "Jet_pt", "Jet_pt_jesAbsoluteDown");
    Helper::replaceStringInPlace(sel_jesAbsoluteyearUp,   "Jet_pt", "Jet_pt_jesAbsolute_" + year + "Up");
    Helper::replaceStringInPlace(sel_jesAbsoluteyearDown, "Jet_pt", "Jet_pt_jesAbsolute_" + year + "Down");
    Helper::replaceStringInPlace(sel_jesBBEC1Up,   "Jet_pt", "Jet_pt_jesBBEC1Up");
    Helper::replaceStringInPlace(sel_jesBBEC1Down, "Jet_pt", "Jet_pt_jesBBEC1Down");
    Helper::replaceStringInPlace(sel_jesBBEC1yearUp,   "Jet_pt", "Jet_pt_jesBBEC1_" + year + "Up");
    Helper::replaceStringInPlace(sel_jesBBEC1yearDown, "Jet_pt", "Jet_pt_jesBBEC1_" + year + "Down");
    Helper::replaceStringInPlace(sel_jesEC2Up,   "Jet_pt", "Jet_pt_jesEC2Up");
    Helper::replaceStringInPlace(sel_jesEC2Down, "Jet_pt", "Jet_pt_jesEC2Down");
    Helper::replaceStringInPlace(sel_jesEC2yearUp,   "Jet_pt", "Jet_pt_jesEC2_" + year + "Up");
    Helper::replaceStringInPlace(sel_jesEC2yearDown, "Jet_pt", "Jet_pt_jesEC2_" + year + "Down");
    Helper::replaceStringInPlace(sel_jesFlavorQCDUp,   "Jet_pt", "Jet_pt_jesFlavorQCDUp");
    Helper::replaceStringInPlace(sel_jesFlavorQCDDown, "Jet_pt", "Jet_pt_jesFlavorQCDDown");
    Helper::replaceStringInPlace(sel_jesHFUp,   "Jet_pt", "Jet_pt_jesHFUp");
    Helper::replaceStringInPlace(sel_jesHFDown, "Jet_pt", "Jet_pt_jesHFDown");
    Helper::replaceStringInPlace(sel_jesHFyearUp,   "Jet_pt", "Jet_pt_jesHF_" + year + "Up");
    Helper::replaceStringInPlace(sel_jesHFyearDown, "Jet_pt", "Jet_pt_jesHF_" + year + "Down");
    Helper::replaceStringInPlace(sel_jesRelativeBalUp,   "Jet_pt", "Jet_pt_jesRelativeBalUp");
    Helper::replaceStringInPlace(sel_jesRelativeBalDown, "Jet_pt", "Jet_pt_jesRelativeBalDown");
    Helper::replaceStringInPlace(sel_jesRelativeSampleUp,   "Jet_pt", "Jet_pt_jesRelativeSample_" + year + "Up");  // Even though "Year" isn't in the name of the branch it seems to depend on year
    Helper::replaceStringInPlace(sel_jesRelativeSampleDown, "Jet_pt", "Jet_pt_jesRelativeSample_" + year + "Down");

    std::cout << ">>> selection up: " << sel_jerUp << std::endl;

    // in selected W+Jets events one of the leptons is faked by hadronic jet and this jet should be counted as a part of hadronic recoil to the W boson. Therefore, when processing W+Jets MC sample, increase the number of jets, passed to the recoil corrector, by one.
    auto df3 = df2.Define("jetsVeto30", selection)
              .Define("deltaR_for_jetsVeto30", [=]() { return (float) 0.5; })  // deltaR between jet and either tautau leg
              .Define("nRecoilJets", get_nRecoilJets, {"nJet", "jetsVeto30", "deltaR_for_jetsVeto30", "Jet_eta", "Jet_phi", "eta_1", "phi_1", "eta_2", "phi_2", "isWJets"});
    if (hasJERcorrections) {

      return df3.Define("jetsVeto30_jerUp",   sel_jerUp)
                .Define("jetsVeto30_jerDown", sel_jerDown)
                .Define("nRecoilJets_jerUp",   get_nRecoilJets, {"nJet", "jetsVeto30_jerUp",   "deltaR_for_jetsVeto30", "Jet_eta", "Jet_phi", "eta_1", "phi_1", "eta_2", "phi_2", "isWJets"})
                .Define("nRecoilJets_jerDown", get_nRecoilJets, {"nJet", "jetsVeto30_jerDown", "deltaR_for_jetsVeto30", "Jet_eta", "Jet_phi", "eta_1", "phi_1", "eta_2", "phi_2", "isWJets"})
                .Define("jetsVeto30_jesAbsoluteUp",   sel_jesAbsoluteUp)
                .Define("jetsVeto30_jesAbsoluteDown", sel_jesAbsoluteDown)
                .Define("nRecoilJets_jesAbsoluteUp",   get_nRecoilJets, {"nJet", "jetsVeto30_jesAbsoluteUp",   "deltaR_for_jetsVeto30", "Jet_eta", "Jet_phi", "eta_1", "phi_1", "eta_2", "phi_2", "isWJets"})
                .Define("nRecoilJets_jesAbsoluteDown", get_nRecoilJets, {"nJet", "jetsVeto30_jesAbsoluteDown", "deltaR_for_jetsVeto30", "Jet_eta", "Jet_phi", "eta_1", "phi_1", "eta_2", "phi_2", "isWJets"})
                .Define("jetsVeto30_jesAbsoluteyearUp",   sel_jesAbsoluteyearUp)
                .Define("jetsVeto30_jesAbsoluteyearDown", sel_jesAbsoluteyearDown)
                .Define("nRecoilJets_jesAbsoluteyearUp",   get_nRecoilJets, {"nJet", "jetsVeto30_jesAbsoluteyearUp",   "deltaR_for_jetsVeto30", "Jet_eta", "Jet_phi", "eta_1", "phi_1", "eta_2", "phi_2", "isWJets"})
                .Define("nRecoilJets_jesAbsoluteyearDown", get_nRecoilJets, {"nJet", "jetsVeto30_jesAbsoluteyearDown", "deltaR_for_jetsVeto30", "Jet_eta", "Jet_phi", "eta_1", "phi_1", "eta_2", "phi_2", "isWJets"})
                .Define("jetsVeto30_jesBBEC1Up",   sel_jesBBEC1Up)
                .Define("jetsVeto30_jesBBEC1Down", sel_jesBBEC1Down)
                .Define("nRecoilJets_jesBBEC1Up",   get_nRecoilJets, {"nJet", "jetsVeto30_jesBBEC1Up",   "deltaR_for_jetsVeto30", "Jet_eta", "Jet_phi", "eta_1", "phi_1", "eta_2", "phi_2", "isWJets"})
                .Define("nRecoilJets_jesBBEC1Down", get_nRecoilJets, {"nJet", "jetsVeto30_jesBBEC1Down", "deltaR_for_jetsVeto30", "Jet_eta", "Jet_phi", "eta_1", "phi_1", "eta_2", "phi_2", "isWJets"})
                .Define("jetsVeto30_jesBBEC1yearUp",   sel_jesBBEC1yearUp)
                .Define("jetsVeto30_jesBBEC1yearDown", sel_jesBBEC1yearDown)
                .Define("nRecoilJets_jesBBEC1yearUp",   get_nRecoilJets, {"nJet", "jetsVeto30_jesBBEC1yearUp",   "deltaR_for_jetsVeto30", "Jet_eta", "Jet_phi", "eta_1", "phi_1", "eta_2", "phi_2", "isWJets"})
                .Define("nRecoilJets_jesBBEC1yearDown", get_nRecoilJets, {"nJet", "jetsVeto30_jesBBEC1yearDown", "deltaR_for_jetsVeto30", "Jet_eta", "Jet_phi", "eta_1", "phi_1", "eta_2", "phi_2", "isWJets"})
                .Define("jetsVeto30_jesEC2Up",   sel_jesEC2Up)
                .Define("jetsVeto30_jesEC2Down", sel_jesEC2Down)
                .Define("nRecoilJets_jesEC2Up",   get_nRecoilJets, {"nJet", "jetsVeto30_jesEC2Up",   "deltaR_for_jetsVeto30", "Jet_eta", "Jet_phi", "eta_1", "phi_1", "eta_2", "phi_2", "isWJets"})
                .Define("nRecoilJets_jesEC2Down", get_nRecoilJets, {"nJet", "jetsVeto30_jesEC2Down", "deltaR_for_jetsVeto30", "Jet_eta", "Jet_phi", "eta_1", "phi_1", "eta_2", "phi_2", "isWJets"})
                .Define("jetsVeto30_jesEC2yearUp",   sel_jesEC2yearUp)
                .Define("jetsVeto30_jesEC2yearDown", sel_jesEC2yearDown)
                .Define("nRecoilJets_jesEC2yearUp",   get_nRecoilJets, {"nJet", "jetsVeto30_jesEC2yearUp",   "deltaR_for_jetsVeto30", "Jet_eta", "Jet_phi", "eta_1", "phi_1", "eta_2", "phi_2", "isWJets"})
                .Define("nRecoilJets_jesEC2yearDown", get_nRecoilJets, {"nJet", "jetsVeto30_jesEC2yearDown", "deltaR_for_jetsVeto30", "Jet_eta", "Jet_phi", "eta_1", "phi_1", "eta_2", "phi_2", "isWJets"})
                .Define("jetsVeto30_jesFlavorQCDUp",   sel_jesFlavorQCDUp)
                .Define("jetsVeto30_jesFlavorQCDDown", sel_jesFlavorQCDDown)
                .Define("nRecoilJets_jesFlavorQCDUp",   get_nRecoilJets, {"nJet", "jetsVeto30_jesFlavorQCDUp",   "deltaR_for_jetsVeto30", "Jet_eta", "Jet_phi", "eta_1", "phi_1", "eta_2", "phi_2", "isWJets"})
                .Define("nRecoilJets_jesFlavorQCDDown", get_nRecoilJets, {"nJet", "jetsVeto30_jesFlavorQCDDown", "deltaR_for_jetsVeto30", "Jet_eta", "Jet_phi", "eta_1", "phi_1", "eta_2", "phi_2", "isWJets"})
                .Define("jetsVeto30_jesHFUp",   sel_jesHFUp)
                .Define("jetsVeto30_jesHFDown", sel_jesHFDown)
                .Define("nRecoilJets_jesHFUp",   get_nRecoilJets, {"nJet", "jetsVeto30_jesHFUp",   "deltaR_for_jetsVeto30", "Jet_eta", "Jet_phi", "eta_1", "phi_1", "eta_2", "phi_2", "isWJets"})
                .Define("nRecoilJets_jesHFDown", get_nRecoilJets, {"nJet", "jetsVeto30_jesHFDown", "deltaR_for_jetsVeto30", "Jet_eta", "Jet_phi", "eta_1", "phi_1", "eta_2", "phi_2", "isWJets"})
                .Define("jetsVeto30_jesHFyearUp",   sel_jesHFyearUp)
                .Define("jetsVeto30_jesHFyearDown", sel_jesHFyearDown)
                .Define("nRecoilJets_jesHFyearUp",   get_nRecoilJets, {"nJet", "jetsVeto30_jesHFyearUp",   "deltaR_for_jetsVeto30", "Jet_eta", "Jet_phi", "eta_1", "phi_1", "eta_2", "phi_2", "isWJets"})
                .Define("nRecoilJets_jesHFyearDown", get_nRecoilJets, {"nJet", "jetsVeto30_jesHFyearDown", "deltaR_for_jetsVeto30", "Jet_eta", "Jet_phi", "eta_1", "phi_1", "eta_2", "phi_2", "isWJets"})
                .Define("jetsVeto30_jesRelativeBalUp",   sel_jesRelativeBalUp)
                .Define("jetsVeto30_jesRelativeBalDown", sel_jesRelativeBalDown)
                .Define("nRecoilJets_jesRelativeBalUp",   get_nRecoilJets, {"nJet", "jetsVeto30_jesRelativeBalUp",   "deltaR_for_jetsVeto30", "Jet_eta", "Jet_phi", "eta_1", "phi_1", "eta_2", "phi_2", "isWJets"})
                .Define("nRecoilJets_jesRelativeBalDown", get_nRecoilJets, {"nJet", "jetsVeto30_jesRelativeBalDown", "deltaR_for_jetsVeto30", "Jet_eta", "Jet_phi", "eta_1", "phi_1", "eta_2", "phi_2", "isWJets"})
                .Define("jetsVeto30_jesRelativeSampleUp",   sel_jesRelativeSampleUp)
                .Define("jetsVeto30_jesRelativeSampleDown", sel_jesRelativeSampleDown)
                .Define("nRecoilJets_jesRelativeSampleUp",   get_nRecoilJets, {"nJet", "jetsVeto30_jesRelativeSampleUp",   "deltaR_for_jetsVeto30", "Jet_eta", "Jet_phi", "eta_1", "phi_1", "eta_2", "phi_2", "isWJets"})
                .Define("nRecoilJets_jesRelativeSampleDown", get_nRecoilJets, {"nJet", "jetsVeto30_jesRelativeSampleDown", "deltaR_for_jetsVeto30", "Jet_eta", "Jet_phi", "eta_1", "phi_1", "eta_2", "phi_2", "isWJets"});
    }
    else {
      return df3;
    }
  }

  // MC with no recoil corrections, or data or Embed: or no recoil corrections necessary
  return df.Define("nRecoilJets", [=]() { return 0.0f; })
           .Define("nRecoilJets_jerUp", [=]() { return 0.0f; })
           .Define("nRecoilJets_jerDown", [=]() { return 0.0f; })
           .Define("nRecoilJets_jesAbsoluteUp", [=]() { return 0.0f; })
           .Define("nRecoilJets_jesAbsoluteDown", [=]() { return 0.0f; })
           .Define("nRecoilJets_jesAbsoluteyearUp", [=]() { return 0.0f; })
           .Define("nRecoilJets_jesAbsoluteyearDown", [=]() { return 0.0f; })
           .Define("nRecoilJets_jesBBEC1Up", [=]() { return 0.0f; })
           .Define("nRecoilJets_jesBBEC1Down", [=]() { return 0.0f; })
           .Define("nRecoilJets_jesBBEC1yearUp", [=]() { return 0.0f; })
           .Define("nRecoilJets_jesBBEC1yearDown", [=]() { return 0.0f; })
           .Define("nRecoilJets_jesEC2Up", [=]() { return 0.0f; })
           .Define("nRecoilJets_jesEC2Down", [=]() { return 0.0f; })
           .Define("nRecoilJets_jesEC2yearUp", [=]() { return 0.0f; })
           .Define("nRecoilJets_jesEC2yearDown", [=]() { return 0.0f; })
           .Define("nRecoilJets_jesFlavorQCDUp", [=]() { return 0.0f; })
           .Define("nRecoilJets_jesFlavorQCDDown", [=]() { return 0.0f; })
           .Define("nRecoilJets_jesHFUp", [=]() { return 0.0f; })
           .Define("nRecoilJets_jesHFDown", [=]() { return 0.0f; })
           .Define("nRecoilJets_jesHFyearUp", [=]() { return 0.0f; })
           .Define("nRecoilJets_jesHFyearDown", [=]() { return 0.0f; })
           .Define("nRecoilJets_jesRelativeBalUp", [=]() { return 0.0f; })
           .Define("nRecoilJets_jesRelativeBalDown", [=]() { return 0.0f; })
           .Define("nRecoilJets_jesRelativeSampleUp", [=]() { return 0.0f; })
           .Define("nRecoilJets_jesRelativeSampleDown", [=]() { return 0.0f; });
}



/***************************************************************************/



#endif
