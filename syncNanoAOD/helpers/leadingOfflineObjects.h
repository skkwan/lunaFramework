#ifndef LEADING_OFFLINE_OBJECTS_H_INCL
#define LEADING_OFFLINE_OBJECTS_H_INCL

#include "computePhysicsVariables.h"
#include "helperFunctions.h"

/**********************************************************/

std::vector<int> build_pair_from_channel(int channel,
                                         RVec<float>& Muon_pt, RVec<float>& Muon_eta, RVec<float>& Muon_phi, RVec<float>& Muon_iso,
                                         RVec<float>& Tau_pt, RVec<float>& Tau_eta, RVec<float>& Tau_phi, RVec<float>& Tau_iso,
                                         RVec<float>& Electron_pt, RVec<float>& Electron_eta, RVec<float>& Electron_phi, RVec<float>& Electron_iso,
                                         RVec<int>& goodMuons_mutauh,
                                         RVec<int>& goodTaus_mutauh_and_etauh,
                                         RVec<int>& goodElectrons_etauh,
                                         RVec<int>& goodMuons_emu,
                                         RVec<int>& goodElectrons_emu) {


    std::vector<int> pairIdx;
    float deltaR;
    if (channel == Helper::mt) {
        deltaR = 0.4;
        pairIdx = build_pair(goodMuons_mutauh,          Muon_pt, Muon_eta, Muon_phi, Muon_iso,
                             goodTaus_mutauh_and_etauh, Tau_pt,  Tau_eta,  Tau_phi,  Tau_iso,  deltaR);
    }
    else if (channel == Helper::et) {
        deltaR = 0.4;
        pairIdx = build_pair(goodElectrons_etauh,       Electron_pt, Electron_eta, Electron_phi, Electron_iso,
                             goodTaus_mutauh_and_etauh, Tau_pt,      Tau_eta,      Tau_phi,      Tau_iso,     deltaR);
    }
    else if (channel == Helper::em) {
        deltaR = 0.3;
        // Special customization for e+mu channel: per Higgs To TauTau, treat muon as the leading leg for sorting by iso. But for the rest of our
        // analysis leg _1 is an electron and leg _2 is a muon.
        int reverseIndices = true;
        pairIdx = build_pair(goodMuons_emu,     Muon_pt, Muon_eta, Muon_phi, Muon_iso,
                             goodElectrons_emu, Electron_pt, Electron_eta, Electron_phi, Electron_iso, deltaR, reverseIndices);
    }
    else {
        pairIdx = {-1, -1};
    }
    return pairIdx;
}

/**********************************************************/

/*
 * Filter by presence of leading objects
 */
template <typename T>
auto FilterByLeadingOfflineObjects(T &df) {


    return df.Define("pairIdx", build_pair_from_channel,
                                  {"channel",
                                   "Muon_pt", "Muon_eta", "Muon_phi", "Muon_pfRelIso04_all",
                                   "Tau_pt", "Tau_eta", "Tau_phi", "Tau_rawDeepTau2017v2p1VSjet",
                                   "Electron_pt", "Electron_eta", "Electron_phi", "Electron_pfRelIso03_all",
                                   "goodMuons_mutauh",
                                   "goodTaus_mutauh_and_etauh",
                                   "goodElectrons_etauh",
                                   "goodMuons_emu", "goodElectrons_emu"
                                   })
           .Define("idx_1", [](std::vector<int> pairIdx) { return pairIdx[0]; }, {"pairIdx"})
           .Define("idx_2", [](std::vector<int> pairIdx) { return pairIdx[1]; }, {"pairIdx"})
           .Filter([](int idx) { return (bool) (idx != -1); }, {"idx_1"}, "leadingOfflineObjects.h: Valid first leg in selected pair")
           .Filter([](int idx) { return (bool) (idx != -1); }, {"idx_2"}, "leadingOfflineObjects.h: Valid second leg in selected pair");

}




#endif
