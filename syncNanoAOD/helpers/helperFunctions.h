// Functions in the Helper namespace.
#ifndef HELPERFUNCTIONS_H_INCL
#define HELPERFUNCTIONS_H_INCL

#include <cmath>
#include <string>

#include <ROOT/RVec.hxx>

/***************************************************************************/

namespace Helper {

  const int mt = 0;
  const int et = 1;
  const int em  = 2;
  const int none = 3;

  const int pdgId_muon = 13;
  const int pdgId_tau = 15;
  const int pdgId_electron = 11;

  const float pi0mass = 0.1349768; // 134.97 MeV according to PDG ID
  const float piPMmass = 0.13957; // 139.57 MeV according to PDG ID

  // Returns whether a substring is in a string.
  bool containsSubstring(std::string string, std::string substring);

  // Returns whether any substrings in the provided vector, are in the string.
  bool containsAnyOfTheseSubstrings(std::string string, std::vector<std::string> vSubstring);

  // In "subject", replace all instances of "string" with "replace"
  void replaceStringInPlace(std::string& subject, const std::string& search, const std::string& replace);

  // Returns the difference in the azimuth coordinates of v1 and v2, taking
  // the boundary conditions at 2 * pi into account.
  template <typename T>
  float DeltaPhi(T v1, T v2, const T c = M_PI) {
      auto r = std::fmod(v2 - v1, 2.0 * c);
      if (r < -c) {
      r += 2.0 * c;
      }
      else if (r > c) {
      r -= 2.0 * c;
      }
      return r;
  }

  // Template function for computing Delta R.
  template <typename T>
  float DeltaR(T eta1, T eta2, T phi1, T phi2) {
      auto deltaR = sqrt(pow(eta1 - eta2, 2) + pow(Helper::DeltaPhi(phi1, phi2), 2));
      return deltaR;
  }

  // DeltaPhi and deltaR with floats.
  float compute_deltaPhi(float phi1, float phi2, const double c = M_PI);
  float compute_deltaR(float eta1, float eta2, float phi1, float phi2);

  // Return the value with index idx from branch b (floats). If idx is invalid
  // (-1), return -9999.
  float GetFloat(ROOT::RVec<float>& b, int idx);

  // Same as above, but for a branch b of ints.
  int GetInt(ROOT::RVec<int>& b, int idx);

  // Logical OR of four paths (for compiled Define)
  bool logicalOR(bool b1, bool b2, bool b3, bool b4);

  float getValueByChannelWithDefault(int channel, float val_default, float val_mt, float val_et, float val_em);
  float getValueByChannel(int channel, float val_mt, float val_et, float val_em);
}

/***************************************************************************/




#endif
