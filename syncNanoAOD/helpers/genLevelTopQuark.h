// Helper functions for the top quark generator-level information.
#ifndef GEN_LEVEL_TOP_QUARK_H_INCL
#define GEN_LEVEL_TOP_QUARK_H_INCL


#include "computePhysicsVariables.h"
#include "fileIO.h"
#include "genHelper.h"
#include "helperFunctions.h"
#include "sampleConfig_class.h"

/***************************************************************************/

/*
 * Get gen-level top quark information (only for ttbar samples).
 */

template <typename T>
auto GetGenTopQuarks(T &df, LUNA::sampleConfig_t &sConfig) {

  // Define function that we will use to get the gen top quarks pT in a vector (-10.0 if no top quark found)
  using namespace ROOT::VecOps;
  auto find_tt_pair = [](unsigned long long event,
			 unsigned int nGenPart,
			 RVec<int>&   GenPart_pdgId,
			 RVec<int>&   GenPart_statusFlags,
			 RVec<float>& GenPart_pt) {

    (void) event; // in case I need it for debugging

    std::vector<float> output;

    // Loop through genParticle collection
    for (unsigned int i = 0; i < nGenPart; i++) {

      int pdgId      = GenPart_pdgId[i];
      int statusFlag = GenPart_statusFlags[i];
      float pt       = GenPart_pt[i];

      /* std::cout << "event: " << event << ", "  */
      /* 		<< "pdgId: " << pdgId << ", " */
      /* 		<< "statusFlag: " << statusFlag << ", " */
      /* 		<< "pt: " << pt << std::endl; */

      // Top quark (see GenHelper.h and https://cms-nanoaod-integration.web.cern.ch/integration/master-102X/mc102X_doc.html#GenPart
      // for documentation)
      if (abs(pdgId) == GenHelper::pdgId_top) {

	bool fromHardProcess = GenHelper::fromHardProcess(statusFlag);
	bool isLastCopy      = GenHelper::isLastCopy(statusFlag);

	if (fromHardProcess && isLastCopy) {
	  output.push_back(pt);
	}

      }
    }

    // If there are insufficient top quarks, fill the remainder of the vector with dummy pT values
    if      (output.size() == 0) { output.push_back(-10.); output.push_back(-10.); }
    else if (output.size() == 1) { output.push_back(-10.); }

    // std::cout << " find_tt_pair: " << output[0] << " " << output[1] << std::endl;

    return output;
  };

  std::cout << ">>> genLevelTopQuark.h: is ttbar sample? " << sConfig.isTTbar() << std::endl;

  // TTbar sample
  if (sConfig.isTTbar()) {
    return df.Define("pairGenTT", find_tt_pair,
		     {"event", "nGenPart", "GenPart_pdgId", "GenPart_statusFlags", "GenPart_pt"})
             .Define("gen_topPt_1", "pairGenTT[0]")
             .Define("gen_topPt_2", "pairGenTT[1]");
  }
  else {
    return df;
  }
}



/***************************************************************************/



#endif
