#ifndef INCLUSIVE_JETS_SAMPLES_H_INCL
#define INCLUSIVE_JETS_SAMPLES_H_INCL

#include "sampleConfig_class.h"

/***************************************************************************/

template <typename T>
auto GetInclusiveJetsSamplesInfo(T &df, LUNA::sampleConfig_t &sConfig) {

    if (sConfig.isDY() || sConfig.isWJets()) {
        return df.Define("lhe_njets_DY_W_jets", "(int) LHE_Njets");
    }
    return df;
}

/***************************************************************************/


#endif
