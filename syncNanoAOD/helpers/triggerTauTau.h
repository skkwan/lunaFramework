#ifndef TRIGGER_TAUTAU_H_INCL
#define TRIGGER_TAUTAU_H_INCL


#include "computePhysicsVariables.h"
#include "fileIO.h"
#include "helperFunctions.h"
#include "trigObj.h"

/**********************************************************/


/*
 * Check if an event passes a single trigger.
 */
int passes_single_trigger(int passes_hlt, int passes_filter, int is_trigObj_matched, float pt, float pt_threshold) {
                                return (passes_hlt && passes_filter && is_trigObj_matched && (pt > pt_threshold));
                            }



/*
 * _1 and _2 refer to the two legs for the cross trigger.
 */
int passes_cross_trigger(int isEmbedded, int passes_cross_hlt, int passes_cross_filter,
                          int is_trigObj_matched_1, int is_trigObj_matched_2,
                          float pt_1, float pt_1_min, float pt_1_max, // first leg has a min and max
                          float pt_2, float pt_2_min,                 // second leg only has a min
                          float eta_1, float eta_1_max,
                          float eta_2, float eta_2_max) {
                            if (isEmbedded) {
                                return (passes_cross_filter &&
                                    is_trigObj_matched_1 && is_trigObj_matched_2 &&
                                    (pt_1 > pt_1_min) && (pt_1 < pt_1_max) &&
                                    (pt_2 > pt_2_min) &&
                                    (abs(eta_1) < eta_1_max) && (abs(eta_2) < eta_2_max));
                            }
                            return (passes_cross_hlt && passes_cross_filter &&
                                    is_trigObj_matched_1 && is_trigObj_matched_2 &&
                                    (pt_1 > pt_1_min) && (pt_1 < pt_1_max) &&
                                    (pt_2 > pt_2_min) &&
                                    (abs(eta_1) < eta_1_max) && (abs(eta_2) < eta_2_max));
                          }

/*
 * _1 and _2 refer to the two legs for the cross trigger. Simpler version of the above function, where we only have pT minima to check.
 */
int passes_cross_trigger_pt_min_only(int isEmbedded, int passes_cross_hlt, int passes_cross_filter,
                                      int is_trigObj_matched_1, int is_trigObj_matched_2,
                                      float pt_1, float pt_1_min,
                                      float pt_2, float pt_2_min) {
                                        if (isEmbedded) {
                                            return (passes_cross_filter &&
                                                is_trigObj_matched_1 && is_trigObj_matched_2 &&
                                                (pt_1 > pt_1_min) && (pt_2 > pt_2_min));
                                        }
                                        return (passes_cross_hlt && passes_cross_filter &&
                                                is_trigObj_matched_1 && is_trigObj_matched_2 &&
                                                (pt_1 > pt_1_min) && (pt_2 > pt_2_min));
                                      }

/**********************************************************/

/*
 * 2018: filter bits and nominal trigger decisions
 */
template <typename T>
auto GetTauTauTriggerFilter_And_NominalDecisions_2018(T &df, LUNA::sampleConfig_t &sConfig) {

    // Check whether the appropriate bits in filterBits are passed
    // https://github.com/cms-sw/cmssw/blob/master/PhysicsTools/NanoAOD/python/triggerObjects_cff.py#L53

    if (sConfig.year() == 2018) {

        //////////////// MuTau: filters first
        return df.Define("filter_mt_Mu24", [](int ch, int filterBits1) { if (ch == Helper::mt) return (int) ((filterBits1 & 2) && (filterBits1 & 8)); else return ((int) 0); }, {"channel", "filterBits1"})
        .Define("filter_mt_Mu27",         [](int ch, int filterBits1) { if (ch == Helper::mt) return (int) ((filterBits1 & 2) && (filterBits1 & 8)); else return ((int) 0); }, {"channel", "filterBits1"})
        .Define("filter_mt_Mu20Tau27",    [](int ch, int isEmbedded, int filterBits1, int filterBits2) {
            if (ch == Helper::mt) {
                if (isEmbedded) {
                    // https://twiki.cern.ch/twiki/bin/viewauth/CMS/TauTauEmbeddingSamples2018#MuTau
                    return (int) ((filterBits1 & 32778) && (filterBits2 & 4096));
                }
                else {
                    return (int) ((filterBits2 & 1) && (filterBits2 & 16) && (filterBits2 & 256));
                }
            }
            else {
                return ((int) 0);
            }
            }, {"channel", "isEmbedded", "filterBits1", "filterBits2"})
        .Define("filter_mt_Mu20HPSTau27", [](int ch, int isEmbedded, int filterBits1, int filterBits2) {
            if (ch == Helper::mt) {
                if (isEmbedded) {
                    // https://twiki.cern.ch/twiki/bin/viewauth/CMS/TauTauEmbeddingSamples2018#MuTau
                    return (int) ((filterBits1 & 32778) && (filterBits2 & 4096));
                }
                else {
                    return (int) ((filterBits2 & 1) && (filterBits2 & 8) && (filterBits2 & 16) && (filterBits2 & 256));
                }
            }
            else {
                return ((int) 0);
            }
            }, {"channel", "isEmbedded", "filterBits1", "filterBits2"})
        // Get nominal trigger decision for single mu24 trigger (note hard-coded pt>25 check)
        .Define("mt_pt_1_min", [] { return 21.0f; })
        .Define("mt_pt_1_max", [] { return 25.0f; })
        .Define("mt_pt_2_min", [] { return 20.0f; })
        .Define("mt_eta_max", [] { return 2.1f; })
        .Define("trigger_mt_Mu24", [](int passHLTPath, int isTrigObjMatched, int passFilterBits, float pt_1, float pt_2) {
            return (int) (passHLTPath && isTrigObjMatched && passFilterBits && (pt_1 > 25) && (pt_2 > 20)); },
            {"pass_mt_HLT_IsoMu24", "filter_mt_Mu24", "match1", "pt_1", "pt_2"})
        // Get nominal trigger decision for single mu27 trigger (note hard-coded pt>25 check)
        .Define("trigger_mt_Mu27", [](int passHLTPath, int isTrigObjMatched, int passFilterBits, float pt_1, float pt_2) {
            return (int) (passHLTPath && isTrigObjMatched && passFilterBits && (pt_1 > 25) && (pt_2 > 20)); },
            {"pass_mt_HLT_IsoMu27", "filter_mt_Mu27", "match1", "pt_1", "pt_2"})
        // Get nominal trigger decision for cross-trigger #1: HLT_IsoMu20_eta2p1_LooseChargedIsoPFTau27_eta2p1_CrossL1
        // Note hard-coded: muon pT must be between 21 and 25, and tau pT must be > 32
        .Define("trigger_mt_Mu20Tau27",    passes_cross_trigger, {"isEmbedded", "pass_mt_HLT_Mu20Tau27",   "filter_mt_Mu20Tau27",
                                                                   "match1", "match2", "pt_1", "mt_pt_1_min", "mt_pt_1_max",
                                                                    "pt_2", "mt_pt_2_min",
                                                                    "eta_1", "mt_eta_max",
                                                                    "eta_2", "mt_eta_max"})
        .Define("trigger_mt_Mu20HPSTau27", passes_cross_trigger, {"isEmbedded", "pass_mt_HLT_Mu20HPSTau27", "filter_mt_Mu20HPSTau27",
                                                                    "match1", "match2", "pt_1", "mt_pt_1_min", "mt_pt_1_max",
                                                                    "pt_2", "mt_pt_2_min",
                                                                    "eta_1", "mt_eta_max",
                                                                    "eta_2", "mt_eta_max"})
        // Get 2018 nominal trigger decision for mutau. Cone-based trigger does not exist anymore in UL
        .Define("passMuTauTrigger", [](int single1, int single2, int cross1, int cross2) {
            return (int) (single1 || single2 || cross1 || cross2);
          } , {"trigger_mt_Mu24", "trigger_mt_Mu27", "trigger_mt_Mu20Tau27", "trigger_mt_Mu20HPSTau27"})

        ///////////////// ETau: get filter bits (needed later to re-compute trigger decisions)
        // `HLT_Ele32_WPTight_Gsf_v` would require: bit 2 (1e (WPTight))
        .Define("filter_et_Ele32", [](int ch, int filterBits1)  { if (ch == Helper::et) return (int) (filterBits1 & 2); else return ((int) 0); }, {"channel", "filterBits1"})
        .Define("filter_et_Ele35", [](int ch, int filterBits1)  { if (ch == Helper::et) return (int) (filterBits1 & 2); else return ((int) 0); }, {"channel", "filterBits1"})
        // MC and data run >= 317509: "HLT_Ele24_eta2p1_WPTight_Gsf_LooseChargedIsoPFTauHPS30_eta2p1_CrossL1"
        .Define("filter_et_Ele24Tau30",    [](int ch, int isEmbedded, int filterBits1, int filterBits2) {
                if (ch == Helper::et) {
                    if (isEmbedded) {
                        return (int) ((filterBits1 & 524288) && (filterBits2 & 1024));
                    }
                    else {
                        return (int) ((filterBits1 & 2) && (filterBits2 & 1) && (filterBits2 & 16) && (filterBits2 & 128));
                    }
                }
                else {
                    return (int) 0;
                }
            },
            {"channel", "isEmbedded", "filterBits1", "filterBits2"})
        .Define("filter_et_Ele24HPSTau30", [](int ch, int isEmbedded, int filterBits1, int filterBits2) {
                if (ch == Helper::et) {
                    if (isEmbedded) {
                        return (int) ((filterBits1 & 524288) && (filterBits2 & 1024));
                    }
                    else {
                        return (int) ((filterBits1 & 2) && (filterBits2 & 1) && (filterBits2 & 8) && (filterBits2 & 16) && (filterBits2 & 128));
                    }
                }
                else {
                    return (int) 0;
                }
            },
            {"channel", "isEmbedded", "filterBits1", "filterBits2"})
        // Get 2018 nominal etau single triggers: note hard-coded pT threshold
        .Define("trigger_et_Ele32", [](int passHLTPath, int isTrigObjMatched, int passFilterBits, float pt_1, float pt_2) {
            return (int) (passHLTPath && isTrigObjMatched && passFilterBits && (pt_1 > 33) && (pt_2 > 20)); },
            {"pass_et_HLT_Ele32", "filter_et_Ele32", "match1", "pt_1", "pt_2"})
        .Define("trigger_et_Ele35", [](int passHLTPath, int isTrigObjMatched, int passFilterBits, float pt_1, float pt_2) {
            return (int) (passHLTPath && isTrigObjMatched && passFilterBits && (pt_1 > 33) && (pt_2 > 20)); },
            {"pass_et_HLT_Ele35", "filter_et_Ele35", "match1", "pt_1", "pt_2"})
        // Get 2018 nominal etau cross triggers
        .Define("pt_1_et_cross_min", []() { return (float) 25; })
        .Define("pt_1_et_cross_max", []() { return (float) 33; })
        .Define("pt_2_et_cross_min", []() { return (float) 35; }) // 5 GeV greater than the online threshold
        .Define("eta_et_cross_max",  []() { return (float) 2.1; })
        .Define("trigger_et_Ele24Tau30",    passes_cross_trigger, {"isEmbedded", "pass_et_HLT_Ele24Tau30", "filter_et_Ele24Tau30", "match1", "match2", "pt_1", "pt_1_et_cross_min", "pt_1_et_cross_max", "pt_2", "pt_2_et_cross_min", "eta_1", "eta_et_cross_max", "eta_2", "eta_et_cross_max"})
        .Define("trigger_et_Ele24HPSTau30", passes_cross_trigger, {"isEmbedded", "pass_et_HLT_Ele24HPSTau30", "filter_et_Ele24HPSTau30", "match1", "match2", "pt_1", "pt_1_et_cross_min", "pt_1_et_cross_max", "pt_2", "pt_2_et_cross_min", "eta_1", "eta_et_cross_max", "eta_2", "eta_et_cross_max"})
        .Define("passETauTrigger", [](int single1, int single2, int cross1, int cross2) {
          return (int) (single1 || single2 || cross1 || cross2); },
            {"trigger_et_Ele32", "trigger_et_Ele35", "trigger_et_Ele24Tau30", "trigger_et_Ele24HPSTau30"})

        ////////////////// EMu: get passEMuTrigger
        // HLT_Mu23_TrkIsoVVL_Ele12_CaloIdL_TrackIdL_IsoVL_DZ, which requires electron bit 1 = CaloIdL_TrackIdL_IsoVL, and electron bit 32 = 1e-1mu,
        // and muon bit 1 = TrkIsoVVL and bit 32 = 1mu-1e,
        .Define("filter_em_Mu23Ele12", [] (int ch, int filterBits1, int filterBits2) { if (ch == Helper::em) return (int) ((filterBits1 & 1) && (filterBits2 & 1) && (filterBits1 & 32) && (filterBits2 & 32)); else return ((int) 0); }, {"channel", "filterBits1", "filterBits2"})
        // HLT_Mu8_TrkIsoVVL_Ele23_CaloIdL_TrackIdL_IsoVL_DZ
        .Define("filter_em_Mu8Ele23",  [] (int ch, int filterBits1, int filterBits2) { if (ch == Helper::em) return (int) ((filterBits1 & 1) && (filterBits2 & 1) && (filterBits1 & 32) && (filterBits2 & 32)); else return ((int) 0); }, {"channel", "filterBits1", "filterBits2"})
        // For the Mu23Ele12 trigger, i.e. cross-trigger with leading muon
        .Define("pt_1_em_crossLeadingMuon_min", []() { return (float) 13; })  // ele (pt_1) offline pT > 13 GeV
        .Define("pt_2_em_crossLeadingMuon_min", []() { return (float) 24; })  // muon (pt_2) offline pT > 24 GeV
        // For the Mu8Ele23 trigger, i.e. cross-trigger with leading electron
        .Define("pt_1_em_crossLeadingEle_min", []() { return (float) 24; })  // ele (pt_1) offline pT > 24 GeV
        .Define("pt_2_em_crossLeadingEle_min", []() { return (float) 13; })  // muon (pt_2) offline pT > 13 GeV
        .Define("trigger_em_Mu23Ele12", passes_cross_trigger_pt_min_only, {"isEmbedded", "pass_em_HLT_Mu23Ele12", "filter_em_Mu23Ele12", "match1", "match2",
                                                                            "pt_1", "pt_1_em_crossLeadingMuon_min", "pt_2", "pt_2_em_crossLeadingMuon_min"})
        .Define("trigger_em_Mu8Ele23",  passes_cross_trigger_pt_min_only, {"isEmbedded", "pass_em_HLT_Mu8Ele23", "filter_em_Mu8Ele23", "match1", "match2",
                                                                            "pt_1", "pt_1_em_crossLeadingEle_min", "pt_2", "pt_2_em_crossLeadingEle_min"})
        .Define("passEMuTrigger", [](int cross1, int cross2) { return (int) (cross1 || cross2); }, {"trigger_em_Mu23Ele12", "trigger_em_Mu8Ele23"});
    }
    else {
        return df;
    }
}

/**********************************************************/

/*
 * 2017: filter bits and nominal trigger decisions
 */
template <typename T>
auto GetTauTauTriggerFilter_And_NominalDecisions_2017(T &df, LUNA::sampleConfig_t &sConfig) {

    // Check whether the appropriate bits in filterBits are passed
    // https://github.com/cms-sw/cmssw/blob/master/PhysicsTools/NanoAOD/python/triggerObjects_cff.py#L53

    if (sConfig.year() == 2017) {

        //////////////// MuTau: filters first
        return df.Define("filter_mt_Mu27",         [](int ch, int filterBits1) { if (ch == Helper::mt) return (int) ((filterBits1 & 2) && (filterBits1 & 8)); else return ((int) 0); }, {"channel", "filterBits1"})
        .Define("filter_mt_Mu20Tau27",    [](int ch, int filterBits1, int filterBits2) { if (ch == Helper::mt) return (int) ((filterBits1 & 2) && (filterBits2 & 1)); else return ((int) 0); }, {"channel", "filterBits1", "filterBits2"})

        // Get nominal trigger decision for single mu27 trigger (note hard-coded pt>28 check)
        .Define("trigger_mt_Mu27", [](int passHLTPath, int isTrigObjMatched, int passFilterBits, float pt) {
            return (int) (passHLTPath && isTrigObjMatched && passFilterBits && (pt > 25)); },
            {"pass_mt_HLT_IsoMu27", "filter_mt_Mu27", "match1", "pt_1"})
        // Get nominal trigger decision for cross-trigger (only one for 2017): HLT_IsoMu20_eta2p1_LooseChargedIsoPFTau27_eta2p1_CrossL1
        // Note hard-coded: muon pT must be between 21 and 25, and tau pT must be > 32
        .Define("trigger_mt_Mu20Tau27", [](int passHLT, int passFilterBits,
                                           int isTrigObjMatched_1, float pt_1, float eta_1,
                                           int isTrigObjMatched_2, float pt_2, float eta_2) {
                                            return (int) (passHLT && passFilterBits && isTrigObjMatched_1 && isTrigObjMatched_2 &&
                                                    (pt_1 > 21) && (pt_1 < 25) && (abs(eta_1) < 2.1) &&
                                                    (pt_2 > 32) && (abs(eta_2) < 2.1));   },
           {"pass_mt_HLT_Mu20Tau27", "filter_mt_Mu20Tau27", "match1", "pt_1", "eta_1", "match2", "pt_2", "eta_2"})
        // Get 2017 nominal trigger decision for mutau
	  .Define("passMuTauTrigger", [](int single, int cross) { return (int) (single || cross);},
          {"trigger_mt_Mu27", "trigger_mt_Mu20Tau27"})

        ///////////////// ETau: get filter bits (needed later to re-compute trigger decisions)
        // To mimic `filter_et_Ele32`, `HLT_Ele32_WPTight_SF_L1DoubleEG` would require bit 2 (1e (WPTight)) and bit 1024 (1e (32_L1DoubleEG_AND_L1SingleEGOr1e))
        .Define("filter_et_Ele32", [](int ch, int filterBits1)  { if (ch == Helper::et) return (int) ((filterBits1 & 2) && (filterBits1 & 1024)); else return ((int) 0); }, {"channel", "filterBits1"})
        .Define("filter_et_Ele35", [](int ch, int filterBits1)  { if (ch == Helper::et) return (int) (filterBits1 & 2); else return ((int) 0); }, {"channel", "filterBits1"})
        // Only one cross-trigger in 2017: HLT_Ele24_eta2p1_WPTight_Gsf_LooseChargedIsoPFTau30_eta2p1_CrossL1
        .Define("filter_et_Ele24Tau30", [](int ch, int filterBits1, int filterBits2) { if (ch == Helper::et) return (int) ((filterBits1 & 2) && (filterBits2 & 1) && (filterBits2 & 16) && (filterBits2 & 128)); else return ((int) 0); }, {"channel", "filterBits1", "filterBits2"})

        // Get 2017 nominal etau single triggers: note hard-coded pT threshold: https://github.com/hftsoi/aabbtt_finalselection/blob/master/selection_et_allyears.cc#L723-L724
        .Define("trigger_et_Ele32", [](int passHLTPath, int isTrigObjMatched, int passFilterBits, float pt_1, float pt_2) {
            return (int) (passHLTPath && isTrigObjMatched && passFilterBits && (pt_1 > 28) && (pt_2 > 20)); },
            {"pass_et_HLT_Ele32", "filter_et_Ele32", "match1", "pt_1", "pt_2"})
        .Define("trigger_et_Ele35", [](int passHLTPath, int isTrigObjMatched, int passFilterBits, float pt_1, float pt_2) {
            return (int) (passHLTPath && isTrigObjMatched && passFilterBits && (pt_1 > 28) && (pt_2 > 20)); },
            {"pass_et_HLT_Ele35", "filter_et_Ele35", "match1", "pt_1", "pt_2"})
        // Get 2017 nominal etau cross trigger (only one)
        .Define("pt_1_et_cross_min", []() { return (float) 25; })
        .Define("pt_1_et_cross_max", []() { return (float) 33; })
        .Define("pt_2_et_cross_min", []() { return (float) 35; }) // 5 GeV greater than the online threshold
        .Define("eta_et_cross_max",  []() { return (float) 2.1; })
        .Define("trigger_et_Ele24Tau30", passes_cross_trigger, {"isEmbedded", "pass_et_HLT_Ele24Tau30", "filter_et_Ele24Tau30", "match1", "match2", "pt_1", "pt_1_et_cross_min", "pt_1_et_cross_max", "pt_2", "pt_2_et_cross_min", "eta_1", "eta_et_cross_max", "eta_2", "eta_et_cross_max"})
        .Define("passETauTrigger", [](int single1, int single2, int cross) { return (int) (single1 || single2 || cross); },
            {"trigger_et_Ele32", "trigger_et_Ele35", "trigger_et_Ele24Tau30"})

        ////////////////// EMu 2017: only two cross-triggers: get passEMuTrigger
        // HLT_Mu23_TrkIsoVVL_Ele12_CaloIdL_TrackIdL_IsoVL_DZ, which requires electron bit 1 = CaloIdL_TrackIdL_IsoVL, and electron bit 32 = 1e-1mu,
        // and muon bit 1 = TrkIsoVVL and bit 32 = 1mu-1e,
        .Define("filter_em_Mu23Ele12", [] (int ch, int filterBits1, int filterBits2) { if (ch == Helper::em) return (int) ((filterBits1 & 1) && (filterBits2 & 1) && (filterBits1 & 32) && (filterBits2 & 32)); else return ((int) 0); }, {"channel", "filterBits1", "filterBits2"})
        // HLT_Mu8_TrkIsoVVL_Ele23_CaloIdL_TrackIdL_IsoVL_DZ
        .Define("filter_em_Mu8Ele23",  [] (int ch, int filterBits1, int filterBits2) { if (ch == Helper::em) return (int) ((filterBits1 & 1) && (filterBits2 & 1) && (filterBits1 & 32) && (filterBits2 & 32)); else return ((int) 0); }, {"channel", "filterBits1", "filterBits2"})
        // 2017: For the Mu23Ele12 trigger, i.e. cross-trigger with leading muon
        .Define("pt_1_em_crossLeadingMuon_min", []() { return (float) 13; })  // ele (pt_1) offline pT > 13 GeV
        .Define("pt_2_em_crossLeadingMuon_min", []() { return (float) 24; })  // muon (pt_2) offline pT > 24 GeV
        // 2017: For the Mu8Ele23 trigger, i.e. cross-trigger with leading electron
        .Define("pt_1_em_crossLeadingEle_min", []() { return (float) 24; })  // ele (pt_1) offline pT > 24 GeV
        .Define("pt_2_em_crossLeadingEle_min", []() { return (float) 13; })  // muon (pt_2) offline pT > 13 GeV
        .Define("trigger_em_Mu23Ele12", passes_cross_trigger_pt_min_only, {"isEmbedded", "pass_em_HLT_Mu23Ele12", "filter_em_Mu23Ele12", "match1", "match2",
                                                                            "pt_1", "pt_1_em_crossLeadingMuon_min", "pt_2", "pt_2_em_crossLeadingMuon_min"})
        .Define("trigger_em_Mu8Ele23",  passes_cross_trigger_pt_min_only, {"isEmbedded", "pass_em_HLT_Mu8Ele23", "filter_em_Mu8Ele23", "match1", "match2",
                                                                            "pt_1", "pt_1_em_crossLeadingEle_min", "pt_2", "pt_2_em_crossLeadingEle_min"})
        .Define("passEMuTrigger", [](int cross1, int cross2) { return (int) (cross1 || cross2); }, {"trigger_em_Mu23Ele12", "trigger_em_Mu8Ele23"});
    }
    else {
        return df;
    }
}

/**********************************************************/

/*
 * 2016: filter bits and nominal trigger decisions. Filter bit definitions are different for 2016 than 2017 and 2018!
 */
template <typename T>
auto GetTauTauTriggerFilter_And_NominalDecisions_2016(T &df, LUNA::sampleConfig_t &sConfig) {

    // Check whether the appropriate bits in filterBits are passed
    // https://github.com/cms-sw/cmssw/blob/master/PhysicsTools/NanoAOD/python/triggerObjects_cff.py#L53

    if (sConfig.year() == 2016) {

        //////////////// MuTau: filters first. bit 2 for muon is IsoMu, bit 8 is IsoTkMu
        return df
        .Define("filter_mt_IsoMu22",          [](int ch, int filterBits1) { if (ch == Helper::mt) return (int) (filterBits1 & 2); else return ((int) 0); }, {"channel", "filterBits1"})
        .Define("filter_mt_IsoTkMu22",        [](int ch, int filterBits1) { if (ch == Helper::mt) return (int) (filterBits1 & 8); else return ((int) 0); }, {"channel", "filterBits1"})
        .Define("filter_mt_IsoMu19_eta2p1_LooseIsoPFTau20", [](int ch, int filterBits1, int filterBits2) { if (ch == Helper::mt) return (int) ((filterBits1 & 8) && (filterBits2 & 1)); else return ((int) 0); }, {"channel", "filterBits1", "filterBits2"})

        // Get nominal trigger decision for single muon trigger. Note that there is already a cut on eta_1 and eta_2 earlier in the cutflow.
        .Define("mt_single_trigger_2016_muon_pt_threshold", []() { return (float) 23; })
        .Define("trigger_mt_IsoMu22",          passes_single_trigger, {"pass_mt_HLT_IsoMu22",          "filter_mt_IsoMu22",   "match1", "pt_1", "mt_single_trigger_2016_muon_pt_threshold"})
        .Define("trigger_mt_IsoMu22_eta2p1",   passes_single_trigger, {"pass_mt_HLT_IsoMu22_eta2p1",   "filter_mt_IsoMu22",   "match1", "pt_1", "mt_single_trigger_2016_muon_pt_threshold"})
        .Define("trigger_mt_IsoTkMu22",        passes_single_trigger, {"pass_mt_HLT_IsoTkMu22",        "filter_mt_IsoTkMu22", "match1", "pt_1", "mt_single_trigger_2016_muon_pt_threshold"})
        .Define("trigger_mt_IsoTkMu22_eta2p1", passes_single_trigger, {"pass_mt_HLT_IsoTkMu22_eta2p1", "filter_mt_IsoTkMu22", "match1", "pt_1", "mt_single_trigger_2016_muon_pt_threshold"})
        // Get nominal trigger decision for cross triggers. Note that there is already a cut on eta_1 and eta_2 earlier in the cutflow.
        .Define("trigger_mt_IsoMu19_eta2p1_LooseIsoPFTau20", [](int passHLT, int passFilterBits,
                                                                int isTrigObjMatched_1, float pt_1, float eta_1,
                                                                int isTrigObjMatched_2, float pt_2) {
                                            return (int) (passHLT && passFilterBits && isTrigObjMatched_1 && isTrigObjMatched_2 &&
                                                    (pt_1 > 20) && (pt_1 < 23) && (abs(eta_1) < 2.1) &&
                                                    (pt_2 > 25));   },
           {"pass_mt_HLT_IsoMu19_eta2p1_LooseIsoPFTau20",          "filter_mt_IsoMu19_eta2p1_LooseIsoPFTau20", "match1", "pt_1", "eta_1", "match2", "pt_2"})
        .Define("trigger_mt_IsoMu19_eta2p1_LooseIsoPFTau20_SingleL1", [](int passHLT, int passFilterBits,
                                                                int isTrigObjMatched_1, float pt_1, float eta_1,
                                                                int isTrigObjMatched_2, float pt_2) {
                                            return (int) (passHLT && passFilterBits && isTrigObjMatched_1 && isTrigObjMatched_2 &&
                                                    (pt_1 > 20) && (pt_1 < 23) && (abs(eta_1) < 2.1) &&
                                                    (pt_2 > 25));   },
           {"pass_mt_HLT_IsoMu19_eta2p1_LooseIsoPFTau20_SingleL1", "filter_mt_IsoMu19_eta2p1_LooseIsoPFTau20", "match1", "pt_1", "eta_1", "match2", "pt_2"})
        // Get 2016 nominal trigger decision for mutau
        .Define("passMuTauTrigger", [](int single1, int single2, int single3, int single4, int cross1, int cross2) { return (int) (single1 || single2 || single3 || single4 || cross1 || cross2);},
          {"trigger_mt_IsoMu22", "trigger_mt_IsoMu22_eta2p1", "trigger_mt_IsoTkMu22_eta2p1", "trigger_mt_IsoTkMu22_eta2p1", "trigger_mt_IsoMu19_eta2p1_LooseIsoPFTau20", "trigger_mt_IsoMu19_eta2p1_LooseIsoPFTau20_SingleL1"})

        ///////////////// ETau: only one single trigger: HLT Ele25 eta2p1 WPTight Gsf v
        // Eta cut was already applied
        .Define("filter_et_Ele25",  [](int ch, int filterBits1) { if (ch == Helper::et) return (int) (filterBits1 & 2); else return ((int) 0); }, {"channel", "filterBits1"})
        .Define("trigger_et_Ele25", [](int passHLTPath, int passFilterBits, int isTrigObjMatched, float pt_1, float pt_2) {
            return (int) (passHLTPath && isTrigObjMatched && passFilterBits && (pt_1 > 26) && (pt_2 > 20)); },
                {"pass_et_HLT_Ele25_eta2p1_WPTight_Gsf", "filter_et_Ele25", "match1", "pt_1", "pt_2"})
        .Define("passETauTrigger", [](int single) { return single; }, {"trigger_et_Ele25"})


        ////////////////// EMu 2016: two cross triggers for runs B-F and MC, and two cross triggers (with DZ) for runs G-H
        // The way we set this up in hltPaths.h is that pass_em_HLT_Mu23Ele12 and pass_em_HLT_Mu8Ele23 are defined for all eras,
        // with the implication that for runs G-H these paths correspond to the "with DZ" trigger.         /
        // For the filter bits, there is no bit for electron DZ: compare
        //     https://cms-nanoaod-integration.web.cern.ch/autoDoc/NanoAODv9/2016ULpostVFP/doc_SingleMuon_Run2016H-UL2016_MiniAODv2_NanoAODv9-v1.html#TrigObj
        // and https://cms-nanoaod-integration.web.cern.ch/autoDoc/NanoAODv9/2016ULpreVFP/doc_TTToSemiLeptonic_TuneCP5_13TeV-powheg-pythia8_RunIISummer20UL16NanoAODAPVv9-106X_mcRun2_asymptotic_preVFP_v11-v1.html#TrigObj
        // so this is the same filter
        .Define("filter_em_Mu23Ele12", [](int ch, int filterBits1, int filterBits2) { if (ch == Helper::em) return (int) ((filterBits1 & 2) && (filterBits2 & 1)); else return ((int) 0); }, {"channel", "filterBits1", "filterBits2"})
        .Define("filter_em_Mu8Ele23",  [](int ch, int filterBits1, int filterBits2) { if (ch == Helper::em) return (int) ((filterBits1 & 2) && (filterBits2 & 1)); else return ((int) 0); }, {"channel", "filterBits1", "filterBits2"})
        .Define("trigger_em_Mu23Ele12", [](int passHLTPath, int passFilterBits, int isTrigObjMatched_1, int isTrigObjMatched_2, float pt_1, float pt_2) {
          return (int) (passHLTPath && passFilterBits && isTrigObjMatched_1 && isTrigObjMatched_2 && (pt_1 > 24) && (pt_2 > 13)); },
          {"pass_em_HLT_Mu23Ele12", "filter_em_Mu23Ele12", "match1", "match2", "pt_1", "pt_2"})
        .Define("trigger_em_Mu8Ele23",  [](int passHLTPath, int passFilterBits, int isTrigObjMatched_1, int isTrigObjMatched_2, float pt_1, float pt_2) {
          return (int) (passHLTPath && passFilterBits && isTrigObjMatched_1 && isTrigObjMatched_2 && (pt_1 > 13) && (pt_2 > 24)); },
          {"pass_em_HLT_Mu8Ele23", "filter_em_Mu8Ele23", "match1", "match2", "pt_1", "pt_2"})
        .Define("passEMuTrigger", [](int cross1, int cross2) { return (int) (cross1 || cross2); }, {"trigger_em_Mu23Ele12", "trigger_em_Mu8Ele23"});

    }
    else {
      return df;
    }
}


/**********************************************************/

/*
 * Get tautau trigger information:
 * - whether the filter matching is correct (filter_*)
 * - the nominal trigger decision (trigger_*) - nominal only, because it depends on the pT
 */

template <typename T>
auto GetTauTauTriggerDecisions(T &df, LUNA::sampleConfig_t &sConfig) {

    // Gets filterBits, and match1/match2
    auto df1 = GetLeadingPairTrigObjInfo(df);

    auto df2 = GetTauTauTriggerFilter_And_NominalDecisions_2018(df1, sConfig);

    auto df3 = GetTauTauTriggerFilter_And_NominalDecisions_2017(df2, sConfig);

    auto df4 = GetTauTauTriggerFilter_And_NominalDecisions_2016(df3, sConfig);

    return df4;

}


/**********************************************************/



#endif
