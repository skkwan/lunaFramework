#include "helperFunctions.h"
#include <iostream>
// n.b. Template type functions have to go in the header file

/***************************************************************************/

namespace Helper {

    bool containsSubstring(std::string string, std::string substring) {
        return (string.find(substring) != std::string::npos);
    }

    bool containsAnyOfTheseSubstrings(std::string str, std::vector<std::string> vSubstring) {
        for (std::string substring : vSubstring) {
            if (str.find(substring) != std::string::npos) {
                return true;
            }
        }
        return false;
    }

    void replaceStringInPlace(std::string& subject, const std::string& search,
			    const std::string& replace) {
        size_t pos = 0;
        while ((pos = subject.find(search, pos)) != std::string::npos) {
            subject.replace(pos, search.length(), replace);
            pos += replace.length();
        }
    }


    // Return the value with index idx from branch b (floats). If idx is invalid
    // (-1), return -9999.
    float GetFloat(ROOT::RVec<float>& b, int idx) {
        float nullValue = -9999;
        if (idx != -1)
        return b[idx];
        else
        return nullValue;
    }

    // Same as above, but for a branch b of ints.
    int GetInt(ROOT::RVec<int>& b, int idx) {
        int nullValue = -9999;
        if (idx != -1)
        return b[idx];
        else
        return nullValue;
    }

    float compute_deltaPhi(float phi1, float phi2, const double c) {
        auto r = std::fmod(phi2 - phi1, 2.0 * c);
        if (r < -c) {
        r += 2.0 * c;
        }
        else if (r > c) {
        r -= 2.0 * c;
        }
        return r;
    }

    float compute_deltaR(float eta1, float eta2, float phi1, float phi2) {
        auto deltaR = sqrt(pow(eta1 - eta2, 2) + pow(compute_deltaPhi(phi1, phi2), 2));
        return deltaR;
    }

    bool logicalOR(bool b1, bool b2, bool b3, bool b4) {
        return (b1 || b2 || b3 || b4);
    }

    /*
     * Return value by channel.
     */
    float getValueByChannelWithDefault(int channel, float val_default, float val_mt, float val_et, float val_em) {
        if      (channel == Helper::mt) { return val_mt; }
        else if (channel == Helper::et) { return val_et;  }
        else if (channel == Helper::em) { return val_em;  }
        else { return val_default; }
    }

    float getValueByChannel(int channel, float val_mt, float val_et, float val_em) {
        if      (channel == Helper::mt) { return val_mt; }
        else if (channel == Helper::et) { return val_et;  }
        else if (channel == Helper::em) { return val_em;  }
        else {
            std::cout << "[ERROR:] Event does not belong to one of the three channels!" << std::endl;
            return 1.0;
        }
    }

}


/***************************************************************************/
