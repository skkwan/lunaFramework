/*
 * Implementation of the skimming step of the analysis
 *
 * The skimming step reduces the inital generic samples to a dataset optimized
 * for this specific analysis. Most important, the skimming removes all events
 * from the initial dataset, which are not of interest for our study and builds
 * from the reconstructed muons and taus a valid pair, which may originate from
 * the decay of a Higgs boson.
 */

// #include "ROOT/RDataFrame.hxx"
#include <ROOT/RVec.hxx>

#include <Math/Vector4D.h>
#include <TChain.h>
#include <TStopwatch.h>

#include <stdlib.h>
#include <string>
#include <vector>
#include <iostream>
#include <cmath>

#include "helpers/bTagJets.h"
#include "helpers/cascade.h"
#include "helpers/categoryVariables.h"
#include "helpers/computePhysicsVariables.h"
#include "helpers/eventLevelVariables.h"
#include "helpers/extraLeptons.h"
#include "helpers/fileIO.h"
#include "helpers/genFinalVariables.h"
#include "helpers/genLevelMuTauh.h"
#include "helpers/genTauTau.h"
#include "helpers/helperFunctions.h"
#include "helpers/jets.h"
#include "helpers/matchRecoJetsToGenBB.h"
#include "helpers/pfMET.h"
#include "helpers/runsTTree.h"
#include "helpers/sampleConfig_class.h"
#include "helpers/tau.h"
#include "helpers/triggerTauTau.h"

/*
 * Main function of the skimming step of the analysis
 *
 * The function loops over the input sample, reduces the content to the
 * interesting events and writes them to new files.
 */
int main(int argc, char **argv) {

    if (! ((argc == 8) || (argc == 10))) {
      std::cout << argc << std::endl;
      std::cout << "[Error:] Insufficient or wrong number of arguments: argc is " << argc << ", expecting argc = 8 or 10. Use executable with following arguments: ./skim sample_name input output config_dir cross_section integrated_luminosity scale [optional: start line in .list] [optional: # of lines to process in .list]" << std::endl;
      return -1;
    }

    //ROOT::EnableImplicitMT(); // Tell ROOT we want to go parallel

    sampleName_ = argv[1];      // global variable declared in fileIO.h
    std::cout << ">>> Sample name: " << sampleName_ << std::endl;

    std::string input = argv[2];
    std::cout << ">>> Process input: " << input << std::endl;

    configDir_ = argv[4];
    std::cout << ">>> Configuration directory: " << configDir_ << std::endl;

    long int startLine = -1;
    long int numLines = -1;
    if (argc == 10) {
      startLine = atol(argv[8]);
      numLines  = atol(argv[9]);
    }

    TStopwatch time;
    time.Start();

    TChain *ch = new TChain("Events");
    TChain *chRuns = new TChain("Runs");
    // Prepare the TChains. If startLine and numLines are unspecified, use this C-style approach to adding
    // all the lines in the input file.
    ifstream file;
    std::cout << "Before opening input " << input << std::endl;
    file.open(input, ifstream::in);
    std::string line;
    std::cout << "Input " << input << " read successfully" << std::endl;

    if (startLine == -1 || numLines == -1) {
      std::cout << "Will read all lines" << std::endl;
      // Read all lines
      for (; std::getline(file, line); ) {
      const char *filename = line.c_str();
      int result = ch->Add(filename);
      int result2 = chRuns->Add(filename);
      if (result == 0 || result2 == 0) {
        std::cout << "[ERROR:] skim.cxx: Failed to find file Events TTree or Runs TTree" << std::endl;
        break;
      }
      std::cout << "Added " << filename
          << " Events and Runs TTRees with (1 for success): "
          << result << " " << result2 << endl;
          }
    }
    else {
      // Only read lines (startLine, startLine + numLines)
      // Iterate std::getline past the first nth lines
      for (int i = 1; i < startLine; i++ )  {
    	std::getline(file, line);
    	std::cout << "[~~] skipping a line" << std::endl;
      }

      // The nth time std::getline is called, it will be on line n
      for (int i = 0; i < numLines; i++ ) {

    	std::getline(file, line);
    	const char *filename = line.c_str();
    	int result = ch->Add(filename);
        int result2 = chRuns->Add(filename);
        if (result == 0 || result2 == 0) {
    	  std::cout << "[ERROR:] gen.cxx: Failed to access the file: check that startLine " << startLine
    		    << " and numLines " << numLines
    		    << " are within bounds, and that the trees Events and Runs exist" << std::endl;
          break;
        }

    	std::cout << "Added: " << filename << endl;
      }


    }

    // temp: test fewer events

    ROOT::RDataFrame df(*ch);
    ROOT::RDataFrame dfRuns(*chRuns);

    const auto numEvents = *df.Count();
    std::cout << "Number of events: " << numEvents << std::endl;

    const auto xsec = atof(argv[5]);
    std::cout << "Cross-section: " << xsec << std::endl;

    const auto lumi = atof(argv[6]);
    std::cout << "Integrated luminosity: " << lumi << std::endl;

    const auto scale = atof(argv[7]);
    std::cout << "Global scaling: " << scale << std::endl;

    float genEventSumw  = (float) SumGenEventSumw(dfRuns);   // if data, this will be set to a placeholder value 1.0
    float genEventCount = (float) SumGenEventCount(dfRuns);  // if data, this will be set to a placeholder value 1.0

    // Store genEventSumw and genEventCount in a TH1F histogram hRuns.
    TH1F *hRuns = new TH1F("hRuns", "hRuns", 2, 0, 2);
    hRuns->SetBinContent(1, genEventSumw);
    hRuns->SetBinContent(2, genEventCount);

    std::cout << sampleName_ << ": "
	      << "Cross-section: " << xsec         << ", "
	      << "Integrated luminosity: " << lumi << ", "
	      << "Number of events: " << numEvents << ", "
	      << "Global scaling: " << scale       << ", "
	      << "genEventSumw = " << genEventSumw << ", "
	      << "genEventCount = " << genEventCount << ", "
	      << "(If data, should be 1.0 placeholder value) " << std::endl;

    LUNA::sampleConfig_t sConfig = LUNA::sampleConfig_t(sampleName_);

    auto df2 = GetGenRunLumiEvent(df);
    auto df3 = AddEventWeightAndPUInfo(df2, sConfig, xsec, numEvents, genEventCount, genEventSumw, scale);
    auto df4 = GetCascadeInformation(df3, sampleName_);
    auto df5 = MatchRecoJetsToGenBB(df4, sampleName_);

    auto dfFinal = df5;
    auto report = dfFinal.Report();

    const std::string output = argv[3];
    std::cout << ">>> gen.cxx: Output name: " << output << std::endl;

    dfFinal.Snapshot("mutau_tree", output, genFinalVariables);
    time.Stop();

    report->Print();
    time.Print();
}
