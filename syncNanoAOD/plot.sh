1;95;0c

#!/bin/bash

HIST_LIST=$1
RUNNER=$2

HISTOGRAMS_DIR="histograms"
OUTPUT_FOLDER="plots"
EOS_FOLDER="/eos/user/s/skkwan/hToAA/syncNanoAOD/plots"

if [ "$RUNNER" = GITLAB ]; then
    echo ">>> histograms.sh: Is running in Gitlab ..."
    EOS_FOLDER="root://eosuser.cern.ch/"${EOS_FOLDER}
fi


# Produce plots from histograms
while IFS=, read -r SAMPLE YEAR PROCESS SCALE
do
    INPUT_DIR=${HISTOGRAMS_DIR}/${YEAR}/histograms_${PROCESS}.root
    OUTPUT_DIR=${OUTPUT_FOLDER}/${YEAR}/${PROCESS}
    mkdir -p ${OUTPUT_DIR}
    echo ">>> Plotting ${INPUT_DIR} for ${SAMPLE} with scale ${SCALE}, output to ${OUTPUT_DIR}"
    python plot.py $INPUT_DIR $OUTPUT_DIR $SCALE $PROCESS

    # cp everything in the output directory to EOS
    EOS_FULL=${EOS_FOLDER}/${YEAR}/${PROCESS}/
    mkdir -p ${EOS_FULL}
    cp ${OUTPUT_DIR}/*.pdf ${EOS_FULL}
    cp ${OUTPUT_DIR}/*.png ${EOS_FULL}
done < ${HIST_LIST}
