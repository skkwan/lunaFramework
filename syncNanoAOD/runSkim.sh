# Usage:
#  bash runSkim.sh

SAMPLES="skim_test.csv"
COMPILE=false
EXECUTE=true
IS_LOCAL=1

#--------------------------------------------------------
# Check if we used bash
#--------------------------------------------------------
if [[ "$0" != "$BASH_SOURCE" ]]; then
    echo ">>> ${BASH_SOURCE[0]}: Error: Script must be run with bash"
    return
fi
#--------------------------------------------------------
# Check if environments are set
#--------------------------------------------------------
if [[ -z ${CMSSW_BASE} ]]; then
    echo ">>> ${BASH_SOURCE[0]}: CMSSW environment is not set! Make sure to do cmsenv"
    exit 1
fi
if [[ -z ${VIRTUAL_ENV} ]]; then
    echo ">>> ${BASH_SOURCE[0]}: Virtual environment is not set! Make sure to source venv-LUNA/bin/activate"
    exit 1
fi


#####################################################
# Build skim executable
#####################################################
if [[ "${COMPILE}" == true ]]; then
    TARGET_FILES="helpers/sampleConfig_class.cc helpers/genHelper.cc helpers/computePhysicsVariables.cc helpers/helperFunctions.cc helpers/lumiMask.cc"
    COMPILER=$(root-config --cxx)
    FLAGS="$(root-config --cflags --libs) $(correction config --cflags --ldflags)"
    time $COMPILER -g -O3 -Wall -Wextra -Wpedantic -o skim skim.cxx $FLAGS ${TARGET_FILES}
    if [[ $? -ne 0 ]]; then
        echo ">>> Compile failed, exit"
        exit 1
    fi
fi

#####################################################
# Check that the input .list files exist first!
#####################################################
echo ">>> ${BASH_SOURCE[0]}: Reading from ${SAMPLES}..."
while IFS=, read -r SAMPLE INPUT_PATH YEAR CONFIG_DIR
do
    echo ">>> ${BASH_SOURCE[0]}: Skim sample ${SAMPLE}: with config directory ${CONFIG_DIR} and input list of files: ${INPUT_PATH}.list"
    INPUT_DIR="${INPUT_PATH}.list"
    if [[ -f ${INPUT_DIR} ]]; then
	echo "   >>> ${INPUT_DIR} exists"
    else
	echo "   >>> ${BASH_SOURCE[0]}: [ERROR]: Input list of files not found, exit"
    exit 1
    fi
done < ${SAMPLES}

#####################################################
# Call the executable
#####################################################
if [[ "${EXECUTE}" == true ]]; then
    DATETIME="$(date +"%Y-%m-%d-%Hh%Mm")"

    EOS_DIR="/eos/user/s/skkwan/hToAA/skims"
    EOSDIR=${EOS_DIR}/${DATETIME}/
    mkdir -p ${EOSDIR}

    # Access the .csv list of samples to run over
    while IFS=, read -r SAMPLE INPUT_PATH YEAR CONFIG_DIR
    do
	{
	    OUTPUT=${SAMPLE}_Skim.root
	    LOG=${EOSDIR}${SAMPLE}_Skim.log
	    INPUT_DIR="${INPUT_PATH}.list"

	    echo "--- Starting executable for ${SAMPLE}: [Time: $(TZ=America/New_York date)] --- "
	    echo ">>> ${BASH_SOURCE[0]}: Writing to ${OUTPUT}, log in ${LOG}"
	    ./skim $SAMPLE "${INPUT_DIR}" $OUTPUT $CONFIG_DIR $XSEC $LUMI ${IS_LOCAL} | tee $LOG
	    mv $OUTPUT ${EOSDIR}${OUTPUT}

	} &

    done < ${SAMPLES}
    wait
    echo ">>> Check ${EOSDIR} for outputs"
else
    echo ">>> Execute flag was false"
fi
