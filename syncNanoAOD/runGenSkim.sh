# runGenSkim.sh

COMPILE=true
SAMPLES="skim_gen.csv"
EXECUTE=false

#--------------------------------------------------------
# Check if we used bash
#--------------------------------------------------------
if [[ "$0" != "$BASH_SOURCE" ]]; then
    echo ">>> ${BASH_SOURCE[0]}: Error: Script must be run with bash"
    return
fi
#--------------------------------------------------------
# Check if environments are set
#--------------------------------------------------------
if [[ -z ${CMSSW_BASE} ]]; then
    echo ">>> ${BASH_SOURCE[0]}: CMSSW environment is not set! Make sure to do cmsenv"
    exit 1
fi
if [[ -z ${VIRTUAL_ENV} ]]; then
    echo ">>> ${BASH_SOURCE[0]}: Virtual environment is not set! Make sure to source venv-LUNA/bin/activate"
    exit 1
fi


#####################################################
# Build custom class dictionary
#####################################################

cd helpers/
CUSTOM_OBJ_DICT="custom_dict.cxx"
CUSTOM_OBJ_HEADERS="genParticle_class.h genCascade_class.h"
rootcling -f ${CUSTOM_OBJ_DICT} ${CUSTOM_OBJ_HEADERS} linkdef.h
if [[ $? -ne 0 ]]; then
    echo ">>> rootcling failed, exit"
    exit 1
fi
cd ../

#####################################################
# Build genSkim executable
#####################################################
if [[ "$COMPILE" = true ]]; then
    TARGET_FILES="helpers/sampleConfig_class.cc helpers/genCascade_class.cc helpers/genParticle_class.cc helpers/${CUSTOM_OBJ_DICT} helpers/genHelper.cc helpers/computePhysicsVariables.cc helpers/helperFunctions.cc"
    COMPILER=$(root-config --cxx)
    FLAGS=$(root-config --cflags --libs)
    time $COMPILER -g -O3 -Wall -Wextra -Wpedantic -o genSkim gen.cxx $FLAGS ${TARGET_FILES}
fi

#####################################################
# Check that the input .list files exist
#####################################################
echo ">>> ${BASH_SOURCE[0]}:  Reading from ${SAMPLES}..."
while IFS=, read -r SAMPLE INPUT_PATH YEAR CONFIG_DIR XSEC LUMI SCALE
do
    echo ">>> ${BASH_SOURCE[0]}:  Skim sample ${SAMPLE}: with config directory ${CONFIG_DIR} and input path of files: ${INPUT_PATH}"
    INPUT_DIR="${INPUT_PATH}.list"
    if [[ -f ${INPUT_DIR} ]]; then
	echo "   >>> ${INPUT_DIR} exists"
    else
	echo "   >>> ${BASH_SOURCE[0]}: [ERROR]: Input list of files ${INPUT_DIR} not found"
	exit 1
    fi
done < ${SAMPLES}

#####################################################
# Set up output area
#####################################################
DATETIME="$(date +"%Y-%m-%d-%Hh%Mm")"

EOS_DIR="/eos/user/s/skkwan/hToAA/genSkims"
EOSDIR=${EOS_DIR}/${DATETIME}/

#####################################################
# Loop over physics samples and call the genSkim executable
#####################################################
while IFS=, read -r SAMPLE INPUT_PATH YEAR CONFIG_DIR XSEC LUMI SCALE
do
    {
	OUTPUT="${EOSDIR}${SAMPLE}_GenSkim.root"
	LOG="${EOSDIR}${SAMPLE}_GenSkim.log"

	INPUT_DIR="${INPUT_PATH}.list"

	echo ">>> ${BASH_SOURCE[0]}: Writing to ${OUTPUT}, log in ${LOG}"
    if [[ "$EXECUTE" = true ]]; then
        mkdir -p ${EOSDIR}
        COMMAND="./genSkim $SAMPLE "${INPUT_DIR}" $OUTPUT $CONFIG_DIR $XSEC $LUMI $SCALE | tee $LOG"
        echo "Attempting ${COMMAND}..."
	    eval ${COMMAND}
    else
        echo ">>> Execute flag false, did not execute"
    fi

    } &

done < ${SAMPLES}

wait
