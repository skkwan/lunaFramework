#!/bin/bash

# Compare two n-tuples and copy the resulting .pdf and .csv files to the specified EOS area/directory.
# See the documentation in compare.py (stolen from HTT 2015 GitHub: https://github.com/CMS-HTT/2015-sync/blob/master/compare.py) for more command
# line arguments and use cases.

# Example command:
#      python compare.py /eos/user/s/skkwan/hToAA/syncNanoAOD/102X_RunIIAutumn18/sync_ntuples/skim_vbf_2018_stephanie_jetPUid_80X.root /eos/user/s/skkwan/hToAA/syncNanoAOD/102X_RunIIAutumn18/sync_ntuples/skim_vbf_2018_stephanie_jetPUid_102X.root -t EightyX,HundredTwoX

FIRST_NTUP="/eos/user/s/skkwan/hToAA/skims/Jun-14-2023-14h46m-Benchmark-Sync/VBFHToTauTau_Skim.root"

SECOND_NTUP="/eos/user/s/skkwan/hToAA/skims/Jun-14-2023-14h46m-Benchmark-Sync/VBFHToTauTau_Skim.root"

# Descriptive names of the n-tuples (will show up in the comparison plot legends). Can't have numbers in these.
FIRST_NAME="LUNA_mutau"
SECOND_NAME="LUNA_etau"

# Destination directory where the .pdfs will be copied to.
EXPORTDIR="/eos/user/s/skkwan/hToAA/unitTests/compareOutputs"
mkdir -p ${EXPORTDIR}

# Options for compare.py See the original compare.py for a full description.
varslist=("pt_1")
for var in ${varslist[*]}; do
   OPTIONS="--diff --var-diff="${var}""
   cmd="python compare.py ${OPTIONS} "${FIRST_NTUP}" "${SECOND_NTUP}" --titles="${FIRST_NAME}","${SECOND_NAME}""
   echo ">> Executing ${cmd}"
   eval ${cmd}

   if [[ -f common.pdf ]]; then
      echo ">>> Copying outputs to ${EXPORTDIR}..."
      cp common.pdf "${EXPORTDIR}/${var}_common.pdf"
      cp sync.pdf "${EXPORTDIR}/${var}_sync.pdf"
      cp intersect.pdf "${EXPORTDIR}/${var}_intersect.pdf"
      # cp ${FIRST_NAME}.csv "${EXPORTDIR}/${var}_${FIRSTNAME}.csv"
      # cp ${SECOND_NAME}.csv "${EXPORTDIR}/${var}_${SECONDNAME}.csv"
   fi
done
