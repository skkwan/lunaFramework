/*
 * Implementation of the skimming step of the analysis
 *
 * The skimming step reduces the inital generic samples to a dataset optimized
 * for this specific analysis. Most important, the skimming removes all events
 * from the initial dataset, which are not of interest for our study and builds
 * from the reconstructed muons and taus a valid pair, which may originate from
 * the decay of a Higgs boson.
 */


#include "ROOT/RDataFrame.hxx"
#include "ROOT/RSnapshotOptions.hxx"
#include "ROOT/RVec.hxx"
#include "correction.h"

#include "Math/Vector4D.h"
#include "TChain.h"
#include "TROOT.h"
#include "TStopwatch.h"

#include <stdlib.h>
#include <string>
#include <vector>
#include <iostream>
#include <cmath>

#include "helpers/assignChannels.h"
#include "helpers/bTagJets.h"
#include "helpers/categoryVariables.h"
#include "helpers/computePhysicsVariables.h"
#include "helpers/cleaning.h"
#include "helpers/electronES.h"
#include "helpers/encapsulateVariedTauTau.h"
#include "helpers/eventLevelVariables.h"
#include "helpers/extraLeptons.h"
#include "helpers/fileIO.h"
#include "helpers/finalVariables.h"
#include "helpers/genHelper.h"
#include "helpers/genTauTau.h"
#include "helpers/genLevelEmbed.h"
#include "helpers/genLevelTopQuark.h"
#include "helpers/getSampleConfigurationInfo.h"
#include "helpers/helperFunctions.h"
#include "helpers/hltPaths.h"
#include "helpers/inclusiveJetsSamples.h"
#include "helpers/jets.h"
#include "helpers/leadingOfflineObjects.h"
#include "helpers/leadingTauTau.h"
#include "helpers/leadingTauTauGenInfo.h"
#include "helpers/looseSelectionOfflineObjects.h"
#include "helpers/lumiMask.h"
#include "helpers/metFilters.h"
#include "helpers/metRecoilCorrections.h"
#include "helpers/MEtSys.h"
#include "helpers/metXYShiftCorrection.h"
#include "helpers/muonES.h"
#include "helpers/overlapEmbedded.h"
#include "helpers/pfMET.h"
#include "helpers/prefiringWeights.h"
#include "helpers/tau.h"
#include "helpers/tauES.h"
#include "helpers/triggerTauTau.h"
#include "helpers/RecoilCorrector.h"
#include "helpers/recoilCorrectionsInfo.h"
#include "helpers/runsTTree.h"
#include "helpers/sampleConfig_class.h"

using correction::CorrectionSet;

/**********************************************************/

/*
 * Wrapper for functions that need to be performed for each channel
 */

template <typename T>
auto PerformSkimming(T &df, LUNA::sampleConfig_t &sConfig, TString jsonDir, TString jsonDirEmbed, std::string configDir, RecoilCorrector &recoilPFMetCorrector_, MEtSys &wMetSys_) {

  auto dfBasic_7 = FilterLooseBaselineObjects(df);
  auto dfBasic_8 = FilterByLeadingOfflineObjects(dfBasic_7);
  auto dfBasic_9 = GetLeadingTauTauPairInfo(dfBasic_8, sConfig);
  auto dfBasic_10 = FilterOSByChannel(dfBasic_9);  // defined in leadingTauTau.h
  auto dfBasic_11 = FilterOfflineTauTauPropertiesByChannel(dfBasic_10);
  auto dfBasic_12 = FilterMCOverlapWithEmbedded(dfBasic_11, sConfig);
  auto dfBasic_13 = FilterThirdLeptonAndDiLeptonVetoes(dfBasic_12);
  auto dfBasic_14 = GetTauTauTriggerDecisions(dfBasic_13, sConfig);
  auto dfBasic = dfBasic_14;
  auto dfLepton_1 = GetElectronEnergyScaleShifts(dfBasic, sConfig, jsonDir, jsonDirEmbed);
  auto dfLepton_2 = GetMuonEnergyScaleShifts(dfLepton_1, sConfig);
  auto dfLepton_3 = GetTauEnergyScaleShifts(dfLepton_2, sConfig, jsonDir);
  auto dfLepton_4 = EncapsulateVariedTauTau(dfLepton_3, sConfig);
  auto thisDf = dfLepton_4;
  auto df1 = FindGoodBTaggedJets(thisDf, configDir);
  auto df2 = FindLeadingBTaggedJetPair(df1, sConfig);
  auto df3 = FindGoodJets(df2, configDir);
  auto df4 = FindLeadingJetPair(df3);
  auto df5 = GetGenTopQuarks(df4, sConfig);
  auto df6 = GetGenRecoilBoson(df5, sConfig, configDir);   // also gets the Z boson gen mass and pT, later used for Z pT reweighing
  auto df7 = GetMETRecoilCorrections(df6, sConfig, recoilPFMetCorrector_, wMetSys_);
  auto df8 = ComputeCategoryVariables(df7, sConfig);
  auto df9 = GetPrefiringWeights(df8, sConfig);
  auto df10 = GetEmbedGenLevelLegs(df9, sConfig);
  auto df11 = GetInclusiveJetsSamplesInfo(df10, sConfig);

  return df11;
}

/**********************************************************/

/*
 * Main function of the skimming step of the analysis
 *
 * The function loops over the input sample, reduces the content to the
 * interesting events and writes them to new files.
 */
int main(int argc, char **argv) {

  // for (int count{ 0 }; count < argc; ++count) {
  //      std::cout << count << " " << argv[count] << '\n';
  //}

    if (argc != 6) {
      std::cout << argc << std::endl;
      std::cout << "[Error:] Insufficient or wrong number of arguments: argc is " << argc << ", expecting argc = 6 Use executable with following arguments: ./skim sample_name input output config_dir isLocalJob" << std::endl;
      return -1;
  }

    std::cout << "[ROOT version:] " << gROOT->GetVersion() << std::endl;

    ROOT::EnableImplicitMT(); // Tell ROOT we want to go parallel

    std::string sampleName = argv[1];
    std::string input = argv[2];
    const std::string output = argv[3];
    std::string configDir = argv[4];
    int isLocalJob = (int) atoi(argv[5]);

    std::cout << ">>> Sample name: " << sampleName << std::endl;
    std::cout << ">>> Process input: " << input << std::endl;
    std::cout << ">>> Is a local job (0 is False): " << isLocalJob << std::endl;
    std::cout << ">>> skim.cxx: Output name: " << output << std::endl;
    std::cout << ">>> Configuration directory: " << configDir << std::endl;

    TStopwatch time;
    time.Start();

    TChain *ch = new TChain("Events");
    TChain *chRuns = new TChain("Runs");

    // Prepare the TChains. Use this C-style approach to adding
    // all the lines in the input file.
    std::ifstream file(input);
    if (file.is_open()) {
        std::string line;
        while (std::getline(file, line)) {
            std::string pathToFile = line;
            if (!isLocalJob) {
              // If batch job, assume job has copied the file to run on to the worker node
              pathToFile = line.substr(line.find_last_of("/") + 1);
            }
            std::cout << pathToFile << "\n" << std::endl;
            int result = ch->Add(pathToFile.c_str());
            int result2 = chRuns->Add(pathToFile.c_str());
            if (result == 0 || result2 == 0) {
            std::cout << "[ERROR:] skim.cxx: Failed to access the file at " << pathToFile << " from " << line << std::endl;
              break;
            }
        }
        file.close();
    }

    ROOT::RDataFrame df(*ch);
    ROOT::RDataFrame dfRuns(*chRuns);

    // Set configuration information
    LUNA::sampleConfig_t sConfig = LUNA::sampleConfig_t(sampleName);
    sConfig.setDoSystematics(true);
    std::string era = WhichEra(configDir);
    sConfig.setEra(era);  // sets era and year

    float numEvents = (float) *df.Count();

    // Store the number of processed events in a TH1F histogram hEvents.
    TH1F *hEvents = new TH1F("hEvents", "hEvents", 1, 0, 1);
    hEvents->SetBinContent(1, numEvents);

    float genEventSumw  = (float) SumGenEventSumw(dfRuns, sConfig);   // if data, this will be set to a placeholder value 1.0
    float genEventCount = (float) SumGenEventCount(dfRuns, sConfig);  // if data, this will be set to a placeholder value 1.0

    // Store genEventSumw and genEventCount in a TH1F histogram hRuns.
    TH1F *hRuns = new TH1F("hRuns", "hRuns", 2, 0, 2);
    hRuns->SetBinContent(1, genEventSumw);
    hRuns->SetBinContent(2, genEventCount);


    std::cout << sampleName << ": "
            << "Era: " << era << ", "
    	      << "Number of events: " << numEvents << ", "
    	      << "genEventSumw = " << genEventSumw << ", "
    	      << "genEventCount = " << genEventCount << ", "
    	      << "(If data, should be 1.0 placeholder value) " << std::endl;

    // Global variable in metRecoilCorrections.h
    TString recoilDir = "RecoilCorrections/data/";
    TString recoilCorrFileName = recoilDir + "TypeI-PFMet_Run2018.root";
    TString recoilSysFileName = recoilDir + "PFMEtSys_2018.root";
    if (sConfig.year() == 2017)      {
      recoilCorrFileName = recoilDir + "TypeI-PFMET_2017.root";
      recoilSysFileName = recoilDir + "PFMEtSys_2017.root";
    }
    else if (sConfig.year() == 2016) {
      recoilCorrFileName = recoilDir + "TypeI-PFMet_Run2016_legacy.root";
      recoilSysFileName = recoilDir + "PFMEtSys_2016.root";
    }
    RecoilCorrector recoilPFMetCorrector_(recoilCorrFileName.Data()); // Type I PF MET
    MEtSys wMetSys_(recoilSysFileName.Data());

    /**** [Start getting external histograms/files] ******************/
    TString localCommonFilesDir = "../commonFiles/";
    TString jsonDir = "/afs/cern.ch/work/s/skkwan/public/jsonpog-integration/";
    TString jsonDirEmbed = "/afs/cern.ch/work/s/skkwan/public/CROWN-data/data/embedding/";
    if (!isLocalJob) {
        localCommonFilesDir = ""; // use Condor transfer input files to move files to the sandbox
        jsonDir = "";
        jsonDirEmbed = "";
    }

    // Loading Golden JSON for lumi mask
    TString lumiJsonfile = localCommonFilesDir + "lumi/Cert_314472-325175_13TeV_Legacy2018_Collisions18_JSON.txt";
    if (sConfig.year() == 2017) { lumiJsonfile = localCommonFilesDir + "lumi/Cert_294927-306462_13TeV_UL2017_Collisions17_GoldenJSON.txt";  }
    else if (sConfig.year() == 2016){ lumiJsonfile = localCommonFilesDir + "lumi/Cert_271036-284044_13TeV_Legacy2016_Collisions16_JSON.txt"; }
    const auto myLumiMask = lumiMask::fromJSON(lumiJsonfile.Data());
    //std::cout << "Testing the JSON! Known good run/lumi returns: " << myLumiMask.accept(315257, 10) << ", and known bad run returns: " << myLumiMask.accept(315257, 90) << std::endl;
    bool isData = sConfig.isData();
    auto passesGoldenJSON = [myLumiMask, isData](unsigned int &run, unsigned int &luminosityBlock) {
      if (isData) {
        return (bool) myLumiMask.accept(run, luminosityBlock);
      }
      return true;
    };

    /**** [End getting external histograms/files] ********************/

    auto dfBasic_0 = df.Define("zero", [](){ return 0.0f; })   // kind of weird but we need it for add_p4
                       .Define("era", [era](){ return era; })
                       .Define("passesJSON", passesGoldenJSON, {"run", "luminosityBlock"})
                       .Filter("passesJSON", "[main] Is MC, or is data passing Golden JSON");
    auto dfBasic_1 = ApplyMETFilters(dfBasic_0, sConfig);
    auto dfBasic_2 = GetSampleConfigurationInfo(dfBasic_1, sConfig);
    auto dfBasic_3 = GetRunLumiEvent(dfBasic_2);
    auto dfBasic_4 = AddEventWeightAndPUInfo(dfBasic_3, sConfig, numEvents, genEventCount, genEventSumw);
    auto dfBasic_5 = GetPFMETVariables(dfBasic_4, sConfig);     // need met and metphi before lepton energy scale
    auto dfBasic_6 = ApplyMETXYShift(dfBasic_5, sConfig);

    auto dfBasic_7 = FilterUsingHLTPaths(dfBasic_6, sConfig);
    auto dfBasic_8 = CleanOfflineObjects(dfBasic_7, sConfig);
    auto dfBasic = AssignChannels(dfBasic_8, sConfig);
    auto dfSkimmed = PerformSkimming(dfBasic, sConfig, jsonDir, jsonDirEmbed, configDir, recoilPFMetCorrector_, wMetSys_);

    auto df_mt = dfSkimmed.Filter([](int channel) { return channel == Helper::mt; }, {"channel"}, "main: is_mt: is mutau channel?");
    auto df_et = dfSkimmed.Filter([](int channel) { return channel == Helper::et; }, {"channel"}, "main: is_et: is etau channel?");
    auto df_em = dfSkimmed.Filter([](int channel) { return channel == Helper::em; }, {"channel"}, "main: is_em: is emu channel?");

    auto report_mt = df_mt.Report();
    auto report_et = df_et.Report();
    auto report_em = df_em.Report();

    // list of variables to save are in finalVariables.h
    std::vector<std::string> allCommonVariables = commonVariables;
    std::cout << ">>> skim.cxx: Access dictYearSpecificVariables " << std::to_string(sConfig.year()) << std::endl;
    allCommonVariables.insert(allCommonVariables.end(), dictYearSpecificVariables[std::to_string(sConfig.year())].begin(),
                                                        dictYearSpecificVariables[std::to_string(sConfig.year())].end());
    SnapshotFinalVariables(df_mt, "mutau_tree", output, allCommonVariables, optionalVariables, jetSysVariables);
    SnapshotFinalVariables(df_et, "etau_tree",  output, allCommonVariables, optionalVariables, jetSysVariables);
    SnapshotFinalVariables(df_em, "emu_tree",   output, allCommonVariables, optionalVariables, jetSysVariables);

    report_mt->Print();
    report_et->Print();
    report_em->Print();

    // Write the other histograms
    TFile *f = new TFile(output.c_str(), "update");
    f->cd();
    hEvents->Write();
    hRuns->Write();
    f->Close();

    time.Stop();
    time.Print();
}
