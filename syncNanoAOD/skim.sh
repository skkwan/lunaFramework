#!/bin/bash

# Usage: bash skim.sh [COMPILE or NO_COMPILE] [samples.csv] [GITLAB or LXPLUS] [RUNSKIM or DRYRUN]

COMPILE=$1
SAMPLES=$2
RUNNER=$3
RUNSKIM=$4

# Compile executable
if [ "$COMPILE" = COMPILE ]; then
    echo ">>> skim.sh: Compile skimming executable ..."
    COMPILER=$(root-config --cxx)
    FLAGS=$(root-config --cflags --libs)
    time $COMPILER -g -O3 -Wall -Wextra -Wpedantic -o skim skim.cxx $FLAGS
fi

# Check that the input .list files exist first!
echo ">>> skim.sh: Reading from ${SAMPLES}..."
while IFS=, read -r SAMPLE INPUT_PATH YEAR CONFIG_DIR XSEC LUMI SCALE
do
    echo ">>> skim.sh: Skim sample ${SAMPLE}: with config directory ${CONFIG_DIR} and input list of files: ${INPUT_PATH}.list"
    INPUT_DIR="${INPUT_PATH}.list"
    if [[ -f ${INPUT_DIR} ]]; then
	echo "   >>> ${INPUT_DIR} exists"
    else
	echo "   >>> skim.sh: [ERROR]: Input list of files not found"
	exit 1
    fi

done < ${SAMPLES}

# Get the current date and time, needed for output files

if [ "$RUNSKIM" = RUNSKIM ]; then
    DATETIME="$(date +"%b-%d-%Y-%Hh%Mm")"

    EOS_DIR="/eos/user/s/skkwan/hToAA/skims"
    #EOS_DIR="/hdfs/store/user/skkwan/hToAA/skims"
    EOSDIR=${EOS_DIR}/${DATETIME}/
    mkdir -p ${EOSDIR}

    echo "# README for ${DATETIME}" | cat - README.md > temp && mv temp ${EOSDIR}/README.md
    echo  ">>> skim.sh: Wrote ${EOSDIR}/README.md"


    # Access the .csv list of samples to run over. For now: do not use NFILESPERJOB in interactive setup
    while IFS=, read -r SAMPLE INPUT_PATH YEAR CONFIG_DIR XSEC LUMI SCALE NFILESPERJOB
    do
	{
	    # OUTPUT=${EOSDIR}${SAMPLE}Skim.root
	    OUTPUT=${SAMPLE}_Skim.root
	    LOG=${EOSDIR}${SAMPLE}_Skim.log
	    INPUT_DIR="${INPUT_PATH}.list"

	    echo "--- Starting executable for ${SAMPLE}: [Time: $(TZ=America/New_York date)] --- "
	    echo ">>> skim.sh: Writing to ${OUTPUT}, log in ${LOG}"
	    ./skim $SAMPLE "${INPUT_DIR}" $OUTPUT $CONFIG_DIR $XSEC $LUMI $SCALE | tee $LOG
	    mv $OUTPUT ${EOSDIR}${OUTPUT}

	} &

    done < ${SAMPLES}

    wait
    echo ">>> Check ${EOSDIR} for outputs"
fi

if [ "$RUNSKIM" = DRYRUN ]; then
    echo "Dryrun only, did not call skim executable"
fi
