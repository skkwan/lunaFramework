#!/bin/bash

#Usage: bash histograms.sh [histograms.csv] [GITLAB or LXPLUS]

HIST_LIST=$1
RUNNER=$2

INPUT_DIR="skims"
OUTPUT_DIR="histograms"
EOS_DIR="/eos/user/s/skkwan/hToAA/skims/plainHistogram"

if [ "$RUNNER" = GITLAB ]; then
    echo ">>> histograms.sh: Is running in Gitlab ..."
    EOS_DIR="root://eosuser.cern.ch/"${EOS_DIR}
fi


# Produce histograms from skimmed samples
while IFS=, read -r SAMPLE YEAR PROCESS SCALE
do
    INPUT=${INPUT_DIR}/${YEAR}/${SAMPLE}Skim.root

    # set up local area for results
    mkdir -p ${OUTPUT_DIR}/${YEAR}
    OUTPUT=${OUTPUT_DIR}/${YEAR}/histograms_${PROCESS}.root

    # Remove any file with the existing name
    rm ${OUTPUT}
    # Make the histograms
    python histograms.py $INPUT $PROCESS $OUTPUT

    # copy results to EOS
    mkdir -p ${EOS_DIR}/${YEAR}/
    cp ${OUTPUT} ${EOS_DIR}/${YEAR}/
done < ${HIST_LIST}

# 2018: Merge histograms in a single file
hadd -f ${OUTPUT_DIR}/2018/histograms.root ${OUTPUT_DIR}/2018/histograms_*.root
echo ">>> histograms.sh: Copying output histogram to ${EOS_DIR}/2018/"
cp ${OUTPUT_DIR}/2018/histograms.root ${EOS_DIR}/2018/

# TO-DO: Add 2017 and 2016
