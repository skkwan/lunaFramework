#!/bin/bash

# Usage: bash skim.sh [COMPILE or NO_COMPILE] [samples.csv] [GITLAB or LXPLUS] [paths to all .cc source files that are not gen.cxx]

COMPILE=$1
SAMPLES=$2
RUNNER=$3
TARGET_FILES=$4

# Assume custom dictionary was compiled in runGenSkim.sh, and exists
# Compile executable
if [ "$COMPILE" = COMPILE ]; then
    echo ">>> genSkim.sh: Compile skimming executable gen.cxx..."
    COMPILER=$(root-config --cxx)
    FLAGS=$(root-config --cflags --libs)
    time $COMPILER -g -O3 -Wall -Wextra -Wpedantic -o genSkim gen.cxx $FLAGS ${TARGET_FILES}
fi

# # Check that the input .list files exist first!
# echo ">>> genSkim.sh: Reading from ${SAMPLES}..."
# while IFS=, read -r SAMPLE INPUT_DIR YEAR CONFIG_DIR XSEC LUMI SCALE
# do
#     echo ">>> genSkim.sh: Skim sample ${SAMPLE}: with config directory ${CONFIG_DIR} and input list of files: ${INPUT_DIR}"
#     if [[ -f ${INPUT_DIR} ]]; then
# 	echo "   >>> ${INPUT_DIR} exists"
#     else
# 	echo "   >>> genSkim.sh: [ERROR]: Input list of files not found"
# 	exit 1
#     fi

# done < ${SAMPLES}

# # Get the current date and time, needed for output files
# DATETIME="$(date +"%b-%d-%Y-%Hh%Mm")"

# # EOS_DIR="/eos/user/s/skkwan/hToAA/genSkims"
# # EOSDIR=${EOS_DIR}/${DATETIME}/
# # mkdir -p ${EOSDIR}

# # echo "# README for ${DATETIME}" | cat - README.md > temp && mv temp ${EOSDIR}/README.md
# # echo  ">>> genSkim.sh: Wrote ${EOSDIR}/README.md"


# # Access the .csv list of samples to run over
# while IFS=, read -r SAMPLE INPUT_DIR YEAR CONFIG_DIR XSEC LUMI SCALE
# do
#     {
# 	OUTPUT=${SAMPLE}GenSkim.root
# 	LOG=${EOSDIR}${SAMPLE}GenSkim.log

# 	echo "--- Starting executable for ${SAMPLE}: [Time: $(TZ=America/New_York date)] --- "
# 	echo ">>> genSkim.sh: Writing to ${OUTPUT}, log in ${LOG}"
# 	./genSkim $SAMPLE "${INPUT_DIR}" $OUTPUT $CONFIG_DIR $XSEC $LUMI $SCALE | tee $LOG
#     # mv $OUTPUT ${EOSDIR}${OUTPUT}
#     # mv $LOG ${EOSDIR}${LOG}
#     } &

# done < ${SAMPLES}

# wait
# # echo ">>> Check ${EOSDIR} for outputs"
# # echo ">>> scp 'skkwan@lxplus.cern.ch:${EOSDIR}*' ."
