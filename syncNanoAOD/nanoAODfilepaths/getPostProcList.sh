# Usage: (e.g. on lxplus)
# First make sure your grid certificate is created (voms proxy init etc.)
# source getPostProcLists.sh

# Creates a .csv file which you can use as an argument to postprocess.sh/postprocess.cxx


##################################################
# Keep this updated
##################################################

LFNLIST="temp.csv"
BASEPATH="/eos/cms/store/group/phys_susy/AN-24-166/skkwan/condorSkim/2024-11-14-00h04m-year-2016-iteration0"
POSTPROC_CONFIG="config/2016preVFP/"
SCALE="1.0"

rm -r test_*

DESTINATION="postProcHists-temp-.csv"
rm -r ${DESTINATION}

##################################################
# Do Data and MC
##################################################
while IFS=, read -r YEAR DATASETNAME DASDATASET NENTRIES
do
  echo "getPostProcLists.sh: dataset name: ${DATASETNAME}, DAS dataset: ${DASDATASET}"

  if [[ ${DASDATASET} == *"/USER"* ]]; then
    COMMAND="dasgoclient -query='dataset=${DASDATASET} instance=prod/phys03 | grep dataset.nevents'"
  else
    COMMAND="dasgoclient -query='dataset=${DASDATASET} | grep dataset.nevents'"
  fi
  echo ${COMMAND}
  eval ${COMMAND} >> test_${DATASETNAME}

  # The event count may be in line 1, 2, or 3, so we have to get each one, strip the spaces, and get the non-zero one
  COUNTS1=$(sed -n '1p' < test_${DATASETNAME})   # get the first line
  COUNTS2=$(sed -n '2p' < test_${DATASETNAME})
  COUNTS3=$(sed -n '3p' < test_${DATASETNAME})
  echo "comparing ${COUNTS1}, ${COUNTS2}, ${COUNTS3}"
  COUNT1=${COUNTS1//$' '/}     # get rid of spaces
  COUNT2=${COUNTS2//$' '/}     # get rid of spaces
  COUNT3=${COUNTS3//$' '/}     # get rid of spaces
  if [[ ${COUNT1} != "" ]]; then
    COUNT=${COUNT1}
  fi
  if [[ ${COUNT2} != "" ]]; then
    COUNT=${COUNT2}
  fi
  if [[ ${COUNT3} != "" ]]; then
    COUNT=${COUNT3}
  fi
  echo "Count: ${COUNT}"

  # The line which will be written to postproc
  LINE="${BASEPATH},${DATASETNAME},${POSTPROC_CONFIG},xsec,${COUNT}.0"

  echo "${LINE}" | tee -a ${DESTINATION}

  wait
done < ${LFNLIST}

rm -r test_*
