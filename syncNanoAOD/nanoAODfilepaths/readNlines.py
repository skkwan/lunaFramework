# readNlines.py

# Example:
# python3 readNlines.py 2018/EmbeddingRun2018A-MuTau.list 2018/EmbeddingRun2018/ EmbeddingRun2018

import argparse
from itertools import islice
import os


def next_n_lines(file_opened, N):
    """
    Each time this function is called, the function returns the next N lines from
    file_opened as a list.
    """
    # print ([x.strip() for x in islice(file_opened, N)])
    return [x.strip() for x in islice(file_opened, N)]


def main(fullList, outputDir, sample, nFilesPerJob):
    """
    Writes _BATCH_0.list, _BATCH_1.list, _BATCH_2.list etc. to outputDir, 5 lines at a time.
    """
    print(fullList)
    if not os.path.exists(outputDir):
        os.mkdir(outputDir)
    print("Directory '%s' opened/created" % outputDir)

    batchNumber = -1
    with open(fullList, "r") as f:
        while True:
            nextLines = next_n_lines(f, nFilesPerJob)
            if nextLines:
                batchNumber += 1
                with open(
                    outputDir + sample + "_BATCH_" + str(batchNumber) + ".list", "a"
                ) as targetFile:
                    for l in nextLines:
                        if l != nextLines[-1]:
                            mystring = "{}\n".format(l)
                            targetFile.write(mystring)
                        else:
                            targetFile.write(l)

            else:
                break


if __name__ == "__main__":
    parser = argparse.ArgumentParser(
        "Write smaller .list files using a .list file that contains paths to the full dataset."
    )
    parser.add_argument(
        "--fullList",
        type=str,
        help="Full path to full .list of logical file names",
        required=True,
    )
    parser.add_argument(
        "--outputDir",
        type=str,
        help="Folder where smaller .list s will be written",
        required=True,
    )
    parser.add_argument("--sample", type=str, help="Name of sample", required=True)
    parser.add_argument(
        "--nFilesPerJob", type=int, help="Number of files per job", required=True
    )
    args = parser.parse_args()
    main(args.fullList, args.outputDir, args.sample, args.nFilesPerJob)
