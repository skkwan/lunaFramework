# getFilePaths.sh
#   Queries DAS using the command-line interface (dasgoclient) to get a list of .ROOT files in each dataset. Uses helper functions in readNlines.py
#   to split up the list into smaller "batch" files.
#   Specify the datasets to use and the size of each "batch" in the .txt file pointed to in LFNLIST:
#   e.g. 2016,preVFP_MuonEG-Run2016F_HIPM,/MuonEG/Run2016F-HIPM_UL2016_MiniAODv2_NanoAODv9-v2/NANOAOD,1
#   --> write the output in the directory 2016/, using a sample name preVFP_MuonEG-Run2016F_HIPM, with the DAS dataset shown, with each "batch" being 1 NanoAOD file
#
# Usage:
#   1. Get a grid certificate: voms-proxy-init --voms cms --valid 194:00
#   2. Edit the LFNLIST variable below to point to the desired list of datasets (typically have one .list for each year)
#   3. Run with bash getFilePaths.h
#   4. ls or cat the resulting files (e.g. in 2018/VBFHToTauTau, or 2016/preVFP_VBFHToTauTau for example) to see that it actually did something

LFNLIST="temp.csv"

while IFS=, read -r YEAR DATASETNAME DASDATASET NFILES_PER_JOB
do
  echo ">>>>>>>> Year: ${YEAR}, dataset name: ${DATASETNAME}, DAS dataset: ${DASDATASET}"

  ############################################################
  # Ceate .list files
  ############################################################

  # Make a directory to hold the batches
  BATCHDIR="${YEAR}/${DATASETNAME}/"
  # Remove the temp file
  if ! [[ -z ${BATCHDIR} ]]; then

    rm -r ${BATCHDIR}
    mkdir -p ${BATCHDIR}

    FULLLIST=${YEAR}/${DATASETNAME}.list
    TEMP=${YEAR}/${DATASETNAME}-temp.list

    if [[ ${DASDATASET} == *"/USER" ]]; then
      dasgoclient --query="file dataset=${DASDATASET} instance=prod/phys03 status=VALID" > ${TEMP}
    else
      dasgoclient --query="file dataset=${DASDATASET}" > ${TEMP}
    fi

    # Pre-pend the redirector in front of each line
    if [[ ${DASDATASET} == *"MuonEG"* ]] || [[ ${DASDATASET} == *"SingleMuon"* ]] || [[ ${DASDATASET} == *"EGamma"* ]]; then
      awk '{print "root://cms-xrd-global.cern.ch/" $0}' ${TEMP} > ${FULLLIST}
    else
      awk '{print "root://cmsxrootd.hep.wisc.edu/" $0}' ${TEMP} > ${FULLLIST}
    fi

    # Check it looks ok
    if [[ -z "${FULLLIST}" ]]; then
      echo "[WARNING:] No files found!"
    fi

    # Remove the temp file
    if [[ -z ${TEMP} ]]; then
      rm ${TEMP}
    fi

    CMD="python3 readNlines.py --fullList ${FULLLIST} --outputDir ${BATCHDIR} --sample ${DATASETNAME} --nFilesPerJob ${NFILES_PER_JOB}"
    echo "...Executing: ${CMD}"
    eval ${CMD}

    #############################################################
    ## Also create -mini.list files for testing the skimming step on
    #############################################################

    DESTINATION=${YEAR}/${DATASETNAME}-mini.list
    TEMP=${YEAR}/${DATASETNAME}-temp-mini.list
    TEMP2=${YEAR}/${DATASETNAME}-temp2-mini.list

    if [[ ${DASDATASET} == *"/USER" ]]; then
      dasgoclient --query="file dataset=${DASDATASET} instance=prod/phys03 status=*" > ${TEMP}
    else
      dasgoclient --query="file dataset=${DASDATASET}" > ${TEMP}
    fi

    # Pre-pend the redirector in front of each line
    awk '{print "root://cmsxrootd.hep.wisc.edu/" $0}' ${TEMP} > ${TEMP2}

    # Write only the first line to -mini.list
    (head -1 ${TEMP2} ) > ${DESTINATION}

    # Remove the temp files
    rm ${TEMP}
    rm ${TEMP2}
  fi
done < "${LFNLIST}"
