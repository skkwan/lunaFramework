"""
getFilePaths-Embedded.py: for getting files that can be ls'd
"""

import os

# Say we want to write all the *.root file paths in
# a directory 'rootdir'/'year'/'originalName'/*.root into a list
# 'year'/'targetName'.list

year = "2017"
rootdir = "/eos/user/s/skkwan/hToAA/embeddedNanoAOD"

# Desired *.list name first, then the original sample name used in the NanoAOD *.root file
dataset = [
    ("Embedded-Run2017B-MuTau", "EmbeddingRun2017B-MuTau"),
    ("Embedded-Run2017C-MuTau", "EmbeddingRun2017C-MuTau"),
    ("Embedded-Run2017D-MuTau", "EmbeddingRun2017D-MuTau"),
    ("Embedded-Run2017E-MuTau", "EmbeddingRun2017E-MuTau"),
    ("Embedded-Run2017F-MuTau", "EmbeddingRun2017F-MuTau"),
]

for targetName, originalName in dataset:
    tempName = "temp_%s" % targetName
    tempCommand = "ls -d %s/%s/%s/*.root > %s" % (rootdir, year, originalName, tempName)

    # Now read that into the .list
    command = "cat %s > %s/%s.list" % (tempName, year, targetName)
    shortcommand = "head -1 %s > %s/%s-mini.list" % (tempName, year, targetName)
    print(tempCommand)
    print(command)
    print(shortcommand)
    os.system(tempCommand)
    os.system(command)
    os.system(shortcommand)
    os.system("rm %s" % tempName)

# ls -d $PWD/*.root > /afs/cern.ch/work/s/skkwan/public/hToAA/syncNanoAOD/nanoAODfilepaths/2017/Embedded-Run2017B-MuTau
