# Implementation of the plotting step of the analysis
#
# The plotting combines the histograms to plots which allow us to study the
# inital dataset based on observables motivated through physics.


import argparse
import ROOT
from helpers.plotLabels import *

ROOT.gROOT.SetBatch(True)


# Specify the color for each process
colors = {
    #        "ggH": ROOT.TColor.GetColor("#BF2229"),
    "qqH": ROOT.TColor.GetColor("#00A88F"),
    #        "TT": ROOT.TColor.GetColor(155, 152, 204),
    #        "W": ROOT.TColor.GetColor(222, 90, 106),
    #        "QCD":  ROOT.TColor.GetColor(250, 202, 255),
    #        "ZLL": ROOT.TColor.GetColor(100, 192, 232),
    "DYJetsToLL": ROOT.TColor.GetColor(100, 192, 232),
    "ZTT": ROOT.TColor.GetColor(248, 206, 104),
    "Embedded-Run2018A": ROOT.TColor.GetColor(248, 206, 104),
    "Embedded-Run2018B": ROOT.TColor.GetColor(248, 206, 104),
    "Embedded-Run2018C": ROOT.TColor.GetColor(248, 206, 104),
    "Embedded-Run2018D": ROOT.TColor.GetColor(248, 206, 104),
    "VBF-HtoAA-AtoBB-AtoTauTau-M40": ROOT.TColor.GetColor(255, 40, 0),
    "default": ROOT.TColor.GetColor("#BF2229"),
}


# Retrieve a histogram from the input file based on the process and the variable
# name
def getHistogram(tfile, name, variable, tag=""):
    name = "{}_{}{}".format(name, variable, tag)
    h = tfile.Get(name)
    if not h:
        raise Exception("Failed to load histogram {}.".format(name))
    return h


# Main function of the plotting step
#
# The major part of the code below is dedicated to define a nice-looking layout.
# The interesting part is the combination of the histograms to the QCD estimation.
# There, we take the data histogram from the control region and subtract all known
# processes defined in simulation and define the remaining part as QCD. Then,
# this shape is extrapolated into the signal region with a scale factor.
def main(path, output, variable, scale, process):
    tfile = ROOT.TFile(path, "READ")

    # Styles
    ROOT.gStyle.SetOptStat(0)

    ROOT.gStyle.SetCanvasBorderMode(0)
    ROOT.gStyle.SetCanvasColor(ROOT.kWhite)
    ROOT.gStyle.SetCanvasDefH(600)
    ROOT.gStyle.SetCanvasDefW(600)
    ROOT.gStyle.SetCanvasDefX(0)
    ROOT.gStyle.SetCanvasDefY(0)

    ROOT.gStyle.SetPadTopMargin(0.08)
    ROOT.gStyle.SetPadBottomMargin(0.13)
    ROOT.gStyle.SetPadLeftMargin(0.16)
    ROOT.gStyle.SetPadRightMargin(0.05)

    ROOT.gStyle.SetHistLineColor(1)
    ROOT.gStyle.SetHistLineStyle(0)
    ROOT.gStyle.SetHistLineWidth(1)
    ROOT.gStyle.SetEndErrorSize(2)
    ROOT.gStyle.SetMarkerStyle(20)

    ROOT.gStyle.SetOptTitle(0)
    ROOT.gStyle.SetTitleFont(42)
    ROOT.gStyle.SetTitleColor(1)
    ROOT.gStyle.SetTitleTextColor(1)
    ROOT.gStyle.SetTitleFillColor(10)
    ROOT.gStyle.SetTitleFontSize(0.05)

    ROOT.gStyle.SetTitleColor(1, "XYZ")
    ROOT.gStyle.SetTitleFont(42, "XYZ")
    ROOT.gStyle.SetTitleSize(0.05, "XYZ")
    ROOT.gStyle.SetTitleXOffset(1.00)
    ROOT.gStyle.SetTitleYOffset(1.60)

    ROOT.gStyle.SetLabelColor(1, "XYZ")
    ROOT.gStyle.SetLabelFont(42, "XYZ")
    ROOT.gStyle.SetLabelOffset(0.007, "XYZ")
    ROOT.gStyle.SetLabelSize(0.04, "XYZ")

    ROOT.gStyle.SetAxisColor(1, "XYZ")
    ROOT.gStyle.SetStripDecimals(True)
    ROOT.gStyle.SetTickLength(0.03, "XYZ")
    ROOT.gStyle.SetNdivisions(510, "XYZ")
    ROOT.gStyle.SetPadTickX(1)
    ROOT.gStyle.SetPadTickY(1)

    ROOT.gStyle.SetPaperSize(20.0, 20.0)
    ROOT.gStyle.SetHatchesLineWidth(5)
    ROOT.gStyle.SetHatchesSpacing(0.05)

    ROOT.TGaxis.SetExponentOffset(-0.08, 0.01, "Y")

    # Simulation
    hist = getHistogram(tfile, process, variable)

    # Draw histograms
    # If data:
    if (("run" in process.lower()) or ("data" in process.lower())) and (
        "embed" not in process.lower()
    ):
        hist.SetMarkerStyle(20)
        hist.SetLineColor(ROOT.kBlack)
    elif process in colors.keys():
        print("Setting", process, "line color to", colors[process])
        hist.SetLineColor(colors[process])
        hist.SetLineWidth(3)
    else:
        print("Setting", process, "line color to default")
        hist.SetLineColor(colors["default"])
        hist.SetLineWidth(3)

    # scale_ggH = 10.0
    # ggH.Scale(scale_ggH)
    # scale_qqH = 1.0
    # qqH.Scale(scale_qqH)

    # for x in [qqH]:  # commented out ggH
    #    x.SetLineWidth(3)

    # for x, l in [(QCD, "QCD"), (TT, "TT"), (ZLL, "ZLL"), (ZTT, "ZTT"), (W, "W")]:
    #     x.SetLineWidth(0)
    #     x.SetFillColor(colors[l])

    stack = ROOT.THStack("", "")
    # for x in [QCD, TT, W, ZLL, ZTT]:
    #     stack.Add(x)
    stack.Add(hist)

    c = ROOT.TCanvas("", "", 600, 600)
    stack.Draw("hist")
    name = hist.GetTitle()  # originally data.GetTitle()
    if name in labels:
        title = labels[name]
    else:
        title = name

    stack.GetXaxis().SetTitle(title)
    print(title)
    stack.GetYaxis().SetTitle("N_{Events}")
    stack.SetMaximum(
        max(stack.GetMaximum(), hist.GetMaximum()) * 1.4
    )  # originally data.GetMaximum() in second arg
    stack.SetMinimum(1.0)

    if (("run" in process.lower()) or ("data" in process.lower())) and (
        "embed" not in process.lower()
    ):
        hist.Draw("E1P SAME")
    else:
        hist.Draw("HIST SAME")

    # Add legend
    legend = ROOT.TLegend(0.4, 0.73, 0.90, 0.88)
    legend.SetNColumns(2)
    # legend.AddEntry(ZTT, "Z#rightarrow#tau#tau", "f")
    # legend.AddEntry(ZLL, "Z#rightarrowll", "f")
    # legend.AddEntry(W, "W+jets", "f")
    # legend.AddEntry(TT, "t#bar{t}", "f")
    # legend.AddEntry(QCD, "QCD multijet", "f")
    # legend.AddEntry(ggH, "gg#rightarrowH (x{:.0f})".format(scale_ggH), "l")
    # legend.AddEntry(qqH, "qq#rightarrowH (x{:.0f})".format(scale_qqH), "l")
    # legend.AddEntry(data, "Data", "lep")
    legend.AddEntry(hist, process, "l")
    legend.SetBorderSize(0)
    legend.Draw()

    # Add title
    latex = ROOT.TLatex()
    latex.SetNDC()
    latex.SetTextSize(0.04)
    latex.SetTextFont(42)
    lumi = 11.467
    # latex.DrawLatex(0.6, 0.935, "{:.1f} fb^{{-1}} (2012, 8 TeV)".format(lumi * scale))
    # latex.DrawLatex(0.16, 0.935, "#bf{CMS Open Data}")

    # Save
    c.SaveAs("{}/{}.pdf".format(output, variable))
    c.SaveAs("{}/{}.png".format(output, variable))


# Loop over all variable names and make a plot for each
if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument(
        "path", type=str, help="Full path to ROOT file with all histograms"
    )
    parser.add_argument("output", type=str, help="Output directory for plots")
    parser.add_argument(
        "scale", type=float, help="Scaling of the integrated luminosity"
    )
    parser.add_argument("process", type=str, help="Name of the physics process")
    args = parser.parse_args()
    for variable in labels.keys():
        main(args.path, args.output, variable, args.scale, args.process)
