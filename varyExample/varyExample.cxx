/*
 * Example of vary
 */


#include <ROOT/RDataFrame.hxx>
#include <ROOT/RDF/RResultMap.hxx>
#include <ROOT/RDFHelpers.hxx>   // need this for VariationsFor()
#include <ROOT/RVec.hxx>

#include <Math/Vector4D.h>


ROOT::Math::PtEtaPhiMVector add_p4(float pt, float eta, float phi, float mass) {
  return ROOT::Math::PtEtaPhiMVector(pt, eta, phi, mass);
}


int main(void) {

    TStopwatch time;
    time.Start();

    TChain *ch = new TChain("Events");

    std::string line = "root://cmsxrootd.fnal.gov//store/mc/RunIIAutumn18NanoAODv7/VBFHToTauTau_M125_13TeV_powheg_pythia8/NANOAODSIM/Nano02Apr2020_102X_upgrade2018_realistic_v21_ext1-v1/100000/A3EB6B6E-8907-E345-B897-D0709785E2A1.root";
    const char *filename = line.c_str();
    int result = ch->Add(filename);
    if (result == 0) {
      std::cout << "[ERROR:] Failed to find file Events TTree" << std::endl;
    }
    std::cout << "Added " << filename
              << " Events TTRee with (1 for success): "
              << result << std::endl;


    ROOT::RDataFrame df(*ch);

    // As a dummy example, get one of the Jet pts and pretend we are doing variations of it
    auto dfNominal = df.Define("jpt_1",  "Jet_pt[0]")
      .Define("jeta_1", "Jet_eta[0]")
      .Define("jphi_1", "Jet_phi[0]")
      .Define("jm_1",   "Jet_mass[0]")
      .Define("jpt_2",  "Jet_pt[1]")
      .Define("jeta_2", "Jet_eta[1]")
      .Define("jphi_2", "Jet_phi[1]")
      .Define("jm_2",   "Jet_mass[1]")
      .Define("pt_1",  "Muon_pt[0]")
      .Define("eta_1", "Muon_eta[0]")
      .Define("phi_1", "Muon_phi[0]")
      .Define("m_1",   "Muon_mass[0]")
      .Define("pt_2",  "Tau_pt[0]")
      .Define("eta_2", "Tau_eta[0]")
      .Define("phi_2", "Tau_phi[0]")
      .Define("m_2",   "Tau_mass[0]")
      .Vary("jpt_1", [] (float jpt) { return ROOT::RVecF{jpt* (float) 0.9, jpt*(float)1.1}; }, {"jpt_1"}, {"down", "up"}, "mySysName")
      .Vary("pt_2",  [](float pt)   { return ROOT::RVecF{pt* (float) 0.8, pt*(float)1.2}; }, {"pt_2"}, {"down", "up"}, "tauES")
      .Filter("jpt_1 > 200.0", "cut on large pT")
      .Define("jp4_1", add_p4, {"jpt_1", "jeta_1", "jphi_1", "jm_1"})
      .Define("jp4_2", add_p4, {"jpt_2", "jeta_2", "jphi_2", "jm_2"})
      .Define("p4_1", add_p4, {"pt_1", "eta_1", "phi_1", "m_1"})
      .Define("p4_2", add_p4, {"pt_2", "eta_2", "phi_2", "m_2"})
      .Define("m_jj", "(jp4_1 + jp4_2).M()")
      .Define("m_vis", "(p4_1+p4_2).M()");

    auto cutReport = dfNominal.Report();

    // Histo1D is an action: it returns a smart pointer to a TH1D histogram. It can be used like a pointer to the underlying object.
    auto h_jpt_1 = dfNominal.Histo1D("jpt_1");
    auto h_m_jj  = dfNominal.Histo1D("m_jj");
    auto h_m_vis = dfNominal.Histo1D("m_vis");

    // VariationsFor produces a RResultMap with full variation names as strings (e.g. "pt:down") and the corresponding varied results as values.
    // There is a "nominal" key as well
    auto map_varied_jpt_1 = ROOT::RDF::Experimental::VariationsFor(h_jpt_1);
    auto map_varied_m_jj  = ROOT::RDF::Experimental::VariationsFor(h_m_jj);
    auto map_varied_m_vis  = ROOT::RDF::Experimental::VariationsFor(h_m_vis);

    // Output file and folder (if different)
    TFile *f = new TFile("out.root", "RECREATE");
    f->cd();
    map_varied_jpt_1["nominal"].Write("jpt_1_nominal");
    map_varied_jpt_1["mySysName:down"].Write("jpt_1_mySysNameDown");
    map_varied_jpt_1["mySysName:up"].Write("jpt_1_mySysNameUp");

    map_varied_m_jj["nominal"].Write("m_jj_nominal");
    map_varied_m_jj["mySysName:down"].Write("m_jj_mySysNameDown");
    map_varied_m_jj["mySysName:up"].Write("m_jj_mySysNameUp");

    map_varied_m_vis["nominal"].Write("m_vis_nominal");
    map_varied_m_vis["tauES:down"].Write("m_vis_tauESDown");
    map_varied_m_vis["tauES:up"].Write("m_vis_tauESUp");

    f->Close();

    cutReport->Print();

    return 0;
}
