"""
haddFiles.py

Description:
    - Sum the hEvents and hRuns histograms from completed Condor n-tuples, to collect metadata about the total number of
events processed. Uses the -T option to only hadd the histograms and not the n-tuples, to save energy. Storing this metadata
in an info_*.root (e.g. info_VBFHToTauTau.root) allows us to continue separating the datasets into smaller jobs on the batch system.
    - The info_*.root file is written to the top-level directory and ALSO copied to a info/ sub-folder, so that the entire info/ folder can
be shipped with the next Condor postprocessing step.

Usage:
    python haddFiles.py --year [year] --rootdir [directory]
        where [directory] is some timestamped directory, e.g. /eos/cms/store/group/phys_susy/AN-24-166/skkwan/condorSkim/2024-11-14-00h04m-year-2016-iteration0

    Add --recreate (FALSE by default) to force re-creation of existing info_*.root files.
    Add --execute to execute all the bash commands.
"""

import argparse
import os
import sys


parser = argparse.ArgumentParser(
    description="Hadd histograms (not n-tuples) files from a directory with sub-directories"
)

parser.add_argument(
    "--rootdir", dest="rootdir", help="Path to directory", required=True
)
parser.add_argument("--recreate", dest="recreate", action="store_true")
parser.add_argument("--execute", dest="execute", action="store_true")
parser.set_defaults(recreate=False)
parser.set_defaults(execute=False)

args = parser.parse_args()

if not args.rootdir.endswith("/"):
    args.rootdir += "/"

print("Directory to use: " + args.rootdir)

# loop through folders in the directory, assuming that the folder names are the sample names (e.g. VBFHToTauTau/)
for sampleName in os.listdir(args.rootdir):
    # skip the "info/" directory, if it exists
    if "info" in sampleName:
        continue
    print(sampleName)

    # the output metadata file will be called info_VBFHToTauTAu.root
    outputFile = args.rootdir + "info_" + sampleName + ".root"
    print(outputFile)

    # the n-tuples to run hadd on, are called VBFHToTauTau/*.root
    targetFile = args.rootdir + sampleName + "/*.root"

    # this is where the output metadata info_VBFHToTauTau.root goes
    targetDir = args.rootdir + sampleName + "/"

    # if the info_*.root file exists and the recreate flag is set to False, do not recreate the file
    if os.path.exists(outputFile) and not args.recreate:
        print(outputFile + " exists, do not recreate")
        continue

    # -T flag only hadds the histograms (not the n-tuples)
    command = "hadd -T -j -k " + outputFile + " " + targetFile
    if args.recreate:
        # use the force -f flag if recreating
        command = "hadd -f -T -j -k " + outputFile + " " + targetFile

    # run the command
    print("Preparing command {}".format(command))
    if args.execute:
        os.system(command)

# Copy all the info_*.root files to a folder info/ so the Condor postprocessing job can just ship out the entire directory
# (wildcards do not work in Condor transfer_input_files)
if args.execute:
    os.system(f"mkdir -p {args.rootdir}/info/")
    os.system(f"cp {args.rootdir}/info_*.root {args.rootdir}info/")
    print(
        f"Copied all info_*.root files to {args.rootdir}info in preparation for Condor postprocessing step"
    )

if not args.execute:
    print("--execute flag was false: did not execute any commands")
