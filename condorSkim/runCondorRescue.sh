#!/bin/bash

# Usage:
# cmsenv
# source ${CMSSW_BASE}/src/lunaFramework/venv-LUNA/bin/activate
# voms-proxy-init --voms cms --valid 194:00
# cd condorSkim/
# python lookForErrorFiles.py [with appropriate arguments, including --execute]
# bash runCondorRescue.sh

# Samples .csv here only dictates which datasets we are submitting rescue jobs for. The jobs to resubmit are dictated by what is in the
# rescue/[year]/* folder, which you generate with lookForErrorFiles.py.
SAMPLES="${CMSSW_BASE}/src/lunaFramework/syncNanoAOD/skim_FULL_stack_2016.csv"
YEAR_OR_DESCRIPTION="patch-2016"
SUBMITCONDOR=true


WORKING_DIR=${CMSSW_BASE}"/src/lunaFramework/syncNanoAOD"
EXECUTABLE_NAME="skim"
EXECUTABLE_FULL_PATH="${WORKING_DIR}/${EXECUTABLE_NAME}"
BASH_SCRIPT=${CMSSW_BASE}"/src/lunaFramework/condorSkim/job.sh"

isLocalJob=0

#--------------------------------------------------------
# Check if we used bash
#--------------------------------------------------------
if [[ "$0" != "$BASH_SOURCE" ]]; then
    echo ">>> ${BASH_SOURCE[0]}: Error: Script must be run with bash"
    return
fi
#--------------------------------------------------------
# Check if environments are set
#--------------------------------------------------------
if [[ -z ${CMSSW_BASE} ]]; then
    echo ">>> ${BASH_SOURCE[0]}: CMSSW environment is not set! Make sure to do cmsenv"
    exit 1
fi
if [[ -z ${VIRTUAL_ENV} ]]; then
    echo ">>> ${BASH_SOURCE[0]}: Virtual environment is not set! Make sure to source venv-LUNA/bin/activate"
    exit 1
fi


#--------------------------------------------------------
# Check that the input .list files exist first!
#--------------------------------------------------------
echo ">>> ${BASH_SOURCE[0]}: Reading from ${SAMPLES}..."
while IFS=, read -r SAMPLE INPUT_DIR YEAR CONFIG_DIR
do
	INPUT_DIR="${CMSSW_BASE}/src/lunaFramework/syncNanoAOD/"${INPUT_DIR}
    echo ">>> ${BASH_SOURCE[0]}: Starting sample ${SAMPLE}: with config directory ${CONFIG_DIR} and input list of files: ${INPUT_DIR}"
    if [[ -d ${INPUT_DIR} ]]; then
	echo "   >>> ${INPUT_DIR} exists"
    else
	echo "   >>> ${BASH_SOURCE[0]}: [ERROR]: Input folder of files ${INPUT_DIR} not found"
	exit 1
    fi
done < ${SAMPLES}

#--------------------------------------------------------
# Get the voms-proxy-info certificate
#--------------------------------------------------------
# Use regex to extract the first letter of the username. ^ is the beginning of the string. \w means any alphanumeric character from the Latin alphabet. ( ) captures the variable
re="^(\w)"
# Bash's =~ operator matches the left hand side to the regex on the right hand side
[[ ${USER} =~ ${re} ]]
# The results of the regex can be extracted from the bash variable BASH_REMATCH. Index [0] returns all of the matches.
USER_FIRST_LETTER=${BASH_REMATCH[0]}

export MYPROXYPATH="$(voms-proxy-info -path)"
export TARGETPROXYPATH="/afs/cern.ch/user/${USER_FIRST_LETTER}/${USER}/private/x509up_file"
echo ">>> ${BASH_SOURCE[0]}: Found proxy at: ${MYPROXYPATH}, copying to ${TARGETPROXYPATH}"

cp ${MYPROXYPATH} ${TARGETPROXYPATH}

#--------------------------------------------------------
# Get the current date and time, needed for output files
#--------------------------------------------------------

DATETIME="$(date +"%Y-%m-%d-%Hh%Mm")"
echo ${DATETIME}

WORKING_DIR=${CMSSW_BASE}"/src/lunaFramework/syncNanoAOD"
EXECUTABLE=${CMSSW_BASE}"/src/lunaFramework/syncNanoAOD/skim"
BASH_SCRIPT=${CMSSW_BASE}"/src/lunaFramework/condorSkim/job.sh"

RESCUE_DIR=${CMSSW_BASE}"/src/lunaFramework/condorSkim/rescue"

# Access the .csv list of samples to run over
# INPUT_DIR is now different: ignore what the .csv says
while IFS=, read -r SAMPLE INPUT_DIR YEAR CONFIG_REL_DIR
do
    {
 	EOS_PREFIX="root://eosuser.cern.ch/"
	EOS_TIMESTAMP_DIR="/eos/cms/store/group/phys_susy/AN-24-166/${USER}/condorSkim/${DATETIME}"
	EOS_BASE_DIR="${EOS_TIMESTAMP_DIR}/${SAMPLE}"
    EOS_DIR=${EOS_PREFIX}${EOS_BASE_DIR}
	JOBLOG_DIR="logs/${DATETIME}/${SAMPLE}"

	# In rescue mode, queue = 1
    NINSTANCES=1

	# Do not clutter the EOS dir, only mkdir if we are submitting
	if [[ "$SUBMITCONDOR" = true ]]; then
		mkdir -p ${EOS_TIMESTAMP_DIR}
	fi

	# Instead of INPUT_DIR, go into the directory where the rescue jobs are
	RESCUELIST="${RESCUE_DIR}/${YEAR}/${SAMPLE}_rescue.csv"
	if [ -f "${RESCUELIST}" ]; then
		echo ">>> ${BASH_SOURCE[0]}: [Time: $(date)] ${SAMPLE}, job log in ${JOBLOG_DIR}, output (if executing) in ${EOS_BASE_DIR}"

	    while IFS=, read -r BATCHNUM BATCHINPUT
	    do
		{
			# Do not clutter the EOS dir, only mkdir for the samples that we are submitting rescue jobs for
			if [[ "$SUBMITCONDOR" = true ]]; then
				mkdir -p ${EOS_BASE_DIR}
			fi

			mkdir -p ${JOBLOG_DIR}

		    echo "File to run on: ${BATCHINPUT}"

		    subfile="${JOBLOG_DIR}/${SAMPLE}_rescue_BATCH_${BATCHNUM}.sub"
			echo ">>> New submit file: ${subfile}"

		    OUTPUT_ROOT_FILE="${SAMPLE}_${BATCHNUM}.root"
		    INPUT_LIST="${WORKING_DIR}/${INPUT_DIR}/${SAMPLE}_BATCH_${BATCHNUM}.list"
		    CONFIG_DIR="${WORKING_DIR}/${CONFIG_REL_DIR}"

			# Increase the request to microcentury by default
			JOB_FLAVOUR='"microcentury"'

		    # Edit parameters in the Condor .sub file
		    cp jobTemplate.sub ${subfile}

			sed -i "s|(target_proxy_path)|${TARGETPROXYPATH}|g" ${subfile}
			sed -i "s|(executable_name)|${EXECUTABLE_NAME}|g" ${subfile}
			sed -i "s|(executable_full_path)|${EXECUTABLE_FULL_PATH}|g" ${subfile}
			sed -i "s|(bash_script)|${BASH_SCRIPT}|g" ${subfile}
			sed -i "s|(sample)|${SAMPLE}|g" ${subfile}
			sed -i "s|(inputdir)|${INPUT_LIST}|g" ${subfile}
			sed -i "s|(output_root_file)|${OUTPUT_ROOT_FILE}|g" ${subfile}
			sed -i "s|(configdir)|${CONFIG_DIR}|g" ${subfile}
			sed -i "s|(is_local_job)|${isLocalJob}|g" ${subfile}
			sed -i "s|(job_flavour)|${JOB_FLAVOUR}|g" ${subfile}
			sed -i "s|(eos_dir)|${EOS_DIR}|g" ${subfile}
			sed -i "s|(timestamp)|${DATETIME}|g" ${subfile}

			# For rescue jobs, we specify the batch number to recover
		    sed -i "s|\$(Process)|${BATCHNUM}|g" ${subfile}

			# Earlier we said NINSTANCES = 1 for rescue jobs
		    sed -i "s|(nInstances)|${NINSTANCES}|g" ${subfile}

			if [[ "$SUBMITCONDOR" == true ]]; then
		    	condor_submit -batch-name rescue_skim_${DATETIME}_${SAMPLE} -file ${subfile}
			else
				echo "SUBMITCONDOR false, nothing submitted"
			fi
		}
            done < ${RESCUELIST}
	fi
    }

done < ${SAMPLES}
