#!/bin/bash

# Usage:
# cmsenv
# source ${CMSSW_BASE}/src/lunaFramework/venv-LUNA/bin/activate
# voms-proxy-init --voms cms --valid 194:00
# cd condorSkim/
# bash runCondorSubmit.sh

SAMPLES=${CMSSW_BASE}/src/lunaFramework/syncNanoAOD/skim_syncOnly.csv
YEAR_OR_DESCRIPTION="test"
ATTEMPT_SUBMIT_ENTIRE_SAMPLE=true
SUBMITCONDOR=true

WORKING_DIR=${CMSSW_BASE}"/src/lunaFramework/syncNanoAOD"
EXECUTABLE_NAME="skim"
EXECUTABLE_FULL_PATH="${WORKING_DIR}/${EXECUTABLE_NAME}"
BASH_SCRIPT=${CMSSW_BASE}"/src/lunaFramework/condorSkim/job.sh"

isLocalJob=0
#--------------------------------------------------------
# Check if we used bash
#--------------------------------------------------------
if [[ "$0" != "$BASH_SOURCE" ]]; then
    echo ">>> ${BASH_SOURCE[0]}: Error: Script must be run with bash"
    return
fi
#--------------------------------------------------------
# Check if environments are set
#--------------------------------------------------------
if [[ -z ${CMSSW_BASE} ]]; then
    echo ">>> ${BASH_SOURCE[0]}: CMSSW environment is not set! Make sure to do cmsenv"
    exit 1
fi
if [[ -z ${VIRTUAL_ENV} ]]; then
    echo ">>> ${BASH_SOURCE[0]}: Virtual environment is not set! Make sure to source venv-LUNA/bin/activate"
    exit 1
fi


#--------------------------------------------------------
# Check that the input .list files exist first!
#--------------------------------------------------------
echo ">>> ${BASH_SOURCE[0]}: Reading from ${SAMPLES}..."
while IFS=, read -r SAMPLE INPUT_DIR YEAR CONFIG_DIR
do
	INPUT_DIR="${CMSSW_BASE}/src/lunaFramework/syncNanoAOD/"${INPUT_DIR}
    echo ">>> ${BASH_SOURCE[0]}: Starting sample ${SAMPLE}: with config directory ${CONFIG_DIR} and input list of files: ${INPUT_DIR}"
    if [[ -d ${INPUT_DIR} ]]; then
	echo "   >>> ${INPUT_DIR} exists"
    else
	echo "   >>> ${BASH_SOURCE[0]}: [ERROR]: Input folder of files ${INPUT_DIR} not found"
	exit 1
    fi
done < ${SAMPLES}

#--------------------------------------------------------
# Get the voms-proxy-info certificate
#--------------------------------------------------------
# Use regex to extract the first letter of the username. ^ is the beginning of the string. \w means any alphanumeric character from the Latin alphabet. ( ) captures the variable
re="^(\w)"
# Bash's =~ operator matches the left hand side to the regex on the right hand side
[[ ${USER} =~ ${re} ]]
# The results of the regex can be extracted from the bash variable BASH_REMATCH. Index [0] returns all of the matches.
USER_FIRST_LETTER=${BASH_REMATCH[0]}

export MYPROXYPATH="$(voms-proxy-info -path)"
export TARGETPROXYPATH="/afs/cern.ch/user/${USER_FIRST_LETTER}/${USER}/private/x509up_file"
echo ">>> ${BASH_SOURCE[0]}: Found proxy at: ${MYPROXYPATH}, copying to ${TARGETPROXYPATH}"

cp ${MYPROXYPATH} ${TARGETPROXYPATH}

#--------------------------------------------------------
# Get the current date and time, needed for output files
#--------------------------------------------------------

DATETIME="$(date +"%Y-%m-%d-%Hh%Mm")"
echo ${DATETIME}

# Access the .csv list of samples to run over
while IFS=, read -r SAMPLE INPUT_DIR YEAR CONFIG_REL_DIR
do
    {

    EOS_PREFIX="root://eosuser.cern.ch/"
	EOS_BASE_DIR="/eos/cms/store/group/phys_susy/AN-24-166/${USER}/condorSkim/${DATETIME}/${SAMPLE}"
    EOS_DIR=${EOS_PREFIX}${EOS_BASE_DIR}
	JOBLOG_DIR="logs/${DATETIME}/${SAMPLE}"

	NINSTANCES=`find ${WORKING_DIR}/${INPUT_DIR}/ -name "*BATCH*" | xargs ls -lrt | wc -l`

	if [[ "$SUBMITCONDOR" = true ]]; then
		# Do not clutter the EOS dir, only mkdir if we are submitting
		mkdir -p ${EOS_BASE_DIR}
		mkdir -p ${EOS_BASE_DIR}/logs/${TIMESTAMP}/${SAMPLE}
	fi

	mkdir -p ${JOBLOG_DIR}
	echo ">>> ${BASH_SOURCE[0]}: [Time: $(date)] ${SAMPLE}, job log in ${JOBLOG_DIR}, output (if executing) in ${EOS_BASE_DIR}"

	JOB_FLAVOUR='"microcentury"'
	if  [[ ${SAMPLE} == *"TTTo"* ]] || [[ ${SAMPLE} == *"DY"* ]] || [[ ${SAMPLE} == *"ST"* ]] || [[ ${SAMPLE} == *"VVTo"* ]] || [[ ${SAMPLE} == *"ZZTo"* ]] || [[ ${SAMPLE} == *"WJets"* ]] || [[ ${SAMPLE} == *"SingleMuon"* ]];  then
		JOB_FLAVOUR='"microcentury"'
	fi


	# Calculate how many 'queue' commands to submit
	subfile="${JOBLOG_DIR}/${SAMPLE}_job.sub"
	OUTPUT_ROOT_FILE="${SAMPLE}_\$(Process).root"
	INPUT_LIST="${WORKING_DIR}/${INPUT_DIR}/${SAMPLE}_BATCH_\$(Process).list"
	CONFIG_DIR="${WORKING_DIR}/${CONFIG_REL_DIR}"

	# Edit parameters in the Condor .sub file
	cp jobTemplate.sub ${subfile}

	sed -i "s|(target_proxy_path)|${TARGETPROXYPATH}|g" ${subfile}
	sed -i "s|(executable_name)|${EXECUTABLE_NAME}|g" ${subfile}
    sed -i "s|(executable_full_path)|${EXECUTABLE_FULL_PATH}|g" ${subfile}
	sed -i "s|(bash_script)|${BASH_SCRIPT}|g" ${subfile}
	sed -i "s|(sample)|${SAMPLE}|g" ${subfile}
	sed -i "s|(inputdir)|${INPUT_LIST}|g" ${subfile}
	sed -i "s|(output_root_file)|${OUTPUT_ROOT_FILE}|g" ${subfile}
	sed -i "s|(configdir)|${CONFIG_DIR}|g" ${subfile}
	sed -i "s|(is_local_job)|${isLocalJob}|g" ${subfile}
	sed -i "s|(job_flavour)|${JOB_FLAVOUR}|g" ${subfile}
	sed -i "s|(eos_dir)|${EOS_DIR}|g" ${subfile}
	sed -i "s|(timestamp)|${DATETIME}|g" ${subfile}

	sed -i "s|(nInstances)|${NINSTANCES}|g" ${subfile}
	cat ${subfile}

	if [[ "$SUBMITCONDOR" = true ]] ; then
		condor_submit -batch-name skim_${YEAR_OR_DESCRIPTION}_${DATETIME}_${SAMPLE} -file ${subfile}
	else
		echo "SUBMITCONDOR false, nothing submitted"
	fi


    }

done < ${SAMPLES}
