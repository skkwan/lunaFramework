#!/bin/bash

#-----------------------------------------------
# This is the bash script called by job.sub
#-----------------------------------------------

export X509_USER_PROXY=$1
export skimExecutable=$2
export sample=$3
export input_dir=$4
export output_root_file=$5
export config_dir=$6
export is_local_job=$7
# a tenth command-line argument will need this syntax: ="${10}"

input_file=$(basename ${input_dir})
# eos cp the input files to the worker node
while IFS= read -r line || [ -n "${line}" ]; do
    echo "${line}"
#    COMMAND="eos cp --silent ${line} ."
    COMMAND="eos cp ${line} ."
    echo ${COMMAND}
    eval ${COMMAND}
done < ${input_file}

COMMAND="${skimExecutable} ${sample} ${input_dir} ${output_root_file} ${config_dir} ${is_local_job}"

echo "Attempting to execute: ${COMMAND}"
eval ${COMMAND}

wait

# clean up the files we copied in
while IFS= read -r line || [ -n "${line}" ]; do
    echo "Need to clean up: ${line}"
    this_file="$(basename ${line})"
    COMMAND="rm ${this_file}"
    echo ${COMMAND}
    eval ${COMMAND}
done < ${input_file}
