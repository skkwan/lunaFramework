# lookForErrorFiles.py
# Usage:
#   cmsenv
#   cd ${CMSSW_BASE}/src/lunaFramework
#   source venv-LUNA/bin/activate
#   python lookForErrorsFiles.py --year=2018 --rootdir=[...] --dryrun
#

import argparse
import os
import re
import sys
import shutil

parser = argparse.ArgumentParser(
    description='Check a folder of Condor log files for failed jobs (jobs that do not have the "normal termination" string. e.g. logs/2024-11-14-00h04m'
)

parser.add_argument(
    "--execute", help="Create new rescue *.csvs.", dest="execute", action="store_true"
)
parser.add_argument("--year", help="Year", required=True)
parser.add_argument(
    "--rootdir", help="Directory with Condor logs to scan", required=True
)
parser.set_defaults(execute=False)

args = parser.parse_args()

# The directory where the dataset *.lists are
cmsswBase = os.getenv("CMSSW_BASE")
listdir = cmsswBase + "/src/lunaFramework/syncNanoAOD/nanoAODfilepaths"
# The directory to write the rescue *.lists to (typically a subdirectory /rescue)
rescuedir = cmsswBase + "/src/lunaFramework/condorSkim/rescue"

# The string in the .log file that indicates a succesful job
logfile_proofstring = "(1) Normal termination"
# outfile_proofstring = "Real time"

# ---- Setup ---------------------
# Check if the input directory exists
if not os.path.isdir(args.rootdir):
    raise Exception(f"Directory {args.rootdir} not found!")


# Loop through log files in the directory
for subdir, dirs, files in os.walk(args.rootdir):
    sample = os.path.basename(subdir)
    if sample == os.path.basename(args.rootdir):
        print(">>> Skipping " + sample)
        continue
    print(">>> " + sample)

    # ---- Setup --------------------------------------------------
    # Prepare the rescue directory by emptying it, if we are making new files'

    rescueTargets = os.path.join(rescuedir, args.year, sample + "_rescue.csv")
    print(rescueTargets)
    if not os.path.exists(os.path.join(rescuedir, args.year)):
        os.makedirs(os.path.join(rescuedir, args.year))
    if os.path.exists(rescueTargets) and args.execute:
        os.system(f"rm {rescueTargets}")

    # ---- End of setup -------------------------------------------

    # Loop through the Condor log files
    for filename in files:
        logpath = os.path.join(subdir, filename)

        # In .out files, check for the proofstring
        if ".log" in logpath:
            outpath = logpath.replace(".log", ".out")

            # Get batch number
            num = re.findall("\d+(?=\.\w+$)", filename)
            batchnum = 0
            if len(num) > 1:
                sys.exit("Could not find batch number in " + filename)
            # Mass points will trip up the regex
            elif ("DYJetsToLL_M-" not in filename) and (len(num) > 0):
                batchnum = num[0]

            isLogFileGood = True
            isOutFileGood = True

            with open(logpath) as flog:
                # First check the .log file.
                if logfile_proofstring not in flog.read():
                    isLogFileGood = False

                # # The output file is not always in existence, for instance if wall time was exceeded there will be no .out file
                # try:
                #     with open(outpath) as fout:
                #         if outfile_proofstring not in fout.read():
                #             isOutFileGood = False
                # except FileNotFoundError:
                #     isOutFileGood = False

                if not isLogFileGood:
                    print(
                        "... --> {}: normal .log termination string not found. Last three lines are:\n {} \n".format(
                            logpath, os.system("tail -n 3 {}".format(logpath))
                        )
                    )

                # if not isOutFileGood:
                #     # print("... --> {}: normal .out termination string not found. Last seven lines are:\n {} \n".format(outpath, os.system("tail -n 7 {}".format(outpath))))
                #     print(
                #         "... --> {}: normal .out termination string not found.".format(
                #             outpath
                #         )
                #     )

                # Want to submit one Condor job for each failed job
                # Need condorSkim.sh but have nInstances = 1,
                # INPUT_LIST says the batchnum instead of \$(Process)

                # Add flag arguments to condorSubmit.sh
                # https://www.baeldung.com/linux/use-command-line-arguments-in-bash-script

                # https://linuxize.com/post/bash-check-if-file-exists/
                # If there is a rescue_[sample].csv file, loop through it
                # and create a new sub file etc.

                if (not isLogFileGood) or (not isOutFileGood):
                    if batchnum != -1:
                        listname = sample + "_BATCH_" + str(batchnum) + ".list"
                    else:
                        listname = sample + ".list"
                    print(listname)
                    rescueList = os.path.join(listdir, args.year, sample, listname)

                    if not os.path.exists(rescueList):
                        sys.exit(
                            rescueList
                            + " does not exist; cannot proceed with rescue job creation"
                        )

                    if args.execute:
                        f = open(rescueTargets, "a")
                        f.write(str(batchnum) + "," + listname + "\n")
                        f.close()
                    else:
                        print("--execute flag not set: not create new batch lists")
