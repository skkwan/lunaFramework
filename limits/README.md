# Instructions for limits

1. From the Combine area ([see this repo](https://github.com/skkwan/CombineHarvester-haabbtt/tree/master/CombineTools)), on `lxplus` I have this in `public/combineArea/CMSSW_11_3_4/src/CombineHarvester/CombineTools`, running the `haabbtt` executable will get you a bunch of files called `higgsCombineTest.AsymptoticLimits.mH45.root` and so on. Hadd these into one file:
```bash
hadd higgsCombine_aa_bbtt.root higgsCombineTest.AsymptoticLimits.mH*.root
```
And copy this `higgsCombine_aa_bbtt.root` to this folder.

2. Run the script and this will make a `.png` and copy it to my EOS `www/` directory and website.
```bash
python3 runLimits.py
```
