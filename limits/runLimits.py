import os

outdir = "/eos/user/s/skkwan/www/"
outfolder = "limits/"
# false for expected limits only, true for expected and observed
showObserved = "false"

channels = ["mutau"]  # , "etau", "emu"]
years = ["2018"]
for year in years:
    for channel in channels:
        print(
            ">>> NOTE: running on cascade/ or noncascade/ subfolders -- make sure the files in there are updated!"
        )
        # os.system(
        #     f'root -l -b -q \'plotLimitsAsymm.cpp("cascade/higgsCombine_a1a2_4b2t_{year}_{channel}_m1_15.root", "4b2t", "{year}", "{channel}", "15", 2, {showObserved})\''
        # )
        # os.system(
        #     f'root -l -b -q \'plotLimitsAsymm.cpp("cascade/higgsCombine_a1a2_4b2t_{year}_{channel}_m1_20.root", "4b2t", "{year}", "{channel}", "20", 2, {showObserved})\''
        # )
        # os.system(
        #     f'root -l -b -q \'plotLimitsAsymm.cpp("cascade/higgsCombine_a1a2_4b2t_{year}_{channel}_m1_30.root", "4b2t", "{year}", "{channel}", "30", 2, {showObserved})\''
        # )

        os.system(
            f'root -l -b -q \'plotLimitsAsymm.cpp("noncascade/higgsCombine_a1a2_2b2t_{year}_{channel}_m1_15.root", "2b2t", "{year}", "{channel}", "15", 2, {showObserved})\''
        )
        os.system(
            f'root -l -b -q \'plotLimitsAsymm.cpp("noncascade/higgsCombine_a1a2_2b2t_{year}_{channel}_m1_20.root", "2b2t", "{year}", "{channel}", "20", 2, {showObserved})\''
        )
        os.system(
            f'root -l -b -q \'plotLimitsAsymm.cpp("noncascade/higgsCombine_a1a2_2b2t_{year}_{channel}_m1_30.root", "2b2t", "{year}", "{channel}", "30", 2, {showObserved})\''
        )
        os.system(
            f'root -l -b -q \'plotLimitsAsymm.cpp("noncascade/higgsCombine_a1a2_2b2t_{year}_{channel}_m1_40.root", "2b2t", "{year}", "{channel}", "40", 2, {showObserved})\''
        )
        os.system(
            f'root -l -b -q \'plotLimitsAsymm.cpp("noncascade/higgsCombine_a1a2_2b2t_{year}_{channel}_m1_50.root", "2b2t", "{year}", "{channel}", "50", 2, {showObserved})\''
        )

        outFolder = f"{outdir + outfolder}/limits_{year}_{channel}"
        os.system(f"mkdir -p {outFolder}")
        os.system(f"cp {outdir}index.php {outFolder}")
        os.system(f"cp *{channel}*.png {outFolder}")
        os.system(f"cp *{channel}*.pdf {outFolder}")

print(
    f"Check {outdir} and https://skkwan.web.cern.ch/{outfolder}limits_{year}_{channel} for outputs"
)
