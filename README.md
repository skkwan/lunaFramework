# Lightweight User-Friendly NanoAOD Analysis (LUNA)
This README.md describes this NanoAOD RDataFrame-based framework (C++ and Python) developed for a search for exotic Higgs decays to light neutral scalars to bottom quarks and tau leptons.
- The lightbulb 💡 denotes __the most common steps__ you will need to touch to tweak things.
- __Approximate runtimes__ are marked with a clock 🕰️ .
- The dragon 🐉 marks __infrequent steps__ that you will rarely need to re-run.
- __Configuration files for full Run-2__ are labeled with 🧲.

This framework runs on NanoAOD datasets.

## Table of contents
- [Installation](#installation)
- [At the start of each new shell session](#at-the-start-of-each-new-shell-session)
- [1. Boilerplate preparation of input NanoAOD datasets](#1-boilerplate-preparation-of-input-nanoaod-datasets)
- [2. Skim](#2-skim)
- [3. Postprocessing](#3-postprocessing)
- [4. Histogramming](#4-histogramming)
- [5. DataMCPlots](#5-datamcplots)
- [6. Limits](#6-limits)
- [Special documentation on adding the BDT score to data/MC plots](#special-documentation-on-adding-the-bdt-score-to-datamc-plots)
- [Special documentation on implementing BDT based categores](#special-documentation-on-implementing-the-bdt-based-categories)
- [Note on Pre-commits](#pre-commits)
- [Status](#status-as-of-january-2025)
- [Credits](#credits)

## Installation
1. Get a CMSSW release just for the libraries (Python3, ROOT releases, `correctionlib`). This repository uses a `CMSSW` environment but does not contain any `CMSSW` configuration files (i.e. no calls to `cmsRun`).
```bash
# e.g. on lxplus
$ cmsrel CMSSW_13_1_0_pre4
$ cd CMSSW_13_1_0_pre4/src/
# Create your own fork of LUNA, and then git clone it. The below line clones the original repository
$ git clone ssh://git@gitlab.cern.ch:7999/skkwan/lunaFramework.git
$ cd lunaFramework
$ cmsenv
```

2. Make a virtual environment e.g. called `venv-LUNA`, using a `.txt` of necessary packages.
```bash
$ cmsenv
$ virtualenv venv-LUNA
$ source venv-LUNA/bin/activate
# Now we are in the venv and the shell prompt will say (venv-LUNA)
(venv-LUNA)$ pip install -r environment-LUNA-requirements.txt
```
If there are error messages about other packages (tensorflow, poetry, etc.) they can be ignored as long as the message ends in
"Successfully installed cfgv-3.3.1 filelock-3.4.1 identify-2.4.4 importlib-metadata-4.8.3 importlib-resources-5.2.3 nodeenv-1.6.0 platformdirs-2.4.0 pre-commit-2.17.0 typing_extensions-4.1.1 virtualenv-20.17.1 zipp-3.6.0".

3. Clone the necessary external repositories and __unzip all the necessary files__. These are called in the various executables, and the jobs will fail if the relevant JSON is not unzipped.
First do this for the JSONs from the POGs:
```bash
# first cd to your /public/ directory: /afs/cern.ch/work/[first letter]/[your username]/public/
git clone https://gitlab.cern.ch/cms-nanoAOD/jsonpog-integration.git
cd jsonpog-integration/POG
# cd into TAU, MUO, JME, LUM, EGM, and BTV
# cd into each folder with UL (ultralegacy)
gunzip *
```
And do this for the repository with the Embedded scale factors:
```bash
# first cd back to /public/
git clone https://github.com/KIT-CMS/CROWN.git
cd CROWN-data/data/embedding/
gunzip *
```
_[jump back to table of contents](#table-of-contents)_

## Main workflow
### At the start of each new shell session
At the start of each shell session (or each new terminal, e.g. if you make a new terminal tab in VSCode), set up the environments. For each new `lxplus` session, get a grid certificate.
```bash
$ cmsenv
$ source venv-LUNA/bin/activate
$ voms-proxy-init --voms cms --valid 194:00
```
_[jump back to table of contents](#table-of-contents)_

### 1. Boilerplate preparation of input NanoAOD datasets
   - __What:__ Prepare the input NanoAOD datasets for all three years (done) and write `.txt` files containing the paths to the files in each dataset to run over.
      - __[🐉 Infrequent step]__ Preparing the input NanoAOD datasets:
         - (Done) The [Embedded samples for modeling Z to tautau](https://twiki.cern.ch/twiki/bin/viewauth/CMS/TauEmbeddingSamplesUL) which are specific to tau analyses (including this one) are centrally available only in MiniAOD. We convert these to NanoAOD using [central instructions](https://twiki.cern.ch/twiki/bin/viewauth/CMS/TauEmbeddingSamplesUL#NanoAOD_Conversion). This is already done.
         -  (Done) Monte Carlo bare NanoAOD does not contain the branches for the jet energy resolution systematics. There are a number of ways to get these branches, which are necessary for processing MC samples (but not data or Embedded samples).
            - When this framework was first created, there was only [NanoAODTools (link points to my fork)](https://github.com/skkwan/nanoAOD-tools) that uses [CRAB](https://twiki.cern.ch/twiki/bin/view/CMSPublic/WorkBookCRAB3Tutorial) to to run on bare NanoAOD datasets to apply jet energy corrections and jet energy resolution smearing.
            - Nowadays there are new recommendations to get the systematics on the fly for selected jets. The good thing about `NanoAODTools` is that it only needs to be run once, ever. The bad thing is that it computes systematics for every single jet in the event, and the resulting CRAB datasets take up a bunch of storage space.
            - This `NanoAODTools` step is already done for all the datasets that we should need, using the `merged` JEC uncertainties scheme which adds ~several dozen branches.
            - The CRAB output datasets live in `skkwan` storage on UW Tier 2.
      - 💡 __Once during setup and each time we change the input dataset, we need to get the paths (the logical file names, or LFNs) to each NanoAOD n-tuple to run over.__
         - 💡 __Run this once during setup and each time you change an input NanoAOD dataset:__ (runtime 🕰️: __several minutes interactively__)
            ```bash
            cd syncNanoAOD/nanoAODfilepaths/
            # edit the first few lines of getFilePaths.sh to point to the desired .txt (see below on how to format this, if you need to write your own)
            bash getFilePaths.sh
            ```
            This creates N `.list` files, where N is the (total number of NanoAOD files in the dataset)/(batch size). E.g. looking at one of the files,
            ```bash
            # for example, in 2018 VBFHToTauTau, this is batch number 0
            $ cat 2018/VBFHToTauTau/VBFHToTauTau_BATCH_0.list

            root://cmsxrootd.hep.wisc.edu//store/user/skkwan/NanoPost/VBFHToTauTau_M125_TuneCP5_13TeV-powheg-pythia8/NanoPost_VBFHToTauTau_RunIISummer20UL18NanoAODv9/240911_094709/0000/tree_30.root

            # for example, both 2016preVFP and 2016postVFP go into the 2016/ folder
            $ cat 2016/preVFP_VBFHToTauTau/preVFP_VBFHToTauTau_BATCH_0.list
            root://cmsxrootd.hep.wisc.edu//store/user/skkwan/NanoPost/VBFHToTauTau_M125_TuneCP5_13TeV-powheg-pythia8/NanoPost_VBFHToTauTau_RunIISummer20UL16NanoAODv9-preVFP/241021_115612/0000/tree_2.root
            ```
            The script also creates a `-mini.list` which contains only one ROOT file, which is helpful for testing in the skim step (see the skim step on how to do this).
         - FYI, the syntax for the `.txt` files is as follows, with fields separated by a comma:
           ```bash
           # first field: year
           # second field: dataset name (doesn't have to be the same as the DAS string)
           # third field: the DAS dataset name
           # fourth field: the desired number of NanoAOD input files in each batch, e.g. == 1 means 1 NanoAOD file per "batch".

           2018,DYJetsToLL_M-50,/DYJetsToLL_M-50_TuneCP5_13TeV-madgraphMLM-pythia8/skkwan-NanoPost_DYJetsToLL_M-50_RunIISummer20UL18NanoAODv9-1ea93bf7cfbb24e247bd0f4bd1955225/USER,1
           ```
         - (Done) The full Run-2 datasets with CRAB run on top of them to obtain the `merged` jet energy systematics, are listed in the following `.txt` files 🧲:
            - [logicalFileNames-2018_Merged_JER_uncertainties.txt](syncNanoAOD/nanoAODfilepaths/logicalFileNames-2018_Merged_JER_uncertainties.txt)
            - [logicalFileNames-2017_Merged_JER_uncertainties.txt](syncNanoAOD/nanoAODfilepaths/logicalFileNames-2017_Merged_JER_uncertainties.txt)
            - [logicalFileNames-2016preVFP_Merged_JER_uncertainties.txt](syncNanoAOD/nanoAODfilepaths/logicalFileNames-2016preVFP_Merged_JER_uncertainties.txt)
            - [logicalFileNames-2016postVFP_Merged_JER_uncertainties.txt](syncNanoAOD/nanoAODfilepaths/logicalFileNames-2016postVFP_Merged_JER_uncertainties.txt)

         - (Done) Since CRAB-published datasets do not correctly report the number of events in the dataset in [dasgoclient](https://github.com/dmwm/dasgoclient) or the [Data Aggregation Service (DAS)](https://cmsweb.cern.ch/das/), we also list the datasets before CRAB processing (i.e. bare NanoAOD for MC and data, and MiniAOD for Embedded) in the following `.txt` files for full Run-2 🧲:
            - [logicalFileNames-2018.txt](syncNanoAOD/nanoAODfilepaths/logicalFileNames-2018.txt)
            - [logicalFileNames-2017.txt](syncNanoAOD/nanoAODfilepaths/logicalFileNames-2017.txt)
            - [logicalFileNames-2016preVFP.txt](syncNanoAOD/nanoAODfilepaths/logicalFileNames-2016preVFP.txt)
            - [logicalFileNames-2016postVFP.txt](syncNanoAOD/nanoAODfilepaths/logicalFileNames-2016postVFP.txt)

      These `logicalFileNames*.txt` files will be used later again in the `postProcessing` step (step #3) in a script to query `dasgoclient` for the number of events in each dataset, enabling us to correctly scale our histograms.

   _[jump back to table of contents](#table-of-contents)_
### 2. `skim`
   - __What:__ Out of all events, select events consistent with the signal h -> a1a2 -> 4b2tau/2b2tau, or events needed later to estimate background processes.
      - The minimal amount of information for each event is saved at this stage, but it needs to be sufficient to run `FastMTT` (`SVFit`) and the `BDT`.
      - There will typically be __tens to hundreds__ of output skim n-tuples per NanoAOD dataset. Runtime 🕰️: __one to few hours Condor walltime.__
      - The lepton energy scales (e.g. tau energy scale, muon energy scale, electron energy scale) are applied at this stage.
   - __Where:__
      - The main workhorse executable is [syncNanoAOD/skim.cxx](syncNanoAOD/skim.cxx).
      - Specify the year and datasets to run inside a `.csv` in this format:
         ```
         # separated by commas: the dataset name (affects the output name), the path to the `.list` folder that we obtained in the previous step,
         # the year, and the path to the `skim` config files that contain some of the baseline cuts that we use (these are already committed to the repo)
         VBFHToTauTau,nanoAODfilepaths/2018/VBFHToTauTau,2018,config/2018/

         # to do small tests, it is also possible to point to the `-mini.list` that we obtained in the previous step
         VBFHToTauTau-mini,nanoAODfilepaths/2018/VBFHToTauTau-mini,2018,config/2018/
         ```
         This is done for the full Run-2 in the files 🧲:
         - [skim_FULL_stack_2018.csv](syncNanoAOD/skim_FULL_stack_2018.csv)
         - [skim_FULL_stack_2017.csv](syncNanoAOD/skim_FULL_stack_2017.csv)
         - [skim_FULL_stack_2016.csv](syncNanoAOD/skim_FULL_stack_2016.csv)
      - 💡/(not frequent if n-tuples are stable 🐉) Compile the executable (🕰️ __several minutes interactively__) and interactively run it with [syncNanoAOD/runSkim.sh](syncNanoAOD/runSkim.sh):
         ```bash
         cd syncNanoAOD/
         bash runSkim.sh
         ```
      - 💡/(not frequent if n-tuples are stable 🐉) Submit Condor skimming jobs with [condorSkim/runCondorSubmit.sh](condorSkim/runCondorSubmit.sh) which may take 🕰️ __a few seconds to several minutes interactively__ for many datasets.
         ```bash
         cd condorSkim/
         bash runCondorSubmit.sh
         ```
      -  💡/(__Start here if you have new ntuples i.e. new BDT training or some other changes upstream__) After the Condor jobs are done running, create the metadata `info_*.root` files (🕰️: __few minutes interactively__) using [condorSkim/haddFiles.py](condorSkim/haddFiles.py) (see that file for a more detailed description). All the metadata stores is the output of `hadd -T`, which `hadd`s only the non-TTree structures, i.e. the `TH1F` that stores a simple count of how many events were processed.
         ```bash
         cd condorSkim/
         # --execute flag will create the metadata files, --recreate will recreate any existing metadata files
         python3 haddFiles.py --rootdir=<top-level path to output ntuples> --execute
         ```
      - 🐉 SVFit or BDT module needs to be run at this step (this is done in a separate repository).
         - The SVFit/BDT jobs can make outputs that __do not begin indexing at 0__ and we also need to make the metadata `info_*.root`.
         - So, if you are copying the new n-tuples from someone else's work directory where you do not have write permission and that folder does not have `out_0.root`, you need to first __copy the new n-tuples to your own area__, and run this script which renames the `out_N.root` to `out_0.root` (where `N` is the largest number in that folder).
         - Check the dataset names in the input folder and change contents of [condorPostProcessing/renameSVFittedNTuples.py](condorPostProcessing/renameSVFittedNTuples.py) accordingly.
         ```bash
         # First cp the SVFitted directory to your own folder in the PAG EOS area
         cd condorPostProcessing/
         # Check that the list of folders (datasets) in this script is correct
         # Only do this once: it will rename the highest-numbered file (e.g. out_49.root) to out_0.root because the next step assumes zero-indexed file names
         python3 renameSVFittedNTuples.py --dir=<top-level path to your copy of the files>
         # When you are ready, add the --execute flag to the above command
         python3 renameSVFittedNTuples.py --dir=<top-level path to your copy of the files> --execute
         ```
   _[jump back to table of contents](#table-of-contents)_
### 3. `postProcessing`
   - __What:__ From the skimmed SVFit/BDT n-tuples, compute all the necessary corrections and systematics.
      - There will typically be __tens to hundreds__ of postprocess n-tuples for each input NanoAOD dataset. Runtime: 🕰️: __several hours Condor walltime.__
      - Here you can add new branches to the postprocess n-tuples by editing [postProcessing/helpers/finalVariables.h](postProcessing/helpers/finalVariables.h).
   - __Where:__
      - The main workhorse executable is [postProcessing/postprocess-experimental.cxx](postProcessing/postprocess-experimental.cxx).
      - 💡 Compile (🕰️ __several minutes interactively__) and interactively run it using [postProcessing/runPostProcess-experimental.sh](postProcessing/runPostProcess-experimental.sh) after editing this script with your proxy location.
         ```bash
         cd postProcessing/
         bash runPostProcess-experimental.sh
         ```
      - 💡 Submit Condor postprocessing jobs with [condorPostProcessing/runCondorPostprocessing.sh](condorPostProcessing/runCondorPostprocessing.sh). Submitting may take 🕰️ __a few seconds to several minutes interactively__ for many datasets. The input `.csv` file to this script should exist in the `postProcessing` folder. Confirm all the input arguments/options inside before running!
         ```bash
         cd condorPostProcessing/
         bash runCondorPostprocessing.sh
         ```
         - __Note:__ Sometimes jobs will go to status HOLD / held with the error message:
         ```bash
         10820692.061:  Job is held.
         Hold reason: Transfer output files failure at execution point slot1_20@b9p29p9581.cern.ch using protocol root. Details: RuntimeError: b'Run: [ERROR] Server responded with an error: [3011] Unable to remove file for truncation /eos/cms/store/group/phys_susy/AN-24-166/skkwan/condorPostProcessing/2024-12-18-12h13m/TTToSemiLeptonic/skkwan.cc; No such file or directory (destination)\n\n' ( URL file = root://eosuser.cern.ch//eos/cms/store/group/phys_susy/AN-24-166/skkwan/condorPostProcessing/2024-12-18-12h13m/TTToSemiLeptonic/skkwan.cc )|
         ```
         The `<user>.cc` file is a [Condor credentials cache file](https://htcondor.readthedocs.io/en/latest/admin-manual/file-and-cred-transfer.html). This hold error happens from time to time (especially with many jobs writing to the same output directory) and is not a problem with the `postprocess-experimental.cxx` job itself. Periodically run `condor_release -all` until the jobs complete.
         - 💡 After the jobs complete, create the metadata files (🕰️: __few minutes interactively__):
            ```bash
            cd condorPostProcessing/
            python3 haddPostprocessed.py --dir=<top-level path to output ntuples> --execute
            ```
   _[jump back to table of contents](#table-of-contents)_
### 4. `histogramming`
   - __What:__ From the postprocess n-tuples, create histograms for each desired variable and the systematic variations (either for only `m_tt` or all the control plot variables).
      - There will be __one__ output histogram corresponding to __one value of one variable from each input NanoAOD dataset, in one channel__ (e.g. one histogram for `TTTo2L2Nu`, `pt_1`, nominal (`nom`) value, `mutau` channel).
      - The variables to make histograms of are defined in [histogramming/histogramming.cxx](histogramming/histogramming.cxx) `std::vector<std::string> vars`.
      - The ranges and bin numbers of the variables are defined in [histogramming/helpers/ranges.h](histogramming/helpers/ranges.h).
      - One helper file in histogramming/helpers books all the affected variables using the [RDataFrame method .Vary](https://root.cern/doc/master/classROOT_1_1RDF_1_1RInterface.html#a84d15369c945e4fe85e919224a0fc99f).
      - The categories are defined in [histogramming/histogramming.cxx](histogramming/histogramming.cxx) `std::vector<std::vector<std::string>> categories`.
   - __Where:__
      - The main workhorse executable is [histogramming/histogramming.cxx](histogramming/histogramming.cxx).
      - 💡 Compile (🕰️ __several minutes interactively__) and interactively run it with [histogramming/runHistogramming.sh](histogramming/runHistogramming.sh):
         ```bash
         cd histogramming/
         bash runHistogramming.sh
         ```
      - 💡 Submit Condor histogramming jobs with [histogramming/runCondorHistogramming.sh](histogramming/runCondorHistogramming.sh) (submitting may take 🕰️ __few seconds to several minutes interactively__ for many datasets). It is possible to run the histograms for only the final fitting variable (`m_tt`) in 🕰️ __20 minutes Condor walltime__ which is very good for datacards and debugging, or for all the variables which will take 🕰️ __20 minutes Condor walltime__.
         ```bash
         cd histogramming/
         bash runCondorHistogramming.sh
         ```
      - 💡 After the jobs complete, manually `hadd` the histograms (runtime 🕰️: __few minutes interactively__):
         ```bash
         # cd to the timestamped output directory in EOS
         hadd -f -j -k histograms.root */*.root
         ```
      - __Important note: avoid submitting twice for the same year before the first set of jobs complete:__ e.g. if I submit jobs for 2018 all variables and the jobs are still running, I avoid submitting 2018 again before they finish, because `runCondorHistogramming.sh` will delete any existing `2018/*.list` files. If I submit another batch of jobs, the first batch of jobs will complain about not being able to find the `2018/*.list` files and fail.
   _[jump back to table of contents](#table-of-contents)_
### 5. `dataMCPlots`
   - __What:__ Inside a bash wrapper script [dataMCPlots/runBkgEstimation.sh](dataMCPlots/runBkgEstimation.sh), call [dataMCPlots/backgroundEstimation.py](dataMCPlots/backgroundEstimation.py) which groups the histograms (one per input NanoAOD dataset) into groups that are ultimately displayed in the data/MC and the datacards, and saves every histogram and up/down shape to an output file `out_mutau.root`/`out_etau.root`/`out_emu.root`. In [dataMCPlots/runBkgEstimation.sh](dataMCPlots/runBkgEstimation.sh):

      - Set `TIMESTAMP`, where the `hadd`ed outputs of `histogramming` are located at `/eos/cms/store/group/phys_susy/AN-24-166/${USER}/condorHistogramming/${TIMESTAMP}/histograms.root`. If some day you have a different file structure feel free to change this.
      - Set `YEAR`, which will be `2018`, `2017`, `2016preVFP`, or `2016postVFP` (no parentheses).
      - Set `CHANNEL`, which will be `"mutau"`, `"etau"`, or `"emu"` (with parentheses).
      - Set `DATACARD_MODE` to be `true` to make histograms for only the final fit variable in all event categories, or `false` to do all variables (currently only in `inclusive` region, otherwise it will take a long time).
      - Set `DO_BKG_ESTIMATION` to `true` perform the background estimation. Or if you already did it once and only want to tweak plotting, set to `false`.

      At the top of [dataMCPlots/backgroundEstimation.py](dataMCPlots/backgroundEstimation.py) we import configuration files to control the background estimation:

      - The variables and their ranges (should match with what was produced in `histogramming`) are declared in [dataMCPlots/config/variables.py](dataMCPlots/config/variables.py) for background estimation. Note that the variables to _plot_ are declared again in [dataMCPlots/stackPlots.py](dataMCPlots/stackPlots.py).
      - From the previous step, we have one `sample` per DAS dataset `TTTo2L2Nu`, `TTToHadronic`, and `TTToSemiLeptonic`.
      - The `sample`s are organized into `group`s. E.g. `ttbar` consists of the sum of `TTTo2L2Nu`, `TTToHadronic`, and `TTToSemiLeptonic`. The `samples` in each `group` is defined per year per channel in [dataMCPlots/config/samplesInGroups.py](dataMCPlots/config/samplesInGroups.py).
      - The `group`s are declared per channel per year in [dataMCPlots/config/groups.py](dataMCPlots/config/groups.py).
      - One exception is the background jet faking tauh (`fake`) (for `mutau` and `etau`) and the QCD background (`qcd`) (for `emu`) (see the AN for details) and these must be computed in this step.
      - The systematics affecting each `group` are declared in [dataMCPlots/config/systematics.py](dataMCPlots/config/systematics.py) in the `dictSystematics` dictionary per channel per year.
      - After computation, save these `group` histograms into a file `out_mutau.root` (or `out_etau.root` or `out_emu.root`) which we can then quickly plot/re-plot from.
      - There will be a print statement prompting you to copy the `out_mutau.root`/`out_etau.root`/`out_emu.root` to the Combine area (only do this if you made the `DATACARD_MODE` true files, and are happy with the results, because it will overwrite whatever is there).

      Next, the bash wrapper script [dataMCPlots/runBkgEstimation.sh](dataMCPlots/runBkgEstimation.sh) will call [dataMCPlots/runStackPlots.sh](dataMCPlots/runStackPlots.sh) which finally calls [dataMCPlots/stackPlots.py](dataMCPlots/stackPlots.py), running on `out_mutau.root`/`out_etau.root`/`out_emu.root`. The reason for the separate steps is to help with copying the plots to an output area.

      - The variables to plot are declared at the top of [dataMCPlots/stackPlots.py](dataMCPlots/stackPlots.py).
      - It is possible to make plots for each mass point. Uncomment/comment the various mass points (Ctrl+F `cascade` and `non-cascade`) to find the loop over mass points.

   - __Where:__
      - 💡 There is one bash script which encapsulates all the above steps (see above for all the flags that need to be set) [dataMCPlots/runBkgEstimation.sh](dataMCPlots/runBkgEstimation.sh):
         ```bash
         cd dataMCPlots/
         # Set TIMESTAMP, YEAR, CHANNEL, DATACARD_MODE, DO_BKG_ESTIMATION flags
         bash runBkgEstimation.sh
         ```
         - It first calls [dataMCPlots/backgroundEstimation.py](dataMCPlots/backgroundEstimation.py). This takes __🕰️ around five minutes interactively__ if only doing one variable, or __🕰️ about ten minutes interactively__ for all variables.
         - Next, it calls [dataMCPlots/runStackPlots.sh](dataMCPlots/runStackPlots.sh), which in turn calls [dataMCPlots/stackPlots.py](dataMCPlots/stackPlots.py).
            - It is possible to run the plotting step only (__🕰️ few minutes interactively__) to tweak cosmetics: set `DO_BKG_ESTIMATION` to be `false` at the top of [dataMCPlots/runBkgEstimation.sh](dataMCPlots/runBkgEstimation.sh).
   _[jump back to table of contents](#table-of-contents)_
### 6. `Limits`
   The limits are run on the `out_mutau.root`/`out_etau.root`/`out_emu.root` files produced with `DATACARD_MODE` = `true` in the previous step.
   1. The [Combine repository](https://github.com/skkwan/CombineHarvester-haabbtt) needs to be set up separately, in a new CMSSW area (referred to as `${combine_CMSSW_BASE}`).
   2. If satisfied with the data/MC from the previous step, copy `out_mutau.root`/`out_etau.root`/`out_emu.root` to the directory `${combine_CMSSW_BASE}/src/auxiliaries/shapes${YEAR}` (be careful about overwriting any existing files there).
   3. Run the steps in [CombineHarvester-haabbtt/CombineTools/README.md](https://github.com/skkwan/CombineHarvester-haabbtt/blob/devel-fullSys/CombineTools/README.md) (note: URL currently goes to the current working branch `devel-fullSys` not `master`).
   Included in this repo and README.md are:
      - Creating the `.txt` datacards
      - Getting expected limits
      - Combining limits from three channels into one year (to-do: limits combined across years)
      - Getting blinded impacts for a specific mass point
      - (After unblinding) Getting observed limits, unblinded impacts, postfit distributions
   4. As stated at the end of that README, the Combine steps culminate in copying the limits back to this repository for plotting:
   ```bash
   # copy the files to ${CMSSW_BASE}/src/limits/cascade or ${CMSSW_BASE}/src/limits/noncascade
   cd ${CMSSW_BASE}/src/limits
   ls cascade/
   # higgsCombine_a1a2_4b2t_2018_mutau_m1_15.root, higgsCombine_a1a2_4b2t_2018_mutau_m1_20.root, etc.
   # comment/uncomment the commands in this file to only do cascade or noncascade or both
   python3 runLimits.py
   ```
   5. (After unblinding) To plot postfit distributions, the `postfitShapes*.root` should be copied to the path pointed to in [dataMCPlots/runPostFitPlots.py](dataMCPlots/runPostFitPlots.py).
   ```bash
   cd ${CMSSW_BASE}/src/dataMCPlots
   python3 runPostFitPlots.py
   ```
   _[jump back to table of contents](#table-of-contents)_

## Special documentation on adding the BDT score to data/MC plots
1. Since the BDT score (call it `score`) will be added right before the [postProcessing](#3-postprocessing) step, there should be nothing to do in `postProcessing` except propagating `score` and its variations `score_*` to the next step.
   - Add all branches to [postProcessing/helpers/finalVariables.h](postProcessing/helpers/finalVariables.h) (make sure there is a trailing comma after the last one, otherwise it will treat two variables as mushed into one). If you add a branch to `std::vector<std::string> allCommonVariables`, [postProcessing/postprocess-experimental.cxx](postProcessing/postprocess-experimental.cxx) will throw an error if it does not find that branch. Whereas branches in `std::vector<std::string> optionalVariables` will be checked for one by one and an error will not be thrown if it is not found.

   For example:
      ```cpp
      # nominal branch could go in allCommonVariables or optionalVariables
      "score",
      # probably makes sense to put the variations in optionalVariables
      "score_JetAbsoluteUp", "score_JetAbsoluteDown",
      ...
      ```
2. Booking the `score` variations in [histogramming](#4-histogramming) will be slightly more involved but you can follow the pattern for other variables. For every systematic affecting `score`, find its corresponding helper function and add a `.Vary()` call.
   - E.g. To add the variations for `JetAbsolute` -> Ctrl+F in histogramming/helpers -> one result in [histogramming/helpers/jerShifts-histo.h](histogramming/helpers/jerShifts-histo.h). Book the variation:
      ```cpp
      [...]
      # in jerShifts-histo.h add something like
      .Vary("score", "ROOT::RVecF{score_JetAbsoluteUp, score_JetAbsoluteDown}", {"up", "down"}, "JetAbsolute")
      # be careful of typos, or accidentally swapping the up and down branches
      [...]
      ```
   As shown in [postProcessing/helpers/jerShifts-histo.h](postProcessing/helpers/jerShifts-histo.h) it is possible to declare multiple variables as being `.Vary()`'d by the same systematic.
3. In the main function of [histogramming/histogramming.cxx](histogramming/histogramming.cxx):
   - To simply add the BDT score to the data/MC plots, add `"score"` to [histogramming/histogramming.cxx](histogramming/histogramming.cxx) `std::vector<std::string> vars`
   - To define the BDT score histogramming ranges, add to [histogramming/helpers/ranges.h](histogramming/helpers/ranges.h) `ranges`.
4. To add the BDT score to the background estimation, add to [dataMCPlots/config/variables.py](dataMCPlots/config/variables.py) `ranges` with the same number of bins and range as was used in the histogramming step.
5. To plot the BDT score and give it an x-axis legend label, add it to the [dataMCPlots/stackPlots.py](dataMCPlots/stackPlots.py) `labels` list.

_[jump back to table of contents](#table-of-contents)_

## Special documentation on implementing the BDT based categories
1. In `histogramming`
   - To define BDT score-based categories, edit `std::vector<std::vector<std::string>> categories`, leaving `inclusive` unchanged:
      ```cpp
      # do this for each channel mutau, etau, emu
      categories = {
          {"inclusive", "true"},  // dummy cut
          {"myBDTCategory1", "(score > 0.90) && ..."},
          {"myBDTCategory2", "(score < 0.90) && (score > 0.80) && ..."}
         ... }
      ```
2. In [dataMCPlots/backgroundEstimation.py](dataMCPlots/backgroundEstimation.py), Ctrl+F and edit the `categories` list used when `args.datacard` is true.
3. In [dataMCPlots/stackPlots.py](dataMCPlots/stackPlots.py), Ctrl+F and edit the `categories` list used when `args.datacard` is true.
4. The [Combine and limits step](#6-limits) will also need to be modified.

_[jump back to table of contents](#table-of-contents)_

## Pre-commits
This repository has pre-commits configured (read more about pre-commits [here](https://pre-commit.com)) to do automatic code formatting and checks.
This is configured in [.pre-commit-config.yaml](.pre-commit-config.yaml) and can be changed.
As you can see in the configuration file, the pre-commits include a code formatting for all Python scripts called [Black](https://black.readthedocs.io/en/stable/index.html),
as well as some checks like removing trailing white spaces.

Whenever a new Git commit is created with `git commit`, all uncommitted changes are __stashed__. If `git commit` is interrupted at any point and it seems that the
uncommitted changes are "gone" (no longer appearing in the current working directory), they should be recoverable with `git stash apply`.
```bash
(venv-LUNA) [skkwan@lxplus931 lunaFramework]$ git commit -m "update ci yml"
[WARNING] Unstaged files detected.
[INFO] Stashing unstaged files to /afs/cern.ch/user/s/skkwan/.cache/pre-commit/patch1732728354-4007834.
# etc.
```
If any of the tests fail, the pre-commit will re-format the code so that the tests pass, on top of the user's changes.
Run `git add` for the modified files, and run `git commit` again.

In the unusual circumstance of wanting to manually run the pre-commit it can be done with this:
```bash
# In the highly unusual case of needing to manually run the pre-commit
pre-commit run -a
```

_[jump back to table of contents](#table-of-contents)_

## Status as of January 2025
- Completing documentation.
- Adding dedicated section(s) for how to add a variable.
- Next major development steps are adding the BDT-based event categories.
_[jump back to table of contents](#table-of-contents)_

## Credits
Structure and code initially based on the [CI/CD Virtual Workshop](https://awesome-workshop.github.io/awesome-htautau-analysis/).

Some functions and header files are originally authored by others and cited where possible.

Major analysis elements and scale factors have been developed by other collaborators on the analysis.
_[jump back to table of contents](#table-of-contents)_
