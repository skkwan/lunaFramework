# README for commonFiles

- `emu` channel QCD scale factors for 2018 UL and 2017 UL are copied from `/eos/cms/store/group/phys_susy/AN-24-166/pdas/QCD_emu_SF` (`osss_em_2018.root`, `closure_em_2018.root`, `osss_em_2017.root`, `closure_em_2017.root`)
- `htt_scalefactors_legacy_2018.root` are from the HIG-22-007 analysis, currently we only use a few corrections from it (e.g. the Z pT reweighing) for UL.
- `lumi/*.txt` golden JSON files are from [the LUMI POG](https://twiki.cern.ch/twiki/bin/view/CMS/TWikiLUM) (Ctrl+F "Recorded lumi, legacy golden JSON").
- `jetFakeRates/*.root` are from Anagha
