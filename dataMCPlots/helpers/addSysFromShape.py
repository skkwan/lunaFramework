import numpy as np
import ROOT

from helpers.getHists import getHistogramSum


def addSystematicFromHistograms(tAsymm, hNominal, hUp, hDown):
    """
    Add systematic errors in quadrature (defined by TH1 hUp, hNominal, hDown) to an existing TGraphAsymmErrors tAsymm. Returns nothing.
    """

    # Include overflow
    nBins = hUp.GetNbinsX() + 1

    # print(hNominal)
    # print(hUp)
    # print(hDown)

    # Validate inputs
    # print(f">>> {hUp.GetNbinsX()}, {hDown.GetNbinsX()}")
    assert hUp.GetNbinsX() == hDown.GetNbinsX()

    # print(
    #     f"central value for iBin {0}: x-value {hNominal.GetBinCenter(0)}, {hNominal.GetBinContent(0)}"
    # )

    # Add errors in quadrature by bin
    for iBin in range(1, nBins):
        iAsymm = iBin - 1
        errUp = tAsymm.GetErrorYhigh(iAsymm)
        errDown = tAsymm.GetErrorYlow(iAsymm)

        errUpFromSys = hUp.GetBinContent(iBin) - hNominal.GetBinContent(iBin)
        errDownFromSys = hDown.GetBinContent(iBin) - hNominal.GetBinContent(iBin)

        totalErrUp = np.sqrt(np.power(errUp, 2) + np.power(errUpFromSys, 2))

        # print(
        #     f"central value for iBin {iBin}: x-value {hNominal.GetBinCenter(iBin)}, {hNominal.GetBinContent(iBin)}, systematic to add up/down are {errUpFromSys}, {errDownFromSys}"
        # )

        # print(
        #     f".... bin {iBin}: totalErrUp {totalErrUp} from existing errUp {errUp}, errUpFromSys {errUpFromSys}"
        # )

        totalErrDown = np.sqrt(np.power(errDown, 2) + np.power(errDownFromSys, 2))
        # print(
        #     f".... bin {iBin}: totalErrDown {totalErrDown} from existing errDown {errDown}, errDownFromSys {errDownFromSys}"
        # )

        # print(
        #     f".... bin {iBin}: setting tAsymm bin {iAsymm} with center {tAsymm.GetPointX(iAsymm)}, which had original errors {errUp}, {errDown}. Now: {totalErrUp}, {totalErrDown}"
        # )

        tAsymm.SetPointEYhigh(iAsymm, totalErrUp)
        tAsymm.SetPointEYlow(iAsymm, totalErrDown)

    # print(
    #     f"central value for iBin {nBins + 1}: x-value {hNominal.GetBinCenter(nBins+1)}, {hNominal.GetBinContent(nBins+1)}"
    # )


def addSystematicInQuadrature(
    tfile: ROOT.TFile,
    tAsymm: ROOT.TGraphAsymmErrors,
    processes: list,
    variable: str,
    channel: str,
    sys: str,
    category: str,
) -> None:
    """
    Add systematic errors in quadrature to the existing TGraphAsymmErrors tAsymm, where the systematic affects the processes
    listed as strings in processes, for the given variable and systematic.
    """
    hNominal = getHistogramSum(tfile, processes, variable, channel, "", category)
    hUp = getHistogramSum(
        tfile, processes, variable, channel, "_" + sys + "Up", category
    )
    hDown = getHistogramSum(
        tfile, processes, variable, channel, "_" + sys + "Down", category
    )

    addSystematicFromHistograms(tAsymm, hNominal, hUp, hDown)


def addSystematicByGroup(
    tfile, tAsymm, sysDict, histGroupsDict, variable, channel, category=""
):
    """
    Add systematic errors in quadrature to the existing TGraphAsymmErrors tAsymm:
    sysDict is a dictionary containing at least two key-value pairs: "systematics" with a list of the systematic names in the histograms to get,
    and "affectedProcesses", which contains a list of affected process-families. Each process-family must appear as a key in histGroupsDict, where
    the value is the list of actual processes (e.g. DYJets) to get.
    """
    for sys in sysDict["systematics"]:
        processes = []
        # print("sysDict['affectedProcesses'] is ", sysDict["affectedProcesses"])
        for affProc in sysDict["affectedProcesses"]:
            # print("Adding systematic {} for affected process {}".format(sys, affProc))
            # print(
            #     "Going into histGroupsDict[{}]".format(affProc),
            #     " and getting ",
            #     histGroupsDict[affProc],
            # )
            processes += histGroupsDict[affProc]
        # print(
        #     ">>> Adding systematics in quadrature for",
        #     sys,
        #     proce
        # )
        addSystematicInQuadrature(
            tfile, tAsymm, processes, variable, channel, sys, category
        )


def addAllSystematicsForYear(
    tfile, tAsymm, allSysDict, histGroupsDict, variable, channel, category=""
):
    """
    Same as above function but allSysDict is a dictionary contains one dictionary per group of systematics.
    histGroupsDict is a dictionary where the keys are names of process-families (e.g. "MC") and the values are the processes that comprise the family, e.g.
    MC includes DYJets, ttbar, ST, etc.
    """
    for group in allSysDict:
        addSystematicByGroup(
            tfile,
            tAsymm,
            allSysDict[group],
            histGroupsDict,
            variable,
            channel,
            category,
        )
