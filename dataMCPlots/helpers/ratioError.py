import numpy as np
import ROOT


def getRatioWithErrors(
    hNumerator: ROOT.TH1F, tDenominator: ROOT.TGraphAsymmErrors
) -> ROOT.TGraphAsymmErrors:
    """
    Return a TGraphAsymmErrors representing the ratio of a numerator (which has no uncertainties, e.g. data) and a denominator which is a TGraphAsymmErrors (e.g. MC).
    """

    # Include overflow
    nBins = hNumerator.GetNbinsX() + 1
    # print("getRatioWithErrors", nBins)

    # Build the TGraphAsymm by hand with six arrays
    x = np.zeros(nBins)
    y = np.zeros(nBins)

    xErrLow = np.zeros(nBins)
    yErrLow = np.zeros(nBins)

    xErrHigh = np.zeros(nBins)
    yErrHigh = np.zeros(nBins)

    # Calculate ratio and its errors bin by bin
    for iBin in range(nBins):
        numeratorX = hNumerator.GetBinCenter(iBin)
        numeratorY = hNumerator.GetBinContent(iBin)

        # TGraphAsymmError has an underflow bin so to get the same x-axis point we need this
        denominatorX = tDenominator.GetPointX(iBin - 1)
        denominatorY = tDenominator.GetPointY(iBin - 1)

        ratioNominal = 1

        if denominatorY == 0:
            # Do not divide by zero
            ratioNominal = 0
        else:
            ratioNominal = numeratorY / denominatorY

        # Nominal points
        x[iBin] = numeratorX
        y[iBin] = 1.0

        # x-axis errors are just the bin width
        xErrLow[iBin] = hNumerator.GetBinWidth(iBin) / 2
        xErrHigh[iBin] = hNumerator.GetBinWidth(iBin) / 2

        # y-axis errors
        # Of the denominator
        denominatorY_errHigh = tDenominator.GetErrorYhigh(iBin - 1)
        denominatorY_errLow = tDenominator.GetErrorYlow(iBin - 1)

        # By error propagation of ratio = numerator/denominator, assuming numerator does not have uncertainties:
        # sigma_ratio = ratio * (sigma_denominator / denominator)
        if denominatorY != 0:
            yErrLow[iBin] = ratioNominal * (denominatorY_errLow / denominatorY)
            yErrHigh[iBin] = ratioNominal * (denominatorY_errHigh / denominatorY)
        else:
            yErrLow[iBin] = 0
            yErrHigh[iBin] = 0

        # print(
        #     iBin,
        #     (
        #         numeratorX,
        #         numeratorY,
        #         "+/-",
        #         (denominatorY_errHigh, denominatorY_errLow),
        #     ),
        #     "with denominator",
        #     (denominatorX, denominatorY),
        #     "with ratio",
        #     ratioNominal,
        #     "that has error",
        #     (yErrLow[iBin], yErrHigh[iBin]),
        # )

    hAsymmError = ROOT.TGraphAsymmErrors(
        nBins, x, y, xErrLow, xErrHigh, yErrLow, yErrHigh
    )

    return hAsymmError
