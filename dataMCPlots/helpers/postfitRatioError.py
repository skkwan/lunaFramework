import argparse
import numpy as np
import ROOT


def postfitRatioError(
    hNumerator: ROOT.TH1F,
    hDenominator: ROOT.TH1F,
    setCentralValueToUnity=True,
) -> ROOT.TH1F:
    """
    For a numerator TH1F representing the data and denominator TH1F representing the MC, return a ratio TH1F.

    If z = x/y,
    (sigma_z) / z = sqrt((sigma_x/x)^2 + (sigma_y/y)^2)

    z is the ratio, x is MC, and y is data

    """

    # print("hDenominator.GetN(): {}".format(hDenominator.GetN()))
    # print("hNumerator.GetNBins() + 1: {}".format(hNumerator.GetNbinsX() + 1))

    nBins = hDenominator.GetNbinsX()

    # Initialize
    hRatio = hNumerator.Clone("hRatio")
    hRatio.Divide(hDenominator)

    for iBin in range(nBins):
        # Compute the actual ratio
        if hNumerator.GetBinContent(iBin) != 0:
            # The first bin data is zero
            ratioValue = hDenominator.GetBinContent(iBin) / hNumerator.GetBinContent(
                iBin
            )
        else:
            ratioValue = 1.0

        # Get (x, y) point
        if setCentralValueToUnity:
            hRatio.SetBinContent(iBin, 1.0)
        else:
            hRatio.SetBinContent(iBin, ratioValue)

        # By error propagation of ratio = numerator/denominator, assuming numerator (data) does not have uncertainties:
        # (Stat error of data is plotted separately)
        # sigma_ratio = ratio * (sigma_denominator / denominator)
        sigma_denominator = hDenominator.GetBinError(iBin)
        denominator = hDenominator.GetBinContent(iBin)

        if denominator != 0:
            sigma_ratio = ratioValue * (sigma_denominator / denominator)
            hRatio.SetBinError(iBin, sigma_ratio)
        else:
            sigma_ratio = 0
            hRatio.SetBinError(iBin, sigma_ratio)

    return hRatio
