# getHists.py
def getYield(h):
    """
    Get the yield of a histogram.
    """
    y = h.Integral()
    return y


def getHistogram(tfile, name, variable, channel="mutau", category="", tag=""):
    """
    Retrieve a histogram from the input file based on the process and the variable
    """
    # print(">>> getHistogram: file: {} \n name: {} \n variable: {} \n category: {} \n tag: {}".format(tfile, name, variable, category, tag))
    if category == "":
        hName = "{}_{}{}".format(name, variable, tag)
        # print("looking for histo", hName)
    else:
        hName = "{}/{}_{}{}".format(category, name, variable, tag)
    if variable == "":
        if category != "":
            hName = f"{category}/{name}"
    h = tfile.Get(hName)
    if not h:
        raise Exception("Failed to load histogram {}.".format(hName))
    # print("getHistogram: {} yield : {}".format(hName, getYield(h)))
    return h


def getHistogramSum(tfile, processes, variable, channel="mutau", tag="", category=""):
    """
    Return a TH1F that is the sum of the histograms of the processes (list of strings) where the histograms are formatted as
    {category}/{processname}_{variable}{tag}.
    """
    # print("getHistogramSum", processes)
    # Initialize by cloning the first
    total = getHistogram(tfile, processes[0], variable, channel, category, tag).Clone()
    for process in processes:
        # Do not double-count the first
        if process == processes[0]:
            continue
        h = getHistogram(tfile, process, variable, channel, category, tag)
        total.Add(h)
    return total
