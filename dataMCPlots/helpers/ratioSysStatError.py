import argparse
import numpy as np
import ROOT


def getRatioSysStatError(
    hNumerator: ROOT.TH1F,
    tDenominator: ROOT.TGraphAsymmErrors,
    setCentralValueToUnity=True,
) -> ROOT.TGraphAsymmErrors:
    """
    For a numerator TH1F representing the data and TGraphAsymmError representing the MC, return a ratio data/MC as a TGraphAsymmErrors.

    If z = x/y,
    (sigma_z) / z = sqrt((sigma_x/x)^2 + (sigma_y/y)^2)

    z is the ratio, x is MC, and y is data

    The input MC TGraphAsymmError is assumed to either have the statistical error only, or the systematic error only.
    """
    # print("hNumerator.GetNBins() + 1: {}".format(hNumerator.GetNbinsX() + 1))

    nBins = tDenominator.GetN()

    # Build TGraphAsymmError by hand
    x = np.zeros(nBins)
    y = np.zeros(nBins)

    xErrLow = np.zeros(nBins)
    yErrLow = np.zeros(nBins)

    xErrHigh = np.zeros(nBins)
    yErrHigh = np.zeros(nBins)

    # # Print the bins in the data
    # for iMC in range(nBins):
    #     print(f"MC with error bars: {tDenominator.GetPointX(iMC)}, {tDenominator.GetPointY(iMC)}, {tDenominator.GetErrorYlow(iMC)}, {tDenominator.GetErrorYhigh(iMC)} ")
    # # Print the bins in the MC
    # for i in range(hNumerator.GetNbinsX()):
    #     print(f"data: {hNumerator.GetBinCenter(i)}, {hNumerator.GetBinContent(i)}")

    # Start at bin 1 to avoid overflow
    for iMC in range(nBins):
        iData = iMC + 1
        # Compute the actual ratio
        if tDenominator.GetPointY(iMC) != 0:
            ratioValue = hNumerator.GetBinContent(iData) / tDenominator.GetPointY(iMC)
        else:
            ratioValue = 1.0

        # Get (x, y) point
        x[iMC] = tDenominator.GetPointX(iMC)
        y[iMC] = ratioValue
        if setCentralValueToUnity:
            y[iMC] = 1.0

        # Copy error in x
        xErrLow[iMC] = tDenominator.GetErrorXlow(iMC)
        xErrHigh[iMC] = tDenominator.GetErrorXhigh(iMC)

        # Get error in the ratio due to the numerator and denominator
        if (tDenominator.GetPointY(iMC) != 0) and (
            hNumerator.GetBinContent(iData) != 0
        ):
            # print(f"ratio value {ratioValue} from {hNumerator.GetBinContent(iData)} divided by {tDenominator.GetPointY(iMC)}, errorYlow/high is {tDenominator.GetErrorYlow(iMC)} {tDenominator.GetErrorYhigh(iMC)}")

            ratioFractionalErrorLow = np.sqrt(
                (tDenominator.GetErrorYlow(iMC) / tDenominator.GetPointY(iMC)) ** 2
                + (hNumerator.GetBinError(iData) / hNumerator.GetBinContent(iData) ** 2)
            )
            yErrLow[iMC] = ratioValue * ratioFractionalErrorLow

            ratioFractionalErrorHigh = np.sqrt(
                (tDenominator.GetErrorYhigh(iMC) / tDenominator.GetPointY(iMC)) ** 2
                + (hNumerator.GetBinError(iData) / hNumerator.GetBinContent(iData) ** 2)
            )
            yErrHigh[iMC] = ratioValue * ratioFractionalErrorHigh

        else:
            yErrLow[iMC] = 0.0
            yErrHigh[iMC] = 0.0

        # print(f">>>> bin: {iMC}, x: {x[iMC]}, y: {y[iMC]}, yErrLow: {yErrLow[iMC]}, yErrHigh: {yErrHigh[iMC]}")

    tRatioTotalError = ROOT.TGraphAsymmErrors(
        nBins, x, y, xErrLow, xErrHigh, yErrLow, yErrHigh
    )

    return tRatioTotalError
