import argparse
import numpy as np
import ROOT


def getRatioStatError(hNumerator, hDenominator, setCentralValueToUnity=True):
    """
    Return the ratio of two TH1Fs as a TH1F where the numerator does NOT have uncertainties, and
    the denominator does.
    e.g. in ratio of observed/expected events.

    If setCentralValueToUnity is False, the resulting TH1F's y-value will be the ratio, otherwise it is 1.0
    (for plotting the stat error as a shaded grey area in the ratio subplot).

    Originally by https://github.com/pallabidas/MetStudiesMiniAOD/blob/master/GammaJets/Plot/staterror.h.
    """

    # Include overflow
    nBins = hNumerator.GetNbinsX() + 1

    # Central ratio as TH1F
    hRatio = hNumerator.Clone("hRatio")
    hRatio.Divide(hDenominator)

    # Calculate ratio errors by bin
    for iBin in range(nBins):
        ratioNominal = hRatio.GetBinContent(iBin)

        # n.b. "Note that in case the user sets after calling SetBinError explicitly, a
        # new bin content (e.g. using SetBinContent), the corresponding bin error needs to be set explicitly
        # (using SetBinError) since the bin error will not be recalculated after setting the content and a default error = 0
        # will be used for those bins."
        if setCentralValueToUnity:
            hRatio.SetBinContent(iBin, 1.0)

        # By error propagation of ratio = numerator/denominator, assuming numerator does not have uncertainties:
        # sigma_ratio = ratio * (sigma_denominator / denominator)
        sigma_denominator = hDenominator.GetBinError(iBin)
        denominator = hDenominator.GetBinContent(iBin)

        if denominator != 0:
            sigma_ratio = ratioNominal * (sigma_denominator / denominator)
            hRatio.SetBinError(iBin, sigma_ratio)
        else:
            sigma_ratio = 0
            hRatio.SetBinError(iBin, sigma_ratio)

    hRatio.Print()
    return hRatio
