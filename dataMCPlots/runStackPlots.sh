# runStackPlots.sh
# Called at the end of runBkgEstimation.sh with the correct input arguments.
# It is unlikely that you will need to call this script by itself, but it is possible in a very long or multi-line command, e.g. like so:
# bash runStackPlots.sh \
#   2016preVFP \
#   /eos/cms/store/group/phys_susy/AN-24-166/skkwan/condorHistogramming/2024-12-16-16h10m-benchmark-2016preVFP-mutau-iteration1-m_vis/out_mutau.root \
#   2024-12-16-16h10m-benchmark-2016preVFP-mutau-iteration1-m_vis \
#   mutau \
#   true

export YEAR=$1  # 2018, 2017, 2016, 2016postVFP, or 2016preVFP (affects the year and lumi printed at the top of the plot)
export INPUT=$2  # path to the out_mutau.root (or out_etau.root, or out_emu.root) produced by the previous step in runBkgEstimation.sh
export TIMESTAMP=$3  # the timestamp of the histogram directory -- only used to create a corresponding timestamped CERNBOX EOS website webpage
export CHANNEL=$4 # mutau, etau, or emu (no parentheses)
export DATACARDMODE=$5 # true if doing control region + signal region plots of the fit variable (m_tt) or false to do all variables

if [[ "${DATACARDMODE}" == true ]]; then
    CARD_MODE_FLAG="--datacard"
fi

rm -r stackPlotsOutputs/*
mkdir -p stackPlotsOutputs/
mkdir -p stackPlotsOutputs/logScale/

COMMAND="python3 stackPlots.py --path=${INPUT} --channel=${CHANNEL} --output=stackPlotsOutputs/logScale --year=${YEAR} --scale=1.0 --doLog ${CARD_MODE_FLAG}"
echo ${COMMAND}
python3 stackPlots.py --path=${INPUT} --channel=${CHANNEL} --output=stackPlotsOutputs/logScale --year=${YEAR} --scale=1.0 --doLog ${CARD_MODE_FLAG}
python3 stackPlots.py --path=${INPUT} --channel=${CHANNEL} --output=stackPlotsOutputs --year=${YEAR} --scale=1.0 ${CARD_MODE_FLAG}

# Use regex to extract the first letter of the username. ^ is the beginning of the string. \w means any alphanumeric character from the Latin alphabet. ( ) captures the variable
re="^(\w)"
# Bash's =~ operator matches the left hand side to the regex on the right hand side
[[ ${USER} =~ ${re} ]]
# The results of the regex can be extracted from the bash variable BASH_REMATCH. Index [0] returns all of the matches.
USER_FIRST_LETTER=${BASH_REMATCH[0]}

WEBSITE="https://${USER}.web.cern.ch/dataMC/${TIMESTAMP}"
FOLDER="/eos/user/${USER_FIRST_LETTER}/${USER}/www/dataMC/${TIMESTAMP}"
LOGSCALE_FOLDER="${FOLDER}/logScale/"

mkdir -p ${FOLDER}
mkdir -p ${LOGSCALE_FOLDER}

echo ">>> runStackPlots.sh: copying outputs to ${FOLDER}, check CERNBOX or ${WEBSITE} for data/MC figures"
cp -r stackPlotsOutputs/* ${FOLDER}
cp -r stackPlotsOutputs/logScale/* ${LOGSCALE_FOLDER}
cp /eos/user/${USER_FIRST_LETTER}/${USER}/www/dataMC/index.php ${FOLDER}
cp /eos/user/${USER_FIRST_LETTER}/${USER}/www/dataMC/index.php ${LOGSCALE_FOLDER}
echo ">>> runStackPlots.sh: Next step: make datacards, copy out_${CHANNEL}.root to the combine area, e.g. with cp ${INPUT} /afs/cern.ch/work/${USER_FIRST_LETTER}/${USER}/public/combineArea/CMSSW_11_3_4/src/auxiliaries/shapes${YEAR}"
