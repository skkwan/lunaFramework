# Implementation of the plotting step of the analysis
#
# The plotting combines the histograms to plots which allow us to study the
# inital dataset based on observables motivated through physics.


import argparse
import ROOT

from config.plotConfig import plotConfigParams
from config.cardConfig import allHistsGroups, allShapeSystematics

from helpers.ratioSysStatError import getRatioSysStatError
from helpers.addSysFromShape import addAllSystematicsForYear
from helpers.getHists import getHistogram, getHistogramSum

ROOT.gROOT.SetBatch(True)


# List which variables to plot, and their x-axis labels
labels = {
    "m_tt": "m_{#tau#tau} / GeV",
    "pt_1": "Leg1 p_{T} / GeV",
    "pt_2": "Leg2 p_{T} / GeV",
    "eta_1": "Leg1 #eta",
    "eta_2": "Leg2 #eta",
    "phi_1": "Leg1 #phi",
    "phi_2": "Leg2 #phi",
    "met": "MET / GeV",
    "metphi": "MET (#phi)",
    "pt_tt": "p_{T} of #tau#tau/ GeV",
    "bpt_deepflavour_1": "Leading b-tag jet p_{T} / GeV",
    "beta_deepflavour_1": "Leading b-tag jet #eta",
    "bphi_deepflavour_1": "Leading b-tag jet #phi",
    "bpt_deepflavour_2": "Sub-leading b-tag jet p_{T} / GeV",
    "beta_deepflavour_2": "Sub-leading b-tag jet #eta",
    "bphi_deepflavour_2": "Sub-leading b-tag jet #phi",
    "D_zeta": "D_{#zeta} / GeV",
    "m_btautau_vis": "m_{b#tau#tau}^{vis} / GeV",
    "pt_btautau_vis": "p_{T, b#tau#tau}^{vis} / GeV",
    "mtMET_1": "m_{T}(L1Latex, MET) / GeV",
    "mtMET_2": "m_{T}(L2Latex, MET) / GeV",
    "metcov00": "MET covariance matrix 00",
    "metcov01": "MET covariance matrix 01",
    "metcov10": "MET covariance matrix 10",
    "metcov11": "MET covariance matrix 11",
    # "mjj": "Di-jet mass / GeV",
    # "jpt_1": "Leading jet p_{T} / GeV",
    # "jpt_2": "Trailing jet p_{T} / GeV",
    # "jeta_1": "Leading jet #eta",
    # "jeta_2": "Trailing jet #eta",
    # "jphi_1": "Leading jet #phi",
    # "jphi_2": "Trailing jet #phi",
}


# Histogram colors for each process
colors = {
    "embed": ROOT.TColor.GetColor("#ffa90e"),
    "ttBar": ROOT.TColor.GetColor("#92dadd"),
    "fake": ROOT.TColor.GetColor("#b9ac70"),
    "qcd": ROOT.TColor.GetColor("#b9ac70"),
    "other": ROOT.TColor.GetColor("#e76300"),
    "DY": ROOT.TColor.GetColor("#3f90da"),
    "signal": ROOT.TColor.GetColor("#e42536"),
}


def isNominalOrControl(category: str) -> bool:
    return (
        (category == "")
        or ("inclusive" in category)
        or ("cr" in category)
        or ("CR" in category)
    )


def setMyROOTGStyle():
    """
    Customize ROOT.gStyles.
    """
    ROOT.gStyle.SetOptStat(0)
    ROOT.gStyle.SetCanvasBorderMode(0)
    ROOT.gStyle.SetCanvasColor(ROOT.kWhite)
    ROOT.gStyle.SetCanvasDefH(600)
    ROOT.gStyle.SetCanvasDefW(600)
    ROOT.gStyle.SetCanvasDefX(0)
    ROOT.gStyle.SetCanvasDefY(0)

    ROOT.gStyle.SetPadTopMargin(0.08)
    ROOT.gStyle.SetPadBottomMargin(0.02)
    ROOT.gStyle.SetPadLeftMargin(0.16)
    ROOT.gStyle.SetPadRightMargin(0.05)

    ROOT.gStyle.SetHistLineColor(1)
    ROOT.gStyle.SetHistLineStyle(1)  # solid line
    ROOT.gStyle.SetHistLineWidth(1)
    ROOT.gStyle.SetEndErrorSize(2)
    ROOT.gStyle.SetMarkerStyle(20)

    ROOT.gStyle.SetOptTitle(0)
    ROOT.gStyle.SetTitleFont(42)
    ROOT.gStyle.SetTitleColor(1)
    ROOT.gStyle.SetTitleTextColor(1)
    ROOT.gStyle.SetTitleFillColor(10)
    ROOT.gStyle.SetTitleFontSize(0.05)

    ROOT.gStyle.SetTitleColor(1, "XYZ")
    ROOT.gStyle.SetTitleFont(42, "XYZ")
    ROOT.gStyle.SetTitleSize(0.05, "XYZ")
    ROOT.gStyle.SetTitleXOffset(1.00)
    ROOT.gStyle.SetTitleYOffset(0.90)

    ROOT.gStyle.SetLabelColor(1, "XYZ")
    ROOT.gStyle.SetLabelFont(42, "XYZ")
    ROOT.gStyle.SetLabelOffset(0.007, "XYZ")
    ROOT.gStyle.SetLabelSize(0.04, "XYZ")

    ROOT.gStyle.SetAxisColor(1, "XYZ")
    ROOT.gStyle.SetStripDecimals(True)
    ROOT.gStyle.SetTickLength(0.03, "XYZ")
    ROOT.gStyle.SetNdivisions(510, "XYZ")
    ROOT.gStyle.SetPadTickX(1)
    ROOT.gStyle.SetPadTickY(1)

    ROOT.gStyle.SetPaperSize(20.0, 20.0)
    ROOT.gStyle.SetHatchesLineWidth(5)
    ROOT.gStyle.SetHatchesSpacing(0.05)

    ROOT.TGaxis.SetExponentOffset(-0.08, 0.01, "Y")


def createSimpleRatioAndUnitLine(h1, h2):
    """
    Create a ratio of two histograms (h1/h2): assuming Sumw2() was already run on the h1, h2 and return h3
    """

    h3 = h1.Clone("h3")
    h3.Divide(h2)

    # h3.SetStats(0)

    # Find the unit line
    xmin = h3.GetXaxis().GetXmin()
    xmax = h3.GetXaxis().GetXmax()
    unitLine = ROOT.TLine(xmin, 1.0, xmax, 1.0)
    unitLine.SetLineColor(ROOT.kBlack)
    unitLine.SetLineWidth(1)

    # Y-axis range is set elsewhere: band.GetYaxis().SetRangeUser(-0.5, 1.5)
    return h3, unitLine


def createCanvasPads(doLog=False):
    """
    Create and return canvas and pads for a stack plot + ratio plot underneath.
    If doLog is true, set log y-axis in main plot
    """
    c = ROOT.TCanvas("", "", 1000, 1000)
    if doLog:
        c = ROOT.TCanvas("", "", 1000, 1000)
    pad1 = ROOT.TPad("pad1", "The pad with the stack histogram", 0, 0.3, 1, 1)
    pad1.SetBottomMargin(
        0.05
    )  # otherwise the stack plot x-axis tick labels will be cut off
    pad2 = ROOT.TPad("pad2", "Pad with ratio plot", 0, 0, 1, 0.3)
    if doLog:
        pad1.SetLogy(True)
    pad1.Draw()
    pad2.Draw()
    return c, pad1, pad2


def writeYieldTxt(
    output,
    channel,
    category,
    variable,
    data,
    embed,
    other,
    ttBar,
    DY,
    fakeHisto,
    fakeName,
    signal,
    signal_scale,
    masspoint1,
    masspoint2=0.0,
):
    """
    Write little .txt file with yields, for bookkeeping
    """
    # Prepare the output log file
    if variable == "m_tt":
        if masspoint2 == 0.0:
            f = open(
                "{}/{}_{}_{}_mass_{}_yields_with_overflow.txt".format(
                    output, variable, channel, category, masspoint1
                ),
                "w",
            )
        else:  # asymmetric cascade
            f = open(
                "{}/{}_{}_{}_cascade_mass_{}_{}_yields_with_overflow.txt".format(
                    output, variable, channel, category, masspoint1, masspoint2
                ),
                "w",
            )

        # Print yields to output log file
        # print(">>> ", channel, category, variable)
        f.write("%s\n%s" % (category, variable))
        for x, l in [
            (data, "data"),
            (embed, "embed"),
            (other, "other"),
            (ttBar, "ttBar"),
            (DY, "DY"),
            (fakeHisto, fakeName),
            (signal, "signal"),
        ]:
            # print(
            #     "     %s \t %s \t %0.4f" % (variable, l, x.Integral(0, x.GetNbinsX() + 1))
            # )
            if l != "signal":
                f.write("   %s \t %0.4f \n" % (l, x.Integral(0, x.GetNbinsX() + 1)))
            else:
                f.write(
                    "   %s \t %0.4f, signal scaled by %f\n"
                    % (l, x.Integral(0, x.GetNbinsX() + 1), signal_scale)
                )
        # print("\n")
        f.write("\n")
        f.close()


def makePlots(
    signalType,
    isBlinded,
    output,
    year,
    channel,
    category,
    variable,
    doLog,
    lumiscale,
    data,
    embed,
    other,
    ttBar,
    DY,
    fakeHisto,
    fakeName,
    totalMC,
    sysError,
    signal,
    signal_scale,
    masspoint1,
    masspoint2=0.0,
):
    """
    Make plots for a certain mass point.
        isBlinded: if true, do not draw data if the category name contains the string "SR".
    """
    # Set marker style, line color, etc. for main stack plot
    data.SetMarkerStyle(20)
    data.SetLineColor(ROOT.kBlack)
    for x, l in [
        (embed, "embed"),
        (other, "other"),
        (ttBar, "ttBar"),
        (DY, "DY"),
        (fakeHisto, fakeName),
        (signal, "signal"),
    ]:
        x.SetLineWidth(1)
        x.SetLineColor(1)
        x.SetFillColor(colors[l])
        if l == "signal":
            x.SetLineWidth(3)
            x.SetFillColor(0)
            x.SetLineColor(colors[l])

    # Create the main stack and draw it and the data
    stack = ROOT.THStack("", "")
    for x in [fakeHisto, DY, embed, ttBar, other]:
        stack.Add(x)

    c, pad1, pad2 = createCanvasPads(doLog)
    pad1.cd()
    stack.Draw("hist")
    name = data.GetTitle()
    if name in labels:
        title = labels[name]
        # Change the title based on the channel
        if channel == "mutau":
            title = title.replace("Leg1", "Muon")
            title = title.replace("Leg2", "Tau")
            title = title.replace("L1Latex", "#mu")
            title = title.replace("L2Latex", "#tau_{h}")
        elif channel == "etau":
            title = title.replace("Leg1", "Electron")
            title = title.replace("Leg2", "Tau")
            title = title.replace("L1Latex", "e")
            title = title.replace("L2Latex", "#tau_{h}")
        elif channel == "emu":
            title = title.replace("Leg1", "Electron")
            title = title.replace("Leg2", "Muon")
            title = title.replace("L1Latex", "e")
            title = title.replace("L2Latex", "#mu")
    else:
        title = name
    # don't need x-axis title if we have two canvases
    # stack.GetXaxis().SetTitle(title)
    stack.GetYaxis().SetTitleSize(0.06)  # size of the axis title
    stack.GetYaxis().SetLabelSize(0.05)  # size of the tick labels
    stack.GetYaxis().SetTitle("N_{Events}")
    stack.SetMaximum(max(stack.GetMaximum(), data.GetMaximum()) * 1.4)
    if doLog:
        stack.SetMaximum(max(stack.GetMaximum(), data.GetMaximum()) * 10)
    stack.SetMinimum(1.0)

    if isNominalOrControl(category):
        data.Draw("E0 SAME")
    if isBlinded:
        if "SR" not in category:
            data.Draw("E0 SAME")
    signal.Draw("HIST SAME")

    # Get total MC and plot the stat error as error bars
    totalMC.SetFillColor(1)
    totalMC.SetFillColorAlpha(ROOT.kBlack, 0.8)
    totalMC.SetFillStyle(3354)
    ROOT.gStyle.SetHatchesSpacing(0.4)
    ROOT.gStyle.SetHatchesLineWidth(1)
    totalMC.Draw("E2 SAME")

    # Systematic errors as shaded
    sysError.SetFillColorAlpha(ROOT.kBlack, 0.8)
    sysError.SetFillStyle(3354)
    sysError.SetLineColor(ROOT.kBlack)
    ROOT.gStyle.SetHatchesSpacing(0.4)
    ROOT.gStyle.SetHatchesLineWidth(1)
    sysError.Draw("E2 SAME")

    # Legend in top plot
    legend = ROOT.TLegend(0.38, 0.73, 0.90, 0.88)
    legend.SetTextSize(0.035)
    legend.SetNColumns(2)
    legend.AddEntry(embed, "Embedded", "f")
    legend.AddEntry(DY, "DY#rightarrow ll", "f")
    legend.AddEntry(ttBar, "t#bar{t}", "f")
    if (channel == "mutau") or (channel == "etau"):
        legend.AddEntry(fakeHisto, "Fakes", "f")
    else:
        legend.AddEntry(fakeHisto, "QCD", "f")
    legend.AddEntry(other, "Other", "f")
    # legend.AddEntry(signal, "Signal (x{:.2f})".format(scale_signal), "l")
    if masspoint2 == 0.0:  # symmetric
        legend.AddEntry(
            signal,
            "Signal m_a = {} GeV (BR = {})".format(masspoint1, signal_scale),
            "l",
        )
    else:
        legend.AddEntry(
            signal,
            "Signal ({}, {}) GeV (x{})".format(masspoint1, masspoint2, signal_scale),
            "l",
        )
        # Add text underneath signal sample legend to clarify that BR is assumed to be 100%, in addition to scaling it by x100
        legend.AddEntry(0, "", "")
        legend.AddEntry(0, "(BR = 100%)", "")
    legend.AddEntry(data, "Data", "lep")
    legend.AddEntry(sysError, "Stat. + Shape unc.", "f")
    legend.SetBorderSize(0)
    legend.Draw()

    # Ratio plot
    pad2.cd()
    pad2.SetTopMargin(0.05)
    pad2.SetBottomMargin(0.3)
    pad2.SetFillStyle(0)

    # h3 is the TH1D representing ratio central value (only the black dot)
    # unitLine is the line at ratio = 1.0
    h3, unitLine = createSimpleRatioAndUnitLine(data, totalMC)
    h3.GetXaxis().SetTitle(title)
    h3.GetYaxis().SetTitle("Data/Pred.")
    h3.GetXaxis().SetTitleSize(0.12)
    h3.GetYaxis().SetTitleSize(0.12)
    h3.GetXaxis().SetLabelSize(0.1)
    h3.GetYaxis().SetLabelSize(0.08)
    h3.SetNdivisions(210, "Y")
    h3.GetYaxis().SetTitleOffset(0.4)
    h3.GetYaxis().SetRangeUser(0.0, 2.0)
    if isBlinded and "SR" in category:
        h3.SetMarkerColorAlpha(0, 0.0)
        h3.SetLineColorAlpha(0, 0.0)

    # hRatioSys is the TGraphAsymmError representing the MC statistical and MC systematic error, added in quadrature
    # Plot it as a shaded area centered around 1.0 in the ratio plot
    hRatioSys = getRatioSysStatError(data, sysError)
    hRatioSys.SetMarkerSize(0.0)
    hRatioSys.SetFillColor(14)
    hRatioSys.SetFillStyle(3354)
    ROOT.gStyle.SetHatchesSpacing(0.4)
    ROOT.gStyle.SetHatchesLineWidth(1)

    # For some reason, ROOT forgets all these settings
    pad2.cd()
    ROOT.gStyle.SetAxisColor(1, "XYZ")
    ROOT.gStyle.SetStripDecimals(True)
    ROOT.gStyle.SetTickLength(0.075, "XYZ")
    ROOT.gStyle.SetNdivisions(510, "XYZ")
    ROOT.gStyle.SetPadTickX(1)
    ROOT.gStyle.SetPadTickY(1)

    # if isBlinded and ("SR" in category):
    #     h3.SetMarkerColorAlpha(0, 0)
    #     h3.Draw("HIST P")
    # else:
    h3.Draw("E")
    hRatioSys.Draw("E2 SAME")
    unitLine.Draw("SAME")
    pad2.RedrawAxis()

    # Add title
    latex = ROOT.TLatex()
    latex.SetNDC()
    latex.SetTextSize(0.04)
    latex.SetTextFont(42)
    lumi = plotConfigParams[year]["lumi"]
    sqrts = plotConfigParams[year]["energy"]
    yearToDisplay = plotConfigParams[year]["yearToDisplay"]
    pad1.cd()

    if "VFP" in yearToDisplay:
        lumiYearTextPositionXaxis = 0.60
    else:
        lumiYearTextPositionXaxis = 0.69

    latex.DrawLatex(
        lumiYearTextPositionXaxis,
        0.935,
        "{0:.1f} fb^{{-1}} ({1}, {2:.0f} TeV)".format(
            lumi * lumiscale, yearToDisplay, sqrts
        ),
    )
    latex.DrawLatex(0.16, 0.935, "#scale[1.3]{#bf{CMS}} #it{Work in progress}")
    # latex.DrawLatex(0.16, 0.935, "Private work (CMS data/simulation)")

    # Save
    if masspoint2 == 0.0:  # symmetric
        outNameStem = "{}/{}_{}_{}_signal_mass_{}".format(
            output, channel, category, variable, masspoint1
        )
    else:
        outNameStem = "{}/{}_{}_{}_signal_{}_mass_{}_{}".format(
            output, channel, category, variable, signalType, masspoint1, masspoint2
        )

    c.SaveAs(outNameStem + ".pdf")
    c.SaveAs(outNameStem + ".png")


def main(
    path, output, year, variable, lumiscale, channel="mutau", category="", doLog=False
):
    """
    Create stack plots with ratio underneath
    doLog : also output the plots as log scale on y-axis
    """
    tfile = ROOT.TFile(path, "READ")

    setMyROOTGStyle()

    # Embedded
    embed = getHistogram(tfile, "embedded", variable, channel, category)

    # mutau or etau: Jet faking tauh, emu: qcd
    if (channel == "mutau") or (channel == "etau"):
        fakeName = "fake"
    else:
        fakeName = "qcd"
    fakeHisto = getHistogram(tfile, fakeName, variable, channel, category)
    # Data
    data = getHistogram(tfile, "data_obs", variable, channel, category)

    # MC background
    DY = getHistogram(tfile, "ZJ", variable, channel, category)
    ttBar = getHistogram(tfile, "ttbar", variable, channel, category)
    otherlist = [
        "ST",
        "VV",
        "ggh_htt",
        "ggh_hww",
        "qqh_htt",
        "qqh_hww",
        "Zh_htt",
        "Zh_hww",
        "Wh_htt",
        "Wh_hww",
        "tth",
    ]
    if channel == "emu":
        otherlist.append("WJ")
    other = getHistogramSum(tfile, otherlist, variable, channel, "", category)

    # Get total MC and plot the stat error as error bars
    totalMC = fakeHisto.Clone()
    for x in [DY, embed, ttBar, other]:
        totalMC.Add(x)

    # Systematic errors
    # The initial histogram is going to depend on which processes we have in it, for instance for JER we are only
    # comparing against MC and fake, not Embedded
    sysError = ROOT.TGraphAsymmErrors(totalMC.Clone())

    shapeSystematics = allShapeSystematics[year][channel]
    histGroups = allHistsGroups[year][channel]

    addAllSystematicsForYear(
        tfile,
        sysError,
        shapeSystematics,
        histGroups,
        variable,
        channel,
        category,
    )

    # Asymmetric cascade
    for m1, m2 in [[100, 15]]:
        # [[30, 15] , [40, 15], [50, 15], [60, 15], [70, 15], [80, 15], [90, 15], [100, 15], [110, 15],
        #            [40, 20], [50, 20], [60, 20], [70, 20], [80, 20], [90, 20], [100, 20],
        #            [60, 30], [70, 30], [80, 30], [90, 30]]:

        # Signal
        signal_gghbbtt = getHistogram(
            tfile, "ggh4b2t-{}-{}".format(m1, m2), variable, channel, category
        )
        signal_vbfbbtt = getHistogram(
            tfile, "vbf4b2t-{}-{}".format(m1, m2), variable, channel, category
        )
        signal = signal_gghbbtt + signal_vbfbbtt

        # Scale any histograms that need to be scaled: # assuming 100\% h->a1a2->4b2tau, and then multiplying by x100
        asymm_scale_signal = 10
        signal = signal * asymm_scale_signal

        isBlinded = True
        writeYieldTxt(
            output,
            channel,
            category,
            variable,
            data,
            embed,
            other,
            ttBar,
            DY,
            fakeHisto,
            fakeName,
            signal,
            asymm_scale_signal,
            m1,
            m2,
        )

        makePlots(
            "4b2t",
            isBlinded,
            output,
            year,
            channel,
            category,
            variable,
            doLog,
            lumiscale,
            data,
            embed,
            other,
            ttBar,
            DY,
            fakeHisto,
            fakeName,
            totalMC,
            sysError,
            signal,
            asymm_scale_signal,
            m1,
            m2,
        )

    # Asymmetric non-cascade
    for m1, m2 in [[30, 15]]:
        #  [[20,15 ], [30, 15],
        #                 [30, 20], [40, 20],
        #                 [40, 30], [50, 30], [60, 30],
        #                 [50, 40], [60, 40], [70, 40], [80, 40],
        #                 [60, 50], [70, 50]]:
        # Signal
        signal_gghbbtt = getHistogram(
            tfile, "ggh2b2t-{}-{}".format(m1, m2), variable, channel, category
        )
        signal_vbfbbtt = getHistogram(
            tfile, "vbf2b2t-{}-{}".format(m1, m2), variable, channel, category
        )
        signal = signal_gghbbtt + signal_vbfbbtt

        # Scale any histograms that need to be scaled: # assuming 100\% h->a1a2->4b2tau, and then multiplying by x100
        asymm_scale_signal = 1
        signal = signal * asymm_scale_signal

        isBlinded = True
        writeYieldTxt(
            output,
            channel,
            category,
            variable,
            data,
            embed,
            other,
            ttBar,
            DY,
            fakeHisto,
            fakeName,
            signal,
            asymm_scale_signal,
            m1,
            m2,
        )

        makePlots(
            "2b2t",
            isBlinded,
            output,
            year,
            channel,
            category,
            variable,
            doLog,
            lumiscale,
            data,
            embed,
            other,
            ttBar,
            DY,
            fakeHisto,
            fakeName,
            totalMC,
            sysError,
            signal,
            asymm_scale_signal,
            m1,
            m2,
        )


# Loop over all variable names and make a plot for each
if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument(
        "--path", type=str, help="Full path to ROOT file with all histograms"
    )
    parser.add_argument("--channel", type=str, help="Channel (mutau), (etau), (emu)")
    parser.add_argument(
        "--output", type=str, help="Output directory for plots and log files"
    )
    parser.add_argument("--year", type=str, help="Year")
    parser.add_argument(
        "--scale", type=float, help="Scaling of the integrated luminosity"
    )
    parser.add_argument(
        "--doLog",
        action=argparse.BooleanOptionalAction,
        help="Make plots with log scale",
    )
    parser.add_argument(
        "--datacard",
        help="Do datacard variable and categories, i.e. does input ROOT file only have the final variable to fit?",
        action="store_true",
    )
    args = parser.parse_args()

    print(f">>>> args.datacard: {args.datacard}")
    # Default behaviour
    categories = ["inclusive"]
    if args.datacard:
        categories = [
            # TODO: replace with BDT categories
            "inclusive",
            "lowMassSR",
            "mediumMassSR",
            "highMassSR",
            "highMassCR",
        ]
    # Default behaviour
    variables = labels.keys()
    if args.datacard:
        variables = {"m_tt": labels["m_tt"]}

    year = args.year
    for variable in variables:
        for category in categories:
            main(
                args.path,
                args.output,
                year,
                variable,
                args.scale,
                args.channel,
                category,
                args.doLog,
            )
