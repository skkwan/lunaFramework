# Implementation of the plotting step of the analysis
#
# The plotting combines the histograms to plots which allow us to study the
# inital dataset based on observables motivated through physics.


import argparse
import ROOT

from config.plotConfig import lumiParams

# from config.cardConfig import allHistsGroups, allShapeSystematics

from helpers.ratioError import getRatioWithErrors
from helpers.ratioStatError import getRatioStatError
from helpers.postfitRatioError import postfitRatioError
from helpers.getHists import getHistogram, getHistogramSum

ROOT.gROOT.SetBatch(True)


# List which variables to plot, and their x-axis labels
labels = {
    "m_tt": "m_{#tau#tau} / GeV",
}


# Histogram colors for each process
colors = {
    "ggH": ROOT.TColor.GetColor("#BF2229"),
    "qqH": ROOT.TColor.GetColor("#00A88F"),
    "ttBar": ROOT.TColor.GetColor(155, 152, 204),
    "other": ROOT.TColor.GetColor(64, 224, 208),
    "W": ROOT.TColor.GetColor(222, 90, 106),
    "qcd": ROOT.TColor.GetColor(250, 202, 255),
    "fake": ROOT.TColor.GetColor(250, 202, 255),
    "DY": ROOT.TColor.GetColor(100, 192, 232),
    "ZLL": ROOT.TColor.GetColor(100, 192, 232),
    "embed": ROOT.TColor.GetColor(248, 206, 104),
    "signal": ROOT.TColor.GetColor(255, 40, 0),
}


def isNominalOrControl(category: str) -> bool:
    return (
        (category == "")
        or ("inclusive" in category)
        or ("cr" in category)
        or ("CR" in category)
    )


def setMyROOTGStyle():
    """
    Customize ROOT.gStyles.
    """
    ROOT.gStyle.SetOptStat(0)
    ROOT.gStyle.SetCanvasBorderMode(0)
    ROOT.gStyle.SetCanvasColor(ROOT.kWhite)
    ROOT.gStyle.SetCanvasDefH(600)
    ROOT.gStyle.SetCanvasDefW(600)
    ROOT.gStyle.SetCanvasDefX(0)
    ROOT.gStyle.SetCanvasDefY(0)

    ROOT.gStyle.SetPadTopMargin(0.08)
    ROOT.gStyle.SetPadBottomMargin(0.02)
    ROOT.gStyle.SetPadLeftMargin(0.16)
    ROOT.gStyle.SetPadRightMargin(0.05)

    ROOT.gStyle.SetHistLineColor(1)
    ROOT.gStyle.SetHistLineStyle(1)  # solid line
    ROOT.gStyle.SetHistLineWidth(1)
    ROOT.gStyle.SetEndErrorSize(2)
    ROOT.gStyle.SetMarkerStyle(20)

    ROOT.gStyle.SetOptTitle(0)
    ROOT.gStyle.SetTitleFont(42)
    ROOT.gStyle.SetTitleColor(1)
    ROOT.gStyle.SetTitleTextColor(1)
    ROOT.gStyle.SetTitleFillColor(10)
    ROOT.gStyle.SetTitleFontSize(0.05)

    ROOT.gStyle.SetTitleColor(1, "XYZ")
    ROOT.gStyle.SetTitleFont(42, "XYZ")
    ROOT.gStyle.SetTitleSize(0.05, "XYZ")
    ROOT.gStyle.SetTitleXOffset(1.00)
    ROOT.gStyle.SetTitleYOffset(0.90)

    ROOT.gStyle.SetLabelColor(1, "XYZ")
    ROOT.gStyle.SetLabelFont(42, "XYZ")
    ROOT.gStyle.SetLabelOffset(0.007, "XYZ")
    ROOT.gStyle.SetLabelSize(0.04, "XYZ")

    ROOT.gStyle.SetAxisColor(1, "XYZ")
    ROOT.gStyle.SetStripDecimals(True)
    ROOT.gStyle.SetTickLength(0.03, "XYZ")
    ROOT.gStyle.SetNdivisions(510, "XYZ")
    ROOT.gStyle.SetPadTickX(1)
    ROOT.gStyle.SetPadTickY(1)

    ROOT.gStyle.SetPaperSize(20.0, 20.0)
    ROOT.gStyle.SetHatchesLineWidth(5)
    ROOT.gStyle.SetHatchesSpacing(0.05)

    ROOT.TGaxis.SetExponentOffset(-0.08, 0.01, "Y")


def createSimpleRatioAndUnitLine(h1, h2):
    """
    Create a ratio of two histograms (h1/h2): assuming Sumw2() was already run on the h1, h2 and return h3
    """

    h3 = h1.Clone("h3")
    h3.Divide(h2)

    # h3.SetStats(0)

    # Find the unit line
    xmin = h3.GetXaxis().GetXmin()
    xmax = h3.GetXaxis().GetXmax()
    unitLine = ROOT.TLine(xmin, 1.0, xmax, 1.0)
    unitLine.SetLineColor(ROOT.kBlack)
    unitLine.SetLineWidth(1)

    # Y-axis range is set elsewhere: band.GetYaxis().SetRangeUser(-0.5, 1.5)
    return h3, unitLine


def createCanvasPads(doLog=False):
    """
    Create and return canvas and pads for a stack plot + ratio plot underneath.
    If doLog is true, set log y-axis in main plot
    """
    c = ROOT.TCanvas("", "", 1000, 1000)
    if doLog:
        c = ROOT.TCanvas("", "", 1000, 1000)
    pad1 = ROOT.TPad("pad1", "The pad with the stack histogram", 0, 0.3, 1, 1)
    pad1.SetBottomMargin(
        0.05
    )  # otherwise the stack plot x-axis tick labels will be cut off
    pad2 = ROOT.TPad("pad2", "Pad with ratio plot", 0, 0, 1, 0.3)
    if doLog:
        pad1.SetLogy(True)
    pad1.Draw()
    pad2.Draw()
    return c, pad1, pad2


def writeYieldTxt(
    output,
    channel,
    category,
    variable,
    data,
    embed,
    other,
    ttBar,
    DY,
    fakeHisto,
    fakeName,
    signal,
    signal_scale,
    masspoint1,
    masspoint2=0.0,
):
    """
    Write little .txt file with yields, for bookkeeping
    """
    # Prepare the output log file
    if variable == "m_vis":
        variable = ""
        if masspoint2 == 0.0:
            f = open(
                "{}/{}_{}_{}_mass_{}_yields_with_overflow.txt".format(
                    output, variable, channel, category, masspoint1
                ),
                "w",
            )
        else:  # asymmetric cascade
            f = open(
                "{}/{}_{}_{}_cascade_mass_{}_{}_yields_with_overflow.txt".format(
                    output, variable, channel, category, masspoint1, masspoint2
                ),
                "w",
            )

        # Print yields to output log file
        # print(">>> ", channel, category, variable)
        f.write("%s\n%s" % (category, variable))
        for x, l in [
            (data, "data"),
            (embed, "embed"),
            (other, "other"),
            (ttBar, "ttBar"),
            (DY, "DY"),
            (fakeHisto, fakeName),
            (signal, "signal"),
        ]:
            # print(
            #     "     %s \t %s \t %0.4f" % (variable, l, x.Integral(0, x.GetNbinsX() + 1))
            # )
            if l != "signal":
                f.write("   %s \t %0.4f \n" % (l, x.Integral(0, x.GetNbinsX() + 1)))
            else:
                f.write(
                    "   %s \t %0.4f, signal scaled by %f\n"
                    % (l, x.Integral(0, x.GetNbinsX() + 1), signal_scale)
                )
        # print("\n")
        f.write("\n")
        f.close()


def makePlots(
    isBlinded,
    output,
    year,
    channel,
    category,
    variable,
    doLog,
    lumiscale,
    data,
    embed,
    other,
    ttBar,
    DY,
    fakeHisto,
    fakeName,
    totalBkg,
    signal,
    signal_scale,
    masspoint1,
    masspoint2=0.0,
):
    """
    Make plots for a certain mass point.
        isBlinded: if true, do not draw data if the category name contains the string "SR".
    """

    if variable == "m_vis":
        variable = ""
    # Set marker style, line color, etc. for main stack plot
    data.SetMarkerStyle(20)
    data.SetLineColor(ROOT.kBlack)
    for x, l in [
        (embed, "embed"),
        (other, "other"),
        (ttBar, "ttBar"),
        (DY, "DY"),
        (fakeHisto, fakeName),
        (signal, "signal"),
    ]:
        x.SetLineWidth(1)
        x.SetLineColor(1)
        x.SetFillColor(colors[l])
        if l == "signal":
            x.SetLineWidth(3)
            x.SetFillColor(0)
            x.SetLineColor(colors[l])

    # Create the main stack and draw it and the data
    stack = ROOT.THStack("", "")
    for x in [fakeHisto, DY, ttBar, other, embed]:
        stack.Add(x)

    c, pad1, pad2 = createCanvasPads(doLog)
    pad1.cd()
    stack.Draw("hist")
    name = ""
    if name in labels:
        title = labels[name]
        # Change the title based on the channel
        if channel == "mutau":
            title = title.replace("Leg1", "Muon")
            title = title.replace("Leg2", "Tau")
        elif channel == "etau":
            title = title.replace("Leg1", "Electron")
            title = title.replace("Leg2", "Tau")
        elif channel == "emu":
            title = title.replace("Leg1", "Electron")
            title = title.replace("Leg2", "Muon")
    else:
        title = name
    # don't need x-axis title if we have two canvases
    # stack.GetXaxis().SetTitle(title)
    stack.GetYaxis().SetTitleSize(0.06)  # size of the axis title
    stack.GetYaxis().SetLabelSize(0.05)  # size of the tick labels
    stack.GetYaxis().SetTitle("N_{Events}")
    stack.SetMaximum(max(stack.GetMaximum(), data.GetMaximum()) * 1.4)
    if doLog:
        stack.SetMaximum(max(stack.GetMaximum(), data.GetMaximum()) * 10)
    stack.SetMinimum(1.0)

    if not isBlinded:
        data.Draw("E0 SAME")
    signal.Draw("HIST SAME")

    # Get total MC and plot the stat error as error bars
    totalBkg.SetFillColor(1)
    totalBkg.SetFillColorAlpha(ROOT.kBlack, 0.8)
    totalBkg.SetFillStyle(3354)
    ROOT.gStyle.SetHatchesSpacing(0.4)
    ROOT.gStyle.SetHatchesLineWidth(1)
    totalBkg.Draw("E2 SAME")

    # Legend in top plot
    legend = ROOT.TLegend(0.38, 0.73, 0.90, 0.88)
    legend.SetTextSize(0.035)
    legend.SetNColumns(2)
    legend.AddEntry(embed, "Embedded", "f")
    legend.AddEntry(DY, "DY#rightarrow ll", "f")
    legend.AddEntry(ttBar, "t#bar{t}", "f")
    if (channel == "mutau") or (channel == "etau"):
        legend.AddEntry(fakeHisto, "Fakes", "f")
    else:
        legend.AddEntry(fakeHisto, "QCD", "f")
    legend.AddEntry(other, "Other", "f")
    # legend.AddEntry(signal, "Signal (x{:.2f})".format(scale_signal), "l")
    if masspoint2 == 0.0:  # symmetric
        legend.AddEntry(
            signal,
            "Signal m_a = {} GeV (BR = {})".format(masspoint1, signal_scale),
            "l",
        )
    else:
        legend.AddEntry(
            signal,
            "Signal ({}, {}) GeV (x{})".format(masspoint1, masspoint2, signal_scale),
            "l",
        )
        # Add text underneath signal sample legend to clarify that BR is assumed to be 100%, in addition to scaling it by x100
        legend.AddEntry(0, "", "")
        legend.AddEntry(0, "(BR = 100%)", "")
    # legend.AddEntry(sysError, "Uncertainty", "f")
    legend.AddEntry(data, "Data", "lep")
    legend.SetBorderSize(0)
    legend.Draw()

    # Ratio plot
    pad2.cd()
    pad2.SetTopMargin(0.05)
    pad2.SetBottomMargin(0.3)
    pad2.SetFillStyle(0)

    # h3 is the TH1D representing ratio central value (only the black dot)
    # unitLine is the line at ratio = 1.0
    h3, unitLine = createSimpleRatioAndUnitLine(data, totalBkg)
    h3.GetXaxis().SetTitle(title)
    h3.GetYaxis().SetTitle("Data/Pred.")
    h3.GetXaxis().SetTitleSize(0.12)
    h3.GetYaxis().SetTitleSize(0.12)
    h3.GetXaxis().SetLabelSize(0.1)
    h3.GetYaxis().SetLabelSize(0.08)
    h3.SetNdivisions(210, "Y")
    h3.GetYaxis().SetTitleOffset(0.4)
    h3.GetYaxis().SetRangeUser(0.0, 2.0)

    # hRatioSys is the TGraphAsymmError representing the MC statistical and MC systematic error, added in quadrature
    # Plot it as a shaded area centered around 1.0 in the ratio plot
    hRatioSys = postfitRatioError(data, totalBkg)
    hRatioSys.SetMarkerSize(0.0)
    hRatioSys.SetFillColor(14)
    hRatioSys.SetFillStyle(3354)
    ROOT.gStyle.SetHatchesSpacing(0.4)
    ROOT.gStyle.SetHatchesLineWidth(1)

    # For some reason, ROOT forgets all these settings
    pad2.cd()
    ROOT.gStyle.SetAxisColor(1, "XYZ")
    ROOT.gStyle.SetStripDecimals(True)
    ROOT.gStyle.SetTickLength(0.075, "XYZ")
    ROOT.gStyle.SetNdivisions(510, "XYZ")
    ROOT.gStyle.SetPadTickX(1)
    ROOT.gStyle.SetPadTickY(1)

    if isBlinded:
        h3.SetMarkerColorAlpha(0, 0)
        h3.Draw("HIST P")
    else:
        h3.Draw("E")
    hRatioSys.Draw("E2 SAME")
    unitLine.Draw("SAME")
    pad2.RedrawAxis()

    # Add title
    latex = ROOT.TLatex()
    latex.SetNDC()
    latex.SetTextSize(0.04)
    latex.SetTextFont(42)
    lumi = lumiParams[year]["lumi"]
    sqrts = lumiParams[year]["energy"]
    pad1.cd()

    # Add hRatioSys to main legend

    legend.AddEntry(hRatioSys, "Uncertainties", "f")
    legend.Draw()

    latex.DrawLatex(
        0.69,
        0.935,
        "{0:.1f} fb^{{-1}} ({1}, {2:.0f} TeV)".format(lumi * lumiscale, year, sqrts),
    )
    latex.DrawLatex(0.16, 0.935, "#scale[1.3]{#bf{CMS}} #it{Work in progress}")
    # latex.DrawLatex(0.16, 0.935, "Private work (CMS data/simulation)")

    # Save
    if masspoint2 == 0.0:  # symmetric
        outNameStem = "{}/{}_{}_{}_signal_mass_{}".format(
            output, channel, category, variable, masspoint1
        )
    else:
        outNameStem = "{}/{}_{}_{}_signal_cascade_mass_{}_{}".format(
            output, channel, category, variable, masspoint1, masspoint2
        )

    c.SaveAs(outNameStem + ".pdf")
    c.SaveAs(outNameStem + ".png")


def main(
    path,
    output,
    year,
    variable,
    lumiscale,
    channel="mutau",
    category="",
    doLog=False,
    m1=30,
    m2=15,
):
    """
    Create stack plots with ratio underneath
    doLog : also output the plots as log scale on y-axis
    """
    tfile = ROOT.TFile(path, "READ")

    if variable == "m_vis":
        variable = ""

    setMyROOTGStyle()

    print(path, output, year, variable, lumiscale, channel, category, doLog, m1, m2)

    # Embedded
    embed = getHistogram(tfile, "embedded", variable, channel, category)

    # mutau or etau: Jet faking tauh, emu: qcd
    if (channel == "mutau") or (channel == "etau"):
        fakeName = "fake"
    elif channel == "emu":
        fakeName = "qcd"
    fakeHisto = getHistogram(tfile, fakeName, variable, channel, category)
    # Data
    data = getHistogram(tfile, "data_obs", variable, channel, category)

    # MC background
    DY = getHistogram(tfile, "ZJ", variable, channel, category)
    ttBar = getHistogram(tfile, "ttbar", variable, channel, category)
    otherlist = [
        "ST",
        "VV",
        "ggh_htt",
        "ggh_hww",
        "qqh_htt",
        "qqh_hww",
        "Zh_htt",
        "Zh_hww",
        "Wh_htt",
        "Wh_hww",
        "tth",
    ]
    if channel == "emu":
        otherlist.append("WJ")

    other = getHistogramSum(tfile, otherlist, variable, channel, "", category)

    # Get total background
    totalBkg = getHistogram(tfile, "TotalBkg", variable, channel, category)

    # Asymmetric cascade

    # Signal: can get by production mode, or just get the total
    # signal_gghbbtt = getHistogram(
    #     tfile, "ggh4b2t-{}-{}".format(m1, m2), variable, channel, category
    # )
    # signal_vbfbbtt = getHistogram(
    #     tfile, "vbf4b2t-{}-{}".format(m1, m2), variable, channel, category
    # )
    # signal = signal_gghbbtt + signal_vbfbbtt
    signal = getHistogram(tfile, "TotalSig", variable, channel, category)

    # Scale any histograms that need to be scaled: # assuming 100\% h->a1a2->4b2tau, and then multiplying by x100
    asymm_scale_signal = 10
    signal = signal * asymm_scale_signal

    isBlinded = False
    makePlots(
        isBlinded,
        output,
        year,
        channel,
        category,
        variable,
        doLog,
        lumiscale,
        data,
        embed,
        other,
        ttBar,
        DY,
        fakeHisto,
        fakeName,
        totalBkg,
        signal,
        asymm_scale_signal,
        m1,
        m2,
    )


# Loop over all variable names and make a plot for each
if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument(
        "--path", type=str, help="Full path to ROOT file with all histograms"
    )
    parser.add_argument("--channel", type=str, help="Channel (mutau), (etau), (emu)")
    parser.add_argument(
        "--output", type=str, help="Output directory for plots and log files"
    )
    parser.add_argument("--year", type=str, help="Year")
    parser.add_argument(
        "--scale", type=float, help="Scaling of the integrated luminosity"
    )
    parser.add_argument(
        "--doLog",
        action=argparse.BooleanOptionalAction,
        help="Make plots with log scale",
    )
    parser.add_argument("--m1", type=int, help="First mass point")
    parser.add_argument("--m2", type=int, help="Second mass point")
    args = parser.parse_args()
    prefit_categories = [
        "ch1_prefit",
        "ch2_prefit",
        "ch3_prefit",
    ]
    postfit_categories = [
        "ch1_postfit",
        "ch2_postfit",
        "ch3_postfit",
    ]
    for variable in labels.keys():
        for category in prefit_categories + postfit_categories:
            main(
                args.path,
                args.output,
                args.year,
                variable,
                args.scale,
                args.channel,
                category,
                args.doLog,
                args.m1,
                args.m2,
            )
