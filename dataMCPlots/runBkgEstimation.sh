# Usage:
#  # do cmsenv and source the venv first, or this will give warnings and quit
#  bash runBkgEstimation.sh

# TIMESTAMP is the timestamp of this iteration (passed to INPUT_HISTOGRAMS_ROOT and the eventual output www folder).
# YEAR is 2018, 2017, 2016preVFP, or 2016postVFP (no parentheses needed)
# CHANNEL is "mutau", "etau", or "emu" (with parentheses)
# INPUT_HISTOGRAMS_ROOT is the path to the .ROOT file from hadd (e.g. hadd -f -j -k) of all the histograms to run on
# DO_BKG_ESTIMATION: If true, perform the background estimation. If false, only want to re-run the plotting step (e.g. to change cosmetics) and not change the histograms

export TIMESTAMP="2025-01-24-23h46m-year-2018-emu-iteration1-allVars"
export YEAR=2018
export CHANNEL="emu"
export DATACARD_MODE=false
export DO_BKG_ESTIMATION=false

export INPUT_HISTOGRAMS_ROOT="/eos/cms/store/group/phys_susy/AN-24-166/${USER}/condorHistogramming/${TIMESTAMP}/histograms.root"

#--------------------------------------------------------
# Check if we used bash
#--------------------------------------------------------
if [[ "$0" != "$BASH_SOURCE" ]]; then
    echo ">>> ${BASH_SOURCE[0]}: Error: Script must be run with bash"
    return
fi
#--------------------------------------------------------
# Check if environments are set
#--------------------------------------------------------
if [[ -z ${CMSSW_BASE} ]]; then
    echo ">>> ${BASH_SOURCE[0]}: CMSSW environment is not set! Make sure to do cmsenv"
    exit 1
fi
if [[ -z ${VIRTUAL_ENV} ]]; then
    echo ">>> ${BASH_SOURCE[0]}: Virtual environment is not set! Make sure to source venv-LUNA/bin/activate"
    exit 1
fi

#--------------------------------------------------------
# Run the background estimation
#--------------------------------------------------------
start=$(date +%s)


# mutau channel: specify the input/output file paths...
BKG_EST_OUT_ROOT="$(dirname ${INPUT_HISTOGRAMS_ROOT})/out_${CHANNEL}.root"



CARD_MODE_FLAG=""
if [[ "${DATACARD_MODE}" == true ]]; then
    CARD_MODE_FLAG="--datacard"
fi

if [[ "${DO_BKG_ESTIMATION}" == true ]]; then
    if [[ -f ${BKG_EST_OUT_ROOT} ]]; then
        rm ${BKG_EST_OUT_ROOT}
    fi
    python3 backgroundEstimation.py --inFile=${INPUT_HISTOGRAMS_ROOT} --year=$YEAR --outHists=${BKG_EST_OUT_ROOT} --doSys --channel=${CHANNEL} ${CARD_MODE_FLAG}
else
    if ! [[ -f ${BKG_EST_OUT_ROOT} ]]; then
        echo ">>>  ${BASH_SOURCE[0]}: DO_BKG_ESTIMATION was false but ${BKG_EST_OUT_ROOT} does not exist, exiting"
        exit 1
    else
        echo ">>>  ${BASH_SOURCE[0]}: DO_BKG_ESTIMATION was false, do not perform background estimation (i.e. do not run backgroundEstimation.py), assume ${BKG_EST_OUT_ROOT} exists, and use it"
    fi
fi

#--------------------------------------------------------
# If the previous step was successful, call the script that makes the stack plots
#--------------------------------------------------------
if [[ $? == 0 ]]; then
    echo ">>> Producing plots from the ROOT file: ${BKG_EST_OUT_ROOT}"
    command="source runStackPlots.sh $YEAR ${BKG_EST_OUT_ROOT} ${TIMESTAMP} ${CHANNEL} ${DATACARD_MODE}"
    echo ">>>  ${BASH_SOURCE[0]}: attempting to run command...... ${command}"
    eval ${command}
fi

endtime=$(( $(date +%s) - $start ))

echo "Done in ${endtime} seconds"
