# backgroundEstimation.py
#
# The configuration files control these aspects:
# 1. Everything is either MC, Embedded, or Data.
# 2. Each of these, includes one or more "groups". (e.g. MC includes "ttbar"). Defined in config/groups.py.
# 3. Each "group" consists of one or more "processes". e.g. "ttbar" includes TTTo2L2Nu, TTToSemiLeptonic, and TTToHadronic. Defined in config/samplesInGroups.py.
# 4. The generator-level signal efficiency is defined in config/signalConfig.py.
# 5. The systematics affecting each group is defined in config/systematics.py.
# 6. The variables are defined in config/variables.py. (Whether we are running only on the final fit variable or all control variables is set by a flag passed to this script,
#    no need to comment out things in config/variables.py).

import argparse
import ROOT
import os
from config.groups import dictGroups
from config.samplesInGroups import dictSamplesInGroups
from config.signalConfig import dictSignalConfig
from config.systematics import dictSystematics
from config.variables import ranges

ROOT.gROOT.SetBatch(True)


def isSignal(name):
    return (
        ("signal" in name)
        or ("gghbbtt" in name)
        or ("vbfbbtt" in name)
        or ("vbf4b2t" in name)
        or ("ggh4b2t" in name)
        or ("vbf2b2t" in name)
        or ("ggh2b2t" in name)
    )


def printErrorBars(h):
    """
    Print error bars of a histogram.
    """
    nBins = h.GetNbinsX() + 1
    for i in range(nBins):
        print(
            "... bin {}, content: {}, error: {}".format(
                i, h.GetBinContent(i), h.GetBinError(i)
            )
        )


def getHistogram(
    tfile, name, variable, channel="mutau", category="", scale=1.0, tag=""
):
    """
    Retrieve a histogram from the input file based on the process and the variable
    """
    # print(">>> getHistogram: file: {} \n name: {} \n variable: {} \n category: {} \n tag: {}".format(tfile, name, variable, category, tag))
    if category == "":
        hName = "{}/{}_{}{}".format(channel, name, variable, tag)
    else:
        hName = "{}/{}/{}_{}{}".format(channel, category, name, variable, tag)
    h = tfile.Get(hName)
    if not h:
        raise Exception("Failed to load histogram {}.".format(hName))
    h.Scale(scale)
    # print("getHistogram: {} yield : {}".format(hName, getYield(h)))
    return h


def writeHistogram(file, h, name, folder="", subfolder=""):
    """
    Write a histogram with a given name to the output ROOT file. If folder is specified write to a specific folder.
    """
    # print("name and folder", name, folder)
    if folder != "":
        folderDir = file.GetDirectory(folder)
        if not folderDir:
            folderDir = file.mkdir(folder)
        folderDir.cd()
        subFolderDir = folderDir.GetDirectory(subfolder)
        if not subFolderDir:
            subFolderDir = folderDir.mkdir(subfolder)
        subFolderDir.cd()
    h.SetName(name)
    h.Write()


def getYield(h):
    """
    Get the yield of a histogram.
    """
    y = h.Integral()
    return y


def getHistogramArray(
    infile, processes, variable, channel="mutau", category="", scale=1.0, tag=""
):
    """
    (Helper) Return an array of the histograms corresponding to the list 'processes'
    """
    arr = []
    for proc in processes:
        h = getHistogram(infile, proc, variable, channel, category, scale, tag)
        arr.append(h)
    return arr


def addHists(arr):
    """
    (Helper) Return the sum of the TH1 histograms in the given array (using the .Add() method)
    """
    hSum = arr[0]
    for h in arr[1:]:
        hSum.Add(h)
    return hSum


def getTotalHistogram(
    infile, processes, variable, channel="mutau", category="", scale=1.0, tag=""
):
    """
    Returns one histogram, consisting of the sum of the TH1 histograms for input file infile,
    process list 'processes', and the specified variable.
    """
    # print(processes)
    arr = getHistogramArray(infile, processes, variable, channel, category, scale, tag)
    return addHists(arr)


def createCategoriesDataMCHistograms(
    path,
    year,
    yearOnly,
    output,
    variable,
    doSys=False,
    datacardVariable="m_tt",
    channel="mutau",
    category="",
):
    """
    createCategoriesDataMCHistograms: essentially the same thing as the previous function: only
    have the option to write to a sub-folder folderName, and the datacard
    requires different process groupings (more fine-grained than data/MC). I should have
    made this a for loop but it is what it is
    """
    infile = ROOT.TFile(path, "READ")

    # Dict: for each umbrella label (MC, embed, data), which groups (ttbar, etc.) are in each one
    dGroups = dictGroups[year][channel]

    # Dict: for each group (e.g. ttbar), which samples are in it (e.g. for ttbar, there is TTToSemiLeptonic, TTTo2L2Nu, TTToHadronic)
    dSamples = dictSamplesInGroups[year][channel]

    # Dict telling us which systematics apply for each dataset type
    dSys = dictSystematics[year][channel]

    # Initialize the nested dictionary (for each process, a dictionary where the keys are the systematics
    # and the values are another dictionary where the keys are "Up", "Down" and the
    # values are the TH1F.
    # Eventually want something which can be accessed like this:
    #      hDict["ttbar"]["CMS_muES_eta0to1p2"]["Up"]
    hDict = {}
    for (
        group
    ) in (
        dGroups.keys()
    ):  # umbrellas are MC, embedded, data_obs, fake, signal (ONLY used to group systematics, e.g.
        # saying that "this umbrella of samples will be affected by these systematics"
        for process in dGroups[group]:  # within MC: ZJ, bar, etc.
            # print("Doing {} in group {}".format(process, group))
            hDict[process] = {}
            for nom in ["Nominal", "Nominal_antiIso"]:
                hDict[process][nom] = {}
            for sys in dSys[group]:
                hDict[process][sys] = {}
    # Also initialize jetfake
    for group in dGroups.keys():
        for sys in dSys[group]:
            if (channel == "mutau") or (channel == "etau"):
                hDict["fake"][sys] = {}
            elif channel == "emu":
                hDict["qcd"][sys] = {}

    # Read from the input file and fill the nested dictionary
    for group in dGroups.keys():
        # print(">>> Going into group {}", group)

        processes = dGroups[group]
        # Skip fakes and QCD: we need to compute this, it will not be in the input file
        if (group == "fake") or (group == "qcd"):
            continue

        for process in processes:
            sampleScale = 1.0
            if isSignal(process):
                sampleScale = dictSignalConfig[year][process]
            # print(">>> Going into channel {} and process {}".format(channel, process))
            # mutau and etau: Nominal histograms
            if (channel == "mutau") or (channel == "etau"):
                hDict[process]["Nominal"] = getTotalHistogram(
                    infile, dSamples[process], variable, channel, category, sampleScale
                )
                hDict[process]["Nominal_antiIso"] = getTotalHistogram(
                    infile,
                    dSamples[process],
                    variable,
                    channel,
                    category,
                    sampleScale,
                    "_antiIso",
                )
            # emu: Nominal histograms
            elif channel == "emu":
                hDict[process]["Nominal"] = getTotalHistogram(
                    infile, dSamples[process], variable, channel, category, sampleScale
                )
                hDict[process]["Nominal_ss"] = getTotalHistogram(
                    infile,
                    dSamples[process],
                    variable,
                    channel,
                    category,
                    sampleScale,
                    "_ss",
                )

            if doSys:
                # Get the systematic shifts
                systematics = dSys[group]
                # print(
                #     "... For process",
                #     process,
                #     " we need to get the systematics:",
                #     systematics,
                # )
                for sys in systematics:
                    # Skip nonDY: this we will compute by hand
                    if "nonDY" in sys:
                        continue

                    # Custom renaming of histograms to get
                    sysToGet = sys  # default
                    if (("Jet" in sys) or ("btagsf" in sys)) and not (yearOnly in sys):
                        sysToGet = f"{sys}_{yearOnly}"  # e.g. for CMS_JetAbsolute, get "CMS_JetAbsolute_2018"
                    elif (("Jet" in sys) or ("btagsf" in sys)) and not (
                        yearOnly in sys
                    ):
                        sysToGet = sys.replace(
                            "_" + yearOnly, "year_" + yearOnly
                        )  # e.g. for CMS_JetRelativeSample_2018, get "CMS_JetRelativeSampleyear_2018"
                    # mutau and etau
                    if (channel == "mutau") or (channel == "etau"):
                        for isoRegion in ["", "_antiIso"]:
                            for shift in ["Up", "Down"]:
                                hDict[process][sys][
                                    shift + isoRegion
                                ] = getTotalHistogram(
                                    infile,
                                    dSamples[process],
                                    variable,
                                    channel,
                                    category,
                                    sampleScale,
                                    "_" + sysToGet + shift + isoRegion,
                                )
                                # print(
                                #     "I was supposed to get: ",
                                #     hDict[process][sys][shift + isoRegion],
                                #     "to fill",
                                #     process,
                                #     sys,
                                #     shift + isoRegion,
                                # )
                    # emu: histograms had different naming convention, e.g. VBFHToTauTau_pt_2_ss_CMS_JetHFyear_2018Down
                    elif channel == "emu":
                        for signRegion in ["", "_ss"]:
                            for shift in ["Up", "Down"]:
                                hDict[process][sys][
                                    shift + signRegion
                                ] = getTotalHistogram(
                                    infile,
                                    dSamples[process],
                                    variable,
                                    channel,
                                    category,
                                    sampleScale,
                                    signRegion + "_" + sysToGet + shift,
                                )

    # mutau and etau: Compute the nominal fake histogram
    # print(hDict.keys())
    if (channel == "mutau") or (channel == "etau"):
        fake = hDict["data_obs"]["Nominal_antiIso"].Clone()
        # fake.Sumw2()
        # print(channel, "Computing fake histogram...")
        # print(channel, dGroups["MC"] + dGroups["embedded"])
        for processToSubtract in dGroups["MC"] + dGroups["embedded"]:
            # Do not subtract signal!
            if isSignal(processToSubtract):
                continue
            # ZJ, ttbar, ST, etc.
            # print(channel, "Subtracting", processToSubtract)
            histToSubtract = hDict[processToSubtract]["Nominal_antiIso"]
            fake.Add(histToSubtract, -1)
            # print(">>> After subtracting, getting these error bars...")
            # printErrorBars(fake)

        hDict["fake"]["Nominal"] = fake
    # emu: Compute the nominal QCD histogram: all same-sign MC events are subtracted from the same-sign data events
    elif channel == "emu":
        qcd = hDict["data_obs"]["Nominal_ss"].Clone()
        # print(channel, "Computing same-sign histogram...")
        # print(channel, dGroups["MC"] + dGroups["embedded"])
        for processToSubtract in dGroups["MC"] + dGroups["embedded"]:
            # Do not subtract signal!
            if isSignal(processToSubtract):
                continue
            # print(channel, "Subtracting", processToSubtract)
            histToSubtract = hDict[processToSubtract]["Nominal_ss"]
            qcd.Add(histToSubtract, -1)
        hDict["qcd"]["Nominal"] = qcd

    if doSys:
        # One specific systematic must be computed by hand: collect all the *contamination* histograms for this variable from the different samples, and add them up.
        # Compute Embedded minus 10% of this contamination sum, and compute Embedded plus 10% of this contamination sum, and add an entry to the dictionary for this
        # Initialize
        contaminationUp = hDict["embedded"]["Nominal"].Clone()
        contaminationDown = hDict["embedded"]["Nominal"].Clone()
        for process in dGroups["MC"]:
            thisContribution = getTotalHistogram(
                infile,
                dSamples[process],
                variable,
                channel,
                category,
                sampleScale,
                "_contamination_estimation",
            ).Clone()
            contaminationUp = contaminationUp + 0.1 * thisContribution
            contaminationDown = contaminationDown - 0.1 * thisContribution
        # Initialize
        hDict["embedded"]["CMS_nonDY_" + yearOnly] = {}
        hDict["embedded"]["CMS_nonDY_" + yearOnly]["Up"] = contaminationUp
        hDict["embedded"]["CMS_nonDY_" + yearOnly]["Down"] = contaminationDown

        # For each systematic, compute the fake histogram, using nominal histograms when a process is not shifted
        for group in dSys.keys():
            systematics = dSys[group]
            for sys in systematics:
                # Skip nonDY
                if "nonDY" in sys:
                    continue
                for shift in ["Up", "Down"]:
                    if (channel == "mutau") or (channel == "etau"):
                        # Initialize fake histo
                        fake = hDict["data_obs"]["Nominal_antiIso"].Clone()
                        # fake.Sumw2()
                    elif channel == "emu":
                        qcd = hDict["data_obs"]["Nominal_ss"].Clone()
                        # qcd.Sumw2()
                    # print(">>> About to handle", channel, sys, shift)
                    # Get all the processes we need to subtract
                    # The problem is that for a given systematic (e.g. CMS_muES_eta0to1p2),
                    # not all datasets will have shifts for that.
                    # For instace dSys["MC"] includes CMS_muES_eta0to1p2 but dSys["Embed"] does not.
                    # And for instance dSys["MC"] and dSys["data_obs"] will include CMS_jetFR_pt0to25.
                    for myGroup in dSys.keys():
                        # Don't double-subtract from fake or data_obs
                        if (
                            myGroup == "fake"
                            or myGroup == "qcd"
                            or myGroup == "data_obs"
                        ):
                            continue

                        if sys in dSys[myGroup]:
                            # Skip nonDY
                            if "nonDY" in sys:
                                continue
                            # e.g. if CMS_muES_eta0to1p2 is in dSys["MC"]
                            # Subtract the processes that DO have shifts
                            # print(channel, "Found", sys, "in dSys[myGroup]", myGroup)
                            for processToSubtract in dGroups[myGroup]:
                                # Do not subtract signal!
                                if isSignal(processToSubtract):
                                    continue
                                # E.g. ZJ, ttbar, ST, VV, etc. for CMS_muES_eta01to1p2
                                if (channel == "mutau") or (channel == "etau"):
                                    # print(
                                    #     "About to subtract",
                                    #     channel,
                                    #     processToSubtract,
                                    #     sys,
                                    #     shift + "_antiIso",
                                    # )
                                    # print(hDict[processToSubtract])
                                    histToSubtract = hDict[processToSubtract][sys][
                                        shift + "_antiIso"
                                    ]
                                    fake.Add(histToSubtract, -1)
                                elif channel == "emu":
                                    # print(
                                    #     "About to subtract",
                                    #     channel,
                                    #     processToSubtract,
                                    #     "_ss",
                                    #     sys,
                                    #     shift,
                                    # )
                                    histToSubtract = hDict[processToSubtract][sys][
                                        shift + "_ss"
                                    ]
                                    qcd.Add(histToSubtract, -1)

                        else:
                            # Subtract the processes that DON'T have shifts
                            for processToSubtract in dGroups[myGroup]:
                                # Do not subtract signal!
                                if isSignal(processToSubtract):
                                    continue

                                if (channel == "mutau") or (channel == "etau"):
                                    #  e.g. Embed for CMS_muES_eta0to1p2
                                    # print(
                                    #     channel,
                                    #     "About to subtract",
                                    #     processToSubtract,
                                    #     "Nominal_antiIso",
                                    # )
                                    histToSubtract = hDict[processToSubtract][
                                        "Nominal_antiIso"
                                    ]
                                    fake.Add(histToSubtract, -1)
                                elif channel == "emu":
                                    #  e.g. Embed for CMS_muES_eta0to1p2
                                    # print(
                                    #     channel,
                                    #     "About to subtract",
                                    #     processToSubtract,
                                    #     "Nominal_ss",
                                    # )
                                    histToSubtract = hDict[processToSubtract][
                                        "Nominal_ss"
                                    ]
                                    qcd.Add(histToSubtract, -1)

                    # After subtracting all the possible non-data, non-signal contributions, store the shifted fake/qcd histogram
                    if (channel == "mutau") or (channel == "etau"):
                        # print("About to store fake", sys, shift)
                        hDict["fake"][sys][shift] = fake
                    elif channel == "emu":
                        # print("About to store QCD", sys, shift)
                        hDict["qcd"][sys][shift] = qcd

    # Write output nominal histograms to out.root
    outfile = ROOT.TFile(args.outHists, "UPDATE")
    for process in hDict.keys():
        hist = hDict[process]["Nominal"]
        writeHistogram(outfile, hist, "{}_{}".format(process, variable), category)
        # # CombineHarvester expects the shapes to just be "data_obs" not "data_obs_m_tt"
        if variable == datacardVariable:
            writeHistogram(outfile, hist, "{}".format(process), category)

    # Write shifted histograms to out.root
    print(">>> Writing shifted histograms now")
    for process in hDict.keys():
        if doSys:
            availableHists = hDict[process].keys()
            for sys in availableHists:
                if "_EMB_" in sys:
                    sysWithoutEMB = sys.replace("_EMB_", "_")
                if "Nominal" in sys:
                    continue
                # Skip writing nonDY for fake
                if (process == "fake") and ("nonDY" in sys):
                    continue
                for shift in ["Up", "Down"]:
                    # Default
                    histogramName = f"{process}_{variable}_{sys}{shift}"

                    hist = hDict[process][sys][shift]
                    writeHistogram(
                        outfile,
                        hist,
                        histogramName,
                        category,
                    )
                    # Again: CombineHarvester expects the shapes to just be "ttbar_CMS_btagsf_hf_2018Up" not "ttbar_m_tt_CMS_btagsf_hf_2018Up"
                    if variable == datacardVariable:
                        writeHistogram(
                            outfile,
                            hist,
                            histogramName.replace("_m_tt_", "_"),
                            category,
                        )

                    # If embedded and writing an _EMB_ shift, ALSO write it without the _EMB_ to the output file, so we can pass correlated uncertainties
                    # to Combine
                    if "_EMB_" in sys:
                        writeHistogram(
                            outfile,
                            hist,
                            f"{process}_{variable}_{sysWithoutEMB}{shift}",
                            category,
                        )
                        # repeat for m_tt
                        if variable == datacardVariable:
                            writeHistogram(
                                outfile,
                                hist,
                                f"{process}_{sysWithoutEMB}{shift}",
                                category,
                            )

                    # Special case: for embedded also write EMB_tauidWP
                    if "Emb" in process and "tauidWP" in sys:
                        writeHistogram(
                            outfile,
                            hist,
                            f"{process}_{variable}_EMB_{sys}{shift}",
                            category,
                        )
                        # repeat for m_tt
                        if variable == datacardVariable:
                            writeHistogram(
                                outfile,
                                hist,
                                f"{process}_EMB_{sys}{shift}",
                                category,
                            )

    infile.Close()
    outfile.Close()


if __name__ == "__main__":
    parser = argparse.ArgumentParser(
        description="Perform background estimation to produce histograms for data/MC figures, and produce datacards."
    )
    parser.add_argument(
        "--inFile", help="Path to input ROOT file with all histograms", required=True
    )
    parser.add_argument(
        "--year", help="Year (2018, 2017, 2016preVFP, or 2016postVFP)", required=True
    )
    parser.add_argument(
        "--channel", help="Channel (mutau, etau, or emu)", required=True
    )
    parser.add_argument(
        "--outHists",
        help="Path of output ROOT file for data/MC histograms",
        required=True,
    )
    parser.add_argument(
        "--outputDatacard",
        help="Path of output ROOT file for datacards",
        required=False,
    )
    parser.add_argument("--doSys", help="Do systematics", action="store_true")
    parser.add_argument(
        "--datacard",
        help="Do datacard variable and categories, i.e. does input ROOT file only have the final variable to fit?",
        required=False,
        action="store_true",
    )
    args = parser.parse_args()

    # Default behaviour
    categories = ["inclusive"]
    if args.datacard:
        categories = [
            "inclusive",
            "lowMassSR",
            "mediumMassSR",
            "highMassSR",
            "highMassCR",
        ]

    # Default variables
    variables = ranges.keys()
    if args.datacard:
        variables = ["m_tt"]

    yearOnly = args.year
    if "2016" in args.year:
        yearOnly = "2016"

    print(">>> {}: {}: doing data/MC histograms...".format(__file__, args.channel))
    for category in categories:
        for variable in variables:
            createCategoriesDataMCHistograms(
                args.inFile,
                args.year,
                yearOnly,
                args.outputDatacard,
                variable,
                args.doSys,
                "m_tt",  # this is the variable to write to the histograms with no variable name (e.g. "data_obs"). This is what Combine will treat as the fit variable
                args.channel,
                category,
            )
