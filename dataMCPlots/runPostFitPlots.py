# runPostfitPlots.sh

import os

masspoints = [[100, 15], [100, 20], [40, 15], [60, 20], [60, 30], [80, 30]]

for pair in masspoints:
    m1 = pair[0]
    m2 = pair[1]
    year = "2018"
    inputFile = f"/eos/user/s/skkwan/www/limits/postfitShapes_a1a2_{m1}_{m2}.root"
    timestamp = "postfit-test"
    channel = "mutau"

    os.system("rm -r postfitPlotsOutputs/*")
    os.system("mkdir -p postfitPlotsOutputs/")

    os.system(
        f"python3 postfitPlots.py --path={inputFile} --channel={channel} --output=postfitPlotsOutputs --year={year} --scale=1.0 --doLog --m1={m1} --m2={m2}"
    )

    website = f"https://skkwan.web.cern.ch/dataMC/{timestamp}"
    folder = f"/eos/user/s/skkwan/www/dataMC/{timestamp}"

    os.system(f"mkdir -p {folder}")

    print(
        f">>> runStackPlots.sh: copying outputs to {folder}, check CERNBOX or {website} for data/MC figures"
    )
    os.system(f"cp postfitPlotsOutputs/* {folder}")
    os.system(f"cp /eos/user/s/skkwan/www/dataMC/index.php {folder}")
