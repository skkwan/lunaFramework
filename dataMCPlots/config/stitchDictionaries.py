from samplesInGroups import dictSamplesInGroups

pre = dictSamplesInGroups["2016preVFP"]
post = dictSamplesInGroups["2016postVFP"]

channels = ["mutau"]

for channel in channels:
    for process in pre[channel]:
        pre_samples = pre[channel][process]
        post_samples = post[channel][process]
        total_samples = pre_samples + post_samples
        print(total_samples)
