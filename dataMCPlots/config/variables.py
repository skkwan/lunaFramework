"""
variables.py

Lists the variables and ranges to do up/down/nominal shapes for in out_mutau.root, out_etau.root, and out_emu.root. Included in backgroundEstimation.py.

NOTE: this only controls what variables to make grouped histograms of. To control what variables to make plots of, declare those in dataMCPlots/stackPlots.py.
"""

default_card_nbins = 20
default_nbins = 20

ranges = {
    # TODO: add BDT score
    "m_tt": (default_nbins, 0, 500),
    "pt_tt": (default_nbins, 0, 500),
    "pt_1": (default_nbins, 10, 120),
    "eta_1": (default_nbins, -2.4, 2.4),
    "phi_1": (default_nbins, -3.14, 3.14),
    "pt_2": (default_nbins, 10, 120),
    "eta_2": (default_nbins, -2.4, 2.4),
    "phi_2": (default_nbins, -3.14, 3.14),
    "bpt_deepflavour_1": (default_nbins, 10, 120),
    "beta_deepflavour_1": (default_nbins, -2.4, 2.4),
    "bphi_deepflavour_1": (default_nbins, -3.14, 3.14),
    "bpt_deepflavour_2": (default_nbins, 10, 120),
    "beta_deepflavour_2": (default_nbins, -2.4, 2.4),
    "bphi_deepflavour_2": (default_nbins, -3.14, 3.14),
    "met": (default_nbins, 0, 200),
    "metphi": (default_nbins, -3.14, 3.14),
    "mtMET_1": (default_nbins, 0, 160),
    "mtMET_2": (default_nbins, 0, 160),
    "D_zeta": (default_nbins, -140, 110),
    "m_btautau_vis": (default_nbins, 0, 400),
    "pt_btautau_vis": (default_nbins, 0, 400),
    "metcov00": (30, 0, 500),
    "metcov01": (30, 0, 500),
    "metcov10": (30, 0, 500),
    "metcov11": (30, 0, 500),
}
