"""
plotConfig.py

Useful parameters for stackPlots.py: luminosity, energy, and the year to display in the plot legend
"""

config2018 = {"lumi": 59.8, "energy": 13, "yearToDisplay": "2018"}

config2017 = {"lumi": 41.5, "energy": 13, "yearToDisplay": "2017"}

config2016 = {"lumi": 36.31, "energy": 13, "yearToDisplay": "2016"}

config2016preVFP = {"lumi": 19.5, "energy": 13, "yearToDisplay": "2016 preVFP"}

config2016postVFP = {"lumi": 16.8, "energy": 13, "yearToDisplay": "2016 postVFP"}

# Dictionary of dictionaries
plotConfigParams = {
    "2018": config2018,
    "2017": config2017,
    "2016": config2016,
    "2016preVFP": config2016preVFP,
    "2016postVFP": config2016postVFP,
}
