"""
systematics.py

Which systematics apply to {MC, data_obs, embed}?

Included in backgroundEstimation.py.
backgroundEstimation.py will expect to find (in the input file) histograms for each of these systematics.
"""

muSys = [
    # Muon ES
    "CMS_muES_eta0to1p2",
    "CMS_muES_eta1p2to2p1",
    "CMS_muES_eta2p1to2p4",
]

eleSys = ["CMS_eleES_bar", "CMS_eleES_end"]

muSysEmbed = [
    "CMS_EMB_muES_eta0to1p2",
    "CMS_EMB_muES_eta1p2to2p1",
    "CMS_EMB_muES_eta2p1to2p4",
]

eleSysEmbed = [
    "CMS_EMB_eleES_bar",
    "CMS_EMB_eleES_end",
]

tauSys = [
    "CMS_tauES_dm0",
    "CMS_tauES_dm1",
    "CMS_tauES_dm10",
    "CMS_tauES_dm11",
    "CMS_eleTES_dm0",
    "CMS_eleTES_dm1",
    "CMS_muTES_dm0",
    "CMS_muTES_dm1",
    "CMS_tauideff_pt20to25",
    "CMS_tauideff_pt25to30",
    "CMS_tauideff_pt30to35",
    "CMS_tauideff_pt35to40",
    "CMS_tauideff_pt40to500",
    "CMS_tauideff_pt500to1000",
    "CMS_tauideff_ptgt1000",
    "CMS_tauideff_VSe_bar",
    "CMS_tauideff_VSe_end",
    "CMS_tauideff_VSmu_eta0to0p4",
    "CMS_tauideff_VSmu_eta0p4to0p8",
    "CMS_tauideff_VSmu_eta0p8to1p2",
    "CMS_tauideff_VSmu_eta1p2to1p7",
    "CMS_tauideff_VSmu_eta1p7to2p3",
]

tauSysEmbed = [
    # Tau ES
    "CMS_EMB_tauES_dm0",
    "CMS_EMB_tauES_dm1",
    "CMS_EMB_tauES_dm10",
    "CMS_EMB_tauES_dm11",
    # Tau ID efficiency systematics
    "CMS_EMB_tauideff_pt20to25",
    "CMS_EMB_tauideff_pt25to30",
    "CMS_EMB_tauideff_pt30to35",
    "CMS_EMB_tauideff_pt35to40",
    "CMS_EMB_tauideff_pt40to500",
    "CMS_EMB_tauideff_pt500to1000",
    "CMS_EMB_tauideff_ptgt1000",
]

jerSys_2018 = [
    "CMS_JetAbsolute",
    "CMS_JetBBEC1",
    "CMS_JetEC2",
    "CMS_JetFlavorQCD",
    "CMS_JetHF",
    "CMS_JetRelativeBal",
    "CMS_JetRelativeSample",
    "CMS_JetAbsolute_2018",
    "CMS_JetBBEC1_2018",
    "CMS_JetEC2_2018",
    "CMS_JetHF_2018",
    "CMS_JetRelativeSample_2018",
]

jerSys_2017 = [
    "CMS_JetAbsolute",
    "CMS_JetBBEC1",
    "CMS_JetEC2",
    "CMS_JetFlavorQCD",
    "CMS_JetHF",
    "CMS_JetRelativeBal",
    "CMS_JetRelativeSample",
    "CMS_JetAbsolute_2017",
    "CMS_JetBBEC1_2017",
    "CMS_JetEC2_2017",
    "CMS_JetHF_2017",
    "CMS_JetRelativeSample_2017",
]

jerSys_2016 = [
    "CMS_JetAbsolute",
    "CMS_JetBBEC1",
    "CMS_JetEC2",
    "CMS_JetFlavorQCD",
    "CMS_JetHF",
    "CMS_JetRelativeBal",
    "CMS_JetRelativeSample",
    "CMS_JetAbsolute_2016",
    "CMS_JetBBEC1_2016",
    "CMS_JetEC2_2016",
    "CMS_JetHF_2016",
    "CMS_JetRelativeSample_2016",
]

btagSys = [
    "CMS_btagsf_hf",
    "CMS_btagsf_lf",
    "CMS_btagsf_hfstats1",
    "CMS_btagsf_hfstats2",
    "CMS_btagsf_lfstats1",
    "CMS_btagsf_lfstats2",
    "CMS_btagsf_cferr1",
    "CMS_btagsf_cferr2",
]

metSys = [
    "CMS_met_0j_resolution",
    "CMS_met_1j_resolution",
    "CMS_met_gt1j_resolution",
    "CMS_met_0j_response",
    "CMS_met_1j_response",
    "CMS_met_gt1j_response",
    "CMS_UES",
]

jetFakingTauBackgroundSys = [
    "CMS_jetFR_pt0to25",
    "CMS_jetFR_pt25to30",
    "CMS_jetFR_pt30to35",
    "CMS_jetFR_pt35to40",
    "CMS_jetFR_pt40to50",
    "CMS_jetFR_pt50to60",
    "CMS_jetFR_pt60to80",
    "CMS_jetFR_pt80to100",
    "CMS_jetFR_pt100to120",
    "CMS_jetFR_pt120to150",
    "CMS_jetFR_ptgt150",
]

# crosstrg_fakefactor for mutau and etau
crosstrg_fakefactor = ["CMS_crosstrg_fakefactor"]

# weight_zPt
zPtSys = ["CMS_Zpt"]

# weight_topPt
topPtSys = ["CMS_toppt"]

nonDYSys = ["CMS_nonDY"]

# trgeff_single, trgeff_cross for mutau and etau
mutau_trgeffSys = ["CMS_trgeff_single_mt", "CMS_trgeff_cross_mt"]
etau_trgeffSys = ["CMS_trgeff_single_et", "CMS_trgeff_cross_et"]
emu_trgeffSys = ["CMS_trgeff_Mu8E23_em", "CMS_trgeff_Mu23E12_em", "CMS_trgeff_both_em"]

mutau_trgeffSys_Embed = ["CMS_EMB_trgeff_single_mt", "CMS_EMB_trgeff_cross_mt"]
etau_trgeffSys_Embed = ["CMS_EMB_trgeff_single_et", "CMS_EMB_trgeff_cross_et"]
emu_trgeffSys_Embed = [
    "CMS_EMB_trgeff_Mu8E23_em",
    "CMS_EMB_trgeff_Mu23E12_em",
    "CMS_EMB_trgeff_both_em",
]

emu_qcdSys = ["CMS_SSclosure", "CMS_SScorrection", "CMS_SSboth2D", "CMS_osss"]

# Embed only: mutau etau: "tautrack_dm0dm10", "tautrack_dm1", "tautrack_dm11"
embed_trackSys = [
    "CMS_EMB_tautrack_dm0dm10",
    "CMS_EMB_tautrack_dm1",
    "CMS_EMB_tautrack_dm11",
]
etau_tauidWP = ["CMS_tauidWP_et"]

# Note: selection efficiencies: no up/down, this is included in the Embedded yield uncertainty

# Prefiring: for all years in UL
prefiring_2018 = ["CMS_prefiring_2018"]
prefiring_2017 = ["CMS_prefiring_2017"]
prefiring_2016 = ["CMS_prefiring_2016"]

mutauSys_MC_2018 = (
    [
        s + "_2018"
        for s in (
            muSys
            + tauSys
            + btagSys
            + jetFakingTauBackgroundSys
            + metSys
            + crosstrg_fakefactor
            + zPtSys
            + topPtSys
            + mutau_trgeffSys
        )
    ]
    + jerSys_2018
    + prefiring_2018
    )

etauSys_MC_2018 = (
    [
        s + "_2018"
        for s in (
            eleSys
            + tauSys
            + btagSys
            + jetFakingTauBackgroundSys
            + metSys
            + crosstrg_fakefactor
            + zPtSys
            + topPtSys
            + etau_trgeffSys
            + etau_tauidWP
        )
    ]
    + jerSys_2018
    + prefiring_2018
    )

# Note the lack of the emu_qcdSys in the signal systematics to do
emuSys_signal_2018 = (
    [
        s + "_2018"
        for s in (
            eleSys 
            + muSys 
            + btagSys 
            + metSys 
            + zPtSys 
            + topPtSys 
            + emu_trgeffSys)
    ] 
    + jerSys_2018 
    + prefiring_2018
    )

emuSys_MC_2018 = ( 
    [
        s + "_2018"
        for s in (
            eleSys
            + muSys
            + btagSys
            + metSys
            + zPtSys
            + topPtSys
            + emu_trgeffSys
            + emu_qcdSys
        )
    ] 
    + jerSys_2018
    + prefiring_2018
    )

### 2017
mutauSys_MC_2017 = (
    [
        s + "_2017"
        for s in (
            muSys
            + tauSys
            + btagSys
            + jetFakingTauBackgroundSys
            + metSys
            + crosstrg_fakefactor
            + zPtSys
            + topPtSys
            + mutau_trgeffSys
        )
    ]
    + jerSys_2017
    + prefiring_2017
    )

etauSys_MC_2017 = (
    [
        s + "_2017"
        for s in (
            eleSys
            + tauSys
            + btagSys
            + jetFakingTauBackgroundSys
            + metSys
            + crosstrg_fakefactor
            + zPtSys
            + topPtSys
            + etau_trgeffSys
            + etau_tauidWP
        )
    ]
    + jerSys_2017
    + prefiring_2017
)

# 2017: Note the lack of the emu_qcdSys in the signal systematics to do
emuSys_signal_2017 = (
    [
        s + "_2017"
        for s in (
            eleSys
            + muSys
            + btagSys
            + metSys
            + zPtSys
            + topPtSys
            + emu_trgeffSys)
    ]
    + jerSys_2017
    + prefiring_2017
    )

emuSys_MC_2017 = (
    [
        s + "_2017"
        for s in (
            eleSys
            + muSys
            + btagSys
            + metSys
            + zPtSys
            + topPtSys
            + emu_trgeffSys
            + emu_qcdSys
        )
    ]
    + jerSys_2017
    + prefiring_2017
    )

### 2016
mutauSys_MC_2016 = (
    [
        s + "_2016"
        for s in (
            muSys
            + tauSys
            + btagSys
            + jetFakingTauBackgroundSys
            + metSys
            + crosstrg_fakefactor
            + zPtSys
            + topPtSys
            + mutau_trgeffSys
        )
    ]
    + jerSys_2016
    + prefiring_2016
    )

etauSys_MC_2016 = (
    [
        s + "_2016"
        for s in (
            eleSys
            + tauSys
            + btagSys
            + jetFakingTauBackgroundSys
            + metSys
            + crosstrg_fakefactor
            + zPtSys
            + topPtSys
            + etau_trgeffSys
            + etau_tauidWP
        )
    ]
    + jerSys_2016
    + prefiring_2016
    )

# 2016: Note the lack of the emu_qcdSys in the signal systematics to do
emuSys_signal_2016 = (
    [
        s + "_2016"
        for s in (
            eleSys
            + muSys
            + btagSys
            + metSys
            + zPtSys
            + topPtSys
            + emu_trgeffSys)
    ]
    + jerSys_2016
    + prefiring_2016
    )

emuSys_MC_2016 = (
    [
        s + "_2016"
        for s in (
            eleSys
            + muSys
            + btagSys
            + metSys
            + zPtSys
            + topPtSys
            + emu_trgeffSys
            + emu_qcdSys
        )
    ]
    + jerSys_2016
    + prefiring_2016
    )

dictSystematics = {
    "2018": {
        "mutau": {
            "MC": mutauSys_MC_2018,
            "data_obs": [],
            "embedded": [s + "_2018" for s in muSysEmbed]
            + [s + "_2018" for s in tauSysEmbed]
            + [s + "_2018" for s in embed_trackSys]
            + [s + "_2018" for s in mutau_trgeffSys_Embed],
            "fake": [],  # these are computed
            "vbf4b2t-110-15": mutauSys_MC_2018,
            "vbf4b2t-100-15": mutauSys_MC_2018,
            "vbf4b2t-100-20": mutauSys_MC_2018,
            "vbf4b2t-30-15": mutauSys_MC_2018,
            "vbf4b2t-40-20": mutauSys_MC_2018,
            "vbf4b2t-40-15": mutauSys_MC_2018,
            "vbf4b2t-50-15": mutauSys_MC_2018,
            "vbf4b2t-50-20": mutauSys_MC_2018,
            "vbf4b2t-60-15": mutauSys_MC_2018,
            "vbf4b2t-60-20": mutauSys_MC_2018,
            "vbf4b2t-60-30": mutauSys_MC_2018,
            "vbf4b2t-70-15": mutauSys_MC_2018,
            "vbf4b2t-70-20": mutauSys_MC_2018,
            "vbf4b2t-70-30": mutauSys_MC_2018,
            "vbf4b2t-80-15": mutauSys_MC_2018,
            "vbf4b2t-80-20": mutauSys_MC_2018,
            "vbf4b2t-80-30": mutauSys_MC_2018,
            "vbf4b2t-90-15": mutauSys_MC_2018,
            "vbf4b2t-90-20": mutauSys_MC_2018,
            "vbf4b2t-90-30": mutauSys_MC_2018,
            "ggh4b2t-110-15": mutauSys_MC_2018,
            "ggh4b2t-100-15": mutauSys_MC_2018,
            "ggh4b2t-100-20": mutauSys_MC_2018,
            "ggh4b2t-30-15": mutauSys_MC_2018,
            "ggh4b2t-40-20": mutauSys_MC_2018,
            "ggh4b2t-40-15": mutauSys_MC_2018,
            "ggh4b2t-50-15": mutauSys_MC_2018,
            "ggh4b2t-50-20": mutauSys_MC_2018,
            "ggh4b2t-60-15": mutauSys_MC_2018,
            "ggh4b2t-60-20": mutauSys_MC_2018,
            "ggh4b2t-60-30": mutauSys_MC_2018,
            "ggh4b2t-70-15": mutauSys_MC_2018,
            "ggh4b2t-70-20": mutauSys_MC_2018,
            "ggh4b2t-70-30": mutauSys_MC_2018,
            "ggh4b2t-80-15": mutauSys_MC_2018,
            "ggh4b2t-80-20": mutauSys_MC_2018,
            "ggh4b2t-80-30": mutauSys_MC_2018,
            "ggh4b2t-90-15": mutauSys_MC_2018,
            "ggh4b2t-90-20": mutauSys_MC_2018,
            "ggh4b2t-90-30": mutauSys_MC_2018,
            "vbf2b2t-20-15": mutauSys_MC_2018,
            "vbf2b2t-30-15": mutauSys_MC_2018,
            "vbf2b2t-30-20": mutauSys_MC_2018,
            "vbf2b2t-40-20": mutauSys_MC_2018,
            "vbf2b2t-40-30": mutauSys_MC_2018,
            "vbf2b2t-50-30": mutauSys_MC_2018,
            "vbf2b2t-50-40": mutauSys_MC_2018,
            "vbf2b2t-60-30": mutauSys_MC_2018,
            "vbf2b2t-60-40": mutauSys_MC_2018,
            "vbf2b2t-60-50": mutauSys_MC_2018,
            "vbf2b2t-70-40": mutauSys_MC_2018,
            "vbf2b2t-70-50": mutauSys_MC_2018,
            "vbf2b2t-80-40": mutauSys_MC_2018,
            "ggh2b2t-20-15": mutauSys_MC_2018,
            "ggh2b2t-30-15": mutauSys_MC_2018,
            "ggh2b2t-30-20": mutauSys_MC_2018,
            "ggh2b2t-40-20": mutauSys_MC_2018,
            "ggh2b2t-40-30": mutauSys_MC_2018,
            "ggh2b2t-50-30": mutauSys_MC_2018,
            "ggh2b2t-50-40": mutauSys_MC_2018,
            "ggh2b2t-60-30": mutauSys_MC_2018,
            "ggh2b2t-60-40": mutauSys_MC_2018,
            "ggh2b2t-60-50": mutauSys_MC_2018,
            "ggh2b2t-70-40": mutauSys_MC_2018,
            "ggh2b2t-70-50": mutauSys_MC_2018,
            "ggh2b2t-80-40": mutauSys_MC_2018,
        },
        "etau": {
            "MC": etauSys_MC_2018,
            "data_obs": [],
            "embedded": [s + "_2018" for s in eleSysEmbed]
            + [s + "_2018" for s in tauSysEmbed]
            + [s + "_2018" for s in embed_trackSys]
            + [s + "_2018" for s in etau_trgeffSys_Embed]
            + [s + "_2018" for s in etau_tauidWP],
            "fake": [],
            # "gghbbtt15": etauSys_MC_2018,
            # "vbfbbtt15": etauSys_MC_2018,
            # "gghbbtt20": etauSys_MC_2018,
            # "vbfbbtt20": etauSys_MC_2018,
            # "gghbbtt25": etauSys_MC_2018,
            # "vbfbbtt25": etauSys_MC_2018,
            # "gghbbtt30": etauSys_MC_2018,
            # "vbfbbtt30": etauSys_MC_2018,
            # "gghbbtt35": etauSys_MC_2018,
            # "vbfbbtt35": etauSys_MC_2018,
            # "gghbbtt40": etauSys_MC_2018,
            # "vbfbbtt40": etauSys_MC_2018,
            # "gghbbtt45": etauSys_MC_2018,
            # "vbfbbtt45": etauSys_MC_2018,
            # "gghbbtt50": etauSys_MC_2018,
            # "vbfbbtt50": etauSys_MC_2018,
            # "gghbbtt55": etauSys_MC_2018,
            # "vbfbbtt55": etauSys_MC_2018,
            # "gghbbtt60": etauSys_MC_2018,
            # "vbfbbtt60": etauSys_MC_2018,
            "vbf4b2t-110-15": etauSys_MC_2018,
            "vbf4b2t-100-15": etauSys_MC_2018,
            "vbf4b2t-100-20": etauSys_MC_2018,
            "vbf4b2t-30-15": etauSys_MC_2018,
            "vbf4b2t-40-20": etauSys_MC_2018,
            "vbf4b2t-40-15": etauSys_MC_2018,
            "vbf4b2t-50-15": etauSys_MC_2018,
            "vbf4b2t-50-20": etauSys_MC_2018,
            "vbf4b2t-60-15": etauSys_MC_2018,
            "vbf4b2t-60-20": etauSys_MC_2018,
            "vbf4b2t-60-30": etauSys_MC_2018,
            "vbf4b2t-70-15": etauSys_MC_2018,
            "vbf4b2t-70-20": etauSys_MC_2018,
            "vbf4b2t-70-30": etauSys_MC_2018,
            "vbf4b2t-80-15": etauSys_MC_2018,
            "vbf4b2t-80-20": etauSys_MC_2018,
            "vbf4b2t-80-30": etauSys_MC_2018,
            "vbf4b2t-90-15": etauSys_MC_2018,
            "vbf4b2t-90-20": etauSys_MC_2018,
            "vbf4b2t-90-30": etauSys_MC_2018,
            "ggh4b2t-110-15": etauSys_MC_2018,
            "ggh4b2t-100-15": etauSys_MC_2018,
            "ggh4b2t-100-20": etauSys_MC_2018,
            "ggh4b2t-30-15": etauSys_MC_2018,
            "ggh4b2t-40-20": etauSys_MC_2018,
            "ggh4b2t-40-15": etauSys_MC_2018,
            "ggh4b2t-50-15": etauSys_MC_2018,
            "ggh4b2t-50-20": etauSys_MC_2018,
            "ggh4b2t-60-15": etauSys_MC_2018,
            "ggh4b2t-60-20": etauSys_MC_2018,
            "ggh4b2t-60-30": etauSys_MC_2018,
            "ggh4b2t-70-15": etauSys_MC_2018,
            "ggh4b2t-70-20": etauSys_MC_2018,
            "ggh4b2t-70-30": etauSys_MC_2018,
            "ggh4b2t-80-15": etauSys_MC_2018,
            "ggh4b2t-80-20": etauSys_MC_2018,
            "ggh4b2t-80-30": etauSys_MC_2018,
            "ggh4b2t-90-15": etauSys_MC_2018,
            "ggh4b2t-90-20": etauSys_MC_2018,
            "ggh4b2t-90-30": etauSys_MC_2018,
            "vbf2b2t-20-15": etauSys_MC_2018,
            "vbf2b2t-30-15": etauSys_MC_2018,
            "vbf2b2t-30-20": etauSys_MC_2018,
            "vbf2b2t-40-20": etauSys_MC_2018,
            "vbf2b2t-40-30": etauSys_MC_2018,
            "vbf2b2t-50-30": etauSys_MC_2018,
            "vbf2b2t-50-40": etauSys_MC_2018,
            "vbf2b2t-60-30": etauSys_MC_2018,
            "vbf2b2t-60-40": etauSys_MC_2018,
            "vbf2b2t-60-50": etauSys_MC_2018,
            "vbf2b2t-70-40": etauSys_MC_2018,
            "vbf2b2t-70-50": etauSys_MC_2018,
            "vbf2b2t-80-40": etauSys_MC_2018,
            "ggh2b2t-20-15": etauSys_MC_2018,
            "ggh2b2t-30-15": etauSys_MC_2018,
            "ggh2b2t-30-20": etauSys_MC_2018,
            "ggh2b2t-40-20": etauSys_MC_2018,
            "ggh2b2t-40-30": etauSys_MC_2018,
            "ggh2b2t-50-30": etauSys_MC_2018,
            "ggh2b2t-50-40": etauSys_MC_2018,
            "ggh2b2t-60-30": etauSys_MC_2018,
            "ggh2b2t-60-40": etauSys_MC_2018,
            "ggh2b2t-60-50": etauSys_MC_2018,
            "ggh2b2t-70-40": etauSys_MC_2018,
            "ggh2b2t-70-50": etauSys_MC_2018,
            "ggh2b2t-80-40": etauSys_MC_2018,
        },
        "emu": {
            "MC": emuSys_MC_2018,
            "data_obs": [],
            "embedded": [s + "_2018" for s in eleSysEmbed]
            + [s + "_2018" for s in muSysEmbed]
            + [s + "_2018" for s in emu_trgeffSys_Embed]
            + [s + "_2018" for s in emu_qcdSys],
            "qcd": [],
            # "gghbbtt15": emuSys_signal_2018,
            # "vbfbbtt15": emuSys_signal_2018,
            # "gghbbtt20": emuSys_signal_2018,
            # "vbfbbtt20": emuSys_signal_2018,
            # "gghbbtt25": emuSys_signal_2018,
            # "vbfbbtt25": emuSys_signal_2018,
            # "gghbbtt30": emuSys_signal_2018,
            # "vbfbbtt30": emuSys_signal_2018,
            # "gghbbtt35": emuSys_signal_2018,
            # "vbfbbtt35": emuSys_signal_2018,
            # "gghbbtt40": emuSys_signal_2018,
            # "vbfbbtt40": emuSys_signal_2018,
            # "gghbbtt45": emuSys_signal_2018,
            # "vbfbbtt45": emuSys_signal_2018,
            # "gghbbtt50": emuSys_signal_2018,
            # "vbfbbtt50": emuSys_signal_2018,
            # "gghbbtt55": emuSys_signal_2018,
            # "vbfbbtt55": emuSys_signal_2018,
            # "gghbbtt60": emuSys_signal_2018,
            # "vbfbbtt60": emuSys_signal_2018,
            "vbf4b2t-110-15": emuSys_signal_2018,
            "vbf4b2t-100-15": emuSys_signal_2018,
            "vbf4b2t-100-20": emuSys_signal_2018,
            "vbf4b2t-30-15": emuSys_signal_2018,
            "vbf4b2t-40-20": emuSys_signal_2018,
            "vbf4b2t-40-15": emuSys_signal_2018,
            "vbf4b2t-50-15": emuSys_signal_2018,
            "vbf4b2t-50-20": emuSys_signal_2018,
            "vbf4b2t-60-15": emuSys_signal_2018,
            "vbf4b2t-60-20": emuSys_signal_2018,
            "vbf4b2t-60-30": emuSys_signal_2018,
            "vbf4b2t-70-15": emuSys_signal_2018,
            "vbf4b2t-70-20": emuSys_signal_2018,
            "vbf4b2t-70-30": emuSys_signal_2018,
            "vbf4b2t-80-15": emuSys_signal_2018,
            "vbf4b2t-80-20": emuSys_signal_2018,
            "vbf4b2t-80-30": emuSys_signal_2018,
            "vbf4b2t-90-15": emuSys_signal_2018,
            "vbf4b2t-90-20": emuSys_signal_2018,
            "vbf4b2t-90-30": emuSys_signal_2018,
            "ggh4b2t-110-15": emuSys_signal_2018,
            "ggh4b2t-100-15": emuSys_signal_2018,
            "ggh4b2t-100-20": emuSys_signal_2018,
            "ggh4b2t-30-15": emuSys_signal_2018,
            "ggh4b2t-40-20": emuSys_signal_2018,
            "ggh4b2t-40-15": emuSys_signal_2018,
            "ggh4b2t-50-15": emuSys_signal_2018,
            "ggh4b2t-50-20": emuSys_signal_2018,
            "ggh4b2t-60-15": emuSys_signal_2018,
            "ggh4b2t-60-20": emuSys_signal_2018,
            "ggh4b2t-60-30": emuSys_signal_2018,
            "ggh4b2t-70-15": emuSys_signal_2018,
            "ggh4b2t-70-20": emuSys_signal_2018,
            "ggh4b2t-70-30": emuSys_signal_2018,
            "ggh4b2t-80-15": emuSys_signal_2018,
            "ggh4b2t-80-20": emuSys_signal_2018,
            "ggh4b2t-80-30": emuSys_signal_2018,
            "ggh4b2t-90-15": emuSys_signal_2018,
            "ggh4b2t-90-20": emuSys_signal_2018,
            "ggh4b2t-90-30": emuSys_signal_2018,
            "vbf2b2t-20-15": emuSys_signal_2018,
            "vbf2b2t-30-15": emuSys_signal_2018,
            "vbf2b2t-30-20": emuSys_signal_2018,
            "vbf2b2t-40-20": emuSys_signal_2018,
            "vbf2b2t-40-30": emuSys_signal_2018,
            "vbf2b2t-50-30": emuSys_signal_2018,
            "vbf2b2t-50-40": emuSys_signal_2018,
            "vbf2b2t-60-30": emuSys_signal_2018,
            "vbf2b2t-60-40": emuSys_signal_2018,
            "vbf2b2t-60-50": emuSys_signal_2018,
            "vbf2b2t-70-40": emuSys_signal_2018,
            "vbf2b2t-70-50": emuSys_signal_2018,
            "vbf2b2t-80-40": emuSys_signal_2018,
            "ggh2b2t-20-15": emuSys_signal_2018,
            "ggh2b2t-30-15": emuSys_signal_2018,
            "ggh2b2t-30-20": emuSys_signal_2018,
            "ggh2b2t-40-20": emuSys_signal_2018,
            "ggh2b2t-40-30": emuSys_signal_2018,
            "ggh2b2t-50-30": emuSys_signal_2018,
            "ggh2b2t-50-40": emuSys_signal_2018,
            "ggh2b2t-60-30": emuSys_signal_2018,
            "ggh2b2t-60-40": emuSys_signal_2018,
            "ggh2b2t-60-50": emuSys_signal_2018,
            "ggh2b2t-70-40": emuSys_signal_2018,
            "ggh2b2t-70-50": emuSys_signal_2018,
            "ggh2b2t-80-40": emuSys_signal_2018,
        },
    },
    "2017": {
        "mutau": {
            "MC": mutauSys_MC_2017,
            "data_obs": [],
            "embedded": [s + "_2017" for s in muSysEmbed]
            + [s + "_2017" for s in tauSysEmbed]
            + [s + "_2017" for s in embed_trackSys]
            + [s + "_2017" for s in mutau_trgeffSys_Embed],
            "fake": [],
            "vbf4b2t-110-15": mutauSys_MC_2017,
            "vbf4b2t-100-15": mutauSys_MC_2017,
            "vbf4b2t-100-20": mutauSys_MC_2017,
            "vbf4b2t-30-15": mutauSys_MC_2017,
            "vbf4b2t-40-20": mutauSys_MC_2017,
            "vbf4b2t-40-15": mutauSys_MC_2017,
            "vbf4b2t-50-15": mutauSys_MC_2017,
            "vbf4b2t-50-20": mutauSys_MC_2017,
            "vbf4b2t-60-15": mutauSys_MC_2017,
            "vbf4b2t-60-20": mutauSys_MC_2017,
            "vbf4b2t-60-30": mutauSys_MC_2017,
            "vbf4b2t-70-15": mutauSys_MC_2017,
            "vbf4b2t-70-20": mutauSys_MC_2017,
            "vbf4b2t-70-30": mutauSys_MC_2017,
            "vbf4b2t-80-15": mutauSys_MC_2017,
            "vbf4b2t-80-20": mutauSys_MC_2017,
            "vbf4b2t-80-30": mutauSys_MC_2017,
            "vbf4b2t-90-15": mutauSys_MC_2017,
            "vbf4b2t-90-20": mutauSys_MC_2017,
            "vbf4b2t-90-30": mutauSys_MC_2017,
            "ggh4b2t-110-15": mutauSys_MC_2017,
            "ggh4b2t-100-15": mutauSys_MC_2017,
            "ggh4b2t-100-20": mutauSys_MC_2017,
            "ggh4b2t-30-15": mutauSys_MC_2017,
            "ggh4b2t-40-20": mutauSys_MC_2017,
            "ggh4b2t-40-15": mutauSys_MC_2017,
            "ggh4b2t-50-15": mutauSys_MC_2017,
            "ggh4b2t-50-20": mutauSys_MC_2017,
            "ggh4b2t-60-15": mutauSys_MC_2017,
            "ggh4b2t-60-20": mutauSys_MC_2017,
            "ggh4b2t-60-30": mutauSys_MC_2017,
            "ggh4b2t-70-15": mutauSys_MC_2017,
            "ggh4b2t-70-20": mutauSys_MC_2017,
            "ggh4b2t-70-30": mutauSys_MC_2017,
            "ggh4b2t-80-15": mutauSys_MC_2017,
            "ggh4b2t-80-20": mutauSys_MC_2017,
            "ggh4b2t-80-30": mutauSys_MC_2017,
            "ggh4b2t-90-15": mutauSys_MC_2017,
            "ggh4b2t-90-20": mutauSys_MC_2017,
            "ggh4b2t-90-30": mutauSys_MC_2017,
            "vbf2b2t-20-15": mutauSys_MC_2017,
            "vbf2b2t-30-15": mutauSys_MC_2017,
            "vbf2b2t-30-20": mutauSys_MC_2017,
            "vbf2b2t-40-20": mutauSys_MC_2017,
            "vbf2b2t-40-30": mutauSys_MC_2017,
            "vbf2b2t-50-30": mutauSys_MC_2017,
            "vbf2b2t-50-40": mutauSys_MC_2017,
            "vbf2b2t-60-30": mutauSys_MC_2017,
            "vbf2b2t-60-40": mutauSys_MC_2017,
            "vbf2b2t-60-50": mutauSys_MC_2017,
            "vbf2b2t-70-40": mutauSys_MC_2017,
            "vbf2b2t-70-50": mutauSys_MC_2017,
            "vbf2b2t-80-40": mutauSys_MC_2017,
            "ggh2b2t-20-15": mutauSys_MC_2017,
            "ggh2b2t-30-15": mutauSys_MC_2017,
            "ggh2b2t-30-20": mutauSys_MC_2017,
            "ggh2b2t-40-20": mutauSys_MC_2017,
            "ggh2b2t-40-30": mutauSys_MC_2017,
            "ggh2b2t-50-30": mutauSys_MC_2017,
            "ggh2b2t-50-40": mutauSys_MC_2017,
            "ggh2b2t-60-30": mutauSys_MC_2017,
            "ggh2b2t-60-40": mutauSys_MC_2017,
            "ggh2b2t-60-50": mutauSys_MC_2017,
            "ggh2b2t-70-40": mutauSys_MC_2017,
            "ggh2b2t-70-50": mutauSys_MC_2017,
            "ggh2b2t-80-40": mutauSys_MC_2017,
        },
        "etau": {
            "MC": etauSys_MC_2017,
            "data_obs": [],
            "embedded": [s + "_2017" for s in eleSysEmbed]
            + [s + "_2017" for s in tauSysEmbed]
            + [s + "_2017" for s in embed_trackSys]
            + [s + "_2017" for s in etau_trgeffSys_Embed]
            + [s + "_2017" for s in etau_tauidWP],
            "fake": [],
            # "gghbbtt15": etauSys_MC_2017,
            # "vbfbbtt15": etauSys_MC_2017,
            # "gghbbtt20": etauSys_MC_2017,
            # "vbfbbtt20": etauSys_MC_2017,
            # "gghbbtt25": etauSys_MC_2017,
            # "vbfbbtt25": etauSys_MC_2017,
            # "gghbbtt30": etauSys_MC_2017,
            # "vbfbbtt30": etauSys_MC_2017,
            # "gghbbtt35": etauSys_MC_2017,
            # "vbfbbtt35": etauSys_MC_2017,
            # "gghbbtt40": etauSys_MC_2017,
            # "vbfbbtt40": etauSys_MC_2017,
            # "gghbbtt45": etauSys_MC_2017,
            # "vbfbbtt45": etauSys_MC_2017,
            # "gghbbtt50": etauSys_MC_2017,
            # "vbfbbtt50": etauSys_MC_2017,
            # "gghbbtt55": etauSys_MC_2017,
            # "vbfbbtt55": etauSys_MC_2017,
            # "gghbbtt60": etauSys_MC_2017,
            # "vbfbbtt60": etauSys_MC_2017,
            "vbf4b2t-110-15": etauSys_MC_2017,
            "vbf4b2t-100-15": etauSys_MC_2017,
            "vbf4b2t-100-20": etauSys_MC_2017,
            "vbf4b2t-30-15": etauSys_MC_2017,
            "vbf4b2t-40-20": etauSys_MC_2017,
            "vbf4b2t-40-15": etauSys_MC_2017,
            "vbf4b2t-50-15": etauSys_MC_2017,
            "vbf4b2t-50-20": etauSys_MC_2017,
            "vbf4b2t-60-15": etauSys_MC_2017,
            "vbf4b2t-60-20": etauSys_MC_2017,
            "vbf4b2t-60-30": etauSys_MC_2017,
            "vbf4b2t-70-15": etauSys_MC_2017,
            "vbf4b2t-70-20": etauSys_MC_2017,
            "vbf4b2t-70-30": etauSys_MC_2017,
            "vbf4b2t-80-15": etauSys_MC_2017,
            "vbf4b2t-80-20": etauSys_MC_2017,
            "vbf4b2t-80-30": etauSys_MC_2017,
            "vbf4b2t-90-15": etauSys_MC_2017,
            "vbf4b2t-90-20": etauSys_MC_2017,
            "vbf4b2t-90-30": etauSys_MC_2017,
            "ggh4b2t-110-15": etauSys_MC_2017,
            "ggh4b2t-100-15": etauSys_MC_2017,
            "ggh4b2t-100-20": etauSys_MC_2017,
            "ggh4b2t-30-15": etauSys_MC_2017,
            "ggh4b2t-40-20": etauSys_MC_2017,
            "ggh4b2t-40-15": etauSys_MC_2017,
            "ggh4b2t-50-15": etauSys_MC_2017,
            "ggh4b2t-50-20": etauSys_MC_2017,
            "ggh4b2t-60-15": etauSys_MC_2017,
            "ggh4b2t-60-20": etauSys_MC_2017,
            "ggh4b2t-60-30": etauSys_MC_2017,
            "ggh4b2t-70-15": etauSys_MC_2017,
            "ggh4b2t-70-20": etauSys_MC_2017,
            "ggh4b2t-70-30": etauSys_MC_2017,
            "ggh4b2t-80-15": etauSys_MC_2017,
            "ggh4b2t-80-20": etauSys_MC_2017,
            "ggh4b2t-80-30": etauSys_MC_2017,
            "ggh4b2t-90-15": etauSys_MC_2017,
            "ggh4b2t-90-20": etauSys_MC_2017,
            "ggh4b2t-90-30": etauSys_MC_2017,
            "vbf2b2t-20-15": etauSys_MC_2017,
            "vbf2b2t-30-15": etauSys_MC_2017,
            "vbf2b2t-30-20": etauSys_MC_2017,
            "vbf2b2t-40-20": etauSys_MC_2017,
            "vbf2b2t-40-30": etauSys_MC_2017,
            "vbf2b2t-50-30": etauSys_MC_2017,
            "vbf2b2t-50-40": etauSys_MC_2017,
            "vbf2b2t-60-30": etauSys_MC_2017,
            "vbf2b2t-60-40": etauSys_MC_2017,
            "vbf2b2t-60-50": etauSys_MC_2017,
            "vbf2b2t-70-40": etauSys_MC_2017,
            "vbf2b2t-70-50": etauSys_MC_2017,
            "vbf2b2t-80-40": etauSys_MC_2017,
            "ggh2b2t-20-15": etauSys_MC_2017,
            "ggh2b2t-30-15": etauSys_MC_2017,
            "ggh2b2t-30-20": etauSys_MC_2017,
            "ggh2b2t-40-20": etauSys_MC_2017,
            "ggh2b2t-40-30": etauSys_MC_2017,
            "ggh2b2t-50-30": etauSys_MC_2017,
            "ggh2b2t-50-40": etauSys_MC_2017,
            "ggh2b2t-60-30": etauSys_MC_2017,
            "ggh2b2t-60-40": etauSys_MC_2017,
            "ggh2b2t-60-50": etauSys_MC_2017,
            "ggh2b2t-70-40": etauSys_MC_2017,
            "ggh2b2t-70-50": etauSys_MC_2017,
            "ggh2b2t-80-40": etauSys_MC_2017,
        },
        "emu": {
            "MC": emuSys_MC_2017,
            "data_obs": [],
            "embedded": [s + "_2017" for s in eleSysEmbed]
            + [s + "_2017" for s in muSysEmbed]
            + [s + "_2017" for s in emu_trgeffSys_Embed]
            + [s + "_2017" for s in emu_qcdSys],
            "qcd": [],
            "vbf4b2t-110-15": emuSys_signal_2017,
            "vbf4b2t-100-15": emuSys_signal_2017,
            "vbf4b2t-100-20": emuSys_signal_2017,
            "vbf4b2t-30-15": emuSys_signal_2017,
            "vbf4b2t-40-20": emuSys_signal_2017,
            "vbf4b2t-40-15": emuSys_signal_2017,
            "vbf4b2t-50-15": emuSys_signal_2017,
            "vbf4b2t-50-20": emuSys_signal_2017,
            "vbf4b2t-60-15": emuSys_signal_2017,
            "vbf4b2t-60-20": emuSys_signal_2017,
            "vbf4b2t-60-30": emuSys_signal_2017,
            "vbf4b2t-70-15": emuSys_signal_2017,
            "vbf4b2t-70-20": emuSys_signal_2017,
            "vbf4b2t-70-30": emuSys_signal_2017,
            "vbf4b2t-80-15": emuSys_signal_2017,
            "vbf4b2t-80-20": emuSys_signal_2017,
            "vbf4b2t-80-30": emuSys_signal_2017,
            "vbf4b2t-90-15": emuSys_signal_2017,
            "vbf4b2t-90-20": emuSys_signal_2017,
            "vbf4b2t-90-30": emuSys_signal_2017,
            "ggh4b2t-110-15": emuSys_signal_2017,
            "ggh4b2t-100-15": emuSys_signal_2017,
            "ggh4b2t-100-20": emuSys_signal_2017,
            "ggh4b2t-30-15": emuSys_signal_2017,
            "ggh4b2t-40-20": emuSys_signal_2017,
            "ggh4b2t-40-15": emuSys_signal_2017,
            "ggh4b2t-50-15": emuSys_signal_2017,
            "ggh4b2t-50-20": emuSys_signal_2017,
            "ggh4b2t-60-15": emuSys_signal_2017,
            "ggh4b2t-60-20": emuSys_signal_2017,
            "ggh4b2t-60-30": emuSys_signal_2017,
            "ggh4b2t-70-15": emuSys_signal_2017,
            "ggh4b2t-70-20": emuSys_signal_2017,
            "ggh4b2t-70-30": emuSys_signal_2017,
            "ggh4b2t-80-15": emuSys_signal_2017,
            "ggh4b2t-80-20": emuSys_signal_2017,
            "ggh4b2t-80-30": emuSys_signal_2017,
            "ggh4b2t-90-15": emuSys_signal_2017,
            "ggh4b2t-90-20": emuSys_signal_2017,
            "ggh4b2t-90-30": emuSys_signal_2017,
            "vbf2b2t-20-15": emuSys_signal_2017,
            "vbf2b2t-30-15": emuSys_signal_2017,
            "vbf2b2t-30-20": emuSys_signal_2017,
            "vbf2b2t-40-20": emuSys_signal_2017,
            "vbf2b2t-40-30": emuSys_signal_2017,
            "vbf2b2t-50-30": emuSys_signal_2017,
            "vbf2b2t-50-40": emuSys_signal_2017,
            "vbf2b2t-60-30": emuSys_signal_2017,
            "vbf2b2t-60-40": emuSys_signal_2017,
            "vbf2b2t-60-50": emuSys_signal_2017,
            "vbf2b2t-70-40": emuSys_signal_2017,
            "vbf2b2t-70-50": emuSys_signal_2017,
            "vbf2b2t-80-40": emuSys_signal_2017,
            "ggh2b2t-20-15": emuSys_signal_2017,
            "ggh2b2t-30-15": emuSys_signal_2017,
            "ggh2b2t-30-20": emuSys_signal_2017,
            "ggh2b2t-40-20": emuSys_signal_2017,
            "ggh2b2t-40-30": emuSys_signal_2017,
            "ggh2b2t-50-30": emuSys_signal_2017,
            "ggh2b2t-50-40": emuSys_signal_2017,
            "ggh2b2t-60-30": emuSys_signal_2017,
            "ggh2b2t-60-40": emuSys_signal_2017,
            "ggh2b2t-60-50": emuSys_signal_2017,
            "ggh2b2t-70-40": emuSys_signal_2017,
            "ggh2b2t-70-50": emuSys_signal_2017,
            "ggh2b2t-80-40": emuSys_signal_2017,
        },
    },
    "2016preVFP": {
        "mutau": {
            "MC": mutauSys_MC_2016,
            "data_obs": [],
            "embedded": [s + "_2016" for s in muSysEmbed]
            + [s + "_2016" for s in tauSysEmbed]
            + [s + "_2016" for s in embed_trackSys]
            + [s + "_2016" for s in mutau_trgeffSys_Embed],
            "fake": [],
            "vbf4b2t-110-15": mutauSys_MC_2016,
            "vbf4b2t-100-15": mutauSys_MC_2016,
            "vbf4b2t-100-20": mutauSys_MC_2016,
            "vbf4b2t-30-15": mutauSys_MC_2016,
            "vbf4b2t-40-20": mutauSys_MC_2016,
            "vbf4b2t-40-15": mutauSys_MC_2016,
            "vbf4b2t-50-15": mutauSys_MC_2016,
            "vbf4b2t-50-20": mutauSys_MC_2016,
            "vbf4b2t-60-15": mutauSys_MC_2016,
            "vbf4b2t-60-20": mutauSys_MC_2016,
            "vbf4b2t-60-30": mutauSys_MC_2016,
            "vbf4b2t-70-15": mutauSys_MC_2016,
            "vbf4b2t-70-20": mutauSys_MC_2016,
            "vbf4b2t-70-30": mutauSys_MC_2016,
            "vbf4b2t-80-15": mutauSys_MC_2016,
            "vbf4b2t-80-20": mutauSys_MC_2016,
            "vbf4b2t-80-30": mutauSys_MC_2016,
            "vbf4b2t-90-15": mutauSys_MC_2016,
            "vbf4b2t-90-20": mutauSys_MC_2016,
            "vbf4b2t-90-30": mutauSys_MC_2016,
            "ggh4b2t-110-15": mutauSys_MC_2016,
            "ggh4b2t-100-15": mutauSys_MC_2016,
            "ggh4b2t-100-20": mutauSys_MC_2016,
            "ggh4b2t-30-15": mutauSys_MC_2016,
            "ggh4b2t-40-20": mutauSys_MC_2016,
            "ggh4b2t-40-15": mutauSys_MC_2016,
            "ggh4b2t-50-15": mutauSys_MC_2016,
            "ggh4b2t-50-20": mutauSys_MC_2016,
            "ggh4b2t-60-15": mutauSys_MC_2016,
            "ggh4b2t-60-20": mutauSys_MC_2016,
            "ggh4b2t-60-30": mutauSys_MC_2016,
            "ggh4b2t-70-15": mutauSys_MC_2016,
            "ggh4b2t-70-20": mutauSys_MC_2016,
            "ggh4b2t-70-30": mutauSys_MC_2016,
            "ggh4b2t-80-15": mutauSys_MC_2016,
            "ggh4b2t-80-20": mutauSys_MC_2016,
            "ggh4b2t-80-30": mutauSys_MC_2016,
            "ggh4b2t-90-15": mutauSys_MC_2016,
            "ggh4b2t-90-20": mutauSys_MC_2016,
            "ggh4b2t-90-30": mutauSys_MC_2016,
            "vbf2b2t-20-15": mutauSys_MC_2016,
            "vbf2b2t-30-15": mutauSys_MC_2016,
            "vbf2b2t-30-20": mutauSys_MC_2016,
            "vbf2b2t-40-20": mutauSys_MC_2016,
            "vbf2b2t-40-30": mutauSys_MC_2016,
            "vbf2b2t-50-30": mutauSys_MC_2016,
            "vbf2b2t-50-40": mutauSys_MC_2016,
            "vbf2b2t-60-30": mutauSys_MC_2016,
            "vbf2b2t-60-40": mutauSys_MC_2016,
            "vbf2b2t-60-50": mutauSys_MC_2016,
            "vbf2b2t-70-40": mutauSys_MC_2016,
            "vbf2b2t-70-50": mutauSys_MC_2016,
            "vbf2b2t-80-40": mutauSys_MC_2016,
            "ggh2b2t-20-15": mutauSys_MC_2016,
            "ggh2b2t-30-15": mutauSys_MC_2016,
            "ggh2b2t-30-20": mutauSys_MC_2016,
            "ggh2b2t-40-20": mutauSys_MC_2016,
            "ggh2b2t-40-30": mutauSys_MC_2016,
            "ggh2b2t-50-30": mutauSys_MC_2016,
            "ggh2b2t-50-40": mutauSys_MC_2016,
            "ggh2b2t-60-30": mutauSys_MC_2016,
            "ggh2b2t-60-40": mutauSys_MC_2016,
            "ggh2b2t-60-50": mutauSys_MC_2016,
            "ggh2b2t-70-40": mutauSys_MC_2016,
            "ggh2b2t-70-50": mutauSys_MC_2016,
            "ggh2b2t-80-40": mutauSys_MC_2016,
        },
        "etau": {
            "MC": etauSys_MC_2016,
            "data_obs": [],
            "embedded": [s + "_2016" for s in eleSysEmbed]
            + [s + "_2016" for s in tauSysEmbed]
            + [s + "_2016" for s in embed_trackSys]
            + [s + "_2016" for s in etau_trgeffSys_Embed]
            + [s + "_2016" for s in etau_tauidWP],
            "fake": [],
            "vbf4b2t-110-15": etauSys_MC_2016,
            "vbf4b2t-100-15": etauSys_MC_2016,
            "vbf4b2t-100-20": etauSys_MC_2016,
            "vbf4b2t-30-15": etauSys_MC_2016,
            "vbf4b2t-40-20": etauSys_MC_2016,
            "vbf4b2t-40-15": etauSys_MC_2016,
            "vbf4b2t-50-15": etauSys_MC_2016,
            "vbf4b2t-50-20": etauSys_MC_2016,
            "vbf4b2t-60-15": etauSys_MC_2016,
            "vbf4b2t-60-20": etauSys_MC_2016,
            "vbf4b2t-60-30": etauSys_MC_2016,
            "vbf4b2t-70-15": etauSys_MC_2016,
            "vbf4b2t-70-20": etauSys_MC_2016,
            "vbf4b2t-70-30": etauSys_MC_2016,
            "vbf4b2t-80-15": etauSys_MC_2016,
            "vbf4b2t-80-20": etauSys_MC_2016,
            "vbf4b2t-80-30": etauSys_MC_2016,
            "vbf4b2t-90-15": etauSys_MC_2016,
            "vbf4b2t-90-20": etauSys_MC_2016,
            "vbf4b2t-90-30": etauSys_MC_2016,
            "ggh4b2t-110-15": etauSys_MC_2016,
            "ggh4b2t-100-15": etauSys_MC_2016,
            "ggh4b2t-100-20": etauSys_MC_2016,
            "ggh4b2t-30-15": etauSys_MC_2016,
            "ggh4b2t-40-20": etauSys_MC_2016,
            "ggh4b2t-40-15": etauSys_MC_2016,
            "ggh4b2t-50-15": etauSys_MC_2016,
            "ggh4b2t-50-20": etauSys_MC_2016,
            "ggh4b2t-60-15": etauSys_MC_2016,
            "ggh4b2t-60-20": etauSys_MC_2016,
            "ggh4b2t-60-30": etauSys_MC_2016,
            "ggh4b2t-70-15": etauSys_MC_2016,
            "ggh4b2t-70-20": etauSys_MC_2016,
            "ggh4b2t-70-30": etauSys_MC_2016,
            "ggh4b2t-80-15": etauSys_MC_2016,
            "ggh4b2t-80-20": etauSys_MC_2016,
            "ggh4b2t-80-30": etauSys_MC_2016,
            "ggh4b2t-90-15": etauSys_MC_2016,
            "ggh4b2t-90-20": etauSys_MC_2016,
            "ggh4b2t-90-30": etauSys_MC_2016,
            "vbf2b2t-20-15": etauSys_MC_2016,
            "vbf2b2t-30-15": etauSys_MC_2016,
            "vbf2b2t-30-20": etauSys_MC_2016,
            "vbf2b2t-40-20": etauSys_MC_2016,
            "vbf2b2t-40-30": etauSys_MC_2016,
            "vbf2b2t-50-30": etauSys_MC_2016,
            "vbf2b2t-50-40": etauSys_MC_2016,
            "vbf2b2t-60-30": etauSys_MC_2016,
            "vbf2b2t-60-40": etauSys_MC_2016,
            "vbf2b2t-60-50": etauSys_MC_2016,
            "vbf2b2t-70-40": etauSys_MC_2016,
            "vbf2b2t-70-50": etauSys_MC_2016,
            "vbf2b2t-80-40": etauSys_MC_2016,
            "ggh2b2t-20-15": etauSys_MC_2016,
            "ggh2b2t-30-15": etauSys_MC_2016,
            "ggh2b2t-30-20": etauSys_MC_2016,
            "ggh2b2t-40-20": etauSys_MC_2016,
            "ggh2b2t-40-30": etauSys_MC_2016,
            "ggh2b2t-50-30": etauSys_MC_2016,
            "ggh2b2t-50-40": etauSys_MC_2016,
            "ggh2b2t-60-30": etauSys_MC_2016,
            "ggh2b2t-60-40": etauSys_MC_2016,
            "ggh2b2t-60-50": etauSys_MC_2016,
            "ggh2b2t-70-40": etauSys_MC_2016,
            "ggh2b2t-70-50": etauSys_MC_2016,
            "ggh2b2t-80-40": etauSys_MC_2016,
        },
        "emu": {
            "MC": emuSys_MC_2016,
            "data_obs": [],
            "embedded": [s + "_2016" for s in eleSysEmbed]
            + [s + "_2016" for s in muSysEmbed]
            + [s + "_2016" for s in emu_trgeffSys_Embed]
            + [s + "_2016" for s in emu_qcdSys],
            "qcd": [],
            "vbf4b2t-110-15": emuSys_signal_2016,
            "vbf4b2t-100-15": emuSys_signal_2016,
            "vbf4b2t-100-20": emuSys_signal_2016,
            "vbf4b2t-30-15": emuSys_signal_2016,
            "vbf4b2t-40-20": emuSys_signal_2016,
            "vbf4b2t-40-15": emuSys_signal_2016,
            "vbf4b2t-50-15": emuSys_signal_2016,
            "vbf4b2t-50-20": emuSys_signal_2016,
            "vbf4b2t-60-15": emuSys_signal_2016,
            "vbf4b2t-60-20": emuSys_signal_2016,
            "vbf4b2t-60-30": emuSys_signal_2016,
            "vbf4b2t-70-15": emuSys_signal_2016,
            "vbf4b2t-70-20": emuSys_signal_2016,
            "vbf4b2t-70-30": emuSys_signal_2016,
            "vbf4b2t-80-15": emuSys_signal_2016,
            "vbf4b2t-80-20": emuSys_signal_2016,
            "vbf4b2t-80-30": emuSys_signal_2016,
            "vbf4b2t-90-15": emuSys_signal_2016,
            "vbf4b2t-90-20": emuSys_signal_2016,
            "vbf4b2t-90-30": emuSys_signal_2016,
            "ggh4b2t-110-15": emuSys_signal_2016,
            "ggh4b2t-100-15": emuSys_signal_2016,
            "ggh4b2t-100-20": emuSys_signal_2016,
            "ggh4b2t-30-15": emuSys_signal_2016,
            "ggh4b2t-40-20": emuSys_signal_2016,
            "ggh4b2t-40-15": emuSys_signal_2016,
            "ggh4b2t-50-15": emuSys_signal_2016,
            "ggh4b2t-50-20": emuSys_signal_2016,
            "ggh4b2t-60-15": emuSys_signal_2016,
            "ggh4b2t-60-20": emuSys_signal_2016,
            "ggh4b2t-60-30": emuSys_signal_2016,
            "ggh4b2t-70-15": emuSys_signal_2016,
            "ggh4b2t-70-20": emuSys_signal_2016,
            "ggh4b2t-70-30": emuSys_signal_2016,
            "ggh4b2t-80-15": emuSys_signal_2016,
            "ggh4b2t-80-20": emuSys_signal_2016,
            "ggh4b2t-80-30": emuSys_signal_2016,
            "ggh4b2t-90-15": emuSys_signal_2016,
            "ggh4b2t-90-20": emuSys_signal_2016,
            "ggh4b2t-90-30": emuSys_signal_2016,
            "vbf2b2t-20-15": emuSys_signal_2016,
            "vbf2b2t-30-15": emuSys_signal_2016,
            "vbf2b2t-30-20": emuSys_signal_2016,
            "vbf2b2t-40-20": emuSys_signal_2016,
            "vbf2b2t-40-30": emuSys_signal_2016,
            "vbf2b2t-50-30": emuSys_signal_2016,
            "vbf2b2t-50-40": emuSys_signal_2016,
            "vbf2b2t-60-30": emuSys_signal_2016,
            "vbf2b2t-60-40": emuSys_signal_2016,
            "vbf2b2t-60-50": emuSys_signal_2016,
            "vbf2b2t-70-40": emuSys_signal_2016,
            "vbf2b2t-70-50": emuSys_signal_2016,
            "vbf2b2t-80-40": emuSys_signal_2016,
            "ggh2b2t-20-15": emuSys_signal_2016,
            "ggh2b2t-30-15": emuSys_signal_2016,
            "ggh2b2t-30-20": emuSys_signal_2016,
            "ggh2b2t-40-20": emuSys_signal_2016,
            "ggh2b2t-40-30": emuSys_signal_2016,
            "ggh2b2t-50-30": emuSys_signal_2016,
            "ggh2b2t-50-40": emuSys_signal_2016,
            "ggh2b2t-60-30": emuSys_signal_2016,
            "ggh2b2t-60-40": emuSys_signal_2016,
            "ggh2b2t-60-50": emuSys_signal_2016,
            "ggh2b2t-70-40": emuSys_signal_2016,
            "ggh2b2t-70-50": emuSys_signal_2016,
            "ggh2b2t-80-40": emuSys_signal_2016,
        },
    },
    "2016postVFP": {  # identical to 2016preVFP
        "mutau": {
            "MC": mutauSys_MC_2016,
            "data_obs": [],
            "embedded": [s + "_2016" for s in muSysEmbed]
            + [s + "_2016" for s in tauSysEmbed]
            + [s + "_2016" for s in embed_trackSys]
            + [s + "_2016" for s in mutau_trgeffSys_Embed],
            "fake": [],
            "vbf4b2t-110-15": mutauSys_MC_2016,
            "vbf4b2t-100-15": mutauSys_MC_2016,
            "vbf4b2t-100-20": mutauSys_MC_2016,
            "vbf4b2t-30-15": mutauSys_MC_2016,
            "vbf4b2t-40-20": mutauSys_MC_2016,
            "vbf4b2t-40-15": mutauSys_MC_2016,
            "vbf4b2t-50-15": mutauSys_MC_2016,
            "vbf4b2t-50-20": mutauSys_MC_2016,
            "vbf4b2t-60-15": mutauSys_MC_2016,
            "vbf4b2t-60-20": mutauSys_MC_2016,
            "vbf4b2t-60-30": mutauSys_MC_2016,
            "vbf4b2t-70-15": mutauSys_MC_2016,
            "vbf4b2t-70-20": mutauSys_MC_2016,
            "vbf4b2t-70-30": mutauSys_MC_2016,
            "vbf4b2t-80-15": mutauSys_MC_2016,
            "vbf4b2t-80-20": mutauSys_MC_2016,
            "vbf4b2t-80-30": mutauSys_MC_2016,
            "vbf4b2t-90-15": mutauSys_MC_2016,
            "vbf4b2t-90-20": mutauSys_MC_2016,
            "vbf4b2t-90-30": mutauSys_MC_2016,
            "ggh4b2t-110-15": mutauSys_MC_2016,
            "ggh4b2t-100-15": mutauSys_MC_2016,
            "ggh4b2t-100-20": mutauSys_MC_2016,
            "ggh4b2t-30-15": mutauSys_MC_2016,
            "ggh4b2t-40-20": mutauSys_MC_2016,
            "ggh4b2t-40-15": mutauSys_MC_2016,
            "ggh4b2t-50-15": mutauSys_MC_2016,
            "ggh4b2t-50-20": mutauSys_MC_2016,
            "ggh4b2t-60-15": mutauSys_MC_2016,
            "ggh4b2t-60-20": mutauSys_MC_2016,
            "ggh4b2t-60-30": mutauSys_MC_2016,
            "ggh4b2t-70-15": mutauSys_MC_2016,
            "ggh4b2t-70-20": mutauSys_MC_2016,
            "ggh4b2t-70-30": mutauSys_MC_2016,
            "ggh4b2t-80-15": mutauSys_MC_2016,
            "ggh4b2t-80-20": mutauSys_MC_2016,
            "ggh4b2t-80-30": mutauSys_MC_2016,
            "ggh4b2t-90-15": mutauSys_MC_2016,
            "ggh4b2t-90-20": mutauSys_MC_2016,
            "ggh4b2t-90-30": mutauSys_MC_2016,
            "vbf2b2t-20-15": mutauSys_MC_2016,
            "vbf2b2t-30-15": mutauSys_MC_2016,
            "vbf2b2t-30-20": mutauSys_MC_2016,
            "vbf2b2t-40-20": mutauSys_MC_2016,
            "vbf2b2t-40-30": mutauSys_MC_2016,
            "vbf2b2t-50-30": mutauSys_MC_2016,
            "vbf2b2t-50-40": mutauSys_MC_2016,
            "vbf2b2t-60-30": mutauSys_MC_2016,
            "vbf2b2t-60-40": mutauSys_MC_2016,
            "vbf2b2t-60-50": mutauSys_MC_2016,
            "vbf2b2t-70-40": mutauSys_MC_2016,
            "vbf2b2t-70-50": mutauSys_MC_2016,
            "vbf2b2t-80-40": mutauSys_MC_2016,
            "ggh2b2t-20-15": mutauSys_MC_2016,
            "ggh2b2t-30-15": mutauSys_MC_2016,
            "ggh2b2t-30-20": mutauSys_MC_2016,
            "ggh2b2t-40-20": mutauSys_MC_2016,
            "ggh2b2t-40-30": mutauSys_MC_2016,
            "ggh2b2t-50-30": mutauSys_MC_2016,
            "ggh2b2t-50-40": mutauSys_MC_2016,
            "ggh2b2t-60-30": mutauSys_MC_2016,
            "ggh2b2t-60-40": mutauSys_MC_2016,
            "ggh2b2t-60-50": mutauSys_MC_2016,
            "ggh2b2t-70-40": mutauSys_MC_2016,
            "ggh2b2t-70-50": mutauSys_MC_2016,
            "ggh2b2t-80-40": mutauSys_MC_2016,
        },
        "etau": {
            "MC": etauSys_MC_2016,
            "data_obs": [],
            "embedded": [s + "_2016" for s in eleSysEmbed]
            + [s + "_2016" for s in tauSysEmbed]
            + [s + "_2016" for s in embed_trackSys]
            + [s + "_2016" for s in etau_trgeffSys_Embed]
            + [s + "_2016" for s in etau_tauidWP],
            "fake": [],
            "vbf4b2t-110-15": etauSys_MC_2016,
            "vbf4b2t-100-15": etauSys_MC_2016,
            "vbf4b2t-100-20": etauSys_MC_2016,
            "vbf4b2t-30-15": etauSys_MC_2016,
            "vbf4b2t-40-20": etauSys_MC_2016,
            "vbf4b2t-40-15": etauSys_MC_2016,
            "vbf4b2t-50-15": etauSys_MC_2016,
            "vbf4b2t-50-20": etauSys_MC_2016,
            "vbf4b2t-60-15": etauSys_MC_2016,
            "vbf4b2t-60-20": etauSys_MC_2016,
            "vbf4b2t-60-30": etauSys_MC_2016,
            "vbf4b2t-70-15": etauSys_MC_2016,
            "vbf4b2t-70-20": etauSys_MC_2016,
            "vbf4b2t-70-30": etauSys_MC_2016,
            "vbf4b2t-80-15": etauSys_MC_2016,
            "vbf4b2t-80-20": etauSys_MC_2016,
            "vbf4b2t-80-30": etauSys_MC_2016,
            "vbf4b2t-90-15": etauSys_MC_2016,
            "vbf4b2t-90-20": etauSys_MC_2016,
            "vbf4b2t-90-30": etauSys_MC_2016,
            "ggh4b2t-110-15": etauSys_MC_2016,
            "ggh4b2t-100-15": etauSys_MC_2016,
            "ggh4b2t-100-20": etauSys_MC_2016,
            "ggh4b2t-30-15": etauSys_MC_2016,
            "ggh4b2t-40-20": etauSys_MC_2016,
            "ggh4b2t-40-15": etauSys_MC_2016,
            "ggh4b2t-50-15": etauSys_MC_2016,
            "ggh4b2t-50-20": etauSys_MC_2016,
            "ggh4b2t-60-15": etauSys_MC_2016,
            "ggh4b2t-60-20": etauSys_MC_2016,
            "ggh4b2t-60-30": etauSys_MC_2016,
            "ggh4b2t-70-15": etauSys_MC_2016,
            "ggh4b2t-70-20": etauSys_MC_2016,
            "ggh4b2t-70-30": etauSys_MC_2016,
            "ggh4b2t-80-15": etauSys_MC_2016,
            "ggh4b2t-80-20": etauSys_MC_2016,
            "ggh4b2t-80-30": etauSys_MC_2016,
            "ggh4b2t-90-15": etauSys_MC_2016,
            "ggh4b2t-90-20": etauSys_MC_2016,
            "ggh4b2t-90-30": etauSys_MC_2016,
            "vbf2b2t-20-15": etauSys_MC_2016,
            "vbf2b2t-30-15": etauSys_MC_2016,
            "vbf2b2t-30-20": etauSys_MC_2016,
            "vbf2b2t-40-20": etauSys_MC_2016,
            "vbf2b2t-40-30": etauSys_MC_2016,
            "vbf2b2t-50-30": etauSys_MC_2016,
            "vbf2b2t-50-40": etauSys_MC_2016,
            "vbf2b2t-60-30": etauSys_MC_2016,
            "vbf2b2t-60-40": etauSys_MC_2016,
            "vbf2b2t-60-50": etauSys_MC_2016,
            "vbf2b2t-70-40": etauSys_MC_2016,
            "vbf2b2t-70-50": etauSys_MC_2016,
            "vbf2b2t-80-40": etauSys_MC_2016,
            "ggh2b2t-20-15": etauSys_MC_2016,
            "ggh2b2t-30-15": etauSys_MC_2016,
            "ggh2b2t-30-20": etauSys_MC_2016,
            "ggh2b2t-40-20": etauSys_MC_2016,
            "ggh2b2t-40-30": etauSys_MC_2016,
            "ggh2b2t-50-30": etauSys_MC_2016,
            "ggh2b2t-50-40": etauSys_MC_2016,
            "ggh2b2t-60-30": etauSys_MC_2016,
            "ggh2b2t-60-40": etauSys_MC_2016,
            "ggh2b2t-60-50": etauSys_MC_2016,
            "ggh2b2t-70-40": etauSys_MC_2016,
            "ggh2b2t-70-50": etauSys_MC_2016,
            "ggh2b2t-80-40": etauSys_MC_2016,
        },
        "emu": {
            "MC": emuSys_MC_2016,
            "data_obs": [],
            "embedded": [s + "_2016" for s in eleSysEmbed]
            + [s + "_2016" for s in muSysEmbed]
            + [s + "_2016" for s in emu_trgeffSys_Embed]
            + [s + "_2016" for s in emu_qcdSys],
            "qcd": [],
            "vbf4b2t-110-15": emuSys_signal_2016,
            "vbf4b2t-100-15": emuSys_signal_2016,
            "vbf4b2t-100-20": emuSys_signal_2016,
            "vbf4b2t-30-15": emuSys_signal_2016,
            "vbf4b2t-40-20": emuSys_signal_2016,
            "vbf4b2t-40-15": emuSys_signal_2016,
            "vbf4b2t-50-15": emuSys_signal_2016,
            "vbf4b2t-50-20": emuSys_signal_2016,
            "vbf4b2t-60-15": emuSys_signal_2016,
            "vbf4b2t-60-20": emuSys_signal_2016,
            "vbf4b2t-60-30": emuSys_signal_2016,
            "vbf4b2t-70-15": emuSys_signal_2016,
            "vbf4b2t-70-20": emuSys_signal_2016,
            "vbf4b2t-70-30": emuSys_signal_2016,
            "vbf4b2t-80-15": emuSys_signal_2016,
            "vbf4b2t-80-20": emuSys_signal_2016,
            "vbf4b2t-80-30": emuSys_signal_2016,
            "vbf4b2t-90-15": emuSys_signal_2016,
            "vbf4b2t-90-20": emuSys_signal_2016,
            "vbf4b2t-90-30": emuSys_signal_2016,
            "ggh4b2t-110-15": emuSys_signal_2016,
            "ggh4b2t-100-15": emuSys_signal_2016,
            "ggh4b2t-100-20": emuSys_signal_2016,
            "ggh4b2t-30-15": emuSys_signal_2016,
            "ggh4b2t-40-20": emuSys_signal_2016,
            "ggh4b2t-40-15": emuSys_signal_2016,
            "ggh4b2t-50-15": emuSys_signal_2016,
            "ggh4b2t-50-20": emuSys_signal_2016,
            "ggh4b2t-60-15": emuSys_signal_2016,
            "ggh4b2t-60-20": emuSys_signal_2016,
            "ggh4b2t-60-30": emuSys_signal_2016,
            "ggh4b2t-70-15": emuSys_signal_2016,
            "ggh4b2t-70-20": emuSys_signal_2016,
            "ggh4b2t-70-30": emuSys_signal_2016,
            "ggh4b2t-80-15": emuSys_signal_2016,
            "ggh4b2t-80-20": emuSys_signal_2016,
            "ggh4b2t-80-30": emuSys_signal_2016,
            "ggh4b2t-90-15": emuSys_signal_2016,
            "ggh4b2t-90-20": emuSys_signal_2016,
            "ggh4b2t-90-30": emuSys_signal_2016,
            "vbf2b2t-20-15": emuSys_signal_2016,
            "vbf2b2t-30-15": emuSys_signal_2016,
            "vbf2b2t-30-20": emuSys_signal_2016,
            "vbf2b2t-40-20": emuSys_signal_2016,
            "vbf2b2t-40-30": emuSys_signal_2016,
            "vbf2b2t-50-30": emuSys_signal_2016,
            "vbf2b2t-50-40": emuSys_signal_2016,
            "vbf2b2t-60-30": emuSys_signal_2016,
            "vbf2b2t-60-40": emuSys_signal_2016,
            "vbf2b2t-60-50": emuSys_signal_2016,
            "vbf2b2t-70-40": emuSys_signal_2016,
            "vbf2b2t-70-50": emuSys_signal_2016,
            "vbf2b2t-80-40": emuSys_signal_2016,
            "ggh2b2t-20-15": emuSys_signal_2016,
            "ggh2b2t-30-15": emuSys_signal_2016,
            "ggh2b2t-30-20": emuSys_signal_2016,
            "ggh2b2t-40-20": emuSys_signal_2016,
            "ggh2b2t-40-30": emuSys_signal_2016,
            "ggh2b2t-50-30": emuSys_signal_2016,
            "ggh2b2t-50-40": emuSys_signal_2016,
            "ggh2b2t-60-30": emuSys_signal_2016,
            "ggh2b2t-60-40": emuSys_signal_2016,
            "ggh2b2t-60-50": emuSys_signal_2016,
            "ggh2b2t-70-40": emuSys_signal_2016,
            "ggh2b2t-70-50": emuSys_signal_2016,
            "ggh2b2t-80-40": emuSys_signal_2016,
        },
    },
}
