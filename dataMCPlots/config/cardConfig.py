"""
cardConfig.py

Lists the processes/datasets that go into each histogram in each year's datacards.
Included in stackPlots.py
"""

# Can have overlapping key-value pairs.
allHistsGroups = {
    "2018": {
        "mutau": {
            "MC": [
                "ZJ",
                "ttbar",
                "ST",
                "VV",
                "ggh_htt",
                "ggh_hww",
                "qqh_htt",
                "qqh_hww",
                "Zh_htt",
                "Zh_hww",
                "Wh_htt",
                "Wh_hww",
                "tth",
            ],
            "embedded": ["embedded"],
            "data_obs": ["data_obs"],
            "fake": ["fake"],
            "signal": ["ggh4b2t-100-15", "vbf4b2t-100-15"],
            # List these again
            "ttbar": ["ttbar"],
            "drellYan": ["ZJ"],
        },
        "etau": {  # 2018
            "MC": [
                "ZJ",
                "ttbar",
                "ST",
                "VV",
                "ggh_htt",
                "ggh_hww",
                "qqh_htt",
                "qqh_hww",
                "Zh_htt",
                "Zh_hww",
                "Wh_htt",
                "Wh_hww",
                "tth",
            ],
            "embedded": ["embedded"],
            "data_obs": ["data_obs"],
            "fake": ["fake"],
            "signal": ["ggh4b2t-100-15", "vbf4b2t-100-15"],
            # List these again
            "ttbar": ["ttbar"],
            "drellYan": ["ZJ"],
        },
        "emu": {  #  2018
            "MC": [
                "ZJ",
                "ttbar",
                "ST",
                "VV",
                "WJ",
                "ggh_htt",
                "ggh_hww",
                "qqh_htt",
                "qqh_hww",
                "Zh_htt",
                "Zh_hww",
                "Wh_htt",
                "Wh_hww",
                "tth",
            ],
            "embedded": ["embedded"],
            "data_obs": ["data_obs"],
            "qcd": ["qcd"],
            "signal": ["ggh4b2t-100-15", "vbf4b2t-100-15"],
            # List these again
            "ttbar": ["ttbar"],
            "drellYan": ["ZJ"],
        },
    },
    "2017": {
        "mutau": {
            "MC": [
                "ZJ",
                "ttbar",
                "ST",
                "VV",
                "ggh_htt",
                "ggh_hww",
                "qqh_htt",
                "qqh_hww",
                "Zh_htt",
                "Zh_hww",
                "Wh_htt",
                "Wh_hww",
                "tth",
            ],
            "embedded": ["embedded"],
            "data_obs": ["data_obs"],
            "fake": ["fake"],
            "signal": ["ggh4b2t-100-15", "vbf4b2t-100-15"],
            # List these again
            "ttbar": ["ttbar"],
            "drellYan": ["ZJ"],
        },
        "etau": {  # 2017
            "MC": [
                "ZJ",
                "ttbar",
                "ST",
                "VV",
                "ggh_htt",
                "ggh_hww",
                "qqh_htt",
                "qqh_hww",
                "Zh_htt",
                "Zh_hww",
                "Wh_htt",
                "Wh_hww",
                "tth",
            ],
            "embedded": ["embedded"],
            "data_obs": ["data_obs"],
            "fake": ["fake"],
            "signal": ["ggh4b2t-100-15", "vbf4b2t-100-15"],
            # List these again
            "ttbar": ["ttbar"],
            "drellYan": ["ZJ"],
        },
        "emu": {  #  2017
            "MC": [
                "ZJ",
                "ttbar",
                "ST",
                "VV",
                "WJ",
                "ggh_htt",
                "ggh_hww",
                "qqh_htt",
                "qqh_hww",
                "Zh_htt",
                "Zh_hww",
                "Wh_htt",
                "Wh_hww",
                "tth",
            ],
            "embedded": ["embedded"],
            "data_obs": ["data_obs"],
            "qcd": ["qcd"],
            "signal": ["ggh4b2t-100-15", "vbf4b2t-100-15"],
            # List these again
            "ttbar": ["ttbar"],
            "drellYan": ["ZJ"],
        },
    },
    "2016preVFP": {
        "mutau": {
            "MC": [
                "ZJ",
                "ttbar",
                "ST",
                "VV",
                "ggh_htt",
                "ggh_hww",
                "qqh_htt",
                "qqh_hww",
                "Zh_htt",
                "Zh_hww",
                "Wh_htt",
                "Wh_hww",
                "tth",
            ],
            "embedded": ["embedded"],
            "data_obs": ["data_obs"],
            "fake": ["fake"],
            "signal": ["ggh4b2t-100-15", "vbf4b2t-100-15"],
            # List these again
            "ttbar": ["ttbar"],
            "drellYan": ["ZJ"],
        },
        "etau": {  # 2016
            "MC": [
                "ZJ",
                "ttbar",
                "ST",
                "VV",
                "ggh_htt",
                "ggh_hww",
                "qqh_htt",
                "qqh_hww",
                "Zh_htt",
                "Zh_hww",
                "Wh_htt",
                "Wh_hww",
                "tth",
            ],
            "embedded": ["embedded"],
            "data_obs": ["data_obs"],
            "fake": ["fake"],
            "signal": ["ggh4b2t-100-15", "vbf4b2t-100-15"],
            # List these again
            "ttbar": ["ttbar"],
            "drellYan": ["ZJ"],
        },
        "emu": {  #  2016
            "MC": [
                "ZJ",
                "ttbar",
                "ST",
                "VV",
                "WJ",
                "ggh_htt",
                "ggh_hww",
                "qqh_htt",
                "qqh_hww",
                "Zh_htt",
                "Zh_hww",
                "Wh_htt",
                "Wh_hww",
                "tth",
            ],
            "embedded": ["embedded"],
            "data_obs": ["data_obs"],
            "qcd": ["qcd"],
            "signal": ["ggh4b2t-100-15", "vbf4b2t-100-15"],
            # List these again
            "ttbar": ["ttbar"],
            "drellYan": ["ZJ"],
        },
    },
    "2016postVFP": {
        "mutau": {
            "MC": [
                "ZJ",
                "ttbar",
                "ST",
                "VV",
                "ggh_htt",
                "ggh_hww",
                "qqh_htt",
                "qqh_hww",
                "Zh_htt",
                "Zh_hww",
                "Wh_htt",
                "Wh_hww",
                "tth",
            ],
            "embedded": ["embedded"],
            "data_obs": ["data_obs"],
            "fake": ["fake"],
            "signal": ["ggh4b2t-100-15", "vbf4b2t-100-15"],
            # List these again
            "ttbar": ["ttbar"],
            "drellYan": ["ZJ"],
        },
        "etau": {  # 2016
            "MC": [
                "ZJ",
                "ttbar",
                "ST",
                "VV",
                "ggh_htt",
                "ggh_hww",
                "qqh_htt",
                "qqh_hww",
                "Zh_htt",
                "Zh_hww",
                "Wh_htt",
                "Wh_hww",
                "tth",
            ],
            "embedded": ["embedded"],
            "data_obs": ["data_obs"],
            "fake": ["fake"],
            "signal": ["ggh4b2t-100-15", "vbf4b2t-100-15"],
            # List these again
            "ttbar": ["ttbar"],
            "drellYan": ["ZJ"],
        },
        "emu": {  #  2016
            "MC": [
                "ZJ",
                "ttbar",
                "ST",
                "VV",
                "WJ",
                "ggh_htt",
                "ggh_hww",
                "qqh_htt",
                "qqh_hww",
                "Zh_htt",
                "Zh_hww",
                "Wh_htt",
                "Wh_hww",
                "tth",
            ],
            "embedded": ["embedded"],
            "data_obs": ["data_obs"],
            "qcd": ["qcd"],
            "signal": ["ggh4b2t-100-15", "vbf4b2t-100-15"],
            # List these again
            "ttbar": ["ttbar"],
            "drellYan": ["ZJ"],
        },
    },
    "2016": {
        "mutau": {
            "MC": [
                "ZJ",
                "ttbar",
                "ST",
                "VV",
                "ggh_htt",
                "ggh_hww",
                "qqh_htt",
                "qqh_hww",
                "Zh_htt",
                "Zh_hww",
                "Wh_htt",
                "Wh_hww",
                "tth",
            ],
            "embedded": ["embedded"],
            "data_obs": ["data_obs"],
            "fake": ["fake"],
            "signal": ["ggh4b2t-100-15", "vbf4b2t-100-15"],
            # List these again
            "ttbar": ["ttbar"],
            "drellYan": ["ZJ"],
        },
        "etau": {  # 2016
            "MC": [
                "ZJ",
                "ttbar",
                "ST",
                "VV",
                "ggh_htt",
                "ggh_hww",
                "qqh_htt",
                "qqh_hww",
                "Zh_htt",
                "Zh_hww",
                "Wh_htt",
                "Wh_hww",
                "tth",
            ],
            "embedded": ["embedded"],
            "data_obs": ["data_obs"],
            "fake": ["fake"],
            "signal": ["ggh4b2t-100-15", "vbf4b2t-100-15"],
            # List these again
            "ttbar": ["ttbar"],
            "drellYan": ["ZJ"],
        },
        "emu": {  #  2016
            "MC": [
                "ZJ",
                "ttbar",
                "ST",
                "VV",
                "WJ",
                "ggh_htt",
                "ggh_hww",
                "qqh_htt",
                "qqh_hww",
                "Zh_htt",
                "Zh_hww",
                "Wh_htt",
                "Wh_hww",
                "tth",
            ],
            "embedded": ["embedded"],
            "data_obs": ["data_obs"],
            "qcd": ["qcd"],
            "signal": ["ggh4b2t-100-15", "vbf4b2t-100-15"],
            # List these again
            "ttbar": ["ttbar"],
            "drellYan": ["ZJ"],
        },
    },
}

muES_sys = [
    # Muon ES
    "CMS_muES_eta0to1p2",
    "CMS_muES_eta1p2to2p1",
    "CMS_muES_eta2p1to2p4",
]

eleES_sys = ["CMS_eleES_bar", "CMS_eleES_end"]

muES_sys_embed = [
    "CMS_EMB_muES_eta0to1p2",
    "CMS_EMB_muES_eta1p2to2p1",
    "CMS_EMB_muES_eta2p1to2p4",
]

eleES_sys_embed = [
    "CMS_EMB_eleES_bar",
    "CMS_EMB_eleES_end",
]

tauES_sys = ["CMS_tauES_dm0", "CMS_tauES_dm1", "CMS_tauES_dm10", "CMS_tauES_dm11"]


eleTES_sys = ["CMS_eleTES_dm0", "CMS_eleTES_dm1"]

muTES_sys = ["CMS_muTES_dm0", "CMS_muTES_dm1"]

tauIDeff_sys = [
    "CMS_tauideff_pt20to25",
    "CMS_tauideff_pt25to30",
    "CMS_tauideff_pt30to35",
    "CMS_tauideff_pt35to40",
    "CMS_tauideff_pt40to500",
    "CMS_tauideff_pt500to1000",
    "CMS_tauideff_ptgt1000",
    "CMS_tauideff_VSe_bar",
    "CMS_tauideff_VSe_end",
    "CMS_tauideff_VSmu_eta0to0p4",
    "CMS_tauideff_VSmu_eta0p4to0p8",
    "CMS_tauideff_VSmu_eta0p8to1p2",
    "CMS_tauideff_VSmu_eta1p2to1p7",
    "CMS_tauideff_VSmu_eta1p7to2p3",
]

tauIDeff_sys_embed = [
    "CMS_EMB_tauideff_pt20to25",
    "CMS_EMB_tauideff_pt25to30",
    "CMS_EMB_tauideff_pt30to35",
    "CMS_EMB_tauideff_pt35to40",
    "CMS_EMB_tauideff_pt40to500",
    "CMS_EMB_tauideff_pt500to1000",
    "CMS_EMB_tauideff_ptgt1000",
]

tauES_sys_embed = [
    # Tau ES
    "CMS_EMB_tauES_dm0",
    "CMS_EMB_tauES_dm1",
    "CMS_EMB_tauES_dm10",
    "CMS_EMB_tauES_dm11",
    # Tau ID efficiency systematics
    "CMS_EMB_tauideff_pt20to25",
    "CMS_EMB_tauideff_pt25to30",
    "CMS_EMB_tauideff_pt30to35",
    "CMS_EMB_tauideff_pt35to40",
    "CMS_EMB_tauideff_pt40to500",
    "CMS_EMB_tauideff_pt500to1000",
    "CMS_EMB_tauideff_ptgt1000",
]

jerSys_2018 = [
    "CMS_JetAbsolute",
    "CMS_JetBBEC1",
    "CMS_JetEC2",
    "CMS_JetFlavorQCD",
    "CMS_JetHF",
    "CMS_JetRelativeBal",
    "CMS_JetRelativeSample",
    "CMS_JetAbsolute_2018",
    "CMS_JetBBEC1_2018",
    "CMS_JetEC2_2018",
    "CMS_JetHF_2018",
    "CMS_JetRelativeSample_2018",
]

jerSys_2017 = [
    "CMS_JetAbsolute",
    "CMS_JetBBEC1",
    "CMS_JetEC2",
    "CMS_JetFlavorQCD",
    "CMS_JetHF",
    "CMS_JetRelativeBal",
    "CMS_JetRelativeSample",
    "CMS_JetAbsolute_2017",
    "CMS_JetBBEC1_2017",
    "CMS_JetEC2_2017",
    "CMS_JetHF_2017",
    "CMS_JetRelativeSample_2017",
]

jerSys_2016 = [
    "CMS_JetAbsolute",
    "CMS_JetBBEC1",
    "CMS_JetEC2",
    "CMS_JetFlavorQCD",
    "CMS_JetHF",
    "CMS_JetRelativeBal",
    "CMS_JetRelativeSample",
    "CMS_JetAbsolute_2016",
    "CMS_JetBBEC1_2016",
    "CMS_JetEC2_2016",
    "CMS_JetHF_2016",
    "CMS_JetRelativeSample_2016",
]


btagSys = [
    "CMS_btagsf_hf",
    "CMS_btagsf_lf",
    "CMS_btagsf_hfstats1",
    "CMS_btagsf_hfstats2",
    "CMS_btagsf_lfstats1",
    "CMS_btagsf_lfstats2",
    "CMS_btagsf_cferr1",
    "CMS_btagsf_cferr2",
]

metSys = [
    "CMS_met_0j_resolution",
    "CMS_met_1j_resolution",
    "CMS_met_gt1j_resolution",
    "CMS_met_0j_response",
    "CMS_met_1j_response",
    "CMS_met_gt1j_response",
    "CMS_UES",
]

jetFakingTauBackgroundSys = [
    "CMS_jetFR_pt0to25",
    "CMS_jetFR_pt25to30",
    "CMS_jetFR_pt30to35",
    "CMS_jetFR_pt35to40",
    "CMS_jetFR_pt40to50",
    "CMS_jetFR_pt50to60",
    "CMS_jetFR_pt60to80",
    "CMS_jetFR_pt80to100",
    "CMS_jetFR_pt100to120",
    "CMS_jetFR_pt120to150",
    "CMS_jetFR_ptgt150",
]


zPtSys = ["CMS_Zpt"]
topPtSys = ["CMS_toppt"]
nonDYSys = ["CMS_nonDY"]

# crosstrg_fakefactor for mutau and etau
crosstrg_fakefactor = ["CMS_crosstrg_fakefactor"]

# trigger efficiency
mutau_trgeffSys = ["CMS_trgeff_single_mt", "CMS_trgeff_cross_mt"]
etau_trgeffSys = ["CMS_trgeff_single_et", "CMS_trgeff_cross_et"]
emu_trgeffSys = ["CMS_trgeff_Mu8E23_em", "CMS_trgeff_Mu23E12_em", "CMS_trgeff_both_em"]

mutau_trgeffSys_embed = ["CMS_EMB_trgeff_single_mt", "CMS_EMB_trgeff_cross_mt"]
etau_trgeffSys_embed = ["CMS_EMB_trgeff_single_et", "CMS_EMB_trgeff_cross_et"]
emu_trgeffSys_embed = [
    "CMS_EMB_trgeff_Mu8E23_em",
    "CMS_EMB_trgeff_Mu23E12_em",
    "CMS_EMB_trgeff_both_em",
]

emu_qcdSys = ["CMS_SSclosure", "CMS_SScorrection", "CMS_SSboth2D", "CMS_osss"]

# Embed only: mutau etau: "tautrack_dm0dm10", "tautrack_dm1", "tautrack_dm11"
embed_trackSys = [
    "CMS_EMB_tautrack_dm0dm10",
    "CMS_EMB_tautrack_dm1",
    "CMS_EMB_tautrack_dm11",
]

# Prefiring: for all years in UL
prefiring_2018 = ["CMS_prefiring_2018"]
prefiring_2017 = ["CMS_prefiring_2017"]
prefiring_2016 = ["CMS_prefiring_2016"]

allShapeSystematics = {
    "2018": {
        "mutau": {
            "prefiring": {
                "systematics": prefiring_2018,
                "affectedProcesses": ["MC", "fake"],
            },
            "TES": {
                "systematics": [s + "_2018" for s in tauES_sys],
                "affectedProcesses": ["MC"],
            },
            "eleTES": {
                "systematics": [s + "_2018" for s in eleTES_sys],
                "affectedProcesses": ["MC"],
            },
            "muTES": {
                "systematics": [s + "_2018" for s in muTES_sys],
                "affectedProcesses": ["MC"],
            },
            "EMB_TES": {
                "systematics": [s + "_2018" for s in tauES_sys_embed],
                "affectedProcesses": ["embedded"],
            },
            # Muon ES
            "muES": {
                "systematics": [s + "_2018" for s in muES_sys],
                "affectedProcesses": ["MC"],
            },
            # Embedded muon ES
            "EMB_muES": {
                "systematics": [s + "_2018" for s in muES_sys_embed],
                "affectedProcesses": ["embedded"],
            },
            "JER": {
                "systematics": jerSys_2018,
                "affectedProcesses": ["MC", "fake"],
            },
            "tauideff": {
                "systematics": [s + "_2018" for s in tauIDeff_sys],
                "affectedProcesses": ["MC"],
            },
            "EMB_tauideff": {
                "systematics": [s + "_2018" for s in tauIDeff_sys_embed],
                "affectedProcesses": ["embedded"],
            },
            "btag": {
                "systematics": [s + "_2018" for s in btagSys],
                "affectedProcesses": ["MC", "fake"],
            },
            "met": {
                "systematics": [s + "_2018" for s in metSys],
                "affectedProcesses": ["MC", "fake"],
            },
            "jetFakingTauBackgroundSys": {
                "systematics": [s + "_2018" for s in jetFakingTauBackgroundSys],
                "affectedProcesses": ["fake"],
            },
            "mutau_trgeffSys": {
                "systematics": [s + "_2018" for s in mutau_trgeffSys],
                "affectedProcesses": ["MC", "fake"],
            },
            "mutau_trgeffSys_embed": {
                "systematics": [s + "_2018" for s in mutau_trgeffSys_embed],
                "affectedProcesses": ["embedded", "fake"],
            },
            "embed_trackSys": {
                "systematics": [s + "_2018" for s in embed_trackSys],
                "affectedProcesses": ["embedded"],
            },
            "zPtSys": {
                "systematics": [s + "_2018" for s in zPtSys],
                "affectedProcesses": ["drellYan", "fake"],
            },
            "topPt": {
                "systematics": [s + "_2018" for s in topPtSys],
                "affectedProcesses": ["ttbar", "fake"],
            },
            "nonDY": {
                "systematics": [s + "_2018" for s in nonDYSys],
                "affectedProcesses": ["embedded"],
            },
        },
        "etau": {
            "prefiring": {
                "systematics": prefiring_2018,
                "affectedProcesses": ["MC", "fake"],
            },
            "TES": {
                "systematics": [s + "_2018" for s in tauES_sys],
                "affectedProcesses": ["MC", "fake"],
            },
            "eleTES": {
                "systematics": [s + "_2018" for s in eleTES_sys],
                "affectedProcesses": ["MC"],
            },
            "muTES": {
                "systematics": [s + "_2018" for s in muTES_sys],
                "affectedProcesses": ["MC"],
            },
            "EMB_TES": {
                "systematics": [s + "_2018" for s in tauES_sys_embed],
                "affectedProcesses": ["embedded"],
            },
            # Ele ES
            "eleES": {
                "systematics": [s + "_2018" for s in eleES_sys],
                "affectedProcesses": ["MC"],
            },
            # Embedded ele ES
            "EMB_eleES": {
                "systematics": [s + "_2018" for s in eleES_sys_embed],
                "affectedProcesses": ["embedded"],
            },
            "JER": {
                "systematics": jerSys_2018,
                "affectedProcesses": ["MC", "fake"],
            },
            "tauideff": {
                "systematics": [s + "_2018" for s in tauIDeff_sys],
                "affectedProcesses": ["MC"],
            },
            "EMB_tauideff": {
                "systematics": [s + "_2018" for s in tauES_sys_embed],
                "affectedProcesses": ["embedded"],
            },
            "btag": {
                "systematics": [s + "_2018" for s in btagSys],
                "affectedProcesses": ["MC", "fake"],
            },
            "met": {
                "systematics": [s + "_2018" for s in metSys],
                "affectedProcesses": ["MC", "fake"],
            },
            "etau_trgeffSys": {
                "systematics": [s + "_2018" for s in etau_trgeffSys],
                "affectedProcesses": ["MC", "fake"],
            },
            "etau_trgeffSys_embed": {
                "systematics": [s + "_2018" for s in etau_trgeffSys_embed],
                "affectedProcesses": ["embedded"],
            },
            "embed_trackSys": {
                "systematics": [s + "_2018" for s in embed_trackSys],
                "affectedProcesses": ["embedded"],
            },
            "zPtSys": {
                "systematics": [s + "_2018" for s in zPtSys],
                "affectedProcesses": ["drellYan", "fake"],
            },
            "topPt": {
                "systematics": [s + "_2018" for s in topPtSys],
                "affectedProcesses": ["ttbar", "fake"],
            },
            "nonDY": {
                "systematics": [s + "_2018" for s in nonDYSys],
                "affectedProcesses": ["embedded"],
            },
        },
        "emu": {
            "prefiring": {
                "systematics": prefiring_2018,
                "affectedProcesses": ["MC", "qcd"],
            },
            # Ele ES
            "eleES": {
                "systematics": [s + "_2018" for s in eleES_sys],
                "affectedProcesses": ["MC"],
            },
            # Embedded ele ES
            "EMB_eleES": {
                "systematics": [s + "_2018" for s in eleES_sys_embed],
                "affectedProcesses": ["embedded"],
            },
            # Muon ES
            "muES": {
                "systematics": [s + "_2018" for s in muES_sys],
                "affectedProcesses": ["MC"],
            },
            # Embedded muon ES
            "EMB_muES": {
                "systematics": [s + "_2018" for s in muES_sys_embed],
                "affectedProcesses": ["embedded"],
            },
            "JER": {
                "systematics": jerSys_2018,
                "affectedProcesses": ["MC", "qcd"],
            },
            "btag": {
                "systematics": [s + "_2018" for s in btagSys],
                "affectedProcesses": ["MC", "qcd"],
            },
            "met": {
                "systematics": [s + "_2018" for s in metSys],
                "affectedProcesses": ["MC", "qcd"],
            },
            "emu_trgeffSys": {
                "systematics": [s + "_2018" for s in emu_trgeffSys],
                "affectedProcesses": ["MC", "qcd"],
            },
            "emu_trgeffSys_embed": {
                "systematics": [s + "_2018" for s in emu_trgeffSys_embed],
                "affectedProcesses": ["embedded", "qcd"],
            },
            "emu_qcdSys": {
                "systematics": [s + "_2018" for s in emu_qcdSys],
                "affectedProcesses": ["qcd"],
            },
            "zPtSys": {
                "systematics": [s + "_2018" for s in zPtSys],
                "affectedProcesses": ["drellYan", "qcd"],
            },
            "topPt": {
                "systematics": [s + "_2018" for s in topPtSys],
                "affectedProcesses": ["ttbar", "qcd"],
            },
            "nonDY": {
                "systematics": [s + "_2018" for s in nonDYSys],
                "affectedProcesses": ["embedded"],
            },
        },
    },
    "2017": {
        "mutau": {
            "prefiring": {
                "systematics": prefiring_2017,
                "affectedProcesses": ["MC", "fake"],
            },
            "TES": {
                "systematics": [s + "_2017" for s in tauES_sys],
                "affectedProcesses": ["MC"],
            },
            "eleTES": {
                "systematics": [s + "_2017" for s in eleTES_sys],
                "affectedProcesses": ["MC"],
            },
            "muTES": {
                "systematics": [s + "_2017" for s in muTES_sys],
                "affectedProcesses": ["MC"],
            },
            "EMB_TES": {
                "systematics": [s + "_2017" for s in tauES_sys_embed],
                "affectedProcesses": ["embedded"],
            },
            # Muon ES
            "muES": {
                "systematics": [s + "_2017" for s in muES_sys],
                "affectedProcesses": ["MC"],
            },
            # Embedded muon ES
            "EMB_muES": {
                "systematics": [s + "_2017" for s in muES_sys_embed],
                "affectedProcesses": ["embedded"],
            },
            "JER": {
                "systematics": jerSys_2017,
                "affectedProcesses": ["MC", "fake"],
            },
            "tauideff": {
                "systematics": [s + "_2017" for s in tauIDeff_sys],
                "affectedProcesses": ["MC"],
            },
            "EMB_tauideff": {
                "systematics": [s + "_2017" for s in tauIDeff_sys_embed],
                "affectedProcesses": ["embedded"],
            },
            "btag": {
                "systematics": [s + "_2017" for s in btagSys],
                "affectedProcesses": ["MC", "fake"],
            },
            "met": {
                "systematics": [s + "_2017" for s in metSys],
                "affectedProcesses": ["MC", "fake"],
            },
            "jetFakingTauBackgroundSys": {
                "systematics": [s + "_2017" for s in jetFakingTauBackgroundSys],
                "affectedProcesses": ["fake"],
            },
            "mutau_trgeffSys": {
                "systematics": [s + "_2017" for s in mutau_trgeffSys],
                "affectedProcesses": ["MC", "fake"],
            },
            "mutau_trgeffSys_embed": {
                "systematics": [s + "_2017" for s in mutau_trgeffSys_embed],
                "affectedProcesses": ["embedded", "fake"],
            },
            "embed_trackSys": {
                "systematics": [s + "_2017" for s in embed_trackSys],
                "affectedProcesses": ["embedded"],
            },
            "zPtSys": {
                "systematics": [s + "_2017" for s in zPtSys],
                "affectedProcesses": ["drellYan", "fake"],
            },
            "topPt": {
                "systematics": [s + "_2017" for s in topPtSys],
                "affectedProcesses": ["ttbar", "fake"],
            },
            "nonDY": {
                "systematics": [s + "_2017" for s in nonDYSys],
                "affectedProcesses": ["embedded"],
            },
        },
        "etau": {
            "prefiring": {
                "systematics": prefiring_2017,
                "affectedProcesses": ["MC", "fake"],
            },
            "TES": {
                "systematics": [s + "_2017" for s in tauES_sys],
                "affectedProcesses": ["MC", "fake"],
            },
            "eleTES": {
                "systematics": [s + "_2017" for s in eleTES_sys],
                "affectedProcesses": ["MC"],
            },
            "muTES": {
                "systematics": [s + "_2017" for s in muTES_sys],
                "affectedProcesses": ["MC"],
            },
            "EMB_TES": {
                "systematics": [s + "_2017" for s in tauES_sys_embed],
                "affectedProcesses": ["embedded"],
            },
            # Ele ES
            "eleES": {
                "systematics": [s + "_2017" for s in eleES_sys],
                "affectedProcesses": ["MC"],
            },
            # Embedded ele ES
            "EMB_eleES": {
                "systematics": [s + "_2017" for s in eleES_sys_embed],
                "affectedProcesses": ["embedded"],
            },
            "JER": {
                "systematics": jerSys_2017,
                "affectedProcesses": ["MC", "fake"],
            },
            "tauideff": {
                "systematics": [s + "_2017" for s in tauIDeff_sys],
                "affectedProcesses": ["MC"],
            },
            "EMB_tauideff": {
                "systematics": [s + "_2017" for s in tauES_sys_embed],
                "affectedProcesses": ["embedded"],
            },
            "btag": {
                "systematics": [s + "_2017" for s in btagSys],
                "affectedProcesses": ["MC", "fake"],
            },
            "met": {
                "systematics": [s + "_2017" for s in metSys],
                "affectedProcesses": ["MC", "fake"],
            },
            "etau_trgeffSys": {
                "systematics": [s + "_2017" for s in etau_trgeffSys],
                "affectedProcesses": ["MC", "fake"],
            },
            "etau_trgeffSys_embed": {
                "systematics": [s + "_2017" for s in etau_trgeffSys_embed],
                "affectedProcesses": ["embedded"],
            },
            "embed_trackSys": {
                "systematics": [s + "_2017" for s in embed_trackSys],
                "affectedProcesses": ["embedded"],
            },
            "zPtSys": {
                "systematics": [s + "_2017" for s in zPtSys],
                "affectedProcesses": ["drellYan", "fake"],
            },
            "topPt": {
                "systematics": [s + "_2017" for s in topPtSys],
                "affectedProcesses": ["ttbar", "fake"],
            },
            "nonDY": {
                "systematics": [s + "_2017" for s in nonDYSys],
                "affectedProcesses": ["embedded"],
            },
        },
        "emu": {
            "prefiring": {
                "systematics": prefiring_2017,
                "affectedProcesses": ["MC", "qcd"],
            },
            # Ele ES
            "eleES": {
                "systematics": [s + "_2017" for s in eleES_sys],
                "affectedProcesses": ["MC"],
            },
            # Embedded ele ES
            "EMB_eleES": {
                "systematics": [s + "_2017" for s in eleES_sys_embed],
                "affectedProcesses": ["embedded"],
            },
            # Muon ES
            "muES": {
                "systematics": [s + "_2017" for s in muES_sys],
                "affectedProcesses": ["MC"],
            },
            # Embedded muon ES
            "EMB_muES": {
                "systematics": [s + "_2017" for s in muES_sys_embed],
                "affectedProcesses": ["embedded"],
            },
            "JER": {
                "systematics": jerSys_2017,
                "affectedProcesses": ["MC", "qcd"],
            },
            "btag": {
                "systematics": [s + "_2017" for s in btagSys],
                "affectedProcesses": ["MC", "qcd"],
            },
            "met": {
                "systematics": [s + "_2017" for s in metSys],
                "affectedProcesses": ["MC", "qcd"],
            },
            "emu_trgeffSys": {
                "systematics": [s + "_2017" for s in emu_trgeffSys],
                "affectedProcesses": ["MC", "qcd"],
            },
            "emu_trgeffSys_embed": {
                "systematics": [s + "_2017" for s in emu_trgeffSys_embed],
                "affectedProcesses": ["embedded", "qcd"],
            },
            "emu_qcdSys": {
                "systematics": [s + "_2017" for s in emu_qcdSys],
                "affectedProcesses": ["qcd"],
            },
            "zPtSys": {
                "systematics": [s + "_2017" for s in zPtSys],
                "affectedProcesses": ["drellYan", "qcd"],
            },
            "topPt": {
                "systematics": [s + "_2017" for s in topPtSys],
                "affectedProcesses": ["ttbar", "qcd"],
            },
        },
    },
    "2016preVFP": {
        "mutau": {
            "prefiring": {
                "systematics": prefiring_2016,
                "affectedProcesses": ["MC", "fake"],
            },
            "TES": {
                "systematics": [s + "_2016" for s in tauES_sys],
                "affectedProcesses": ["MC"],
            },
            "eleTES": {
                "systematics": [s + "_2016" for s in eleTES_sys],
                "affectedProcesses": ["MC"],
            },
            "muTES": {
                "systematics": [s + "_2016" for s in muTES_sys],
                "affectedProcesses": ["MC"],
            },
            "EMB_TES": {
                "systematics": [s + "_2016" for s in tauES_sys_embed],
                "affectedProcesses": ["embedded"],
            },
            # Muon ES
            "muES": {
                "systematics": [s + "_2016" for s in muES_sys],
                "affectedProcesses": ["MC"],
            },
            # Embedded muon ES
            "EMB_muES": {
                "systematics": [s + "_2016" for s in muES_sys_embed],
                "affectedProcesses": ["embedded"],
            },
            "JER": {
                "systematics": jerSys_2016,
                "affectedProcesses": ["MC", "fake"],
            },
            "tauideff": {
                "systematics": [s + "_2016" for s in tauIDeff_sys],
                "affectedProcesses": ["MC"],
            },
            "EMB_tauideff": {
                "systematics": [s + "_2016" for s in tauIDeff_sys_embed],
                "affectedProcesses": ["embedded"],
            },
            "btag": {
                "systematics": [s + "_2016" for s in btagSys],
                "affectedProcesses": ["MC", "fake"],
            },
            "met": {
                "systematics": [s + "_2016" for s in metSys],
                "affectedProcesses": ["MC", "fake"],
            },
            "jetFakingTauBackgroundSys": {
                "systematics": [s + "_2016" for s in jetFakingTauBackgroundSys],
                "affectedProcesses": ["fake"],
            },
            "mutau_trgeffSys": {
                "systematics": [s + "_2016" for s in mutau_trgeffSys],
                "affectedProcesses": ["MC", "fake"],
            },
            "mutau_trgeffSys_embed": {
                "systematics": [s + "_2016" for s in mutau_trgeffSys_embed],
                "affectedProcesses": ["embedded", "fake"],
            },
            "embed_trackSys": {
                "systematics": [s + "_2016" for s in embed_trackSys],
                "affectedProcesses": ["embedded"],
            },
            "zPtSys": {
                "systematics": [s + "_2016" for s in zPtSys],
                "affectedProcesses": ["drellYan", "fake"],
            },
            "topPt": {
                "systematics": [s + "_2016" for s in topPtSys],
                "affectedProcesses": ["ttbar", "fake"],
            },
            "nonDY": {
                "systematics": [s + "_2016" for s in nonDYSys],
                "affectedProcesses": ["embedded"],
            },
        },
        "etau": {
            "prefiring": {
                "systematics": prefiring_2016,
                "affectedProcesses": ["MC", "fake"],
            },
            "TES": {
                "systematics": [s + "_2016" for s in tauES_sys],
                "affectedProcesses": ["MC", "fake"],
            },
            "eleTES": {
                "systematics": [s + "_2016" for s in eleTES_sys],
                "affectedProcesses": ["MC"],
            },
            "muTES": {
                "systematics": [s + "_2016" for s in muTES_sys],
                "affectedProcesses": ["MC"],
            },
            "EMB_TES": {
                "systematics": [s + "_2016" for s in tauES_sys_embed],
                "affectedProcesses": ["embedded"],
            },
            # Ele ES
            "eleES": {
                "systematics": [s + "_2016" for s in eleES_sys],
                "affectedProcesses": ["MC"],
            },
            # Embedded ele ES
            "EMB_eleES": {
                "systematics": [s + "_2016" for s in eleES_sys_embed],
                "affectedProcesses": ["embedded"],
            },
            "JER": {
                "systematics": jerSys_2016,
                "affectedProcesses": ["MC", "fake"],
            },
            "tauideff": {
                "systematics": [s + "_2016" for s in tauIDeff_sys],
                "affectedProcesses": ["MC"],
            },
            "EMB_tauideff": {
                "systematics": [s + "_2016" for s in tauES_sys_embed],
                "affectedProcesses": ["embedded"],
            },
            "btag": {
                "systematics": [s + "_2016" for s in btagSys],
                "affectedProcesses": ["MC", "fake"],
            },
            "met": {
                "systematics": [s + "_2016" for s in metSys],
                "affectedProcesses": ["MC", "fake"],
            },
            "etau_trgeffSys": {
                "systematics": [s + "_2016" for s in etau_trgeffSys],
                "affectedProcesses": ["MC", "fake"],
            },
            "etau_trgeffSys_embed": {
                "systematics": [s + "_2016" for s in etau_trgeffSys_embed],
                "affectedProcesses": ["embedded"],
            },
            "embed_trackSys": {
                "systematics": [s + "_2016" for s in embed_trackSys],
                "affectedProcesses": ["embedded"],
            },
            "zPtSys": {
                "systematics": [s + "_2016" for s in zPtSys],
                "affectedProcesses": ["drellYan", "fake"],
            },
            "topPt": {
                "systematics": [s + "_2016" for s in topPtSys],
                "affectedProcesses": ["ttbar", "fake"],
            },
            "nonDY": {
                "systematics": [s + "_2016" for s in nonDYSys],
                "affectedProcesses": ["embedded"],
            },
        },
        "emu": {
            "prefiring": {
                "systematics": prefiring_2016,
                "affectedProcesses": ["MC", "qcd"],
            },
            # Ele ES
            "eleES": {
                "systematics": [s + "_2016" for s in eleES_sys],
                "affectedProcesses": ["MC"],
            },
            # Embedded ele ES
            "EMB_eleES": {
                "systematics": [s + "_2016" for s in eleES_sys_embed],
                "affectedProcesses": ["embedded"],
            },
            # Muon ES
            "muES": {
                "systematics": [s + "_2016" for s in muES_sys],
                "affectedProcesses": ["MC"],
            },
            # Embedded muon ES
            "EMB_muES": {
                "systematics": [s + "_2016" for s in muES_sys_embed],
                "affectedProcesses": ["embedded"],
            },
            "JER": {
                "systematics": jerSys_2016,
                "affectedProcesses": ["MC", "qcd"],
            },
            "btag": {
                "systematics": [s + "_2016" for s in btagSys],
                "affectedProcesses": ["MC", "qcd"],
            },
            "met": {
                "systematics": [s + "_2016" for s in metSys],
                "affectedProcesses": ["MC", "qcd"],
            },
            "emu_trgeffSys": {
                "systematics": [s + "_2016" for s in emu_trgeffSys],
                "affectedProcesses": ["MC", "qcd"],
            },
            "emu_trgeffSys_embed": {
                "systematics": [s + "_2016" for s in emu_trgeffSys_embed],
                "affectedProcesses": ["embedded", "qcd"],
            },
            "emu_qcdSys": {
                "systematics": [s + "_2016" for s in emu_qcdSys],
                "affectedProcesses": ["qcd"],
            },
            "zPtSys": {
                "systematics": [s + "_2016" for s in zPtSys],
                "affectedProcesses": ["drellYan", "qcd"],
            },
            "topPt": {
                "systematics": [s + "_2016" for s in topPtSys],
                "affectedProcesses": ["ttbar", "qcd"],
            },
        },
    },
    "2016postVFP": {
        "mutau": {
            "prefiring": {
                "systematics": prefiring_2016,
                "affectedProcesses": ["MC", "fake"],
            },
            "TES": {
                "systematics": [s + "_2016" for s in tauES_sys],
                "affectedProcesses": ["MC"],
            },
            "eleTES": {
                "systematics": [s + "_2016" for s in eleTES_sys],
                "affectedProcesses": ["MC"],
            },
            "muTES": {
                "systematics": [s + "_2016" for s in muTES_sys],
                "affectedProcesses": ["MC"],
            },
            "EMB_TES": {
                "systematics": [s + "_2016" for s in tauES_sys_embed],
                "affectedProcesses": ["embedded"],
            },
            # Muon ES
            "muES": {
                "systematics": [s + "_2016" for s in muES_sys],
                "affectedProcesses": ["MC"],
            },
            # Embedded muon ES
            "EMB_muES": {
                "systematics": [s + "_2016" for s in muES_sys_embed],
                "affectedProcesses": ["embedded"],
            },
            "JER": {
                "systematics": jerSys_2016,
                "affectedProcesses": ["MC", "fake"],
            },
            "tauideff": {
                "systematics": [s + "_2016" for s in tauIDeff_sys],
                "affectedProcesses": ["MC"],
            },
            "EMB_tauideff": {
                "systematics": [s + "_2016" for s in tauIDeff_sys_embed],
                "affectedProcesses": ["embedded"],
            },
            "btag": {
                "systematics": [s + "_2016" for s in btagSys],
                "affectedProcesses": ["MC", "fake"],
            },
            "met": {
                "systematics": [s + "_2016" for s in metSys],
                "affectedProcesses": ["MC", "fake"],
            },
            "jetFakingTauBackgroundSys": {
                "systematics": [s + "_2016" for s in jetFakingTauBackgroundSys],
                "affectedProcesses": ["fake"],
            },
            "mutau_trgeffSys": {
                "systematics": [s + "_2016" for s in mutau_trgeffSys],
                "affectedProcesses": ["MC", "fake"],
            },
            "mutau_trgeffSys_embed": {
                "systematics": [s + "_2016" for s in mutau_trgeffSys_embed],
                "affectedProcesses": ["embedded", "fake"],
            },
            "embed_trackSys": {
                "systematics": [s + "_2016" for s in embed_trackSys],
                "affectedProcesses": ["embedded"],
            },
            "zPtSys": {
                "systematics": [s + "_2016" for s in zPtSys],
                "affectedProcesses": ["drellYan", "fake"],
            },
            "topPt": {
                "systematics": [s + "_2016" for s in topPtSys],
                "affectedProcesses": ["ttbar", "fake"],
            },
            "nonDY": {
                "systematics": [s + "_2016" for s in nonDYSys],
                "affectedProcesses": ["embedded"],
            },
        },
        "etau": {
            "prefiring": {
                "systematics": prefiring_2016,
                "affectedProcesses": ["MC", "fake"],
            },
            "TES": {
                "systematics": [s + "_2016" for s in tauES_sys],
                "affectedProcesses": ["MC", "fake"],
            },
            "eleTES": {
                "systematics": [s + "_2016" for s in eleTES_sys],
                "affectedProcesses": ["MC"],
            },
            "muTES": {
                "systematics": [s + "_2016" for s in muTES_sys],
                "affectedProcesses": ["MC"],
            },
            "EMB_TES": {
                "systematics": [s + "_2016" for s in tauES_sys_embed],
                "affectedProcesses": ["embedded"],
            },
            # Ele ES
            "eleES": {
                "systematics": [s + "_2016" for s in eleES_sys],
                "affectedProcesses": ["MC"],
            },
            # Embedded ele ES
            "EMB_eleES": {
                "systematics": [s + "_2016" for s in eleES_sys_embed],
                "affectedProcesses": ["embedded"],
            },
            "JER": {
                "systematics": jerSys_2016,
                "affectedProcesses": ["MC", "fake"],
            },
            "tauideff": {
                "systematics": [s + "_2016" for s in tauIDeff_sys],
                "affectedProcesses": ["MC"],
            },
            "EMB_tauideff": {
                "systematics": [s + "_2016" for s in tauES_sys_embed],
                "affectedProcesses": ["embedded"],
            },
            "btag": {
                "systematics": [s + "_2016" for s in btagSys],
                "affectedProcesses": ["MC", "fake"],
            },
            "met": {
                "systematics": [s + "_2016" for s in metSys],
                "affectedProcesses": ["MC", "fake"],
            },
            "etau_trgeffSys": {
                "systematics": [s + "_2016" for s in etau_trgeffSys],
                "affectedProcesses": ["MC", "fake"],
            },
            "etau_trgeffSys_embed": {
                "systematics": [s + "_2016" for s in etau_trgeffSys_embed],
                "affectedProcesses": ["embedded"],
            },
            "embed_trackSys": {
                "systematics": [s + "_2016" for s in embed_trackSys],
                "affectedProcesses": ["embedded"],
            },
            "zPtSys": {
                "systematics": [s + "_2016" for s in zPtSys],
                "affectedProcesses": ["drellYan", "fake"],
            },
            "topPt": {
                "systematics": [s + "_2016" for s in topPtSys],
                "affectedProcesses": ["ttbar", "fake"],
            },
            "nonDY": {
                "systematics": [s + "_2016" for s in nonDYSys],
                "affectedProcesses": ["embedded"],
            },
        },
        "emu": {
            "prefiring": {
                "systematics": prefiring_2016,
                "affectedProcesses": ["MC", "qcd"],
            },
            # Ele ES
            "eleES": {
                "systematics": [s + "_2016" for s in eleES_sys],
                "affectedProcesses": ["MC"],
            },
            # Embedded ele ES
            "EMB_eleES": {
                "systematics": [s + "_2016" for s in eleES_sys_embed],
                "affectedProcesses": ["embedded"],
            },
            # Muon ES
            "muES": {
                "systematics": [s + "_2016" for s in muES_sys],
                "affectedProcesses": ["MC"],
            },
            # Embedded muon ES
            "EMB_muES": {
                "systematics": [s + "_2016" for s in muES_sys_embed],
                "affectedProcesses": ["embedded"],
            },
            "JER": {
                "systematics": jerSys_2016,
                "affectedProcesses": ["MC", "qcd"],
            },
            "btag": {
                "systematics": [s + "_2016" for s in btagSys],
                "affectedProcesses": ["MC", "qcd"],
            },
            "met": {
                "systematics": [s + "_2016" for s in metSys],
                "affectedProcesses": ["MC", "qcd"],
            },
            "emu_trgeffSys": {
                "systematics": [s + "_2016" for s in emu_trgeffSys],
                "affectedProcesses": ["MC", "qcd"],
            },
            "emu_trgeffSys_embed": {
                "systematics": [s + "_2016" for s in emu_trgeffSys_embed],
                "affectedProcesses": ["embedded", "qcd"],
            },
            "emu_qcdSys": {
                "systematics": [s + "_2016" for s in emu_qcdSys],
                "affectedProcesses": ["qcd"],
            },
            "zPtSys": {
                "systematics": [s + "_2016" for s in zPtSys],
                "affectedProcesses": ["drellYan", "qcd"],
            },
            "topPt": {
                "systematics": [s + "_2016" for s in topPtSys],
                "affectedProcesses": ["ttbar", "qcd"],
            },
        },
    },
    "2016": {
        "mutau": {
            "prefiring": {
                "systematics": prefiring_2016,
                "affectedProcesses": ["MC", "fake"],
            },
            "TES": {
                "systematics": [s + "_2016" for s in tauES_sys],
                "affectedProcesses": ["MC"],
            },
            "eleTES": {
                "systematics": [s + "_2016" for s in eleTES_sys],
                "affectedProcesses": ["MC"],
            },
            "muTES": {
                "systematics": [s + "_2016" for s in muTES_sys],
                "affectedProcesses": ["MC"],
            },
            "EMB_TES": {
                "systematics": [s + "_2016" for s in tauES_sys_embed],
                "affectedProcesses": ["embedded"],
            },
            # Muon ES
            "muES": {
                "systematics": [s + "_2016" for s in muES_sys],
                "affectedProcesses": ["MC"],
            },
            # Embedded muon ES
            "EMB_muES": {
                "systematics": [s + "_2016" for s in muES_sys_embed],
                "affectedProcesses": ["embedded"],
            },
            "JER": {
                "systematics": jerSys_2016,
                "affectedProcesses": ["MC", "fake"],
            },
            "tauideff": {
                "systematics": [s + "_2016" for s in tauIDeff_sys],
                "affectedProcesses": ["MC"],
            },
            "EMB_tauideff": {
                "systematics": [s + "_2016" for s in tauIDeff_sys_embed],
                "affectedProcesses": ["embedded"],
            },
            "btag": {
                "systematics": [s + "_2016" for s in btagSys],
                "affectedProcesses": ["MC", "fake"],
            },
            "met": {
                "systematics": [s + "_2016" for s in metSys],
                "affectedProcesses": ["MC", "fake"],
            },
            "jetFakingTauBackgroundSys": {
                "systematics": [s + "_2016" for s in jetFakingTauBackgroundSys],
                "affectedProcesses": ["fake"],
            },
            "mutau_trgeffSys": {
                "systematics": [s + "_2016" for s in mutau_trgeffSys],
                "affectedProcesses": ["MC", "fake"],
            },
            "mutau_trgeffSys_embed": {
                "systematics": [s + "_2016" for s in mutau_trgeffSys_embed],
                "affectedProcesses": ["embedded", "fake"],
            },
            "embed_trackSys": {
                "systematics": [s + "_2016" for s in embed_trackSys],
                "affectedProcesses": ["embedded"],
            },
            "zPtSys": {
                "systematics": [s + "_2016" for s in zPtSys],
                "affectedProcesses": ["drellYan", "fake"],
            },
            "topPt": {
                "systematics": [s + "_2016" for s in topPtSys],
                "affectedProcesses": ["ttbar", "fake"],
            },
            "nonDY": {
                "systematics": [s + "_2016" for s in nonDYSys],
                "affectedProcesses": ["embedded"],
            },
        },
        "etau": {
            "prefiring": {
                "systematics": prefiring_2016,
                "affectedProcesses": ["MC", "fake"],
            },
            "TES": {
                "systematics": [s + "_2016" for s in tauES_sys],
                "affectedProcesses": ["MC", "fake"],
            },
            "eleTES": {
                "systematics": [s + "_2016" for s in eleTES_sys],
                "affectedProcesses": ["MC"],
            },
            "muTES": {
                "systematics": [s + "_2016" for s in muTES_sys],
                "affectedProcesses": ["MC"],
            },
            "EMB_TES": {
                "systematics": [s + "_2016" for s in tauES_sys_embed],
                "affectedProcesses": ["embedded"],
            },
            # Ele ES
            "eleES": {
                "systematics": [s + "_2016" for s in eleES_sys],
                "affectedProcesses": ["MC"],
            },
            # Embedded ele ES
            "EMB_eleES": {
                "systematics": [s + "_2016" for s in eleES_sys_embed],
                "affectedProcesses": ["embedded"],
            },
            "JER": {
                "systematics": jerSys_2016,
                "affectedProcesses": ["MC", "fake"],
            },
            "tauideff": {
                "systematics": [s + "_2016" for s in tauIDeff_sys],
                "affectedProcesses": ["MC"],
            },
            "EMB_tauideff": {
                "systematics": [s + "_2016" for s in tauES_sys_embed],
                "affectedProcesses": ["embedded"],
            },
            "btag": {
                "systematics": [s + "_2016" for s in btagSys],
                "affectedProcesses": ["MC", "fake"],
            },
            "met": {
                "systematics": [s + "_2016" for s in metSys],
                "affectedProcesses": ["MC", "fake"],
            },
            "etau_trgeffSys": {
                "systematics": [s + "_2016" for s in etau_trgeffSys],
                "affectedProcesses": ["MC", "fake"],
            },
            "etau_trgeffSys_embed": {
                "systematics": [s + "_2016" for s in etau_trgeffSys_embed],
                "affectedProcesses": ["embedded"],
            },
            "embed_trackSys": {
                "systematics": [s + "_2016" for s in embed_trackSys],
                "affectedProcesses": ["embedded"],
            },
            "zPtSys": {
                "systematics": [s + "_2016" for s in zPtSys],
                "affectedProcesses": ["drellYan", "fake"],
            },
            "topPt": {
                "systematics": [s + "_2016" for s in topPtSys],
                "affectedProcesses": ["ttbar", "fake"],
            },
            "nonDY": {
                "systematics": [s + "_2016" for s in nonDYSys],
                "affectedProcesses": ["embedded"],
            },
        },
        "emu": {
            "prefiring": {
                "systematics": prefiring_2016,
                "affectedProcesses": ["MC", "qcd"],
            },
            # Ele ES
            "eleES": {
                "systematics": [s + "_2016" for s in eleES_sys],
                "affectedProcesses": ["MC"],
            },
            # Embedded ele ES
            "EMB_eleES": {
                "systematics": [s + "_2016" for s in eleES_sys_embed],
                "affectedProcesses": ["embedded"],
            },
            # Muon ES
            "muES": {
                "systematics": [s + "_2016" for s in muES_sys],
                "affectedProcesses": ["MC"],
            },
            # Embedded muon ES
            "EMB_muES": {
                "systematics": [s + "_2016" for s in muES_sys_embed],
                "affectedProcesses": ["embedded"],
            },
            "JER": {
                "systematics": jerSys_2016,
                "affectedProcesses": ["MC", "qcd"],
            },
            "btag": {
                "systematics": [s + "_2016" for s in btagSys],
                "affectedProcesses": ["MC", "qcd"],
            },
            "met": {
                "systematics": [s + "_2016" for s in metSys],
                "affectedProcesses": ["MC", "qcd"],
            },
            "emu_trgeffSys": {
                "systematics": [s + "_2016" for s in emu_trgeffSys],
                "affectedProcesses": ["MC", "qcd"],
            },
            "emu_trgeffSys_embed": {
                "systematics": [s + "_2016" for s in emu_trgeffSys_embed],
                "affectedProcesses": ["embedded", "qcd"],
            },
            "emu_qcdSys": {
                "systematics": [s + "_2016" for s in emu_qcdSys],
                "affectedProcesses": ["qcd"],
            },
            "zPtSys": {
                "systematics": [s + "_2016" for s in zPtSys],
                "affectedProcesses": ["drellYan", "qcd"],
            },
            "topPt": {
                "systematics": [s + "_2016" for s in topPtSys],
                "affectedProcesses": ["ttbar", "qcd"],
            },
        },
    },
}
