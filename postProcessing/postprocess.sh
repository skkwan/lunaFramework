#!/bin/bash

#--------------------------------------------------------
# Usage: bash postprocess.sh [COMPILE or NO_COMPILE] [YEAR] [samples.csv] [GITLAB or LXPLUS] [COMPILEONLY OR RUNFULL]
#--------------------------------------------------------

COMPILE=$1
YEAR=$2
SAMPLES=$3
RUNNER=$4
COMPILEONLY=$5
DO_SHIFTS=$6

#--------------------------------------------------------
# Compile executable
#--------------------------------------------------------
if [ "$COMPILE" = COMPILE ]; then

    COMPILER=$(root-config --cxx)
    FLAGS=$(root-config --cflags --libs)

    echo ">>> postprocess.sh: Compile postprocess.cxx executable ..."
    time $COMPILER -g -O3 -Wall -Wextra -Wpedantic -lRooFitCore -lRooFit -o postprocess postprocess.cxx $FLAGS

    if [ $? -ne 0 ]; then
        echo ">>> postprocess.sh: Compile failed, exiting"
        exit 2
    fi


fi

#--------------------------------------------------------
# Get the voms-proxy-info certificate
#--------------------------------------------------------
export MYPROXYPATH="$(voms-proxy-info -path)"

if [[ -f ${MYPROXYPATH} ]]; then
    echo ">>> postprocess.sh: Copying proxy from ${MYPROXYPATH} to /afs/cern.ch/user/s/skkwan/private/x509up_file"
    cp ${MYPROXYPATH} /afs/cern.ch/user/s/skkwan/private/x509up_file
else
    echo ">>> postprocess.sh: [ERROR]: x509 proxy not found on this machine, make sure voms-proxy-init was run, exiting"
    exit 2

fi


#--------------------------------------------------------
# First check if the input file exists
#--------------------------------------------------------

# while IFS=, read -r SAMPLE PROCESS CONFIG_DIR SCALE NENTRIES
# do
#     {
#         # echo ">>> postprocess.sh: Check that samples exist... "
# 	# if [[ ! -f ${SAMPLE} ]] ; then
# 	if [[ $(gfal-ls ${SAMPLE}) ]]; then
# 	    echo  "postprocess.sh: ${SAMPLE} exists"
# 	else
# 	    echo "postprocess.sh: ERROR: File ${SAMPLE} is not there, aborting. (Make sure there are no extra newlines in ${SAMPLES}.)"
# 	    exit 1
# 	fi
#     }
# done < ${SAMPLES}


#--------------------------------------------------------
# Then run the postprocess
#--------------------------------------------------------

if [ "$COMPILEONLY" = RUNFULL ]; then

    DATETIME="$(date +"%b-%d-%Y-%Hh%Mm")"

    #--------------------------------------------------------
    # Set up EOS and local directories for outputs
    #--------------------------------------------------------
    OUTPUT_NTUP_DIR="intermediateTuples"/${YEAR}/${DATETIME}
    EOS_NTUP_DIR="/eos/user/s/skkwan/hToAA/dataMC/intermediateTuples/"
    if [ "$RUNNER" = GITLAB ]; then
	echo ">>> postprocess.sh: Is running in Gitlab ..."
	EOS_HIST_DIR="root://eosuser.cern.ch/"${EOS_HIST_DIR}
    fi

    echo ">>> postprocess.sh: Beginning background processes: [Time: $(TZ=America/New_York date)]"
    #--------------------------------------------------------
    # Loop over the files
    #--------------------------------------------------------
    while IFS=, read -r SAMPLE DNN PROCESS CONFIG_DIR SCALE NENTRIES JOBSIZE
    do
	{
	    mkdir -p ${OUTPUT_NTUP_DIR}
	    OUTPUT_HIST=${OUTPUT_NTUP_DIR}/histograms_${PROCESS}.root
	    OUTPUT_NTUP=${OUTPUT_NTUP_DIR}/${PROCESS}_intermediate.root
	    LOG=${OUTPUT_NTUP_DIR}/logRun_${PROCESS}
	    rm ${LOG}

	    JOBNUMBER=0  # for testing

	    echo "--- Starting postprocess executable for ${SAMPLE}: [Time: $(TZ=America/New_York date)] --- "
        echo ">>> postprocess.sh: Associated DNN output file: ${DNN}"
	    echo ">>> postprocess.sh: Writing output histograms to ${OUTPUT_HIST}, and output n-tuples to ${OUTPUT_NTUP}"
		echo ">>> postprocess.sh: Job size: ${JOBSIZE} and job number: ${JOBNUMBER}"
	    source /cvmfs/sft.cern.ch/lcg/views/setupViews.sh LCG_96bpython3 x86_64-centos7-gcc62-opt
	    ./postprocess $SAMPLE $DNN $PROCESS $CONFIG_DIR $OUTPUT_HIST $OUTPUT_NTUP $SCALE $NENTRIES ${SAMPLES} ${DO_SHIFTS} ${JOBSIZE} ${JOBNUMBER}| tee $LOG

	    #--------------------------------------------------------
	    # Copy final outputs to EOS
	    # Build the EOS directory name using timestamp and the job descriptor
	    #--------------------------------------------------------
	    DESCRIPTOR=$(basename $(dirname $SAMPLE))
	    FINAL_EOS_DIR=${EOS_NTUP_DIR}${YEAR}/${DATETIME}-${DESCRIPTOR}/
	    echo ">>> postprocess.sh: copying outputs to ${FINAL_EOS_DIR}..."
	    mkdir -p ${FINAL_EOS_DIR}
	    mv ${LOG} ${FINAL_EOS_DIR}
	    mv ${OUTPUT_HIST} ${FINAL_EOS_DIR}
	    mv ${OUTPUT_NTUP} ${FINAL_EOS_DIR}
	}
    done < ${SAMPLES}
    wait

    #--------------------------------------------------------
    # Optional finishing up
    #--------------------------------------------------------
    echo ">>> postprocess.sh: scp 'skkwan@lxplus.cern.ch:${FINAL_EOS_DIR}*' ."
    # Also createReport.py with just the n-tuple cutflows
    # cd /eos/user/s/skkwan/hToAA/dataMC
    # python createReport.py --rootdir=${FINAL_EOS_DIR}
    # cd -

    echo "All done [Time: $(TZ=America/New_York date)]: next step, modify and run runMakeHistograms.sh to make histograms"
fi
