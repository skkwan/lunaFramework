#!/bin/bash

# Usage: Check source runMakeHistograms.sh for a wrapper.
# The arguments:
# makeHistograms.sh [COMPILE or NO_COMPILE] [YEAR] [samples.csv] [GITLAB or LXPLUS] [COMPILEONLY OR RUNFULL]

# THE ORDER TO RUN THE SCRIPTS:
# 1. runPostProcess.sh: make n-tuples from skim-level n-tuples
# 2. [To-do: SVFIT, DNN..]
# 3. runMakeHistograms.sh: THIS SCRIPT: make histograms (one per DAS dataset) from processed n-tuples
# 4. runBkgEstimation.sh: make final set of histograms (one per histogram in data/MC) from step #3
# 5. runStackPlots.sh: make data/MC plots from the final set of histograms.

COMPILE=$1
YEAR=$2
SAMPLES=$3
RUNNER=$4
COMPILEONLY=$5

# Compile executable
if [ "$COMPILE" = COMPILE ]; then

    COMPILER=$(root-config --cxx)
    FLAGS=$(root-config --cflags --libs)

    echo ">>> makeHistograms.sh: Compile makeHistograms.cxx executable ..."
    time $COMPILER -g -O3 -Wall -Wextra -Wpedantic -lRooFitCore -lRooFit -o makeHistograms makeHistograms.cxx $FLAGS

fi

# First check if the input file exists
while IFS=, read -r SAMPLE PROCESS CONFIG_DIR SCALE NENTRIES
do
    {
        # echo ">>> makeHistograms.sh: Check that samples exist... "
	if [[ ! -f ${SAMPLE} ]] ; then
	    echo "makeHistograms.sh: ERROR: File ${SAMPLE} is not there, aborting."
	    exit
	fi
    }
done < ${SAMPLES}


# Then run the makeHistograms
if [ "$COMPILEONLY" = RUNFULL ]; then

    DATETIME="$(date +"%b-%d-%Y-%Hh%Mm")"

    # Set up EOS and local directories for outputs
    OUTPUT_HIST_DIR="stackHists"/${YEAR}/${DATETIME}
    OUTPUT_NTUP_DIR="intermediateTuples"/${YEAR}/${DATETIME}
    EOS_HIST_DIR="/eos/user/s/skkwan/hToAA/dataMC/stackHists/"
    if [ "$RUNNER" = GITLAB ]; then
	echo ">>> makeHistograms.sh: Is running in Gitlab ..."
	EOS_HIST_DIR="root://eosuser.cern.ch/"${EOS_HIST_DIR}
    fi

    echo ">>> makeHistograms.sh: Beginning background processes: [Time: $(TZ=America/New_York date)]"
    mkdir -p ${OUTPUT_HIST_DIR}

    while IFS=, read -r SAMPLE PROCESS CONFIG_DIR SCALE NENTRIES
    do
	{
	    OUTPUT_HIST=${OUTPUT_HIST_DIR}/histograms_${PROCESS}.root
	    # OUTPUT_NTUP=${OUTPUT_NTUP_DIR}/${PROCESS}_intermediate.root
	    LOG=${OUTPUT_HIST_DIR}/logRun_${PROCESS}
	    rm ${LOG}

	    echo "--- Starting makeHistograms executable for ${SAMPLE}: [Time: $(TZ=America/New_York date)] --- "
	    echo ">>> makeHistograms.sh: Writing output histograms to ${OUTPUT_HIST}"
	    ./makeHistograms $SAMPLE $PROCESS $CONFIG_DIR $OUTPUT_HIST $SCALE $NENTRIES ${SAMPLES} | tee $LOG
	    DESCRIPTOR=$(basename $(dirname $SAMPLE))
	    export NTUPDIR=$(dirname $SAMPLE)
	}
    done < ${SAMPLES}
    wait


    hadd -f ${OUTPUT_HIST_DIR}/histograms.root ${OUTPUT_HIST_DIR}/histograms_*.root
    FINAL_EOS_DIR=${EOS_HIST_DIR}${YEAR}/${DATETIME}-${DESCRIPTOR}
    echo ">>> makeHistograms.sh: copying outputs to ${FINAL_EOS_DIR}..."
    mkdir -p ${FINAL_EOS_DIR}
    cp ${OUTPUT_HIST_DIR}/histograms*.root ${FINAL_EOS_DIR}
    cp ${OUTPUT_HIST_DIR}/logRun* ${FINAL_EOS_DIR}
    echo "Copied histograms and logRuns to ${FINAL_EOS_DIR}."
    echo "makeHistograms.sh: All done [Time: $(TZ=America/New_York date)]. Next step: runBkgEstimation.sh: you will want the lines"
    echo export TUPLES_LOCATION=\"${NTUPDIR}\"
    echo export HISTOGRAMS_ROOT_LOCATION=\"${FINAL_EOS_DIR}\"
fi
