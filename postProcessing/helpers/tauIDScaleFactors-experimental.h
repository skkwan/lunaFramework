#ifndef TAU_ID_SCALE_FACTORS_EXPERIMENTAL_H_INCL
#define TAU_ID_SCALE_FACTORS_EXPERIMENTAL_H_INCL

#include "correction.h"
#include "helperFunctions.h"
#include "sampleConfig_class.h"

#include "TFile.h"
#include "TF1.h"

using correction::CorrectionSet;

/*******************************************************************/

/*
 * Get tau ID scale factor in a branch.
 */

template <typename T>
auto GetTauIDShifts_experimental(T &df, LUNA::sampleConfig_t &sConfig, TString jsonDir) {

  int isMC = sConfig.isMC();
  int isEmbedded = sConfig.isEmbedded();

  // MC or Embedded: any year
  if (isMC || isEmbedded) {

    TString cset_dir;
    TString objName;

    if (isMC) {
      cset_dir.Form("%sPOG/TAU/%s_UL/tau.json", jsonDir.Data(), sConfig.era().c_str());
      objName = "DeepTau2017v2p1VSjet";

    }
    else if (isEmbedded) {
      if (sConfig.year() == 2018) {
        cset_dir.Form("%sPOG/TAU/%s_UL/tau_embed.json", jsonDir.Data(), sConfig.era().c_str());
        objName = "test_DeepTau2017v2p1VSjet_pt-dm";
      }
      else {
        // Different file for 2017 and 2016
        cset_dir.Form("%sPOG/TAU/%s_UL/tau.json", jsonDir.Data(), sConfig.era().c_str());
        objName = "DeepTau2017v2p1VSjet";
      }
    }
    auto cset = CorrectionSet::from_file(cset_dir.Data());
    auto cset_tauID = cset->at(objName.Data());

    // the second "VVLoose" is a vsE working point, but in the json the only valid key for this field is VVLoose

    auto df2 = df.Define("weight_tauID_nominal", [cset_tauID, isMC, isEmbedded] (const int channel, const float pt, const int dm, const unsigned int genmatch) {
                                                  if ((channel == Helper::mt) || (channel == Helper::et)) {
                                                    if (isMC)            { return (float) cset_tauID->evaluate({pt, dm, (int) genmatch, "Medium", "VVLoose", "default", "pt"}); }
                                                    else if (isEmbedded) { return (float) cset_tauID->evaluate({pt, dm, (int) genmatch, "Medium", "VVLoose", "nom", "pt"}); }
                                                  }
                                                  return 1.0f;
                                                  } , {"channel", "pt_2_nominal", "decayMode_2", "gen_match_2"});
    if (sConfig.doShifts()) {
      // mutau and etau: shifts due to the tau's energy scale (es2Up, es2Down)
      return df2.Define("weight_tauID_es2Up",  [cset_tauID, isMC, isEmbedded] (const int channel, const float pt, const int dm, const unsigned int genmatch) {
                                                if ((channel == Helper::mt) || (channel == Helper::et)) {
                                                  if (isMC)            { return (float) cset_tauID->evaluate({pt, dm, (int) genmatch, "Medium", "VVLoose", "default", "pt"}); }
                                                  else if (isEmbedded) { return (float) cset_tauID->evaluate({pt, dm, (int) genmatch, "Medium", "VVLoose", "nom", "pt"}); }
                                                }
                                                return 1.0f;
                                              } , {"channel", "pt_2_es2Up", "decayMode_2", "gen_match_2"})
                .Define("weight_tauID_es2Down", [cset_tauID, isMC, isEmbedded] (const int channel, const float pt, const int dm, const unsigned int genmatch) {
                                                if ((channel == Helper::mt) || (channel == Helper::et)) {
                                                  if (isMC)            { return (float) cset_tauID->evaluate({pt, dm, (int) genmatch, "Medium", "VVLoose", "default", "pt"}); }
                                                  else if (isEmbedded) { return (float) cset_tauID->evaluate({pt, dm, (int) genmatch, "Medium", "VVLoose", "nom", "pt"}); }
                                                }
                                                return 1.0f;
                                              } , {"channel", "pt_2_es2Down", "decayMode_2", "gen_match_2"})
                // mutau and etau: shifts of the tau energy scale itself. Use the nominal pt_2_nominal
                // The json will get the correct pT up/down shift for each pT value
                .Define("weight_tauID_Up", [cset_tauID, isMC, isEmbedded] (const int channel, const float pt, const int dm, const unsigned int genmatch) {
                                                if ((channel == Helper::mt) || (channel == Helper::et)) {
                                                  if (isMC)            { return (float) cset_tauID->evaluate({pt, dm, (int) genmatch, "Medium", "VVLoose", "up", "pt"}); }
                                                  else if (isEmbedded) { return (float) cset_tauID->evaluate({pt, dm, (int) genmatch, "Medium", "VVLoose", "up", "pt"}); }
                                                }
                                                return 1.0f;
                                                } , {"channel", "pt_2_es2Up", "decayMode_2", "gen_match_2"})
                .Define("weight_tauID_Down", [cset_tauID, isMC, isEmbedded] (const int channel, const float pt, const int dm, const unsigned int genmatch) {
                                                if ((channel == Helper::mt) || (channel == Helper::et)) {
                                                  if (isMC)            { return (float) cset_tauID->evaluate({pt, dm, (int) genmatch, "Medium", "VVLoose", "down", "pt"}); }
                                                  else if (isEmbedded) { return (float) cset_tauID->evaluate({pt, dm, (int) genmatch, "Medium", "VVLoose", "down", "pt"}); }
                                                }
                                                return 1.0f;
                                                } , {"channel", "pt_2_es2Up", "decayMode_2", "gen_match_2"})
                // Now we need to bin the uncertainties: e.g. weight_tauID_tauideff_pt20to25_Up will have the up shift for pt > 20 <= 25, but the nominal value for a pt = 40 event
                .Define("weight_tauID_tauideff_pt20to25_Up", [](const float pt, const float weight_nom, const float weight_shifted) {
                                                              return ((pt > 20) && (pt <= 25)) ? weight_shifted : weight_nom; },
                                                              {"pt_2_nominal", "weight_tauID_nominal", "weight_tauID_Up"})
                .Define("weight_tauID_tauideff_pt25to30_Up", [](const float pt, const float weight_nom, const float weight_shifted) {
                                                              return ((pt > 25) && (pt <= 30)) ? weight_shifted : weight_nom; },
                                                              {"pt_2_nominal", "weight_tauID_nominal", "weight_tauID_Up"})
                .Define("weight_tauID_tauideff_pt30to35_Up", [](const float pt, const float weight_nom, const float weight_shifted) {
                                                              return ((pt > 30) && (pt <= 35)) ? weight_shifted : weight_nom; },
                                                              {"pt_2_nominal", "weight_tauID_nominal", "weight_tauID_Up"})
                .Define("weight_tauID_tauideff_pt35to40_Up", [](const float pt, const float weight_nom, const float weight_shifted) {
                                                              return ((pt > 35) && (pt <= 40)) ? weight_shifted : weight_nom; },
                                                              {"pt_2_nominal", "weight_tauID_nominal", "weight_tauID_Up"})
                .Define("weight_tauID_tauideff_pt40to500_Up", [](const float pt, const float weight_nom, const float weight_shifted) {
                                                              return ((pt > 40) && (pt <= 500)) ? weight_shifted : weight_nom; },
                                                              {"pt_2_nominal", "weight_tauID_nominal", "weight_tauID_Up"})
                .Define("weight_tauID_tauideff_pt500to1000_Up", [](const float pt, const float weight_nom, const float weight_shifted) {
                                                              return ((pt > 500) && (pt <= 1000)) ? weight_shifted : weight_nom; },
                                                              {"pt_2_nominal", "weight_tauID_nominal", "weight_tauID_Up"})
                .Define("weight_tauID_tauideff_ptgt1000_Up", [](const float pt, const float weight_nom, const float weight_shifted) {
                                                              return (pt > 1000) ? weight_shifted : weight_nom; },
                                                              {"pt_2_nominal", "weight_tauID_nominal", "weight_tauID_Up"})

                .Define("weight_tauID_tauideff_pt20to25_Down", [](const float pt, const float weight_nom, const float weight_shifted) {
                                                              return ((pt > 20) && (pt <= 25)) ? weight_shifted : weight_nom; },
                                                              {"pt_2_nominal", "weight_tauID_nominal", "weight_tauID_Down"})
                .Define("weight_tauID_tauideff_pt25to30_Down", [](const float pt, const float weight_nom, const float weight_shifted) {
                                                              return ((pt > 25) && (pt <= 30)) ? weight_shifted : weight_nom; },
                                                              {"pt_2_nominal", "weight_tauID_nominal", "weight_tauID_Down"})
                .Define("weight_tauID_tauideff_pt30to35_Down", [](const float pt, const float weight_nom, const float weight_shifted) {
                                                              return ((pt > 30) && (pt <= 35)) ? weight_shifted : weight_nom; },
                                                              {"pt_2_nominal", "weight_tauID_nominal", "weight_tauID_Down"})
                .Define("weight_tauID_tauideff_pt35to40_Down", [](const float pt, const float weight_nom, const float weight_shifted) {
                                                              return ((pt > 35) && (pt <= 40)) ? weight_shifted : weight_nom; },
                                                              {"pt_2_nominal", "weight_tauID_nominal", "weight_tauID_Down"})
                .Define("weight_tauID_tauideff_pt40to500_Down", [](const float pt, const float weight_nom, const float weight_shifted) {
                                                              return ((pt > 40) && (pt <= 500)) ? weight_shifted : weight_nom; },
                                                              {"pt_2_nominal", "weight_tauID_nominal", "weight_tauID_Down"})
                .Define("weight_tauID_tauideff_pt500to1000_Down", [](const float pt, const float weight_nom, const float weight_shifted) {
                                                              return ((pt > 500) && (pt <= 1000)) ? weight_shifted : weight_nom; },
                                                              {"pt_2_nominal", "weight_tauID_nominal", "weight_tauID_Down"})
                .Define("weight_tauID_tauideff_ptgt1000_Down", [](const float pt, const float weight_nom, const float weight_shifted) {
                                                              return (pt > 1000) ? weight_shifted : weight_nom; },
                                                              {"pt_2_nominal", "weight_tauID_nominal", "weight_tauID_Down"});


    }
    else {
      return df2;
    }
  }
  else { // data
    return df.Define("weight_tauID_nominal", [] { return 1.0f; });
  }

}

/*******************************************************************/


#endif
