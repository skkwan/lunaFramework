// Define the m_sv or m_vis to m_tt. To be called in postprocessing

#ifndef DEFINE_DITAU_MASS_VARIABLES_H_INCL
#define DEFINE_DITAU_MASS_VARIABLES_H_INCL


#include "computePhysicsVariables.h"
#include "fileIO.h"
#include "helperFunctions.h"
#include "sampleConfig_class.h"

/******************************************************************************/

/*
 * Alias visible or SVFit mass and other variables to *_tt_* depending on whether m_sv is available or not in the input TTree.
 */

template <typename T>
auto AliasSVFitVariables(T &df, LUNA::sampleConfig_t &sConfig){

    // If the SVFit branches are available, alias to it
    if (HasExactBranch(df, "m_sv")) {
        std::cout << ">>> alias.h: m_sv found" << std::endl;

        auto df2 = df.Define("m_tt_nominal", "m_sv")
                     .Define("pt_tt_nominal", "pt_sv")
                     .Define("eta_tt_nominal", "eta_sv")
                     .Define("phi_tt_nominal", "phi_sv");

        if (sConfig.doShifts() && (sConfig.isMC() || sConfig.isEmbedded())) {

            auto df3 = df2.Define("m_tt_es1Up", "m_sv_es1Up")
                          .Define("m_tt_es1Down", "m_sv_es1Down")
                          .Define("m_tt_es2Up", "m_sv_es2Up")
                          .Define("m_tt_es2Down", "m_sv_es2Down")
                          .Define("pt_tt_es1Up", "pt_sv_es1Up")
                          .Define("pt_tt_es1Down", "pt_sv_es1Down")
                          .Define("pt_tt_es2Up", "pt_sv_es2Up")
                          .Define("pt_tt_es2Down", "pt_sv_es2Down")
                          .Define("eta_tt_es1Up",   "eta_sv_es1Up")
                          .Define("eta_tt_es1Down", "eta_sv_es1Down")
                          .Define("eta_tt_es2Up",   "eta_sv_es2Up")
                          .Define("eta_tt_es2Down", "eta_sv_es2Down")
                          .Define("phi_tt_es1Up",   "phi_sv_es1Up")
                          .Define("phi_tt_es1Down", "phi_sv_es1Down")
                          .Define("phi_tt_es2Up",   "phi_sv_es2Up")
                          .Define("phi_tt_es2Down", "phi_sv_es2Down");

            if (sConfig.isMC()) {

                auto df4 = df3.Define("m_tt_UESUp", "m_sv_UESUp")
                              .Define("m_tt_UESDown", "m_sv_UESDown")
                              .Define("m_tt_ResponseUp", "m_sv_ResponseUp")
                              .Define("m_tt_ResponseDown", "m_sv_ResponseDown")
                              .Define("m_tt_ResolutionUp", "m_sv_ResolutionUp")
                              .Define("m_tt_ResolutionDown", "m_sv_ResolutionDown")
                              .Define("m_tt_JetAbsoluteUp", "m_sv_JetAbsoluteUp")
                              .Define("m_tt_JetAbsoluteDown", "m_sv_JetAbsoluteDown")
                              .Define("m_tt_JetAbsoluteyearUp", "m_sv_JetAbsoluteyearUp")
                              .Define("m_tt_JetAbsoluteyearDown", "m_sv_JetAbsoluteyearDown")
                              .Define("m_tt_JetBBEC1Up", "m_sv_JetBBEC1Up")
                              .Define("m_tt_JetBBEC1Down", "m_sv_JetBBEC1Down")
                              .Define("m_tt_JetBBEC1yearUp", "m_sv_JetBBEC1yearUp")
                              .Define("m_tt_JetBBEC1yearDown", "m_sv_JetBBEC1yearDown")
                              .Define("m_tt_JetEC2Up", "m_sv_JetEC2Up")
                              .Define("m_tt_JetEC2Down", "m_sv_JetEC2Down")
                              .Define("m_tt_JetEC2yearUp", "m_sv_JetEC2yearUp")
                              .Define("m_tt_JetEC2yearDown", "m_sv_JetEC2yearDown")
                              .Define("m_tt_JetFlavorQCDUp", "m_sv_JetFlavorQCDUp")
                              .Define("m_tt_JetFlavorQCDDown", "m_sv_JetFlavorQCDDown")
                              .Define("m_tt_JetHFUp", "m_sv_JetHFUp")
                              .Define("m_tt_JetHFDown", "m_sv_JetHFDown")
                              .Define("m_tt_JetHFyearUp", "m_sv_JetHFyearUp")
                              .Define("m_tt_JetHFyearDown", "m_sv_JetHFyearDown")
                              .Define("m_tt_JetRelativeBalUp", "m_sv_JetRelativeBalUp")
                              .Define("m_tt_JetRelativeBalDown", "m_sv_JetRelativeBalDown")
                              .Define("m_tt_JetRelativeSampleUp", "m_sv_JetRelativeSampleUp")
                              .Define("m_tt_JetRelativeSampleDown", "m_sv_JetRelativeSampleDown")
                              .Define("m_tt_JERUp", "m_sv_JERUp")
                              .Define("m_tt_JERDown", "m_sv_JERDown")

                              .Define("pt_tt_UESUp", "pt_sv_UESUp")
                              .Define("pt_tt_UESDown", "pt_sv_UESDown")
                              .Define("pt_tt_ResponseUp", "pt_sv_ResponseUp")
                              .Define("pt_tt_ResponseDown", "pt_sv_ResponseDown")
                              .Define("pt_tt_ResolutionUp", "pt_sv_ResolutionUp")
                              .Define("pt_tt_ResolutionDown", "pt_sv_ResolutionDown")
                              .Define("pt_tt_JetAbsoluteUp", "pt_sv_JetAbsoluteUp")
                              .Define("pt_tt_JetAbsoluteDown", "pt_sv_JetAbsoluteDown")
                              .Define("pt_tt_JetAbsoluteyearUp", "pt_sv_JetAbsoluteyearUp")
                              .Define("pt_tt_JetAbsoluteyearDown", "pt_sv_JetAbsoluteyearDown")
                              .Define("pt_tt_JetBBEC1Up", "pt_sv_JetBBEC1Up")
                              .Define("pt_tt_JetBBEC1Down", "pt_sv_JetBBEC1Down")
                              .Define("pt_tt_JetBBEC1yearUp", "pt_sv_JetBBEC1yearUp")
                              .Define("pt_tt_JetBBEC1yearDown", "pt_sv_JetBBEC1yearDown")
                              .Define("pt_tt_JetEC2Up", "pt_sv_JetEC2Up")
                              .Define("pt_tt_JetEC2Down", "pt_sv_JetEC2Down")
                              .Define("pt_tt_JetEC2yearUp", "pt_sv_JetEC2yearUp")
                              .Define("pt_tt_JetEC2yearDown", "pt_sv_JetEC2yearDown")
                              .Define("pt_tt_JetFlavorQCDUp", "pt_sv_JetFlavorQCDUp")
                              .Define("pt_tt_JetFlavorQCDDown", "pt_sv_JetFlavorQCDDown")
                              .Define("pt_tt_JetHFUp", "pt_sv_JetHFUp")
                              .Define("pt_tt_JetHFDown", "pt_sv_JetHFDown")
                              .Define("pt_tt_JetHFyearUp", "pt_sv_JetHFyearUp")
                              .Define("pt_tt_JetHFyearDown", "pt_sv_JetHFyearDown")
                              .Define("pt_tt_JetRelativeBalUp", "pt_sv_JetRelativeBalUp")
                              .Define("pt_tt_JetRelativeBalDown", "pt_sv_JetRelativeBalDown")
                              .Define("pt_tt_JetRelativeSampleUp", "pt_sv_JetRelativeSampleUp")
                              .Define("pt_tt_JetRelativeSampleDown", "pt_sv_JetRelativeSampleDown")
                              .Define("pt_tt_JERUp", "pt_sv_JERUp")
                              .Define("pt_tt_JERDown", "pt_sv_JERDown")

                              .Define("eta_tt_UESUp", "eta_sv_UESUp")
                              .Define("eta_tt_UESDown", "eta_sv_UESDown")
                              .Define("eta_tt_ResponseUp", "eta_sv_ResponseUp")
                              .Define("eta_tt_ResponseDown", "eta_sv_ResponseDown")
                              .Define("eta_tt_ResolutionUp", "eta_sv_ResolutionUp")
                              .Define("eta_tt_ResolutionDown", "eta_sv_ResolutionDown")
                              .Define("eta_tt_JetAbsoluteUp", "eta_sv_JetAbsoluteUp")
                              .Define("eta_tt_JetAbsoluteDown", "eta_sv_JetAbsoluteDown")
                              .Define("eta_tt_JetAbsoluteyearUp", "eta_sv_JetAbsoluteyearUp")
                              .Define("eta_tt_JetAbsoluteyearDown", "eta_sv_JetAbsoluteyearDown")
                              .Define("eta_tt_JetBBEC1Up", "eta_sv_JetBBEC1Up")
                              .Define("eta_tt_JetBBEC1Down", "eta_sv_JetBBEC1Down")
                              .Define("eta_tt_JetBBEC1yearUp", "eta_sv_JetBBEC1yearUp")
                              .Define("eta_tt_JetBBEC1yearDown", "eta_sv_JetBBEC1yearDown")
                              .Define("eta_tt_JetEC2Up", "eta_sv_JetEC2Up")
                              .Define("eta_tt_JetEC2Down", "eta_sv_JetEC2Down")
                              .Define("eta_tt_JetEC2yearUp", "eta_sv_JetEC2yearUp")
                              .Define("eta_tt_JetEC2yearDown", "eta_sv_JetEC2yearDown")
                              .Define("eta_tt_JetFlavorQCDUp", "eta_sv_JetFlavorQCDUp")
                              .Define("eta_tt_JetFlavorQCDDown", "eta_sv_JetFlavorQCDDown")
                              .Define("eta_tt_JetHFUp", "eta_sv_JetHFUp")
                              .Define("eta_tt_JetHFDown", "eta_sv_JetHFDown")
                              .Define("eta_tt_JetHFyearUp", "eta_sv_JetHFyearUp")
                              .Define("eta_tt_JetHFyearDown", "eta_sv_JetHFyearDown")
                              .Define("eta_tt_JetRelativeBalUp", "eta_sv_JetRelativeBalUp")
                              .Define("eta_tt_JetRelativeBalDown", "eta_sv_JetRelativeBalDown")
                              .Define("eta_tt_JetRelativeSampleUp", "eta_sv_JetRelativeSampleUp")
                              .Define("eta_tt_JetRelativeSampleDown", "eta_sv_JetRelativeSampleDown")
                              .Define("eta_tt_JERUp", "eta_sv_JERUp")
                              .Define("eta_tt_JERDown", "eta_sv_JERDown")

                              .Define("phi_tt_UESUp", "phi_sv_UESUp")
                              .Define("phi_tt_UESDown", "phi_sv_UESDown")
                              .Define("phi_tt_ResponseUp", "phi_sv_ResponseUp")
                              .Define("phi_tt_ResponseDown", "phi_sv_ResponseDown")
                              .Define("phi_tt_ResolutionUp", "phi_sv_ResolutionUp")
                              .Define("phi_tt_ResolutionDown", "phi_sv_ResolutionDown")
                              .Define("phi_tt_JetAbsoluteUp", "phi_sv_JetAbsoluteUp")
                              .Define("phi_tt_JetAbsoluteDown", "phi_sv_JetAbsoluteDown")
                              .Define("phi_tt_JetAbsoluteyearUp", "phi_sv_JetAbsoluteyearUp")
                              .Define("phi_tt_JetAbsoluteyearDown", "phi_sv_JetAbsoluteyearDown")
                              .Define("phi_tt_JetBBEC1Up", "phi_sv_JetBBEC1Up")
                              .Define("phi_tt_JetBBEC1Down", "phi_sv_JetBBEC1Down")
                              .Define("phi_tt_JetBBEC1yearUp", "phi_sv_JetBBEC1yearUp")
                              .Define("phi_tt_JetBBEC1yearDown", "phi_sv_JetBBEC1yearDown")
                              .Define("phi_tt_JetEC2Up", "phi_sv_JetEC2Up")
                              .Define("phi_tt_JetEC2Down", "phi_sv_JetEC2Down")
                              .Define("phi_tt_JetEC2yearUp", "phi_sv_JetEC2yearUp")
                              .Define("phi_tt_JetEC2yearDown", "phi_sv_JetEC2yearDown")
                              .Define("phi_tt_JetFlavorQCDUp", "phi_sv_JetFlavorQCDUp")
                              .Define("phi_tt_JetFlavorQCDDown", "phi_sv_JetFlavorQCDDown")
                              .Define("phi_tt_JetHFUp", "phi_sv_JetHFUp")
                              .Define("phi_tt_JetHFDown", "phi_sv_JetHFDown")
                              .Define("phi_tt_JetHFyearUp", "phi_sv_JetHFyearUp")
                              .Define("phi_tt_JetHFyearDown", "phi_sv_JetHFyearDown")
                              .Define("phi_tt_JetRelativeBalUp", "phi_sv_JetRelativeBalUp")
                              .Define("phi_tt_JetRelativeBalDown", "phi_sv_JetRelativeBalDown")
                              .Define("phi_tt_JetRelativeSampleUp", "phi_sv_JetRelativeSampleUp")
                              .Define("phi_tt_JetRelativeSampleDown", "phi_sv_JetRelativeSampleDown")
                              .Define("phi_tt_JERUp", "phi_sv_JERUp")
                              .Define("phi_tt_JERDown", "phi_sv_JERDown");
                return df4;
            }
            else {
                // embedded
                return df3;
            }
        }
        else {
            // data or no shifts
            return df2;
        }
    }
    else {
        std::cout << ">>> alias.h: m_sv NOT found, use m_vis" << std::endl;

        // SVFit branches not available
        auto df2 = df.Define("m_tt_nominal", "m_vis_nominal")
                     .Define("pt_tt_nominal", "pt_vis_nominal");

        // do shifts, MC or Embedded: has energy shifts
        if (sConfig.doShifts() && (sConfig.isMC() || sConfig.isEmbedded())) {
            auto df3 = df2.Define("m_tt_es1Up", "m_vis_es1Up")
                          .Define("m_tt_es1Down", "m_vis_es1Down")
                          .Define("m_tt_es2Up", "m_vis_es2Up")
                          .Define("m_tt_es2Down", "m_vis_es2Down")
                          .Define("pt_tt_es1Up", "pt_vis_es1Up")
                          .Define("pt_tt_es1Down", "pt_vis_es1Down")
                          .Define("pt_tt_es2Up", "pt_vis_es2Up")
                          .Define("pt_tt_es2Down", "pt_vis_es2Down");

            // MC only: jet shifts
            if (sConfig.isMC()) {

                return df3.Define("m_tt_UESUp", "m_vis_nominal")     // NOTE: this is just for the Define
                 .Define("m_tt_UESDown", "m_vis_nominal") // NOTE: this is just for the Define
                 .Define("m_tt_ResponseUp", "m_vis_nominal")  // NOTE: this is just for the Define
                 .Define("m_tt_ResponseDown", "m_vis_nominal") // NOTE: this is just for the Define
                 .Define("m_tt_ResolutionUp", "m_vis_nominal") // NOTE: this is just for the Define
                 .Define("m_tt_ResolutionDown", "m_vis_nominal")
                 .Define("m_tt_JetAbsoluteUp", "m_vis_nominal")
                 .Define("m_tt_JetAbsoluteDown", "m_vis_nominal")
                 .Define("m_tt_JetAbsoluteyearUp", "m_vis_nominal")
                 .Define("m_tt_JetAbsoluteyearDown", "m_vis_nominal")
                 .Define("m_tt_JetBBEC1Up", "m_vis_nominal")
                 .Define("m_tt_JetBBEC1Down", "m_vis_nominal")
                 .Define("m_tt_JetBBEC1yearUp", "m_vis_nominal")
                 .Define("m_tt_JetBBEC1yearDown", "m_vis_nominal")
                 .Define("m_tt_JetEC2Up", "m_vis_nominal")
                 .Define("m_tt_JetEC2Down", "m_vis_nominal")
                 .Define("m_tt_JetEC2yearUp", "m_vis_nominal")
                 .Define("m_tt_JetEC2yearDown", "m_vis_nominal")
                 .Define("m_tt_JetFlavorQCDUp", "m_vis_nominal")
                 .Define("m_tt_JetFlavorQCDDown", "m_vis_nominal")
                 .Define("m_tt_JetHFUp", "m_vis_nominal")
                 .Define("m_tt_JetHFDown", "m_vis_nominal")
                 .Define("m_tt_JetHFyearUp", "m_vis_nominal")
                 .Define("m_tt_JetHFyearDown", "m_vis_nominal")
                 .Define("m_tt_JetRelativeBalUp", "m_vis_nominal")
                 .Define("m_tt_JetRelativeBalDown", "m_vis_nominal")
                 .Define("m_tt_JetRelativeSampleUp", "m_vis_nominal")
                 .Define("m_tt_JetRelativeSampleDown", "m_vis_nominal")
                 .Define("m_tt_JERUp", "m_vis_nominal")
                 .Define("m_tt_JERDown", "m_vis_nominal")
                 .Define("pt_tt_UESUp", "pt_vis_nominal")   // NOTE: this is just for the Define, UES shift doesn't exist for visible-only tautau
                 .Define("pt_tt_UESDown", "pt_vis_nominal")  // NOTE: this is just for the Define, UES shift doesn't exist for visible-only tautau
                 .Define("pt_tt_ResponseUp", "pt_vis_nominal")  // NOTE: this is just for the Define
                 .Define("pt_tt_ResponseDown", "pt_vis_nominal") // NOTE: this is just for the Define
                 .Define("pt_tt_ResolutionUp", "pt_vis_nominal") // NOTE: this is just for the Define
                 .Define("pt_tt_ResolutionDown", "pt_vis_nominal") // NOTE: this is just for the Define
                 .Define("pt_tt_JetAbsoluteUp", "pt_vis_nominal")
                 .Define("pt_tt_JetAbsoluteDown", "pt_vis_nominal")
                 .Define("pt_tt_JetAbsoluteyearUp", "pt_vis_nominal")
                 .Define("pt_tt_JetAbsoluteyearDown", "pt_vis_nominal")
                 .Define("pt_tt_JetBBEC1Up", "pt_vis_nominal")
                 .Define("pt_tt_JetBBEC1Down", "pt_vis_nominal")
                 .Define("pt_tt_JetBBEC1yearUp", "pt_vis_nominal")
                 .Define("pt_tt_JetBBEC1yearDown", "pt_vis_nominal")
                 .Define("pt_tt_JetEC2Up", "pt_vis_nominal")
                 .Define("pt_tt_JetEC2Down", "pt_vis_nominal")
                 .Define("pt_tt_JetEC2yearUp", "pt_vis_nominal")
                 .Define("pt_tt_JetEC2yearDown", "pt_vis_nominal")
                 .Define("pt_tt_JetFlavorQCDUp", "pt_vis_nominal")
                 .Define("pt_tt_JetFlavorQCDDown", "pt_vis_nominal")
                 .Define("pt_tt_JetHFUp", "pt_vis_nominal")
                 .Define("pt_tt_JetHFDown", "pt_vis_nominal")
                 .Define("pt_tt_JetHFyearUp", "pt_vis_nominal")
                 .Define("pt_tt_JetHFyearDown", "pt_vis_nominal")
                 .Define("pt_tt_JetRelativeBalUp", "pt_vis_nominal")
                 .Define("pt_tt_JetRelativeBalDown", "pt_vis_nominal")
                 .Define("pt_tt_JetRelativeSampleUp", "pt_vis_nominal")
                 .Define("pt_tt_JetRelativeSampleDown", "pt_vis_nominal")
                 .Define("pt_tt_JERUp", "pt_vis_nominal")
                 .Define("pt_tt_JERDown", "pt_vis_nominal");

            }
            else {
                // Embedded
                return df3;
            }
        }
        else {
            // data or no shifts
            return df2;
        }
    }
}

/******************************************************************************/

#endif
