// Systematic for different tau ID working point used in MC vs. data

#ifndef TAU_ID_WP_SCALE_FACTOR_EXPERIMENTAL_H_INCL
#define TAU_ID_WP_SCALE_FACTOR_EXPERIMENTAL_H_INCL


#include "helperFunctions.h"
#include "sampleConfig_class.h"

/*******************************************************************/

/*
 * https://github.com/hftsoi/aabbtt_finalselection/blob/master/selection_et_allyears.cc#L1256-L1265
 */
ROOT::RVecF getTauIDwpSF_variations(int channel, int isEmbedded, float tauPt, int byMediumDeepVSjet, unsigned int gen_match) {

    float sf_up = 1.0;
    float sf_down = 1.0;

    // See note below on high pT (>100) taus
    if ((channel == Helper::et) && byMediumDeepVSjet && (gen_match == 5) && (tauPt < 100)) {
        if (!isEmbedded) {
            sf_up   = 1.03;
            sf_down = 0.97;
        }
        else {
            sf_up   = 1.05;
            sf_down = 0.95;
        }
    }

    return ROOT::RVecF{sf_up, sf_down};
}

/*******************************************************************/

/*
 * "Analyses that use looser working point of VSe (looser than VLoose) and/or VSmu (looser than Tight) discriminators,
 * should add additional uncertainty. In this case, an additional 3% should be added to MC and 5% to embedding sample
 * for nominal tau pT < 100 GeV. For high pT tau, where tau pT > 100 GeV, an additional 15% should be added."
 * https://twiki.cern.ch/twiki/bin/viewauth/CMS/TauIDRecommendationForRun2#Tau_Identification
 *
 * For us this only affects the etau channel because it uses VLoose vs. muon.
 * We do not use the tau pt > 100 15% uncertainty because our signal has soft objects and this 15% uncertainty would
 * not affect our signal region.
 */
template <typename T>
auto GetTauIDwpShifts_experimental(T &df, LUNA::sampleConfig_t &sConfig) {

    auto df2 = df.Define("weight_tauidWP_nominal", [] { return 1.0f; });

    if (sConfig.doShifts() && (sConfig.isMC() || sConfig.isEmbedded())) {
        int isEmbedded = sConfig.isEmbedded();
        return df2.Define("variations_weight_tauidWP", [isEmbedded] (int channel, float tauPt, int byMediumDeepVSjet, unsigned int gen_match) {
                return getTauIDwpSF_variations(channel, isEmbedded, tauPt, byMediumDeepVSjet, gen_match);
            }, {"channel", "pt_2_nominal", "byMediumDeepVSjet_2", "gen_match_2"})
                  .Define("weight_tauidWP_tauidWP_et_Up",   "variations_weight_tauidWP[0]")
                  .Define("weight_tauidWP_tauidWP_et_Down", "variations_weight_tauidWP[1]");
        // No shifts due to pt_2 because the central value is always 1.0.
    }
    else {
        return df2.Define("weight_tauidWP_tauidWP_et_Up",   [] { return 1.0f; })
                  .Define("weight_tauidWP_tauidWP_et_Down", [] { return 1.0f; });
    }

}



/*******************************************************************/

#endif
