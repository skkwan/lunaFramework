#ifndef COMPUTE_PHYSICS_VARIABLES_H
#define COMPUTE_PHYSICS_VARIABLES_H

#include "helperFunctions.h"

#include <cmath>
#include <ROOT/RVec.hxx>
#include <TVector3.h>
#include <Math/Vector4D.h>

// Add two 4-vectors
ROOT::Math::PtEtaPhiMVector add_p4(float pt, float eta, float phi, float mass);

// Compute m_{jj} from a list of 4-vectors
float compute_mjj_fromlist(ROOT::Math::PtEtaPhiMVector& p4, ROOT::RVec<int>& g);

// Compute m_{jj} if pair is valid
float compute_mjj(ROOT::Math::PtEtaPhiMVector& p4, bool isValidPair);

float compute_ptjj(ROOT::Math::PtEtaPhiMVector& p4, ROOT::RVec<int>& g);

float compute_jdeta(float x, float y, ROOT::RVec<int>& g);

float compute_mt(float pt_1, float phi_1, float pt_met, float phi_met);

float compute_pt_scalar_sum(float pt_1, float pt_2);

// Get a 4-vector; if index = -1 then return a (0, -99, -99, 0) vector.
ROOT::Math::PtEtaPhiMVector get_p4(int index, ROOT::RVec<float>& Vec_pt, ROOT::RVec<float>& Vec_eta, ROOT::RVec<float>& Vec_phi, ROOT::RVec<float>& Vec_mass);

/*------------------------------------------------------------------*/

// Variable D_zeta, defined in Equation 3 in HIG-17-024.
// D_zeta is defined to be p_xi - 0.85 p_xi^{vis}
float compute_D_zeta(float pt_met, float phi_met,
					float pt_1, float phi_1,
					float pt_2, float phi_2);

/*------------------------------------------------------------------*/

#endif
