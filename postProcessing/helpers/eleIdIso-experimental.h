#ifndef ELE_ID_ISO_EXPERIMENTAL_H
#define ELE_ID_ISO_EXPERIMENTAL_H

#include "correction.h"
#include "helperFunctions.h"
#include "sampleConfig_class.h"

using correction::CorrectionSet;

/*******************************************************************/

/*
 * Weight for electron MVA ID
 */

template <typename T>
auto GetEleIdIsoWeight(T &df, LUNA::sampleConfig_t &sConfig, TString jsonDir, TString jsonDirEmb) {

  // MC: any year
  if (sConfig.isMC()) {
    TString cset_dir = TString::Format("%sPOG/EGM/%s_UL/electron.json", jsonDir.Data(), sConfig.era().c_str());
    auto cset = CorrectionSet::from_file(cset_dir.Data());
    auto cset_eleID = cset->at("UL-Electron-ID-SF");

  // eta is binned from -infinity, -2, ... +2, +infinity
  // electron is always leg 1 for etau and emu
    auto df2 = df.Define("weight_eleIdIso_nominal", [cset_eleID] (const int channel, const std::string era, const float eta_1, const float pt_1) {
                      if ((channel == Helper::et) || (channel == Helper::em)) { // ele is leg 1
                        return (float) cset_eleID->evaluate({era, "sf", "wp90noiso", abs(eta_1), pt_1});
                      }
                      return 1.0f;
                    }, {"channel", "era", "eta_1", "pt_1"});
    if (sConfig.doShifts()) {
      return df2.Define("weight_eleIdIso_es1Up", [cset_eleID] (const int channel, const std::string era, const float eta_1, const float pt_1, const float weight_eleIdIso_nominal) {
                      if ((channel == Helper::et) || (channel == Helper::em)) { // ele is leg 1
                        return (float) cset_eleID->evaluate({era, "sf", "wp90noiso", abs(eta_1), pt_1});
                      }
                      return weight_eleIdIso_nominal;
                    }, {"channel", "era", "eta_1", "pt_1_es1Up", "weight_eleIdIso_nominal"})
                .Define("weight_eleIdIso_es1Down", [cset_eleID] (const int channel, const std::string era, const float eta_1, const float pt_1, const float weight_eleIdIso_nominal) {
                      if ((channel == Helper::et) || (channel == Helper::em)) { // ele is leg 1
                        return (float) cset_eleID->evaluate({era, "sf", "wp90noiso", abs(eta_1), pt_1});
                      }
                      return weight_eleIdIso_nominal;
                    }, {"channel", "era", "eta_1", "pt_1_es1Down", "weight_eleIdIso_nominal"});
    }
    else { // MC no shifts
      return df2;
    }
  }
  else if (sConfig.isEmbedded()) {
    // Embedded: one correctionlib for ID, one correctionlib for iso
    TString cset_dir = TString::Format("%selectron_%sUL.json", jsonDirEmb.Data(), sConfig.era().c_str());
    auto cset = CorrectionSet::from_file(cset_dir.Data());
    auto cset_eleID = cset->at("ID90_pt_eta_bins");
    auto cset_eleIso = cset->at("Iso_pt_eta_bins");

    // electron is always leg 1 for etau and emu
    auto df2 = df.Define("weight_eleIdIso_nominal", [cset_eleID, cset_eleIso] (const int channel, const float ele_eta, const float ele_pt) {
      if ((channel == Helper::et) || (channel == Helper::em)) {
        return (float) (cset_eleID->evaluate({ele_pt, abs(ele_eta), "emb"}) * cset_eleIso->evaluate({ele_pt, abs(ele_eta), "emb"}));
      }
      return 1.0f;
    }, {"channel", "eta_1", "pt_1"});

    if (sConfig.doShifts()) {
      return df2.Define("weight_eleIdIso_es1Up", [cset_eleID, cset_eleIso] (const int channel, const float ele_eta, const float ele_pt, const float weight_eleIdIso_nominal) {
                  if ((channel == Helper::et) || (channel == Helper::em)) {
                    return (float) (cset_eleID->evaluate({ele_pt, abs(ele_eta), "emb"}) * cset_eleIso->evaluate({ele_pt, abs(ele_eta), "emb"}));
                  }
                  else {
                    return weight_eleIdIso_nominal;
                  }}, {"channel", "eta_1", "pt_1_es1Up", "weight_eleIdIso_nominal"})
                .Define("weight_eleIdIso_es1Down", [cset_eleID, cset_eleIso] (const int channel, const float ele_eta, const float ele_pt, const float weight_eleIdIso_nominal) {
                  if ((channel == Helper::et) || (channel == Helper::em)) {
                    return (float) (cset_eleID->evaluate({ele_pt, abs(ele_eta), "emb"}) * cset_eleIso->evaluate({ele_pt, abs(ele_eta), "emb"}));
                  }
                  else {
                    return weight_eleIdIso_nominal;
                  }}, {"channel", "eta_1", "pt_1_es1Down", "weight_eleIdIso_nominal"});
    }
    else { // Embed no shifts
      return df2;
    }
  }
  else if (sConfig.isData()) {
    return df.Define("weight_eleIdIso_nominal", []() { return 1.0f; });
  }

  return df;
}

/*******************************************************************/

#endif
