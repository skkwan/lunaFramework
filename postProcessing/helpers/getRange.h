// getRange.h
// Get start/end ranges of a job


#ifndef GET_RANGE_H_INCL
#define GET_RANGE_H_INCL

/*
 * Return <start, end> job numbers that are (jobNumber * jobSize) up to ((jobNumber + 1) * jobSize)
 * and that respect the start/end of the file.
 */
std::pair<int, int> getRange(int jobNumber, int jobSize, int fileSize) {

    int start = jobNumber * jobSize;
    int end  = (jobNumber + 1) * jobSize;

    if (jobNumber < 0) {
        std::cout << ">>> getRange: Job number is negative, perform entire file" << std::endl;
        start = 0;
        end = fileSize;
    }
    else if (start > fileSize) {
        std::cout << ">>> getRange: Can't start at an event larger than the file size: set range to 0" << std::endl;
        start = 0;
        end = 0;
    }
    else if ((start < fileSize) && (end > fileSize)) {
        std::cout << ">>> getRange: Go until end of the file" << std::endl;
        end = fileSize;
    }

    std::pair<int, int> range = {start, end};
    // std::cout << std::get<0>(range) << ", " << std::get<1>(range) << std::endl;

    return range;
}

#endif
