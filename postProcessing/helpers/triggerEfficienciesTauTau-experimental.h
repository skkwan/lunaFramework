#ifndef TRIGGER_EFFICIENCIES_TAUTAU_EXPERIMENTAL_H_INCL
#define TRIGGER_EFFICIENCIES_TAUTAU_EXPERIMENTAL_H_INCL

#include <algorithm>
#include <mutex>

#include "correction.h"
#include "helperFunctions.h"
#include "sampleConfig_class.h"

#include "RooRealVar.h"
#include "RooWorkspace.h"

std::mutex m1;

using correction::CorrectionSet;

/**********************************************************/

/*
 * Get the nominal trigger scale factor for the emu channel (this is supposed to handle all years).
 * https://github.com/hftsoi/aabbtt_finalselection/blob/cefb486856b4c817d20c6b4c49d17eaef115f78e/selection_em_allyears.cc#L1278-L1342
 * If the sample is Embedded, the pointers that we got to the TH2F are already for the embedded samples.
 */

float getEMuTriggerSF_nominal(int channel,
                              const TH2F *th2f_mc_ele_Mu23Ele12, const TH2F *th2f_mc_muo_Mu23Ele12, const TH2F *th2f_mc_ele_Mu8Ele23, const TH2F *th2f_mc_muo_Mu8Ele23,
                              const TH2F *th2f_da_ele_Mu23Ele12, const TH2F *th2f_da_muo_Mu23Ele12, const TH2F *th2f_da_ele_Mu8Ele23, const TH2F *th2f_da_muo_Mu8Ele23,
                              float pt_1, float eta_1, float pt_2, float eta_2,
                              int passCross_leadingEle_em, int passCross_leadingMuon_em,
                              float em_crossLeadingEle_elept_thres, float em_crossLeadingEle_mupt_thres,
                              float em_crossLeadingMuon_elept_thres, float em_crossLeadingMuon_mupt_thres) {

    float trgsf = 1.0;
    float effMC_ele_Mu23Ele12 = 1.0;
    float effData_ele_Mu23Ele12 = 1.0;
    float effMC_muo_Mu23Ele12 = 1.0;
    float effData_muo_Mu23Ele12 = 1.0;

    float effMC_ele_Mu8Ele23 = 1.0;
    float effData_ele_Mu8Ele23 = 1.0;
    float effMC_muo_Mu8Ele23 = 1.0;
    float effData_muo_Mu8Ele23 = 1.0;

    if (channel == Helper::em) {

        // Avoid 0 bins
        float pt_1_treated = pt_1;
        float eta_1_treated = eta_1;
        float pt_2_treated = pt_2;
        float eta_2_treated = eta_2;

        if (eta_1 < -2.25) {
            eta_1_treated = -2.25;
        }
        else if (eta_1 > 2.25) {
            eta_1_treated = 2.25;
        }

        if (pt_1 < 19.5) {
            pt_1_treated = 19.5;
        }
        else if (pt_1 > 150.0) {
            pt_1_treated = 150.0;
        }

        if (pt_2 < 19.5) {
            pt_2_treated = 19.5;
        }
        else if (pt_2 > 160) {
            pt_2_treated = 160.0;
        }

        if (eta_2 < 0.45) {
            eta_2_treated = 0.45;
        }
        else if (eta_2 > 2.25) {
            eta_2_treated = 2.25;
        }

        // Electron leg, MC and data, Mu23Ele12. Assuming the same index that we'll grab
        int i_ele_Mu23Ele12 = th2f_mc_ele_Mu23Ele12->FindFixBin(pt_1_treated, eta_1_treated);
        effMC_ele_Mu23Ele12   = th2f_mc_ele_Mu23Ele12->GetBinContent(i_ele_Mu23Ele12);
        effData_ele_Mu23Ele12 = th2f_da_ele_Mu23Ele12->GetBinContent(i_ele_Mu23Ele12);

        // Muon leg, MC and data, Mu23Ele12. Muon efficiency is binned in abs(eta)
        int i_muo_Mu23Ele12 = th2f_mc_muo_Mu23Ele12->FindFixBin(pt_2_treated, std::abs(eta_2_treated));
        effMC_muo_Mu23Ele12   = th2f_mc_muo_Mu23Ele12->GetBinContent(i_muo_Mu23Ele12);
        effData_muo_Mu23Ele12 = th2f_da_muo_Mu23Ele12->GetBinContent(i_muo_Mu23Ele12);

        // Electron leg, MC and data, Mu8Ele23
        int i_ele_Mu8Ele23 = th2f_mc_ele_Mu8Ele23->FindFixBin(pt_1_treated, eta_1_treated);
        effMC_ele_Mu8Ele23   = th2f_mc_ele_Mu8Ele23->GetBinContent(i_ele_Mu8Ele23);
        effData_ele_Mu8Ele23 = th2f_da_ele_Mu8Ele23->GetBinContent(i_ele_Mu8Ele23);

        // Muon leg, MC and data, Mu8Ele23. Muon efficiency is binned in abs(eta)
        int i_muo_Mu8Ele23 = th2f_mc_muo_Mu8Ele23->FindFixBin(pt_2_treated, std::abs(eta_2_treated));
        effMC_muo_Mu8Ele23   = th2f_mc_muo_Mu8Ele23->GetBinContent(i_muo_Mu8Ele23);
        effData_muo_Mu8Ele23 = th2f_da_muo_Mu8Ele23->GetBinContent(i_muo_Mu8Ele23);

        // Nominal trg sf for emu
        int isLeadingEle = (passCross_leadingEle_em  && (pt_1 > em_crossLeadingEle_elept_thres)  && (pt_2 > em_crossLeadingEle_mupt_thres));
        int isLeadingMu  = (passCross_leadingMuon_em && (pt_1 > em_crossLeadingMuon_elept_thres) && (pt_2 > em_crossLeadingMuon_mupt_thres));
        if (isLeadingEle && !isLeadingMu) {
            trgsf = ((effData_ele_Mu8Ele23 * effData_muo_Mu8Ele23) / (effMC_ele_Mu8Ele23 * effMC_muo_Mu8Ele23));
        }
        else if (!(isLeadingEle) && isLeadingMu) {
            trgsf = ((effData_ele_Mu23Ele12 * effData_muo_Mu23Ele12) / (effMC_ele_Mu23Ele12 * effMC_muo_Mu23Ele12));
        }
        else if (isLeadingEle && isLeadingMu) {
            float numerator = (effData_muo_Mu8Ele23 * effData_ele_Mu8Ele23) + (effData_muo_Mu23Ele12 * effData_ele_Mu23Ele12) - (effData_ele_Mu8Ele23 * effData_muo_Mu23Ele12);
            float denominator = (effMC_muo_Mu8Ele23 * effMC_ele_Mu8Ele23) + (effMC_muo_Mu23Ele12 * effMC_ele_Mu23Ele12) - (effMC_ele_Mu8Ele23 * effMC_muo_Mu23Ele12);
            trgsf = (numerator / denominator);

        }
    }

    return trgsf;
}

/**********************************************************/

/*
 * For emu, just 20% from nominal trigger SF: https://github.com/hftsoi/aabbtt_finalselection/blob/cefb486856b4c817d20c6b4c49d17eaef115f78e/selection_em_allyears.cc#L1409
 */
ROOT::RVecF getEMuTriggerSF_variations(std::string sysToDo, float trgsf_nominal, int channel,
                                       float pt_1, float pt_2,
                                       int passCross_leadingEle_em, int passCross_leadingMuon_em,
                                       float em_crossLeadingEle_elept_thres, float em_crossLeadingEle_mupt_thres,
                                       float em_crossLeadingMuon_elept_thres, float em_crossLeadingMuon_mupt_thres) {

    float trgsf_up = trgsf_nominal;
    float trgsf_down = trgsf_nominal;

    int isLeadingEle = (passCross_leadingEle_em  && (pt_1 > em_crossLeadingEle_elept_thres)  && (pt_2 > em_crossLeadingEle_mupt_thres));
    int isLeadingMu  = (passCross_leadingMuon_em && (pt_1 > em_crossLeadingMuon_elept_thres) && (pt_2 > em_crossLeadingMuon_mupt_thres));

    if (channel == Helper::em) {

        if ((sysToDo == "trgeff_Mu8E23_em") && isLeadingEle && !(isLeadingMu)) {
            trgsf_up   = (trgsf_nominal * 1.02);
            trgsf_down = (trgsf_nominal * 0.98);
        }
        else if ((sysToDo == "trgeff_Mu23E12_em") && !(isLeadingEle) && isLeadingMu) {
            trgsf_up   = (trgsf_nominal * 1.02);
            trgsf_down = (trgsf_nominal * 0.98);
        }
        else if ((sysToDo == "trgeff_both_em") && isLeadingEle && isLeadingMu) {
            trgsf_up   = (trgsf_nominal * 1.02);
            trgsf_down = (trgsf_nominal * 0.98);
        }
    }

    return ROOT::RVecF{trgsf_up, trgsf_down};
}

/**********************************************************/


// https://github.com/hftsoi/aabbtt_finalselection/blob/master/selection_mt_allyears.cc#L1530-L1578

//https://github.com/hftsoi/aabbtt_finalselection/blob/master/selection_et_allyears.cc#L1527-L1599

template <typename T>
auto GetTauTauTriggerEfficiencyShifts_experimental(T &df, LUNA::sampleConfig_t &sConfig, TString jsonDirEmb) {

    int year = sConfig.year();
    int isEmbedded = sConfig.isEmbedded();
    int isMC = sConfig.isMC();

    // Nominal trgsf_single is by year and by embedded sample
    if (sConfig.isData()) {
        return df.Define("weight_mt_trgsf_nominal", [] { return 1.0f; })
                 .Define("weight_et_trgsf_nominal", [] { return 1.0f; })
                 .Define("weight_em_trgsf_nominal", [] { return 1.0f; });
    }
    // single muon trigger efficiencies for Embed and MC are from the CROWN data files
    TString cset_dir_muon = TString::Format("%smuon_%sUL.json", jsonDirEmb.Data(), sConfig.era().c_str());
    auto cset_muon = CorrectionSet::from_file(cset_dir_muon.Data());
    TString muonCorrectionSetToGet = "Trg_IsoMu27_or_IsoMu24_pt_eta_bins";
    if (year == 2016) {
        // 2016 had different paths: this key is for (IsoMu22|IsoTkMu22|IsoMu22_eta2p1|IsoTkMu22_eta2p1)
        muonCorrectionSetToGet = "Trg_pt_eta_bins";
    }
    auto cset_muonTrg = cset_muon->at(muonCorrectionSetToGet.Data()); // for mu_iso < 0.15

    // single electron trigger efficiencies for Embed and MC are from the CROWN data files
    TString cset_dir_ele = TString::Format("%selectron_%sUL.json", jsonDirEmb.Data(), sConfig.era().c_str());
    auto cset_ele = CorrectionSet::from_file(cset_dir_ele.Data());
    TString electronCorrectionSetToGet = "Trg32_or_Trg35_Iso_pt_eta_bins";
    if (year == 2016) {
        // 2016 has only one electron single trigger. Accessing the other key will give an error for std::out_of_range and what(): map::at
        electronCorrectionSetToGet = "Trg25_Iso_pt_eta_bins";
    }
    auto cset_eleTrg = cset_ele->at(electronCorrectionSetToGet.Data()); // for ele_iso < 0.15

    // etau and mutau cross-trigger efficiencies for Embed and MC are from the CROWN data files
    TString cset_dir_tau = TString::Format("%stau_trigger%s_UL.json", jsonDirEmb.Data(), sConfig.era().c_str());
    auto cset_tau = CorrectionSet::from_file(cset_dir_tau.Data());
    auto cset_tauTrg = cset_tau->at("tauTriggerSF");

    // Get nominal weights
    auto df2 = df.Define("weight_mt_trgsf_nominal", [cset_muonTrg, cset_tauTrg, isMC, isEmbedded](const int channel, const float pt_1, const float eta_1, const float pt_2, const int decayMode_2, const int isSingleTrigger, const int isCrossTrigger) {
                    if (channel == Helper::mt) {
                        if (isMC && isSingleTrigger)       { return (float) cset_muonTrg->evaluate({pt_1, abs(eta_1), "mc"}); }
                        if (isEmbedded && isSingleTrigger) { return (float) cset_muonTrg->evaluate({pt_1, abs(eta_1), "emb"}); }
                        if (isCrossTrigger) { return (float) cset_tauTrg->evaluate({pt_2, decayMode_2, "mutau", "Medium", "sf", "nom"}); }
                    }
                    return 1.0f;
                 }, {"channel", "pt_1_nominal", "eta_1", "pt_2_nominal", "decayMode_2", "passSingleTrigger_mt_nominal", "passCrossTrigger_mt_nominal"})
                 // Note in 2016 there was no etau cross-trigger
                 .Define("weight_et_trgsf_nominal", [cset_eleTrg, cset_tauTrg, year, isMC, isEmbedded](const int channel, const float pt_1, const float eta_1, const float pt_2, const int decayMode_2, const int isSingleTrigger, const int isCrossTrigger) {
                    if (channel == Helper::et) {
                        if (isMC && isSingleTrigger)       { return (float) cset_eleTrg->evaluate({pt_1, abs(eta_1), "mc"}); }
                        if (isEmbedded && isSingleTrigger) { return (float) cset_eleTrg->evaluate({pt_1, abs(eta_1), "emb"}); }
                        if (isCrossTrigger) { if ((year == 2017) || (year == 2018)) { return (float) cset_tauTrg->evaluate({pt_2, decayMode_2, "etau", "Medium", "sf", "nom"}); } else { return 1.0f; } }
                    }
                    return 1.0f;
                 }, {"channel", "pt_1_nominal", "eta_1", "pt_2_nominal", "decayMode_2", "passSingleTrigger_et_nominal", "passCrossTrigger_et_nominal"})
                 .Define("weight_em_trgsf_nominal", getEMuTriggerSF_nominal, {"channel",
                                                                      "effMC_ele_Mu23Ele12", "effMC_muo_Mu23Ele12", "effMC_ele_Mu8Ele23", "effMC_muo_Mu8Ele23",
                                                                      "effData_ele_Mu23Ele12", "effData_muo_Mu23Ele12", "effData_ele_Mu8Ele23", "effData_muo_Mu8Ele23",
                                                                      "pt_1_nominal", "eta_1", "pt_2_nominal", "eta_2",
                                                                      "passCross_leadingEle_em_nominal", "passCross_leadingMuon_em_nominal", "em_crossLeadingEle_elept_thres", "em_crossLeadingEle_mupt_thres", "em_crossLeadingMuon_elept_thres", "em_crossLeadingMuon_mupt_thres"});

    if (sConfig.doShifts()) {

        return df2.Define("trgeff_single", []() { return (std::string) "trgeff_single"; })
                  .Define("trgeff_cross",  []() { return (std::string) "trgeff_cross";  })
                  .Define("trgeff_Mu8E23_em",  []() { return (std::string) "trgeff_Mu8E23_em"; })
                  .Define("trgeff_Mu23E12_em", []() { return (std::string) "trgeff_Mu23E12_em"; })
                  .Define("trgeff_both_em",    []() { return (std::string) "trgeff_both_em"; })

                  // mutau: Shifts of the single trigger efficiency: up variation is 1.02 of nominal efficiency, and down variation is 0.98 of nominal efficiency
                  .Define("weight_mt_trgsf_trgeff_single_Up",  [](const int channel, const int isSingleTrigger, const float weight_mt_trgsf_nominal) {
                        if ((channel == Helper::mt) && isSingleTrigger) { return weight_mt_trgsf_nominal * 1.02f; }
                        else { return weight_mt_trgsf_nominal; }  // this is 1.0f if not mutau, or the nominal cross-trigger weight if crosstrigger
                    }, {"channel", "passSingleTrigger_mt_nominal", "weight_mt_trgsf_nominal"})
                  .Define("weight_mt_trgsf_trgeff_single_Down",  [](const int channel, const int isSingleTrigger, const float weight_mt_trgsf_nominal) {
                        if ((channel == Helper::mt) && isSingleTrigger) { return weight_mt_trgsf_nominal * 0.98f; }
                        else { return weight_mt_trgsf_nominal; }  // this is 1.0f if not mutau, or the nominal cross-trigger weight if crosstrigger
                    }, {"channel", "passSingleTrigger_mt_nominal", "weight_mt_trgsf_nominal"})
                    // mutau: Shift of cross-trigger efficiency needs to be taken from the CorrectionSet
                  .Define("weight_mt_trgsf_trgeff_cross_Up", [cset_tauTrg](const int channel, const float pt_2, const int decayMode_2, const int isCrossTrigger, const float weight_mt_trgsf_nominal) {
                        if ((channel == Helper::mt) && isCrossTrigger) { return (float) cset_tauTrg->evaluate({pt_2, decayMode_2, "mutau", "Medium", "sf", "up"}); }
                        else { return weight_mt_trgsf_nominal; }
                    }, {"channel", "pt_2_nominal", "decayMode_2", "passCrossTrigger_mt_nominal", "weight_mt_trgsf_nominal"})
                  .Define("weight_mt_trgsf_trgeff_cross_Down", [cset_tauTrg](const int channel, const float pt_2, const int decayMode_2, const int isCrossTrigger, const float weight_mt_trgsf_nominal) {
                        if ((channel == Helper::mt) && isCrossTrigger) { return (float) cset_tauTrg->evaluate({pt_2, decayMode_2, "mutau", "Medium", "sf", "down"}); }
                        else { return weight_mt_trgsf_nominal; }
                    }, {"channel", "pt_2_nominal", "decayMode_2", "passCrossTrigger_mt_nominal", "weight_mt_trgsf_nominal"})

                  // etau: Shifts of the single trigger efficiency: up variation is 1.02 of nominal efficiency, and down variation is 0.98 of nominal efficiency
                   .Define("weight_et_trgsf_trgeff_single_Up",  [](const int channel, const int isSingleTrigger, const float weight_et_trgsf_nominal) {
                        if ((channel == Helper::et) && isSingleTrigger) { return weight_et_trgsf_nominal * 1.02f; }
                        else { return weight_et_trgsf_nominal; }  // this is 1.0f if not etau, or the nominal cross-trigger weight if crosstrigger
                    }, {"channel", "passSingleTrigger_et_nominal", "weight_et_trgsf_nominal"})
                   .Define("weight_et_trgsf_trgeff_single_Down",  [](const int channel, const int isSingleTrigger, const float weight_et_trgsf_nominal) {
                        if ((channel == Helper::et) && isSingleTrigger) { return weight_et_trgsf_nominal * 0.98f; }
                        else { return weight_et_trgsf_nominal; }  // this is 1.0f if not etau, or the nominal cross-trigger weight if crosstrigger
                    }, {"channel", "passSingleTrigger_et_nominal", "weight_et_trgsf_nominal"})

                    // etau: Shift of cross-trigger efficiency needs to be taken from the CorrectionSet. Note that in 2016 there was no etau cross-trigger.
                   .Define("weight_et_trgsf_trgeff_cross_Up", [cset_tauTrg, year](const int channel, const float pt_2, const int decayMode_2, const int isCrossTrigger, const float weight_et_trgsf_nominal) {
                        if ((channel == Helper::et) && isCrossTrigger && ((year == 2017) || (year == 2018))) { return (float) cset_tauTrg->evaluate({pt_2, decayMode_2, "etau", "Medium", "sf", "up"}); }
                        else { return weight_et_trgsf_nominal; }
                    }, {"channel", "pt_2_nominal", "decayMode_2", "passCrossTrigger_et_nominal", "weight_et_trgsf_nominal"})
                  .Define("weight_et_trgsf_trgeff_cross_Down", [cset_tauTrg, year](const int channel, const float pt_2, const int decayMode_2, const int isCrossTrigger, const float weight_et_trgsf_nominal) {
                        if ((channel == Helper::et) && isCrossTrigger && ((year == 2017) || (year == 2018))) { return (float) cset_tauTrg->evaluate({pt_2, decayMode_2, "etau", "Medium", "sf", "down"}); }
                        else { return weight_et_trgsf_nominal; }
                    }, {"channel", "pt_2_nominal", "decayMode_2", "passCrossTrigger_et_nominal", "weight_et_trgsf_nominal"})

                  // emu systematics (20%)
                  .Define("variations_weight_em_trgsf_trgeff_Mu8E23", getEMuTriggerSF_variations, {"trgeff_Mu8E23_em", "weight_em_trgsf_nominal", "channel", "pt_1_nominal", "pt_2_nominal",
                                                                        "passCross_leadingEle_em_nominal", "passCross_leadingMuon_em_nominal", "em_crossLeadingEle_elept_thres", "em_crossLeadingEle_mupt_thres", "em_crossLeadingMuon_elept_thres", "em_crossLeadingMuon_mupt_thres"})
                  .Define("variations_weight_em_trgsf_trgeff_Mu23E12", getEMuTriggerSF_variations, {"trgeff_Mu23E12_em", "weight_em_trgsf_nominal", "channel", "pt_1_nominal", "pt_2_nominal",
                                                                        "passCross_leadingEle_em_nominal", "passCross_leadingMuon_em_nominal", "em_crossLeadingEle_elept_thres", "em_crossLeadingEle_mupt_thres", "em_crossLeadingMuon_elept_thres", "em_crossLeadingMuon_mupt_thres"})
                  .Define("variations_weight_em_trgsf_trgeff_both", getEMuTriggerSF_variations, {"trgeff_both_em", "weight_em_trgsf_nominal", "channel", "pt_1_nominal", "pt_2_nominal",
                                                                        "passCross_leadingEle_em_nominal", "passCross_leadingMuon_em_nominal", "em_crossLeadingEle_elept_thres", "em_crossLeadingEle_mupt_thres", "em_crossLeadingMuon_elept_thres", "em_crossLeadingMuon_mupt_thres"})
                   // emu: unpack
                  .Define("weight_em_trgsf_trgeff_Mu8E23_Up",    "variations_weight_em_trgsf_trgeff_Mu8E23[0]")
                  .Define("weight_em_trgsf_trgeff_Mu8E23_Down",  "variations_weight_em_trgsf_trgeff_Mu8E23[1]")
                  .Define("weight_em_trgsf_trgeff_Mu23E12_Up",   "variations_weight_em_trgsf_trgeff_Mu23E12[0]")
                  .Define("weight_em_trgsf_trgeff_Mu23E12_Down", "variations_weight_em_trgsf_trgeff_Mu23E12[1]")
                  .Define("weight_em_trgsf_trgeff_both_Up",      "variations_weight_em_trgsf_trgeff_both[0]")
                  .Define("weight_em_trgsf_trgeff_both_Down",    "variations_weight_em_trgsf_trgeff_both[1]")

                // Shifts of the trigger SF due to shifts of the lepton energy scale(s): note the shift of the pT AND the trigger decisions
                .Define("weight_mt_trgsf_es1Up", [cset_muonTrg, cset_tauTrg, isMC, isEmbedded](const int channel, const float pt_1, const float eta_1, const float pt_2, const int decayMode_2, const int isSingleTrigger, const int isCrossTrigger, const float weight_mt_trgsf_nominal) {
                    if (channel == Helper::mt) {
                        if (isSingleTrigger && isMC)       { return (float) cset_muonTrg->evaluate({pt_1, abs(eta_1), "mc"}); }
                        if (isSingleTrigger && isEmbedded) { return (float) cset_muonTrg->evaluate({pt_1, abs(eta_1), "emb"});}
                        // note use of shifted input pT BUT nominal trigger efficiency
                        else if (isCrossTrigger) { return (float) cset_tauTrg->evaluate({pt_2, decayMode_2, "mutau", "Medium", "sf", "nom"}); }
                    }
                    return weight_mt_trgsf_nominal;
                 }, {"channel", "pt_1_es1Up", "eta_1", "pt_2_nominal", "decayMode_2", "passSingleTrigger_mt_es1Up", "passCrossTrigger_mt_es1Up", "weight_mt_trgsf_nominal"})
                .Define("weight_mt_trgsf_es1Down", [cset_muonTrg, cset_tauTrg, isMC, isEmbedded](const int channel, const float pt_1, const float eta_1, const float pt_2, const int decayMode_2, const int isSingleTrigger, const int isCrossTrigger, const float weight_mt_trgsf_nominal) {
                    if (channel == Helper::mt) {
                        if (isSingleTrigger && isMC)       { return (float) cset_muonTrg->evaluate({pt_1, abs(eta_1), "mc"}); }
                        if (isSingleTrigger && isEmbedded) { return (float) cset_muonTrg->evaluate({pt_1, abs(eta_1), "emb"});}
                        // note use of shifted input pT BUT nominal trigger efficiency
                        else if (isCrossTrigger) { return (float) cset_tauTrg->evaluate({pt_2, decayMode_2, "mutau", "Medium", "sf", "nom"}); }
                    }
                    return weight_mt_trgsf_nominal;
                }, {"channel", "pt_1_es1Down", "eta_1", "pt_2_nominal", "decayMode_2", "passSingleTrigger_mt_es1Down", "passCrossTrigger_mt_es1Down", "weight_mt_trgsf_nominal"})

                .Define("weight_mt_trgsf_es2Up", [cset_muonTrg, cset_tauTrg, isMC, isEmbedded](const int channel, const float pt_1, const float eta_1, const float pt_2, const int decayMode_2, const int isSingleTrigger, const int isCrossTrigger, const float weight_mt_trgsf_nominal) {
                    if (channel == Helper::mt) {
                        if (isSingleTrigger && isMC)       { return (float) cset_muonTrg->evaluate({pt_1, abs(eta_1), "mc"}); }
                        if (isSingleTrigger && isEmbedded) { return (float) cset_muonTrg->evaluate({pt_1, abs(eta_1), "emb"});}
                        // note use of shifted input pT BUT nominal trigger efficiency
                        else if (isCrossTrigger) { return (float) cset_tauTrg->evaluate({pt_2, decayMode_2, "mutau", "Medium", "sf", "nom"}); }
                    }
                    return weight_mt_trgsf_nominal;
                 }, {"channel", "pt_1_nominal", "eta_1", "pt_2_es2Up", "decayMode_2", "passSingleTrigger_mt_es2Up", "passCrossTrigger_mt_es2Up", "weight_mt_trgsf_nominal"})
                .Define("weight_mt_trgsf_es2Down", [cset_muonTrg, cset_tauTrg, isMC, isEmbedded](const int channel, const float pt_1, const float eta_1, const float pt_2, const int decayMode_2, const int isSingleTrigger, const int isCrossTrigger, const float weight_mt_trgsf_nominal) {
                    if (channel == Helper::mt) {
                        if (isSingleTrigger && isMC)       { return (float) cset_muonTrg->evaluate({pt_1, abs(eta_1), "mc"}); }
                        if (isSingleTrigger && isEmbedded) { return (float) cset_muonTrg->evaluate({pt_1, abs(eta_1), "emb"});}
                        // note use of shifted input pT BUT nominal trigger efficiency
                        else if (isCrossTrigger) { return (float) cset_tauTrg->evaluate({pt_2, decayMode_2, "mutau", "Medium", "sf", "nom"}); }
                    }
                    return weight_mt_trgsf_nominal;
                 }, {"channel", "pt_1_nominal", "eta_1", "pt_2_es2Down", "decayMode_2", "passSingleTrigger_mt_es2Down", "passCrossTrigger_mt_es2Down", "weight_mt_trgsf_nominal"})

                // etau: shifts of the trigger weight due to energy scale shifts of the electron and the tau
                .Define("weight_et_trgsf_es1Up", [cset_eleTrg, isMC, isEmbedded]( const int channel, const float pt_1, const float eta_1, const int isSingleTrigger, const float weight_et_trgsf_nominal) {
                    if (channel == Helper::et) {
                        if (isMC && isSingleTrigger)       { return (float) cset_eleTrg->evaluate({pt_1, abs(eta_1), "mc"}); }
                        if (isEmbedded && isSingleTrigger) { return (float) cset_eleTrg->evaluate({pt_1, abs(eta_1), "emb"}); }
                        // electron up/down shift: if cross trigger simply do not re-evaluate
                    }
                    return weight_et_trgsf_nominal;
                }, {"channel", "pt_1_es1Up", "eta_1", "passSingleTrigger_et_es1Up", "weight_et_trgsf_nominal"})

                .Define("weight_et_trgsf_es1Down", [cset_eleTrg, isMC, isEmbedded](const int channel, const float pt_1, const float eta_1, const int isSingleTrigger, const float weight_et_trgsf_nominal) {
                    if (channel == Helper::et) {
                        if (isMC && isSingleTrigger)       { return (float) cset_eleTrg->evaluate({pt_1, abs(eta_1), "mc"}); }
                        if (isEmbedded && isSingleTrigger) { return (float) cset_eleTrg->evaluate({pt_1, abs(eta_1), "emb"}); }
                        // electron up/down shift: if cross-trigger simply do not re-evalute
                    }
                    return weight_et_trgsf_nominal;
                }, {"channel", "pt_1_es1Down", "eta_1", "passSingleTrigger_et_es1Down", "weight_et_trgsf_nominal"})

                // etau: shifts due to tau energy scale : do not need to do any math for single trigger events. Note that in 2016 there is no etau cross-trigger.
                .Define("weight_et_trgsf_es2Up", [cset_tauTrg, year](const int channel, const float pt_2, const int decayMode_2, const int isCrossTrigger, const float weight_et_trgsf_nominal) {
                    if ((channel == Helper::et) && isCrossTrigger && ((year == 2017) || (year == 2018))) { return (float) cset_tauTrg->evaluate({pt_2, decayMode_2, "etau", "Medium", "sf", "nom"}); }
                    return weight_et_trgsf_nominal;
                }, {"channel", "pt_2_es2Up", "decayMode_2", "passCrossTrigger_et_es2Up", "weight_et_trgsf_nominal"})

                .Define("weight_et_trgsf_es2Down", [cset_tauTrg, year](const int channel, const float pt_2, const int decayMode_2, const int isCrossTrigger, const float weight_et_trgsf_nominal) {
                    if ((channel == Helper::et) && isCrossTrigger  && ((year == 2017) || (year == 2018))) { return (float) cset_tauTrg->evaluate({pt_2, decayMode_2, "etau", "Medium", "sf", "nom"}); }
                    return weight_et_trgsf_nominal;
                }, {"channel", "pt_2_es2Down", "decayMode_2", "passCrossTrigger_et_es2Down", "weight_et_trgsf_nominal"})

                // UL, handle es1 Up/Down shifts
                 .Define("weight_em_trgsf_es1Up", getEMuTriggerSF_nominal, {"channel",
                                                                      "effMC_ele_Mu23Ele12", "effMC_muo_Mu23Ele12", "effMC_ele_Mu8Ele23", "effMC_muo_Mu8Ele23",
                                                                      "effData_ele_Mu23Ele12", "effData_muo_Mu23Ele12", "effData_ele_Mu8Ele23", "effData_muo_Mu8Ele23",
                                                                      "pt_1_es1Up", "eta_1", "pt_2_nominal", "eta_2",
                                                                      "passCross_leadingEle_em_nominal", "passCross_leadingMuon_em_nominal", "em_crossLeadingEle_elept_thres", "em_crossLeadingEle_mupt_thres", "em_crossLeadingMuon_elept_thres", "em_crossLeadingMuon_mupt_thres"})

                 .Define("weight_em_trgsf_es1Down", getEMuTriggerSF_nominal, {"channel",
                                                                      "effMC_ele_Mu23Ele12", "effMC_muo_Mu23Ele12", "effMC_ele_Mu8Ele23", "effMC_muo_Mu8Ele23",
                                                                      "effData_ele_Mu23Ele12", "effData_muo_Mu23Ele12", "effData_ele_Mu8Ele23", "effData_muo_Mu8Ele23",
                                                                      "pt_1_es1Down", "eta_1", "pt_2_nominal", "eta_2",
                                                                      "passCross_leadingEle_em_nominal", "passCross_leadingMuon_em_nominal", "em_crossLeadingEle_elept_thres", "em_crossLeadingEle_mupt_thres", "em_crossLeadingMuon_elept_thres", "em_crossLeadingMuon_mupt_thres"})

                // UL, make sure es1 and es2 shifts are accounted for
                 .Define("weight_em_trgsf_es2Up", getEMuTriggerSF_nominal, {"channel",
                                                                            "effMC_ele_Mu23Ele12", "effMC_muo_Mu23Ele12", "effMC_ele_Mu8Ele23", "effMC_muo_Mu8Ele23",
                                                                            "effData_ele_Mu23Ele12", "effData_muo_Mu23Ele12", "effData_ele_Mu8Ele23", "effData_muo_Mu8Ele23",
                                                                            "pt_1_nominal", "eta_1", "pt_2_es2Up", "eta_2",
                                                                            "passCross_leadingEle_em_nominal", "passCross_leadingMuon_em_nominal", "em_crossLeadingEle_elept_thres", "em_crossLeadingEle_mupt_thres", "em_crossLeadingMuon_elept_thres", "em_crossLeadingMuon_mupt_thres"})


                 .Define("weight_em_trgsf_es2Down", getEMuTriggerSF_nominal, {"channel",
                                                                              "effMC_ele_Mu23Ele12", "effMC_muo_Mu23Ele12", "effMC_ele_Mu8Ele23", "effMC_muo_Mu8Ele23",
                                                                              "effData_ele_Mu23Ele12", "effData_muo_Mu23Ele12", "effData_ele_Mu8Ele23", "effData_muo_Mu8Ele23",
                                                                              "pt_1_nominal", "eta_1", "pt_2_es2Down", "eta_2",
                                                                              "passCross_leadingEle_em_nominal", "passCross_leadingMuon_em_nominal", "em_crossLeadingEle_elept_thres", "em_crossLeadingEle_mupt_thres", "em_crossLeadingMuon_elept_thres", "em_crossLeadingMuon_mupt_thres"});



    }
    else {
        return df2;
    }

}


/**********************************************************/

#endif
