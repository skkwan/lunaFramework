#ifndef JET_FAKE_BACKGROUND_EXPERIMENTAL_H_INCL
#define JET_FAKE_BACKGROUND_EXPERIMENTAL_H_INCL

#include "helperFunctions.h"
#include "sampleConfig_class.h"

/*******************************************************************/

/*
 * Fake rate depends on tau pT only: not by DM
 */
float getJetFakeWeight_nominal(int channel, float tauPt, int byVVVLooseDeepVSjet_2, int byMediumDeepVSjet_2,
                               Double_t *taufr_y) {
  float fr = 0.0;
  float weight_ff = 1.0;

  // Iso region: do not get the fake rate, return 1.0
  if ((byVVVLooseDeepVSjet_2 > 0) && (byMediumDeepVSjet_2 > 0)) {
    return 1.0f;
  }

  //  [20, 25), [25, 30), [30, 35), [35, 40), [40, 50), [50, 60), [60, 80), [80, 100), [100, 120), [120, 150), and [150, +)
  if ((channel == Helper::mt) || (channel == Helper::et)) {
    if (tauPt < 25)
      fr = taufr_y[0];
    else if ((tauPt >= 25) && (tauPt < 30))
      fr = taufr_y[1];
    else if ((tauPt >= 30) && (tauPt < 35))
      fr = taufr_y[2];
    else if ((tauPt >= 35) && (tauPt < 40))
      fr = taufr_y[3];
    else if ((tauPt >= 40) && (tauPt < 50))
      fr = taufr_y[4];
    else if ((tauPt >= 50) && (tauPt < 60))
      fr = taufr_y[5];
    else if ((tauPt >= 60) && (tauPt < 80))
      fr = taufr_y[6];
    else if ((tauPt >= 80) && (tauPt < 100))
      fr = taufr_y[7];
    else if ((tauPt >= 100) && (tauPt < 120))
      fr = taufr_y[8];
    else if ((tauPt >= 120) && (tauPt < 150))
      fr = taufr_y[9];
    else if (tauPt >= 150)
      fr = taufr_y[10];
  }
  weight_ff = (fr / (1.0 - fr));
  return weight_ff;
}

/*******************************************************************/

/*
 * GetTauFR.h:
 * https://github.com/hftsoi/aabbtt_finalselection/blob/cefb486856b4c817d20c6b4c49d17eaef115f78e/GetTauFR.h
 * Each TGraphAsymmErrors has six bins ordered in [0, 25], [25, 30], [30, 35],
 * [35, 40], [40, 50], [50, 60], [60+]
 */
ROOT::RVecF getJetFakeWeight_variations(
    int channel, float tauPt, int byVVVLooseDeepVSjet_2, int byMediumDeepVSjet_2, float nominal_weight,
    Double_t *taufr_y,
    Double_t *taufr_ylow, Double_t *taufr_yhigh,
    std::string sysToDo) {

  // Iso region: do not get the fake rate
  if ((byVVVLooseDeepVSjet_2 > 0) && (byMediumDeepVSjet_2 > 0)) {
    return ROOT::RVecF{1.0f, 1.0f};
  }

  float fr = 0.0;
  float uncUp = 0.0;
  float uncDown = 0.0;
  float frUp = 0.0;
  float frDown = 0.0;
  float weightUp = nominal_weight;
  float weightDown = nominal_weight;

  int isShifted = false;
  if ((channel == Helper::mt) || (channel == Helper::et)) {
    if ((sysToDo == "jetFR_pt0to25") && (tauPt < 25)) {
      isShifted = true;
      fr = taufr_y[0];
      uncUp = taufr_yhigh[0];
      uncDown = taufr_ylow[0];
    } else if ((sysToDo == "jetFR_pt25to30") && (tauPt >= 25) && (tauPt < 30)) {
      isShifted = true;
      fr = taufr_y[1];
      uncUp = taufr_yhigh[1];
      uncDown = taufr_ylow[1];
    } else if ((sysToDo == "jetFR_pt30to35") && (tauPt >= 30) && (tauPt < 35)) {
      isShifted = true;
      fr = taufr_y[2];
      uncUp = taufr_yhigh[2];
      uncDown = taufr_ylow[2];
    } else if ((sysToDo == "jetFR_pt35to40") && (tauPt >= 35) && (tauPt < 40)) {
      isShifted = true;
      fr = taufr_y[3];
      uncUp = taufr_yhigh[3];
      uncDown = taufr_ylow[3];
    } else if ((sysToDo == "jetFR_pt40to50") && (tauPt >= 40) && (tauPt < 50)) {
      isShifted = true;
      fr = taufr_y[4];
      uncUp = taufr_yhigh[4];
      uncDown = taufr_ylow[4];
    } else if ((sysToDo == "jetFR_pt50to60") && (tauPt >= 50) && (tauPt < 60)) {
      isShifted = true;
      fr = taufr_y[5];
      uncUp = taufr_yhigh[5];
      uncDown = taufr_ylow[5];
    } else if ((sysToDo == "jetFR_pt60to80") && (tauPt >= 60) && (tauPt < 80)) {
      isShifted = true;
      fr = taufr_y[6];
      uncUp = taufr_yhigh[6];
      uncDown = taufr_ylow[6];
    }
    else if ((sysToDo == "jetFR_pt80to100")  && (tauPt >= 80) && (tauPt < 100)) {
      isShifted = true;
      fr = taufr_y[7];
      uncUp = taufr_yhigh[7];
      uncDown = taufr_ylow[7];
    }
    else if ((sysToDo == "jetFR_pt100to120")  && (tauPt >= 100) && (tauPt < 120)) {
      isShifted = true;
      fr = taufr_y[8];
      uncUp = taufr_yhigh[8];
      uncDown = taufr_ylow[8];
    }
    else if ((sysToDo == "jetFR_pt120to150")  && (tauPt >= 120) && (tauPt < 150)) {
      isShifted = true;
      fr = taufr_y[9];
      uncUp = taufr_yhigh[9];
      uncDown = taufr_ylow[9];
    }
    else if ((sysToDo == "jetFR_ptgt150")  && (tauPt >= 150)) {
      isShifted = true;
      fr = taufr_y[10];
      uncUp = taufr_yhigh[10];
      uncDown = taufr_ylow[10];
    }
  }
  if (isShifted) {
    frUp = (fr + uncUp);
    frDown = (fr - uncDown);
    weightUp = (frUp / (1.0 - frUp));
    weightDown = (frDown / (1.0 - frDown));
  }
  return ROOT::RVecF{weightUp, weightDown};
}

/*******************************************************************/

/*
 * In the mutau and etau channels, the jet fake background is estimated by
 * re-weighing anti-isolated events by a scale factor, and subtracting the sum
 * of the MC re-weighted anti-iso histograms, from the data re-weighted anti-iso
 * histograms. This function calculates the weight that needs to be applied to
 * the anti-iso events, and the up/down shifts binned in pT. The anti-iso
 * selection needs to be done elsewhere in the code.
 *  Bins: [20, 25), [25, 30), [30, 35), [35, 40), [40, 50), [50, 60), [60, 80), [80, 100), [100, 120), [120, 150), and [150, +)
 */

template <typename T>
auto GetJetFakeBackgroundScaleFactor_experimental(
    T &df, LUNA::sampleConfig_t &sConfig,
    TGraphAsymmErrors *ptrTGraph_medium_vsVVVL) {

  // can't use Eval() because it is an interpolation: instead get the bins
  Double_t *taufr_y = ptrTGraph_medium_vsVVVL->GetY();
  Double_t *taufr_ylow = ptrTGraph_medium_vsVVVL->GetEYlow();
  Double_t *taufr_yhigh = ptrTGraph_medium_vsVVVL->GetEYhigh();

  auto df2 =
      df.Define("taufr_y", [taufr_y]() { return taufr_y; })
          .Define("taufr_ylow", [taufr_ylow]() { return taufr_ylow; })
          .Define("taufr_yhigh", [taufr_yhigh]() { return taufr_yhigh; })
          // note the use of pt_2_nominal which has the tauES for mutau and eleES
          // for etau
          .Define("weight_ff_nominal", getJetFakeWeight_nominal,
                  {"channel", "pt_2_nominal", "byVVVLooseDeepVSjet_2", "byMediumDeepVSjet_2", "taufr_y"});

  if (sConfig.doShifts()) {
    return df2
        .Define("jetFR_pt0to25", [] { return (std::string) "jetFR_pt0to25"; })
        .Define("jetFR_pt25to30", [] { return (std::string) "jetFR_pt25to30"; })
        .Define("jetFR_pt30to35", [] { return (std::string) "jetFR_pt30to35"; })
        .Define("jetFR_pt35to40", [] { return (std::string) "jetFR_pt35to40"; })
        .Define("jetFR_pt40to50", [] { return (std::string) "jetFR_pt40to50"; })
        .Define("jetFR_pt50to60", [] { return (std::string) "jetFR_pt50to60"; })
        .Define("jetFR_pt60to80", [] { return (std::string) "jetFR_pt60to80"; })
        .Define("jetFR_pt80to100", [] { return (std::string) "jetFR_pt80to100"; })
        .Define("jetFR_pt100to120", [] { return (std::string) "jetFR_pt100to120"; })
        .Define("jetFR_pt120to150", [] { return (std::string) "jetFR_pt120to150"; })
        .Define("jetFR_ptgt150", [] { return (std::string) "jetFR_ptgt150"; })

        .Define("variations_weight_ff_jetFR_pt0to25", getJetFakeWeight_variations,
              {"channel", "pt_2_nominal", "byVVVLooseDeepVSjet_2", "byMediumDeepVSjet_2", "weight_ff_nominal", "taufr_y",
               "taufr_ylow", "taufr_yhigh",
               "jetFR_pt0to25"})
        .Define("variations_weight_ff_jetFR_pt25to30", getJetFakeWeight_variations,
              {"channel", "pt_2_nominal", "byVVVLooseDeepVSjet_2", "byMediumDeepVSjet_2", "weight_ff_nominal", "taufr_y",
               "taufr_ylow", "taufr_yhigh",
               "jetFR_pt25to30"})
        .Define("variations_weight_ff_jetFR_pt30to35", getJetFakeWeight_variations,
              {"channel", "pt_2_nominal", "byVVVLooseDeepVSjet_2", "byMediumDeepVSjet_2", "weight_ff_nominal", "taufr_y",
               "taufr_ylow", "taufr_yhigh",
               "jetFR_pt30to35"})
        .Define("variations_weight_ff_jetFR_pt35to40", getJetFakeWeight_variations,
              {"channel", "pt_2_nominal", "byVVVLooseDeepVSjet_2", "byMediumDeepVSjet_2", "weight_ff_nominal", "taufr_y",
               "taufr_ylow", "taufr_yhigh",
               "jetFR_pt35to40"})
        .Define("variations_weight_ff_jetFR_pt40to50", getJetFakeWeight_variations,
              {"channel", "pt_2_nominal", "byVVVLooseDeepVSjet_2", "byMediumDeepVSjet_2", "weight_ff_nominal", "taufr_y",
               "taufr_ylow", "taufr_yhigh",
               "jetFR_pt40to50"})
        .Define("variations_weight_ff_jetFR_pt50to60", getJetFakeWeight_variations,
              {"channel", "pt_2_nominal", "byVVVLooseDeepVSjet_2", "byMediumDeepVSjet_2", "weight_ff_nominal", "taufr_y",
               "taufr_ylow", "taufr_yhigh",
               "jetFR_pt50to60"})
        .Define("variations_weight_ff_jetFR_pt60to80", getJetFakeWeight_variations,
              {"channel", "pt_2_nominal", "byVVVLooseDeepVSjet_2", "byMediumDeepVSjet_2", "weight_ff_nominal", "taufr_y",
               "taufr_ylow", "taufr_yhigh",
               "jetFR_pt60to80"})
        .Define("variations_weight_ff_jetFR_pt80to100", getJetFakeWeight_variations,
              {"channel", "pt_2_nominal", "byVVVLooseDeepVSjet_2", "byMediumDeepVSjet_2", "weight_ff_nominal", "taufr_y",
               "taufr_ylow", "taufr_yhigh",
               "jetFR_pt80to100"})
        .Define("variations_weight_ff_jetFR_pt100to120", getJetFakeWeight_variations,
              {"channel", "pt_2_nominal", "byVVVLooseDeepVSjet_2", "byMediumDeepVSjet_2", "weight_ff_nominal", "taufr_y",
               "taufr_ylow", "taufr_yhigh",
               "jetFR_pt100to120"})
        .Define("variations_weight_ff_jetFR_pt120to150", getJetFakeWeight_variations,
              {"channel", "pt_2_nominal", "byVVVLooseDeepVSjet_2", "byMediumDeepVSjet_2", "weight_ff_nominal", "taufr_y",
               "taufr_ylow", "taufr_yhigh",
               "jetFR_pt120to150"})
        .Define("variations_weight_ff_jetFR_ptgt150", getJetFakeWeight_variations,
              {"channel", "pt_2_nominal", "byVVVLooseDeepVSjet_2", "byMediumDeepVSjet_2", "weight_ff_nominal", "taufr_y",
               "taufr_ylow", "taufr_yhigh",
               "jetFR_ptgt150"})
        // unpack
        .Define("weight_ff_jetFR_pt0to25_Up",  "variations_weight_ff_jetFR_pt0to25[0]")
        .Define("weight_ff_jetFR_pt25to30_Up", "variations_weight_ff_jetFR_pt25to30[0]")
        .Define("weight_ff_jetFR_pt30to35_Up", "variations_weight_ff_jetFR_pt30to35[0]")
        .Define("weight_ff_jetFR_pt35to40_Up", "variations_weight_ff_jetFR_pt35to40[0]")
        .Define("weight_ff_jetFR_pt40to50_Up", "variations_weight_ff_jetFR_pt40to50[0]")
        .Define("weight_ff_jetFR_pt50to60_Up", "variations_weight_ff_jetFR_pt50to60[0]")
        .Define("weight_ff_jetFR_pt60to80_Up",   "variations_weight_ff_jetFR_pt60to80[0]")
        .Define("weight_ff_jetFR_pt80to100_Up",   "variations_weight_ff_jetFR_pt80to100[0]")
        .Define("weight_ff_jetFR_pt100to120_Up",   "variations_weight_ff_jetFR_pt100to120[0]")
        .Define("weight_ff_jetFR_pt120to150_Up",  "variations_weight_ff_jetFR_pt120to150[0]")
        .Define("weight_ff_jetFR_ptgt150_Up",   "variations_weight_ff_jetFR_ptgt150[0]")
        .Define("weight_ff_jetFR_pt0to25_Down", "variations_weight_ff_jetFR_pt0to25[1]")
        .Define("weight_ff_jetFR_pt25to30_Down", "variations_weight_ff_jetFR_pt25to30[1]")
        .Define("weight_ff_jetFR_pt30to35_Down", "variations_weight_ff_jetFR_pt30to35[1]")
        .Define("weight_ff_jetFR_pt35to40_Down", "variations_weight_ff_jetFR_pt35to40[1]")
        .Define("weight_ff_jetFR_pt40to50_Down", "variations_weight_ff_jetFR_pt40to50[1]")
        .Define("weight_ff_jetFR_pt50to60_Down", "variations_weight_ff_jetFR_pt50to60[1]")
        .Define("weight_ff_jetFR_pt60to80_Down", "variations_weight_ff_jetFR_pt60to80[1]")
        .Define("weight_ff_jetFR_pt80to100_Down",  "variations_weight_ff_jetFR_pt80to100[1]")
        .Define("weight_ff_jetFR_pt100to120_Down", "variations_weight_ff_jetFR_pt100to120[1]")
        .Define("weight_ff_jetFR_pt120to150_Down", "variations_weight_ff_jetFR_pt120to150[1]")
        .Define("weight_ff_jetFR_ptgt150_Down",   "variations_weight_ff_jetFR_ptgt150[1]")

        // shifts due to the pt_2 energy scale up/down variations
        .Define("weight_ff_es2Up",   getJetFakeWeight_nominal, {"channel", "pt_2_es2Up",   "byVVVLooseDeepVSjet_2", "byMediumDeepVSjet_2", "taufr_y"})
        .Define("weight_ff_es2Down", getJetFakeWeight_nominal, {"channel", "pt_2_es2Down", "byVVVLooseDeepVSjet_2", "byMediumDeepVSjet_2", "taufr_y"});

  }
  else {
    return df2;
  }
}

/*******************************************************************/

#endif
