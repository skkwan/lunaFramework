// Pile-up reweighing

#ifndef PILEUP_REWEIGHING_H
#define PILEUP_REWEIGHING_H

#include "correction.h"
#include "fileIO.h"
#include "sampleConfig_class.h"

using correction::CorrectionSet;

/*******************************************************************/

/*
 * Get the pile-up distribution re-weighting as a branch.
 */

template <typename T>
auto GetPileupWeight(T &df, LUNA::sampleConfig_t &sConfig, TString jsonDir){

  TString cset_dir;
  cset_dir.Form("%sPOG/LUM/%s_UL/puWeights.json", jsonDir.Data(), sConfig.era().c_str());
  auto cset = CorrectionSet::from_file(cset_dir.Data());

  TString cset_name;
  if (sConfig.year() == 2018) { cset_name = "Collisions18_UltraLegacy_goldenJSON"; }
  else if (sConfig.year() == 2017) { cset_name = "Collisions17_UltraLegacy_goldenJSON"; }
  else { cset_name = "Collisions16_UltraLegacy_goldenJSON"; }  // same for preVFP and postVFP
  auto cset_pileup = cset->at(cset_name.Data());

  if (sConfig.isMC()) {
    auto df2 = df.Define("weight_pu_nominal", [cset_pileup] (float nTrueInt) { return (float) cset_pileup->evaluate({nTrueInt, "nominal"}); }, {"nTrueInt"});
    if (sConfig.doShifts()) {
      return df2.Define("weight_pu_Up", [cset_pileup] (float nTrueInt) { return (float) cset_pileup->evaluate({nTrueInt, "up"}); }, {"nTrueInt"})
                .Define("weight_pu_Down", [cset_pileup] (float nTrueInt) { return (float) cset_pileup->evaluate({nTrueInt, "down"}); }, {"nTrueInt"});
    }
    else {
      return df2;
    }
  }
  else { // data or Embed
    auto df2 = df.Define("weight_pu_nominal", [] { return 1.0f; });

    if (sConfig.doShifts()) {
      return df2.Define("weight_pu_Up",   [] { return 1.0f; })
                .Define("weight_pu_Down", [] { return 1.0f; });
    }
    else {
      return df2;
    }
  }
}

/*******************************************************************/

#endif
