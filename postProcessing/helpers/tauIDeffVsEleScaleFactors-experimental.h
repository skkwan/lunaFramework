// emuFakingTauhScaleFactors-experimental.h

#ifndef TAU_ID_EFF_VS_E_SCALE_FACTORS_EXPERIMENTAL_H_INCL
#define TAU_ID_EFF_VS_E_SCALE_FACTORS_EXPERIMENTAL_H_INCL

#include "correction.h"
#include "helperFunctions.h"
#include "sampleConfig_class.h"

using correction::CorrectionSet;

/*******************************************************************/

/*
 * ID for electrons and muons faking tauh (to-do: add 2016 and 2017)
 */

template <typename T>
auto GetEFakingTauhWeightShifts_experimental(T &df, LUNA::sampleConfig_t &sConfig, TString jsonDir) {

  // MC: any year
  if (sConfig.isMC()) {

    TString cset_dir;
    TString objName;
    cset_dir.Form("%sPOG/TAU/%s_UL/tau.json", jsonDir.Data(), sConfig.era().c_str());
    objName = "DeepTau2017v2p1VSe";

    auto cset = CorrectionSet::from_file(cset_dir.Data());
    auto cset_tauID = cset->at(objName.Data());

    auto df2 = df.Define("weight_tauideff_VSe_nominal", [cset_tauID] (const int channel, const float eta, const unsigned int genmatch) {
                      if (channel == Helper::mt) { // mutau channel uses VLoose vs. e working point
                        return (float) cset_tauID->evaluate({abs(eta), (int) genmatch, "VLoose", "nom"});
                      }
                      else if (channel == Helper::et) {  // etau channel uses Tight vs. e working point
                        return (float) cset_tauID->evaluate({abs(eta), (int) genmatch, "Tight", "nom"});
                      }
                      else {
                        return 1.0f;
                      }
                    }, {"channel", "eta_2", "gen_match_2"});

    if (sConfig.doShifts()) {
      // shifts of electron faking tauh weight itself. No dependence on energy so no shifts due to lepton energy scales
      return df2.Define("weight_tauideff_VSe_Up", [cset_tauID] (const int channel, const float eta, const unsigned int genmatch) {
                        if (channel == Helper::mt) { // mutau channel uses VLoose vs. e working point
                          return (float) cset_tauID->evaluate({abs(eta), (int) genmatch, "VLoose", "up"});
                        }
                        else if (channel == Helper::et) {  // etau channel uses Tight vs. e working point
                          return (float) cset_tauID->evaluate({abs(eta), (int) genmatch, "Tight", "up"});
                        }
                        else {
                          return 1.0f;
                        }
                      }, {"channel", "eta_2", "gen_match_2"})
                .Define("weight_tauideff_VSe_Down", [cset_tauID] (const int channel, const float eta, const unsigned int genmatch) {
                        if (channel == Helper::mt) { // mutau channel uses VLoose vs. e working point
                          return (float) cset_tauID->evaluate({abs(eta), (int) genmatch, "VLoose", "down"});
                        }
                        else if (channel == Helper::et) {  // etau channel uses Tight vs. e working point
                          return (float) cset_tauID->evaluate({abs(eta), (int) genmatch, "Tight", "down"});
                        }
                        else {
                          return 1.0f;
                        }
                      }, {"channel", "eta_2", "gen_match_2"})
      // now bin the variations
                .Define("weight_tauideff_VSe_bar_Up", [&](const float eta, const float weight_nom, const float weight_shifted) {
                  return ((abs(eta) >= 0) && (abs(eta) < 1.460)) ? weight_shifted : weight_nom;
                }, {"eta_2", "weight_tauideff_VSe_nominal", "weight_tauideff_VSe_Up"})
                .Define("weight_tauideff_VSe_end_Up", [&](const float eta, const float weight_nom, const float weight_shifted) {
                  return ((abs(eta) > 1.558) && (abs(eta) < 2.30)) ? weight_shifted : weight_nom;
                }, {"eta_2", "weight_tauideff_VSe_nominal", "weight_tauideff_VSe_Up"})

                .Define("weight_tauideff_VSe_bar_Down", [&](const float eta, const float weight_nom, const float weight_shifted) {
                  return ((abs(eta) >= 0) && (abs(eta) < 1.460)) ? weight_shifted : weight_nom;
                }, {"eta_2", "weight_tauideff_VSe_nominal", "weight_tauideff_VSe_Down"})
                .Define("weight_tauideff_VSe_end_Down", [&](const float eta, const float weight_nom, const float weight_shifted) {
                  return ((abs(eta) > 1.558) && (abs(eta) < 2.30)) ? weight_shifted : weight_nom;
                }, {"eta_2", "weight_tauideff_VSe_nominal", "weight_tauideff_VSe_Down"});

    }
    else {
      return df2;
    }
  }
  else {  // embedded for instance, or data
    return df.Define("weight_tauideff_VSe_nominal",  [=]() { return 1.0f; })
             .Define("weight_tauideff_VSe_bar_Up",   [=]() { return 1.0f; })
             .Define("weight_tauideff_VSe_bar_Down", [=]() { return 1.0f; })
             .Define("weight_tauideff_VSe_end_Up",   [=]() { return 1.0f; })
             .Define("weight_tauideff_VSe_end_Down", [=]() { return 1.0f; });
  }
}

/*******************************************************************/

#endif
