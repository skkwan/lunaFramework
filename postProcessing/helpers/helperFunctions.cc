#include "helperFunctions.h"
#include <iostream>

namespace Helper {


    // Returns whether a substring is in a string.
    bool containsSubstring(std::string string, std::string substring) {
        return (string.find(substring) != std::string::npos);
    }

    // Returns whether any substrings in the provided vector, are in the string.
    bool containsAnyOfTheseSubstrings(std::string str, std::vector<std::string> vSubstring) {
        for (std::string substring : vSubstring) {
            if (str.find(substring) != std::string::npos) {
                return true;
            }
        }
        return false;
    }

  /***************************************************************************/


  // Returns the difference in the azimuth coordinates of v1 and v2, taking
  // the boundary conditions at 2 * pi into account.

    float compute_deltaPhi(float v1, float v2, const float c) {
        float r = std::fmod(v2 - v1, 2.0 * c);
        if (r < -c) {
            r += 2.0 * c;
        }
        else if (r > c) {
            r -= 2.0 * c;
        }
        return r;
    }



    float compute_deltaR(float eta_1, float eta_2, float phi_1, float phi_2){
        float deltaR = sqrt(
                pow(eta_1 - eta_2, 2) +
                pow(compute_deltaPhi(phi_1, phi_2), 2));

        return deltaR;
    };

    /***************************************************************************/

    /*
     * Return value by channel.
     */
    float getValueByChannelWithDefault(int channel, float val_default, float val_mt, float val_et, float val_em) {
        if      (channel == Helper::mt) { return val_mt; }
        else if (channel == Helper::et) { return val_et;  }
        else if (channel == Helper::em) { return val_em;  }
        else { return val_default; }
    }

    float getValueByChannel(int channel, float val_mt, float val_et, float val_em) {
        if      (channel == Helper::mt) { return val_mt; }
        else if (channel == Helper::et) { return val_et;  }
        else if (channel == Helper::em) { return val_em;  }
        else {
            std::cout << "[ERROR:] Event does not belong to one of the three channels!" << std::endl;
            return 1.0;
        }
    }
}

/***************************************************************************/
