
// List of final variables in the postprocess n-tuple

#ifndef FINALVARIABLES_H
#define FINALVARIABLES_H

#include "fileIO.h"


/*
 * Declare all variables which to appear in the final n-tuple. TO DO: make year-specific
 * designations.
 */

std::vector<std::string> allCommonVariables = {

  // Year and sample info
  "year",
  "run", "lumi", "evt",

  "channel",

  // Whether the offline objects were matched to trigger objects
  "match1", "match2",

  // Filter bits
  "filterBits1", "filterBits2",

  // Trigger decisions
  "passSingleTrigger_mt_nominal", "passCrossTrigger_mt_nominal",
  "passSingleTrigger_et_nominal", "passCrossTrigger_et_nominal",
  "passCross_leadingMuon_em_nominal", "passCross_leadingEle_em_nominal", "passCrossTrigger_em_nominal",


  // Leading tau candidates
  "pt_1_nominal", "eta_1", "phi_1", "m_1_nominal", "q_1", "iso_1",
  "pt_2_nominal", "eta_2", "phi_2", "m_2_nominal", "q_2",
  "m_tt_nominal", "pt_tt_nominal", // defined to be the SVFit di-tau mass if available, otherwise same as m_vis
  "m_vis_nominal", "pt_vis_nominal",
  "gen_match_1", "gen_match_2", "decayMode_2",

  // Discriminants for tauh
  "byVVVLooseDeepVSjet_2", "byVVLooseDeepVSjet_2", "byVLooseDeepVSjet_2", "byLooseDeepVSjet_2", "byMediumDeepVSjet_2", "byTightDeepVSjet_2", "byVTightDeepVSjet_2", "byVVTightDeepVSjet_2",
  "byVVVLooseDeepVSe_2", "byVVLooseDeepVSe_2", "byVLooseDeepVSe_2", "byLooseDeepVSe_2", "byMediumDeepVSe_2", "byTightDeepVSe_2", "byVTightDeepVSe_2", "byVVTightDeepVSe_2",
  "byVLooseDeepVSmu_2", "byLooseDeepVSmu_2", "byMediumDeepVSmu_2", "byTightDeepVSmu_2",

  //  deepFlavour leading and sub-leading b-tagged jets
  "bpt_deepflavour_1", "beta_deepflavour_1", "bphi_deepflavour_1", "bm_deepflavour_1", "bscore_deepflavour_1", "bflavour_deepflavour_1",
  "bpt_deepflavour_2", "beta_deepflavour_2", "bphi_deepflavour_2", "bm_deepflavour_2", "bscore_deepflavour_2", "bflavour_deepflavour_2",
  /* // Experimental: b-jet regression information */
  /* "bbRegCorr_deepflavour_1", "bbRegRes_deepflavour_1", "bnMuons_deepflavour_1", */
  /* "bbRegCorr_deepflavour_2", "bbRegRes_deepflavour_2", "bnMuons_deepflavour_2", */
  /* // Di-b-tag-jet mass, without and with b-jet regression correction, and the gen-jets di-b-tag-jet mass with neutrinos */
  /* "mbb", "mbbCorr", "mbbGenWN", */
  /* // Experimental: gen-jet information */
  /* "bGenIdx_1", "bGenIdx_2", */
  /* "bGenPt_1", "bGenPt_2", */
  /* "bGenPtWN_1", "bGenPtWN_2", */


  // All jets
  "njets",
  "jpt_1", "jeta_1", "jphi_1",
  "jpt_2", "jeta_2", "jphi_2",
  "mjj",

  // B-tag jets
  "nbtag20_nominal",

  // recoil jets
  "nRecoilJets",

  // PF MET
  "met_nominal", "metphi_nominal", "metcov00", "metcov01", "metcov10", "metcov11",

  // Discriminating variables
  "D_zeta_nominal", "m_btautau_vis_nominal", "pt_btautau_vis_nominal", "mtMET_1_nominal", "mtMET_2_nominal",

  // Weights: nominal
  "weight_initial_nominal",
  "weight_tauID_nominal",
  "weight_tauidWP_nominal",
  "weight_tauideff_VSe_nominal", "weight_tauideff_VSmu_nominal",
  "weight_muonIdIso_nominal",
  "weight_eleIdIso_nominal",
  "weight_btagEff_nominal",
  "weight_zPt_nominal", "weight_topPt_nominal",
  "weight_DYJets_nominal", "weight_WJets_nominal",
  "weight_emb_tautracking_nominal",
  "weight_ff_nominal",
  "weight_crosstrg_fakefactor_nominal",
  "weight_mt_trgsf_nominal", "weight_et_trgsf_nominal", "weight_em_trgsf_nominal",
  "weight_osss_nominal", "weight_correction_nominal", "weight_closure_nominal",
  "weight_l1prefiring_nominal", "weight_l1prefiring_up", "weight_l1prefiring_down",
  "weight_pu_nominal",
  "weight_m_sel_id_ratio_1", "weight_m_sel_id_ratio_2", "weight_m_sel_trg_ratio"
};


/*
 * Optional variables which may be in the final n-tuple
 */
std::vector<std::string> optionalVariables = {
  // not defined for data and Embedded
   "npu",

  // Jets
  "bpt_deepflavour_JERUp_1",               "bpt_deepflavour_JERDown_1",
  "bpt_deepflavour_JetAbsoluteUp_1",       "bpt_deepflavour_JetAbsoluteDown_1",
  "bpt_deepflavour_JetAbsoluteyearUp_1",   "bpt_deepflavour_JetAbsoluteyearDown_1",
  "bpt_deepflavour_JetBBEC1Up_1",          "bpt_deepflavour_JetBBEC1Down_1",
  "bpt_deepflavour_JetBBEC1yearUp_1",      "bpt_deepflavour_JetBBEC1yearDown_1",
  "bpt_deepflavour_JetEC2Up_1",            "bpt_deepflavour_JetEC2Down_1",
  "bpt_deepflavour_JetEC2yearUp_1",        "bpt_deepflavour_JetEC2yearDown_1",
  "bpt_deepflavour_JetFlavorQCDUp_1",      "bpt_deepflavour_JetFlavorQCDDown_1",
  "bpt_deepflavour_JetHFUp_1",             "bpt_deepflavour_JetHFDown_1",
  "bpt_deepflavour_JetHFyearUp_1",         "bpt_deepflavour_JetHFyearDown_1",
  "bpt_deepflavour_JetRelativeBalUp_1",    "bpt_deepflavour_JetRelativeBalDown_1",
  "bpt_deepflavour_JetRelativeSampleUp_1", "bpt_deepflavour_JetRelativeSampleDown_1",

  "bpt_deepflavour_JERUp_2",               "bpt_deepflavour_JERDown_2",
  "bpt_deepflavour_JetAbsoluteUp_2",       "bpt_deepflavour_JetAbsoluteDown_2",
  "bpt_deepflavour_JetAbsoluteyearUp_2",   "bpt_deepflavour_JetAbsoluteyearDown_2",
  "bpt_deepflavour_JetBBEC1Up_2",          "bpt_deepflavour_JetBBEC1Down_2",
  "bpt_deepflavour_JetBBEC1yearUp_2",      "bpt_deepflavour_JetBBEC1yearDown_2",
  "bpt_deepflavour_JetEC2Up_2",            "bpt_deepflavour_JetEC2Down_2",
  "bpt_deepflavour_JetEC2yearUp_2",        "bpt_deepflavour_JetEC2yearDown_2",
  "bpt_deepflavour_JetFlavorQCDUp_2",      "bpt_deepflavour_JetFlavorQCDDown_2",
  "bpt_deepflavour_JetHFUp_2",             "bpt_deepflavour_JetHFDown_2",
  "bpt_deepflavour_JetHFyearUp_2",         "bpt_deepflavour_JetHFyearDown_2",
  "bpt_deepflavour_JetRelativeBalUp_2",    "bpt_deepflavour_JetRelativeBalDown_2",
  "bpt_deepflavour_JetRelativeSampleUp_2", "bpt_deepflavour_JetRelativeSampleDown_2",

  // JER systematic shifts of the mass
  "bm_deepflavour_JERUp_1",               "bm_deepflavour_JERDown_1",
  "bm_deepflavour_JetAbsoluteUp_1",       "bm_deepflavour_JetAbsoluteDown_1",
  "bm_deepflavour_JetAbsoluteyearUp_1",   "bm_deepflavour_JetAbsoluteyearDown_1",
  "bm_deepflavour_JetBBEC1Up_1",          "bm_deepflavour_JetBBEC1Down_1",
  "bm_deepflavour_JetBBEC1yearUp_1",      "bm_deepflavour_JetBBEC1yearDown_1",
  "bm_deepflavour_JetEC2Up_1",            "bm_deepflavour_JetEC2Down_1",
  "bm_deepflavour_JetEC2yearUp_1",        "bm_deepflavour_JetEC2yearDown_1",
  "bm_deepflavour_JetFlavorQCDUp_1",      "bm_deepflavour_JetFlavorQCDDown_1",
  "bm_deepflavour_JetHFUp_1",             "bm_deepflavour_JetHFDown_1",
  "bm_deepflavour_JetHFyearUp_1",         "bm_deepflavour_JetHFyearDown_1",
  "bm_deepflavour_JetRelativeBalUp_1",    "bm_deepflavour_JetRelativeBalDown_1",
  "bm_deepflavour_JetRelativeSampleUp_1", "bm_deepflavour_JetRelativeSampleDown_1",

  "bm_deepflavour_JERUp_2",               "bm_deepflavour_JERDown_2",
  "bm_deepflavour_JetAbsoluteUp_2",       "bm_deepflavour_JetAbsoluteDown_2",
  "bm_deepflavour_JetAbsoluteyearUp_2",   "bm_deepflavour_JetAbsoluteyearDown_2",
  "bm_deepflavour_JetBBEC1Up_2",          "bm_deepflavour_JetBBEC1Down_2",
  "bm_deepflavour_JetBBEC1yearUp_2",      "bm_deepflavour_JetBBEC1yearDown_2",
  "bm_deepflavour_JetEC2Up_2",            "bm_deepflavour_JetEC2Down_2",
  "bm_deepflavour_JetEC2yearUp_2",        "bm_deepflavour_JetEC2yearDown_2",
  "bm_deepflavour_JetFlavorQCDUp_2",      "bm_deepflavour_JetFlavorQCDDown_2",
  "bm_deepflavour_JetHFUp_2",             "bm_deepflavour_JetHFDown_2",
  "bm_deepflavour_JetHFyearUp_2",         "bm_deepflavour_JetHFyearDown_2",
  "bm_deepflavour_JetRelativeBalUp_2",    "bm_deepflavour_JetRelativeBalDown_2",
  "bm_deepflavour_JetRelativeSampleUp_2", "bm_deepflavour_JetRelativeSampleDown_2",

  // PF MET JEC/JER systematic shifts
  "met_JERUp",               "met_JERDown",
  "met_JetAbsoluteUp",       "met_JetAbsoluteDown",
  "met_JetAbsoluteyearUp",   "met_JetAbsoluteyearDown",
  "met_JetBBEC1Up",          "met_JetBBEC1Down",
  "met_JetBBEC1yearUp",      "met_JetBBEC1yearDown",
  "met_JetEC2Up",            "met_JetEC2Down",
  "met_JetEC2yearUp",        "met_JetEC2yearDown",
  "met_JetFlavorQCDUp",      "met_JetFlavorQCDDown",
  "met_JetHFUp",             "met_JetHFDown",
  "met_JetHFyearUp",         "met_JetHFyearDown",
  "met_JetRelativeBalUp",    "met_JetRelativeBalDown",
  "met_JetRelativeSampleUp", "met_JetRelativeSampleDown",

  "metphi_JERUp",               "metphi_JERDown",
  "metphi_JetAbsoluteUp",       "metphi_JetAbsoluteDown",
  "metphi_JetAbsoluteyearUp",   "metphi_JetAbsoluteyearDown",
  "metphi_JetBBEC1Up",          "metphi_JetBBEC1Down",
  "metphi_JetBBEC1yearUp",      "metphi_JetBBEC1yearDown",
  "metphi_JetEC2Up",            "metphi_JetEC2Down",
  "metphi_JetEC2yearUp",        "metphi_JetEC2yearDown",
  "metphi_JetFlavorQCDUp",      "metphi_JetFlavorQCDDown",
  "metphi_JetHFUp",             "metphi_JetHFDown",
  "metphi_JetHFyearUp",         "metphi_JetHFyearDown",
  "metphi_JetRelativeBalUp",    "metphi_JetRelativeBalDown",
  "metphi_JetRelativeSampleUp", "metphi_JetRelativeSampleDown",

  // PF MET systematic shifts: UES
  "met_UESUp",    "met_UESDown",
  "metphi_UESUp", "metphi_UESDown",

  // PF MET systematic shifts: response/resolution. For other datasets, these will just be equal to nominal met/metphi
  "met_responseUp", "met_responseDown",
  "met_resolutionUp", "met_resolutionDown",
  "metphi_responseUp", "metphi_responseDown",
  "metphi_resolutionUp", "metphi_resolutionDown",

  // Whether the HLT path was passed or not
  "pass_mt_HLT_IsoMu24", "pass_mt_HLT_IsoMu27", "pass_mt_HLT_Mu20Tau27", "pass_mt_HLT_Mu20HPSTau27",
  "pass_et_HLT_Ele32", "pass_et_HLT_Ele35", "pass_et_HLT_Ele24Tau30", "pass_et_HLT_Ele24HPSTau30",
  "pass_em_HLT_Mu23Ele12", "pass_em_HLT_Mu8Ele23",

  // Whether the trigger object passed filter bits (behaviour is buggy for NanoAOD v9)
  "filter_mt_Mu24", "filter_mt_Mu27", "filter_mt_Mu20Tau27", "filter_mt_Mu20HPSTau27",
  "filter_et_Ele32", "filter_et_Ele35", "filter_et_Ele24Tau30", "filter_et_Ele24HPSTau30",
  "filter_em_Mu23Ele12", "filter_em_Mu8Ele23",

  "passSingle_1_mt_nominal", "passSingle_2_mt_nominal",
  "passSingle_1_et_nominal", "passSingle_2_et_nominal",

  "passCrossTrigger_nonHPS_mt_nominal", "passCrossTrigger_HPS_mt_nominal",
  "passCrossTrigger_nonHPS_et_nominal", "passCrossTrigger_HPS_et_nominal",

 // Trigger decisions: shifted due to first lepton energy scale up/down
  "passSingle_1_mt_es1Up", "passSingle_2_mt_es1Up", "passSingleTrigger_mt_es1Up", "passCrossTrigger_nonHPS_mt_es1Up", "passCrossTrigger_HPS_mt_es1Up", "passCrossTrigger_mt_es1Up",
  "passSingle_1_et_es1Up", "passSingle_2_et_es1Up", "passSingleTrigger_et_es1Up", "passCrossTrigger_nonHPS_et_es1Up", "passCrossTrigger_HPS_et_es1Up", "passCrossTrigger_et_es1Up",
  "passCross_leadingMuon_em_es1Up", "passCross_leadingEle_em_es1Up", "passCrossTrigger_em_es1Up",

  "passSingle_1_mt_es1Down", "passSingle_2_mt_es1Down", "passSingleTrigger_mt_es1Down", "passCrossTrigger_nonHPS_mt_es1Down", "passCrossTrigger_HPS_mt_es1Down", "passCrossTrigger_mt_es1Down",
  "passSingle_1_et_es1Down", "passSingle_2_et_es1Down", "passSingleTrigger_et_es1Down", "passCrossTrigger_nonHPS_et_es1Down", "passCrossTrigger_HPS_et_es1Down", "passCrossTrigger_et_es1Down",
  "passCross_leadingMuon_em_es1Down", "passCross_leadingEle_em_es1Down", "passCrossTrigger_em_es1Down",

  // Trigger decisions: shifted due to second lepton energy scale up/down
  "passSingle_1_mt_es2Up", "passSingle_2_mt_es2Up", "passSingleTrigger_mt_es2Up", "passCrossTrigger_nonHPS_mt_es2Up", "passCrossTrigger_HPS_mt_es2Up", "passCrossTrigger_mt_es2Up",
  "passSingle_1_et_es2Up", "passSingle_2_et_es2Up", "passSingleTrigger_et_es2Up", "passCrossTrigger_nonHPS_et_es2Up", "passCrossTrigger_HPS_et_es2Up", "passCrossTrigger_et_es2Up",
  "passCross_leadingMuon_em_es2Up", "passCross_leadingEle_em_es2Up", "passCrossTrigger_em_es2Up",

  "passSingle_1_mt_es2Down", "passSingle_2_mt_es2Down", "passSingleTrigger_mt_es2Down", "passCrossTrigger_nonHPS_mt_es2Down", "passCrossTrigger_HPS_mt_es2Down",  "passCrossTrigger_mt_es2Down",
  "passSingle_1_et_es2Down", "passSingle_2_et_es2Down", "passSingleTrigger_et_es2Down", "passCrossTrigger_nonHPS_et_es2Down", "passCrossTrigger_HPS_et_es2Down",  "passCrossTrigger_et_es2Down",
  "passCross_leadingMuon_em_es2Down", "passCross_leadingEle_em_es2Down", "passCrossTrigger_em_es2Down",


  // TauTau legs Kinematics
  "pt_1_es1Up", "pt_1_es1Down",
  "m_1_es1Up", "m_1_es1Down",

  "pt_2_es2Up", "pt_2_es2Down",
  "m_2_es2Up", "m_2_es2Down",

  "met_es1Up", "met_es1Down",
  "met_es2Up", "met_es2Down",

  "metphi_es1Up", "metphi_es1Down",
  "metphi_es2Up", "metphi_es2Down",

  "m_vis_es1Up",                 "m_vis_es1Down",
  "m_vis_es2Up",                 "m_vis_es2Down",

  "pt_vis_es1Up",                 "pt_vis_es1Down",
  "pt_vis_es2Up",                 "pt_vis_es2Down",

  "m_tt_es1Up",                "m_tt_es1Down",
  "m_tt_es2Up",                "m_tt_es2Down",
  "m_tt_UESUp",                 "m_tt_UESDown",
  "m_tt_ResponseUp",            "m_tt_ResponseDown",
  "m_tt_ResolutionUp",          "m_tt_ResolutionDown",
  "m_tt_JetAbsoluteUp",         "m_tt_JetAbsoluteDown",
  "m_tt_JetAbsoluteyearUp",     "m_tt_JetAbsoluteyearDown",
  "m_tt_JetBBEC1Up",            "m_tt_JetBBEC1Down",
  "m_tt_JetBBEC1yearUp",        "m_tt_JetBBEC1yearDown",
  "m_tt_JetEC2Up",              "m_tt_JetEC2Down",
  "m_tt_JetEC2yearUp",          "m_tt_JetEC2yearDown",
  "m_tt_JetFlavorQCDUp",        "m_tt_JetFlavorQCDDown",
  "m_tt_JetHFUp",               "m_tt_JetHFDown",
  "m_tt_JetHFyearUp",           "m_tt_JetHFyearDown",
  "m_tt_JetRelativeBalUp",      "m_tt_JetRelativeBalDown",
  "m_tt_JetRelativeSampleUp",   "m_tt_JetRelativeSampleDown",
  "m_tt_JERUp",                 "m_tt_JERDown",

  "pt_tt_es1Up",                "pt_tt_es1Down",
  "pt_tt_es2Up",                "pt_tt_es2Down",
  "pt_tt_UESUp",                 "pt_tt_UESDown",
  "pt_tt_ResponseUp",            "pt_tt_ResponseDown",
  "pt_tt_ResolutionUp",          "pt_tt_ResolutionDown",
  "pt_tt_JetAbsoluteUp",         "pt_tt_JetAbsoluteDown",
  "pt_tt_JetAbsoluteyearUp",     "pt_tt_JetAbsoluteyearDown",
  "pt_tt_JetBBEC1Up",            "pt_tt_JetBBEC1Down",
  "pt_tt_JetBBEC1yearUp",        "pt_tt_JetBBEC1yearDown",
  "pt_tt_JetEC2Up",              "pt_tt_JetEC2Down",
  "pt_tt_JetEC2yearUp",          "pt_tt_JetEC2yearDown",
  "pt_tt_JetFlavorQCDUp",        "pt_tt_JetFlavorQCDDown",
  "pt_tt_JetHFUp",               "pt_tt_JetHFDown",
  "pt_tt_JetHFyearUp",           "pt_tt_JetHFyearDown",
  "pt_tt_JetRelativeBalUp",      "pt_tt_JetRelativeBalDown",
  "pt_tt_JetRelativeSampleUp",   "pt_tt_JetRelativeSampleDown",
  "pt_tt_JERUp",                 "pt_tt_JERDown",

  "eta_tt_nominal",
  "eta_tt_es1Up",                "eta_tt_es1Down",
  "eta_tt_es2Up",                "eta_tt_es2Down",
  "eta_tt_UESUp",                 "eta_tt_UESDown",
  "eta_tt_ResponseUp",            "eta_tt_ResponseDown",
  "eta_tt_ResolutionUp",          "eta_tt_ResolutionDown",
  "eta_tt_JetAbsoluteUp",         "eta_tt_JetAbsoluteDown",
  "eta_tt_JetAbsoluteyearUp",     "eta_tt_JetAbsoluteyearDown",
  "eta_tt_JetBBEC1Up",            "eta_tt_JetBBEC1Down",
  "eta_tt_JetBBEC1yearUp",        "eta_tt_JetBBEC1yearDown",
  "eta_tt_JetEC2Up",              "eta_tt_JetEC2Down",
  "eta_tt_JetEC2yearUp",          "eta_tt_JetEC2yearDown",
  "eta_tt_JetFlavorQCDUp",        "eta_tt_JetFlavorQCDDown",
  "eta_tt_JetHFUp",               "eta_tt_JetHFDown",
  "eta_tt_JetHFyearUp",           "eta_tt_JetHFyearDown",
  "eta_tt_JetRelativeBalUp",      "eta_tt_JetRelativeBalDown",
  "eta_tt_JetRelativeSampleUp",   "eta_tt_JetRelativeSampleDown",
  "eta_tt_JERUp",                 "eta_tt_JERDown",

  "phi_tt_nominal",
  "phi_tt_es1Up",                "phi_tt_es1Down",
  "phi_tt_es2Up",                "phi_tt_es2Down",
  "phi_tt_UESUp",                 "phi_tt_UESDown",
  "phi_tt_ResponseUp",            "phi_tt_ResponseDown",
  "phi_tt_ResolutionUp",          "phi_tt_ResolutionDown",
  "phi_tt_JetAbsoluteUp",         "phi_tt_JetAbsoluteDown",
  "phi_tt_JetAbsoluteyearUp",     "phi_tt_JetAbsoluteyearDown",
  "phi_tt_JetBBEC1Up",            "phi_tt_JetBBEC1Down",
  "phi_tt_JetBBEC1yearUp",        "phi_tt_JetBBEC1yearDown",
  "phi_tt_JetEC2Up",              "phi_tt_JetEC2Down",
  "phi_tt_JetEC2yearUp",          "phi_tt_JetEC2yearDown",
  "phi_tt_JetFlavorQCDUp",        "phi_tt_JetFlavorQCDDown",
  "phi_tt_JetHFUp",               "phi_tt_JetHFDown",
  "phi_tt_JetHFyearUp",           "phi_tt_JetHFyearDown",
  "phi_tt_JetRelativeBalUp",      "phi_tt_JetRelativeBalDown",
  "phi_tt_JetRelativeSampleUp",   "phi_tt_JetRelativeSampleDown",
  "phi_tt_JERUp",                 "phi_tt_JERDown",

  // recoil jets
  "nRecoilJets_jerUp", "nRecoilJets_jerDown",
  "nRecoilJets_jesAbsoluteUp", "nRecoilJets_jesAbsoluteDown",
  "nRecoilJets_jesAbsoluteyearUp", "nRecoilJets_jesAbsoluteyearDown",
  "nRecoilJets_jesBBEC1Up", "nRecoilJets_jesBBEC1Down",
  "nRecoilJets_jesBBEC1yearUp", "nRecoilJets_jesBBEC1yearDown",
  "nRecoilJets_jesEC2Up", "nRecoilJets_jesEC2Down",
  "nRecoilJets_jesEC2yearUp", "nRecoilJets_jesEC2yearDown",
  "nRecoilJets_jesFlavorQCDUp", "nRecoilJets_jesFlavorQCDDown",
  "nRecoilJets_jesHFUp", "nRecoilJets_jesHFDown",
  "nRecoilJets_jesHFyearUp", "nRecoilJets_jesHFyearDown",
  "nRecoilJets_jesRelativeBalUp", "nRecoilJets_jesRelativeBalDown",
  "nRecoilJets_jesRelativeSampleUp", "nRecoilJets_jesRelativeSampleDown",

  // cut-based variable
  "mtMET_1_es1Up", "mtMET_1_es1Down",
  "mtMET_2_es2Up", "mtMET_2_es2Down",

  "mtMET_1_responseUp",   "mtMET_1_responseDown",
  "mtMET_1_resolutionUp",   "mtMET_1_resolutionDown",
  "mtMET_1_UESUp",   "mtMET_1_UESDown",
  "mtMET_1_JERUp",                 "mtMET_1_JERDown",
  "mtMET_1_JetAbsoluteUp",         "mtMET_1_JetAbsoluteDown",
  "mtMET_1_JetAbsoluteyearUp",     "mtMET_1_JetAbsoluteyearDown",
  "mtMET_1_JetBBEC1Up",            "mtMET_1_JetBBEC1Down",
  "mtMET_1_JetBBEC1yearUp",        "mtMET_1_JetBBEC1yearDown",
  "mtMET_1_JetEC2Up",              "mtMET_1_JetEC2Down",
  "mtMET_1_JetEC2yearUp",          "mtMET_1_JetEC2yearDown",
  "mtMET_1_JetFlavorQCDUp",        "mtMET_1_JetFlavorQCDDown",
  "mtMET_1_JetHFUp",               "mtMET_1_JetHFDown",
  "mtMET_1_JetHFyearUp",           "mtMET_1_JetHFyearDown",
  "mtMET_1_JetRelativeBalUp",      "mtMET_1_JetRelativeBalDown",
  "mtMET_1_JetRelativeSampleUp",   "mtMET_1_JetRelativeSampleDown",

  // cut-based variable
  "mtMET_2_responseUp", "mtMET_2_responseDown",
  "mtMET_2_resolutionUp",   "mtMET_2_resolutionDown",
  "mtMET_2_UESUp",   "mtMET_2_UESDown",
  "mtMET_2_JERUp",                 "mtMET_2_JERDown",
  "mtMET_2_JetAbsoluteUp",         "mtMET_2_JetAbsoluteDown",
  "mtMET_2_JetAbsoluteyearUp",     "mtMET_2_JetAbsoluteyearDown",
  "mtMET_2_JetBBEC1Up",            "mtMET_2_JetBBEC1Down",
  "mtMET_2_JetBBEC1yearUp",        "mtMET_2_JetBBEC1yearDown",
  "mtMET_2_JetEC2Up",              "mtMET_2_JetEC2Down",
  "mtMET_2_JetEC2yearUp",          "mtMET_2_JetEC2yearDown",
  "mtMET_2_JetFlavorQCDUp",        "mtMET_2_JetFlavorQCDDown",
  "mtMET_2_JetHFUp",               "mtMET_2_JetHFDown",
  "mtMET_2_JetHFyearUp",           "mtMET_2_JetHFyearDown",
  "mtMET_2_JetRelativeBalUp",      "mtMET_2_JetRelativeBalDown",
  "mtMET_2_JetRelativeSampleUp",   "mtMET_2_JetRelativeSampleDown",

  // cut-based variable
  "D_zeta_es1Up",  "D_zeta_es1Down",
  "D_zeta_es2Up",  "D_zeta_es2Down",

  // Shifts of D_zeta due to JER ystematics
  "D_zeta_responseUp",          "D_zeta_responseDown",
  "D_zeta_resolutionUp",        "D_zeta_resolutionDown",
  "D_zeta_UESUp",               "D_zeta_UESDown",
  "D_zeta_JERUp",               "D_zeta_JERDown",
  "D_zeta_JetAbsoluteUp",       "D_zeta_JetAbsoluteDown",
  "D_zeta_JetAbsoluteyearUp",   "D_zeta_JetAbsoluteyearDown",
  "D_zeta_JetBBEC1Up",          "D_zeta_JetBBEC1Down",
  "D_zeta_JetBBEC1yearUp",      "D_zeta_JetBBEC1yearDown",
  "D_zeta_JetEC2Up",            "D_zeta_JetEC2Down",
  "D_zeta_JetEC2yearUp",        "D_zeta_JetEC2yearDown",
  "D_zeta_JetFlavorQCDUp",      "D_zeta_JetFlavorQCDDown",
  "D_zeta_JetHFUp",             "D_zeta_JetHFDown",
  "D_zeta_JetHFyearUp",         "D_zeta_JetHFyearDown",
  "D_zeta_JetRelativeBalUp",    "D_zeta_JetRelativeBalDown",
  "D_zeta_JetRelativeSampleUp", "D_zeta_JetRelativeSampleDown",

  // another cut-based variable
  "m_btautau_vis_es1Up",     "pt_btautau_vis_es1Up",
  "m_btautau_vis_es1Down",   "pt_btautau_vis_es1Down",
  "m_btautau_vis_es2Up",     "pt_btautau_vis_es2Up",
  "m_btautau_vis_es2Down",   "pt_btautau_vis_es2Down",

  // Shifts of m_btautau_vis and pt_btautau_vis due to JER systematics of the leading b-tag jet
  "m_btautau_vis_JERUp_1",               "m_btautau_vis_JERDown_1",
  "m_btautau_vis_JetAbsoluteUp_1",       "m_btautau_vis_JetAbsoluteDown_1",
  "m_btautau_vis_JetAbsoluteyearUp_1",   "m_btautau_vis_JetAbsoluteyearDown_1",
  "m_btautau_vis_JetBBEC1Up_1",          "m_btautau_vis_JetBBEC1Down_1",
  "m_btautau_vis_JetBBEC1yearUp_1",      "m_btautau_vis_JetBBEC1yearDown_1",
  "m_btautau_vis_JetEC2Up_1",            "m_btautau_vis_JetEC2Down_1",
  "m_btautau_vis_JetEC2yearUp_1",        "m_btautau_vis_JetEC2yearDown_1",
  "m_btautau_vis_JetFlavorQCDUp_1",      "m_btautau_vis_JetFlavorQCDDown_1",
  "m_btautau_vis_JetHFUp_1",             "m_btautau_vis_JetHFDown_1",
  "m_btautau_vis_JetHFyearUp_1",         "m_btautau_vis_JetHFyearDown_1",
  "m_btautau_vis_JetRelativeBalUp_1",    "m_btautau_vis_JetRelativeBalDown_1",
  "m_btautau_vis_JetRelativeSampleUp_1", "m_btautau_vis_JetRelativeSampleDown_1",

  // not a cut-based variable, but saving anyway
  "pt_btautau_vis_JERUp_1",               "pt_btautau_vis_JERDown_1",
  "pt_btautau_vis_JetAbsoluteUp_1",       "pt_btautau_vis_JetAbsoluteDown_1",
  "pt_btautau_vis_JetAbsoluteyearUp_1",   "pt_btautau_vis_JetAbsoluteyearDown_1",
  "pt_btautau_vis_JetBBEC1Up_1",          "pt_btautau_vis_JetBBEC1Down_1",
  "pt_btautau_vis_JetBBEC1yearUp_1",      "pt_btautau_vis_JetBBEC1yearDown_1",
  "pt_btautau_vis_JetEC2Up_1",            "pt_btautau_vis_JetEC2Down_1",
  "pt_btautau_vis_JetEC2yearUp_1",        "pt_btautau_vis_JetEC2yearDown_1",
  "pt_btautau_vis_JetFlavorQCDUp_1",      "pt_btautau_vis_JetFlavorQCDDown_1",
  "pt_btautau_vis_JetHFUp_1",             "pt_btautau_vis_JetHFDown_1",
  "pt_btautau_vis_JetHFyearUp_1",         "pt_btautau_vis_JetHFyearDown_1",
  "pt_btautau_vis_JetRelativeBalUp_1",    "pt_btautau_vis_JetRelativeBalDown_1",
  "pt_btautau_vis_JetRelativeSampleUp_1", "pt_btautau_vis_JetRelativeSampleDown_1",

  // weights:

  // tau ID efficiency: shifts due to the lepton energy scales
  "weight_tauID_es2Up", "weight_tauID_es2Down",

  // tau ID efficiency: shifts of the weight itself, binned by pT
  "weight_tauID_tauideff_pt20to25_Up",
  "weight_tauID_tauideff_pt25to30_Up",
  "weight_tauID_tauideff_pt30to35_Up",
  "weight_tauID_tauideff_pt35to40_Up",
  "weight_tauID_tauideff_pt40to500_Up",
  "weight_tauID_tauideff_pt500to1000_Up",
  "weight_tauID_tauideff_ptgt1000_Up",
  "weight_tauID_tauideff_pt20to25_Down",
  "weight_tauID_tauideff_pt25to30_Down",
  "weight_tauID_tauideff_pt30to35_Down",
  "weight_tauID_tauideff_pt35to40_Down",
  "weight_tauID_tauideff_pt40to500_Down",
  "weight_tauID_tauideff_pt500to1000_Down",
  "weight_tauID_tauideff_ptgt1000_Down",

  // tau ID efficiency vs electrons
  "weight_tauideff_VSe_bar_Up", "weight_tauideff_VSe_bar_Down",
  "weight_tauideff_VSe_end_Up", "weight_tauideff_VSe_end_Down",
  // tau ID efficiency vs muons
  "weight_tauideff_VSmu_eta0to0p4_Up",
  "weight_tauideff_VSmu_eta0p4to0p8_Up",
  "weight_tauideff_VSmu_eta0p8to1p2_Up",
  "weight_tauideff_VSmu_eta1p2to1p7_Up",
  "weight_tauideff_VSmu_eta1p7to2p3_Up",
  "weight_tauideff_VSmu_eta0to0p4_Down",
  "weight_tauideff_VSmu_eta0p4to0p8_Down",
  "weight_tauideff_VSmu_eta0p8to1p2_Down",
  "weight_tauideff_VSmu_eta1p2to1p7_Down",
  "weight_tauideff_VSmu_eta1p7to2p3_Down",

  // JER shifts affecting number of b-tag jets
  "nbtag20_JER_Up",
  "nbtag20_JetAbsolute_Up",
  "nbtag20_JetAbsoluteyear_Up",
  "nbtag20_JetBBEC1_Up",
  "nbtag20_JetBBEC1year_Up",
  "nbtag20_JetEC2_Up",
  "nbtag20_JetEC2year_Up",
  "nbtag20_JetFlavorQCD_Up",
  "nbtag20_JetHF_Up",
  "nbtag20_JetHFyear_Up",
  "nbtag20_JetRelativeBal_Up",
  "nbtag20_JetRelativeSample_Up",
  "nbtag20_JER_Down",
  "nbtag20_JetAbsolute_Down",
  "nbtag20_JetAbsoluteyear_Down",
  "nbtag20_JetBBEC1_Down",
  "nbtag20_JetBBEC1year_Down",
  "nbtag20_JetEC2_Down",
  "nbtag20_JetEC2year_Down",
  "nbtag20_JetFlavorQCD_Down",
  "nbtag20_JetHF_Down",
  "nbtag20_JetHFyear_Down",
  "nbtag20_JetRelativeBal_Down",
  "nbtag20_JetRelativeSample_Down",

  // Shifts of the b-tagging efficiency weight
  "weight_btagEff_btagsf_hf_Up",
  "weight_btagEff_btagsf_lf_Up",
  "weight_btagEff_btagsf_hfstats1_Up",
  "weight_btagEff_btagsf_hfstats2_Up",
  "weight_btagEff_btagsf_lfstats1_Up",
  "weight_btagEff_btagsf_lfstats2_Up",
  "weight_btagEff_btagsf_cferr1_Up",
  "weight_btagEff_btagsf_cferr2_Up",
  "weight_btagEff_btagsf_hf_Down",
  "weight_btagEff_btagsf_lf_Down",
  "weight_btagEff_btagsf_hfstats1_Down",
  "weight_btagEff_btagsf_hfstats2_Down",
  "weight_btagEff_btagsf_lfstats1_Down",
  "weight_btagEff_btagsf_lfstats2_Down",
  "weight_btagEff_btagsf_cferr1_Down",
  "weight_btagEff_btagsf_cferr2_Down",

  // Shifts of the b-tagging efficiency weight due to scale factors
   "weight_btagEff_JetAbsolute_Up",
   "weight_btagEff_JetAbsoluteyear_Up",
   "weight_btagEff_JetBBEC1_Up",
   "weight_btagEff_JetBBEC1year_Up",
   "weight_btagEff_JetEC2_Up",
   "weight_btagEff_JetEC2year_Up",
   "weight_btagEff_JetFlavorQCD_Up",
   "weight_btagEff_JetHF_Up",
   "weight_btagEff_JetHFyear_Up",
   "weight_btagEff_JetRelativeBal_Up",
   "weight_btagEff_JetRelativeSample_Up",
   "weight_btagEff_JER_Up",
   "weight_btagEff_JetAbsolute_Down",
   "weight_btagEff_JetAbsoluteyear_Down",
   "weight_btagEff_JetBBEC1_Down",
   "weight_btagEff_JetBBEC1year_Down",
   "weight_btagEff_JetEC2_Down",
   "weight_btagEff_JetEC2year_Down",
   "weight_btagEff_JetFlavorQCD_Down",
   "weight_btagEff_JetHF_Down",
   "weight_btagEff_JetHFyear_Down",
   "weight_btagEff_JetRelativeBal_Down",
   "weight_btagEff_JetRelativeSample_Down",
   "weight_btagEff_JER_Down",

   // Shifts of the Z pT reweighing
   "weight_zPt_Zpt_Up", "weight_zPt_Zpt_Down",

  // Shifts of the top pT
  "weight_topPt_toppt_Up", "weight_topPt_toppt_Down",

  // Embedded tau tracking efficiency
  "weight_emb_tautracking_tautrack_dm0dm10_Up",
  "weight_emb_tautracking_tautrack_dm1_Up",
  "weight_emb_tautracking_tautrack_dm11_Up",
  "weight_emb_tautracking_tautrack_dm0dm10_Down",
  "weight_emb_tautracking_tautrack_dm1_Down",
  "weight_emb_tautracking_tautrack_dm11_Down",

  // Shifts of the jet->tauh fake rate
  "weight_ff_jetFR_pt0to25_Up", "weight_ff_jetFR_pt0to25_Down",
  "weight_ff_jetFR_pt25to30_Up", "weight_ff_jetFR_pt25to30_Down",
  "weight_ff_jetFR_pt30to35_Up", "weight_ff_jetFR_pt30to35_Down",
  "weight_ff_jetFR_pt35to40_Up", "weight_ff_jetFR_pt35to40_Down",
  "weight_ff_jetFR_pt40to50_Up", "weight_ff_jetFR_pt40to50_Down",
  "weight_ff_jetFR_pt50to60_Up", "weight_ff_jetFR_pt50to60_Down",
  "weight_ff_jetFR_pt60to80_Up", "weight_ff_jetFR_pt60to80_Down",
  "weight_ff_jetFR_pt80to100_Up", "weight_ff_jetFR_pt80to100_Down",
  "weight_ff_jetFR_pt100to120_Up", "weight_ff_jetFR_pt100to120_Down",
  "weight_ff_jetFR_pt120to150_Up", "weight_ff_jetFR_pt120to150_Down",
  "weight_ff_jetFR_ptgt150_Up", "weight_ff_jetFR_ptgt150_Down",

  // Shifts of the jet->tauh fake rate due to the pt_2 energy scale
  "weight_ff_es2Up", "weight_ff_es2Down",
  // Shifts of the cross trigger fake factor
  "weight_crosstrg_fakefactor_crosstrg_fakefactor_Up", "weight_crosstrg_fakefactor_crosstrg_fakefactor_Down",
  // Shifts of the cross trigger fake factor due to the pt_1 energy scale
  "weight_crosstrg_fakefactor_es1Up", "weight_crosstrg_fakefactor_es1Down",

  // Shifts of the trigger SF
  "weight_mt_trgsf_trgeff_single_Up",
  "weight_mt_trgsf_trgeff_cross_Up",
  "weight_et_trgsf_trgeff_single_Up",
  "weight_et_trgsf_trgeff_cross_Up",
  "weight_em_trgsf_trgeff_Mu8E23_Up",
  "weight_em_trgsf_trgeff_Mu23E12_Up",
  "weight_em_trgsf_trgeff_both_Up",
  "weight_mt_trgsf_trgeff_single_Down",
  "weight_mt_trgsf_trgeff_cross_Down",
  "weight_et_trgsf_trgeff_single_Down",
  "weight_et_trgsf_trgeff_cross_Down",
  "weight_em_trgsf_trgeff_Mu8E23_Down",
  "weight_em_trgsf_trgeff_Mu23E12_Down",
  "weight_em_trgsf_trgeff_both_Down",
  // Shifts of the trigger SF due to the lepton energy scale(s)
  "weight_mt_trgsf_es1Up", "weight_mt_trgsf_es2Up",     "weight_et_trgsf_es1Up",   "weight_et_trgsf_es2Up", "weight_em_trgsf_es1Up", "weight_em_trgsf_es2Up",
  "weight_mt_trgsf_es1Down", "weight_mt_trgsf_es2Down", "weight_et_trgsf_es1Down", "weight_et_trgsf_es2Down", "weight_em_trgsf_es1Down", "weight_em_trgsf_es2Down",

  // Shifts of the muon ID/iso weight due to the muon energy scale
  "weight_muonIdIso_mt_es1Up", "weight_muonIdIso_mt_es1Down", "weight_muonIdIso_em_es2Up", "weight_muonIdIso_em_es2Down",

  // Shift of the electron MVA ID weight due to the electron energy scale
  "weight_eleIdIso_es1Up", "weight_eleIdIso_es1Down",

  // Shifts of the weight due to tau ID WP differences (nominal is 1.0, so no dependence on lepton ES)
  "weight_tauidWP_tauidWP_et_Up", "weight_tauidWP_tauidWP_et_Down",

  // QCD background estimation weights: shifts
  "weight_correction_SScorrection_Up",
  "weight_correction_SScorrection_Down",
  "weight_closure_SSclosure_Up",
  "weight_closure_SSclosure_Down",
  "weight_correction_SSboth2D_Up",
  "weight_correction_SSboth2D_Down",
  "weight_closure_SSboth2D_Up",
  "weight_closure_SSboth2D_Down",
  "weight_osss_osss_Up",
  "weight_osss_osss_Down",
  // Shifts of QCD background estimation weights due to lepton energy scale
  "weight_correction_es1Up",
  "weight_correction_es2Up",
  "weight_correction_es1Down",
  "weight_correction_es2Down",
  "weight_closure_es1Up",
  "weight_closure_es2Up",
  "weight_closure_es1Down",
  "weight_closure_es2Down",

  // Shifts of pileup weight
  "weight_pu_Up",
  "weight_pu_Down"

};

/*****************************************************************/


#endif
