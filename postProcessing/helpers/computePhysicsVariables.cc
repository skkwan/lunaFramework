#include "computePhysicsVariables.h"

ROOT::Math::PtEtaPhiMVector add_p4(float pt, float eta, float phi, float mass) {
  return ROOT::Math::PtEtaPhiMVector(pt, eta, phi, mass);
}


float compute_mjj(ROOT::Math::PtEtaPhiMVector& p4, bool isValidPair) {
      if (isValidPair) return float(p4.M());
      return -9999.f;
}

float compute_ptjj(ROOT::Math::PtEtaPhiMVector& p4, ROOT::RVec<int>& g)
{
  if (Sum(g) >= 2) return float(p4.Pt());
  return -9999.f;
}

float compute_jdeta(float x, float y, ROOT::RVec<int>& g)
{
  if (Sum(g) >= 2) return x - y;
  return -9999.f;
}

float compute_mt(float pt_1, float phi_1, float pt_met, float phi_met)
{
  const auto dphi = Helper::compute_deltaPhi(phi_1, phi_met);
  return std::sqrt(2.0 * pt_1 * pt_met * (1.0 - std::cos(dphi)));
}


float compute_pt_scalar_sum(float pt_1, float pt_2)
{
  return (pt_1 + pt_2);
}

ROOT::Math::PtEtaPhiMVector get_p4(int index, ROOT::RVec<float>& Vec_pt, ROOT::RVec<float>& Vec_eta, ROOT::RVec<float>& Vec_phi, ROOT::RVec<float>& Vec_mass) {
    ROOT::Math::PtEtaPhiMVector p4;
    if (index < 0) {
        p4 = ROOT::Math::PtEtaPhiMVector(0, -99, -999, 0);
    }
    else {
        assert(index < ((int) Vec_pt.size()));
        p4 = ROOT::Math::PtEtaPhiMVector(Vec_pt[index], Vec_eta[index], Vec_phi[index], Vec_mass[index]);
    }

    return p4;
}



/*------------------------------------------------------------------*/

// Variable D_zeta, defined in Equation 3 in HIG-17-024.
// D_zeta is defined to be p_xi - 0.85 p_xi^{vis}
float compute_D_zeta(float pt_met, float phi_met,
		      float pt_1, float phi_1,
		      float pt_2, float phi_2)
{

  // First compute the bisector of the transverse momenta
  TVector3 u1(std::cos(phi_1), std::sin(phi_1), 0);
  TVector3 u2(std::cos(phi_2), std::sin(phi_2), 0);

  TVector3 uBisector = (u1 + u2).Unit();

  // p_xi is the component of \vec{p}_T^{miss} along the uBisector of the
  // transverse momenta of the two tau candidates
  TVector3 ptMiss(pt_met * std::cos(phi_met),
		  pt_met * std::sin(phi_met),
		  0);
  //  std::cout << "ptMiss: " << ptMiss.X() << " "<< ptMiss.Y() << " " <<  ptMiss.Z() << std::endl;
  float p_xi = ptMiss.Dot(uBisector);
  // std::cout << "p_xi: " << p_xi << std::endl;

  // Now, compute p_xi^{vis}
  TVector3 leptonPairTotalPt = (pt_1 * u1) + (pt_2 * u2);

  //  std::cout << "leptonPairTotalPt: " << leptonPairTotalPt.X()<< " "<< leptonPairTotalPt.Y() << " " <<  leptonPairTotalPt.Z() << std::endl;

  float p_xi_vis = leptonPairTotalPt.Dot(uBisector);

  // Compute and return D_zeta
  float D_zeta = p_xi - (0.85 * p_xi_vis);
  // std::cout << "D_zeta: " << D_zeta << std::endl;
  return D_zeta;
}

/*------------------------------------------------------------------*/
