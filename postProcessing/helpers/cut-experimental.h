#ifndef CUT_EXPERIMENTAL_H_INCL
#define CUT_EXPERIMENTAL_H_INCL

#include "helperFunctions.h"
#include "ranges.h"

/******************************************************************************/


class CutExperimental {

 private:
  std::string variable;
  std::vector<std::string> listOfChannelsAffected;
  std::vector<std::string> listOfShifts;
  int x_nbins;
  float x_min;
  float x_max;
  bool embedSeparateName;

 public:
  CutExperimental(std::string myVar, std::vector<std::string> myChannels, std::vector<std::string> myShifts, bool hasSeparateEmbedShift = false) {

        variable = myVar;
        listOfChannelsAffected = myChannels;
        listOfShifts = myShifts;
        embedSeparateName = hasSeparateEmbedShift;

        // Go to the ranges global variable to get the nBins, xMin, xMax for this variable
        ranges_t::iterator it;
        it = ranges.find(myVar);

        // Make sure the entry in ranges was found
        if (it == ranges.end()) {
          std::cout << "[ERROR:] element " << myVar << " not found in ranges, setting default values" << std::endl;
          x_nbins = 30;
          x_min  = 0;
          x_max  = 500;
        }
        else {
          // std::cout << "Found " << myVar << " corresponding to " << it->first << " in ranges" << std::endl;
          x_nbins = it->second[0];
          x_min  = it->second[1];
          x_max  = it->second[2];
        }
    }

  void appendShifts(std::vector<std::string> newShifts) {
    for (auto newShift : newShifts) {
      listOfShifts.push_back(newShift);
    }
  }

  // Getters
  std::string varName() {
    return variable;
  }

  std::vector<std::string> shifts() {
    return listOfShifts;
  }

  std::vector<std::string> channelsAffected() {
    return listOfChannelsAffected;
  }

  bool hasMuTau() {
    return (std::find(listOfChannelsAffected.begin(), listOfChannelsAffected.end(), "mutau") != listOfChannelsAffected.end());
  }

  bool hasETau() {
    return (std::find(listOfChannelsAffected.begin(), listOfChannelsAffected.end(), "etau") != listOfChannelsAffected.end());
  }

  bool hasEMu() {
    return (std::find(listOfChannelsAffected.begin(), listOfChannelsAffected.end(), "emu") != listOfChannelsAffected.end());
  }


  void printShifts() {
    std::cout << "Current shifts for " << varName() << ": ";
    for (auto shift : listOfShifts) {
      std::cout << shift << ", " << std::endl;
    }
    std::cout << std::endl;
  }

  int nBins() {
    return x_nbins;
  }

  float xMin() {
    return x_min;
  }

  float xMax() {
    return x_max;
  }

  bool hasSeparateEmbedShift() {
    return embedSeparateName;
  }

};

/******************************************************************************/



#endif
