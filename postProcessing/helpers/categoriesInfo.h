#ifndef CATEGORIES_INFO_H_INCL
#define CATEGORIES_INFO_H_INCL

#include <cmath>

class CategoriesInfo {


    private:
        std::map<std::string, std::vector<float>> map_Scores;  // map of transformed score boundaries
        int year;              // only 2018 supported currently
        std::string channel;   // only mutau supported currently
        float transformParam;  // n in the analysis note

    public:

        // 2018 mutau only
        CategoriesInfo(int catYear, std::string catChannel) {
            year = catYear;
            channel = catChannel;

            if ((year == 2018) && (channel == "mutau")) {
                map_Scores = {{"b1_sr1", {0.98, 1.00}},
                              {"b1_sr2", {0.95, 0.98}},
                              {"b1_sr3", {0.90, 0.95}},
                              {"b1_cr" , {0.00, 0.90}},

                              {"b2_sr1", {0.99, 1.00}},
                              {"b2_sr2", {0.96, 0.99}},
                              {"b2_cr",  {0.00, 0.96}} };
                transformParam = 1.5;
            }
            else {
                std::cout << "[Warning: categoriesInfo.h] Only 2018 mutau category supported for DNN categories: behavior undefined" << std::endl;
            }
        }

        // Transform the raw DNN score as described in AN-20-213
        float transformScore(float rawScore){
            return ( atanh(rawScore * tanh(transformParam) ) / transformParam ) ;
        }

        // Return ROOT parse-able transformed score
        TString transformedScoreAsTString(TString rawScoreBranch) {
            TString scoreDefinition = TString::Format("( atanh(%s * tanh(%.2f)) / tanh(%.2f) )", rawScoreBranch.Data(), transformParam, transformParam);
            return scoreDefinition;
        }

        float getLow(std::string cat) {
            auto it = map_Scores.find(cat);
            if (it == map_Scores.end() ) {
                std::cout << "Category not found! Returning zero!" << std::endl;
                return 0;
            }
            return it->second[0];
        }

        float getHigh(std::string cat) {
            auto it = map_Scores.find(cat);
            if (it == map_Scores.end() ) {
                std::cout << "Category not found! Returning zero!" << std::endl;
                return 0;
            }
            return it->second[1];
        }

        bool is1b(std::string cat) {
            return (cat.find("b1") != std::string::npos);
        }

        // Return the name of the categories stored in this objects.
        std::vector<std::string> getAllCategoryNames() {
            std::vector<std::string> allNames;
            std::map<std::string, std::vector<float>>::iterator it;
            for (it = map_Scores.begin(); it != map_Scores.end(); it++) {
                allNames.push_back(it->first);
            }

            // std::cout << "[INFO: CategoriesInfo::getAllCategoryNames:] ";
            // for (auto n : allNames) {
            //     std::cout << n << ", ";
            // }
            // std::cout << std::endl;

            return allNames;
        }

        // Return selection (b-jet counting and DNN score) for a given category
        std::string getCategorySelection(std::string category);

        // Return selection which gets all categories. If hasDNN is false, return no cut (""), otherwise the code will
        // try to access the non-existent NN1b and NN2b scores.
        std::string getAllCategoriesSelection(bool hasDNN);

};


std::string CategoriesInfo::getCategorySelection(std::string category) {

    float low = getLow(category);
    float high = getHigh(category);

    // Nominal score: Only two types: 1 b-tag jet or 2 b-tag jets
    TString transformedDnnScore, bJetCounting;
    if (is1b(category)) {
        transformedDnnScore = transformedScoreAsTString("NN1b");
        bJetCounting = "(nbtag20 == 1)";
    }
    else {
        transformedDnnScore = transformedScoreAsTString("NN2b");
        bJetCounting = "(nbtag20 == 2)";
    }

    TString cut;

    // Use <= or < (>= or >) depending on categories
    if ((category == "b1_sr2") || (category == "b1_sr3") || (category == "b2_sr2")) {
        cut.Form("( %s && (%s >= %.2f) && (%s <= %.2f) )", bJetCounting.Data(), transformedDnnScore.Data(), low, transformedDnnScore.Data(), high);
    }
    else if ((category == "b1_sr1") || (category == "b2_sr1")) {
        // Use inclusive upper bound
        cut.Form("( %s && (%s > %.2f)  && (%s <= %.2f) )", bJetCounting.Data(), transformedDnnScore.Data(), low, transformedDnnScore.Data(), high);
    }
    else if ((category == "b1_cr") || (category == "b2_cr")) {
        // Use inclusive lower bound
        cut.Form("( %s && (%s >= %.2f) && (%s < %.2f) )", bJetCounting.Data(), transformedDnnScore.Data(), low, transformedDnnScore.Data(), high);
    }

    // std::cout << "[INFO: CategoriesInfo::getCategoryString:] For category " << category << ", b-tag jet counting and DNN cut: " << cut << std::endl;

    return (std::string) cut;
}


std::string CategoriesInfo::getAllCategoriesSelection(bool hasDNN) {

    std::string cut = "";

    if (hasDNN) {
        // This expression itself needs opening and closing braces
        cut.append("(");
        std::vector<std::string> allNames = getAllCategoryNames();
        for (unsigned int i = 0; i < allNames.size(); i++) {
            cut.append(getCategorySelection(allNames[i]));
            // Take the logical OR of these categories
            if (i < (allNames.size() - 1)) {
                cut.append(" || ");
            }
        }
        // Closing brace
        cut.append(")");
    }
    else {
        cut = " (1) "; // always true
        std::cout << "[INFO: CategoriesInfo::getAllCategoriesSelection:] hasDNN " << hasDNN << "cut: " << cut << std::endl;
    }
    return cut;

}


#endif
