#ifndef EMBEDDED_TAU_TRACKING_EXPERIMENTAL_H_INCL
#define EMBEDDED_TAU_TRACKING_EXPERIMENTAL_H_INCL

#include "helperFunctions.h"
#include "sampleConfig_class.h"

/*******************************************************************/

/*
 * Nominal: https://github.com/hftsoi/aabbtt_finalselection/blob/cefb486856b4c817d20c6b4c49d17eaef115f78e/selection_mt_allyears.cc#L914-L918
 * Same for mutau and etau channels; not applied to emu channel
 */

float getEmbeddedTauTrackingEfficiency_nominal(int channel, int tau_decayMode) {
    float sf_embed = 1.0;
    if ((channel == Helper::mt) || (channel == Helper::et)) {
        if (tau_decayMode == 0) sf_embed = 0.975; // 1prong
        else if (tau_decayMode == 1)  sf_embed = (0.975*1.051); // 1prong + 1strip
        else if (tau_decayMode == 10) sf_embed = (0.975*0.975*0.975); // 3 prongs
        else if (tau_decayMode == 11) sf_embed = (0.975*0.975*0.975*1.051); // 3prongs + 1strip
    }
    return sf_embed;
}

/*
 * Up/down shifts: https://github.com/hftsoi/aabbtt_finalselection/blob/cefb486856b4c817d20c6b4c49d17eaef115f78e/selection_mt_allyears.cc#L1791-L1814
 * https://github.com/hftsoi/aabbtt_finalselection/blob/master/selection_et_allyears.cc#L1820-L1842
 */

ROOT::RVecF getEmbeddedTauTrackingEfficiency_variations(int channel, int tau_decayMode, float weight_emb_tautracking, std::string sysToDo) {
    // Default
    ROOT::RVecF shifts = ROOT::RVecF{weight_emb_tautracking, weight_emb_tautracking};
    if ((channel == Helper::mt) || (channel == Helper::et)) {
        if ((tau_decayMode == 0) && (sysToDo == "tautrack_dm0dm10")) {
            shifts = {(float) (weight_emb_tautracking * 1.01), (float) (weight_emb_tautracking * 0.99)}; // +/- 1%
        }
        else if ((tau_decayMode == 10) && (sysToDo == "tautrack_dm0dm10")) {  // same systematic as dm0, but shifted by a different amount
            shifts = {(float) (weight_emb_tautracking * 1.03), (float) (weight_emb_tautracking * 0.97)}; // +/- 3%
        }
        else if ((tau_decayMode == 1) && (sysToDo == "tautrack_dm1")) {
            shifts = {(float) (weight_emb_tautracking * 1.025), (float) (weight_emb_tautracking * 0.975)}; // +/- 2.5%
        }
        else if ((tau_decayMode == 11) && (sysToDo == "tautrack_dm11")) {
            shifts = {(float) (weight_emb_tautracking * 1.06), (float) (weight_emb_tautracking * 0.94)}; // +/- 6%
        }
    }
    return shifts;
}

/*******************************************************************/

/*
 * In the MiniAOD code it appears only the embedded samples have a tau tracking efficiency
 * https://github.com/hftsoi/aabbtt_finalselection/blob/cefb486856b4c817d20c6b4c49d17eaef115f78e/selection_mt_allyears.cc#L504
 */

template <typename T>
auto GetEmbeddedTauTrackingEfficiency_experimental(T &df, LUNA::sampleConfig_t &sConfig) {

    if (sConfig.isEmbedded()) {
        if (sConfig.doShifts()) {
            return df.Define("weight_emb_tautracking_nominal", getEmbeddedTauTrackingEfficiency_nominal, {"channel", "decayMode_2"})
                     .Define("tautrack_dm0dm10", []() { return (std::string) "tautrack_dm0dm10"; })
                     .Define("tautrack_dm1",     []() { return (std::string) "tautrack_dm1";  })
                     .Define("tautrack_dm11",    []() { return (std::string) "tautrack_dm11"; })
                     .Define("variations_weight_emb_tautracking_tautrack_dm0dm10", getEmbeddedTauTrackingEfficiency_variations, {"channel", "decayMode_2", "weight_emb_tautracking_nominal", "tautrack_dm0dm10"})
                     .Define("variations_weight_emb_tautracking_tautrack_dm1",     getEmbeddedTauTrackingEfficiency_variations, {"channel", "decayMode_2", "weight_emb_tautracking_nominal", "tautrack_dm1"})
                     .Define("variations_weight_emb_tautracking_tautrack_dm11",    getEmbeddedTauTrackingEfficiency_variations, {"channel", "decayMode_2", "weight_emb_tautracking_nominal", "tautrack_dm11"})
                     // Unpack
                     .Define("weight_emb_tautracking_tautrack_dm0dm10_Up",   "variations_weight_emb_tautracking_tautrack_dm0dm10[0]")
                     .Define("weight_emb_tautracking_tautrack_dm1_Up",       "variations_weight_emb_tautracking_tautrack_dm1[0]")
                     .Define("weight_emb_tautracking_tautrack_dm11_Up",      "variations_weight_emb_tautracking_tautrack_dm11[0]")
                     .Define("weight_emb_tautracking_tautrack_dm0dm10_Down", "variations_weight_emb_tautracking_tautrack_dm0dm10[1]")
                     .Define("weight_emb_tautracking_tautrack_dm1_Down",     "variations_weight_emb_tautracking_tautrack_dm1[1]")
                     .Define("weight_emb_tautracking_tautrack_dm11_Down",    "variations_weight_emb_tautracking_tautrack_dm11[1]");
        }
    }

    return df.Define("weight_emb_tautracking_nominal", []() { return 1.0f; })
             .Define("weight_emb_tautracking_tautrack_dm0dm10_Up",   []() { return 1.0f; })
             .Define("weight_emb_tautracking_tautrack_dm1_Up",       []() { return 1.0f; })
             .Define("weight_emb_tautracking_tautrack_dm11_Up",      []() { return 1.0f; })
             .Define("weight_emb_tautracking_tautrack_dm0dm10_Down", []() { return 1.0f; })
             .Define("weight_emb_tautracking_tautrack_dm1_Down",     []() { return 1.0f; })
             .Define("weight_emb_tautracking_tautrack_dm11_Down",    []() { return 1.0f; });


}


/*******************************************************************/



#endif
