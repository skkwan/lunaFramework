#ifndef JET_FAKE_BACKGROUND_CROSS_TRIGGER_EXPERIMENTAL_H_INCL
#define JET_FAKE_BACKGROUND_CROSS_TRIGGER_EXPERIMENTAL_H_INCL

#include "helperFunctions.h"
#include "sampleConfig_class.h"

/*******************************************************************/

/*
 * Only applied to events in the anti-isolated tau region for jet fake backgrounds in mutau and etau
 * https://github.com/hftsoi/aabbtt_finalselection/blob/master/selection_et_allyears.cc#L2335-L2348
 */

float getCrossTriggerFakeFactor(int channel, int compute_xtrg_ff, int passCrossTrigger_mt, int passCrossTrigger_et,
                                float pt_1,
                                float pt_1_mt_min, float pt_1_mt_max,
                                float pt_1_et_min, float pt_1_et_max) {
    float xtrg_ff = 1.0;
    if (compute_xtrg_ff) {
        if ((channel == Helper::mt) && (passCrossTrigger_mt && (pt_1 > pt_1_mt_min) && (pt_1 < pt_1_mt_max))) {
            xtrg_ff = 1.5;
        }
        else if ((channel == Helper::et) && (passCrossTrigger_et && (pt_1 > pt_1_et_min) && (pt_1 < pt_1_et_max))) {
            xtrg_ff = 1.5;
        }
    }
    return xtrg_ff;
}

/*******************************************************************/

/*
 * This systematic is not binned
 * https://github.com/hftsoi/aabbtt_finalselection/blob/master/selection_et_allyears.cc#L2335-L2348
 */

ROOT::RVecF getCrossTriggerFakeFactor_variations(int channel, int compute_xtrg_ff, int passCrossTrigger_mt, int passCrossTrigger_et,
                                                float pt_1,
                                                float pt_1_mt_min, float pt_1_mt_max,
                                                float pt_1_et_min, float pt_1_et_max) {
    ROOT::RVecF shifts = ROOT::RVecF{(float) 1.0, (float) 1.0};
    if (compute_xtrg_ff) {
        if ((channel == Helper::mt) && (passCrossTrigger_mt && (pt_1 > pt_1_mt_min) && (pt_1 < pt_1_mt_max))) {
            shifts = {1.6, 1.4};
        }
        else if ((channel == Helper::et) && (passCrossTrigger_et && (pt_1 > pt_1_et_min) && (pt_1 < pt_1_et_max))) {
            shifts = {1.6, 1.4};
        }
    }
    return shifts;
}

/*******************************************************************/

/*
 * For mutau and etau: this is applied to anti-isolated events in the jet->tauh fake background:
 * For events that were selected with the cross-trigger, tighter pT cuts were applied which results in lower yields,
 * so we need to scale these up. The nominal value is 1.5
 */
template <typename T>
auto GetJetFakeBackground_CrossTriggerShifts_experimental(T &df, LUNA::sampleConfig_t &sConfig) {

    // In principle, in the main() function this weight should only be applied to anti-isolated events in mt/et.
    // Nonetheless, to be safe, here we also only return a non-1.0 weight for anti-isolated events in mt/et.
    auto df2 = df.Define("rejectEMBduplicate", [](int isMCnonHiggs, unsigned int gen_match_1, unsigned int gen_match_2) {
                            return (int) (!(isMCnonHiggs) || (isMCnonHiggs && !(gen_match_1>2 && gen_match_1<6 && gen_match_2>2 && gen_match_2<6)));
                            }, {"isMCnonHiggs", "gen_match_1", "gen_match_2"})
                 .Define("compute_xtrg_ff", [](int channel, int channel_mt, int channel_et, int byVVVLooseDeepVSjet_2, int byMediumDeepVSjet_2, int isSignal, int rejectEMBduplicate) {
                            int do_xtrg_ff = false;
                            if ((channel == channel_mt) || (channel == channel_et)) {
                                do_xtrg_ff = (byVVVLooseDeepVSjet_2 && !byMediumDeepVSjet_2 && !isSignal && rejectEMBduplicate);
                            }
                            return do_xtrg_ff;
                            }, {"channel", "channel_mt", "channel_et", "byVVVLooseDeepVSjet_2", "byMediumDeepVSjet_2", "isSignal", "rejectEMBduplicate"})
                .Define("weight_crosstrg_fakefactor_nominal", getCrossTriggerFakeFactor, {"channel", "compute_xtrg_ff", "passCrossTrigger_mt_nominal", "passCrossTrigger_et_nominal",
                                                                             "pt_1_nominal",
                                                                             "mt_cross_mupt_thres", "mt_single_mupt_thres",
                                                                             "et_cross_elept_thres", "et_single_elept_thres"});
    if (sConfig.doShifts()) {
        // Shifts of the cross trigger fake factor
        return df2.Define("variations_weight_crosstrg_fakefactor", getCrossTriggerFakeFactor_variations, {"channel", "compute_xtrg_ff", "passCrossTrigger_mt_nominal", "passCrossTrigger_et_nominal",
                                                                             "pt_1_nominal",
                                                                             "mt_cross_mupt_thres", "mt_single_mupt_thres",
                                                                             "et_cross_elept_thres", "et_single_elept_thres"})
        // Unpack
                   .Define("weight_crosstrg_fakefactor_crosstrg_fakefactor_Up",   "variations_weight_crosstrg_fakefactor[0]")
                   .Define("weight_crosstrg_fakefactor_crosstrg_fakefactor_Down", "variations_weight_crosstrg_fakefactor[1]")
        // Shifts of the weight due to shifts in pt_1's energy scale (note the trigger decisions and the pt are affected!)
                   .Define("weight_crosstrg_fakefactor_es1Up", getCrossTriggerFakeFactor, {"channel", "compute_xtrg_ff",
                                                                             "passCrossTrigger_mt_es1Up", "passCrossTrigger_et_es1Up",
                                                                             "pt_1_es1Up",
                                                                             "mt_cross_mupt_thres", "mt_single_mupt_thres",
                                                                             "et_cross_elept_thres", "et_single_elept_thres"})
                   .Define("weight_crosstrg_fakefactor_es1Down", getCrossTriggerFakeFactor, {"channel", "compute_xtrg_ff",
                                                                             "passCrossTrigger_mt_es1Down", "passCrossTrigger_et_es1Down",
                                                                             "pt_1_es1Down",
                                                                             "mt_cross_mupt_thres", "mt_single_mupt_thres",
                                                                             "et_cross_elept_thres", "et_single_elept_thres"});
    }
    else {
        return df2;
    }
}

/*******************************************************************/

#endif
