#ifndef TRIGGER_DECISIONS_TAUTAU_H_INCL
#define TRIGGER_DECISIONS_TAUTAU_H_INCL


#include "helperFunctions.h"
#include "sampleConfig_class.h"

/**********************************************************/


/*
 * Helper function to simplify the calls later:
 * Check if an event passes a single trigger. NOTE: currently comments out the filter bits, these have buggy behaviour in NanoAOD v9.
 */
int passes_single_trigger(int passes_hlt, int passes_filter, int is_trigObj_matched, float pt_1, float pt_threshold_1, float pt_2, float pt_threshold_2) {
                                (void) passes_filter;
                                return (passes_hlt && is_trigObj_matched && (pt_1 > pt_threshold_1) && (pt_2 > pt_threshold_2));
                            }

// Slightly different conditions for 2016. NOTE: currently comments out the filter bits, these have buggy behaviour in NanoAOD v9.
// Two single trigger HLT paths (nicknamed "1" and "2") have the same filter bits to check. "2" checks also the eta.
// Two single trigger HLT paths (nicknamed "3" and "4" have the same filter bits to check). "4" checks also the eta.
int passes_single_trigger_2016(int passes_hlt1, int passes_hlt2, int passes_filter_1or2,
                               int passes_hlt3, int passes_hlt4, int passes_filter_3or4,
                               int is_trigObj_matched,
                               float pt_1, float pt_threshold_1,
                               float pt_2, float pt_threshold_2,
                               float eta_1, float eta_threshold_1,
                               float eta_2, float eta_threshold_2) {
    (void) passes_filter_1or2;
    (void) passes_filter_3or4;
    return (passes_hlt1 || passes_hlt2 || passes_hlt3 || passes_hlt4) && is_trigObj_matched && (pt_1 > pt_threshold_1)
        && (pt_2 > pt_threshold_2) && (abs(eta_1) < eta_threshold_1) && (abs(eta_2) < eta_threshold_2);
}

/*
 * Helper function to simplify the calls lateR:
 * _1 and _2 refer to the two legs for the cross trigger. NOTE: currently comments out the filter bits, these have buggy behaviour in NanoAOD v9.
 */
int passes_cross_trigger(int isEmbedded, int passes_cross_hlt, int passes_cross_filter,
                          int is_trigObj_matched_1, int is_trigObj_matched_2,
                          int filterBits1, int filterBits2,
                          float pt_1, float pt_1_min, float pt_1_max, // first leg has a min and max
                          float pt_2, float pt_2_min,                 // second leg only has a min
                          float eta_2, float eta_2_max) {
                            (void) filterBits1;
                            (void) filterBits2;
                            if (isEmbedded) {
                                return (passes_cross_filter && is_trigObj_matched_1 && is_trigObj_matched_2 &&
                                    (pt_1 > pt_1_min) && (pt_1 < pt_1_max) &&
                                    (pt_2 > pt_2_min) &&
                                    (abs(eta_2) < eta_2_max));
                            }
                            else {
                                return (passes_cross_hlt && passes_cross_filter && is_trigObj_matched_1 && is_trigObj_matched_2 &&
                                    (pt_1 > pt_1_min) && (pt_1 < pt_1_max) &&
                                    (pt_2 > pt_2_min) &&
                                    (abs(eta_2) < eta_2_max));
                            }
                          }

// Slightly different conditions for 2016. Two HLT cross-triggers for mutau
int passes_cross_trigger_2016(int passes_cross_hlt1, int passes_cross_hlt2,
                              int filterBit,
                              int is_trigObj_matched_1, int is_trigObj_matched_2,
                              float pt_1, float pt_1_min, float pt_1_max, // first leg has a min and max
                              float pt_2, float pt_2_min,
                              float eta_1, float eta_max_1,
                              float eta_2, float eta_max_2) {
                                (void) filterBit;
                                return ((passes_cross_hlt1 || passes_cross_hlt2) && is_trigObj_matched_1 && is_trigObj_matched_2 &&
                                        (pt_1 > pt_1_min) && (pt_1 < pt_1_max) &&
                                        (pt_2 > pt_2_min) &&
                                        (abs(eta_1) < eta_max_1) &&
                                        (abs(eta_2) < eta_max_2));
                              }

/*
 * _1 and _2 refer to the two legs for the cross trigger. Simpler version of the above function, where we only have pT minima to check.
 */
int passes_cross_trigger_pt_min_only(int isEmbedded, int passes_cross_hlt, int passes_cross_filter,
                                      int is_trigObj_matched_1, int is_trigObj_matched_2,
                                      float pt_1, float pt_1_min,
                                      float pt_2, float pt_2_min) {
                                        (void) passes_cross_filter;
                                        if (isEmbedded) {
                                            return (is_trigObj_matched_1 && is_trigObj_matched_2 &&
                                                (pt_1 > pt_1_min) && (pt_2 > pt_2_min));
                                        }
                                        else {
                                            return (passes_cross_hlt &&
                                                    is_trigObj_matched_1 && is_trigObj_matched_2 &&
                                                    (pt_1 > pt_1_min) && (pt_2 > pt_2_min));
                                        }
                                      }


/**********************************************************/

/*
 * 2018: Goal: define passSingleTrigger_{mt, et}, passCrossTrigger_{mt, et, em}
 */
template <typename T>
auto GetTauTauTriggers_2018(T &df, LUNA::sampleConfig_t &sConfig) {

    if (sConfig.year() == 2018) {
        // Mutau 2018 nominal
        auto df2 = df.Define("mt_single_mupt_thres",  [] { return 25.0f; })
                 .Define("mt_single_taupt_thres", [] { return 20.0f; })
                 .Define("mt_cross_mupt_thres",  [] { return 21.0f; })
                 .Define("mt_cross_taupt_thres", [] { return 35.0f; })
                 .Define("mt_cross_eta_thres", [] { return 2.1f; })

                 .Define("passSingle_1_mt_nominal", passes_single_trigger, {"pass_mt_HLT_IsoMu24", "filter_mt_Mu24", "match1", "pt_1_nominal", "mt_single_mupt_thres", "pt_2_nominal", "mt_single_taupt_thres"})
                 .Define("passSingle_2_mt_nominal", passes_single_trigger, {"pass_mt_HLT_IsoMu27", "filter_mt_Mu27", "match1", "pt_1_nominal", "mt_single_mupt_thres", "pt_2_nominal", "mt_single_taupt_thres"})
                 .Define("passSingleTrigger_mt_nominal", [](int single1, int single2) { return (int) (single1 || single2); }, {"passSingle_1_mt_nominal", "passSingle_2_mt_nominal"})
                 .Define("passCrossTrigger_nonHPS_mt_nominal", passes_cross_trigger, {"isEmbedded", "pass_mt_HLT_Mu20Tau27", "filter_mt_Mu20Tau27", "match1", "match2",
                                                    "filterBits1", "filterBits2",
                                                    "pt_1_nominal", "mt_cross_mupt_thres", "mt_single_mupt_thres",
                                                    "pt_2_nominal", "mt_cross_taupt_thres",
                                                    "eta_2",  "mt_cross_eta_thres"})
                 .Define("passCrossTrigger_HPS_mt_nominal", passes_cross_trigger, {"isEmbedded", "pass_mt_HLT_Mu20HPSTau27", "filter_mt_Mu20HPSTau27", "match1", "match2",
                                                    "filterBits1", "filterBits2",
                                                    "pt_1_nominal", "mt_cross_mupt_thres", "mt_single_mupt_thres",
                                                    "pt_2_nominal", "mt_cross_taupt_thres",
                                                    "eta_2",  "mt_cross_eta_thres"})
                 .Define("passCrossTrigger_mt_nominal", [](unsigned int run, int isData, int cross, int crossHPS) {
                    if (isData && run<317509)                // Data early runs only: cone-based trigger
                        return (cross);
                    else
                        return (crossHPS);
                    }, {"run", "isData", "passCrossTrigger_nonHPS_mt_nominal", "passCrossTrigger_HPS_mt_nominal"})
    // Etau 2018 nominal
                .Define("et_single_elept_thres",  [] { return (float) 33.0; })
                .Define("et_single_taupt_thres", [] { return (float) 20.0; })
                .Define("et_cross_elept_thres",  [] { return (float) 25.0; })
                .Define("et_cross_taupt_thres", [] { return (float) 35.0; })
                .Define("et_cross_eta_thres",  [] { return (float) 2.1; })

                .Define("passSingle_1_et_nominal", passes_single_trigger, {"pass_et_HLT_Ele32", "filter_et_Ele32", "match1", "pt_1_nominal", "et_single_elept_thres", "pt_2_nominal", "et_single_taupt_thres"})
                .Define("passSingle_2_et_nominal", passes_single_trigger, {"pass_et_HLT_Ele35", "filter_et_Ele35", "match1", "pt_1_nominal", "et_single_elept_thres", "pt_2_nominal", "et_single_taupt_thres"})
                .Define("passSingleTrigger_et_nominal", [](int single1, int single2) { return (int) (single1 || single2); }, {"passSingle_1_et_nominal", "passSingle_2_et_nominal"})
                .Define("passCrossTrigger_nonHPS_et_nominal", passes_cross_trigger, {"isEmbedded", "pass_et_HLT_Ele24Tau30",  "filter_et_Ele24Tau30", "match1", "match2",
                                                                        "filterBits1", "filterBits2",
                                                                        "pt_1_nominal", "et_cross_elept_thres", "et_single_elept_thres",
                                                                        "pt_2_nominal", "et_cross_taupt_thres",
                                                                        "eta_2", "et_cross_eta_thres"})
                .Define("passCrossTrigger_HPS_et_nominal", passes_cross_trigger, {"isEmbedded", "pass_et_HLT_Ele24HPSTau30",  "filter_et_Ele24HPSTau30", "match1", "match2",
                                                                        "filterBits1", "filterBits2",
                                                                        "pt_1_nominal", "et_cross_elept_thres", "et_single_elept_thres",
                                                                        "pt_2_nominal", "et_cross_taupt_thres",
                                                                        "eta_2", "et_cross_eta_thres"})
                .Define("passCrossTrigger_et_nominal", [](unsigned int run, int isData, int cross, int crossHPS) {
                    if (isData && run<317509)                // Data early runs only: cone-based trigger
                        return (cross);
                    else
                        return (crossHPS);
                    }, {"run", "isData", "passCrossTrigger_nonHPS_et_nominal", "passCrossTrigger_HPS_et_nominal"})


    // Emu 2018 nominal
            // EMu 2018
                .Define("em_crossLeadingMuon_elept_thres", [] { return (float) 13.0; })  // for Mu23Ele12
                .Define("em_crossLeadingMuon_mupt_thres",  [] { return (float) 24; })
                .Define("em_crossLeadingEle_elept_thres", [] { return (float) 24; })  // for Mu8Ele23
                .Define("em_crossLeadingEle_mupt_thres",  [] { return (float) 13; })
                .Define("passCross_leadingMuon_em_nominal", passes_cross_trigger_pt_min_only, {"isEmbedded", "pass_em_HLT_Mu23Ele12", "filter_em_Mu23Ele12", "match1", "match2",
                                                                                       "pt_1_nominal", "em_crossLeadingMuon_elept_thres",
                                                                                       "pt_2_nominal", "em_crossLeadingMuon_mupt_thres"})
                .Define("passCross_leadingEle_em_nominal",  passes_cross_trigger_pt_min_only, {"isEmbedded", "pass_em_HLT_Mu8Ele23", "filter_em_Mu8Ele23", "match1", "match2",
                                                                                       "pt_1_nominal", "em_crossLeadingEle_elept_thres",
                                                                                       "pt_2_nominal", "em_crossLeadingEle_mupt_thres"})
                .Define("passCrossTrigger_em_nominal", [](int cross1, int cross2) { return (int) (cross1 || cross2); }, {"passCross_leadingMuon_em_nominal", "passCross_leadingEle_em_nominal"});



    if (sConfig.doShifts()) {
                // mutau 2018: es1Up (shifts due to muon ES up/down): repeat this entire block for es1Up/Down, where pt_2 is nominal
        return df2.Define("passSingle_1_mt_es1Up", passes_single_trigger, {"pass_mt_HLT_IsoMu24", "filter_mt_Mu24", "match1", "pt_1_es1Up", "mt_single_mupt_thres", "pt_2_nominal", "mt_single_taupt_thres"})
                 .Define("passSingle_2_mt_es1Up", passes_single_trigger, {"pass_mt_HLT_IsoMu27", "filter_mt_Mu27", "match1", "pt_1_es1Up", "mt_single_mupt_thres", "pt_2_nominal", "mt_single_taupt_thres"})
                 .Define("passSingleTrigger_mt_es1Up", [](int single1, int single2) { return (int) (single1 || single2); }, {"passSingle_1_mt_es1Up", "passSingle_2_mt_es1Up"})
                 .Define("passCrossTrigger_nonHPS_mt_es1Up", passes_cross_trigger, {"isEmbedded", "pass_mt_HLT_Mu20Tau27", "filter_mt_Mu20Tau27", "match1", "match2",
                                                    "filterBits1", "filterBits2",
                                                    "pt_1_es1Up", "mt_cross_mupt_thres", "mt_single_mupt_thres",
                                                    "pt_2_nominal", "mt_cross_taupt_thres",
                                                    "eta_2",  "mt_cross_eta_thres"})
                 .Define("passCrossTrigger_HPS_mt_es1Up", passes_cross_trigger, {"isEmbedded", "pass_mt_HLT_Mu20HPSTau27", "filter_mt_Mu20HPSTau27", "match1", "match2",
                                                    "filterBits1", "filterBits2",
                                                    "pt_1_es1Up", "mt_cross_mupt_thres", "mt_single_mupt_thres",
                                                    "pt_2_nominal", "mt_cross_taupt_thres",
                                                    "eta_2",  "mt_cross_eta_thres"})
                 .Define("passCrossTrigger_mt_es1Up", [](unsigned int run, int isData, int cross, int crossHPS) {
                    if (isData && run<317509)                // Data early runs only: cone-based trigger
                        return (cross);
                    else
                        return (crossHPS);
                    }, {"run", "isData", "passCrossTrigger_nonHPS_mt_es1Up", "passCrossTrigger_HPS_mt_es1Up"})

                // mutau 2018: es1Down
                 .Define("passSingle_1_mt_es1Down", passes_single_trigger, {"pass_mt_HLT_IsoMu24", "filter_mt_Mu24", "match1", "pt_1_es1Down", "mt_single_mupt_thres", "pt_2_nominal", "mt_single_taupt_thres"})
                 .Define("passSingle_2_mt_es1Down", passes_single_trigger, {"pass_mt_HLT_IsoMu27", "filter_mt_Mu27", "match1", "pt_1_es1Down", "mt_single_mupt_thres", "pt_2_nominal", "mt_single_taupt_thres"})
                 .Define("passSingleTrigger_mt_es1Down", [](int single1, int single2) { return (int) (single1 || single2); }, {"passSingle_1_mt_es1Down", "passSingle_2_mt_es1Down"})
                 .Define("passCrossTrigger_nonHPS_mt_es1Down", passes_cross_trigger, {"isEmbedded", "pass_mt_HLT_Mu20Tau27", "filter_mt_Mu20Tau27", "match1", "match2",
                                                    "filterBits1", "filterBits2",
                                                    "pt_1_es1Down", "mt_cross_mupt_thres", "mt_single_mupt_thres",
                                                    "pt_2_nominal", "mt_cross_taupt_thres",
                                                    "eta_2",  "mt_cross_eta_thres"})
                 .Define("passCrossTrigger_HPS_mt_es1Down", passes_cross_trigger, {"isEmbedded", "pass_mt_HLT_Mu20HPSTau27", "filter_mt_Mu20HPSTau27", "match1", "match2",
                                                    "filterBits1", "filterBits2",
                                                    "pt_1_es1Down", "mt_cross_mupt_thres", "mt_single_mupt_thres",
                                                    "pt_2_nominal", "mt_cross_taupt_thres",
                                                    "eta_2",  "mt_cross_eta_thres"})
                 .Define("passCrossTrigger_mt_es1Down", [](unsigned int run, int isData, int cross, int crossHPS) {
                    if (isData && run<317509)                // Data early runs only: cone-based trigger
                        return (cross);
                    else
                        return (crossHPS);
                    }, {"run", "isData", "passCrossTrigger_nonHPS_mt_es1Down", "passCrossTrigger_HPS_mt_es1Down"})


                // mutau 2018: es2Up, using pt_1_nominal
                 .Define("passSingle_1_mt_es2Up", passes_single_trigger, {"pass_mt_HLT_IsoMu24", "filter_mt_Mu24", "match1", "pt_1_nominal", "mt_single_mupt_thres", "pt_2_es2Up", "mt_single_taupt_thres"})
                 .Define("passSingle_2_mt_es2Up", passes_single_trigger, {"pass_mt_HLT_IsoMu27", "filter_mt_Mu27", "match1", "pt_1_nominal", "mt_single_mupt_thres", "pt_2_es2Up", "mt_single_taupt_thres"})
                 .Define("passSingleTrigger_mt_es2Up", [](int single1, int single2) { return (int) (single1 || single2); }, {"passSingle_1_mt_es2Up", "passSingle_2_mt_es2Up"})
                 .Define("passCrossTrigger_nonHPS_mt_es2Up", passes_cross_trigger, {"isEmbedded", "pass_mt_HLT_Mu20Tau27", "filter_mt_Mu20Tau27", "match1", "match2",
                                                                                    "filterBits1", "filterBits2",
                                                                                    "pt_1_nominal", "mt_cross_mupt_thres", "mt_single_mupt_thres",
                                                                                    "pt_2_es2Up", "mt_cross_taupt_thres",
                                                                                    "eta_2",  "mt_cross_eta_thres"})
                 .Define("passCrossTrigger_HPS_mt_es2Up", passes_cross_trigger, {"isEmbedded", "pass_mt_HLT_Mu20HPSTau27", "filter_mt_Mu20HPSTau27", "match1", "match2",
                                                                                 "filterBits1", "filterBits2",
                                                                                 "pt_1_nominal", "mt_cross_mupt_thres", "mt_single_mupt_thres",
                                                                                 "pt_2_es2Up", "mt_cross_taupt_thres",
                                                                                 "eta_2",  "mt_cross_eta_thres"})
                 .Define("passCrossTrigger_mt_es2Up", [](unsigned int run, int isData, int cross, int crossHPS) {
                    if (isData && run<317509)                // Data early runs only: cone-based trigger
                        return (cross);
                    else
                        return (crossHPS);
                    }, {"run", "isData", "passCrossTrigger_nonHPS_mt_es2Up", "passCrossTrigger_HPS_mt_es2Up"})

                // mutau 2018: es2Down, using pt_1_nominal
                 .Define("passSingle_1_mt_es2Down", passes_single_trigger, {"pass_mt_HLT_IsoMu24", "filter_mt_Mu24", "match1", "pt_1_nominal", "mt_single_mupt_thres", "pt_2_es2Down", "mt_single_taupt_thres"})
                 .Define("passSingle_2_mt_es2Down", passes_single_trigger, {"pass_mt_HLT_IsoMu27", "filter_mt_Mu27", "match1", "pt_1_nominal", "mt_single_mupt_thres", "pt_2_es2Down", "mt_single_taupt_thres"})
                 .Define("passSingleTrigger_mt_es2Down", [](int single1, int single2) { return (int) (single1 || single2); }, {"passSingle_1_mt_es2Down", "passSingle_2_mt_es2Down"})
                 .Define("passCrossTrigger_nonHPS_mt_es2Down", passes_cross_trigger, {"isEmbedded", "pass_mt_HLT_Mu20Tau27", "filter_mt_Mu20Tau27", "match1", "match2",
                                                                                      "filterBits1", "filterBits2",
                                                                                      "pt_1_nominal", "mt_cross_mupt_thres", "mt_single_mupt_thres",
                                                                                      "pt_2_es2Down", "mt_cross_taupt_thres",
                                                                                      "eta_2",  "mt_cross_eta_thres"})
                 .Define("passCrossTrigger_HPS_mt_es2Down", passes_cross_trigger, {"isEmbedded", "pass_mt_HLT_Mu20HPSTau27", "filter_mt_Mu20HPSTau27", "match1", "match2",
                                                                                   "filterBits1", "filterBits2",
                                                                                   "pt_1_nominal", "mt_cross_mupt_thres", "mt_single_mupt_thres",
                                                                                   "pt_2_es2Down", "mt_cross_taupt_thres",
                                                                                   "eta_2",  "mt_cross_eta_thres"})
                 .Define("passCrossTrigger_mt_es2Down", [](unsigned int run, int isData, int cross, int crossHPS) {
                    if (isData && run<317509)                // Data early runs only: cone-based trigger
                        return (cross);
                    else
                        return (crossHPS);
                    }, {"run", "isData", "passCrossTrigger_nonHPS_mt_es2Down", "passCrossTrigger_HPS_mt_es2Down"})


                // etau 2018: shifts due to es1Up (electron ES), using pt_2_nominal
                .Define("passSingle_1_et_es1Up", passes_single_trigger, {"pass_et_HLT_Ele32", "filter_et_Ele32", "match1", "pt_1_es1Up", "et_single_elept_thres", "pt_2_nominal", "et_single_taupt_thres"})
                .Define("passSingle_2_et_es1Up", passes_single_trigger, {"pass_et_HLT_Ele35", "filter_et_Ele35", "match1", "pt_1_es1Up", "et_single_elept_thres", "pt_2_nominal", "et_single_taupt_thres"})
                .Define("passSingleTrigger_et_es1Up", [](int single1, int single2) { return (int) (single1 || single2); }, {"passSingle_1_et_es1Up", "passSingle_2_et_es1Up"})
                .Define("passCrossTrigger_nonHPS_et_es1Up", passes_cross_trigger, {"isEmbedded", "pass_et_HLT_Ele24Tau30",  "filter_et_Ele24Tau30", "match1", "match2",
                                                                        "filterBits1", "filterBits2",
                                                                        "pt_1_es1Up", "et_cross_elept_thres", "et_single_elept_thres",
                                                                        "pt_2_nominal", "et_cross_taupt_thres",
                                                                        "eta_2", "et_cross_eta_thres"})
                .Define("passCrossTrigger_HPS_et_es1Up", passes_cross_trigger, {"isEmbedded", "pass_et_HLT_Ele24HPSTau30",  "filter_et_Ele24HPSTau30", "match1", "match2",
                                                                        "filterBits1", "filterBits2",
                                                                        "pt_1_es1Up", "et_cross_elept_thres", "et_single_elept_thres",
                                                                        "pt_2_nominal", "et_cross_taupt_thres",
                                                                        "eta_2", "et_cross_eta_thres"})
                 .Define("passCrossTrigger_et_es1Up", [](unsigned int run, int isData, int cross, int crossHPS) {
                    if (isData && run<317509)                // Data early runs only: cone-based trigger
                        return (cross);
                    else
                        return (crossHPS);
                    }, {"run", "isData", "passCrossTrigger_nonHPS_et_es1Up", "passCrossTrigger_HPS_et_es1Up"})


                // etau 2018: shifts due to es1Down (electronES), using pt_2_nominal
                .Define("passSingle_1_et_es1Down", passes_single_trigger, {"pass_et_HLT_Ele32", "filter_et_Ele32", "match1", "pt_1_es1Down", "et_single_elept_thres", "pt_2_nominal", "et_single_taupt_thres"})
                .Define("passSingle_2_et_es1Down", passes_single_trigger, {"pass_et_HLT_Ele35", "filter_et_Ele35", "match1", "pt_1_es1Down", "et_single_elept_thres", "pt_2_nominal", "et_single_taupt_thres"})
                .Define("passSingleTrigger_et_es1Down", [](int single1, int single2) { return (int) (single1 || single2); }, {"passSingle_1_et_es1Down", "passSingle_2_et_es1Down"})
                .Define("passCrossTrigger_nonHPS_et_es1Down", passes_cross_trigger, {"isEmbedded", "pass_et_HLT_Ele24Tau30",  "filter_et_Ele24Tau30", "match1", "match2",
                                                                        "filterBits1", "filterBits2",
                                                                        "pt_1_es1Down", "et_cross_elept_thres", "et_single_elept_thres",
                                                                        "pt_2_nominal", "et_cross_taupt_thres",
                                                                        "eta_2", "et_cross_eta_thres"})

                .Define("passCrossTrigger_HPS_et_es1Down", passes_cross_trigger, {"isEmbedded", "pass_et_HLT_Ele24HPSTau30",  "filter_et_Ele24HPSTau30", "match1", "match2",
                                                                        "filterBits1", "filterBits2",
                                                                        "pt_1_es1Down", "et_cross_elept_thres", "et_single_elept_thres",
                                                                        "pt_2_nominal", "et_cross_taupt_thres",
                                                                        "eta_2", "et_cross_eta_thres"})
                 .Define("passCrossTrigger_et_es1Down", [](unsigned int run, int isData, int cross, int crossHPS) {
                    if (isData && run<317509)                // Data early runs only: cone-based trigger
                        return (cross);
                    else
                        return (crossHPS);
                    }, {"run", "isData", "passCrossTrigger_nonHPS_et_es1Down", "passCrossTrigger_HPS_et_es1Down"})

                // etau 2018: shifts due to es2Up (tauES), using pt_1_nominal
                .Define("passSingle_1_et_es2Up", passes_single_trigger, {"pass_et_HLT_Ele32", "filter_et_Ele32", "match1", "pt_1_nominal", "et_single_elept_thres", "pt_2_es2Up", "et_single_taupt_thres"})
                .Define("passSingle_2_et_es2Up", passes_single_trigger, {"pass_et_HLT_Ele35", "filter_et_Ele35", "match1", "pt_1_nominal", "et_single_elept_thres", "pt_2_es2Up", "et_single_taupt_thres"})
                .Define("passSingleTrigger_et_es2Up", [](int single1, int single2) { return (int) (single1 || single2); }, {"passSingle_1_et_es2Up", "passSingle_2_et_es2Up"})
                .Define("passCrossTrigger_nonHPS_et_es2Up", passes_cross_trigger, {"isEmbedded", "pass_et_HLT_Ele24Tau30",  "filter_et_Ele24Tau30", "match1", "match2",
                                                                                   "filterBits1", "filterBits2",
                                                                                   "pt_1_nominal", "et_cross_elept_thres", "et_single_elept_thres",
                                                                                   "pt_2_es2Up", "et_cross_taupt_thres",
                                                                                   "eta_2", "et_cross_eta_thres"})
                .Define("passCrossTrigger_HPS_et_es2Up", passes_cross_trigger, {"isEmbedded", "pass_et_HLT_Ele24HPSTau30",  "filter_et_Ele24HPSTau30", "match1", "match2",
                                                                                "filterBits1", "filterBits2",
                                                                                "pt_1_nominal", "et_cross_elept_thres", "et_single_elept_thres",
                                                                                "pt_2_es2Up", "et_cross_taupt_thres",
                                                                                "eta_2", "et_cross_eta_thres"})
                 .Define("passCrossTrigger_et_es2Up", [](unsigned int run, int isData, int cross, int crossHPS) {
                    if (isData && run<317509)                // Data early runs only: cone-based trigger
                        return (cross);
                    else
                        return (crossHPS);
                    }, {"run", "isData", "passCrossTrigger_nonHPS_et_es2Up", "passCrossTrigger_HPS_et_es2Up"})


                // etau 2018: shifts due to es2Down (tauES), using pt_1_nominal
                .Define("passSingle_1_et_es2Down", passes_single_trigger, {"pass_et_HLT_Ele32", "filter_et_Ele32", "match1", "pt_1_nominal", "et_single_elept_thres", "pt_2_es2Down", "et_single_taupt_thres"})
                .Define("passSingle_2_et_es2Down", passes_single_trigger, {"pass_et_HLT_Ele35", "filter_et_Ele35", "match1", "pt_1_nominal", "et_single_elept_thres", "pt_2_es2Down", "et_single_taupt_thres"})
                .Define("passSingleTrigger_et_es2Down", [](int single1, int single2) { return (int) (single1 || single2); }, {"passSingle_1_et_es2Down", "passSingle_2_et_es2Down"})
                .Define("passCrossTrigger_nonHPS_et_es2Down", passes_cross_trigger, {"isEmbedded", "pass_et_HLT_Ele24Tau30",  "filter_et_Ele24Tau30", "match1", "match2",
                                                                                     "filterBits1", "filterBits2",
                                                                                     "pt_1_nominal", "et_cross_elept_thres", "et_single_elept_thres",
                                                                                     "pt_2_es2Down", "et_cross_taupt_thres",
                                                                                     "eta_2", "et_cross_eta_thres"})
                .Define("passCrossTrigger_HPS_et_es2Down", passes_cross_trigger, {"isEmbedded", "pass_et_HLT_Ele24HPSTau30",  "filter_et_Ele24HPSTau30", "match1", "match2",
                                                                                  "filterBits1", "filterBits2",
                                                                                  "pt_1_nominal", "et_cross_elept_thres", "et_single_elept_thres",
                                                                                  "pt_2_es2Down", "et_cross_taupt_thres",
                                                                                  "eta_2", "et_cross_eta_thres"})
                 .Define("passCrossTrigger_et_es2Down", [](unsigned int run, int isData, int cross, int crossHPS) {
                    if (isData && run<317509)                // Data early runs only: cone-based trigger
                        return (cross);
                    else
                        return (crossHPS);
                    }, {"run", "isData", "passCrossTrigger_nonHPS_et_es2Down", "passCrossTrigger_HPS_et_es2Down"})


                // emu 2018: shifts due to es1Up, using pt_2_nominal
                .Define("passCross_leadingMuon_em_es1Up", passes_cross_trigger_pt_min_only, {"isEmbedded", "pass_em_HLT_Mu23Ele12", "filter_em_Mu23Ele12", "match1", "match2",
                                                                                       "pt_1_es1Up",   "em_crossLeadingMuon_elept_thres",
                                                                                       "pt_2_nominal", "em_crossLeadingMuon_mupt_thres"})
                .Define("passCross_leadingEle_em_es1Up",  passes_cross_trigger_pt_min_only, {"isEmbedded", "pass_em_HLT_Mu8Ele23", "filter_em_Mu8Ele23", "match1", "match2",
                                                                                       "pt_1_es1Up",   "em_crossLeadingEle_elept_thres",
                                                                                       "pt_2_nominal", "em_crossLeadingEle_mupt_thres"})
                .Define("passCrossTrigger_em_es1Up", [](int cross1, int cross2) { return (int) (cross1 || cross2); }, {"passCross_leadingMuon_em_es1Up", "passCross_leadingEle_em_es1Up"})

                // emu 2018: shifts due to es1Down, using pt_2_nominal
                .Define("passCross_leadingMuon_em_es1Down", passes_cross_trigger_pt_min_only, {"isEmbedded", "pass_em_HLT_Mu23Ele12", "filter_em_Mu23Ele12", "match1", "match2",
                                                                                       "pt_1_es1Down",   "em_crossLeadingMuon_elept_thres",
                                                                                       "pt_2_nominal", "em_crossLeadingMuon_mupt_thres"})
                .Define("passCross_leadingEle_em_es1Down",  passes_cross_trigger_pt_min_only, {"isEmbedded", "pass_em_HLT_Mu8Ele23", "filter_em_Mu8Ele23", "match1", "match2",
                                                                                       "pt_1_es1Down",   "em_crossLeadingEle_elept_thres",
                                                                                       "pt_2_nominal", "em_crossLeadingEle_mupt_thres"})
                .Define("passCrossTrigger_em_es1Down", [](int cross1, int cross2) { return (int) (cross1 || cross2); }, {"passCross_leadingMuon_em_es1Down", "passCross_leadingEle_em_es1Down"})

                 // emu 2018: shifts due to es2Up, using pt_1_nominal
                .Define("passCross_leadingMuon_em_es2Up", passes_cross_trigger_pt_min_only, {"isEmbedded", "pass_em_HLT_Mu23Ele12", "filter_em_Mu23Ele12", "match1", "match2",
                                                                                       "pt_1_nominal",   "em_crossLeadingMuon_elept_thres",
                                                                                       "pt_2_es2Up", "em_crossLeadingMuon_mupt_thres"})
                .Define("passCross_leadingEle_em_es2Up",  passes_cross_trigger_pt_min_only, {"isEmbedded", "pass_em_HLT_Mu8Ele23", "filter_em_Mu8Ele23", "match1", "match2",
                                                                                       "pt_1_nominal",   "em_crossLeadingEle_elept_thres",
                                                                                       "pt_2_es2Up", "em_crossLeadingEle_mupt_thres"})
                .Define("passCrossTrigger_em_es2Up", [](int cross1, int cross2) { return (int) (cross1 || cross2); }, {"passCross_leadingMuon_em_es2Up", "passCross_leadingEle_em_es2Up"})

                // emu 2018: shifts due to es2Down, using pt_1_nominal
                .Define("passCross_leadingMuon_em_es2Down", passes_cross_trigger_pt_min_only, {"isEmbedded", "pass_em_HLT_Mu23Ele12", "filter_em_Mu23Ele12", "match1", "match2",
                                                                                       "pt_1_nominal",   "em_crossLeadingMuon_elept_thres",
                                                                                       "pt_2_es2Down", "em_crossLeadingMuon_mupt_thres"})
                .Define("passCross_leadingEle_em_es2Down",  passes_cross_trigger_pt_min_only, {"isEmbedded", "pass_em_HLT_Mu8Ele23", "filter_em_Mu8Ele23", "match1", "match2",
                                                                                       "pt_1_nominal",   "em_crossLeadingEle_elept_thres",
                                                                                       "pt_2_es2Down", "em_crossLeadingEle_mupt_thres"})
                .Define("passCrossTrigger_em_es2Down", [](int cross1, int cross2) { return (int) (cross1 || cross2); }, {"passCross_leadingMuon_em_es2Down", "passCross_leadingEle_em_es2Down"});
        }
        else {
            return df2;
        }
    }
    else { // not 2018
        return df;
    }

}

// To-do: other years: et: https://github.com/hftsoi/aabbtt_finalselection/blob/master/selection_et_allyears.cc#L719-L732
// mt: https://github.com/hftsoi/aabbtt_finalselection/blob/master/selection_mt_allyears.cc#L741-L750
// et: https://github.com/hftsoi/aabbtt_finalselection/blob/cefb486856b4c817d20c6b4c49d17eaef115f78e/selection_em_allyears.cc#L716-L720
/**********************************************************/

/*
 * 2017: Goal: define passSingleTrigger_{mt, et}, passCrossTrigger_{mt, et, em}
 */
template <typename T>
auto GetTauTauTriggers_2017(T &df, LUNA::sampleConfig_t &sConfig) {

    if (sConfig.year() == 2017) {
        // Mutau 2017 nominal
        auto df2 = df.Define("mt_single_mupt_thres",  [] { return 25.0f; })
                 .Define("mt_single_taupt_thres", [] { return 20.0f; })
                 .Define("mt_cross_mupt_thres",  [] { return 21.0f; })
                 .Define("mt_cross_taupt_thres", [] { return 35.0f; })
                 .Define("mt_cross_eta_thres", [] { return 2.1f; })

                 .Define("passSingleTrigger_mt_nominal", passes_single_trigger, {"pass_mt_HLT_IsoMu27", "filter_mt_Mu27", "match1", "pt_1_nominal", "mt_single_mupt_thres", "pt_2_nominal", "mt_single_taupt_thres"})
                 .Define("passCrossTrigger_mt_nominal", passes_cross_trigger, {"isEmbedded", "pass_mt_HLT_Mu20Tau27", "filter_mt_Mu20Tau27", "match1", "match2",
                                                    "filterBits1", "filterBits2",
                                                    "pt_1_nominal", "mt_cross_mupt_thres", "mt_single_mupt_thres",
                                                    "pt_2_nominal", "mt_cross_taupt_thres",
                                                    "eta_2",  "mt_cross_eta_thres"})
        // Etau 2017 nominal
                .Define("et_single_elept_thres",  [] { return (float) 33.0; })
                .Define("et_single_taupt_thres", [] { return (float) 20.0; })
                .Define("et_cross_elept_thres",  [] { return (float) 25.0; })
                .Define("et_cross_taupt_thres", [] { return (float) 35.0; })
                .Define("et_cross_eta_thres",  [] { return (float) 2.1; })

                .Define("passSingle_1_et_nominal", passes_single_trigger, {"pass_et_HLT_Ele32", "filter_et_Ele32", "match1", "pt_1_nominal", "et_single_elept_thres", "pt_2_nominal", "et_single_taupt_thres"})
                .Define("passSingle_2_et_nominal", passes_single_trigger, {"pass_et_HLT_Ele35", "filter_et_Ele35", "match1", "pt_1_nominal", "et_single_elept_thres", "pt_2_nominal", "et_single_taupt_thres"})
                .Define("passSingleTrigger_et_nominal", [](int single1, int single2) { return (int) (single1 || single2); }, {"passSingle_1_et_nominal", "passSingle_2_et_nominal"})
                .Define("passCrossTrigger_et_nominal", passes_cross_trigger, {"isEmbedded", "pass_et_HLT_Ele24Tau30",  "filter_et_Ele24Tau30", "match1", "match2",
                                                                        "filterBits1", "filterBits2",
                                                                        "pt_1_nominal", "et_cross_elept_thres", "et_single_elept_thres",
                                                                        "pt_2_nominal", "et_cross_taupt_thres",
                                                                        "eta_2", "et_cross_eta_thres"})
        // Emu 2017 nominal
                .Define("em_crossLeadingMuon_elept_thres", [] { return (float) 13.0; })  // for Mu23Ele12
                .Define("em_crossLeadingMuon_mupt_thres",  [] { return (float) 24; })
                .Define("em_crossLeadingEle_elept_thres", [] { return (float) 24; })  // for Mu8Ele23
                .Define("em_crossLeadingEle_mupt_thres",  [] { return (float) 13; })
                .Define("passCross_leadingMuon_em_nominal", passes_cross_trigger_pt_min_only, {"isEmbedded", "pass_em_HLT_Mu23Ele12", "filter_em_Mu23Ele12", "match1", "match2",
                                                                                       "pt_1_nominal", "em_crossLeadingMuon_elept_thres",
                                                                                       "pt_2_nominal", "em_crossLeadingMuon_mupt_thres"})
                .Define("passCross_leadingEle_em_nominal",  passes_cross_trigger_pt_min_only, {"isEmbedded", "pass_em_HLT_Mu8Ele23", "filter_em_Mu8Ele23", "match1", "match2",
                                                                                       "pt_1_nominal", "em_crossLeadingEle_elept_thres",
                                                                                       "pt_2_nominal", "em_crossLeadingEle_mupt_thres"})
                .Define("passCrossTrigger_em_nominal", [](int cross1, int cross2) { return (int) (cross1 || cross2); }, {"passCross_leadingMuon_em_nominal", "passCross_leadingEle_em_nominal"});

    if (sConfig.doShifts()) {
                // mutau 2017: es1Up (shifts due to muon ES up/down): repeat this entire block for es1Up/Down, where pt_2 is nominal
        return df2.Define("passSingleTrigger_mt_es1Up", passes_single_trigger, {"pass_mt_HLT_IsoMu27", "filter_mt_Mu27", "match1", "pt_1_es1Up", "mt_single_mupt_thres", "pt_2_nominal", "mt_single_taupt_thres"})
                 .Define("passCrossTrigger_mt_es1Up", passes_cross_trigger, {"isEmbedded", "pass_mt_HLT_Mu20Tau27", "filter_mt_Mu20Tau27", "match1", "match2",
                                                    "filterBits1", "filterBits2",
                                                    "pt_1_es1Up", "mt_cross_mupt_thres", "mt_single_mupt_thres",
                                                    "pt_2_nominal", "mt_cross_taupt_thres",
                                                    "eta_2",  "mt_cross_eta_thres"})

                // mutau 2017: es1Down
                 .Define("passSingleTrigger_mt_es1Down", passes_single_trigger, {"pass_mt_HLT_IsoMu27", "filter_mt_Mu27", "match1", "pt_1_es1Down", "mt_single_mupt_thres", "pt_2_nominal", "mt_single_taupt_thres"})
                 .Define("passCrossTrigger_mt_es1Down", passes_cross_trigger, {"isEmbedded", "pass_mt_HLT_Mu20Tau27", "filter_mt_Mu20Tau27", "match1", "match2",
                                                    "filterBits1", "filterBits2",
                                                    "pt_1_es1Down", "mt_cross_mupt_thres", "mt_single_mupt_thres",
                                                    "pt_2_nominal", "mt_cross_taupt_thres",
                                                    "eta_2",  "mt_cross_eta_thres"})
                // mutau 2017: es2Up, using pt_1_nominal
                 .Define("passSingleTrigger_mt_es2Up", passes_single_trigger, {"pass_mt_HLT_IsoMu27", "filter_mt_Mu27", "match1", "pt_1_nominal", "mt_single_mupt_thres", "pt_2_es2Up", "mt_single_taupt_thres"})
                 .Define("passCrossTrigger_mt_es2Up", passes_cross_trigger, {"isEmbedded", "pass_mt_HLT_Mu20Tau27", "filter_mt_Mu20Tau27", "match1", "match2",
                                                                                    "filterBits1", "filterBits2",
                                                                                    "pt_1_nominal", "mt_cross_mupt_thres", "mt_single_mupt_thres",
                                                                                    "pt_2_es2Up", "mt_cross_taupt_thres",
                                                                                    "eta_2",  "mt_cross_eta_thres"})
                // mutau 2017: es2Down, using pt_1_nominal
                 .Define("passSingleTrigger_mt_es2Down", passes_single_trigger, {"pass_mt_HLT_IsoMu27", "filter_mt_Mu27", "match1", "pt_1_nominal", "mt_single_mupt_thres", "pt_2_es2Down", "mt_single_taupt_thres"})
                 .Define("passCrossTrigger_mt_es2Down", passes_cross_trigger, {"isEmbedded", "pass_mt_HLT_Mu20Tau27", "filter_mt_Mu20Tau27", "match1", "match2",
                                                                                      "filterBits1", "filterBits2",
                                                                                      "pt_1_nominal", "mt_cross_mupt_thres", "mt_single_mupt_thres",
                                                                                      "pt_2_es2Down", "mt_cross_taupt_thres",
                                                                                      "eta_2",  "mt_cross_eta_thres"})
                // etau 2017: shifts due to es1Up (electron ES), using pt_2_nominal
                .Define("passSingle_1_et_es1Up", passes_single_trigger, {"pass_et_HLT_Ele32", "filter_et_Ele32", "match1", "pt_1_es1Up", "et_single_elept_thres", "pt_2_nominal", "et_single_taupt_thres"})
                .Define("passSingle_2_et_es1Up", passes_single_trigger, {"pass_et_HLT_Ele35", "filter_et_Ele35", "match1", "pt_1_es1Up", "et_single_elept_thres", "pt_2_nominal", "et_single_taupt_thres"})
                .Define("passSingleTrigger_et_es1Up", [](int single1, int single2) { return (int) (single1 || single2); }, {"passSingle_1_et_es1Up", "passSingle_2_et_es1Up"})
                .Define("passCrossTrigger_et_es1Up", passes_cross_trigger, {"isEmbedded", "pass_et_HLT_Ele24Tau30",  "filter_et_Ele24Tau30", "match1", "match2",
                                                                            "filterBits1", "filterBits2",
                                                                            "pt_1_es1Up", "et_cross_elept_thres", "et_single_elept_thres",
                                                                            "pt_2_nominal", "et_cross_taupt_thres",
                                                                            "eta_2", "et_cross_eta_thres"})

                // etau 2017: shifts due to es1Down (electronES), using pt_2_nominal
                .Define("passSingle_1_et_es1Down", passes_single_trigger, {"pass_et_HLT_Ele32", "filter_et_Ele32", "match1", "pt_1_es1Down", "et_single_elept_thres", "pt_2_nominal", "et_single_taupt_thres"})
                .Define("passSingle_2_et_es1Down", passes_single_trigger, {"pass_et_HLT_Ele35", "filter_et_Ele35", "match1", "pt_1_es1Down", "et_single_elept_thres", "pt_2_nominal", "et_single_taupt_thres"})
                .Define("passSingleTrigger_et_es1Down", [](int single1, int single2) { return (int) (single1 || single2); }, {"passSingle_1_et_es1Down", "passSingle_2_et_es1Down"})
                .Define("passCrossTrigger_et_es1Down", passes_cross_trigger, {"isEmbedded", "pass_et_HLT_Ele24Tau30",  "filter_et_Ele24Tau30", "match1", "match2",
                                                                              "filterBits1", "filterBits2",
                                                                              "pt_1_es1Down", "et_cross_elept_thres", "et_single_elept_thres",
                                                                              "pt_2_nominal", "et_cross_taupt_thres",
                                                                              "eta_2", "et_cross_eta_thres"})
                // etau 2017: shifts due to es2Up (tauES), using pt_1_nominal
                .Define("passSingle_1_et_es2Up", passes_single_trigger, {"pass_et_HLT_Ele32", "filter_et_Ele32", "match1", "pt_1_nominal", "et_single_elept_thres", "pt_2_es2Up", "et_single_taupt_thres"})
                .Define("passSingle_2_et_es2Up", passes_single_trigger, {"pass_et_HLT_Ele35", "filter_et_Ele35", "match1", "pt_1_nominal", "et_single_elept_thres", "pt_2_es2Up", "et_single_taupt_thres"})
                .Define("passSingleTrigger_et_es2Up", [](int single1, int single2) { return (int) (single1 || single2); }, {"passSingle_1_et_es2Up", "passSingle_2_et_es2Up"})
                .Define("passCrossTrigger_et_es2Up", passes_cross_trigger, {"isEmbedded", "pass_et_HLT_Ele24Tau30",  "filter_et_Ele24Tau30", "match1", "match2",
                                                                            "filterBits1", "filterBits2",
                                                                            "pt_1_nominal", "et_cross_elept_thres", "et_single_elept_thres",
                                                                            "pt_2_es2Up", "et_cross_taupt_thres",
                                                                            "eta_2", "et_cross_eta_thres"})
                // etau 2018: shifts due to es2Down (tauES), using pt_1_nominal
                .Define("passSingle_1_et_es2Down", passes_single_trigger, {"pass_et_HLT_Ele32", "filter_et_Ele32", "match1", "pt_1_nominal", "et_single_elept_thres", "pt_2_es2Down", "et_single_taupt_thres"})
                .Define("passSingle_2_et_es2Down", passes_single_trigger, {"pass_et_HLT_Ele35", "filter_et_Ele35", "match1", "pt_1_nominal", "et_single_elept_thres", "pt_2_es2Down", "et_single_taupt_thres"})
                .Define("passSingleTrigger_et_es2Down", [](int single1, int single2) { return (int) (single1 || single2); }, {"passSingle_1_et_es2Down", "passSingle_2_et_es2Down"})
                .Define("passCrossTrigger_et_es2Down", passes_cross_trigger, {"isEmbedded", "pass_et_HLT_Ele24Tau30",  "filter_et_Ele24Tau30", "match1", "match2",
                                                                              "filterBits1", "filterBits2",
                                                                              "pt_1_nominal", "et_cross_elept_thres", "et_single_elept_thres",
                                                                              "pt_2_es2Down", "et_cross_taupt_thres",
                                                                              "eta_2", "et_cross_eta_thres"})

                // emu 2017: shifts due to es1Up, using pt_2_nominal
                .Define("passCross_leadingMuon_em_es1Up", passes_cross_trigger_pt_min_only, {"isEmbedded", "pass_em_HLT_Mu23Ele12", "filter_em_Mu23Ele12", "match1", "match2",
                                                                                       "pt_1_es1Up",   "em_crossLeadingMuon_elept_thres",
                                                                                       "pt_2_nominal", "em_crossLeadingMuon_mupt_thres"})
                .Define("passCross_leadingEle_em_es1Up",  passes_cross_trigger_pt_min_only, {"isEmbedded", "pass_em_HLT_Mu8Ele23", "filter_em_Mu8Ele23", "match1", "match2",
                                                                                       "pt_1_es1Up",   "em_crossLeadingEle_elept_thres",
                                                                                       "pt_2_nominal", "em_crossLeadingEle_mupt_thres"})
                .Define("passCrossTrigger_em_es1Up", [](int cross1, int cross2) { return (int) (cross1 || cross2); }, {"passCross_leadingMuon_em_es1Up", "passCross_leadingEle_em_es1Up"})

                // emu 2018: shifts due to es1Down, using pt_2_nominal
                .Define("passCross_leadingMuon_em_es1Down", passes_cross_trigger_pt_min_only, {"isEmbedded", "pass_em_HLT_Mu23Ele12", "filter_em_Mu23Ele12", "match1", "match2",
                                                                                       "pt_1_es1Down",   "em_crossLeadingMuon_elept_thres",
                                                                                       "pt_2_nominal", "em_crossLeadingMuon_mupt_thres"})
                .Define("passCross_leadingEle_em_es1Down",  passes_cross_trigger_pt_min_only, {"isEmbedded", "pass_em_HLT_Mu8Ele23", "filter_em_Mu8Ele23", "match1", "match2",
                                                                                       "pt_1_es1Down",   "em_crossLeadingEle_elept_thres",
                                                                                       "pt_2_nominal", "em_crossLeadingEle_mupt_thres"})
                .Define("passCrossTrigger_em_es1Down", [](int cross1, int cross2) { return (int) (cross1 || cross2); }, {"passCross_leadingMuon_em_es1Down", "passCross_leadingEle_em_es1Down"})

                 // emu 2018: shifts due to es2Up, using pt_1_nominal
                .Define("passCross_leadingMuon_em_es2Up", passes_cross_trigger_pt_min_only, {"isEmbedded", "pass_em_HLT_Mu23Ele12", "filter_em_Mu23Ele12", "match1", "match2",
                                                                                       "pt_1_nominal",   "em_crossLeadingMuon_elept_thres",
                                                                                       "pt_2_es2Up", "em_crossLeadingMuon_mupt_thres"})
                .Define("passCross_leadingEle_em_es2Up",  passes_cross_trigger_pt_min_only, {"isEmbedded", "pass_em_HLT_Mu8Ele23", "filter_em_Mu8Ele23", "match1", "match2",
                                                                                       "pt_1_nominal",   "em_crossLeadingEle_elept_thres",
                                                                                       "pt_2_es2Up", "em_crossLeadingEle_mupt_thres"})
                .Define("passCrossTrigger_em_es2Up", [](int cross1, int cross2) { return (int) (cross1 || cross2); }, {"passCross_leadingMuon_em_es2Up", "passCross_leadingEle_em_es2Up"})

                // emu 2018: shifts due to es2Down, using pt_1_nominal
                .Define("passCross_leadingMuon_em_es2Down", passes_cross_trigger_pt_min_only, {"isEmbedded", "pass_em_HLT_Mu23Ele12", "filter_em_Mu23Ele12", "match1", "match2",
                                                                                       "pt_1_nominal",   "em_crossLeadingMuon_elept_thres",
                                                                                       "pt_2_es2Down", "em_crossLeadingMuon_mupt_thres"})
                .Define("passCross_leadingEle_em_es2Down",  passes_cross_trigger_pt_min_only, {"isEmbedded", "pass_em_HLT_Mu8Ele23", "filter_em_Mu8Ele23", "match1", "match2",
                                                                                       "pt_1_nominal",   "em_crossLeadingEle_elept_thres",
                                                                                       "pt_2_es2Down", "em_crossLeadingEle_mupt_thres"})
                .Define("passCrossTrigger_em_es2Down", [](int cross1, int cross2) { return (int) (cross1 || cross2); }, {"passCross_leadingMuon_em_es2Down", "passCross_leadingEle_em_es2Down"});
        }
        else {
            return df2;
        }
    }
    else { // not 2017
        return df;
    }

}

/**********************************************************/



/*
 * 2016: Define passSingleTrigger_{mt, et}, passCrossTrigger_{mt, et, em}
 */
template <typename T>
auto GetTauTauTriggers_2016(T &df, LUNA::sampleConfig_t &sConfig) {

    if (sConfig.year() == 2016) {
        // Mutau 2016 nominal: note that for the cross trigger we require the muon to be strictly within 20 < pT < 23 GeV
        // so that the selection is orthogonal with the single muon trigger, simplifying the application of the scale factors
        auto df2 = df.Define("mt_single_mupt_thres",  [] { return 23.0f; })
                 .Define("mt_single_taupt_thres", [] { return 20.0f; })
                 .Define("mt_single_mueta_thres", [] { return 2.1f; })
                 .Define("mt_single_taueta_thres", [] { return 2.3f; })
                 .Define("mt_cross_mupt_thres",  [] { return 20.0f; })
                 .Define("mt_cross_mupt_max", [] { return 23.0f; })
                 .Define("mt_cross_taupt_thres", [] { return 25.0f; })
                 .Define("mt_cross_mueta_thres", [] { return 2.1f; })
                 .Define("mt_cross_taueta_thres", [] { return 2.1f; })

                 .Define("passSingleTrigger_mt_nominal", passes_single_trigger_2016, {"pass_mt_HLT_IsoMu22", "pass_mt_HLT_IsoMu22_eta2p1", "filter_mt_IsoMu22",
                                                                                      "pass_mt_HLT_IsoTkMu22", "pass_mt_HLT_IsoTkMu22_eta2p1", "filter_mt_IsoTkMu22",
                                                                                      "match1",
                                                                                      "pt_1_nominal", "mt_single_mupt_thres",
                                                                                      "pt_2_nominal", "mt_single_taupt_thres",
                                                                                      "eta_1", "mt_single_mueta_thres",
                                                                                      "eta_2", "mt_single_taueta_thres"})
                 .Define("passCrossTrigger_mt_nominal", passes_cross_trigger_2016, {"pass_mt_HLT_IsoMu19_eta2p1_LooseIsoPFTau20", "pass_mt_HLT_IsoMu19_eta2p1_LooseIsoPFTau20_SingleL1",
                                                                                    "filter_mt_IsoMu19_eta2p1_LooseIsoPFTau20", "match1", "match2",
                                                                                    "pt_1_nominal", "mt_cross_mupt_thres", "mt_cross_mupt_max",
                                                                                    "pt_2_nominal", "mt_cross_taupt_thres",
                                                                                    "eta_1", "mt_cross_mueta_thres", "eta_2", "mt_cross_taueta_thres"})
        // Etau 2016 nominal: no cross-trigger
                .Define("et_single_elept_thres",  [] { return 26.0f; })
                .Define("et_single_taupt_thres", [] { return 20.0f; })
                .Define("et_cross_elept_thres", [] { return 26.0f; })  // dummy value for downstream function, since there is no etau cross trigger in 2016
                .Define("passSingleTrigger_et_nominal",  passes_single_trigger, {"pass_et_HLT_Ele25_eta2p1_WPTight_Gsf", "filter_et_Ele25", "match1", "pt_1_nominal", "et_single_elept_thres", "pt_2_nominal", "et_single_taupt_thres"})
                .Define("passCrossTrigger_et_nominal", [] { return 0; })

        // Emu 2016 nominal: two cross-triggers, same as other two years
                .Define("em_crossLeadingMuon_elept_thres", [] { return 13.0f; })  // for Mu23Ele12
                .Define("em_crossLeadingMuon_mupt_thres",  [] { return 24.0f; })
                .Define("em_crossLeadingEle_elept_thres", [] { return 24.0f; })  // for Mu8Ele23
                .Define("em_crossLeadingEle_mupt_thres",  [] { return 13.0f; })
                .Define("passCross_leadingMuon_em_nominal", passes_cross_trigger_pt_min_only, {"isEmbedded", "pass_em_HLT_Mu23Ele12", "filter_em_Mu23Ele12", "match1", "match2",
                                                                                       "pt_1_nominal", "em_crossLeadingMuon_elept_thres",
                                                                                       "pt_2_nominal", "em_crossLeadingMuon_mupt_thres"})
                .Define("passCross_leadingEle_em_nominal",  passes_cross_trigger_pt_min_only, {"isEmbedded", "pass_em_HLT_Mu8Ele23", "filter_em_Mu8Ele23", "match1", "match2",
                                                                                       "pt_1_nominal", "em_crossLeadingEle_elept_thres",
                                                                                       "pt_2_nominal", "em_crossLeadingEle_mupt_thres"})
                .Define("passCrossTrigger_em_nominal", [](int cross1, int cross2) { return (int) (cross1 || cross2); }, {"passCross_leadingMuon_em_nominal", "passCross_leadingEle_em_nominal"});

    if (sConfig.doShifts()) {
                // mutau 2016: es1Up (shifts due to muon ES up/down): repeat this entire block for es1Up/Down, where pt_2 is nominal
        return df2.Define("passSingleTrigger_mt_es1Up", passes_single_trigger_2016, {"pass_mt_HLT_IsoMu22", "pass_mt_HLT_IsoMu22_eta2p1", "filter_mt_IsoMu22",
                                                                                      "pass_mt_HLT_IsoTkMu22", "pass_mt_HLT_IsoTkMu22_eta2p1", "filter_mt_IsoTkMu22",
                                                                                      "match1",
                                                                                      "pt_1_es1Up", "mt_single_mupt_thres",
                                                                                      "pt_2_nominal", "mt_single_taupt_thres",
                                                                                      "eta_1", "mt_single_mueta_thres",
                                                                                      "eta_2", "mt_single_taueta_thres"})
                 .Define("passCrossTrigger_mt_es1Up", passes_cross_trigger_2016, {"pass_mt_HLT_IsoMu19_eta2p1_LooseIsoPFTau20", "pass_mt_HLT_IsoMu19_eta2p1_LooseIsoPFTau20_SingleL1",
                                                                                    "filter_mt_IsoMu19_eta2p1_LooseIsoPFTau20", "match1", "match2",
                                                                                    "pt_1_es1Up", "mt_cross_mupt_thres", "mt_cross_mupt_max",
                                                                                    "pt_2_nominal", "mt_cross_taupt_thres",
                                                                                    "eta_1", "mt_cross_mueta_thres", "eta_2", "mt_cross_taueta_thres"})

                // mutau 2016: note use of pt1_es1Down, keep pt_2 as nominal
                 .Define("passSingleTrigger_mt_es1Down", passes_single_trigger_2016, {"pass_mt_HLT_IsoMu22", "pass_mt_HLT_IsoMu22_eta2p1", "filter_mt_IsoMu22",
                                                                                      "pass_mt_HLT_IsoTkMu22", "pass_mt_HLT_IsoTkMu22_eta2p1", "filter_mt_IsoTkMu22",
                                                                                      "match1",
                                                                                      "pt_1_es1Down", "mt_single_mupt_thres",
                                                                                      "pt_2_nominal", "mt_single_taupt_thres",
                                                                                      "eta_1", "mt_single_mueta_thres",
                                                                                      "eta_2", "mt_single_taueta_thres"})
                 .Define("passCrossTrigger_mt_es1Down", passes_cross_trigger_2016, {"pass_mt_HLT_IsoMu19_eta2p1_LooseIsoPFTau20", "pass_mt_HLT_IsoMu19_eta2p1_LooseIsoPFTau20_SingleL1",
                                                                                    "filter_mt_IsoMu19_eta2p1_LooseIsoPFTau20", "match1", "match2",
                                                                                    "pt_1_es1Down", "mt_cross_mupt_thres", "mt_cross_mupt_max",
                                                                                    "pt_2_nominal", "mt_cross_taupt_thres",
                                                                                    "eta_1", "mt_cross_mueta_thres", "eta_2", "mt_cross_taueta_thres"})
                // mutau 2016: note use of pt2_es2Up, while using pt_1_nominal
                 .Define("passSingleTrigger_mt_es2Up", passes_single_trigger_2016, {"pass_mt_HLT_IsoMu22", "pass_mt_HLT_IsoMu22_eta2p1", "filter_mt_IsoMu22",
                                                                                      "pass_mt_HLT_IsoTkMu22", "pass_mt_HLT_IsoTkMu22_eta2p1", "filter_mt_IsoTkMu22",
                                                                                      "match1",
                                                                                      "pt_1_nominal", "mt_single_mupt_thres",
                                                                                      "pt_2_es2Up", "mt_single_taupt_thres",
                                                                                      "eta_1", "mt_single_mueta_thres",
                                                                                      "eta_2", "mt_single_taueta_thres"})
                 .Define("passCrossTrigger_mt_es2Up", passes_cross_trigger_2016, {"pass_mt_HLT_IsoMu19_eta2p1_LooseIsoPFTau20", "pass_mt_HLT_IsoMu19_eta2p1_LooseIsoPFTau20_SingleL1",
                                                                                    "filter_mt_IsoMu19_eta2p1_LooseIsoPFTau20", "match1", "match2",
                                                                                    "pt_1_nominal", "mt_cross_mupt_thres", "mt_cross_mupt_max",
                                                                                    "pt_2_es2Up", "mt_cross_taupt_thres",
                                                                                    "eta_1", "mt_cross_mueta_thres", "eta_2", "mt_cross_taueta_thres"})
                // mutau 2016: use pt2_es2Down, while using pt_1_nominal
                 .Define("passSingleTrigger_mt_es2Down", passes_single_trigger_2016, {"pass_mt_HLT_IsoMu22", "pass_mt_HLT_IsoMu22_eta2p1", "filter_mt_IsoMu22",
                                                                                      "pass_mt_HLT_IsoTkMu22", "pass_mt_HLT_IsoTkMu22_eta2p1", "filter_mt_IsoTkMu22",
                                                                                      "match1",
                                                                                      "pt_1_nominal", "mt_single_mupt_thres",
                                                                                      "pt_2_es2Down", "mt_single_taupt_thres",
                                                                                      "eta_1", "mt_single_mueta_thres",
                                                                                      "eta_2", "mt_single_taueta_thres"})
                 .Define("passCrossTrigger_mt_es2Down", passes_cross_trigger_2016, {"pass_mt_HLT_IsoMu19_eta2p1_LooseIsoPFTau20", "pass_mt_HLT_IsoMu19_eta2p1_LooseIsoPFTau20_SingleL1",
                                                                                    "filter_mt_IsoMu19_eta2p1_LooseIsoPFTau20", "match1", "match2",
                                                                                    "pt_1_nominal", "mt_cross_mupt_thres", "mt_cross_mupt_max",
                                                                                    "pt_2_es2Up", "mt_cross_taupt_thres",
                                                                                    "eta_1", "mt_cross_mueta_thres", "eta_2", "mt_cross_taueta_thres"})
                // etau 2016: shifts due to es1Up (electron ES), using pt_2_nominal
                .Define("passSingleTrigger_et_es1Up",  passes_single_trigger, {"pass_et_HLT_Ele25_eta2p1_WPTight_Gsf", "filter_et_Ele25", "match1",
                                                                                "pt_1_es1Up", "et_single_elept_thres",
                                                                                "pt_2_nominal", "et_single_taupt_thres"})
                // etau 2016: shifts due to es1Down (electronES), using pt_2_nominal
                .Define("passSingleTrigger_et_es1Down", passes_single_trigger, {"pass_et_HLT_Ele25_eta2p1_WPTight_Gsf", "filter_et_Ele25", "match1",
                                                                                "pt_1_es1Down", "et_single_elept_thres",
                                                                                "pt_2_nominal", "et_single_taupt_thres"})
                // etau 2016: shifts due to es2Up (tauES), using pt_1_nominal
                .Define("passSingleTrigger_et_es2Up", passes_single_trigger, {"pass_et_HLT_Ele25_eta2p1_WPTight_Gsf", "filter_et_Ele25", "match1",
                                                                                "pt_1_nominal", "et_single_elept_thres",
                                                                                "pt_2_es2Up", "et_single_taupt_thres"})
                // etau 2016: shifts due to es2Down (tauES), using pt_1_nominal
                .Define("passSingleTrigger_et_es2Down", passes_single_trigger, {"pass_et_HLT_Ele25_eta2p1_WPTight_Gsf", "filter_et_Ele25", "match1",
                                                                                "pt_1_nominal", "et_single_elept_thres",
                                                                                "pt_2_es2Down", "et_single_taupt_thres"})
                // Dummy values: no cross-trigger for etau 2016
                .Define("passCrossTrigger_et_es1Up", [] { return 0; })
                .Define("passCrossTrigger_et_es1Down", [] { return 0; })
                .Define("passCrossTrigger_et_es2Up", [] { return 0; })
                .Define("passCrossTrigger_et_es2Down", [] { return 0; })

                // emu 2016: shifts due to es1Up, using pt_2_nominal
                .Define("passCross_leadingMuon_em_es1Up", passes_cross_trigger_pt_min_only, {"isEmbedded", "pass_em_HLT_Mu23Ele12", "filter_em_Mu23Ele12", "match1", "match2",
                                                                                       "pt_1_es1Up",   "em_crossLeadingMuon_elept_thres",
                                                                                       "pt_2_nominal", "em_crossLeadingMuon_mupt_thres"})
                .Define("passCross_leadingEle_em_es1Up",  passes_cross_trigger_pt_min_only, {"isEmbedded", "pass_em_HLT_Mu8Ele23", "filter_em_Mu8Ele23", "match1", "match2",
                                                                                       "pt_1_es1Up",   "em_crossLeadingEle_elept_thres",
                                                                                       "pt_2_nominal", "em_crossLeadingEle_mupt_thres"})
                .Define("passCrossTrigger_em_es1Up", [](int cross1, int cross2) { return (int) (cross1 || cross2); }, {"passCross_leadingMuon_em_es1Up", "passCross_leadingEle_em_es1Up"})

                // emu 2016: shifts due to es1Down, using pt_2_nominal
                .Define("passCross_leadingMuon_em_es1Down", passes_cross_trigger_pt_min_only, {"isEmbedded", "pass_em_HLT_Mu23Ele12", "filter_em_Mu23Ele12", "match1", "match2",
                                                                                       "pt_1_es1Down",   "em_crossLeadingMuon_elept_thres",
                                                                                       "pt_2_nominal", "em_crossLeadingMuon_mupt_thres"})
                .Define("passCross_leadingEle_em_es1Down",  passes_cross_trigger_pt_min_only, {"isEmbedded", "pass_em_HLT_Mu8Ele23", "filter_em_Mu8Ele23", "match1", "match2",
                                                                                       "pt_1_es1Down",   "em_crossLeadingEle_elept_thres",
                                                                                       "pt_2_nominal", "em_crossLeadingEle_mupt_thres"})
                .Define("passCrossTrigger_em_es1Down", [](int cross1, int cross2) { return (int) (cross1 || cross2); }, {"passCross_leadingMuon_em_es1Down", "passCross_leadingEle_em_es1Down"})

                 // emu 2016: shifts due to es2Up, using pt_1_nominal
                .Define("passCross_leadingMuon_em_es2Up", passes_cross_trigger_pt_min_only, {"isEmbedded", "pass_em_HLT_Mu23Ele12", "filter_em_Mu23Ele12", "match1", "match2",
                                                                                       "pt_1_nominal",   "em_crossLeadingMuon_elept_thres",
                                                                                       "pt_2_es2Up", "em_crossLeadingMuon_mupt_thres"})
                .Define("passCross_leadingEle_em_es2Up",  passes_cross_trigger_pt_min_only, {"isEmbedded", "pass_em_HLT_Mu8Ele23", "filter_em_Mu8Ele23", "match1", "match2",
                                                                                       "pt_1_nominal",   "em_crossLeadingEle_elept_thres",
                                                                                       "pt_2_es2Up", "em_crossLeadingEle_mupt_thres"})
                .Define("passCrossTrigger_em_es2Up", [](int cross1, int cross2) { return (int) (cross1 || cross2); }, {"passCross_leadingMuon_em_es2Up", "passCross_leadingEle_em_es2Up"})

                // emu 2016: shifts due to es2Down, using pt_1_nominal
                .Define("passCross_leadingMuon_em_es2Down", passes_cross_trigger_pt_min_only, {"isEmbedded", "pass_em_HLT_Mu23Ele12", "filter_em_Mu23Ele12", "match1", "match2",
                                                                                       "pt_1_nominal",   "em_crossLeadingMuon_elept_thres",
                                                                                       "pt_2_es2Down", "em_crossLeadingMuon_mupt_thres"})
                .Define("passCross_leadingEle_em_es2Down",  passes_cross_trigger_pt_min_only, {"isEmbedded", "pass_em_HLT_Mu8Ele23", "filter_em_Mu8Ele23", "match1", "match2",
                                                                                       "pt_1_nominal",   "em_crossLeadingEle_elept_thres",
                                                                                       "pt_2_es2Down", "em_crossLeadingEle_mupt_thres"})
                .Define("passCrossTrigger_em_es2Down", [](int cross1, int cross2) { return (int) (cross1 || cross2); }, {"passCross_leadingMuon_em_es2Down", "passCross_leadingEle_em_es2Down"});
        }
        else {
            return df2;
        }
    }
    else { // not 2016
        return df;
    }

}

/**********************************************************/

/*
 * Define "isEmbedded" if it is not a branch for convenience in the helper functions in this file
 */
template <typename T>
auto DefineIsEmbedded(T &df, LUNA::sampleConfig_t &sConfig) {
    if (!(HasExactBranch(df, "isEmbedded"))) {
        int isEmbedded = sConfig.isEmbedded();
        return df.Define("isEmbedded", [isEmbedded] { return isEmbedded; });
    }
    else {
        return df;
    }
}

/**********************************************************/

/*
 *  Get tautau trigger decisions and their shifts.
 */

template <typename T>
auto GetApplyTauTauTriggerDecisions_experimental(T &df, LUNA::sampleConfig_t &sConfig) {

    auto df2 = DefineIsEmbedded(df, sConfig);
    auto df3 = GetTauTauTriggers_2018(df2, sConfig);
    auto df4 = GetTauTauTriggers_2017(df3, sConfig);
    auto df5 = GetTauTauTriggers_2016(df4, sConfig);
    return df5;

}


/**********************************************************/



#endif
