// reweightWJets.h

#ifndef REWEIGHT_W_JETS_H
#define REWEIGHT_W_JETS_H

#include "fileIO.h"

/*******************************************************************/

// Reweight W+Jets events; needs special handling because the re-weight
// depends on the number of events processed (at the Mini/NanoAOD level)
// in WJets.
// Based on https://twiki.cern.ch/twiki/bin/viewauth/CMS/StandardModelCrossSectionsat13TeV and
// https://github.com/isobelojalvo/PUAnalysis/blob/devel_Htt_Mar_7/ROOT/bin/EventWeightsIterativeWJets.cc#L33-L34

/*******************************************************************/

using namespace ROOT::VecOps;

// nJets: valid values are 0, 1, 2, 3, and 4
// nEntries_WJets: the number of NanoAOD events processed in WJets
// nEntries_this:  "                                    " in this particular WJets sample

float getReweightFactorWJets(int nJets, float nEntries_WJets, float nEntries_this, float luminosity) {

  float reweightFactor = 1.0;

  // Ratio of NNLO to LO cross-section for inclusive WJets
  // https://twiki.cern.ch/twiki/bin/viewauth/CMS/MCStitching#Stitching_inclusive_with_jet_AN1
  double NNLOtoLO =  61526.7/53870.0;

  // 2017 UL LO cross-sections for WJets, W1Jets, W2, W3, and W4
  std::vector<double> WJets_LOxsec{ 53870.0, 8927.0, 2809.0, 826.3, 544.3 };

  float WLo_W = nEntries_WJets / (NNLOtoLO * WJets_LOxsec[0]);
  float WLo_i = nEntries_this  / (NNLOtoLO * WJets_LOxsec[nJets]);

  if (nJets > 0) {
    reweightFactor = (luminosity / (WLo_W + WLo_i));
  }
  else if (nJets == 0) {
    reweightFactor = (luminosity / WLo_W);
  }

  // std::cout << "reweightWJets: " << ", reweightFactor: " << reweightFactor
  // 	<< "(from WLo_W, WLo_i) " << WLo_W << ", " << WLo_i
  // 	<< std::endl;

  return reweightFactor;
}


/*******************************************************************/

template <typename T>
auto GetWJetsWeight_experimental(T &df, LUNA::sampleConfig_t &sConfig, std::map<std::string, std::string> fileMap, int isLocalJob){

  if (sConfig.isWJets()) {
    std::string inclusiveSampleName = "WJetsToLNu";
    if (sConfig.era() == "2016preVFP") {
      inclusiveSampleName = "preVFP_WJetsToLNu";
    }
    else if (sConfig.era() == "2016postVFP") {
      inclusiveSampleName = "postVFP_WJetsToLNu";
    }

    float nEntries_WJets = getEventsProcessedFromMap(inclusiveSampleName, fileMap, isLocalJob);

    std::cout << ">>> reweightWJets-experimental.h: Got " << nEntries_WJets << " events in the inclusive sample" << std::endl;

    return df.Define("nEntries_WJets", [nEntries_WJets](){ return nEntries_WJets; })
             .Define("weight_WJets_nominal", getReweightFactorWJets,
                                        {"lhe_njets_DY_W_jets", "nEntries_WJets", "nEntriesProcessedTotal", "luminosity"});
  }
  else {
    return df.Define("weight_WJets_nominal", []() { return 1.0f; });
  }
}


/*******************************************************************/


#endif
