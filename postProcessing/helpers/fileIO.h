// Helper functions for file I/O (config file I/O) and also writing/reading
// from a map to access other n-tuples.

#ifndef FILE_IO_H_INCL
#define FILE_IO_H_INCL

#include <filesystem>
#include <fstream>
#include <iostream>
#include <regex>
#include <string>

#include "helperFunctions.h"
#include "sampleConfig_class.h"

#include "ROOT/RDataFrame.hxx"
#include "ROOT/RResultPtr.hxx"
#include "TChain.h"

/******************************************************************************/

/*
 * Initialize the dataframe with branches needed for systematics.
 */

template <typename T>
auto InitializeDataframe(T &df, LUNA::sampleConfig_t &sConfig) {

  int isSignal = sConfig.isSignal();
  int isMCnonHiggs = sConfig.isMCnonHiggs();

  return df.Define("channel_mt", [=]() { return Helper::mt; })
           .Define("channel_et", [=]() { return Helper::et; })
           .Define("channel_em", [=]() { return Helper::em; })
           .Define("zero", [](){ return (float) 0.0; })   // kind of weird but we need it for add_p4
           .Define("isSignal", [=]() { return isSignal; })
           .Define("isMCnonHiggs", [=]() { return isMCnonHiggs; });

}

/******************************************************************************/

// /*
//  * Check if a string ends in a substring or starts with a substring.
//  * Maybe some day we will have C++20 compatability with ROOT :(
//  */

// static bool strEndsWith(const std::string str, const std::string suffix)
// {
//   // .*dm10$ will match whether dm10 is at the end of the string
//   return std::regex_match(str, std::regex(".*" + suffix + "$"));
// }

/******************************************************************************/

int WhichYear(std::string configDir) {

  int year = 2018;

  if (configDir.find("2018") != std::string::npos) {
    year = 2018;
  }
  else if (configDir.find("2017") != std::string::npos) {
    year = 2017;
  }
  else if (configDir.find("2016") != std::string::npos) {
    year = 2016;
  }
  else {
    std::cout << "WARNING: fileIO.h: year could not be determined, defaulting to 2018" << std::endl;
  }

  return year;
}


/******************************************************************************/

/*
 * Return the contents of inFile (text file) as a string.
 */

std::string getString(std::string inFile) {

  std::string s;

  std::ifstream f(inFile);
  std::stringstream buffer;
  buffer << f.rdbuf();

  s = buffer.str();

  return s;
}

/******************************************************************************/

/*
 * In big string "subject", replace all instances of string "search" with the string "replace", without
 * creating a new copy of the string (performs the substitution in place).
 * From https://github.com/skkwan/DataCardCreator/blob/master/StatTools/interface/DataCardCreatorHThTh_2016_RDF.h#L201-L212
 */

void ReplaceStringInPlace(std::string& subject, const std::string& search,
			  const std::string& replace) {
  size_t pos = 0;
  while ((pos = subject.find(search, pos)) != std::string::npos) {
    subject.replace(pos, search.length(), replace);
    pos += replace.length();
  }
}

/******************************************************************************/

/*
 * Return true if the dataframe has a branch "branch" and false otherwise.
 * The branch name must match exactly.
 */

template <typename T>
bool HasExactBranch(T &df, std::string branch) {

  bool hasBranch = false;

  // Get df column names (branches in the NanoAOD)
  auto colNames = df.GetColumnNames();
  for (auto &&colName : colNames) {
    if (colName == branch) {
      hasBranch = true;
      break;
    }
  }

  return hasBranch;
}

/******************************************************************************/


// Make map (sample name, path/to/input file) using the input .csv file.
// ! Assumes that the sample name is the third entry in the list, and the path to the input n-tuple is the first.
//  This is used to access values
// in other histograms (enabling postprocess.cxx to execute code that derives
// from n-tuples other than the one being operated on now).

// https://stackoverflow.com/questions/30181600/reading-two-columns-in-csv-file-in-c
// https://stackoverflow.com/questions/24853914/read-2d-array-in-csv-into-a-map-c

std::map<std::string, std::string> makeMapFromCSV(std::string csvFile) {

  // e.g. (DY4JetsToLL, /eos/user/[etc.]/DY4JetsToLLSkim.root)
  std::map<std::string, std::string> map;

  std::string line;
  std::ifstream ifile(csvFile);    // set up the input file

  while (std::getline(ifile, line)) { // read the current line
    std::istringstream iss{line}; // construct a string stream from line

    // read the tokens from current line separated by comma
    std::vector<std::string> tokens; // here we store the tokens
    std::string token; // current token
    while (std::getline(iss, token, ',')) {
	    tokens.push_back(token); // add the token to the vector
    }

    // Hard-coded positions in tokens
    std::string sampleName = tokens[1];
    std::string ntuplePath = tokens[0];

    map[sampleName] = ntuplePath; // fill the map
    std::cout << "sample name:" << sampleName << ", with n-tuple path: " << ntuplePath << std::endl;
  }

  return map;

}

/******************************************************************************/

// Returns whether dataset with sampleName is available this year.

bool isSampleAvailable(std::string sampleName, std::map<std::string, std::string> map) {

  // std::cout << "isSampleAvailable: looking for " << sampleName << std::endl;

  std::map<std::string, std::string>::iterator it;
  it = map.find(sampleName);

  bool hasSample = (it != map.end());
  return hasSample;

}

/******************************************************************************/

// Return the dataframe with samplename equal to targetName in fileMap. Make sure
// that sample is available before calling this function.

auto getDataframeFromMap(std::string targetName,
			 std::map<std::string, std::string> map) {

  std::map<std::string, std::string>::iterator it;
  it = map.find(targetName);

  // std::cout << " >>> Found path to sample " << it->second << std::endl;
  ROOT::RDataFrame df("mutau_tree", it->second);
  return df;

}

/******************************************************************************/

// For a given process called targetName, return the number of processed entries as a float using histMap.

float getEventsProcessedFromMap(std::string targetName,
				std::map<std::string, std::string> map, bool isLocalJob) {

  float nEventsProcessed = 0;

  // Check if the specified sample is part of the datasets
  if (isSampleAvailable(targetName, map)) {

    // Access the map
    std::map<std::string, std::string>::iterator it;
    it = map.find(targetName);
    // std::cout << "getEventsProcessedFromMap: searching for " <<targetName
    //           << " and finding " << it->second.c_str() << std::endl;

    // Open the TH1F "hEvents" in the file (need to convert the std::string to a c_str() for ROOT).
    // TFile f(it->second.c_str()); // this works for local but not remote files
    std::string pathToFile;
    if (isLocalJob) {
      // Open file in /eos/
      pathToFile = it->second + "/info/info_" + targetName + ".root";
    }
    else {
      // The Condor job should have transferred the file to the local sandbox area.
      pathToFile = "info_"+ targetName + ".root";
    }
    std::cout << ">>> fileIO.h: If this was remote job, would have tried to get " <<  "info_"+ targetName + ".root" << std::endl;
    std::cout << ">>> fileIO.h: getEventsProcessedFromMap: opening " << pathToFile << std::endl;
    TFile *fThis = TFile::Open(pathToFile.c_str());
    TH1F *h = (TH1F*)fThis->Get("hEvents");

    // Number of NanoAOD events processed
    nEventsProcessed = (float) h->GetBinContent(1);
  }

  return nEventsProcessed;
}

/******************************************************************************/

// Return genEventSumw given the hRuns histogram created in skim.cxx, which just
// tallies genEventSumw in each ROOT file.

float getGenEventSumw(TH1F *h) {

  float genEventSumw = (float) h->GetBinContent(1);
  return genEventSumw;
}

/******************************************************************************/

// Return nEntries given the hEvents histogram created in skim.cxx.

float getNEntries(TH1F *h) {

  float getnEntries = (float) h->GetBinContent(1);
  return getnEntries;
}

/******************************************************************************/

// Get the first entry of a branch 'floatBranchName' for a specific dataset. Used in reweightWJets.h and
// reweightZJets.h. Uses the global variable fileMap (defined above).
// This format is useful when the branch of floats is identical for each entry.

float getBranchValue(std::map<std::string, std::string> fileMap, std::string processName, std::string floatBranchName) {

  if (isSampleAvailable(processName, fileMap)) {
    auto df = getDataframeFromMap(processName, fileMap);
    auto vEntries = df.Take<double>(floatBranchName);
    float entry = (float) vEntries->at(0);
    // std::cout << "fileIO.h: " << processName << " found, getBranchValue returns " << entry << std::endl;
    return entry;
  }
  else {
    // std::cout << "[WARNING]: fileIO.h: " << processName << " is not in this dataset, getNEntries returning 0" << std::endl;
    return 0;
  }

}

/******************************************************************************/

// Returns the total number of entries associated with a processStem (e.g. "DYJetsToLL")
// which may have extensions to the dataset.

float getEventsProcessedWithExtensionsFromMap(std::string name, std::map<std::string, std::string> map, bool isLocalJob) {

  // All possible dataset name extensions
  std::vector<std::string> extensions = {"", "-ext1", "-ext2", "-ext3"};

  // First strip the sample name of any extensions
  std::string basename = name;
  for (std::string ext : extensions) {
    size_t pos = basename.find(ext);
    if (pos != std::string::npos) {
      basename.erase(pos, ext.length());
    }
    // std::cout << "[!!!] found basename " << basename << std::endl;
  }

  // Next check the basename and its extensions, adding up the entries in them
  float totalEntries = 0;
  for (std::string ext : extensions) {

    std::string extname = basename + ext;
    float extEntries = getEventsProcessedFromMap(extname, map, isLocalJob);

    // std::cout << "    >>> fileIO.h: getTotalEntries: Process " + extname + " turns up " << extEntries << " entries" << std::endl;
    totalEntries += extEntries;
  }

  // std::cout << ">>> fileIO.h: getTotalEntries: found " << totalEntries << " for all " << name << std::endl;
  return totalEntries;

}

/******************************************************************************/

/*
 * Helper function for applying a vector of Defines to a dataframe (if df is a DataFrame it will be automatically
 * cast to a RNode).
 */

auto ApplyDefines(ROOT::RDF::RNode df, const std::vector<std::string> &names, const std::vector<std::string> &exprs,
                  unsigned int i = 0) {

  if (i == names.size())
    return df;

  return ApplyDefines(df.Define(names[i], exprs[i]), names, exprs, i + 1);
}


/*****************************************************************/

// Save the variables in finalVariables (checking if each one exists) from a
// dataframe df to a tree called treeName, in the output file.

template <typename T>
auto SnapshotFinalVariables(T &dfFinal,
			    std::string treeName,
			    std::string output,
			    std::vector<std::string> basicVariables,
			    std::vector<std::string> optionalVariables
			    ) {

  // The branches in "basicVariables" should be available
  std::vector<std::string> finalVariables = basicVariables;

  // Now check if each branch in optionalVariables is in the dataframe
  for (std::string branch : optionalVariables) {
    // std::cout << "Checking for optional variable " << branch << std::endl;
    if (HasExactBranch(dfFinal, branch)) {
      // std::cout << "Optional variable found: " << branch << std::endl;
      finalVariables.push_back(branch);
    }
    else {
      std::cout << "Optional variable NOT found: " << branch << std::endl;
    }
  }

  // If you want to check if each jerSysVariables branch is there, use this block
  /* for (std::string branch : jerSysVariables) { */
  /*   if (HasExactBranch(dfFinal, branch)) { */
  /*     std::cout << branch << std::endl; */
  /*     finalVariables.push_back(branch); */
  /*   } */
  /* } */

  ROOT::RDF::RSnapshotOptions opts;
  opts.fMode = "UPDATE";

  dfFinal.Snapshot(treeName, output, finalVariables, opts);


}


/******************************************************************************/

#endif
