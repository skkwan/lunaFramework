// btagEffScaleFactors.h
// Handle b-tagging efficiency scale factors.

#ifndef B_TAG_EFF_SCALE_FACTORS_EXPERIMENTAL_H_INCL
#define B_TAG_EFF_SCALE_FACTORS_EXPERIMENTAL_H_INCL

#include "correction.h"
#include "helperFunctions.h"
#include "sampleConfig_class.h"

using correction::CorrectionSet;
using Helper::containsSubstring;

/*******************************************************************/


/*
 * Get b-tag efficiency scale factor weight.
 */

template <typename T>
auto GetBTagEfficiencyWeightShifts_experimental(T &df, LUNA::sampleConfig_t &sConfig, TString jsonDir) {


  TString cset_dir_btag = TString::Format("%sPOG/BTV/%s_UL/btagging.json", jsonDir.Data(), sConfig.era().c_str());
  auto cset_btag = CorrectionSet::from_file(cset_dir_btag.Data());
  auto cset_btag_shape = cset_btag->at("deepJet_shape");

  // Helper function
  auto getBTagEffWeight = [cset_btag_shape](std::string valueToGet, float nbtag, float bpt_1, float beta_1, int bflavour_1, float bscore_1,
                                            float bpt_2, float beta_2, int bflavour_2, float bscore_2) {

    bool doBUDSGShift = (containsSubstring(valueToGet, "hf") || containsSubstring(valueToGet, "lf") || containsSubstring(valueToGet, "hfstats1") || containsSubstring(valueToGet, "hfstats2") || containsSubstring(valueToGet, "lfstats1") || containsSubstring(valueToGet, "lfstats2"));
    bool doCShift = (containsSubstring(valueToGet, "cferr1") || containsSubstring(valueToGet, "cferr2"));
    bool isNominal = containsSubstring(valueToGet, "central") ? true : false;
    // if JES shift, then this is "other"
    bool doOtherShift = (!isNominal && (doBUDSGShift || doCShift)) ? false : true;

    float weight = 1.0;

    // if jet exists
    if (nbtag > 0) {  // if the jet is b or udsg and we are doing a b/udsg shift, OR if the jet is c and we are doing a c shift, OR if the jet is anything and we are doing a JER shift, get the shift
      // special handling
      if ( isNominal || ((doOtherShift && ((bflavour_1 == 0) || (bflavour_1 == 5))) || (doBUDSGShift && ((bflavour_1 == 0) || (bflavour_1 == 5))) ||  (doCShift && (bflavour_1 == 4)) ) ){
        weight *= (float) cset_btag_shape->evaluate({valueToGet, bflavour_1, abs(beta_1), bpt_1, bscore_1});
      }
    }
    if (nbtag > 1) {  // same thing but for the sub-leading jet
      if ( isNominal || ((doOtherShift && ((bflavour_2 == 0) || (bflavour_2 == 5))) || (doBUDSGShift && ((bflavour_2 == 0) || (bflavour_2 == 5))) ||  (doCShift && (bflavour_2 == 4)) ) ){
        weight *= (float) cset_btag_shape->evaluate({valueToGet, bflavour_2, abs(beta_2), bpt_2, bscore_2});
      }
    }
    return weight;
  };

  // Only apply to MC (not data or data-driven backgrounds like Embedded)
  if (sConfig.isMC()) {

    auto df2 = df.Define("central", []() { return (std::string) "central"; })
                 .Define("weight_btagEff_nominal", getBTagEffWeight, {"central", "nbtag20_nominal",
                                                                      "bpt_deepflavour_1", "beta_deepflavour_1", "bflavour_deepflavour_1", "bscore_deepflavour_1",
                                                                      "bpt_deepflavour_2", "beta_deepflavour_2", "bflavour_deepflavour_2", "bscore_deepflavour_2"});

    if (sConfig.doShifts()) {
      std::string year = sConfig.yearStr();

      // Shifts of the b-tagging efficiency weight due to the SF itself
      return df2.Define("up_hf", []() { return (std::string) "up_hf"; })
                .Define("up_lf", []() { return (std::string) "up_lf"; })
                .Define("up_hfstats1", []() { return (std::string) "up_hfstats1"; })
                .Define("up_hfstats2", []() { return (std::string) "up_hfstats2"; })
                .Define("up_lfstats1", []() { return (std::string) "up_lfstats1"; })
                .Define("up_lfstats2", []() { return (std::string) "up_lfstats2"; })
                .Define("up_cferr1", []() { return (std::string) "up_cferr1"; })
                .Define("up_cferr2", []() { return (std::string) "up_cferr2"; })
                .Define("down_hf", []() { return (std::string) "down_hf"; })
                .Define("down_lf", []() { return (std::string) "down_lf"; })
                .Define("down_hfstats1", []() { return (std::string) "down_hfstats1"; })
                .Define("down_hfstats2", []() { return (std::string) "down_hfstats2"; })
                .Define("down_lfstats1", []() { return (std::string) "down_lfstats1"; })
                .Define("down_lfstats2", []() { return (std::string) "down_lfstats2"; })
                .Define("down_cferr1", []() { return (std::string) "down_cferr1"; })
                .Define("down_cferr2", []() { return (std::string) "down_cferr2"; })
                // Shifts of the b-tagging efficiency weight due to the scale factor itself
                .Define("weight_btagEff_btagsf_hf_Up", getBTagEffWeight, {"up_hf", "nbtag20_nominal", "bpt_deepflavour_1", "beta_deepflavour_1", "bflavour_deepflavour_1", "bscore_deepflavour_1", "bpt_deepflavour_2", "beta_deepflavour_2", "bflavour_deepflavour_2", "bscore_deepflavour_2"})
                .Define("weight_btagEff_btagsf_lf_Up", getBTagEffWeight, {"up_lf", "nbtag20_nominal", "bpt_deepflavour_1", "beta_deepflavour_1", "bflavour_deepflavour_1", "bscore_deepflavour_1", "bpt_deepflavour_2", "beta_deepflavour_2", "bflavour_deepflavour_2", "bscore_deepflavour_2"})
                .Define("weight_btagEff_btagsf_hfstats1_Up", getBTagEffWeight, {"up_hfstats1", "nbtag20_nominal", "bpt_deepflavour_1", "beta_deepflavour_1", "bflavour_deepflavour_1", "bscore_deepflavour_1", "bpt_deepflavour_2", "beta_deepflavour_2", "bflavour_deepflavour_2", "bscore_deepflavour_2"})
                .Define("weight_btagEff_btagsf_hfstats2_Up", getBTagEffWeight, {"up_hfstats2", "nbtag20_nominal", "bpt_deepflavour_1", "beta_deepflavour_1", "bflavour_deepflavour_1", "bscore_deepflavour_1", "bpt_deepflavour_2", "beta_deepflavour_2", "bflavour_deepflavour_2", "bscore_deepflavour_2"})
                .Define("weight_btagEff_btagsf_lfstats1_Up", getBTagEffWeight, {"up_lfstats1", "nbtag20_nominal", "bpt_deepflavour_1", "beta_deepflavour_1", "bflavour_deepflavour_1", "bscore_deepflavour_1", "bpt_deepflavour_2", "beta_deepflavour_2", "bflavour_deepflavour_2", "bscore_deepflavour_2"})
                .Define("weight_btagEff_btagsf_lfstats2_Up", getBTagEffWeight, {"up_lfstats2", "nbtag20_nominal", "bpt_deepflavour_1", "beta_deepflavour_1", "bflavour_deepflavour_1", "bscore_deepflavour_1", "bpt_deepflavour_2", "beta_deepflavour_2", "bflavour_deepflavour_2", "bscore_deepflavour_2"})
                .Define("weight_btagEff_btagsf_cferr1_Up", getBTagEffWeight, {"up_cferr1", "nbtag20_nominal", "bpt_deepflavour_1", "beta_deepflavour_1", "bflavour_deepflavour_1", "bscore_deepflavour_1", "bpt_deepflavour_2", "beta_deepflavour_2", "bflavour_deepflavour_2", "bscore_deepflavour_2"})
                .Define("weight_btagEff_btagsf_cferr2_Up", getBTagEffWeight, {"up_cferr2", "nbtag20_nominal", "bpt_deepflavour_1", "beta_deepflavour_1", "bflavour_deepflavour_1", "bscore_deepflavour_1", "bpt_deepflavour_2", "beta_deepflavour_2", "bflavour_deepflavour_2", "bscore_deepflavour_2"})

                .Define("weight_btagEff_btagsf_hf_Down", getBTagEffWeight, {"down_hf", "nbtag20_nominal", "bpt_deepflavour_1", "beta_deepflavour_1", "bflavour_deepflavour_1", "bscore_deepflavour_1", "bpt_deepflavour_2", "beta_deepflavour_2", "bflavour_deepflavour_2", "bscore_deepflavour_2"})
                .Define("weight_btagEff_btagsf_lf_Down", getBTagEffWeight, {"down_lf", "nbtag20_nominal", "bpt_deepflavour_1", "beta_deepflavour_1", "bflavour_deepflavour_1", "bscore_deepflavour_1", "bpt_deepflavour_2", "beta_deepflavour_2", "bflavour_deepflavour_2", "bscore_deepflavour_2"})
                .Define("weight_btagEff_btagsf_hfstats1_Down", getBTagEffWeight, {"down_hfstats1", "nbtag20_nominal", "bpt_deepflavour_1", "beta_deepflavour_1", "bflavour_deepflavour_1", "bscore_deepflavour_1", "bpt_deepflavour_2", "beta_deepflavour_2", "bflavour_deepflavour_2", "bscore_deepflavour_2"})
                .Define("weight_btagEff_btagsf_hfstats2_Down", getBTagEffWeight, {"down_hfstats2", "nbtag20_nominal", "bpt_deepflavour_1", "beta_deepflavour_1", "bflavour_deepflavour_1", "bscore_deepflavour_1", "bpt_deepflavour_2", "beta_deepflavour_2", "bflavour_deepflavour_2", "bscore_deepflavour_2"})
                .Define("weight_btagEff_btagsf_lfstats1_Down", getBTagEffWeight, {"down_lfstats1", "nbtag20_nominal", "bpt_deepflavour_1", "beta_deepflavour_1", "bflavour_deepflavour_1", "bscore_deepflavour_1", "bpt_deepflavour_2", "beta_deepflavour_2", "bflavour_deepflavour_2", "bscore_deepflavour_2"})
                .Define("weight_btagEff_btagsf_lfstats2_Down", getBTagEffWeight, {"down_lfstats2", "nbtag20_nominal", "bpt_deepflavour_1", "beta_deepflavour_1", "bflavour_deepflavour_1", "bscore_deepflavour_1", "bpt_deepflavour_2", "beta_deepflavour_2", "bflavour_deepflavour_2", "bscore_deepflavour_2"})
                .Define("weight_btagEff_btagsf_cferr1_Down", getBTagEffWeight, {"down_cferr1", "nbtag20_nominal", "bpt_deepflavour_1", "beta_deepflavour_1", "bflavour_deepflavour_1", "bscore_deepflavour_1", "bpt_deepflavour_2", "beta_deepflavour_2", "bflavour_deepflavour_2", "bscore_deepflavour_2"})
                .Define("weight_btagEff_btagsf_cferr2_Down", getBTagEffWeight, {"down_cferr2", "nbtag20_nominal", "bpt_deepflavour_1", "beta_deepflavour_1", "bflavour_deepflavour_1", "bscore_deepflavour_1", "bpt_deepflavour_2", "beta_deepflavour_2", "bflavour_deepflavour_2", "bscore_deepflavour_2"})

                // Shifts due to JER
                .Define("up_jesAbsolute", []() { return (std::string) "up_jesAbsolute"; })
                .Define("up_jesAbsoluteYear", [year]() { return (std::string) "up_jesAbsolute_" + year;  })
                .Define("up_jesBBEC1", []() { return (std::string) "up_jesBBEC1"; })
                .Define("up_jesBBEC1year", [year]() { return (std::string) "up_jesBBEC1_" + year; })
                .Define("up_jesEC2", []() { return (std::string) "up_jesEC2"; })
                .Define("up_jesEC2year", [year]() { return (std::string) "up_jesEC2_" + year; })
                .Define("up_jesFlavorQCD", []() { return (std::string) "up_jesFlavorQCD"; })
                .Define("up_jesHF", []() { return (std::string) "up_jesHF"; })
                .Define("up_jesHFyear", [year]() { return (std::string) "up_jesHF_" + year; })
                .Define("up_jesRelativeBal", []() { return (std::string) "up_jesRelativeBal"; })
                .Define("up_jesRelativeSample", []() { return (std::string) "up_jesRelativeSample"; })
                .Define("up_jer", []() { return (std::string) "up_jer"; })
                .Define("down_jesAbsolute", []() { return (std::string) "down_jesAbsolute"; })
                .Define("down_jesAbsoluteYear", [year]() { return (std::string) "down_jesAbsolute_" + year;  })
                .Define("down_jesBBEC1", []() { return (std::string) "down_jesBBEC1"; })
                .Define("down_jesBBEC1year", [year]() { return (std::string) "down_jesBBEC1_" + year; })
                .Define("down_jesEC2", []() { return (std::string) "down_jesEC2"; })
                .Define("down_jesEC2year", [year]() { return (std::string) "down_jesEC2_" + year; })
                .Define("down_jesFlavorQCD", []() { return (std::string) "down_jesFlavorQCD"; })
                .Define("down_jesHF", []() { return (std::string) "down_jesHF"; })
                .Define("down_jesHFyear", [year]() { return (std::string) "down_jesHF_" + year; })
                .Define("down_jesRelativeBal", []() { return (std::string) "down_jesRelativeBal"; })
                .Define("down_jesRelativeSample", []() { return (std::string) "down_jesRelativeSample"; })
                .Define("down_jer", []() { return (std::string) "down_jer"; })

                .Define("weight_btagEff_JetAbsolute_Up", getBTagEffWeight, {"up_jesAbsolute", "nbtag20_JetAbsolute_Up", "bpt_deepflavour_JetAbsoluteUp_1", "beta_deepflavour_1", "bflavour_deepflavour_1", "bscore_deepflavour_1",
                                                                                                                        "bpt_deepflavour_JetAbsoluteUp_2", "beta_deepflavour_2", "bflavour_deepflavour_2", "bscore_deepflavour_2"})
                .Define("weight_btagEff_JetAbsoluteyear_Up", getBTagEffWeight, {"up_jesAbsoluteYear", "nbtag20_JetAbsoluteyear_Up", "bpt_deepflavour_JetAbsoluteyearUp_1", "beta_deepflavour_1", "bflavour_deepflavour_1", "bscore_deepflavour_1",
                                                                                                                                    "bpt_deepflavour_JetAbsoluteyearUp_2", "beta_deepflavour_2", "bflavour_deepflavour_2", "bscore_deepflavour_2"})
                .Define("weight_btagEff_JetBBEC1_Up", getBTagEffWeight, {"up_jesBBEC1", "nbtag20_JetBBEC1_Up", "bpt_deepflavour_JetBBEC1Up_1", "beta_deepflavour_1", "bflavour_deepflavour_1", "bscore_deepflavour_1",
                                                                                                               "bpt_deepflavour_JetBBEC1Up_2", "beta_deepflavour_2", "bflavour_deepflavour_2", "bscore_deepflavour_2"})
                .Define("weight_btagEff_JetBBEC1year_Up", getBTagEffWeight, {"up_jesBBEC1year", "nbtag20_JetBBEC1year_Up", "bpt_deepflavour_JetBBEC1yearUp_1", "beta_deepflavour_1", "bflavour_deepflavour_1", "bscore_deepflavour_1",
                                                                                                                           "bpt_deepflavour_JetBBEC1yearUp_2", "beta_deepflavour_2", "bflavour_deepflavour_2", "bscore_deepflavour_2"})
                .Define("weight_btagEff_JetEC2_Up", getBTagEffWeight, {"up_jesEC2", "nbtag20_JetEC2_Up", "bpt_deepflavour_JetEC2Up_1", "beta_deepflavour_1", "bflavour_deepflavour_1", "bscore_deepflavour_1",
                                                                                                         "bpt_deepflavour_JetEC2Up_2", "beta_deepflavour_2", "bflavour_deepflavour_2", "bscore_deepflavour_2"})
                .Define("weight_btagEff_JetEC2year_Up", getBTagEffWeight, {"up_jesEC2year", "nbtag20_JetEC2year_Up", "bpt_deepflavour_JetEC2Up_1", "beta_deepflavour_1", "bflavour_deepflavour_1", "bscore_deepflavour_1",
                                                                                                                     "bpt_deepflavour_JetEC2Up_2", "beta_deepflavour_2", "bflavour_deepflavour_2", "bscore_deepflavour_2"})
                .Define("weight_btagEff_JetFlavorQCD_Up", getBTagEffWeight, {"up_jesFlavorQCD", "nbtag20_JetFlavorQCD_Up", "bpt_deepflavour_JetFlavorQCDUp_1", "beta_deepflavour_1", "bflavour_deepflavour_1", "bscore_deepflavour_1",
                                                                                                                            "bpt_deepflavour_JetFlavorQCDUp_2", "beta_deepflavour_2", "bflavour_deepflavour_2", "bscore_deepflavour_2"})
                .Define("weight_btagEff_JetHF_Up", getBTagEffWeight, {"up_jesHF", "nbtag20_JetHF_Up", "bpt_deepflavour_JetHFUp_1", "beta_deepflavour_1", "bflavour_deepflavour_1", "bscore_deepflavour_1",
                                                                                                      "bpt_deepflavour_JetHFUp_2",  "beta_deepflavour_2", "bflavour_deepflavour_2", "bscore_deepflavour_2"})
                .Define("weight_btagEff_JetHFyear_Up", getBTagEffWeight, {"up_jesHF", "nbtag20_JetHFyear_Up", "bpt_deepflavour_JetHFyearUp_1", "beta_deepflavour_1", "bflavour_deepflavour_1", "bscore_deepflavour_1",
                                                                                                              "bpt_deepflavour_JetHFyearUp_2",  "beta_deepflavour_2", "bflavour_deepflavour_2", "bscore_deepflavour_2"})

                .Define("weight_btagEff_JetRelativeBal_Up", getBTagEffWeight, {"up_jesRelativeBal", "nbtag20_JetRelativeBal_Up", "bpt_deepflavour_JetRelativeBalUp_1", "beta_deepflavour_1", "bflavour_deepflavour_1", "bscore_deepflavour_1",
                                                                                                                                 "bpt_deepflavour_JetRelativeBalUp_2",  "beta_deepflavour_2", "bflavour_deepflavour_2", "bscore_deepflavour_2"})
                .Define("weight_btagEff_JetRelativeSample_Up", getBTagEffWeight, {"up_jesHFyear", "nbtag20_JetRelativeSample_Up", "bpt_deepflavour_JetRelativeSampleUp_1", "beta_deepflavour_1", "bflavour_deepflavour_1", "bscore_deepflavour_1",
                                                                                                                          "bpt_deepflavour_JetRelativeSampleUp_2",  "beta_deepflavour_2", "bflavour_deepflavour_2", "bscore_deepflavour_2"})
                .Define("weight_btagEff_JER_Up", getBTagEffWeight, {"central", "nbtag20_JER_Up", "bpt_deepflavour_JERUp_1", "beta_deepflavour_1", "bflavour_deepflavour_1", "bscore_deepflavour_1",
                                                                                                "bpt_deepflavour_JERUp_2",  "beta_deepflavour_2", "bflavour_deepflavour_2", "bscore_deepflavour_2"})
                // Down
                .Define("weight_btagEff_JetAbsolute_Down", getBTagEffWeight, {"down_jesAbsolute", "nbtag20_JetAbsolute_Down", "bpt_deepflavour_JetAbsoluteUp_1", "beta_deepflavour_1", "bflavour_deepflavour_1", "bscore_deepflavour_1",
                                                                                                                        "bpt_deepflavour_JetAbsoluteUp_2", "beta_deepflavour_2", "bflavour_deepflavour_2", "bscore_deepflavour_2"})
                .Define("weight_btagEff_JetAbsoluteyear_Down", getBTagEffWeight, {"down_jesAbsoluteYear", "nbtag20_JetAbsoluteyear_Down", "bpt_deepflavour_JetAbsoluteyearUp_1", "beta_deepflavour_1", "bflavour_deepflavour_1", "bscore_deepflavour_1",
                                                                                                                                    "bpt_deepflavour_JetAbsoluteyearUp_2", "beta_deepflavour_2", "bflavour_deepflavour_2", "bscore_deepflavour_2"})
                .Define("weight_btagEff_JetBBEC1_Down", getBTagEffWeight, {"down_jesBBEC1", "nbtag20_JetBBEC1_Down", "bpt_deepflavour_JetBBEC1Up_1", "beta_deepflavour_1", "bflavour_deepflavour_1", "bscore_deepflavour_1",
                                                                                                               "bpt_deepflavour_JetBBEC1Up_2", "beta_deepflavour_2", "bflavour_deepflavour_2", "bscore_deepflavour_2"})
                .Define("weight_btagEff_JetBBEC1year_Down", getBTagEffWeight, {"down_jesBBEC1year", "nbtag20_JetBBEC1year_Down", "bpt_deepflavour_JetBBEC1yearUp_1", "beta_deepflavour_1", "bflavour_deepflavour_1", "bscore_deepflavour_1",
                                                                                                                           "bpt_deepflavour_JetBBEC1yearUp_2", "beta_deepflavour_2", "bflavour_deepflavour_2", "bscore_deepflavour_2"})
                .Define("weight_btagEff_JetEC2_Down", getBTagEffWeight, {"down_jesEC2", "nbtag20_JetEC2_Down", "bpt_deepflavour_JetEC2Up_1", "beta_deepflavour_1", "bflavour_deepflavour_1", "bscore_deepflavour_1",
                                                                                                         "bpt_deepflavour_JetEC2Up_2", "beta_deepflavour_2", "bflavour_deepflavour_2", "bscore_deepflavour_2"})
                .Define("weight_btagEff_JetEC2year_Down", getBTagEffWeight, {"down_jesEC2year", "nbtag20_JetEC2year_Down", "bpt_deepflavour_JetEC2Up_1", "beta_deepflavour_1", "bflavour_deepflavour_1", "bscore_deepflavour_1",
                                                                                                                     "bpt_deepflavour_JetEC2Up_2", "beta_deepflavour_2", "bflavour_deepflavour_2", "bscore_deepflavour_2"})
                .Define("weight_btagEff_JetFlavorQCD_Down", getBTagEffWeight, {"down_jesFlavorQCD", "nbtag20_JetFlavorQCD_Down", "bpt_deepflavour_JetFlavorQCDUp_1", "beta_deepflavour_1", "bflavour_deepflavour_1", "bscore_deepflavour_1",
                                                                                                                            "bpt_deepflavour_JetFlavorQCDUp_2", "beta_deepflavour_2", "bflavour_deepflavour_2", "bscore_deepflavour_2"})
                .Define("weight_btagEff_JetHF_Down", getBTagEffWeight, {"down_jesHF", "nbtag20_JetHF_Down", "bpt_deepflavour_JetHFUp_1", "beta_deepflavour_1", "bflavour_deepflavour_1", "bscore_deepflavour_1",
                                                                                                            "bpt_deepflavour_JetHFUp_2",  "beta_deepflavour_2", "bflavour_deepflavour_2", "bscore_deepflavour_2"})
                .Define("weight_btagEff_JetHFyear_Down", getBTagEffWeight, {"up_jesHF", "nbtag20_JetHFyear_Down", "bpt_deepflavour_JetHFyearDown_1", "beta_deepflavour_1", "bflavour_deepflavour_1", "bscore_deepflavour_1",
                                                                                                                  "bpt_deepflavour_JetHFyearDown_2", "beta_deepflavour_2", "bflavour_deepflavour_2", "bscore_deepflavour_2"})
                .Define("weight_btagEff_JetRelativeBal_Down", getBTagEffWeight, {"down_jesRelativeBal", "nbtag20_JetRelativeBal_Down", "bpt_deepflavour_JetRelativeBalUp_1", "beta_deepflavour_1", "bflavour_deepflavour_1", "bscore_deepflavour_1",
                                                                                                                                 "bpt_deepflavour_JetRelativeBalUp_2",  "beta_deepflavour_2", "bflavour_deepflavour_2", "bscore_deepflavour_2"})
                .Define("weight_btagEff_JetRelativeSample_Down", getBTagEffWeight, {"down_jesHFyear", "nbtag20_JetRelativeSample_Down", "bpt_deepflavour_JetRelativeSampleUp_1", "beta_deepflavour_1", "bflavour_deepflavour_1", "bscore_deepflavour_1",
                                                                                                                                        "bpt_deepflavour_JetRelativeSampleUp_2",  "beta_deepflavour_2", "bflavour_deepflavour_2", "bscore_deepflavour_2"})
                // note: different handling
                .Define("weight_btagEff_JER_Down", getBTagEffWeight, {"central", "nbtag20_JER_Down", "bpt_deepflavour_JERUp_1", "beta_deepflavour_1", "bflavour_deepflavour_1", "bscore_deepflavour_1",
                                                                                                "bpt_deepflavour_JERUp_2",  "beta_deepflavour_2", "bflavour_deepflavour_2", "bscore_deepflavour_2"});

    }
    else { // MC no shifts: still do the nominal btagEff weight
      return df2;
    }
  }
  if (sConfig.isEmbedded() && sConfig.doShifts()) { // embedded: define these branches anyway to keep the n-tuples flat
      return df.Define("weight_btagEff_nominal", []() { return 1.0f; })
               .Define("weight_btagEff_btagsf_hf_Up", []() { return 1.0f; })
               .Define("weight_btagEff_btagsf_lf_Up", []() { return 1.0f; })
               .Define("weight_btagEff_btagsf_hfstats1_Up", []() { return 1.0f; })
               .Define("weight_btagEff_btagsf_hfstats2_Up", []() { return 1.0f; })
               .Define("weight_btagEff_btagsf_lfstats1_Up", []() { return 1.0f; })
               .Define("weight_btagEff_btagsf_lfstats2_Up", []() { return 1.0f; })
               .Define("weight_btagEff_btagsf_cferr1_Up", []() { return 1.0f; })
               .Define("weight_btagEff_btagsf_cferr2_Up", []() { return 1.0f; })
               .Define("weight_btagEff_btagsf_hf_Down", []() { return 1.0f; })
               .Define("weight_btagEff_btagsf_lf_Down", []() { return 1.0f; })
               .Define("weight_btagEff_btagsf_hfstats1_Down", []() { return 1.0f; })
               .Define("weight_btagEff_btagsf_hfstats2_Down", []() { return 1.0f; })
               .Define("weight_btagEff_btagsf_lfstats1_Down", []() { return 1.0f; })
               .Define("weight_btagEff_btagsf_lfstats2_Down", []() { return 1.0f; })
               .Define("weight_btagEff_btagsf_cferr1_Down", []() { return 1.0f; })
               .Define("weight_btagEff_btagsf_cferr2_Down", []() { return 1.0f; })
               .Define("weight_btagEff_JetAbsolute_Up", []() { return 1.0f; })
               .Define("weight_btagEff_JetAbsoluteyear_Up", []() { return 1.0f; })
               .Define("weight_btagEff_JetBBEC1_Up", []() { return 1.0f; })
               .Define("weight_btagEff_JetBBEC1year_Up",    []() { return 1.0f; })
               .Define("weight_btagEff_JetEC2_Up",  []() { return 1.0f; })
               .Define("weight_btagEff_JetEC2year_Up",  []() { return 1.0f; })
               .Define("weight_btagEff_JetFlavorQCD_Up",  []() { return 1.0f; })
               .Define("weight_btagEff_JetHF_Up", []() { return 1.0f; })
               .Define("weight_btagEff_JetHFyear_Up", []() { return 1.0f; })
               .Define("weight_btagEff_JetRelativeBal_Up",  []() { return 1.0f; })
               .Define("weight_btagEff_JetRelativeSample_Up", []() { return 1.0f; })
               .Define("weight_btagEff_JER_Up", []() { return 1.0f; })
               .Define("weight_btagEff_JetAbsolute_Down", []() { return 1.0f; })
               .Define("weight_btagEff_JetAbsoluteyear_Down", []() { return 1.0f; })
               .Define("weight_btagEff_JetBBEC1_Down",  []() { return 1.0f; })
               .Define("weight_btagEff_JetBBEC1year_Down",  []() { return 1.0f; })
               .Define("weight_btagEff_JetEC2_Down",  []() { return 1.0f; })
               .Define("weight_btagEff_JetEC2year_Down",  []() { return 1.0f; })
               .Define("weight_btagEff_JetFlavorQCD_Down",  []() { return 1.0f; })
               .Define("weight_btagEff_JetHF_Down", []() { return 1.0f; })
               .Define("weight_btagEff_JetHFyear_Down", []() { return 1.0f; })
               .Define("weight_btagEff_JetRelativeBal_Down",  []() { return 1.0f; })
               .Define("weight_btagEff_JetRelativeSample_Down", []() { return 1.0f; })
               .Define("weight_btagEff_JER_Down", []() { return 1.0f; });
  }
  else { // data, or embedded without shifts
    return df.Define("weight_btagEff_nominal", []() { return 1.0f; });
  }

}

/*******************************************************************/

#endif
