// Helper functions for b-tag jet counting with jet systematics.

#ifndef BTAG_JET_COUNTING_H_INCL
#define BTAG_JET_COUNTING_H_INCL

#include "computePhysicsVariables.h"
#include "cut.h"
#include "fileIO.h"
#include "finalVariables.h"
#include "sampleConfig_class.h"

#include <regex>

/******************************************************************************/

/*
 * Return number of b-tag jets (0, 1, or 2) with > 20 GeV, abs(eta) < 2.4, and passing the respective b-tag score thresholds.
 */
float get_nbtag20(float bpt_1, float bscore_1, float wp_1, float beta_1,
                float bpt_2, float bscore_2, float wp_2, float beta_2) {

                  int hasLeadingJet = (bpt_1 > 20) && (bscore_1 > wp_1) && (abs(beta_1) < 2.4);
                  int hasSubleadJet = (bpt_2 > 20) && (bscore_2 > wp_2) && (abs(beta_2) < 2.4);

                  return (float) (hasLeadingJet + hasSubleadJet);
                }

/******************************************************************************/

/*
 * B-jet counting: Defines hasLeadingBTagJet and hasSubleadBTagJet, with different
 * systematics hasLeadingBTagJet_{sysname}_{year}{.Define("variations_/Down} since the b-jet pT systematics
 * may alter the b-jet counting.
 */

template <typename T>
auto GetBTagJetCountingShifts_experimental(T &df, LUNA::sampleConfig_t &sConfig){

  // 2018 medium WP from https://btv-wiki.docs.cern.ch/ScaleFactors/#useful-links
  //                  -> "2018", "2017",
  // B-tagging score threshold

  float wp_medium = 0.0;
  float wp_loose = 0.0;
  if      (sConfig.year() == 2018) { wp_medium = 0.2783; wp_loose = 0.0490; }
  else if (sConfig.year() == 2017) { wp_medium = 0.3040; wp_loose = 0.0532; }
  else if (sConfig.year() == 2016) { wp_medium = 0.2489; wp_loose = 0.0480; } // post-VFP, pre-VFP is 0.2598 for medium and 0.0508 for loose
  else {
    std::cout << "[WARNING:] btagJetCounting.h: year is not 2016, 2017, or 2018: behavior undefined" << std::endl;
  }

  // To-do: count b-tag jets by systematics as well
  // Nominal values
  // Note: MEDIUM DEEPFLAV FOR LEADING JET AND LOOSE FOR SUBLEADING JET
  auto df2 = df.Define("deepFlav_wp_medium", [wp_medium](){ return wp_medium; })
               .Define("deepFlav_wp_loose",  [wp_loose](){ return wp_loose;  })
               .Define("deepFlav_wp_1", "deepFlav_wp_medium")  // change these if changing WP applied to the jets
               .Define("deepFlav_wp_2", "deepFlav_wp_loose")
               .Define("nbtag20_nominal", get_nbtag20, {"bpt_deepflavour_1", "bscore_deepflavour_1", "deepFlav_wp_1", "beta_deepflavour_1", "bpt_deepflavour_2", "bscore_deepflavour_2", "deepFlav_wp_2", "beta_deepflavour_2"});

  if (sConfig.isMC() && sConfig.doShifts()) {
    // Shifts due to JER shifts
    return df2.Define("nbtag20_JER_Up",        get_nbtag20, {"bpt_deepflavour_JERUp_1", "bscore_deepflavour_1", "deepFlav_wp_1", "beta_deepflavour_1", "bpt_deepflavour_JERUp_2", "bscore_deepflavour_2", "deepFlav_wp_2", "beta_deepflavour_2"})
              .Define("nbtag20_JetAbsolute_Up", get_nbtag20, {"bpt_deepflavour_JetAbsoluteUp_1", "bscore_deepflavour_1", "deepFlav_wp_1", "beta_deepflavour_1", "bpt_deepflavour_JetAbsoluteUp_2", "bscore_deepflavour_2", "deepFlav_wp_2", "beta_deepflavour_2"})
              .Define("nbtag20_JetAbsoluteyear_Up", get_nbtag20, {"bpt_deepflavour_JetAbsoluteyearUp_1", "bscore_deepflavour_1", "deepFlav_wp_1", "beta_deepflavour_1", "bpt_deepflavour_JetAbsoluteyearUp_2", "bscore_deepflavour_2", "deepFlav_wp_2", "beta_deepflavour_2"})
              .Define("nbtag20_JetBBEC1_Up", get_nbtag20, {"bpt_deepflavour_JetBBEC1Up_1", "bscore_deepflavour_1", "deepFlav_wp_1", "beta_deepflavour_1", "bpt_deepflavour_JetBBEC1Up_2", "bscore_deepflavour_2", "deepFlav_wp_2", "beta_deepflavour_2"})
              .Define("nbtag20_JetBBEC1year_Up", get_nbtag20, {"bpt_deepflavour_JetBBEC1yearUp_1", "bscore_deepflavour_1", "deepFlav_wp_1", "beta_deepflavour_1", "bpt_deepflavour_JetBBEC1yearUp_2", "bscore_deepflavour_2", "deepFlav_wp_2", "beta_deepflavour_2"})
              .Define("nbtag20_JetEC2_Up", get_nbtag20, {"bpt_deepflavour_JetEC2Up_1", "bscore_deepflavour_1", "deepFlav_wp_1", "beta_deepflavour_1", "bpt_deepflavour_JetEC2Up_2", "bscore_deepflavour_2", "deepFlav_wp_2", "beta_deepflavour_2"})
              .Define("nbtag20_JetEC2year_Up", get_nbtag20, {"bpt_deepflavour_JetEC2yearUp_1", "bscore_deepflavour_1", "deepFlav_wp_1", "beta_deepflavour_1", "bpt_deepflavour_JetEC2yearUp_2", "bscore_deepflavour_2", "deepFlav_wp_2", "beta_deepflavour_2"})
              .Define("nbtag20_JetFlavorQCD_Up", get_nbtag20, {"bpt_deepflavour_JetFlavorQCDUp_1", "bscore_deepflavour_1", "deepFlav_wp_1", "beta_deepflavour_1", "bpt_deepflavour_JetFlavorQCDUp_2", "bscore_deepflavour_2", "deepFlav_wp_2", "beta_deepflavour_2"})
              .Define("nbtag20_JetHF_Up", get_nbtag20, {"bpt_deepflavour_JetHFUp_1", "bscore_deepflavour_1", "deepFlav_wp_1", "beta_deepflavour_1", "bpt_deepflavour_JetHFUp_2", "bscore_deepflavour_2", "deepFlav_wp_2", "beta_deepflavour_2"})
              .Define("nbtag20_JetHFyear_Up", get_nbtag20, {"bpt_deepflavour_JetHFyearUp_1", "bscore_deepflavour_1", "deepFlav_wp_1", "beta_deepflavour_1", "bpt_deepflavour_JetHFyearUp_2", "bscore_deepflavour_2", "deepFlav_wp_2", "beta_deepflavour_2"})
              .Define("nbtag20_JetRelativeBal_Up", get_nbtag20, {"bpt_deepflavour_JetRelativeBalUp_1", "bscore_deepflavour_1", "deepFlav_wp_1", "beta_deepflavour_1", "bpt_deepflavour_JetRelativeBalUp_2", "bscore_deepflavour_2", "deepFlav_wp_2", "beta_deepflavour_2"})
              .Define("nbtag20_JetRelativeSample_Up", get_nbtag20, {"bpt_deepflavour_JetRelativeSampleUp_1", "bscore_deepflavour_1", "deepFlav_wp_1", "beta_deepflavour_1", "bpt_deepflavour_JetRelativeSampleUp_2", "bscore_deepflavour_2", "deepFlav_wp_2", "beta_deepflavour_2"})

              .Define("nbtag20_JER_Down",        get_nbtag20, {"bpt_deepflavour_JERDown_1", "bscore_deepflavour_1", "deepFlav_wp_1", "beta_deepflavour_1", "bpt_deepflavour_JERDown_2", "bscore_deepflavour_2", "deepFlav_wp_2", "beta_deepflavour_2"})
              .Define("nbtag20_JetAbsolute_Down", get_nbtag20, {"bpt_deepflavour_JetAbsoluteDown_1", "bscore_deepflavour_1", "deepFlav_wp_1", "beta_deepflavour_1", "bpt_deepflavour_JetAbsoluteDown_2", "bscore_deepflavour_2", "deepFlav_wp_2", "beta_deepflavour_2"})
              .Define("nbtag20_JetAbsoluteyear_Down", get_nbtag20, {"bpt_deepflavour_JetAbsoluteyearDown_1", "bscore_deepflavour_1", "deepFlav_wp_1", "beta_deepflavour_1", "bpt_deepflavour_JetAbsoluteyearDown_2", "bscore_deepflavour_2", "deepFlav_wp_2", "beta_deepflavour_2"})
              .Define("nbtag20_JetBBEC1_Down", get_nbtag20, {"bpt_deepflavour_JetBBEC1Down_1", "bscore_deepflavour_1", "deepFlav_wp_1", "beta_deepflavour_1", "bpt_deepflavour_JetBBEC1Down_2", "bscore_deepflavour_2", "deepFlav_wp_2", "beta_deepflavour_2"})
              .Define("nbtag20_JetBBEC1year_Down", get_nbtag20, {"bpt_deepflavour_JetBBEC1yearDown_1", "bscore_deepflavour_1", "deepFlav_wp_1", "beta_deepflavour_1", "bpt_deepflavour_JetBBEC1yearDown_2", "bscore_deepflavour_2", "deepFlav_wp_2", "beta_deepflavour_2"})
              .Define("nbtag20_JetEC2_Down", get_nbtag20, {"bpt_deepflavour_JetEC2Down_1", "bscore_deepflavour_1", "deepFlav_wp_1", "beta_deepflavour_1", "bpt_deepflavour_JetEC2Down_2", "bscore_deepflavour_2", "deepFlav_wp_2", "beta_deepflavour_2"})
              .Define("nbtag20_JetEC2year_Down", get_nbtag20, {"bpt_deepflavour_JetEC2yearDown_1", "bscore_deepflavour_1", "deepFlav_wp_1", "beta_deepflavour_1", "bpt_deepflavour_JetEC2yearDown_2", "bscore_deepflavour_2", "deepFlav_wp_2", "beta_deepflavour_2"})
              .Define("nbtag20_JetFlavorQCD_Down", get_nbtag20, {"bpt_deepflavour_JetFlavorQCDDown_1", "bscore_deepflavour_1", "deepFlav_wp_1", "beta_deepflavour_1", "bpt_deepflavour_JetFlavorQCDDown_2", "bscore_deepflavour_2", "deepFlav_wp_2", "beta_deepflavour_2"})
              .Define("nbtag20_JetHF_Down", get_nbtag20, {"bpt_deepflavour_JetHFDown_1", "bscore_deepflavour_1", "deepFlav_wp_1", "beta_deepflavour_1", "bpt_deepflavour_JetHFDown_2", "bscore_deepflavour_2", "deepFlav_wp_2", "beta_deepflavour_2"})
              .Define("nbtag20_JetHFyear_Down", get_nbtag20, {"bpt_deepflavour_JetHFyearDown_1", "bscore_deepflavour_1", "deepFlav_wp_1", "beta_deepflavour_1", "bpt_deepflavour_JetHFyearDown_2", "bscore_deepflavour_2", "deepFlav_wp_2", "beta_deepflavour_2"})
              .Define("nbtag20_JetRelativeBal_Down", get_nbtag20, {"bpt_deepflavour_JetRelativeBalDown_1", "bscore_deepflavour_1", "deepFlav_wp_1", "beta_deepflavour_1", "bpt_deepflavour_JetRelativeBalDown_2", "bscore_deepflavour_2", "deepFlav_wp_2", "beta_deepflavour_2"})
              .Define("nbtag20_JetRelativeSample_Down", get_nbtag20, {"bpt_deepflavour_JetRelativeSampleDown_1", "bscore_deepflavour_1", "deepFlav_wp_1", "beta_deepflavour_1", "bpt_deepflavour_JetRelativeSampleDown_2", "bscore_deepflavour_2", "deepFlav_wp_2", "beta_deepflavour_2"});

  }
  else {
    return df2.Define("nbtag20_JER_Up",        []() { return 1.0f; })
              .Define("nbtag20_JetAbsolute_Up", []() { return 1.0f; })
              .Define("nbtag20_JetAbsoluteyear_Up", []() { return 1.0f; })
              .Define("nbtag20_JetBBEC1_Up", []() { return 1.0f; })
              .Define("nbtag20_JetBBEC1year_Up", []() { return 1.0f; })
              .Define("nbtag20_JetEC2_Up", []() { return 1.0f; })
              .Define("nbtag20_JetEC2year_Up", []() { return 1.0f; })
              .Define("nbtag20_JetFlavorQCD_Up", []() { return 1.0f; })
              .Define("nbtag20_JetHF_Up", []() { return 1.0f; })
              .Define("nbtag20_JetHFyear_Up", []() { return 1.0f; })
              .Define("nbtag20_JetRelativeBal_Up", []() { return 1.0f; })
              .Define("nbtag20_JetRelativeSample_Up", []() { return 1.0f; })
              .Define("nbtag20_JER_Down",        []() { return 1.0f; })
              .Define("nbtag20_JetAbsolute_Down", []() { return 1.0f; })
              .Define("nbtag20_JetAbsoluteyear_Down", []() { return 1.0f; })
              .Define("nbtag20_JetBBEC1_Down", []() { return 1.0f; })
              .Define("nbtag20_JetBBEC1year_Down", []() { return 1.0f; })
              .Define("nbtag20_JetEC2_Down", []() { return 1.0f; })
              .Define("nbtag20_JetEC2year_Down", []() { return 1.0f; })
              .Define("nbtag20_JetFlavorQCD_Down", []() { return 1.0f; })
              .Define("nbtag20_JetHF_Down", []() { return 1.0f; })
              .Define("nbtag20_JetHFyear_Down", []() { return 1.0f; })
              .Define("nbtag20_JetRelativeBal_Down", []() { return 1.0f; })
              .Define("nbtag20_JetRelativeSample_Down", []() { return 1.0f; });
  }

}

/******************************************************************************/

#endif
