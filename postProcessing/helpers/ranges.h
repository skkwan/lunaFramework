/**********************************************************/
//
/**********************************************************/


#ifndef RANGES_H_INCL
#define RANGES_H_INCL

typedef std::map<std::string, std::vector<float>> ranges_t;

/**********************************************************/

const int default_nbins = 30;

ranges_t ranges {

    {"pt_1", {default_nbins, 10, 160}},
    {"pt_2", {default_nbins, 10, 160}},
    {"eta_1", {default_nbins, -3, 3}},
    {"eta_2", {default_nbins, -3, 3}},
    {"phi_1", {default_nbins, -3.14, 3.14}},
    {"phi_2", {default_nbins, -3.14, 3.14}},
    {"m_1", {default_nbins, 10, 120}},
    {"m_2", {default_nbins, 10, 120}},


    {"jpt_1", {default_nbins, 10, 120}},
    {"jeta_1", {default_nbins, -2.4, 2.4}},
    {"jphi_1", {default_nbins, -3.14, 3.14}},


    {"jpt_2", {default_nbins, 10, 120}},
    {"jeta_2", {default_nbins, -2.4, 2.4}},
    {"jphi_2", {default_nbins, -3.14, 3.14}},

    {"mjj", {default_nbins, 10, 250}},

    {"nbtag20", {3, 0, 3}},
    {"weight_btagEff", {100,0,2}},

    {"bpt_deepflavour_1",{default_nbins, 10, 250}},
    {"beta_deepflavour_1", {default_nbins, -3, 3}},
    {"bphi_deepflavour_1", {default_nbins, -3.14, 3.14}},
    {"bm_deepflavour_1", {default_nbins, 10, 250}},

    {"bpt_deepflavour_2",{default_nbins, 10, 250}},
    {"beta_deepflavour_2", {default_nbins, -3, 3}},
    {"bphi_deepflavour_2", {default_nbins, -3.14, 3.14}},
    {"bm_deepflavour_2", {default_nbins, 10, 250}},

    {"m_vis", {default_nbins, 0, 300}},
    {"m_vis_varied", {default_nbins, 0, 300}},
    {"pt_vis", {default_nbins, 0, 120}},
    {"mtMET_1", {default_nbins, 0, 250}},
    {"mtMET_2", {default_nbins, 0, 250}},

    {"gen_mtt", {default_nbins, 0, 300}},
    // {"m_sv", {default_nbins, 0, 300}},

    {"met",    {default_nbins, 0, 200}},
    {"metphi", {default_nbins, -3.14, 3.14}},
    {"metphi_varied", {default_nbins, -3.14, 3.14}},

    {"D_zeta", {default_nbins, -140, 110}},
    {"m_btautau_vis", {default_nbins, 0, 480}},

    {"passSingleTrigger_mt", {3, 0, 3}},
    {"passCrossTrigger_mt", {3, 0, 3}}

};


#endif
