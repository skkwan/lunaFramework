#ifndef CUT_H_INCL
#define CUT_H_INCL

#include "helperFunctions.h"
#include "ranges.h"

/******************************************************************************/


class Cut {

 public:
  // Data members
  std::string baseVariable;  // base variable (e.g. pt_2)
  std::string varSuffix;     // suffix for the variable (e.g. "_CMS_TES_dm0_Up", or "" for nominal):
                             // the string baseVariable+varSuffix MUST be an existing branch in the RDataFrame
  std::string cut;           // the cut which produces this histogram from the n-tuple
  std::string weight;        // the name of the branch in the RDataFrame containing the weights to apply


  std::string variable;      // baseVariable + varSuffix
  std::string histName;      // the name of the histogram (baseVariable + varSuffix + myNameSuffix)

  std::string category;      // folder name

  int nBins;                 // number of bins in the histogram
  int xMin;                  // minimum x-axis value
  int xMax;                  // maximum x-axis value

  // Constructor: note that we only need to declare these five properties, the rest will be deduced in the constructor
  // myVar: base variable, e.g. m_sv
  // mySuffix: suffix (e.g. _CMS_MuonES_0to1p2_2018Up)
  // myCut: event selection to apply on dataframe for this histogram
  // myWeight: branch in the RDF which has the weight
  // myNameSuffix: only used for _antiIso really
  // thisBranchExists: does the branch (myVar + mySuffix + myNameSuffix) already exist in the RDF?
  Cut(std::string myVar,
      std::string mySuffix,
      std::string myCut,
      std::string myWeight,
      std::string myNameSuffix,
      std::string myCategory = "",
      bool thisBranchExists = true) {

        baseVariable = myVar;
        varSuffix    = mySuffix;
        cut          = myCut;
        weight       = myWeight;

        // Get the histogram name, e.g. m_vis_CMS_muES_0to2p1 or pt_1_CMS_muES_0to2p1_antiIso
        histName = baseVariable + varSuffix + myNameSuffix;

        // If the branch itself exists (e.g. pt_1_CMS_muES_0to2p1_2018Up) because we calculated its direct shift, we are going to fill that histogram
        // with that branch
        if (thisBranchExists) {
          variable = baseVariable + varSuffix;
          std::cout << "....... Cut constructor: trying to build " << myCategory << "/" << histName << ", assuming " << variable << " is already in RDataFrame, "
                    << " with cut " << cut << std::endl;
        }
        // Otherwise for shifts that come about indirectly, e.g. pt_1_CMS_JER_2018Up, because pt_1 isn't directly shifted by JER but its yield is impacted
        // because JER changes the b-tag jet counting, i.e. affects our signal region yield,
        // the histogram will be filled with just the base variable (in this example, pt_1)
        else {
          variable = baseVariable;
          std::cout << "........ Cut initializer: trying to build " << myCategory << "/" << histName << ", assuming " << baseVariable + varSuffix << " or its equivalent is NOT in the RDataFrame, fall back to fill histo with "
                    << baseVariable
                     << " with cut " << cut << std::endl;
        }
        // Go to the ranges global variable to get the nBins, xMin, xMax for this base variable
        ranges_t::iterator it;
        it = ranges.find(myVar);

        // Make sure the entry in ranges was found
        if (it == ranges.end()) {
          std::cout << "[ERROR:] element " << myVar << " not found in ranges, setting default values" << std::endl;
          nBins = 30;
          xMin  = 0;
          xMax  = 500;
        }
        else {
          // std::cout << "Found " << myVar << " corresponding to " << it->first << " in ranges" << std::endl;
          nBins = it->second[0];
          xMin  = it->second[1];
          xMax  = it->second[2];
        }

        category = myCategory;

    }
};

/******************************************************************************/



#endif
