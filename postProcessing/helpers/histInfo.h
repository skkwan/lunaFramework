
#ifndef HIST_INFO_H_INCL
#define HIST_INFO_H_INCL

/******************************************************************************/


/*
 * Class containing the TH1D RResultPtr and the category it belongs to.
 */

class HistInfo {

  private:
    std::string hName;
    std::string category;
    ROOT::RDF::RResultPtr<TH1D> histPtr;
    ROOT::RDF::RResultPtr<ROOT::RDF::RCutFlowReport > reportPtr;


  public:
    HistInfo(std::string name, ROOT::RDF::RResultPtr<TH1D> histogramPtr, ROOT::RDF::RResultPtr<ROOT::RDF::RCutFlowReport> cutflowreportPtr, std::string cat = "") {
      hName = name;
      histPtr = histogramPtr;
      reportPtr = cutflowreportPtr;
      category = cat;
    }

    // Setters
    int setCategory(std::string cat) {
      category = cat;
      return 1;
    }

    // Getters
    std::string getHistName() {
      return hName;
    }

    std::string getFullName() {
      if (category == "") {
        return hName;
      }
      else {
        return category + "/" + hName;
      }
    }

    std::string getCategory() {
      return category;
    }

    ROOT::RDF::RResultPtr<TH1D> getHistSmartPtr() {
      return histPtr;
    }

    ROOT::RDF::RResultPtr<ROOT::RDF::RCutFlowReport> getReportSmartPtr() {
      return reportPtr;
    }



};

/******************************************************************************/

/*
 * Typedef for mapping variables to their HistInfo objects.
 */
typedef std::map<std::string, HistInfo> mapHists_t;
typedef std::map<std::string, mapHists_t> mapChannelmapHists_t;

/******************************************************************************/

/*
 * Typedef for mapping channel strings to its std::vector<Cut>objects.
 */
typedef std::map<std::string, std::vector<Cut>> mapChannelCuts_t;

/******************************************************************************/

#endif
