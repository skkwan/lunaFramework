// Z pT reweighing

#ifndef Z_PT_REWEIGHING_EXPERIMENTAL_H_INCL
#define Z_PT_REWEIGHING_EXPERIMENTAL_H_INCL

#include "helperFunctions.h"
#include "sampleConfig_class.h"

#include "RooRealVar.h"
#include "RooWorkspace.h"

std::mutex rooworkspace_mutex; // protects the RooWorkSpace

/*******************************************************************/

/*
 * https://github.com/pallabidas/AABBTT_allyears/blob/master/mutau_Tree_mt.h#L762-L768
 */

float getZPtWeightNominal(RooWorkspace *wmc, float zGenPt, float zGenMass) {

    std::lock_guard<std::mutex> lock(rooworkspace_mutex);
    // rooworkspace_mutex is automatically released when the lock goes out of scope
    float sf = 1.0;

    wmc->var("z_gen_pt")->setVal(zGenPt);
    wmc->var("z_gen_mass")->setVal(zGenMass);

    sf *= wmc->function("zptmass_weight_nom")->getVal();
    return sf;
}

/*******************************************************************/

template <typename T>
auto GetZPtReweighing_experimental(T &df, LUNA::sampleConfig_t &sConfig) {

    // Nominal correction
    if (sConfig.isMC() && sConfig.isDY()) {
        auto df2 = df.Define("weight_zPt_nominal", getZPtWeightNominal, {"wmcPtr", "gen_recoilBoson_Pt", "gen_recoilBoson_M"});
        // DY+Jets are recoil samples, so we found the Z boson's gen pT and mass at skim-level. Use these branches
        if (sConfig.doShifts()) {
            // +/- 10% of nominal weight: https://twiki.cern.ch/twiki/bin/viewauth/CMS/HiggsToTauTauWorkingLegacyRun2#Uncertainty_in_the_Z_pT_mass_rew
            return df2.Define("weight_zPt_Zpt_Up",   "(float) (weight_zPt_nominal * 1.1)")
                      .Define("weight_zPt_Zpt_Down", "(float) (weight_zPt_nominal * 0.9)");
        }
    }
    return df.Define("weight_zPt_nominal",          [=]() { return (float) 1.0; })
             .Define("weight_zPt_Zpt_Up",   [=]() { return (float) 1.0; })
             .Define("weight_zPt_Zpt_Down", [=]() { return (float) 1.0; });

}


/*******************************************************************/


#endif
