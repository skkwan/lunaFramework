// reweightDYJets.h

#ifndef REWEIGHT_DY_JETS_EXPERIMENTAL_H_INCLs
#define REWEIGHT_DY_JETS_EXPERIMENTAL_H_INCLs

#include "fileIO.h"
#include "sampleConfig_class.h"

/*******************************************************************/


// https://twiki.cern.ch/twiki/bin/viewauth/CMS/MCStitching#Stitching_inclusive_with_jet_AN1

/*******************************************************************/

using namespace ROOT::VecOps;

// isDYJets: whether the sample is a DYJets sample or not
// nJets: valid values are 0, 1, 2, 3, and 4
// nEntries_DYJets: the number of NanoAOD events processed in DYJets
// nEntries_this:  "                                    " in this particular DYJets sample

float getReweightFactorDYJets(int isLowMass,
                           int nJets,
                           float nEntries_DYJets,
                           float nEntries_this,
                           float luminosity) {

  float reweightFactor = 1.0;

  // std::cout << "nEntries_DYJets: " << nEntries_DYJets
  //     << "nEntries_this:   " << nEntries_this << std::endl;

  // Ratio of NNLO to LO cross-section for DYJets
  double NNLOtoLO = 6077.22/5398.0;

  // LO cross-sections for DYJets, etc
  std::vector<double> DYJets_LOxsec{ 5398.0, 876.9, 306.4, 112.0, 44.03 };

  // LO cross-section for low-mass DYJets
  std::vector<double> DYJets_LowMass_LOxsec{ 15810.0 };

  float DYLo_W;
  float DYLo_i;

  if (!isLowMass) {
    // M = 50 GeV
    DYLo_W = nEntries_DYJets / (NNLOtoLO * DYJets_LOxsec[0]);
    DYLo_i = nEntries_this  /  (NNLOtoLO * DYJets_LOxsec[nJets]);

    if (nJets > 0) {
      reweightFactor = (luminosity / (DYLo_W + DYLo_i));
    }
    else if (nJets == 0) {
      reweightFactor = luminosity / DYLo_W;
    }
  }
  else {
    // M = 10 to 50 GeV
    DYLo_W = nEntries_this / (NNLOtoLO * DYJets_LowMass_LOxsec[0]);
    reweightFactor = luminosity / DYLo_W;
  }

  // std::cout << ">>> reweightDYJets.h: "
  //           << "reweightFactor: " << reweightFactor
  //           << "(from DYLo_W, DYLo_i) " << DYLo_W << ", " << DYLo_i
  //           << std::endl;

  return reweightFactor;
}

/*******************************************************************/

/*
 * For DYJets and DYJets low-mass (2018), apply cross-section and luminosity-based weight.
 * For DY{1-4}Jets, apply appropriate cross-section and luminosity-based weight.
 * For other samples, weight_DYJets is 1.0.
 */

template <typename T>
auto GetDYJetsWeight_experimental(T &df, LUNA::sampleConfig_t &sConfig, std::map<std::string, std::string> fileMap, int isLocalJob) {

  if (sConfig.isDY()) {
    int isLowMass = false;
    if (Helper::containsSubstring(sConfig.name(), "M-10to50")) { isLowMass = true; }

    std::string inclusiveSampleName = "DYJetsToLL_M-50";
    if (sConfig.era() == "2016preVFP") {
      inclusiveSampleName = "preVFP_DYJetsToLL_M-50";
    }
    else if (sConfig.era() == "2016postVFP") {
      inclusiveSampleName = "postVFP_DYJetsToLL_M-50";
    }
    float nEntries_DYJets = getEventsProcessedFromMap(inclusiveSampleName, fileMap, isLocalJob);

    std::cout << ">>> reweightDYJets-experimental.h: Got " << nEntries_DYJets << " events in the inclusive sample" << std::endl;
    return df.Define("isLowMass",       [isLowMass](){ return isLowMass; })
             .Define("nEntries_DYJets", [nEntries_DYJets](){ return nEntries_DYJets; })
             .Define("weight_DYJets_nominal", getReweightFactorDYJets,
                      {"isLowMass", "lhe_njets_DY_W_jets", "nEntries_DYJets", "nEntriesProcessedTotal", "luminosity"});
  }
  else {
    return df.Define("weight_DYJets_nominal", []() { return 1.0f; });
  }

}




/*******************************************************************/


#endif
