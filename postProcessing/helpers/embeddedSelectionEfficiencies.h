#ifndef EMBEDDED_SELECTION_EFFICIENCIES_H_INCL
#define EMBEDDED_SELECTION_EFFICIENCIES_H_INCL

#include "helperFunctions.h"
#include "sampleConfig_class.h"

/*******************************************************************/

template <typename T>
auto GetEmbeddedSelectionEfficiencies(T &df, LUNA::sampleConfig_t &sConfig, TString jsonDir) {

    if (sConfig.isEmbedded()) {

        TString cset_dir;
        cset_dir.Form("%sembeddingselection_%sUL.json", jsonDir.Data(), sConfig.era().c_str());
        std::cout << cset_dir << std::endl;
        auto cset = CorrectionSet::from_file(cset_dir.Data());
        auto cset_sel_ratio = cset->at("m_sel_trg_kit_ratio");
        auto cset_embID = cset->at("EmbID_pt_eta_bins");

        // "Data efficiency of the muon ID used during the Embedding selection"
        // One per muon
        return df.Define("weight_m_sel_id_ratio_1", [cset_embID] (const float genPt, const float genEta) {
            return (float) cset_embID->evaluate({genPt, abs(genEta)});
        }, {"genPt_embLeg1", "genEta_embLeg1"})
        .Define("weight_m_sel_id_ratio_2", [cset_embID] (const float genPt, const float genEta) {
            return (float) cset_embID->evaluate({genPt, abs(genEta)});
        }, {"genPt_embLeg2", "genEta_embLeg2"})
        // "Scale Factor to be applied for the Trigger Selection used during the Selection step of Tau-Embedding. Calcualates as 1 / Efficiency of the DoubleMuon Trigger used for the selection.",
        .Define("weight_m_sel_trg_ratio", [cset_sel_ratio] (const float genPt_1, const float genEta_1, const float genPt_2, const float genEta_2) {
            return (float) cset_sel_ratio->evaluate({genPt_1, abs(genEta_1), genPt_2, abs(genEta_2)});
        }, {"genPt_embLeg1", "genEta_embLeg1", "genPt_embLeg2", "genEta_embLeg2"});
    }

    return df.Define("weight_m_sel_id_ratio_1", []() { return 1.0f; })
             .Define("weight_m_sel_id_ratio_2", []() { return 1.0f; })
             .Define("weight_m_sel_trg_ratio", []() { return 1.0f; });

}

/*******************************************************************/

#endif
