//Top pT reweighing

#ifndef TOP_PT_REWEIGHING_EXPERIMENTAL_H_INCL
#define TOP_PT_REWEIGHING_EXPERIMENTAL_H_INCL

#include "helperFunctions.h"
#include "sampleConfig_class.h"

/*******************************************************************/

/*
 * Re-weight top pT spectrum for ttbar samples.
 */

float getTopPtWeightNominal(float genPt_top1, float genPt_top2) {

  float weight = 1.0;  // default value

  // If both gen top quarks were found:
  if ((genPt_top1 > 0) && (genPt_top2 > 0)) {
    float pt_top1 = genPt_top1;
    if (pt_top1 > 500) pt_top1 = 500;
    float pt_top2 = genPt_top2;
    if (pt_top2 > 500) pt_top2 = 500;
    weight = sqrt(exp(0.0615 - 0.0005*pt_top1) * exp(0.0615 - 0.0005*pt_top2));
  }

  // std::cout << ">>> getTopPtWeight.h: weight: " << weight << std::endl;
  return weight;
}

/*******************************************************************/

/*
 * Vary() will replace the nominal weight_topPt with the up/down shifts.
 * Up is the weight applied twice, down is no weight applied.
 */
ROOT::RVecF getTopPtWeightVariations(float weight_topPt) {
  return ROOT::RVecF{weight_topPt * weight_topPt, 1.0};
}

/*******************************************************************/

template <typename T>
auto GetTopPtReweighing_experimental(T &df, LUNA::sampleConfig_t &sConfig) {

    if (sConfig.isMC() && sConfig.isTTbar()) {
      auto df2 = df.Define("weight_topPt_nominal", getTopPtWeightNominal, {"gen_topPt_1", "gen_topPt_2"});
      if (sConfig.doShifts()) {
        return df2.Define("variations_weight_topPt",  getTopPtWeightVariations, {"weight_topPt_nominal"})
                  .Define("weight_topPt_toppt_Up",   "variations_weight_topPt[0]")
                  .Define("weight_topPt_toppt_Down", "variations_weight_topPt[1]");
      }
    }
    return df.Define("weight_topPt_nominal",    []() { return 1.0f; })
             .Define("weight_topPt_toppt_Up",   []() { return 1.0f; })
             .Define("weight_topPt_toppt_Down", []() { return 1.0f; });

}


/*******************************************************************/


#endif
