#ifndef TAU_ID_EFF_VS_MU_SCALE_FACTORS_EXPERIMENTAL_H_INCL
#define TAU_ID_EFF_VS_MU_SCALE_FACTORS_EXPERIMENTAL_H_INCL

#include "correction.h"
#include "helperFunctions.h"
#include "sampleConfig_class.h"

using correction::CorrectionSet;

/*******************************************************************/

/*
 * Weights and shifts for DeepTau2017v2p1 muons faking tauh
 */

template <typename T>
auto GetMuFakingTauhWeightShifts_experimental(T &df, LUNA::sampleConfig_t &sConfig, TString jsonDir) {

  // MC: any year
  if (sConfig.isMC()) {
    TString cset_dir;
    TString objName;
    cset_dir.Form("%sPOG/TAU/%s_UL/tau.json", jsonDir.Data(), sConfig.era().c_str());
    objName = "DeepTau2017v2p1VSmu";

    auto cset = CorrectionSet::from_file(cset_dir.Data());
    auto cset_tauID = cset->at(objName.Data());

    auto df2 = df.Define("weight_tauideff_VSmu_nominal", [cset_tauID] (const int channel, const float eta, const unsigned int genmatch) {
                      if (channel == Helper::mt) { // mutau channel uses Tight vs. muon working point
                        return (float) cset_tauID->evaluate({abs(eta), (int) genmatch, "Tight", "nom"});
                      }
                      else if (channel == Helper::et) {  // etau channel uses VLoose vs. muon working point
                        return (float) cset_tauID->evaluate({abs(eta), (int) genmatch, "VLoose", "nom"});
                      }
                      else {
                        return 1.0f;
                      }
                    }, {"channel", "eta_2", "gen_match_2"});
    if (sConfig.doShifts()) {
      return df2.Define("weight_tauideff_VSmu_Up", [cset_tauID] (const int channel, const float eta, const int unsigned genmatch) {
                        if (channel == Helper::mt) { // mutau channel uses Tight vs. muon working point
                          return (float) cset_tauID->evaluate({abs(eta), (int) genmatch, "Tight", "up"});
                        }
                        else if (channel == Helper::et) {  // etau channel uses VLoose vs. muon working point
                          return (float) cset_tauID->evaluate({abs(eta), (int) genmatch, "VLoose", "up"});
                        }
                        else {
                          return 1.0f;
                        }
                      }, {"channel", "eta_2", "gen_match_2"})
                .Define("weight_tauideff_VSmu_Down", [cset_tauID] (const int channel, const float eta, const int unsigned genmatch) {
                        if (channel == Helper::mt) { // mutau channel uses Tight vs. muon working point
                          return (float) cset_tauID->evaluate({abs(eta), (int) genmatch, "Tight", "down"});
                        }
                        else if (channel == Helper::et) {  // etau channel uses VLoose vs. muon working point
                          return (float) cset_tauID->evaluate({abs(eta), (int) genmatch, "VLoose", "down"});
                        }
                        else {
                          return 1.0f;
                        }
                      }, {"channel", "eta_2", "gen_match_2"})

                // Bin by eta: up shifts
                .Define("weight_tauideff_VSmu_eta0to0p4_Up", [&](const float eta, const float weight_nom, const float weight_shifted) {
                            return ((abs(eta) >= 0) && (abs(eta) < 0.4)) ? weight_shifted : weight_nom;
                        }, {"eta_2", "weight_tauideff_VSmu_nominal", "weight_tauideff_VSmu_Up"})
                .Define("weight_tauideff_VSmu_eta0p4to0p8_Up", [&](const float eta, const float weight_nom, const float weight_shifted) {
                            return ((abs(eta) >= 0.4) && (abs(eta) < 0.8)) ? weight_shifted : weight_nom;
                        }, {"eta_2", "weight_tauideff_VSmu_nominal", "weight_tauideff_VSmu_Up"})
                .Define("weight_tauideff_VSmu_eta0p8to1p2_Up", [&](const float eta, const float weight_nom, const float weight_shifted) {
                            return ((abs(eta) >= 0.8) && (abs(eta) < 1.2)) ? weight_shifted : weight_nom;
                        }, {"eta_2", "weight_tauideff_VSmu_nominal", "weight_tauideff_VSmu_Up"})
                .Define("weight_tauideff_VSmu_eta1p2to1p7_Up", [&](const float eta, const float weight_nom, const float weight_shifted) {
                            return ((abs(eta) >= 1.2) && (abs(eta) < 1.7)) ? weight_shifted : weight_nom;
                        }, {"eta_2", "weight_tauideff_VSmu_nominal", "weight_tauideff_VSmu_Up"})
                .Define("weight_tauideff_VSmu_eta1p7to2p3_Up", [&](const float eta, const float weight_nom, const float weight_shifted) {
                            return ((abs(eta) >= 1.7) && (abs(eta) < 2.3)) ? weight_shifted : weight_nom;
                        }, {"eta_2", "weight_tauideff_VSmu_nominal", "weight_tauideff_VSmu_Up"})
                // Bin by eta: down shifts
                .Define("weight_tauideff_VSmu_eta0to0p4_Down", [&](const float eta, const float weight_nom, const float weight_shifted) {
                            return ((abs(eta) >= 0) && (abs(eta) < 0.4)) ? weight_shifted : weight_nom;
                        }, {"eta_2", "weight_tauideff_VSmu_nominal", "weight_tauideff_VSmu_Down"})
                .Define("weight_tauideff_VSmu_eta0p4to0p8_Down", [&](const float eta, const float weight_nom, const float weight_shifted) {
                            return ((abs(eta) >= 0.4) && (abs(eta) < 0.8)) ? weight_shifted : weight_nom;
                        }, {"eta_2", "weight_tauideff_VSmu_nominal", "weight_tauideff_VSmu_Down"})
                .Define("weight_tauideff_VSmu_eta0p8to1p2_Down", [&](const float eta, const float weight_nom, const float weight_shifted) {
                            return ((abs(eta) >= 0.8) && (abs(eta) < 1.2)) ? weight_shifted : weight_nom;
                        }, {"eta_2", "weight_tauideff_VSmu_nominal", "weight_tauideff_VSmu_Down"})
                .Define("weight_tauideff_VSmu_eta1p2to1p7_Down", [&](const float eta, const float weight_nom, const float weight_shifted) {
                            return ((abs(eta) >= 1.2) && (abs(eta) < 1.7)) ? weight_shifted : weight_nom;
                        }, {"eta_2", "weight_tauideff_VSmu_nominal", "weight_tauideff_VSmu_Down"})
                .Define("weight_tauideff_VSmu_eta1p7to2p3_Down", [&](const float eta, const float weight_nom, const float weight_shifted) {
                            return ((abs(eta) >= 1.7) && (abs(eta) < 2.3)) ? weight_shifted : weight_nom;
                        }, {"eta_2", "weight_tauideff_VSmu_nominal", "weight_tauideff_VSmu_Down"});

    }
    else { // MC no shifts
      return df2;
    }
  }
  if (sConfig.isEmbedded()) { // initialize anyway
    return df.Define("weight_tauideff_VSmu_nominal", [=]() { return 1.0f; })
             .Define("weight_tauideff_VSmu_eta0to0p4_Up", [=]() { return 1.0f; })
             .Define("weight_tauideff_VSmu_eta0p4to0p8_Up", [=]() { return 1.0f; })
             .Define("weight_tauideff_VSmu_eta0p8to1p2_Up", [=]() { return 1.0f; })
             .Define("weight_tauideff_VSmu_eta1p2to1p7_Up", [=]() { return 1.0f; })
             .Define("weight_tauideff_VSmu_eta1p7to2p3_Up", [=]() { return 1.0f; })
             .Define("weight_tauideff_VSmu_eta0to0p4_Down", [=]() { return 1.0f; })
             .Define("weight_tauideff_VSmu_eta0p4to0p8_Down", [=]() { return 1.0f; })
             .Define("weight_tauideff_VSmu_eta0p8to1p2_Down", [=]() { return 1.0f; })
             .Define("weight_tauideff_VSmu_eta1p2to1p7_Down", [=]() { return 1.0f; })
             .Define("weight_tauideff_VSmu_eta1p7to2p3_Down", [=]() { return 1.0f; });
  }
  if (sConfig.isData()) {
    return df.Define("weight_tauideff_VSmu_nominal", [=]() { return 1.0f; });
  }

  return df;
}

/*******************************************************************/

#endif
