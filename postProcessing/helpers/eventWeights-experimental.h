// eventWeights.h
// The first function to call: defines the "weight" branch

#ifndef EVENT_WEIGHTS_EXPERIMENTAL_H_INCL
#define EVENT_WEIGHTS_EXPERIMENTAL_H_INCL

#include "fileIO.h"
#include "sampleConfig_class.h"

// Initialize the weight, reweighting if there are any extensions
// to the dataset. If nEntriesWithExt = nEntries, this should just return 1.0.

using namespace ROOT::VecOps;

/*******************************************************************/

float getLuminosity(std::string era) {

  float lumi = 1.0;

  if      (era == "2018") lumi = 59830.0;
  else if (era == "2017") lumi = 41480.0;
  else if (era == "2016preVFP") lumi = 19520.0;
  else if (era == "2016postVFP") lumi = 16810.0;

  return lumi;
}


/*******************************************************************/



float getInitialWeight(int isData,
                       int isMC,
                       int isEmbedded,
                       int isDYJets,
                       int isWJets,
                       float nEntriesOriginal, float nEntriesProcessedTotal,
                       float genWeight,
                       float genEventSumw,
                       float luminosity, float xsec) {

  // Initialize the return value
  float weight = 1.0;

  // Fraction of events processed. Ideally 1, but less than 1.0 if there were failed jobs at the
  // NanoAOD -> n-tuple stage, and in the case of Embedded samples, also possible
  // for jobs to fail at the MiniAOD -> NanoAOD level)
  float fracProcessed = (nEntriesProcessedTotal / nEntriesOriginal);

  // Go case by case
  if (isData) {
    weight = 1.0;
    weight /= fracProcessed;
  }
  else if (isEmbedded) {

    weight = genWeight;
    weight /= fracProcessed;
  }
  else if (isMC) {
    weight = 1.0;
    weight *= (nEntriesProcessedTotal * genWeight / genEventSumw);

    // std::cout << " weight at stage 1: " << weight << std::endl;
    weight /= fracProcessed;
    // std::cout << " weight at stage 2: " << weight << std::endl;

    // DYJets and WJets have a dedicated re-weighing module for luminosity-based weights
    // If the MC sample is neither of these, use the usual xsec formula
    if (!isDYJets && !isWJets) {
      weight *= (luminosity * xsec / nEntriesOriginal);
      // std::cout << " weight at stage 3: " << weight << std::endl;
    }
    else {
     // std::cout << ">>> eventWeights.h: DYJets or WJets, do not perform xsec-based weighing until dedicated reweighing module" << std::endl;
    }

  }

  // std::cout << ">>> eventWeights-experimental.h: "
  //       << "nEntriesProcessedTotal: " << nEntriesProcessedTotal << ", "
  //       << "genWeight: " << genWeight << ", "
  //       << "genEventSumw: " << genEventSumw << ", "
  //       << "nEntriesOriginal: " << nEntriesOriginal << ". "
  // 	    << "fraction of events processed: " << fracProcessed  << ". "
  // 	    << "weight " << weight << "."
  // 	    << "lumi, xsec: " << luminosity << ", " << xsec << std::endl;

  return weight;
}


/*******************************************************************/


template <typename T>
auto AddEventWeights_experimental(T &df, LUNA::sampleConfig_t &sConfig, TH1F* hEventsThisFile, TH1F* hEvents, TH1F* hRuns, float xsec, float nEntriesOriginal) {

  float nEntriesProcessedTotal = getNEntries(hEvents);
  float nEntriesThisFile = getNEntries(hEventsThisFile);

  float genEventSumwTotal = getGenEventSumw(hRuns);
  std::cout << ">>> eventWeights.h: " << sConfig.name() << ": in this file, there are " << nEntriesThisFile << " events. "
	    << "In total for this dataset, there are " << nEntriesProcessedTotal << " events processed, " << " out of " << nEntriesOriginal << " entries." << std::endl;
  std::cout << "Fraction of events processed: "
	    << (nEntriesProcessedTotal / nEntriesOriginal) << std::endl;
  if ((nEntriesProcessedTotal / nEntriesOriginal) < 0.95) {
    std::cout << "[WARNING]: Less than 95% of original dataset processed!" << std::endl;
  }
  if ((nEntriesProcessedTotal / nEntriesOriginal) < 0.99) {
    std::cout << "[WARNING]: Less than 99% of original dataset processed"  << std::endl;
  }
  if ((nEntriesProcessedTotal / nEntriesOriginal) > 1.00) {
    std::cout << "[ERROR:] Seemingly greater than 100% of original dataset processed - doesn't make sense! Check the config file." << std::endl;
  }

  int isData = sConfig.isData();
  int isEmbedded = sConfig.isEmbedded();
  int isMC = sConfig.isMC();
  int isDYJets = sConfig.isDY();
  int isWJets = sConfig.isWJets();
  float luminosity = getLuminosity(sConfig.era());

  // need these for reweightDYJets and reweightWJets
  auto df2 = df.Define("nEntriesProcessedTotal", [nEntriesProcessedTotal] () { return nEntriesProcessedTotal; })
               .Define("luminosity", [luminosity] () { return luminosity; });

  // TODO: eventually can delete this first "if" block when we fully phase out hard-baking xsec into the skim n-tuples
  if (HasExactBranch(df, "xsec")) {
    return df2.Redefine("xsec", [xsec] () { return xsec; })
              .Define("weight_initial_nominal", [isData, isMC, isEmbedded, isDYJets, isWJets, nEntriesOriginal, genEventSumwTotal, xsec]
                                                (float genWeight, float nEntriesProcessedTotal, float luminosity) {
                      return getInitialWeight(isData, isMC, isEmbedded, isDYJets, isWJets,
                                       nEntriesOriginal, nEntriesProcessedTotal,
                                        genWeight, genEventSumwTotal,
                                        luminosity, xsec); } , {"genWeight", "nEntriesProcessedTotal", "luminosity"});
  }
  else {
    return df2.Define("weight_initial_nominal", [isData, isMC, isEmbedded, isDYJets, isWJets, nEntriesOriginal, genEventSumwTotal, xsec]
                                                (float genWeight, float nEntriesProcessedTotal, float luminosity) {
                      return getInitialWeight(isData, isMC, isEmbedded, isDYJets, isWJets,
                                       nEntriesOriginal, nEntriesProcessedTotal,
                                        genWeight, genEventSumwTotal,
                                        luminosity, xsec); } , {"genWeight", "nEntriesProcessedTotal", "luminosity"});
  }

}




/*******************************************************************/


#endif
