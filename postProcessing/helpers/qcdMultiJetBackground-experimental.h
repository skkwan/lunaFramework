#ifndef QCD_MULTIJET_BACKGROUND_EXPERIMENTAL_H_INCL
#define QCD_MULTIJET_BACKGROUND_EXPERIMENTAL_H_INCL

#include <mutex>

#include "TH1.h"
#include "TF1.h"
#include "helperFunctions.h"
#include "sampleConfig_class.h"

/******************************************************************************/

float getQCDMultijetWeightOSSS_nominal(int channel, float deltaR, const TF1* osss_bjet, const int q_1, const int q_2) {
    float weight_osss = 1.0;
    // If same-sign event, get the weight
    if ((channel == Helper::em) && (q_1 * q_2 > 0)) {
        weight_osss = osss_bjet->Eval(deltaR);
    }
    return weight_osss;

}

float getQCDMultijetWeightCorrection_nominal(int channel, const TH2F *th2f_correction, float electron_pt, float muon_pt, const int q_1, const int q_2) {
    float weight_correction = 1.0;
    if ((channel == Helper::em) && (q_1 * q_2 > 0)) {     // If same-sign event, get the weight
        // Use FindFixBin which is a const method to be safe
        // x-axis is muon pT
        int globalBin = th2f_correction->FindFixBin(muon_pt, electron_pt);
        weight_correction = th2f_correction->GetBinContent(globalBin);
    }
    return weight_correction;
}

float getQCDMultijetWeightClosure_nominal(int channel, const TH2F *th2f_closureOS, float electron_pt, float muon_pt, const int q_1, const int q_2) {
    float weight_closure = 1.0;
    if ((channel == Helper::em) && (q_1 * q_2 > 0)) {     // If same-sign event, get the weight
        // x-axis is muon pT
        int globalBin = th2f_closureOS->FindFixBin(muon_pt, electron_pt);
        weight_closure = th2f_closureOS->GetBinContent(globalBin);
    }
    return weight_closure;
}

/*
 * For OSSS weight up/down, read the dedicated TF1s
 */
ROOT::RVecF getQCDMultijetWeightOSSS_variations(int channel, float deltaR, const TF1* osss_bjet_up, const TF1* osss_bjet_down, const int q_1, const int q_2) {
    float weight_osss_up = 1.0;
    float weight_osss_down = 1.0;
    if ((channel == Helper::em) && (q_1 * q_2 > 0)) {     // If same-sign event, get the weight
        weight_osss_up = osss_bjet_up->Eval(deltaR);
        weight_osss_down = osss_bjet_down->Eval(deltaR);
    }
    return ROOT::RVecF{weight_osss_up, weight_osss_down};
}
/******************************************************************************/

/*
 * Three scale factors necessary to extrapolate the QCD background for the e+mu channel from same-sign events
 * The pointers to the TF1 and TH2Fs are captured into the dataframe in the main function (postprocess-experimental.cxx)
 */
template <typename T>
auto GetQCDMultijetBackgroundScaleFactor_experimental(T &df, LUNA::sampleConfig_t &sConfig) {

    if (!sConfig.isSignal()) {
        auto df2 = df.Define("deltaR", Helper::compute_deltaR, {"eta_1", "eta_2", "phi_1", "phi_2"})
                     .Define("weight_osss_nominal",       getQCDMultijetWeightOSSS_nominal,       {"channel", "deltaR", "osss_bjet_ptr", "q_1", "q_2"})
                     .Define("weight_correction_nominal", getQCDMultijetWeightCorrection_nominal, {"channel", "th2f_correction_ptr", "pt_1_nominal", "pt_2_nominal", "q_1", "q_2"})
                     .Define("weight_closure_nominal",    getQCDMultijetWeightClosure_nominal,    {"channel", "th2f_closureOS_ptr",  "pt_1_nominal", "pt_2_nominal", "q_1", "q_2"});

        if (sConfig.doShifts()) {
            // Correction up/down: up is applying the weight twice, down is not applying the weight at all
            // weight_correction and weight_closure are = 1.0 for non-emu events, so 1.0^2 is still 1.0
            return df2.Define("variations_weight_correction", [](float nominal) { return ROOT::RVecF{nominal * nominal, (float) 1.0}; }, {"weight_correction_nominal"})
                      .Define("weight_correction_SScorrection_Up",   "variations_weight_correction[0]")
                      .Define("weight_correction_SScorrection_Down", "variations_weight_correction[1]")
            // Closure up/down: same logic
                      .Define("variations_weight_closure",    [](float nominal) { return ROOT::RVecF{nominal * nominal, (float) 1.0}; }, {"weight_closure_nominal"})
                      .Define("weight_closure_SSclosure_Up",   "variations_weight_closure[0]")
                      .Define("weight_closure_SSclosure_Down", "variations_weight_closure[1]")
            // Both up/down _CMS_SSboth2D_
                      .Define("variations_pair_SSboth2D", [](float nominal_correction, float nominal_closure) {
                                return ROOT::RVec<ROOT::RVecF>{{nominal_correction * nominal_correction, (float) 1.0}, {nominal_closure * nominal_closure, (float) 1.0}};
                            }, {"weight_correction_nominal", "weight_closure_nominal"})
                      .Define("weight_correction_SSboth2D_Up",   "variations_pair_SSboth2D[0][0]")
                      .Define("weight_correction_SSboth2D_Down", "variations_pair_SSboth2D[0][1]")
                      .Define("weight_closure_SSboth2D_Up",      "variations_pair_SSboth2D[1][0]")
                      .Define("weight_closure_SSboth2D_Down",    "variations_pair_SSboth2D[1][1]")
            // OS/SS up/down
                      .Define("variations_weight_osss", getQCDMultijetWeightOSSS_variations, {"channel", "deltaR", "osss_bjet_up_ptr", "osss_bjet_down_ptr", "q_1", "q_2"})
                      .Define("weight_osss_osss_Up",   "variations_weight_osss[0]")
                      .Define("weight_osss_osss_Down", "variations_weight_osss[1]")

            // Shifts due to the lepton energy scales of leg 1 and leg 2
                     .Define("weight_correction_es1Up", getQCDMultijetWeightCorrection_nominal, {"channel", "th2f_correction_ptr", "pt_1_es1Up", "pt_2_nominal", "q_1", "q_2"})
                     .Define("weight_correction_es2Up", getQCDMultijetWeightCorrection_nominal, {"channel", "th2f_correction_ptr", "pt_1_nominal", "pt_2_es2Up", "q_1", "q_2"})
                     .Define("weight_correction_es1Down", getQCDMultijetWeightCorrection_nominal, {"channel", "th2f_correction_ptr", "pt_1_es1Down", "pt_2_nominal", "q_1", "q_2"})
                     .Define("weight_correction_es2Down", getQCDMultijetWeightCorrection_nominal, {"channel", "th2f_correction_ptr", "pt_1_nominal", "pt_2_es2Down", "q_1", "q_2"})

                     .Define("weight_closure_es1Up", getQCDMultijetWeightClosure_nominal, {"channel", "th2f_closureOS_ptr", "pt_1_es1Up", "pt_2_nominal", "q_1", "q_2"})
                     .Define("weight_closure_es2Up", getQCDMultijetWeightClosure_nominal, {"channel", "th2f_closureOS_ptr", "pt_1_nominal", "pt_2_es2Up", "q_1", "q_2"})
                     .Define("weight_closure_es1Down", getQCDMultijetWeightClosure_nominal, {"channel", "th2f_closureOS_ptr", "pt_1_es1Down", "pt_2_nominal", "q_1", "q_2"})
                     .Define("weight_closure_es2Down", getQCDMultijetWeightClosure_nominal, {"channel", "th2f_closureOS_ptr", "pt_1_nominal", "pt_2_es2Down", "q_1", "q_2"});


        }
        else {
            return df2;
        }
    }
    else {  // signal sample
        auto df2 = df.Define("weight_osss_nominal",       []() { return 1.0f; })
                     .Define("weight_correction_nominal", []() { return 1.0f; })
                     .Define("weight_closure_nominal",    []() { return 1.0f; });
        if (sConfig.doShifts()) { // initialize these values anyway for signal sample to keep the n-tuples flat
            return df2.Define("weight_correction_SScorrection_Up", []() { return 1.0f; })
                    .Define("weight_correction_SScorrection_Down", []() { return 1.0f; })
                    .Define("weight_closure_SSclosure_Up", []() { return 1.0f; })
                    .Define("weight_closure_SSclosure_Down", []() { return 1.0f; })
                    .Define("weight_correction_SSboth2D_Up", []() { return 1.0f; })
                    .Define("weight_correction_SSboth2D_Down", []() { return 1.0f; })
                    .Define("weight_closure_SSboth2D_Up", []() { return 1.0f; })
                    .Define("weight_closure_SSboth2D_Down", []() { return 1.0f; })
                    .Define("weight_osss_osss_Up", []() { return 1.0f; })
                    .Define("weight_osss_osss_Down", []() { return 1.0f; })

                    .Define("weight_closure_es1Up",   []() { return 1.0f; })
                    .Define("weight_closure_es2Up",   []() { return 1.0f; })
                    .Define("weight_closure_es1Down", []() { return 1.0f; })
                    .Define("weight_closure_es2Down", []() { return 1.0f; })

                    .Define("weight_correction_es1Up",   []() { return 1.0f; })
                    .Define("weight_correction_es2Up",   []() { return 1.0f; })
                    .Define("weight_correction_es1Down", []() { return 1.0f; })
                    .Define("weight_correction_es2Down", []() { return 1.0f; });
        }
        else {
            return df2;
        }
    }

}

/******************************************************************************/


#endif
