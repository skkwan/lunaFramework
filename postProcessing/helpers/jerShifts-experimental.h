// Helper functions for b-tag jet counting with jet systematics.

#ifndef JER_SHIFS_EXPERIMENTAL_H_INCL
#define JER_SHIFS_EXPERIMENTAL_H_INCL

#include "computePhysicsVariables.h"
#include "cut.h"
#include "fileIO.h"
#include "finalVariables.h"
#include "sampleConfig_class.h"

#include <regex>

/******************************************************************************/

/*
 * B-jet counting: Defines hasLeadingBTagJet and hasSubleadBTagJet, with different
 * systematics hasLeadingBTagJet_{sysname}_{year}{Up/Down} since the b-jet pT systematics
 * may alter the b-jet counting.
 */

template <typename T>
auto GetJERShifts_experimental(T &df, LUNA::sampleConfig_t &sConfig){

  std::string year = std::to_string(sConfig.year());

  if (sConfig.isMC() && sConfig.doShifts()) {
    // Simultaneous .Vary() of the two b-tag jet's pT, met, and metphi
    // In the end we did not change met/metphi with the tau energy scale
    return df.Vary({"bpt_deepflavour_1", "bpt_deepflavour_2", "met", "metphi"}, "ROOT::RVec<ROOT::RVecF>{{bpt_deepflavour_JERUp_1, bpt_deepflavour_JERDown_1}, {bpt_deepflavour_JERUp_2, bpt_deepflavour_JERDown_2}, {met_JERUp, met_JERDown}, {metphi_JERUp, metphi_JERDown}}", {"up", "down"}, "JER")
             .Vary({"bpt_deepflavour_1", "bpt_deepflavour_2", "met", "metphi"}, "ROOT::RVec<ROOT::RVecF>{{bpt_deepflavour_JetAbsoluteUp_1, bpt_deepflavour_JetAbsoluteDown_1}, {bpt_deepflavour_JetAbsoluteUp_2, bpt_deepflavour_JetAbsoluteDown_2}, {met_JetAbsoluteUp, met_JetAbsoluteDown}, {metphi_JetAbsoluteUp, metphi_JetAbsoluteDown}}", {"up", "down"}, "JetAbsolute")
             .Vary({"bpt_deepflavour_1", "bpt_deepflavour_2", "met", "metphi"}, "ROOT::RVec<ROOT::RVecF>{{bpt_deepflavour_JetAbsoluteyearUp_1, bpt_deepflavour_JetAbsoluteyearDown_1}, {bpt_deepflavour_JetAbsoluteyearUp_2, bpt_deepflavour_JetAbsoluteyearDown_2}, {met_JetAbsoluteyearUp, met_JetAbsoluteyearDown}, {metphi_JetAbsoluteyearUp, metphi_JetAbsoluteyearDown}}", {"up", "down"}, "JetAbsoluteyear")
             .Vary({"bpt_deepflavour_1", "bpt_deepflavour_2", "met", "metphi"}, "ROOT::RVec<ROOT::RVecF>{{bpt_deepflavour_JetBBEC1Up_1, bpt_deepflavour_JetBBEC1Down_1}, {bpt_deepflavour_JetBBEC1Up_2, bpt_deepflavour_JetBBEC1Down_2}, {met_JetBBEC1Up, met_JetBBEC1Down}, {metphi_JetBBEC1Up, metphi_JetBBEC1Down}}", {"up", "down"}, "JetBBEC1")
             .Vary({"bpt_deepflavour_1", "bpt_deepflavour_2", "met", "metphi"}, "ROOT::RVec<ROOT::RVecF>{{bpt_deepflavour_JetBBEC1yearUp_1, bpt_deepflavour_JetBBEC1yearDown_1}, {bpt_deepflavour_JetBBEC1yearUp_2, bpt_deepflavour_JetBBEC1yearDown_2}, {met_JetBBEC1yearUp, met_JetBBEC1yearDown}, {metphi_JetBBEC1yearUp, metphi_JetBBEC1yearDown}}", {"up", "down"}, "JetBBEC1year")
             .Vary({"bpt_deepflavour_1", "bpt_deepflavour_2", "met", "metphi"}, "ROOT::RVec<ROOT::RVecF>{{bpt_deepflavour_JetEC2Up_1, bpt_deepflavour_JetEC2Down_1}, {bpt_deepflavour_JetEC2Up_2, bpt_deepflavour_JetEC2Down_2}, {met_JetEC2Up, met_JetEC2Down}, {metphi_JetEC2Up, metphi_JetEC2Down}}", {"up", "down"}, "JetEC2")
             .Vary({"bpt_deepflavour_1", "bpt_deepflavour_2", "met", "metphi"}, "ROOT::RVec<ROOT::RVecF>{{bpt_deepflavour_JetEC2yearUp_1, bpt_deepflavour_JetEC2yearDown_1}, {bpt_deepflavour_JetEC2yearUp_2, bpt_deepflavour_JetEC2yearDown_2}, {met_JetEC2yearUp, met_JetEC2yearDown}, {metphi_JetEC2yearUp, metphi_JetEC2yearDown}}", {"up", "down"}, "JetEC2year")
             .Vary({"bpt_deepflavour_1", "bpt_deepflavour_2", "met", "metphi"}, "ROOT::RVec<ROOT::RVecF>{{bpt_deepflavour_JetFlavorQCDUp_1, bpt_deepflavour_JetFlavorQCDDown_1}, {bpt_deepflavour_JetFlavorQCDUp_2, bpt_deepflavour_JetFlavorQCDDown_2}, {met_JetFlavorQCDUp, met_JetFlavorQCDDown}, {metphi_JetFlavorQCDUp, metphi_JetFlavorQCDDown}}", {"up", "down"}, "JetFlavorQCD")
             .Vary({"bpt_deepflavour_1", "bpt_deepflavour_2", "met", "metphi"}, "ROOT::RVec<ROOT::RVecF>{{bpt_deepflavour_JetHFUp_1, bpt_deepflavour_JetHFDown_1}, {bpt_deepflavour_JetHFUp_2, bpt_deepflavour_JetHFDown_2}, {met_JetHFUp, met_JetHFDown}, {metphi_JetHFUp, metphi_JetHFDown}}", {"up", "down"}, "JetHF")
             .Vary({"bpt_deepflavour_1", "bpt_deepflavour_2", "met", "metphi"}, "ROOT::RVec<ROOT::RVecF>{{bpt_deepflavour_JetHFyearUp_1, bpt_deepflavour_JetHFyearDown_1}, {bpt_deepflavour_JetHFyearUp_2, bpt_deepflavour_JetHFyearDown_2}, {met_JetHFyearUp, met_JetHFyearDown}, {metphi_JetHFyearUp, metphi_JetHFyearDown}}", {"up", "down"}, "JetHFyear")
             .Vary({"bpt_deepflavour_1", "bpt_deepflavour_2", "met", "metphi"}, "ROOT::RVec<ROOT::RVecF>{{bpt_deepflavour_JetRelativeBalUp_1, bpt_deepflavour_JetRelativeBalDown_1}, {bpt_deepflavour_JetRelativeBalUp_2, bpt_deepflavour_JetRelativeBalDown_2}, {met_JetRelativeBalUp, met_JetRelativeBalDown}, {metphi_JetRelativeBalUp, metphi_JetRelativeBalDown}}", {"up", "down"}, "JetRelativeBal")
             .Vary({"bpt_deepflavour_1", "bpt_deepflavour_2", "met", "metphi"}, "ROOT::RVec<ROOT::RVecF>{{bpt_deepflavour_JetRelativeSampleUp_1, bpt_deepflavour_JetRelativeSampleDown_1}, {bpt_deepflavour_JetRelativeSampleUp_2, bpt_deepflavour_JetRelativeSampleDown_2}, {met_JetRelativeSampleUp, met_JetRelativeSampleDown}, {metphi_JetRelativeSampleUp, metphi_JetRelativeSampleDown}}", {"up", "down"}, "JetRelativeSample")
             // The b-tagging efficiency calibration reader takes a std::string argument for the JER systematics, so we have to .Vary() a std::string. Note that the variation name has to match what is above
             .Define("btagCalibReader_string", []() { return (std::string) "central"; })
             .Vary("btagCalibReader_string", "ROOT::RVec<std::string>{\"up_jesAbsolute\",                \"down_jesAbsolute\"}",                {"up", "down"}, "JetAbsolute")
             .Vary("btagCalibReader_string", "ROOT::RVec<std::string>{\"up_jesAbsolute_"+year+"\",       \"down_jesAbsolute_"+year+"\"}",       {"up", "down"}, "JetAbsoluteyear")  // the calib reader needs "up_jesAbsolute_2018", and C++11 doesn't support std::format
             .Vary("btagCalibReader_string", "ROOT::RVec<std::string>{\"up_jesBBEC1\",                   \"down_jesBBEC1\"}",                   {"up", "down"}, "JetBBEC1")
             .Vary("btagCalibReader_string", "ROOT::RVec<std::string>{\"up_jesBBEC1_"+year+"\",          \"down_jesBBEC1_"+year+"\"}",          {"up", "down"}, "JetBBEC1year")
             .Vary("btagCalibReader_string", "ROOT::RVec<std::string>{\"up_jesEC2\",                     \"down_jesEC2\"}",                     {"up", "down"}, "JetEC2")
             .Vary("btagCalibReader_string", "ROOT::RVec<std::string>{\"up_jesEC2_"+year+"\",            \"down_jesEC2_"+year+"\"}",            {"up", "down"}, "JetEC2year")
             .Vary("btagCalibReader_string", "ROOT::RVec<std::string>{\"up_jesFlavorQCD\",               \"down_jesFlavorQCD\"}",               {"up", "down"}, "JetFlavorQCD")
             .Vary("btagCalibReader_string", "ROOT::RVec<std::string>{\"up_jesHF\",                      \"down_jesHF\"}",                      {"up", "down"}, "JetHF")
             .Vary("btagCalibReader_string", "ROOT::RVec<std::string>{\"up_jesHF_"+year+"\",             \"down_jesHF_"+year+"\"}",             {"up", "down"}, "JetHFyear")
             .Vary("btagCalibReader_string", "ROOT::RVec<std::string>{\"up_jesRelativeBal\",             \"down_jesRelativeBal\"}",             {"up", "down"}, "JetRelativeBal")
             .Vary("btagCalibReader_string", "ROOT::RVec<std::string>{\"up_jesRelativeSample_"+year+"\", \"down_jesRelativeSample_"+year+"\"}", {"up", "down"}, "JetRelativeSample");

  }
  else {
    return df;
  }

}

/******************************************************************************/

#endif
