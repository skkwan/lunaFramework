#ifndef MUON_ID_ISO_EXPERIMENTAL_H
#define MUON_ID_ISO_EXPERIMENTAL_H

#include "correction.h"
#include "helperFunctions.h"
#include "sampleConfig_class.h"

using correction::CorrectionSet;

/*******************************************************************/

/*
 * Weight for muon ID and isolation
 */

template <typename T>
auto GetMuonIdIsoWeight(T &df, LUNA::sampleConfig_t &sConfig, TString jsonDir, TString jsonDirEmb) {

  // MC: any year
  if (sConfig.isMC()) {

    TString cset_dir = TString::Format("%sPOG/MUO/%s_UL/muon_Z.json", jsonDir.Data(), sConfig.era().c_str());
    auto cset = CorrectionSet::from_file(cset_dir.Data());
    auto cset_muonID = cset->at("NUM_MediumID_DEN_genTracks");
    auto cset_muonIso = cset->at("NUM_TightRelIso_DEN_MediumID");

    auto df2 = df.Define("weight_muonIdIso_nominal", [cset_muonID, cset_muonIso] (const int channel, const float eta_1, const float pt_1, const float eta_2, const float pt_2) {
                      if (channel == Helper::mt) { // muon is leg 1
                        return (float) (cset_muonID->evaluate({abs(eta_1), std::max(15.0f, pt_1),  "nominal"}) * cset_muonIso->evaluate({abs(eta_1), std::max(15.0f, pt_1),  "nominal"}));
                      }
                      else if (channel == Helper::em) {  // muon is leg 2
                        return (float) (cset_muonID->evaluate({abs(eta_2), std::max(15.0f, pt_2),  "nominal"}) * cset_muonIso->evaluate({abs(eta_2), std::max(15.0f, pt_2),  "nominal"}));
                      }
                      return 1.0f;
                    }, {"channel", "eta_1", "pt_1", "eta_2", "pt_2"});
    if (sConfig.doShifts()) {
      return df2.Define("weight_muonIdIso_mt_es1Up", [cset_muonID, cset_muonIso] (const int channel, const float eta_1, const float pt_1, const float eta_2, const float pt_2, const float weight_muonIdIso_nominal) {
                      if (channel == Helper::mt) { // muon is leg 1
                        return (float) (cset_muonID->evaluate({abs(eta_1), std::max(15.0f, pt_1),  "nominal"}) * cset_muonIso->evaluate({abs(eta_1), std::max(15.0f, pt_1),  "nominal"}));
                      }
                      else if (channel == Helper::em) { // emu muon is leg 2
                        return (float) (cset_muonID->evaluate({abs(eta_2), std::max(15.0f, pt_2),  "nominal"}) * cset_muonIso->evaluate({abs(eta_2), std::max(15.0f, pt_2),  "nominal"}));
                      }
                      return weight_muonIdIso_nominal;
                    }, {"channel", "eta_1", "pt_1_es1Up", "eta_2", "pt_2_es2Up", "weight_muonIdIso_nominal"})
                .Define("weight_muonIdIso_mt_es1Down", [cset_muonID, cset_muonIso] (const int channel, const float eta_1, const float pt_1, const float weight_muonIdIso_nominal) {
                      if (channel == Helper::mt) { // muon is leg 1
                        return (float) (cset_muonID->evaluate({abs(eta_1), std::max(15.0f, pt_1),  "nominal"}) * cset_muonIso->evaluate({abs(eta_1), std::max(15.0f, pt_1),  "nominal"}));
                      }
                      return weight_muonIdIso_nominal;
                    }, {"channel", "eta_1", "pt_1_es1Down", "weight_muonIdIso_nominal"})
              // emu channel: muon is leg 2
                .Define("weight_muonIdIso_em_es2Up", [cset_muonID, cset_muonIso] (const int channel, const float eta_2, const float pt_2, const float weight_muonIdIso_nominal) {
                      if (channel == Helper::em) { // muon is leg 2
                        return (float) (cset_muonID->evaluate({abs(eta_2), std::max(15.0f, pt_2),  "nominal"}) * cset_muonIso->evaluate({abs(eta_2), std::max(15.0f, pt_2),  "nominal"}));
                      }
                      return weight_muonIdIso_nominal;
                    }, {"channel", "eta_2", "pt_2_es2Up", "weight_muonIdIso_nominal"})
                .Define("weight_muonIdIso_em_es2Down", [cset_muonID, cset_muonIso] (const int channel, const float eta_2, const float pt_2, const float weight_muonIdIso_nominal) {
                      if (channel == Helper::em) { // muon is leg 2
                        return (float) (cset_muonID->evaluate({abs(eta_2), std::max(15.0f, pt_2),  "nominal"}) * cset_muonIso->evaluate({abs(eta_2), std::max(15.0f, pt_2),  "nominal"}));
                      }
                      return weight_muonIdIso_nominal;
                    }, {"channel", "eta_2", "pt_2_es2Down", "weight_muonIdIso_nominal"});

    }
    else { // MC no shifts
      return df2;
    }
  }
  // Embedded
  else if (sConfig.isEmbedded()) {
    TString cset_dir = TString::Format("%smuon_%sUL.json", jsonDirEmb.Data(), sConfig.era().c_str());
    auto cset = CorrectionSet::from_file(cset_dir.Data());
    auto cset_muonID = cset->at("ID_pt_eta_bins");
    auto cset_muonIso = cset->at("Iso_pt_eta_bins");

    auto df2 = df.Define("weight_muonIdIso_nominal", [cset_muonID, cset_muonIso] (const int channel, const float eta_1, const float pt_1, const float eta_2, const float pt_2) {
                      if (channel == Helper::mt) { // muon is leg 1
                        return (float) (cset_muonID->evaluate({pt_1, abs(eta_1), "emb"}) * cset_muonIso->evaluate({pt_1, abs(eta_1), "emb"}));
                      }
                      else if (channel == Helper::em) {  // muon is leg 2
                        return (float) (cset_muonID->evaluate({pt_2, abs(eta_2), "emb"}) * cset_muonIso->evaluate({pt_2, abs(eta_2), "emb"}));
                      }
                      return 1.0f;
                    }, {"channel", "eta_1", "pt_1", "eta_2", "pt_2"});
    if (sConfig.doShifts()) {
      return df2.Define("weight_muonIdIso_mt_es1Up", [cset_muonID, cset_muonIso] (const int channel, const float eta_1, const float pt_1, const float weight_muonIdIso_nominal) {
                      if (channel == Helper::mt) { // muon is leg 1
                        return (float) (cset_muonID->evaluate({pt_1, abs(eta_1), "emb"}) * cset_muonIso->evaluate({pt_1, abs(eta_1), "emb"}));
                      }
                      return weight_muonIdIso_nominal;
                    }, {"channel", "eta_1", "pt_1_es1Up", "weight_muonIdIso_nominal"})
                .Define("weight_muonIdIso_mt_es1Down", [cset_muonID, cset_muonIso] (const int channel, const float eta_1, const float pt_1, const float weight_muonIdIso_nominal) {
                      if (channel == Helper::mt) { // muon is leg 1
                        return (float) (cset_muonID->evaluate({pt_1, abs(eta_1), "emb"}) * cset_muonIso->evaluate({pt_1, abs(eta_1), "emb"}));
                      }
                      return weight_muonIdIso_nominal;
                    }, {"channel", "eta_1", "pt_1_es1Down", "weight_muonIdIso_nominal"})
              // emu channel: muon is leg 2
                .Define("weight_muonIdIso_em_es2Up", [cset_muonID, cset_muonIso] (const int channel, const float eta_2, const float pt_2, const float weight_muonIdIso_nominal) {
                      if (channel == Helper::em) { // muon is leg 2
                        return (float) (cset_muonID->evaluate({pt_2, abs(eta_2), "emb"}) * cset_muonIso->evaluate({pt_2, abs(eta_2), "emb"}));
                      }
                      return weight_muonIdIso_nominal;
                    }, {"channel", "eta_2", "pt_2_es2Up", "weight_muonIdIso_nominal"})
                .Define("weight_muonIdIso_em_es2Down", [cset_muonID, cset_muonIso] (const int channel, const float eta_2, const float pt_2, const float weight_muonIdIso_nominal) {
                      if (channel == Helper::em) { // muon is leg 2
                        return (float) (cset_muonID->evaluate({pt_2, abs(eta_2), "emb"}) * cset_muonIso->evaluate({pt_2, abs(eta_2), "emb"}));
                      }
                      return weight_muonIdIso_nominal;
                    }, {"channel", "eta_2", "pt_2_es2Down", "weight_muonIdIso_nominal"});

    }
    else { // Embedded no shifts
      return df2;
    }
  }
  else if (sConfig.isData()) {
    return df.Define("weight_muonIdIso_nominal", []() { return 1.0f; });
  }

  return df;
}

/*******************************************************************/

#endif
