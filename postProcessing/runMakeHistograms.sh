# runMakeHistograms.sh

# 1. `runPostProcess.sh`: make n-tuples with tau ES, tau ID, trigger weights, etc. from skim-level n-tuples.
# 2. [To-do: SVFIT, DNN..]
# 3. [THIS STEP:] `runMakeHistograms.sh`: make histograms (one per DAS dataset) from processed n-tuples.
# 4. `runBkgEstimation.sh`: make final set of histograms (one per histogram in data/MC) from step #3.
# 5. `runStackPlots.sh`: make data/MC plots from the final set of histograms.


source /cvmfs/sft.cern.ch/lcg/views/setupViews.sh LCG_96bpython3 x86_64-centos7-gcc62-opt


bash makeHistograms.sh COMPILE 2018 finalHistsConfig/finalHists-2018.csv LXPLUS RUNFULL
#bash makeHistograms.sh COMPILE 2018 finalHistsConfig/finalHists-2018-bare-NanoAOD.csv LXPLUS RUNFULL


#bash postprocess.sh COMPILE 2017 postProcHists-2017.csv LXPLUS RUNFULL
#bash postprocess.sh COMPILE 2017 postProcHists-2017-fix-Jun-30-2021-03h07m.csv LXPLUS RUNFULL
