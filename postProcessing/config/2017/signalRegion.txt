(q_1*q_2 < 0) && (iso_1 < 0.15) &&
passExtraElectronVeto && passExtraMuonVeto && passDiMuonVeto &&
(passMuTauFilter > 0) &&
(mtMET_1 < 50) &&
(mtMET_2 < 60) &&
byTightDeepVSmu_2 &&
byVLooseDeepVSe_2
