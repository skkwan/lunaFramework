"""
groups.py

Included in backgroundEstimation.py.
Lists which groups are MC/embedded/data_obs. See sampelsInGroups.py for which DAS dataset names go into each one of these
"""

dictGroups = {
    "2018": {
        "mutau": {
            "MC": [
                "ZJ",
                "ttbar",
                "ST",
                "VV",
                "WJ",
                "ggh_htt",
                "ggh_hww",
                "qqh_htt",
                "qqh_hww",
                "Zh_htt",
                "Zh_hww",
                "Wh_htt",
                "Wh_hww",
                "tth",
            ],
            "embedded": ["embedded"],
            "data_obs": ["data_obs"],
            "fake": ["fake"],
            "signal": ["signal"],
        },
        "etau": {
            "MC": [
                "ZJ",
                "ttbar",
                "ST",
                "VV",
                "WJ",
                "ggh_htt",
                "ggh_hww",
                "qqh_htt",
                "qqh_hww",
                "Zh_htt",
                "Zh_hww",
                "Wh_htt",
                "Wh_hww",
                "tth",
            ],
            "embedded": ["embedded"],
            "data_obs": ["data_obs"],
            "fake": ["fake"],
            "signal": ["signal"],
        },
    }
}
