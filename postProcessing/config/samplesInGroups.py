"""
samplesInGroups.py

Lists the samples that go into each histogram in each year's datacards.
Included in backgroundEstimation.py.
"""

dictSamplesInGroups = {
    "2018": {
        "mutau": {
            "ZJ": [
                "DYJetsToLL_M-50",
                "DY1JetsToLL",
                "DY2JetsToLL",
                "DY3JetsToLL",
                "DY4JetsToLL",
            ],
            "ttbar": ["TTToHadronic", "TTToSemiLeptonic", "TTTo2L2Nu"],
            "ST": [
                "ST_tW_antitop",
                "ST_tW_top",
                "ST_t-channel_top",
                "ST_t-channel_antitop",
            ],
            "VV": ["VVTo2L2Nu", "WZTo3LNu", "WZTo2Q2L", "ZZTo2Q2L", "ZZTo4L"],
            "WJ": [
                "WJetsToLNu",
                "W1JetsToLNu",
                "W2JetsToLNu",
                "W3JetsToLNu",
                "W4JetsToLNu",
            ],
            "ggh_htt": ["GluGluHToTauTau"],
            "ggh_hww": ["GluGluHToWWTo2L2Nu"],
            "qqh_htt": ["VBFHToTauTau"],
            "qqh_hww": ["VBFHToWWTo2L2Nu"],
            "Zh_htt": ["ZHToTauTau"],
            "Zh_hww": ["GluGluZH_HToWWTo2L2Nu", "GluGluZH_HToWW_ZTo2L"],
            "Wh_htt": ["WminusHToTauTau", "WplusHToTauTau"],
            "Wh_hww": ["HWminusJ_HToWW", "HWplusJ_HToWW", "HZJ_HToWW"],
            "tth": ["ttHToNonbb", "ttHTobb"],
            "embedded": [
                "Embedded-Run2018A-MuTau",
                "Embedded-Run2018B-MuTau",
                "Embedded-Run2018C-MuTau",
                "Embedded-Run2018D-MuTau",
            ],
            "data_obs": [
                "SingleMuon-Run2018A",
                "SingleMuon-Run2018B",
                "SingleMuon-Run2018C",
                "SingleMuon-Run2018D",
            ],
            "signal": [
                "SUSYVBFHToAA_AToBB_AToTauTau_M-45",
                "SUSYGluGluToHToAA_AToBB_AToTauTau_M-45",
            ],
        },
        "etau": {
            "ZJ": [
                "DYJetsToLL_M-50",
                "DY1JetsToLL",
                "DY2JetsToLL",
                "DY3JetsToLL",
                "DY4JetsToLL",
            ],
            "ttbar": ["TTToHadronic", "TTToSemiLeptonic", "TTTo2L2Nu"],
            "ST": [
                "ST_tW_antitop",
                "ST_tW_top",
                "ST_t-channel_top",
                "ST_t-channel_antitop",
            ],
            "VV": ["VVTo2L2Nu", "WZTo3LNu", "WZTo2Q2L", "ZZTo2Q2L", "ZZTo4L"],
            "WJ": [
                "WJetsToLNu",
                "W1JetsToLNu",
                "W2JetsToLNu",
                "W3JetsToLNu",
                "W4JetsToLNu",
            ],
            "ggh_htt": ["GluGluHToTauTau"],
            "ggh_hww": ["GluGluHToWWTo2L2Nu"],
            "qqh_htt": ["VBFHToTauTau"],
            "qqh_hww": ["VBFHToWWTo2L2Nu"],
            "Zh_htt": ["ZHToTauTau"],
            "Zh_hww": ["GluGluZH_HToWWTo2L2Nu", "GluGluZH_HToWW_ZTo2L"],
            "Wh_htt": ["WminusHToTauTau", "WplusHToTauTau"],
            "Wh_hww": ["HWminusJ_HToWW", "HWplusJ_HToWW", "HZJ_HToWW"],
            "tth": ["ttHToNonbb", "ttHTobb"],
            "embedded": [
                "Embedded-Run2018A-ElTau",
                "Embedded-Run2018B-ElTau",
                "Embedded-Run2018C-ElTau",
                "Embedded-Run2018D-ElTau",
            ],
            "data_obs": [
                "EGamma-Run2018A",
                "EGamma-Run2018B",
                "EGamma-Run2018C",
                "EGamma-Run2018D",
            ],
            "signal": [
                "SUSYVBFHToAA_AToBB_AToTauTau_M-45",
                "SUSYGluGluToHToAA_AToBB_AToTauTau_M-45",
            ],
        },
    }
}
