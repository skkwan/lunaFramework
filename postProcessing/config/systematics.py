"""
systematics.py

Which systematics apply to {MC, data_obs, embed}?

Included in backgroundEstimation.py.
backgroundEstimation.py will expect to find (in the input file) histograms for each of these systematics.
"""

dictSystematics = {
    "2018": {
        "mutau": {
            "MC": [  # Muon ES
                "CMS_muES_eta0to1p2",
                "CMS_muES_eta1p2to2p1",
                "CMS_muES_eta2p1to2p4",
                # Tau ES
                "CMS_TES_dm0",
                "CMS_TES_dm1",
                "CMS_TES_dm10",
                "CMS_TES_dm11",
                "CMS_eleTES_dm0",
                "CMS_eleTES_dm1",
                "CMS_muTES_dm0",
                "CMS_muTES_dm1",
                # JER
                "CMS_JER",
                "CMS_JetAbsolute",
                "CMS_JetBBEC1",
                "CMS_JetEC2",
                "CMS_JetFlavorQCD",
                "CMS_JetHF",
                "CMS_JetRelativeBal",
                "CMS_JetAbsoluteyear",
                "CMS_JetBBEC1year",
                "CMS_JetEC2year",
                "CMS_JetHFyear",
                "CMS_JetRelativeSample",
                # Recoil corrections and UES uncertainties
                # "CMS_UES"
            ],
            "data_obs": [],
            "embedded": [  # Muon ES
                "CMS_EMB_muES_eta0to1p2",
                "CMS_EMB_muES_eta1p2to2p1",
                "CMS_EMB_muES_eta2p1to2p4",
                # Tau ES
                "CMS_EMB_TES_dm0",
                "CMS_EMB_TES_dm1",
                "CMS_EMB_TES_dm10",
                "CMS_EMB_TES_dm11",
            ],
            "fake": [],
            "signal": [],
        },
        "etau": {
            "MC": [  # Muon ES
                "CMS_muES_eta0to1p2",
                "CMS_muES_eta1p2to2p1",
                "CMS_muES_eta2p1to2p4",
                # Tau ES
                "CMS_TES_dm0",
                "CMS_TES_dm1",
                "CMS_TES_dm10",
                "CMS_TES_dm11",
                "CMS_eleTES_dm0",
                "CMS_eleTES_dm1",
                "CMS_muTES_dm0",
                "CMS_muTES_dm1",
                # JER
                "CMS_JER",
                "CMS_JetAbsolute",
                "CMS_JetBBEC1",
                "CMS_JetEC2",
                "CMS_JetFlavorQCD",
                "CMS_JetHF",
                "CMS_JetRelativeBal",
                "CMS_JetAbsoluteyear",
                "CMS_JetBBEC1year",
                "CMS_JetEC2year",
                "CMS_JetHFyear",
                "CMS_JetRelativeSample",
                # Recoil corrections and UES uncertainties
                # "CMS_UES"
            ],
            "data_obs": [],
            "embedded": [  # Muon ES
                "CMS_EMB_muES_eta0to1p2",
                "CMS_EMB_muES_eta1p2to2p1",
                "CMS_EMB_muES_eta2p1to2p4",
                # Tau ES
                "CMS_EMB_TES_dm0",
                "CMS_EMB_TES_dm1",
                "CMS_EMB_TES_dm10",
                "CMS_EMB_TES_dm11",
            ],
            "fake": [],
            "signal": [],
        },
    }
}
