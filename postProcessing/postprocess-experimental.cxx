/*
 * Implementation of the postprocessing step of the analysis
 * Post-processing takes skimmed n-tuples and n-tuples with all necessary branches.
 */

#include "ROOT/RDataFrame.hxx"

#include "ROOT/RVec.hxx"
#include <ROOT/RDF/RResultMap.hxx>
#include <ROOT/RDFHelpers.hxx>   // need this for VariationsFor()
#include "correction.h"

#include "Math/Vector4D.h"
#include "RooWorkspace.h"
#include "TChain.h"
#include "TGraphAsymmErrors.h"
#include "TStopwatch.h"

#include <stdlib.h>
#include <iostream>
#include <cmath>

#include "helpers/alias.h"
#include "helpers/btagJetCounting-experimental.h"
#include "helpers/btagEffScaleFactors-experimental.h"
#include "helpers/computePhysicsVariables.h"
#include "helpers/cut-experimental.h"
#include "helpers/eleIdIso-experimental.h"
#include "helpers/embeddedSelectionEfficiencies.h"
#include "helpers/embeddedTauTracking-experimental.h"
#include "helpers/eventWeights-experimental.h"
#include "helpers/fileIO.h"
#include "helpers/getRange.h"
#include "helpers/helperFunctions.h"
#include "helpers/jetFakeBackground-experimental.h"
#include "helpers/jetFakeBackground-crossTrigger-experimental.h"
#include "helpers/jerShifts-experimental.h"
#include "helpers/muonIdIso-experimental.h"
#include "helpers/pileupReweighing.h"
#include "helpers/qcdMultiJetBackground-experimental.h"
#include "helpers/ranges.h"
#include "helpers/reweightDYJets-experimental.h"
#include "helpers/reweightWJets-experimental.h"
#include "helpers/sampleConfig_class.h"
#include "helpers/tauIDeffVsEleScaleFactors-experimental.h"
#include "helpers/tauIDeffVsMuScaleFactors-experimental.h"
#include "helpers/tauIDScaleFactors-experimental.h"
#include "helpers/tauIDwpScaleFactor-experimental.h"
#include "helpers/topPtReweighing-experimental.h"
#include "helpers/triggerDecisionsTauTau-experimental.h"
#include "helpers/triggerEfficienciesTauTau-experimental.h"
#include "helpers/zPtReweighing-experimental.h"

using correction::CorrectionSet;

/*****************************************************/

template <typename T>
auto PerformAnalysis(T &df, LUNA::sampleConfig_t &sConfig, TString &jsonDir, TString &jsonDirEmb, std::map<std::string, std::string> &fileMap,
                    int isLocalJob, TGraphAsymmErrors* ptrTGraph_medium_vsVVVL) {

    // Each function needs to be flexible to handle all channels
    auto df_with_leading_pair = GetApplyTauTauTriggerDecisions_experimental(df, sConfig);

    // Calculate weights and calculate number of b-tag jets
    auto df_2 = GetTauIDShifts_experimental(df_with_leading_pair, sConfig, jsonDir);
    auto df_3 = GetEFakingTauhWeightShifts_experimental(df_2, sConfig, jsonDir);
    auto df_4 = GetMuFakingTauhWeightShifts_experimental(df_3, sConfig, jsonDir);
    auto df_5 = GetBTagJetCountingShifts_experimental(df_4, sConfig);
    auto df_6 = GetBTagEfficiencyWeightShifts_experimental(df_5, sConfig, jsonDir);
    auto df_7 = GetZPtReweighing_experimental(df_6, sConfig);
    auto df_8 = GetTopPtReweighing_experimental(df_7, sConfig);
    auto df_9 = GetDYJetsWeight_experimental(df_8, sConfig, fileMap, isLocalJob);
    auto df_10 = GetWJetsWeight_experimental(df_9, sConfig, fileMap, isLocalJob);

    auto df_11 = GetEmbeddedTauTrackingEfficiency_experimental(df_10, sConfig);
    auto df_12 = GetEmbeddedSelectionEfficiencies(df_11, sConfig, jsonDirEmb);

    auto df_13 = GetJetFakeBackgroundScaleFactor_experimental(df_12, sConfig, ptrTGraph_medium_vsVVVL);
    auto df_14 = GetJetFakeBackground_CrossTriggerShifts_experimental(df_13, sConfig);

    auto df_15 = GetTauTauTriggerEfficiencyShifts_experimental(df_14, sConfig, jsonDirEmb);
    auto df_16 = GetTauIDwpShifts_experimental(df_15, sConfig);

    auto df_17 = GetQCDMultijetBackgroundScaleFactor_experimental(df_16, sConfig);
    auto df_18 = GetPileupWeight(df_17, sConfig, jsonDir);

    auto df_19 = GetMuonIdIsoWeight(df_18, sConfig, jsonDir, jsonDirEmb);
    auto df_20 = GetEleIdIsoWeight(df_19, sConfig, jsonDir, jsonDirEmb);

    auto df_21 = AliasSVFitVariables(df_20, sConfig);
    return df_21;
}
/*****************************************************/

/*
 * Create histograms from skimmed n-tuples with associated DNN scores.
 */
int main(int argc, char **argv) {

  // for (int count{ 0 }; count < argc; ++count) {
  //      std::cout << count << " " << argv[count] << '\n';
  //}

    int nArgc_withDNN = 17;
    int nArgc_withoutDNN = 16;
    if (! ((argc == nArgc_withDNN) || (argc == nArgc_withoutDNN)) ) {
        std::cout << argc << std::endl;
        std::cout << "[Error:] Incorrect number of arguments -- Use executable with following arguments: ./postprocess sampleInfoPath sampleFullPath process configDir outputHist xsec nEntriesInOriginalDataset pathToCsv doMuTau doETau doEMu doShifts doCategories doAllVariables isLocalJob dnnFull"
                << ", where the last one is optional" << std::endl;
        std::cout << "[Error:] Found " << argc << " arguments instead of " << nArgc_withDNN << " or " << nArgc_withoutDNN << std::endl;
        return -1;
    }
    std::cout << gROOT->GetVersion() << std::endl;

    std::string sampleInfoDir = argv[1];
    std::cout << ">>> Path to info file for the sample's TOTAL hEvents and hRuns TTree: " << sampleInfoDir << std::endl;

    std::string input = argv[2];
    std::cout << ">>> Path to the input file (will clean this to just the file name if running on batch): " << input << std::endl;

    std::string process = argv[3];
    std::cout << ">>> Physics process name: " << process << std::endl;

    std::string configDir = argv[4];
    std::cout << ">>> Configuration directory: " << configDir << std::endl;

    std::string outputFile = argv[5];
    std::cout << ">>> Output file: " << outputFile << std::endl;

    float xsec = atof(argv[6]);
    std::cout << ">>> Cross-section: " << xsec << std::endl;

    float nEntriesOriginal = atof(argv[7]);
    std::cout << ">>> Number of entries in un-skimmed dataset: " << nEntriesOriginal << std::endl;

    std::string sampleListDir = argv[8];
    std::cout << ">>> Path to list of samples .csv: " << sampleListDir << std::endl;

    int doMuTau = (int) atoi(argv[9]);
    std::cout << ">>> Do mutau channel (0 is False): " << doMuTau << std::endl;

    int doETau = (int) atoi(argv[10]);
    std::cout << ">>> Do etau channel (0 is False): " << doETau << std::endl;

    int doEMu = (int) atoi(argv[11]);
    std::cout << ">>> Do emu channel (0 is False): " << doEMu << std::endl;

    int doShifts = (int) atoi(argv[12]);
    std::cout << ">>> Do shifts (0 is False): " << doShifts << std::endl;

    int doCategories = (int) atoi(argv[13]);
    std::cout << ">>> Do categories (0 is False): " << doCategories << std::endl;

    int doAllVariables = (int) atoi(argv[14]);
    std::cout << ">>> Do all variables (0 is False, m_vis only, 1: all variables): " << doAllVariables << std::endl;

    int isLocalJob = (int) atoi(argv[15]);
    std::cout << ">>> Is a local job (0 is False): " << isLocalJob << std::endl;


    std::string dnnDir = "";
    int hasDNN = false;
    if (argc == nArgc_withDNN) {
        dnnDir = argv[16];
        hasDNN = true;
        std::cout << ">>> Path to file with DNN friend tree: " << dnnDir << std::endl;
    }

    TStopwatch time;
    time.Start();
    ROOT::EnableImplicitMT(); // Tell ROOT we want to go parallel - only compatible if Range is NOT used

    //*****************************************//
    // Prepare input skim and DNN n-tuples
    //*****************************************//
    // Get the input TTree
    TFile *fInput = TFile::Open(input.c_str());
    TString channelName, treeName, channelShorthand;

    if (doMuTau) {
        channelName = "mutau";
        treeName = "mutau_tree";
        channelShorthand = "mt";
    }
    else if (doETau) {
        channelName = "etau";
        treeName = "etau_tree";
        channelShorthand = "et";
    }
    else if (doEMu) {
        channelName = "emu";
        treeName = "emu_tree";
        channelShorthand = "em";
    }

    TTree *tMain = (TTree*)fInput->Get(treeName);

    TFile *fInfo = TFile::Open(sampleInfoDir.c_str());

    if (fInput == 0) {
        std::cerr << "[ERROR!] The input file " << input << " was not found" << std::endl;
        return 1;
    }
    if (fInfo == 0) {
        std::cerr << "[ERROR!] The input file " << sampleInfoDir << " was not found" << std::endl;
        return 1;
    }
    if (tMain == 0) {
        std::cerr << "[ERROR!] The input TTree " << treeName << " from " << input << " was not found. This could be expected if this file has no events in this channel. Terminating..." << std::endl;
        return 0;
    }

    // // Get DNN as a friend tree, if the argument was provided
    // TFile *fDNN;
    // TTree *tDNN;
    // if (hasDNN) {
    //     fDNN = TFile::Open(dnnDir.c_str());
    //     if (fDNN == 0) {
    //         std::cerr << "[ERROR!] The input file " << dnnDir << " was not found" << std::endl;
    //         return 1;
    //     }
    //     tDNN = (TTree*)fDNN->Get("event_tree_NN");
    //     if (tDNN == 0) {
    //         std::cerr << "[ERROR!] The input event_tree_NN from " << dnnDir << " was not found" << std::endl;
    //         return 1;
    //     }
    //     tMain->AddFriend(tDNN, "dnn");

    // }


    ROOT::RDataFrame dfInput(*tMain);
    const auto numEvents = *dfInput.Count();

    std::cout << ">>> Number of events in input tree " << treeName << ": " << numEvents << std::endl;

    // hRuns and hEvents histogram from the input file
    TH1F *hEvents = (TH1F*)fInfo->Get("hEvents");
    TH1F *hRuns   = (TH1F*)fInfo->Get("hRuns");
    if ((hEvents == 0) || (hRuns == 0)) {
        std::cout << "[ERROR:] hEvents or hRuns tree NOT FOUND in " << sampleInfoDir << ", EXITING" << std::endl;
        return 0;
    }

    TH1F *hEventsThisFile = (TH1F*) fInput->Get("hEvents");
    TH1F *hRunsThisFile = (TH1F*) fInput->Get("hRuns");
    if ((hEventsThisFile == 0) || (hRunsThisFile == 0)) {
        std::cout << "[ERROR:] hEvents or hRuns tree NOT FOUND in " << input << ", EXITING" << std::endl;
        return 0;
    }

    //*****************************************//
    // Load necessary workspaces and data
    //*****************************************//
    std::map<std::string, std::string> fileMap = makeMapFromCSV(sampleListDir);

    int year  = WhichYear(configDir);
    std::string workspace = "2018ReReco";
    if      ( year == 2018 ) { workspace = "2018ReReco"; }
    else if ( year == 2017 ) { workspace = "2017ReReco"; }
    else if ( year == 2016 ) { workspace = "2016Legacy"; }
    else {
        std::cout << "WARNING: postprocess.cxx: workspace year could not be determined, defaulting to 2018" << std::endl;
    }
    LUNA::sampleConfig_t sConfig(process, doShifts, hasDNN);

    std::string era = "2018";
    if (configDir.find("2018") != std::string::npos) {
        era = "2018";
    }
    else if (configDir.find("2017") != std::string::npos) {
        era = "2017";
    }
    else if (configDir.find("2016preVFP") != std::string::npos) {
        era = "2016preVFP";
    }
    else if (configDir.find("2016postVFP") != std::string::npos) {
        era = "2016postVFP";
    }
    sConfig.setEra(era);

    std::cout << ">>> isMC: " << sConfig.isMC() << ", isEmbedded: " << sConfig.isEmbedded() << ", isData: " << sConfig.isData() << ", year " << year << ", era " << era << std::endl;

    TString localCommonFilesDir = "../commonFiles/";
    TString jsonDir = "/afs/cern.ch/work/s/skkwan/public/jsonpog-integration/";
    TString jsonDirEmb = "/afs/cern.ch/work/s/skkwan/public/CROWN-data/data/embedding/";

    if (!isLocalJob) {
        localCommonFilesDir = ""; // use Condor transfer input files to move files to the sandbox
        jsonDir = "";
        jsonDirEmb = "";
    }

    // HTT RooWorkspace
    TString wsFileName = localCommonFilesDir + "htt_scalefactors_legacy_" + sConfig.yearStr() + ".root";
    TFile fwmc(wsFileName);
    RooWorkspace *wmc;
    fwmc.GetObject("w", wmc);
    if (wmc == 0) {
        std::cout << "[ERROR:] RooWorkspace not retrievable from " << wsFileName << "!" << std::endl;
        return 0;
    }

    // Jet->tauh fake rate histograms. Fake rate is f = (number of events passing Medium isolation/ number of events passing the VVVLoose isolation)
    TGraphAsymmErrors *ptrTGraph_medium_vsVVVL;
    // Default value: mutau
    TString jetFRFileName = localCommonFilesDir + "jetFakeRates/fr_mt_" + sConfig.era() + ".root";
    TString jetFRGraphName = "fr_taus_Medium_vs_VVVL_1jet_mt";
    // If etau, get the etau versions
    if (channelName == "etau") {
        jetFRFileName = localCommonFilesDir + "jetFakeRates/fr_et_" + sConfig.era() + ".root";
        jetFRGraphName = "fr_taus_Medium_vs_VVVL_1jet_et";
    }
    TFile fFR(jetFRFileName);
    fFR.GetObject(jetFRGraphName.Data(), ptrTGraph_medium_vsVVVL);
    if (ptrTGraph_medium_vsVVVL == 0) {
        std::cout << "[ERROR:] TGraph pointer " << jetFRGraphName << " not retrievable from " << jetFRFileName << "!" << std::endl;
        return 0;
    }

    // QCD multijet background TF1s: OS to SS: pointers are declared in qcdMultiJetBackground-experimental.h
    const TF1 *osss_bjet,  *osss_bjet_up, *osss_bjet_down;
    TString osssFileName = localCommonFilesDir + "osss_em_" + sConfig.era() + ".root";
    TFile fQCD(osssFileName);
    fQCD.GetObject("OSSS_qcd_bjet", osss_bjet);
    fQCD.GetObject("OSSS_qcd_bjet_up", osss_bjet_up);
    fQCD.GetObject("OSSS_qcd_bjet_down", osss_bjet_down);
    if ((osss_bjet == 0) || (osss_bjet_up == 0) || (osss_bjet_down == 0)) {
        std::cout << "[ERROR:] OS/SS TF1 pointers not retrievable from " << osssFileName << "!" << std::endl;
        return 0;
    }
    // do not close file until end of main function, otherwise it segfaults

    // QCD multijet background TH2Fs : OS to SS Correction: pointers are declared in qcdMultiJetBackground-experimental.h
    const TH2F *th2f_correction, *th2f_closureOS;
    TString correctionClosureFileName = localCommonFilesDir + "closure_em_" + sConfig.era() + ".root";
    TFile fQCD2(correctionClosureFileName);
    fQCD2.GetObject("correction", th2f_correction);
    fQCD2.GetObject("closureOS", th2f_closureOS);
    if ((th2f_correction == 0) || (th2f_closureOS == 0)) {
        std::cout << "[ERROR:] OS/SS TH2F not retrievable from " << correctionClosureFileName << "!" << std::endl;
        return 0;
    }

    // e+mu trigger SF TH2Fs
    const TH2F *effMC_muo_Mu23Ele12, *effMC_ele_Mu23Ele12, *effMC_muo_Mu8Ele23, *effMC_ele_Mu8Ele23;
    const TH2F *effData_muo_Mu23Ele12, *effData_ele_Mu23Ele12, *effData_muo_Mu8Ele23, *effData_ele_Mu8Ele23;
    TString file_mu_Mu23Ele12 = localCommonFilesDir + "triggerSF/sf_mu_" + sConfig.era() + "_HLTMu23Ele12.root";
    TString file_ele_Mu23Ele12 = localCommonFilesDir + "triggerSF/sf_el_" + sConfig.era() + "_HLTMu23Ele12.root";
    TString file_mu_Mu8Ele23 = localCommonFilesDir + "triggerSF/sf_mu_" + sConfig.era() + "_HLTMu8Ele23.root";
    TString file_ele_Mu8Ele23 = localCommonFilesDir + "triggerSF/sf_el_" +  sConfig.era() + "_HLTMu8Ele23.root";
    TFile f_mu_Mu23Ele12(file_mu_Mu23Ele12);
    TFile f_ele_Mu23Ele12(file_ele_Mu23Ele12);
    TFile f_mu_Mu8Ele23(file_mu_Mu8Ele23);
    TFile f_ele_Mu8Ele23(file_ele_Mu8Ele23);
    // Default behaviour
    f_mu_Mu23Ele12.GetObject("eff_mc", effMC_muo_Mu23Ele12);
    f_ele_Mu23Ele12.GetObject("eff_mc", effMC_ele_Mu23Ele12);
    f_mu_Mu8Ele23.GetObject("eff_mc", effMC_muo_Mu8Ele23);
    f_ele_Mu8Ele23.GetObject("eff_mc", effMC_ele_Mu8Ele23);
    // Get the embedded 2D histograms instead, if the sample is Embedded
    if (sConfig.isEmbedded()) {
        f_mu_Mu23Ele12.GetObject("eff_embedded", effMC_muo_Mu23Ele12);
        f_ele_Mu23Ele12.GetObject("eff_embedded", effMC_ele_Mu23Ele12);
        f_mu_Mu8Ele23.GetObject("eff_embedded", effMC_muo_Mu8Ele23);
        f_ele_Mu8Ele23.GetObject("eff_embedded", effMC_ele_Mu8Ele23);
    }
    f_mu_Mu23Ele12.GetObject("eff_data", effData_muo_Mu23Ele12);
    f_ele_Mu23Ele12.GetObject("eff_data", effData_ele_Mu23Ele12);
    f_mu_Mu8Ele23.GetObject("eff_data", effData_muo_Mu8Ele23);
    f_ele_Mu8Ele23.GetObject("eff_data", effData_ele_Mu8Ele23);

    if ((effMC_muo_Mu23Ele12 == 0) || (effData_muo_Mu23Ele12 == 0) || (effMC_ele_Mu23Ele12 == 0) || (effData_ele_Mu23Ele12 == 0) ||
        (effMC_muo_Mu8Ele23 == 0) || (effData_muo_Mu8Ele23 == 0) || (effMC_ele_Mu8Ele23 == 0) || (effData_ele_Mu8Ele23 == 0)) {
        std::cout << "[ERROR:] SF2D not retrievable from " << file_mu_Mu23Ele12 << ", " << file_ele_Mu23Ele12 << ", "
                  << file_mu_Mu8Ele23 << ", or " << file_ele_Mu8Ele23 << "!" << std::endl;
    }

    //*****************************************//
    // Initialize. When this is less annoying it won't have to be written out like this
    //*****************************************//

    auto df_init_0 = InitializeDataframe(dfInput, sConfig);
    auto df_init_1 = AddEventWeights_experimental(df_init_0, sConfig, hEventsThisFile, hEvents, hRuns, xsec, nEntriesOriginal);  // define/initialize "weight_initial"
    auto df_init_2 = df_init_1.Define("era", [era]() { return era; })
                            .Define("wmcPtr", [wmc]() { return wmc; }) // capture pointer to RooWorkSpace
                            .Define("osss_bjet_ptr", [osss_bjet]() { return osss_bjet; })
                            .Define("osss_bjet_up_ptr", [osss_bjet_up]() { return osss_bjet_up; })
                            .Define("osss_bjet_down_ptr", [osss_bjet_down]() { return osss_bjet_down; })
                            .Define("th2f_correction_ptr", [th2f_correction]() { return th2f_correction; })
                            .Define("th2f_closureOS_ptr",  [th2f_closureOS]() { return th2f_closureOS;  })
                            .Define("effMC_ele_Mu23Ele12", [effMC_ele_Mu23Ele12]() { return effMC_ele_Mu23Ele12; })
                            .Define("effMC_muo_Mu23Ele12", [effMC_muo_Mu23Ele12]() { return effMC_muo_Mu23Ele12; })
                            .Define("effMC_ele_Mu8Ele23", [effMC_ele_Mu8Ele23]() { return effMC_ele_Mu8Ele23; })
                            .Define("effMC_muo_Mu8Ele23", [effMC_muo_Mu8Ele23]() { return effMC_muo_Mu8Ele23; })
                            .Define("effData_ele_Mu23Ele12", [effData_ele_Mu23Ele12]() { return effData_ele_Mu23Ele12; })
                            .Define("effData_muo_Mu23Ele12", [effData_muo_Mu23Ele12]() { return effData_muo_Mu23Ele12; })
                            .Define("effData_ele_Mu8Ele23", [effData_ele_Mu8Ele23]() { return effData_ele_Mu8Ele23; })
                            .Define("effData_muo_Mu8Ele23", [effData_muo_Mu8Ele23]() { return effData_muo_Mu8Ele23; });

    auto dfFinal = PerformAnalysis(df_init_2, sConfig, jsonDir, jsonDirEmb, fileMap, isLocalJob, ptrTGraph_medium_vsVVVL);

    // Make folders for trees and copy hEvents and hRuns histograms in the output file
    TFile *fOut = new TFile(outputFile.c_str(), "UPDATE");
    fOut->cd();
    fOut->mkdir(channelName);
    fOut->cd();
    // Write the INPUT file's hEvents and hRuns
    hEventsThisFile->Write();
    hRunsThisFile->Write();
    fOut->Close();

    // In the snapshot call, set mode to "UPDATE". RDF snapshot will handle file opening and closing
    TString outTree;
    outTree.Form("%s/event_tree", channelName.Data());
    SnapshotFinalVariables(dfFinal, (std::string) outTree, outputFile, allCommonVariables, optionalVariables);

    fwmc.Close();
    fQCD.Close();
    fQCD2.Close();
    fFR.Close();

    // Make and print reports
    dfFinal.Report()->Print();

    time.Stop();
    time.Print();

    return 0;

}
