#!/bin/bash

#--------------------------------------------------------
# Usage:
#  cmsenv
#  cd ${CMSSW_BASE}/src/lunaFramework/
#  source venv-LUNA/bin/activate
#  bash runPostprocess-experimental.sh
#
#--------------------------------------------------------

COMPILE=true
SAMPLES="postProcHists-2018-syncOnly.csv"
RUN_POSTPROCESS=true

# only one of these can be 1 (true)
DO_MUTAU=1
DO_ETAU=0
DO_EMU=0

# do shifts, categories (or just inclusive)
DO_SHIFTS=1
DO_CATEGORIES=0
DO_ALL_VARIABLES=0
IS_LOCAL=1

#--------------------------------------------------------
# Check if we used bash
#--------------------------------------------------------
if [[ "$0" != "$BASH_SOURCE" ]]; then
    echo ">>> ${BASH_SOURCE[0]}: Error: Script must be run with bash"
    return
fi
#--------------------------------------------------------
# Check if environments are set
#--------------------------------------------------------
if [[ -z ${CMSSW_BASE} ]]; then
    echo ">>> ${BASH_SOURCE[0]}: CMSSW environment is not set! Make sure to do cmsenv"
    exit 1
fi
if [[ -z ${VIRTUAL_ENV} ]]; then
    echo ">>> ${BASH_SOURCE[0]}: Virtual environment is not set! Make sure to source venv-LUNA/bin/activate"
    exit 1
fi

#--------------------------------------------------------
# Get the voms-proxy-info certificate
#--------------------------------------------------------
export MYPROXYPATH="$(voms-proxy-info -path)"

if [[ -f ${MYPROXYPATH} ]]; then
    echo ">>> ${BASH_SOURCE[0]}: Copying proxy from ${MYPROXYPATH} to /afs/cern.ch/user/s/skkwan/private/x509up_file"
    cp ${MYPROXYPATH} /afs/cern.ch/user/s/skkwan/private/x509up_file
else
    echo ">>> ${BASH_SOURCE[0]}: [ERROR]: x509 proxy not found on this machine, make sure voms-proxy-init was run, exiting"
    exit 1
fi

#--------------------------------------------------------
# Compile executable
#--------------------------------------------------------
if [[ "$COMPILE" = true ]]; then
    echo ">>> ${BASH_SOURCE[0]}: Compiling postprocess-experimental.cxx executable ..."
    TARGET_FILES="helpers/sampleConfig_class.cc helpers/helperFunctions.cc helpers/computePhysicsVariables.cc"
    COMPILER=$(root-config --cxx)
    FLAGS="$(root-config --cflags --libs) $(correction config --cflags --ldflags)"
    time $COMPILER -g -O3 -Wall -Wextra -Wpedantic -lRooFitCore -lRooFit -o postprocess-experimental postprocess-experimental.cxx $FLAGS ${TARGET_FILES}
    if [[ $? -ne 0 ]]; then
        echo ">>> Compile failed, exit"
        exit 1
    fi
fi

#--------------------------------------------------------
# First check if the input files exists
#--------------------------------------------------------
while IFS=, read -r SAMPLEDIR PROCESS CONFIG_DIR XSEC NENTRIES
do
    {
    INFO=${SAMPLEDIR}/info/info_${PROCESS}.root
    if [[ ! -f ${INFO} ]]; then
        echo "${BASH_SOURCE[0]}: ERROR: File ${INFO} is not there, aborting. (Make sure there are no extra newlines in ${SAMPLE_LIST}.)"
        exit 1
    fi
    }
done < ${SAMPLES}


#--------------------------------------------------------
# Then run the postprocess
#--------------------------------------------------------

if [[ "${RUN_POSTPROCESS}" = true ]]; then

    DATETIME="$(date +"%Y-%m-%d-%Hh%Mm")"

    # Loop over the files
    while IFS=, read -r DIR PROCESS CONFIG_DIR XSEC NENTRIES
    do
	{
        SAMPLE_INFO="${DIR}/info/info_${PROCESS}.root"
        OUTPUT_FILE="out_${PROCESS}.root"
        rm -f ${OUTPUT_FILE}

	    LOG="logRun_${PROCESS}"

        # Infer the ${SAMPLE_DIR}
        # when running on non-SVFit:
        #        SAMPLE_DIR="${DIR}/${PROCESS}/${PROCESS}_0.root"
        # if running on post-SVFit files:
        SAMPLE_DIR="${DIR}/${PROCESS}/out_1.root"
        echo ">>> ${BASH_SOURCE[0]}: Inferring ${SAMPLE_DIR} from the info file path"
        if [[ ! -f ${SAMPLE_DIR} ]]; then
            echo "${BASH_SOURCE[0]}: ERROR: File ${SAMPLE_DIR} is not there, aborting."
            exit 1
        fi

	    echo ">>> ${BASH_SOURCE[0]}: Starting postprocess executable for ${SAMPLE_DIR}, writing output histograms to ${OUTPUT_FILE}"
	    ./postprocess-experimental ${SAMPLE_INFO} ${SAMPLE_DIR} ${PROCESS} ${CONFIG_DIR} ${OUTPUT_FILE} ${XSEC} ${NENTRIES} ${SAMPLES} ${DO_MUTAU} ${DO_ETAU} ${DO_EMU} ${DO_SHIFTS} ${DO_CATEGORIES} ${DO_ALL_VARIABLES} ${IS_LOCAL} | tee $LOG

	}
    done < ${SAMPLES}
    wait

    echo "Done"
    #--------------------------------------------------------
    # Optional finishing up
    #--------------------------------------------------------
    # echo ">>> ${BASH_SOURCE[0]}: scp 'skkwan@lxplus.cern.ch:${FINAL_EOS_DIR}*' ."
    # Also createReport.py with just the n-tuple cutflows
    # cd /eos/user/s/skkwan/hToAA/dataMC
    # python createReport.py --rootdir=${FINAL_EOS_DIR}
    # cd -
fi
