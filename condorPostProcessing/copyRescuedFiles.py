# copyRescuedFiles.py
# Usage: python copyRescuedFiles.py [args]

import argparse
import os
import re
import shutil

# Base path of EOS where all the .root files are
eosDir = "/eos/user/s/skkwan/hToAA/condorSkim/"

# Base path of directory (usually priv/) where the Condor log files are.
logDir = "/afs/cern.ch/work/s/skkwan/private/condor_hToAA/"

parser = argparse.ArgumentParser(
    description="Check rescue jobs and copy successfully rescued jobs to the main directories."
)

parser.add_argument(
    "--execute", help="Perform copy commands", dest="execute", action="store_true"
)

# Parser arguments
parser.add_argument(
    "--mainTime",
    help="Target timestamp to copy rescued *.root files TO",
    required=True,
)

parser.add_argument(
    "--rescueTime",
    help="Target timestamp to rescue FROM",
    required=True,
)

parser.set_defaults(execute=False)

args = parser.parse_args()

# The string in the .log file that indicates a succesful job
proofstring = "(1) Normal termination"

# Build the paths we need
mainLogDir = os.path.join(logDir, args.mainTime)
rescLogDir = os.path.join(logDir, args.rescueTime)

mainEosDir = os.path.join(eosDir, args.mainTime)
rescEosDir = os.path.join(eosDir, args.rescueTime)

print(rescLogDir)
# Loop through log files in the /priv directory of the rescue job
# If the job was successful, copy the corresponding .root file in rescEosDir
# to the args.maineos where we are collecting all the successful .root files.
for subdir, dirs, files in os.walk(rescLogDir):
    sample = os.path.basename(subdir)

    # Loop through the Condor log files
    for filename in files:
        path = os.path.join(rescLogDir, sample, filename)

        # In .out files, check for the proofstring
        if ".log" in path:
            with open(path) as f:
                if proofstring in f.read():
                    num = re.findall("\d+(?=\.\w+$)", filename)

                    # Make sure we unequivocably know the batch number
                    if len(num) > 1:
                        sys.exit("Could not find batch number in " + filename)

                    batchnum = num[0]

                    # Get the target .root file
                    targetRoot = sample + "_" + batchnum + ".root"
                    targetLogs = "test_" + sample + "_" + batchnum + "*"
                    print(os.path.join(rescEosDir, sample, targetRoot))
                    print(os.path.join(mainEosDir, sample))

                    # Commands
                    cmd_copyRootFile = "cp {} {}".format(
                        os.path.join(rescEosDir, sample, targetRoot),
                        os.path.join(mainEosDir, sample),
                    )
                    cmd_copyLog = "cp {} {}".format(
                        os.path.join(rescEosDir, sample, targetLogs),
                        os.path.join(mainEosDir, sample),
                    )

                    print("Preparing command {}".format(cmd_copyRootFile))
                    print("Preparing command {}".format(cmd_copyLog))

                    if args.execute:
                        print("executing commands...")
                        os.system(cmd_copyRootFile)
                        os.system(cmd_copyLog)
                    else:
                        print("execute flag false, do not do anything")
