#!/bin/bash

# Usage:
#  cmsenv
#  source venv-LUNA/bin/activate
#  voms-proxy-init
#  (Make sure postprocess.cxx is compiled into postprocess)
#  bash runCondorPostprocessing.sh

SAMPLES_FILENAME="temp.csv"
YEAR_OR_DESCRIPTION="fix-xsec"
SUBMIT=true
DO_MUTAU=1
DO_ETAU=1
DO_EMU=1
DO_SHIFTS=1
DO_CATEGORIES=0
DO_ALL_VARIABLES=1
IS_LOCAL=0

WORKING_DIR=${CMSSW_BASE}"/src/lunaFramework/postProcessing"
SAMPLES="${WORKING_DIR}/${SAMPLES_FILENAME}"
EXECUTABLE_NAME="postprocess-experimental"
EXECUTABLE_FULL_PATH="${WORKING_DIR}/${EXECUTABLE_NAME}"
BASH_SCRIPT=${CMSSW_BASE}"/src/lunaFramework/condorPostProcessing/job.sh"

# Use regex to extract the first letter of the username. ^ is the beginning of the string. \w means any alphanumeric character from the Latin alphabet. ( ) captures the variable
re="^(\w)"
# Bash's =~ operator matches the left hand side to the regex on the right hand side
[[ ${USER} =~ ${re} ]]
# The results of the regex can be extracted from the bash variable BASH_REMATCH. Index [0] returns all of the matches.
USER_FIRST_LETTER=${BASH_REMATCH[0]}

JSONPOG_PATH="/afs/cern.ch/work/${USER_FIRST_LETTER}/${USER}/public/jsonpog-integration/"
CROWN_EMBEDDED_FILES_PATH="/afs/cern.ch/work/${USER_FIRST_LETTER}/${USER}/public/CROWN-data/data/embedding/"
COMMON_FILES_PATH="${CMSSW_BASE}/src/lunaFramework/commonFiles/"

#--------------------------------------------------------
# Check if we used bash
#--------------------------------------------------------
if [[ "$0" != "$BASH_SOURCE" ]]; then
    echo ">>> ${BASH_SOURCE[0]}: Error: Script must be run with bash"
    return
fi
#--------------------------------------------------------
# Check if environments are set
#--------------------------------------------------------
if [[ -z ${CMSSW_BASE} ]]; then
    echo ">>> ${BASH_SOURCE[0]}: CMSSW environment is not set! Make sure to do cmsenv"
    exit 1
fi
if [[ -z ${VIRTUAL_ENV} ]]; then
    echo ">>> ${BASH_SOURCE[0]}: Virtual environment is not set! Make sure to source venv-LUNA/bin/activate"
    exit 1
fi
#--------------------------------------------------------
# Get the voms-proxy-info certificate
#--------------------------------------------------------
export MYPROXYPATH="$(voms-proxy-info -path)"

x509_TARGET_PATH="/afs/cern.ch/user/${USER_FIRST_LETTER}/${USER}/private/x509up_file"

if [[ -f ${MYPROXYPATH} ]]; then
    echo ">>> runCondorPostprocessing.sh: Copying proxy from ${MYPROXYPATH} to ${x509_TARGET_PATH}"
    cp ${MYPROXYPATH} ${x509_TARGET_PATH}
else
    echo ">>> ${BASH_SOURCE[0]}: [ERROR]: x509 proxy not found on this machine, make sure voms-proxy-init was run, exiting"
    exit 1
fi

#--------------------------------------------------------
# Check that the input directory files exist first!
#--------------------------------------------------------
echo ">>> ${BASH_SOURCE[0]}: Reading from ${SAMPLES}..."
while IFS=, read -r DIR SAMPLE_NAME CONFIG_DIR XSEC N_ENTRIES_ORIGINAL_DATASET DNN_PATH
do

    SAMPLE_INFO="${DIR}/info/info_${SAMPLE_NAME}.root"

    if [[ ${SAMPLE_INFO} == "" ]]; then
        break
    fi

    echo ">>> ${BASH_SOURCE[0]}: Will run on sample ${SAMPLE_INFO}, process ${SAMPLE_NAME}, config directory ${CONFIG_DIR}, xsec ${XSEC}, with nEntriesInOriginalDataset ${N_ENTRIES_ORIGINAL_DATASET}, and DNN TTree (if available) ${DNN_FULL}"
    if [[ ${SAMPLE_INFO} == "/store/" ]] ; then
        # File is remote
        xrdfs root://cmsxrootd.fnal.gov/ ls -l locate ${SAMPLE_INFO}
        if [[ $? -eq 0 ]] ; then
            echo "   >>> Remote file found: ${SAMPLE_INFO}"
        else
            echo "  >>>  [ERROR]: Remote file ${SAMPLE_INFO} not found"
            exit 1
        fi
    else
        # File is local
        if [[ -f "${SAMPLE_INFO}" ]]; then
            echo "  >>> Local file found: ${SAMPLE_INFO}"
        else
            echo "  >>> [ERROR]: Local file ${SAMPLE_INFO} not found"
            exit 1
        fi
    fi
    if [[ ${DNN_FULL} == "" ]]; then
		echo "  >>> [Warning:] ${SAMPLE_NAME}: No DNN TTree found"
	fi
done < ${SAMPLES}

#--------------------------------------------------------
# Check that the input executable exists
#--------------------------------------------------------
if [[ !  -f "../postProcessing/postprocess-experimental" ]]; then
    echo "   >>> ${BASH_SOURCE[0]}: [ERROR]: Executable postprocess-experimental not found, make sure it is compiled in the postProcessing/ directory, exiting"
    exit 1
fi

#--------------------------------------------------------
# Create the Condor submit files
#--------------------------------------------------------
DATETIME="$(date +"%Y-%m-%d-%Hh%Mm")"

copiedInfo=false

while IFS=, read -r DIR SAMPLE_NAME CONFIG_REL_DIR XSEC N_ENTRIES_ORIGINAL_DATASET SPLIT_SAMPLE DNN_PATH
do

	# Create the job submit directory and EOS directory
    CONFIG_DIR="${WORKING_DIR}/${CONFIG_REL_DIR}"
    JOBLOG_DIR="logs/${DATETIME}/${SAMPLE_NAME}"
    mkdir -p "logs/"
    mkdir -p "logs/${DATETIME}"
    mkdir -p ${JOBLOG_DIR}

    if [ ${copiedInfo} = false ]; then
        # Copy the /info/ to afs for easier transfer
        cp -r ${DIR}/info logs/${DATETIME}
        echo " >>> Copying ${DIR}/info to logs/${DATETIME}/info for shipout"
        copiedInfo=true
    fi

    EOS_PREFIX="root://eosuser.cern.ch/"
    EOS_BASE_DIR="/eos/cms/store/group/phys_susy/AN-24-166/${USER}/condorPostProcessing/${DATETIME}/${SAMPLE_NAME}"
    EOS_DIR="${EOS_PREFIX}${EOS_BASE_DIR}"
    mkdir -p ${JOBLOG_DIR}
    mkdir -p ${EOS_BASE_DIR}
    # arcane but we need this for the logs to write out correctly
    mkdir -p ${EOS_BASE_DIR}/logs/${DATETIME}/${SAMPLE_NAME}
    OUTPUT_NTUPLE="postprocessed_ntuple_${SAMPLE_NAME}.root"
    subfile=${JOBLOG_DIR}/postprocessed_ntuple_${SAMPLE_NAME}.sub

    # Copy the template
    cp jobTemplate.sub ${subfile}
    JOB_FLAVOUR='"microcentury"'

    # Set the input file information
    SAMPLE_INFO_FILE_NAME="${DIR}/info/info_${SAMPLE_NAME}.root"
    SAMPLE_INFO_FULL_PATH="logs/${DATETIME}/info/"

    # We need to do the same thing for the input TTree file. With a bit of bash frankensteining, if we have the path to the
    # info file, we need its directory.. then the sample name as a folder again.. then the sample name _$(Process).root
    SAMPLE_FILE_NAME="out_\$(Process).root"
    thisdir="${DIR}/${SAMPLE_NAME}/${SAMPLE_FILE_NAME}"
    SAMPLE_FULL_PATH="${EOS_PREFIX}${thisdir}"

    NJOBS=$(find ${DIR}/${SAMPLE_NAME}/*.root | wc -l)
    echo "   >>> njobs: ${NJOBS}"

    OUTPUT_NTUPLE="postprocessed_ntuple_${SAMPLE_NAME}_\$(Process).root"

    # Edit parameters in the Condor .sub file
    sed -i "s|(x509_target_path)|${x509_TARGET_PATH}|g" ${subfile}
    sed -i "s|(executable_name)|${EXECUTABLE_NAME}|g" ${subfile}
    sed -i "s|(executable_full_path)|${EXECUTABLE_FULL_PATH}|g" ${subfile}
    sed -i "s|(bash_script)|${BASH_SCRIPT}|g" ${subfile}
    sed -i "s|(sample_name)|${SAMPLE_NAME}|g" ${subfile}
    sed -i "s|(sample_file_name)|${SAMPLE_FILE_NAME}|g" ${subfile}
    echo ">>>>>>>>>>>>>>>>>>> ${SAMPLE_FULL_PATH}"
    sed -i "s|(sample_full_path)|${SAMPLE_FULL_PATH}|g" ${subfile}
    sed -i "s|(sample_info_file_name)|${SAMPLE_INFO_FILE_NAME}|g" ${subfile}
    sed -i "s|(sample_info_full_path)|${SAMPLE_INFO_FULL_PATH}|g" ${subfile}
    sed -i "s|(sample_dir)|${SAMPLE_DIR}|g" ${subfile}
    sed -i "s|(PROCESS)|${PROCESS}|g" ${subfile}
    sed -i "s|(config_dir)|${CONFIG_DIR}|g" ${subfile}
    sed -i "s|(output_ntuple)|${OUTPUT_NTUPLE}|g" ${subfile}
    sed -i "s|(xsec)|${XSEC}|g" ${subfile}
    sed -i "s|(n_entries_in_original_dataset)|${N_ENTRIES_ORIGINAL_DATASET}|g" ${subfile}
    sed -i "s|(local_path_to_csv)|${SAMPLES_FILENAME}|g" ${subfile}
    sed -i "s|(afs_path_to_csv)|${SAMPLES}|g" ${subfile}
    sed -i "s|(json_pog_path)|${JSONPOG_PATH}|g" ${subfile}
    sed -i "s|(CROWN_embedded_files_path)|${CROWN_EMBEDDED_FILES_PATH}|g" ${subfile}
    sed -i "s|(common_files_path)|${COMMON_FILES_PATH}|g" ${subfile}
    sed -i "s|(sample_info_dir)|${SAMPLE_INFO_DIR}|g" ${subfile}
    sed -i "s|(eos_dir)|${EOS_DIR}|g" ${subfile}
    sed -i "s|(joblog_dir)|${JOBLOG_DIR}|g" ${subfile}
    sed -i "s|(do_mutau)|${DO_MUTAU}|g" ${subfile}
    sed -i "s|(do_etau)|${DO_ETAU}|g" ${subfile}
    sed -i "s|(do_emu)|${DO_EMU}|g" ${subfile}
    sed -i "s|(do_shifts)|${DO_SHIFTS}|g" ${subfile}
    sed -i "s|(do_categories)|${DO_CATEGORIES}|g" ${subfile}
    sed -i "s|(do_all_variables)|${DO_ALL_VARIABLES}|g" ${subfile}
    sed -i "s|(is_local)|${IS_LOCAL}|g" ${subfile}
    # If there is no DNN, DNN_PATH is going to be empty and sed will give a warning
  	sed -i "s|(dnn_path)|${DNN_PATH}|g" ${subfile}
    sed -i "s|(njobs)|${NJOBS}|g" ${subfile}

    sed -i "s|(job_flavour)|${JOB_FLAVOUR}|g" ${subfile}
    sed -i "s|(cpus)|${CPUS}|g" ${subfile}
    #   cat ${subfile}


    mysubmitcommand="condor_submit -batch-name postproc_${YEAR_OR_DESCRIPTION}_${DATETIME}_${SAMPLE_NAME} -file ${subfile}"
 	echo ">>>  ${SAMPLE_NAME}, submitting from file ${subfile}, preparing command ${mysubmitcommand}"
    if [[ ${SUBMIT} == true ]]; then
        eval ${mysubmitcommand}
    else
        echo ">>> Submit was false, did not submit"
    fi

done < ${SAMPLES}
