# lookForErrorFiles.py
# Usage:
#   cmsenv
#   cd ${CMSSW_BASE}/src/lunaFramework
#   source venv-LUNA/bin/activate
#   python lookForErrorsFiles.py --year=2018 --rootdir=[...] --dryrun
#

import argparse
import os
import re
import sys
import shutil

parser = argparse.ArgumentParser(
    description='Check a folder of Condor log files for failed jobs (jobs that do not have the "normal termination" string.'
)

parser.add_argument(
    "--execute", help="Create new rescue *.csvs.", dest="execute", action="store_true"
)
parser.add_argument(
    "--dryrun",
    help="Only check log files, do not create new rescue *.csv.",
    dest="execute",
    action="store_true",
)
parser.add_argument("--year", help="Year", required=True)
parser.add_argument(
    "--rootdir", help="Directory with Condor logs to scan", required=True
)
parser.set_defaults(execute=False)

args = parser.parse_args()

# The directory where the dataset *.lists are
cmsswBase = os.getenv("CMSSW_BASE")
listdir = cmsswBase + "/src/lunaFramework/syncNanoAOD/nanoAODfilepaths"
# The directory to write the rescue *.lists to (typically a subdirectory /rescue)
rescuedir = cmsswBase + "/src/lunaFramework/condorPostProcessing/rescue"

# The string in the .log file that indicates a succesful job
outfile_proofstring = "Real time"

# ---- Setup ---------------------
if args.execute:
    print(">>> Deleting files in " + os.path.join(rescuedir, args.year))
    if os.path.exists(os.path.join(rescuedir, args.year)):
        shutil.rmtree(os.path.join(rescuedir, args.year))
# ---- End of setup --------------

# Loop through log files in the directory
for subdir, dirs, files in os.walk(args.rootdir):
    sample = os.path.basename(subdir)
    if sample == os.path.basename(args.rootdir):
        print(">>> Skipping " + sample)
        continue
    print(">>> " + sample)

    # ---- Setup --------------------------------------------------
    # Prepare the rescue directory by emptying it, if we are making new files'

    rescueTargets = os.path.join(rescuedir, args.year, sample + "_rescue.csv")
    if not os.path.exists(os.path.join(rescuedir, args.year)):
        os.makedirs(os.path.join(rescuedir, args.year))
        if os.path.exists(rescueTargets) and args.execute:
            os.remove(rescueTargets)

    # ---- End of setup -------------------------------------------

    # Loop through the Condor log files
    for filename in files:
        outpath = os.path.join(subdir, filename)

        # In .out files, check for the proofstring
        if ".out" in outpath:
            # Get batch number
            num = re.findall("\d+(?=\.\w+$)", filename)
            batchnum = 0
            if len(num) > 1:
                sys.exit("Could not find batch number in " + filename)
            # Mass points will trip up the regex
            elif ("DYJetsToLL_M-" not in filename) and (len(num) > 0):
                batchnum = num[0]

            isOutFileGood = True

            with open(outpath) as fout:
                if outfile_proofstring not in fout.read():
                    isOutFileGood = False
                if not isOutFileGood:
                    # print("... --> {}: normal .out termination string not found. Last seven lines are:\n {} \n".format(outpath, os.system("tail -n 7 {}".format(outpath))))
                    print(
                        "... --> {}: normal .out termination string not found.".format(
                            outpath
                        )
                    )

                # Want to submit one Condor job for each failed job
                # Need condorSkim.sh but have nInstances = 1,
                # INPUT_LIST says the batchnum instead of \$(Process)

                # Add flag arguments to condorSubmit.sh
                # https://www.baeldung.com/linux/use-command-line-arguments-in-bash-script

                # https://linuxize.com/post/bash-check-if-file-exists/
                # If there is a rescue_[sample].csv file, loop through it
                # and create a new sub file etc.

                if not isOutFileGood:
                    if batchnum != -1:
                        listname = sample + "_BATCH_" + str(batchnum) + ".list"
                    else:
                        listname = sample + ".list"
                    print(listname)
                    rescueList = os.path.join(listdir, args.year, sample, listname)

                    if not os.path.exists(rescueList):
                        sys.exit(
                            rescueList
                            + " does not exist; cannot proceed with rescue job creation"
                        )

                    if args.execute:
                        print(rescueTargets)
                        f = open(rescueTargets, "a")
                        f.write(str(batchnum) + "," + listname + "\n")
                        f.close()
                    else:
                        print("Dryrun: do not create new batch lists")
