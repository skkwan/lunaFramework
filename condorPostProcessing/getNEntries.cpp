#include "TTree.h"

int getNEntries(std::string myFile, std::string treeName) {

    if (myFile == "") {
        std::cout << "[Error:] File passed in was empty, returning 0" << std::endl;
        return 0;
    }

    if (treeName == "") {
        std::cout << "[Error:] Tree passed in was empty, returning 0" << std::endl;
        return 0;
    }

    // Open the file
    TFile *f = TFile::Open(myFile.c_str());
    if (!f->IsOpen() || (f == 0)) {
        std::cout << "[Error:] File " << myFile << " not found, returning 0" << std::endl;
        return 0;
    }

    // Get the TTree
    TTree *t = (TTree*)f->Get(treeName.c_str());
    if (t == 0) {
        std::cout << "[Error:] TTree " << treeName << " in file " << myFile << " not found, returning 0" << std::endl;
        return 0;
    }

    int nEntries = t->GetEntries();
    return nEntries;
}
