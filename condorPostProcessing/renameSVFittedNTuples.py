# renameSVFittedNTuples.py
# Description: The SVFitted ntuples do not have out_0.root. Rename the highest out_N.root to out_0.root to make things smoother for the postprocess Condor script.
# ONLY run this once, after copying the SVFitted n-tuples to your EOS area.
# Usage:
#    python3 renameSVFittedNTuples.py --dir=[path to copy of SVFitted n-tuples]
#
# Hint: after running this, then you can do python3 haddPostprocessed.py (also only needs to be done once) before sending these to histogramming.

import argparse
import subprocess
import os

parser = argparse.ArgumentParser(
    description="Hadd files from a directory with sub-directories"
)

parser.add_argument("--dir", dest="rootdir", help="Path to directory", required=True)
parser.add_argument("--execute", dest="execute", action="store_true")
parser.set_defaults(execute=False)

args = parser.parse_args()

procs = [
    "Cascade_ggH_MA1-15_MA2-100",
    "Cascade_ggH_MA1-15_MA2-110",
    "Cascade_ggH_MA1-15_MA2-30",
    "Cascade_ggH_MA1-15_MA2-40",
    "Cascade_ggH_MA1-15_MA2-50",
    "Cascade_ggH_MA1-15_MA2-60",
    "Cascade_ggH_MA1-15_MA2-70",
    "Cascade_ggH_MA1-15_MA2-80",
    "Cascade_ggH_MA1-15_MA2-90",
    "Cascade_ggH_MA1-20_MA2-100",
    "Cascade_ggH_MA1-20_MA2-40",
    "Cascade_ggH_MA1-20_MA2-50",
    "Cascade_ggH_MA1-20_MA2-60",
    "Cascade_ggH_MA1-20_MA2-70",
    "Cascade_ggH_MA1-20_MA2-80",
    "Cascade_ggH_MA1-20_MA2-90",
    "Cascade_ggH_MA1-30_MA2-60",
    "Cascade_ggH_MA1-30_MA2-70",
    "Cascade_ggH_MA1-30_MA2-80",
    "Cascade_ggH_MA1-30_MA2-90",
    "Cascade_VBF_MA1-15_MA2-100",
    "Cascade_VBF_MA1-15_MA2-110",
    "Cascade_VBF_MA1-15_MA2-30",
    "Cascade_VBF_MA1-15_MA2-40",
    "Cascade_VBF_MA1-15_MA2-50",
    "Cascade_VBF_MA1-15_MA2-60",
    "Cascade_VBF_MA1-15_MA2-70",
    "Cascade_VBF_MA1-15_MA2-80",
    "Cascade_VBF_MA1-15_MA2-90",
    "Cascade_VBF_MA1-20_MA2-100",
    "Cascade_VBF_MA1-20_MA2-40",
    "Cascade_VBF_MA1-20_MA2-50",
    "Cascade_VBF_MA1-20_MA2-60",
    "Cascade_VBF_MA1-20_MA2-70",
    "Cascade_VBF_MA1-20_MA2-80",
    "Cascade_VBF_MA1-20_MA2-90",
    "Cascade_VBF_MA1-30_MA2-60",
    "Cascade_VBF_MA1-30_MA2-70",
    "Cascade_VBF_MA1-30_MA2-80",
    "Cascade_VBF_MA1-30_MA2-90",
    "DY1JetsToLL",
    "DY2JetsToLL",
    "DY3JetsToLL",
    "DY4JetsToLL",
    "DYJetsToLL_M-10to50",
    "DYJetsToLL_M-50",
    "DYJetsToLL_M-50-ext1",
    "Embedded-Run2017B-ElMu",
    "Embedded-Run2017B-ElTau",
    "Embedded-Run2017B-MuTau",
    "Embedded-Run2017C-ElMu",
    "Embedded-Run2017C-ElTau",
    "Embedded-Run2017C-MuTau",
    "Embedded-Run2017D-ElMu",
    "Embedded-Run2017D-ElTau",
    "Embedded-Run2017D-MuTau",
    "Embedded-Run2017E-ElMu",
    "Embedded-Run2017E-ElTau",
    "Embedded-Run2017E-MuTau",
    "Embedded-Run2017F-ElMu",
    "Embedded-Run2017F-ElTau",
    "Embedded-Run2017F-MuTau",
    "GluGluHToTauTau",
    "GluGluHToWWTo2L2Nu",
    "GluGluZH_HToWW",
    "HWminusJ_HToWW",
    "HWplusJ_HToWW",
    "HZJ_HToWW",
    "MuonEG-Run2017B",
    "MuonEG-Run2017C",
    "MuonEG-Run2017D",
    "MuonEG-Run2017E",
    "MuonEG-Run2017F",
    "NonCascade_ggH_MA2-20-MA1-15",
    "NonCascade_ggH_MA2-30-MA1-15",
    "NonCascade_ggH_MA2-30-MA1-20",
    "NonCascade_ggH_MA2-40-MA1-20",
    "NonCascade_ggH_MA2-40-MA1-30",
    "NonCascade_ggH_MA2-50-MA1-30",
    "NonCascade_ggH_MA2-50-MA1-40",
    "NonCascade_ggH_MA2-60-MA1-30",
    "NonCascade_ggH_MA2-60-MA1-40",
    "NonCascade_ggH_MA2-60-MA1-50",
    "NonCascade_ggH_MA2-70-MA1-40",
    "NonCascade_ggH_MA2-70-MA1-50",
    "NonCascade_ggH_MA2-80-MA1-40",
    "NonCascade_VBF_MA2-20-MA1-15",
    "NonCascade_VBF_MA2-30-MA1-15",
    "NonCascade_VBF_MA2-30-MA1-20",
    "NonCascade_VBF_MA2-40-MA1-20",
    "NonCascade_VBF_MA2-40-MA1-30",
    "NonCascade_VBF_MA2-50-MA1-30",
    "NonCascade_VBF_MA2-50-MA1-40",
    "NonCascade_VBF_MA2-60-MA1-30",
    "NonCascade_VBF_MA2-60-MA1-40",
    "NonCascade_VBF_MA2-60-MA1-50",
    "NonCascade_VBF_MA2-70-MA1-40",
    "NonCascade_VBF_MA2-70-MA1-50",
    "NonCascade_VBF_MA2-80-MA1-40",
    "SingleElectron-Run2017B",
    "SingleElectron-Run2017C",
    "SingleElectron-Run2017D",
    "SingleElectron-Run2017E",
    "SingleElectron-Run2017F",
    "SingleMuon-Run2017B",
    "SingleMuon-Run2017C",
    "SingleMuon-Run2017D",
    "SingleMuon-Run2017E",
    "SingleMuon-Run2017F",
    "ST_t-channel_antitop",
    "ST_t-channel_top",
    "ST_tW_antitop",
    "ST_tW_top",
    "ttHTobb",
    "ttHToNonbb",
    "TTTo2L2Nu",
    "TTToHadronic",
    "TTToSemiLeptonic",
    "VBFHToTauTau",
    "VBFHToWWTo2L2Nu",
    "VVTo2L2Nu",
    "W1JetsToLNu",
    "W2JetsToLNu",
    "W3JetsToLNu",
    "W4JetsToLNu",
    "WJetsToLNu",
    "WminusHToTauTau",
    "WplusHToTauTau",
    "WZTo2Q2L",
    "WZTo3LNu",
    "ZHToTauTau",
    "ZZTo2Q2L",
    "ZZTo4L",
]

if not args.rootdir.endswith("/"):
    args.rootdir += "/"

for proc in procs:
    basedir = f"{args.rootdir}{proc}"

    command = f"ls {basedir}/*.root | wc -l"
    print(command)
    result = subprocess.check_output(f"ls {basedir}/*.root | wc -l", shell=True)
    # print(result)
    nFiles = result.decode("utf-8").rstrip()
    print(nFiles)

    command2 = f"mv {basedir}/out_{nFiles}.root {basedir}/out_0.root"

    print(command2)

    if args.execute:
        os.system(command2)
    else:
        print("--execute flag was not set: did not execute any commands")
