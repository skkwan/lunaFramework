#!/bin/bash

#-----------------------------------------------
# This is the bash script called by job.sub
#-----------------------------------------------

export X509_USER_PROXY=$1
# voms-proxy-info -all

export postprocessExecutable=$2
export sample_info=$3           # path to file with the sample's TOTAL hEvents and hRuns TTree
export sample_full=$4           # path to TTree events .root file
export sample_name=$5           # name of process
export config_dir=$6            # path to config directory
export output_hist=$7           # path to output historam .root file
export xsec=$8                  # cross-section
export nentries=$9              # number of entries in this dataset according to DAS
export path_to_csv="${10}"      # path to .csv file that lists all the samples
export do_mutau="${11}"         # do mutau channel
export do_etau="${12}"          # do etau channel
export do_emu="${13}"           # do emu channel
export do_shifts="${14}"        # do systematic shifts or not?
export do_categories="${15}"    # do categories or not?
export do_all_variables="${16}" # do all variables or only m_vis?
export is_local="${17}"         # is local?
export dnn_full="${18}"         # path to DNN .root file, same number of events as above file

input_file=$(basename ${sample_full})
# eos cp the input files to the worker node

COMMAND="eos cp ${sample_full} ."
echo ${COMMAND}
eval ${COMMAND}

# MUTAU
if [[ ${do_mutau} == 1 ]]; then
    do_mt=1
    do_et=0
    do_em=0
    COMMAND="./${postprocessExecutable} ${sample_info} ${input_file} ${sample_name} ${config_dir} ${output_hist} ${xsec} ${nentries} ${path_to_csv} ${do_mt} ${do_et} ${do_em} ${do_shifts} ${do_categories} ${do_all_variables} ${is_local} ${dnn_full}"
    echo "Mutau: Attempting to execute: ${COMMAND}"
    eval ${COMMAND}
fi

# ETAU
if [[ ${do_etau} == 1 ]]; then
    do_mt=0
    do_et=1
    do_em=0
    COMMAND="./${postprocessExecutable} ${sample_info} ${input_file} ${sample_name} ${config_dir} ${output_hist} ${xsec} ${nentries} ${path_to_csv} ${do_mt} ${do_et} ${do_em} ${do_shifts} ${do_categories} ${do_all_variables} ${is_local} ${dnn_full}"
    echo "Etau: Attempting to execute: ${COMMAND}"
    eval ${COMMAND}
fi

# EMU
if [[ ${do_emu} == 1 ]]; then
    do_mt=0
    do_et=0
    do_em=1
    COMMAND="./${postprocessExecutable} ${sample_info} ${input_file} ${sample_name} ${config_dir} ${output_hist} ${xsec} ${nentries} ${path_to_csv} ${do_mt} ${do_et} ${do_em} ${do_shifts} ${do_categories} ${do_all_variables} ${is_local} ${dnn_full}"
    echo "Emu: Attempting to execute: ${COMMAND}"
    eval ${COMMAND}
fi

wait
rm ${input_file}
